<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// JOOMLA Instances
$app = JFactory::getApplication();

$current_layout = $app->input->get('layout', 'edit', 'string');

?>

<?php if($this->item_building_plans) : ?>
    <?php if(isset($this->item_building_plans[0])) : ?>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_0') ?></h4>
                <hr />

                <?php
                    $this->current_rooms = 0;

                    $this->setLayout('edit');
                    echo $this->loadTemplate('building_plans_list_table');
                    $this->setLayout($current_layout);
                ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if(isset($this->item_building_plans[1])) : ?>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_1') ?></h4>
                <hr />

                <?php
                    $this->current_rooms = 1;

                    $this->setLayout('edit');
                    echo $this->loadTemplate('building_plans_list_table');
                    $this->setLayout($current_layout);
                ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if(isset($this->item_building_plans[2]) && $this->item_building_plans[2]) : ?>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_2') ?></h4>
                <hr />

                <?php
                    $this->current_rooms = 2;

                    $this->setLayout('edit');
                    echo $this->loadTemplate('building_plans_list_table');
                    $this->setLayout($current_layout);
                ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if(isset($this->item_building_plans[3])) : ?>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_3') ?></h4>
                <hr />

                <?php
                    $this->current_rooms = 3;

                    $this->setLayout('edit');
                    echo $this->loadTemplate('building_plans_list_table');
                    $this->setLayout($current_layout);
                ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if(isset($this->item_building_plans[4])) : ?>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_4') ?></h4>
                <hr />

                <?php
                    $this->current_rooms = 4;

                    $this->setLayout('edit');
                    echo $this->loadTemplate('building_plans_list_table');
                    $this->setLayout($current_layout);
                ?>
            </div>
        </div>
    <?php endif; ?>





<?php endif; ?>