<?php

defined('_JEXEC') or die('Restricted access');

class SttNmlsModelRatings extends JModelList{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
			'ID', 'f.ID',
			'realname', 'f.realname',
			'NAME', 'f.NAME',
			'PHONE', 'f.PHONE',
			'k1','r.k1',
			'k2','r.k2',
			'k3','r.k3',
			'k4','r.k4',
			'k5','r.k5',
			'kitog','r.kitog'
			);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$cat = JRequest::getInt('cat', 0);
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select($this->getState('list.select', 'f.*, r.k1, r.k2, r.k3, r.k4, r.k5 ,r.kitog '));
		$query->from(' `#__sttnmlsvocfirms` AS f ');
		$query->join('LEFT','`#__sttnmlsratingf` as r on r.idfirm=f.ID ');
		$query->where(' f.flag<3');
		if($cat)
			$query->where(' f.ID in (select idfirm from #__sttnmlsfirmcatf where idcatf='.$db->quote($cat).')');
		$query->where(' f.active=1 ');
		$query->where(' IFNULL(r.kitog,0)>0 ');
		$this->addWhere('search', 'f.realname LIKE ', $query, true);
		$query->group('f.ID');
		$orderCol = $this->state->get('list.ordering', 'r.kitog');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if($orderCol=='name') $orderCol='realname';
		$query->order($db->escape($orderCol.' '.$orderDirn));
//		$sql = (string) $query;
//		echo $sql;

		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		$this->setState('list.limit', $limit);

		$this->setFilterState('search','');
		parent::populateState('kitog', 'asc');
		$limitstart = JRequest::getUInt('limitstart', 0);
		$this->setState('list.start', $limitstart); 
		
	} 
	public function setFilterState($par,$default=null)
	{
		$a=$this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
		$this->setState('filter.' .$par, $a);
	}
	public function addWhere($par,$fld,$query,$like=false)
	{
		if($par)
		$search = $this->getState('filter.'.$par);
		if($search)
		{
			if($like) $search = "'%$search%'";
			$query->where($fld . $search );
		}
	}

	public function getPagination() {
		$p = parent::getPagination();
		$cat = JRequest::getInt('cat', 0);
		if($cat) {
			$p->setAdditionalUrlParam('cat', $cat);
		}
		return $p;
	}
	public function getCategories($idfirm)
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		return SttNmlsHelper::getCategories($idfirm);
	}
}

