<?php defined('_JEXEC') or die('Restricted access'); 

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);

$gorod = '';
$object = '';
$raion = '';
$rooms = '';
$ulica = '';

?>
<div class="col-md-12" id="favorites">
    <h2><?php echo JText::_('COM_STTNMLS_FAVORITES'); ?></h2>
    <p style="text-align: center;">
        <?php echo JText::_('COM_STTNMLS_FAVORITES_MISC'); ?>
    </p>
    
    <div class="row-fluid text-right">
        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="glyphicon glyphicon-print"></span>
                Распечатать <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&print=1&typepdf=cli&format=raw') ?>">
                        Клиентский
                    </a>
                </li>
                <li>
                    <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&print=1&format=raw') ?>">
                        Агентский
                    </a>
                </li>
                <li>
                    <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&print=1&typepdf=agcli&format=raw') ?>">
                        Агентский с контактами клиента
                    </a>
                </li>
            </ul>
        </div>
        
        <a class="btn btn-default removeAllFromFavorites" href="javascript:void(0);">
            <span class="glyphicon glyphicon-remove"></span>
            <?php echo JText::_('COM_STTNMLS_BUTTON_REMOVE_FAVORITES') ?>
        </a>
    </div>
    <div class="clearfix"></div>
    
    <div class="row-fluid">
    <?php
        // Тип кукиса для объекта типа "Квартира/Комната"
        if(in_array($this->tp, array(0, 1)) && isset($this->items[1])) {
            echo $this->loadTemplate('favorites_tp1');
        }

        // Тип кукиса для объекта типа "Дом/Участок"
        if(in_array($this->tp, array(0, 3)) && isset($this->items[3])) {
            echo $this->loadTemplate('favorites_tp3');
        }

        // Тип кукиса для объекта типа "Коммерческая недвижимость"
        if(in_array($this->tp, array(0, 4)) && isset($this->items[4])) {
            echo $this->loadTemplate('favorites_tp4');
        }

        // Тип кукиса для объекта типа "Гараж"
        if(in_array($this->tp, array(0, 5)) && isset($this->items[5])) {
            echo $this->loadTemplate('favorites_tp5');
        }
    ?>
    </div>
    <div class="clearfix"></div>
</div>


