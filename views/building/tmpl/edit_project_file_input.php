<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>

<?php if($this->item->proj_file) : ?>
    <div class="thumbnail">
        <div class="btn-group">
            <a href="<?php echo JURI::root() . 'files/buildings/' . $this->item->proj_file ?>" class="btn btn-xs btn-default" target="_blank">
                <?php echo $this->item->proj_file ?>
            </a>
            <a href="javascript:void(0);" class="btn btn-xs btn-danger removeProjectFile" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="building">
                <span class="glyphicon glyphicon-remove"></span>
                <span class="sr-only">remove</span>
            </a>
        </div>
    </div>
<?php else: ?>
    <input type="text" id="project" class="form-control" name="jform[project]" value="<?php echo $this->item->project ?>" />
    <div class="help-block">
        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_PROJECT_DOCS_UPLOAD') ?>:<br />
        <input type="file" name="proj_doc" />
    </div>
<?php endif; ?>