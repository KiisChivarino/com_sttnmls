<?php defined('_JEXEC') or die('Restricted access'); 

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>

<?php if($this->item->PICTURES) : ?>
    <?php
        $pics = explode(';', trim($this->item->PICTURES));
        $picnames = explode(';', trim($this->item->PICNAMES));
    ?>
    <?php foreach ($pics as $k => $p) : ?>
    <div class="thumbnail">
        <div class="row" style="margin-left: 0; margin-right: 0;">
            <div class="col-sm-7 col-md-5 text-center" style="padding-left: 0;">
                <img class="img-thumbnail" src="<?php echo SttNmlsHelper::getLinkPhoto($p, 'smallcrop') . '&ts=' . time() ?>" /><br />
                <div style="margin-top:5px;">
                    <a href="javascript:void(0);" class="btn btn-success btn-xs rotateObjectImage" data-direction="left" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="house" data-idx="<?php echo $k ?>" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_ROTATE_TO_THE_LEFT', 90) ?>">
                        <img src="<?php echo JURI::root() . 'components/com_sttnmls/assets/images/icon_rotate_left.png' ?>" alt="Rotate to the left" style="width:20px;height:20px;" />
                    </a>
                    <b><?php echo JText::_('COM_STTNMLS_LABEL_DO_ROTATE') ?></b>
                    <a href="javascript:void(0);" class="btn btn-success btn-xs rotateObjectImage" data-direction="right" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="house" data-idx="<?php echo $k ?>" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_ROTATE_TO_THE_RIGHT', 90) ?>">
                        <img src="<?php echo JURI::root() . 'components/com_sttnmls/assets/images/icon_rotate_right.png' ?>" alt="Rotate to the right" style="width:20px;height:20px;" />
                    </a>
                </div>
            </div>
            <div class="col-sm-5 col-md-7">
                <input type="hidden" name="pics[]" value="<?php echo $p ?>" />
                <div class="form-group">
                    <label>
                        <?php echo JText::_('COM_STTNMLS_LABEL_PHOTO_TITLE') ?>
                    </label>
                    <input type="text" class="form-control" name="picnames[]" value="<?php echo $picnames[$k] ?>" />
                </div>
                <div class="form-group text-right">
                    
                    
                    <a href="javascript:void(0);" class="btn btn-danger btn-xs removeObjectImage" data-view="house"  data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-idx="<?php echo $k ?>">
                        <span class="glyphicon glyphicon-remove"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_DELETE') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
<?php else: ?>
    <p class="lead text-center">
        <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_NO_ITEMS') ?>
    </p>
<?php endif; ?>