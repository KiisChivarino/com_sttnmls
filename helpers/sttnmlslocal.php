<?php

defined('_JEXEC') or die;

abstract class SttNmlsLocal {
	
    public static function GetText($txt) {
        $params = JComponentHelper::getParams('com_sttnmls');
        $kursk = $params->get('kursk', 'Курск');
        $region = $params->get('region', 'Курская область');
        $portal = $params->get('portal', 'КурскМетр');
        $kurska = $params->get('kurska', 'Курска');
        $kurske = $params->get('kurske', 'Курске');
        $regione = $params->get('regione', 'Курской области');
        $gild = $params->get('gild', '');
        $gildurl = $params->get('gildurl', '');
        $res = $txt;
        $res = str_replace('kursk', $kursk, $res);
        $res = str_replace('region', $region, $res);
        $res = str_replace('portal', $portal, $res);
        $res = str_replace('kurska', $kurska, $res);
        $res = str_replace('kurske', $kurske, $res);
        $res = str_replace('regione', $regione, $res);
        $res = str_replace('gild', $gild, $res);
        $res = str_replace('gildurl', $gildurl, $res);
        return $res;
    }
    
    public static function TText($txt) {
        $res = JText::_($txt);
        return self::GetText($res);
    }


    public static function parseString($text, $data = array(), $filters = array())
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // JOOMLA Instances
        $params = JComponentHelper::getParams('com_sttnmls');

        // Выделение параметров чистки из строки
        $in = explode('|', $text);
        $text = $in[0];
        array_shift($in);
        $filters = array_merge($filters, $in);

        // Подстановка значений ----------------------------------------------------------------------------------------
        $options = array(
            '{gorod}'       =>      $params->get('kursk', 'Курск'),
            '{gorode}'      =>      $params->get('kurske', 'Курске'),
            '{goroda}'      =>      $params->get('kurska', 'Курска'),
            '{region}'      =>      $params->get('region', 'Курская область'),
            '{regione}'     =>      $params->get('regione', 'Курской области'),
            '{portal}'      =>      $params->get('portal', 'КурскМетр'),
            '{sitename}'    =>      $params->get('portal', 'КурскМетр'),
            '{siteurl}'     =>      JURI::root(),
            '{profile_link}'=>      SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile')
        );

        if($data) {
            foreach($data as $key => $value) {
                $options['{' . mb_strtolower($key) . '}'] = $value;
            }
        }
        $text = strtr($text, $options);
        // END Подстановка значений ------------------------------------------------------------------------------------



        // Обработка фильтров - поиск и замена -------------------------------------------------------------------------
        if(count($filters) > 0) {
            foreach($filters as $filter) {
                $exp = explode('@', $filter);
                $text = preg_replace($exp[0], ((isset($exp[1])) ? $exp[1] : ''), $text);
            }
        }
        // END Обработка фильтров - поиск и замена ---------------------------------------------------------------------

        return $text;
    }
}