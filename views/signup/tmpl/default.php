<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Подключение плагина капчи
JPluginHelper::importPlugin('captcha');

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$menu = $app->getMenu()->getActive();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());
$plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
$plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
$plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);


// Инициализация капчи
if($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
    JEventDispatcher::getInstance()->trigger('onInit', 'captcha');
}

?>
<div class="com_sttnmls">
    <h1><?php echo JText::_('COM_STTNMLS_LABEL_SIGNUP_STEP1') ?></h1>
    <form action="<?php echo JRoute::_('index.php?option=com_sttnmls&view=signup', TRUE, -1) ?>" class="form-validate form-horizontal" id="adminForm" method="post" novalidate="novalidate">
        <?php echo JHtml::_('form.token');?>
        <input type="hidden" name="task" value="signup.register" />
        <input type="hidden" name="step" value="1" />
        <div class="form-group">
            <label class="control-label col-sm-6 col-md-3">
                <?php
                    switch($this->realtor) {
                        case 1: $class = 'image-realtor'; break;
                        case 2: $class = 'image-specialist'; break;
                        default: $class = 'image-sobstvennik'; break;
                    }
                ?>
                <div id="account_type_image" class="<?php echo $class ?>"></div>
            </label>
            <div class="col-sm-6 col-md-9" style="padding-top: 20px;">
                
                <div class="radio">
                    <label>
                        <input type="radio" name="jform[profile][realtor]" value="0"<?php echo ((!$this->realtor) ? ' checked="checked"' : '') ?> class="account_type_selector" data-img-class="image-sobstvennik" />
                        <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SOBSTVENNIK'))) ?>
                    </label>
                </div>
                
                <div class="radio">
                    <label>
                        <input type="radio" name="jform[profile][realtor]" value="1"<?php echo (($this->realtor == 1) ? ' checked="checked"' : '') ?> class="account_type_selector showExtendFields" data-img-class="image-realtor" />
                        <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_REALTOR'))) ?>
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="jform[profile][realtor]" value="2"<?php echo (($this->realtor == 2) ? ' checked="checked"' : '') ?> class="account_type_selector showExtendFields" data-img-class="image-specialist" />
                        <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SPECIALIST'))) ?>
                    </label>
                </div>
            </div>
        </div>
        
        
        <div class="form-group">
            <label class="control-label col-sm-6 col-md-3" for="jform_email">
                <?php echo JText::_('COM_STTNMLS_LABEL_EMAIL') ?>
                <span class="required">*</span>
            </label>
            <div class="col-sm-6 col-md-4">
                <input type="email" name="jform[email1]" value="" style="display:none;"/>
                <input type="email" name="jform[email1]" id="jform_email" class="validate-email required form-control" autocomplete="off"/>
            </div>
        </div>

        <div id="extendFields" class="hide">
            <div class="form-group">
                <label class="control-label col-sm-6 col-md-3" for="jform_password1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_PASSWORD') ?>
                    <span class="required">*</span>
                </label>
                <div class="col-sm-6 col-md-4">
                    <input type="hidden" name="jform[password1]" style="display:none;"/>
                    <input type="password" name="jform[password1]" id="jform_password1" class="validate-password form-control" autocomplete="off"/>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-6 col-md-3" for="jform_password2">
                    <?php echo JText::_('COM_STTNMLS_LABEL_PASSWORD_RETRY') ?>
                    <span class="required">*</span>
                </label>
                <div class="col-sm-6 col-md-4">
                    <input type="hidden" name="jform[password2]" style="display:none;"/>
                    <input type="password" name="jform[password2]" id="jform_password2" class="validate-password form-control" autocomplete="off"/>
                </div>
            </div>
        </div>
        
        <?php if($params->get('use_captcha_security', TRUE) && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') : ?>
        <div class="form-group">
            <label class="control-label col-sm-6 col-md-3">
                <?php echo JText::_('COM_STTNMLS_LABEL_CAPTCHA') ?>
                <span class="required">*</span>
            </label>
            <div class="col-sm-6 col-md-4">
                <div class="g-recaptcha" data-sitekey="6LdcKwcTAAAAAG3VVXdl_ItwbhYYiQTjpmaiPMD_"></div>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="form-group">
            <div class="col-md-12">
                <div class="alert alert-info">
                    <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_SIGNUP_EMAIL_ALERT') ?>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-6 col-md-offset-3 col-md-4">
                <button type="submit" class="validate btn btn-success">
                    <?php echo JText::_('COM_STTNMLS_BUTTON_CONTINUE') ?>
                    <span class="glyphicon glyphicon-arrow-right"></span>
                </button>
            </div>
        </div>
        
    </form>
</div>