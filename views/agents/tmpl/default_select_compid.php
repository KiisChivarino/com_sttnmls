<?php defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();

?>
<?php if($this->firms) : ?>
    <select id="filter_compid" name="filter_compid" class="form-control input-sm">
        <option value="0">Все записи</option>
        <?php foreach($this->firms as $firm) : ?>
            <option value="<?php echo $firm->ID ?>"<?php echo (($firm->ID == $this->state->get('filter.compid')) ? ' selected="selected"' : '') ?>><?php echo $firm->realname ?></option>
        <?php endforeach; ?>
    </select>
<?php endif; ?>