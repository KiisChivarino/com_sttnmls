<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

$_new_plan = (($this->building_plan) ? FALSE : TRUE);

?>

<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&task=jsonSaveBuildingPlan')?>" class="form-horizontal" method="post" enctype="multipart/form-data">
            <?php echo JHTML::_( 'form.token' ); ?>
            <input type="hidden" name="bplan_id" value="<?php echo (($_new_plan) ? 0 : $this->building_plan->id) ?>" />
            <input type="hidden" name="cn" value="<?php echo $this->item->CARDNUM ?>" />
            <input type="hidden" name="cid" value="<?php echo $this->item->COMPID ?>" />

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo JText::_('COM_STTNMLS_LABEL_BUILD_PLAN_' . (($_new_plan) ? 'NEW' : 'EDIT')) ?></h4>
            </div>

            <div class="modal-body">
                <!-- VAR #1: Rooms count -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_ROOMS_COUNT') ?>
                    </label>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                    <?php
                        $options = array();
                        $options[] = JHtml::_('select.option', 0, 'Студия');
                        $options[] = JHtml::_('select.option', 1, '1-комнатная');
                        $options[] = JHtml::_('select.option', 2, '2-комнатная');
                        $options[] = JHtml::_('select.option', 3, '3-комнатная');
                        $options[] = JHtml::_('select.option', 4, '4-комнатная');
                        echo JHtml::_('select.genericlist', $options, 'rooms', ' class="form-control"', 'value', 'text', (($_new_plan) ? 0 : $this->building_plan->rooms));
                    ?>
                    </div>
                </div>
                <!-- /VAR #1: Rooms count -->

                <!-- VAR #2: Total square -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_AREA_TOTAL') ?>
                    </label>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <input type="text" name="sall" class="form-control validate-numeric required" value="<?php echo (($_new_plan) ? '' : $this->building_plan->sall) ?>" />
                    </div>
                </div>
                <!-- /VAR #2: Total square -->

                <!-- VAR #3: Live square -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_AREA_LIVE') ?>
                    </label>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <input type="text" name="szhil" class="form-control validate-numeric required" value="<?php echo (($_new_plan) ? '' : $this->building_plan->szhil) ?>" />
                    </div>
                </div>
                <!-- /VAR #3: Live square -->

                <!-- VAR #4: Kitchen square -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_AREA_KITCHEN') ?>
                    </label>
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <input type="text" name="skitch" class="form-control validate-numeric required" value="<?php echo (($_new_plan) ? '' : $this->building_plan->skitch) ?>" />
                    </div>
                </div>
                <!-- /VAR #4: Kitchen square -->

                <!-- VAR #5: WC -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_WC_TYPE') ?>
                    </label>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <label class="radio-inline">
                            <input type="radio" name="san" value="0"<?php echo (($_new_plan OR (!$_new_plan && $this->building_plan->san == 0)) ? ' checked="checked"' : '') ?> />
                            <?php echo JText::_('COM_STTNMLS_LABEL_WC_TYPE_0') ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="san" value="1"<?php echo ((!$_new_plan && $this->building_plan->san == 1) ? ' checked="checked"' : '') ?> />
                            <?php echo JText::_('COM_STTNMLS_LABEL_WC_TYPE_1') ?>
                        </label>
                    </div>
                </div>
                <!-- /VAR #5: WC -->

                <!-- VAR #6: Balkon -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_BAL') ?>
                    </label>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="bal" value="1"<?php echo ((!$_new_plan && $this->building_plan->bal) ? ' checked="checked"' : '') ?> />
                            <?php echo JText::_('JYES') ?>
                        </label>
                    </div>
                </div>
                <!-- /VAR #6: Balkon -->

                <!-- VAR #7: Price qm -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_EDIT_PRICEQM') ?>
                    </label>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <input type="text" name="price" class="form-control validate-numeric required" value="<?php echo (($_new_plan) ? '' : $this->building_plan->price) ?>" />
                    </div>
                </div>
                <!-- /VAR #7: Price qm -->

                <!-- VAR #8: Image -->
                <div class="form-group">
                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                        <?php echo JText::_('COM_STTNMLS_LABEL_PHOTO') ?>
                    </label>
                    <div class="col-md-7 col-sm-6 col-xs-12">
                        <?php if(!$_new_plan && $this->building_plan->photos != '') : ?>
                            <img class="img-thumbnail" src="<?php echo SttNmlsHelper::getLinkPhoto($this->building_plan->photos, 'smallcrop', 'images/objects/') ?>" />
                            <br />
                            <br />
                        <?php endif; ?>
                        <input type="file" name="photo[]" />
                    </div>
                </div>
                <!-- /VAR #8: Image -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success validate saveData"><?php echo JText::_('JSave') ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JText::_('JCancel') ?></button>
            </div>
        </form>
    </div>
</div>