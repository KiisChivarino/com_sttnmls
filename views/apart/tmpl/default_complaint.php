<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/complaint.php';

// Загрузка JOOMLA фреймворков
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');

// JOOMLA Instances
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
$plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
$plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

$script .= <<<JS
    jQuery(document).ready(function($){
        document.formvalidator.setHandler('sttnum', function(value) {
			if(parseInt(value) === 0) return false;
			regex = /^-?\d+((\.|\,)\d+)?$/;
			return regex.test(value);
        });
    });

JS;
$doc->addScriptDeclaration($script);

$rs = Complaint::getOptReason();
$userid = JFactory::getUser()->id;

?>
<form class="form-horizontal form-validate" action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'apart', '&type=card&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID) ?>" method="post" novalidate="novalidate">
    <?php echo JHtml::_('form.token') ?>
    <input type="hidden" name="task" value="complaint" />
    <input type="hidden" name="view" value="apart" />
    <input type="hidden" name="cn" value="<?php echo $this->item->CARDNUM ?>" />
    <input type="hidden" name="cid" value="<?php echo $this->item->COMPID ?>" />
    <input type="hidden" name="typeobj" value="1" />

    <h4 class="text-center">
        <?php echo JText::_('COM_STTNMLS_COMPLAINT_TEXT'); ?>
    </h4>
    
    <div class="form-group" style="margin-top: 20px;">
        <label class="col-sm-4 control-label"><?php echo JText::_('COM_STTNMLS_COMPLAINT_REASON'); ?></label>
        <div class="col-sm-6">
            <?php echo $rs; ?>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo JText::_('COM_STTNMLS_COMPLAINT_DESC'); ?></label>
        <div class="col-sm-6">
            <textarea id="description" class="form-control" name="description" rows="6"></textarea>
        </div>
    </div>
    
    <?php if(!$userid) : ?>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo JText::_('COM_STTNMLS_COMPLAINT_NAME'); ?><span class="required">*</span></label>
        <div class="col-sm-6">
            <input type="text" id="complname" name="complname" class="form-control required"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo JText::_('COM_STTNMLS_COMPLAINT_MAIL'); ?><span class="required">*</span></label>
        <div class="col-sm-6">
            <input type="text" id="complemail" name="complemail" class="form-control validate-email required" />
        </div>
    </div>
    <?php endif; ?>

    <?php if($params->get('use_captcha_security', TRUE) && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') : ?>
        <div class="form-group">
            <label class="control-label col-sm-4">
                <?php echo JText::_('COM_STTNMLS_LABEL_CAPTCHA') ?>
                <span class="required">*</span>
            </label>
            <div class="col-sm-6">
                <div id="captcha_user_request_form"></div>
            </div>
        </div>
    <?php endif; ?>
    
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-6">
            <button type="submit" id="complsubmit" class="btn btn-success validate">
                <?php echo JText::_('COM_STTNMLS_SEND'); ?>
            </button>
        </div>
    </div>
</form>