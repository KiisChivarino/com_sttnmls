<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' ;
require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';

?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/com_sttnmls.css">
<div class="sttnmls"> 
	<h2><?php echo $this->title->head; ?></h2>
	<div class="fdescr">
		<?php echo $this->title->descr; ?>
	</div>
	
	<form action="<?php echo JRoute::_('index.php?option=com_sttnmls&view=ratings',false); ?>" method="post" name="adminForm" id="adminForm">
	<div class="table-responsive">
        <table class="table table-striped">
            <thead class="table-header-text-center">
            <tr>
                <th align="center"  style="white-space: normal;"><?php echo JHtml::_('grid.sort',  'COM_STTNMLS_RATING_MESTO', 'r.kitog', $listDirn, $listOrder); ?></th>
                <th align="center"><?php echo JHtml::_('grid.sort',  'COM_STTNMLS_AGENC', 'f.realname', $listDirn, $listOrder); ?></th>
                <th align="center"  style="white-space: normal;"><?php echo JHtml::_('grid.sort',  'COM_STTNMLS_RATING3', 'r.k3', $listDirn, $listOrder); ?></th>
                <th align="center"  style="white-space: normal;"><?php echo JHtml::_('grid.sort',  'COM_STTNMLS_RATING4', 'r.k4', $listDirn, $listOrder); ?></th>
                <th align="center"  style="white-space: normal;"><?php echo JHtml::_('grid.sort',  'COM_STTNMLS_RATING5', 'r.k5', $listDirn, $listOrder); ?></th>
            </tr>
            </thead>
            <?php
            $my_inc = 0;
            foreach ($this->items as $item) {
                $link =  ($item->article_url != '') ? $item->article_url : 'component/sttnmls/?view=firm&agency='. $item->ID;
                $imgfirm = Avatar::getLinkFirmLogo($item->ID,0);

                $style = ($my_inc % 2) ? 'odd0' : 'even0';
                $name = $item->realname;
                if(!$name) $name = $item->NAME;
                if(!$name) $name = JText::_('COM_STTNMLS_FIRMS_NONAME');
                ?>
                <tr class="middle <?php echo $style; ?>" onmouseover="this.className='middle target' " onmouseout="this.className='middle <?php echo $style; ?>'">
                    <td align="center">
                        <?php echo round($item->kitog); ?>
                    </td>
                    <td>
                        <a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a>
                    </td>
                    <td align="center">
                        <?php echo round($item->k3,2); ?>
                    </td>
                    <td align="center">
                        <?php echo round($item->k4,2); ?>
                    </td>
                    <td align="center">
                        <?php echo round($item->k5); ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
	<div class="pagination">
		<?php echo $this->pagination->getListFooter(); ?>
	</div>
<!--	<input type="hidden" name="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" />-->
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</form>

</div>
