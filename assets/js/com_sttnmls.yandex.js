function init () {
    ymaps.geocode(g_u, {results: 1}).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0);
        var coords = firstGeoObject.geometry.getCoordinates()

        window.myMap = new ymaps.Map("YMapsID", {
            center: coords,
            zoom: _zoom
        });

        myPlacemark = new ymaps.Placemark(coords,{
            balloonContentBody: g_u
        },{preset: 'twirl#buildingsIcon'});

        myMap.controls
            .add('smallZoomControl', {left: 5, top: 120})
            .add('mapTools');

        myMap.geoObjects.add(myPlacemark);
    });
}
