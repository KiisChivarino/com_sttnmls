<?php defined('_JEXEC') or die('Restricted access');

class SttnmlsControllerJson extends JControllerLegacy
{
    /**
     * Формирует JSON ответ сервера
     *
     * @param           string                  $task           Метод этого класса
     * @param           array                   $result         Результат выполнения
     * @param           string                  $component      Название компонента
     */
    protected function _sendJsonResponse(
        $task,
        $result = array(
            'result' => false,
            'messages' => array()
        ),
        $component = 'com_sttnmls'
    )
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=' . $component . '&task=json.' . $task . '&format=raw') . '"');

        echo json_encode($result);
        $app->close();
    }

    public function changeExpiredObjectEditDate()
    {
        $model = $this->getModel('aparts');
        $result['result'] = $model->changeExpiredObjectEditDate();
        $result['message'] = $model->getError();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }

    /**
     * Изменение даты обновления объектов
     */
    public function changeExpiredObjectEditDateAll()
    {
        $model = $this->getModel('aparts');
        $result['result'] = $model->changeExpiredObjectEditDateALL();
        $result['message'] = $model->getError();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }


    public function uploadAvatarImage()
    {
        $model = $this->getModel('agent');
        $image = $model->uploadImage();

        $result['id'] = (($image) ? $image[0]->output_raw_filename : false);
        $result['message'] = $model->getError();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }

    public function createAvatar()
    {
        $model = $this->getModel('agent');
        $result['result'] = $model->createAvatar();
        $result['message'] = $model->getError();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }


    public function setObjectCheckedState()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();


        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=json.setObjectCheckedState&format=raw') . '"');

        $result = array(
            'result'     =>  array(),
            'messages'    =>  array()
        );

        //
        $view = $app->input->get('view', 'aparts', 'string');

        $model = $this->getModel($view);
        $result['result'] = $model->setObjectCheckState();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        $app->close();
    }


    public function uploadBuildingProgressPhoto()
    {
        //
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();


        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=uploadObjectPhoto&format=raw') . '"');


        $result = array(
            'files'     =>  array(),
            'errors'    =>  array()
        );

        //
        $view = $app->input->get('view', 'building', 'string');

        $model = $this->getModel($view);
        $result['files'] = $model->addBuildingProgressPhoto();
        $result['errors'] = $model->getErrors();

        echo json_encode($result);
        $app->close();
    }


    public function repairObjectFromArchive()
    {
        $app = JFactory::getApplication();

        $view = $app->input->get('view', 'apart', 'string');
        $oid = $app->input->get('oid', 0, 'uint');

        $model = $this->getModel($view);
        $result['result'] = $model->repairObjectFromArchive($view, $oid);
        $result['messages'] = $model->getErrors();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }

    public function removeObjectFromArchive()
    {
        $app = JFactory::getApplication();

        $view = $app->input->get('view', 'apart', 'string');
        $oid = $app->input->get('oid', 0, 'uint');

        $model = $this->getModel($view);
        $result['result'] = $model->removeObjectFromArchive($view, $oid);
        $result['messages'] = $model->getErrors();

        $this->_sendJsonResponse(__FUNCTION__, $result);
    }

}