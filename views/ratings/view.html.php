<?php
defined('_JEXEC') or die ('Restricted access');

class SttNmlsViewRatings extends JViewLegacy
{
	function display($tpl = null) 
	{
		$items = $this->get('Items');
		$this->items = $items;
		$state = $this->get('State');
		$pagination = $this->get('Pagination');
		
		

		$this->model = $this->getModel();
		$this->pagination = $pagination;
		$this->state = $state;
		$this->params = $state->get('parameters.menu');
		if($this->params)
			$this->_prepareDocument();
		parent::display($tpl);
	}

	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$title	= null;

		$title = $this->params->get('page_title', '');
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$document = JFactory::getDocument();
		$document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$document->setMetadata('robots', $this->params->get('robots'));
		}
	} 
	
}
