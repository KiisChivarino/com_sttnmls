<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/helpers/class.sttnmlslist.php');

class SttnmlsModelAparts extends SttnmlsModelList
{
    // Индекс объекта в избранном (1-5)
    protected $favoriteObjectType = 1;
    // Название таблицы объектов в зависимости от отображаемой модели
    protected $objectsTableName = '#__sttnmlsaparts';

    protected function getDefaultFiltersValue()
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        $filters = array(
            // ОПИСАНИЕ ФИЛЬТРОВ:
            //    [имя_поля_фильтра]    =>  [массив значений]
            //
            //    (!) - обязательное значение фильтра
            //    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
            //
            // ЗНАЧЕНИЯ ФИЛЬТРОВ:
            //      "type"      -   (!) тип значения фильтра
            //      "value"     -   (!) значение фильтра по умолчанию
            //
            //      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
            //
            //      [!] >>>
            //      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
            //                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
            //
            //      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
            //
            //      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
            //                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
            //
            //      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
            //                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
            //                              либо завершенную строку WHERE запроса
            //      <<< [!]
            //

            'rooms'             =>  array('type' => 'array', 'value' => array('null')),
            'mw'                =>  array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'mw', 'valueFromTable' => '#__sttnmlsvocalldata', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s) AND ' . $db->quoteName('RAZDELID') . '=' . $db->quote(8)),
            'newbuild_selector' =>  array('type' => 'uint', 'value' => 2, 'valueFromArray' => array(JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_0'), JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_1'), JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_2'))),
            'price_day'         =>  array('type' => 'bool', 'value' => FALSE),
            'price_hour'        =>  array('type' => 'bool', 'value' => FALSE),
            'kitch_ot'          =>  array('type' => 'uint', 'value' => 0),
            'kitch_do'          =>  array('type' => 'uint', 'value' => 0),
        );
        return array_merge(parent::getDefaultFiltersValue(), $filters);
    }

    protected function setPersonalConditions()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Request VARS ------------------------------------------------------------------------------------------------
        $builder = $app->input->getUint('builder', 0);
        $building = $app->input->getUint('building', 0);
        $act = $app->input->getString('act', '');

        // State VARS --------------------------------------------------------------------------------------------------
        if(isset($_REQUEST['filter_rooms']) && !is_array($_REQUEST['filter_rooms']) && $app->input->getUint('filter_rooms', 0)) {
            $this->_updateFilterState = TRUE;
            $this->setFilterState('rooms', array(0 => 'null', 1 => $app->input->getUint('filter_rooms', 0)));
            $this->_updateFilterState = FALSE;
        }
        $filter_rooms = $this->getState('filter.rooms', array('null'));


        $newbuild_type = $this->getState('filter.newbuild_selector', 2);
        $_price_day = $this->getState('filter.price_day', FALSE);
        $_price_hour = $this->getState('filter.price_hour', FALSE);

        // Раздел SELECT запроса ---------------------------------------------------------------------------------------
        $this->query->select($db->quoteName('c.NAME', 'gorod') . ', ' . $db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d4.NAME', 'tip'));
        $this->query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
        $this->query->select('IF(' . $db->quoteName('o.WHAT') . ' = 0, "квартира", "комната") AS ' . $db->quoteName('object'));
        $this->query->select('CONCAT(' . $db->quoteName('o.STAGE') . ', "/", ' . $db->quoteName('o.HSTAGE') . ', "-", ' . $db->quoteName('d8.SHORTNAME') . ') AS ' . $db->quoteName('planirovka'));
        $this->query->select('CONCAT(TRUNCATE(' . $db->quoteName('o.AAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.LAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.KAREA') . ', 0)) AS ' . $db->quoteName('o_zh_k'));
        $this->query->select($db->quoteName('barel.builder', 'builder') . ', ' . $db->quoteName('barel.builder_cardnum', 'building'));
        $this->query->select($db->quoteName('building.title', 'building_title'));

        // Раздел JOIN запроса -----------------------------------------------------------------------------------------
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd4') . ' ON (' . $db->quoteName('o.HTYPEID') . '=' . $db->quoteName('d4.id') . ' AND ' . $db->quoteName('d4.RAZDELID') . '=' . $db->quote(4) . ')');
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON (' . $db->quoteName('o.WMATERID') . '=' . $db->quoteName('d8.id') . ' AND ' . $db->quoteName('d8.RAZDELID') . '=' . $db->quote(8) . ')');

        $this->query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.aparts_compid') . '=' . $db->quoteName('o.COMPID') . ' AND ' . $db->quoteName('barel.aparts_cardnum') . '=' . $db->quoteName('o.CARDNUM') . ')');
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsbuildings', 'building') . ' ON(' . $db->quoteName('barel.builder') . '=' . $db->quoteName('building.comp') . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quoteName('building.CARDNUM') . ')');


        // Раздел WHERE запроса ----------------------------------------------------------------------------------------
        if($act != 'pdf') {
            $this->query->where($db->quoteName('o.WHAT') . (($this->what) ? '>' : '=') . $db->quote(0));
        }

        //> Переданы значения ID Застройщика и Новостройки
        if($builder && $building) {
            //> Поиск по застройщику, если переданы его данные
            $this->query->where('(' . $db->quoteName('barel.builder') . '=' . $db->quote($builder) . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quote($building) . ')');
        }

        //> Передан тип поиска "Новостройка, вторичка или оба"
        switch($newbuild_type) {
            case 0:
                //> Поиск только вторички
                $this->query->where($db->quoteName('o.NEWBUILD') . '=' . $db->quote(0));
                break;

            case 1:
                //> Поиск только новостроек
                $this->query->where($db->quoteName('o.NEWBUILD') . '=' . $db->quote(1));
                break;
        }

        // Учет запрошенного кол-ва комнат в объектах ------------------------------------------------------------------
        if($filter_rooms && count($filter_rooms) >= 1) {
            //> Удаление нулевого значения из массива комнат
            foreach($filter_rooms as $k => $v) {
                if($v == 'null') {
                    unset($filter_rooms[$k]);
                }
            }

            if($filter_rooms) {
                $t = '';
                foreach($filter_rooms as $k => $v) {
                    $t .= $db->quoteName('o.ROOMS') . (($v == 4) ? '>=' : '=') . $db->quote($v) . ' OR ';
                }
                if($t != '') {
                    $this->query->where('(' . rtrim($t, ' OR ') . ')');
                }
            }
        }
        // END Учет запрошенного кол-ва комнат в объектах --------------------------------------------------------------


        // Учет посуточной и почасовой аренды для видов КВАРТИРЫ/КОМНАТЫ -----------------------------------------------
        if($this->stype) {
            if($_price_day && $_price_hour) {
                //> Установлены флаги посуточной и почасовой аренды
                $this->query->where($db->quoteName('o.PRICE_DAY') . '>0 OR ' . $db->quoteName('o.PRICE_HOUR') . '>0');
            } else {
                if($_price_day) {
                    //> Установлен флаг посуточной аренды
                    $this->query->where($db->quoteName('o.PRICE_DAY') . '>0');
                }
                if($_price_hour) {
                    //> Установлен флаг почасовой аренды
                    $this->query->where($db->quoteName('o.PRICE_HOUR') . '>0');
                }
            }
        }
        // END Учет посуточной и почасовой аренды для видов КВАРТИРЫ/КОМНАТЫ и КОММЕРЧЕСКАЯ ----------------------------


        // Учет запрошенного материала стен ----------------------------------------------------------------------------
        $this->setAdditionalWhereFromStateCheckbox($this->query, 'mw', 'o.WMATERID');
        // END Учет запрошенного материала стен ------------------------------------------------------------------------

        // Учет площади кухни ------------------------------------------------------------------------------------------
        $this->setAdditionalWhereFromState($this->query, 'kitch_ot', 'o.KAREA', '>=');
        $this->setAdditionalWhereFromState($this->query, 'kitch_do', 'o.KAREA', '<=');
        // END Учет площади кухни --------------------------------------------------------------------------------------


        if($act == 'pdf') {
            $this->query->order($db->quoteName('o.WHAT') . ' ASC, ' . $db->quoteName('o.CITYID') . ' ASC, ' . $db->quoteName('o.RAIONID') . ' ASC, ' . $db->quoteName('o.ROOMS') . ' ASC, ' . $db->quoteName('ulica') . ' ASC, ' . $db->quoteName('o.PRICE'));
        }
    }

}
