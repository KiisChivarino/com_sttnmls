<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="com_sttnmls_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 830px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php echo JText::_('COM_STTNMLS_LABEL_IMAGE_REGION_SELECT') ?>
                </h4>
            </div>
            <div class="modal-body">
                <p class="text-justify" style="margin-bottom: 20px;">
                    <?php echo JText::_('COM_STTNMLS_LABEL_IMAGE_REGION_SELECT_DESC') ?>
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <img id="target_image" src="<?php echo $this->avatar ?>" alt="" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <form
                    action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '', 'profile') ?>"
                    method="post"
                    id="com_sttnmls_thumbnail_form"
                >
                    <?php echo JHtml::_( 'form.token' ); ?>
                    <input type="hidden" name="id" value="<?php echo $this->id ?>" />
                    <input type="hidden" name="task" value="json.createAvatar" />
                    <input type="hidden" id="crop_x" name="x" value="0" />
                    <input type="hidden" id="crop_y" name="y" value="0" />
                    <input type="hidden" id="crop_w" name="w" value="0" />
                    <input type="hidden" id="crop_h" name="h" value="0" />

                    <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_STTNMLS_BUTTON_SAVE') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#target_image').on('load', function(){
            var width = $(this).get(0).width + 30;
            $('#com_sttnmls_modal .modal-dialog').width(width);
        });

        $('#target_image').Jcrop({
            minSize: [250, 250],
            aspectRatio: 1,
            onChange: showPreview,
            onSelect: showPreview,
            setSelect: [0, 0, 250, 250]
        });

        $('#com_sttnmls_thumbnail_form').on('submit', function(e){
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function(response) {
                    if(!response.result) {
                        alert(response.message);
                    } else {
                        window.location.reload();
                    }
                }
            });
            e.preventDefault();
        });

        function showPreview(coords)
        {
            $('#crop_x').val(coords.x);
            $('#crop_y').val(coords.y);
            $('#crop_w').val(coords.w);
            $('#crop_h').val(coords.h);
        }
    });
</script>

