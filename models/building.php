<?php defined('_JEXEC') or die('Restricted access');

require_once JPATH_COMPONENT . '/helpers/sttnmlsitem.php';

jimport('joomla.application.component.helper');

class SttNmlsModelBuilding extends SttNmlsModelItem
{
	public function __construct($config = array()) {
		$this->setTabname('#__sttnmlsbuildings');
		$this->setTp('1');
		parent::__construct($config);
	}

    public function getBuildingsTypes()
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlstype'));
        $query->order($db->quoteName('id') . ' ASC');
        $db->setQuery($query);

        return $db->loadObjectList('id');
    }

    public function getPaymentTypes()
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlspay'));
        $query->order($db->quoteName('id') . ' ASC');
        $db->setQuery($query);

        return $db->loadObjectList('id');
    }


    public function getBanksList()
    {
        $result = FALSE;

        // JOOMLA Instances
        $params = JComponentHelper::getParams('com_sttnmls');
        $db = JFactory::getDbo();
        $banks_category = $params->get('banks_category', 0);

        if($banks_category) {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('f') . '.*');
            $query->from($db->quoteName('#__sttnmlsvocfirms', 'f'));
            $query->join('LEFT', $db->quoteName('#__sttnmlsfirmcatf', 'fc') . ' ON (' . $db->quoteName('f.ID') . '=' . $db->quoteName('fc.idfirm') . ')');
            $query->where($db->quoteName('fc.idcatf') . '=' . $db->quote($banks_category));
            $query->order($db->quoteName('NAME'));
            $db->setQuery($query);
            $result = $db->loadObjectList('ID');
        }
        return $result;
    }

    public function getBuildingPlans($cardnum, $compid)
    {
        $result = FALSE;

        // JOOMLA Instances
        $db = JFactory::getDbo();

        if($cardnum && $compid) {
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsbplan'));
            $query->where($db->quoteName('bid') . '=' . $db->quote($cardnum));
            $query->where($db->quoteName('cid') . '=' . $db->quote($compid));
            $db->setQuery($query);
            $r = $db->loadObjectList('id');

            if($r) {
                foreach($r as $item) {
                    $rooms = $item->rooms;
                    if($rooms > 4) { $rooms = 4; }
                    $result[$rooms][] = $item;
                }
            }
        }

        return $result;
    }

    public function getBuildingPlan($plan_id)
    {
        $result = FALSE;

        // JOOMLA Instances
        $db = JFactory::getDbo();

        if($plan_id) {
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsbplan'));
            $query->where($db->quoteName('id') . '=' . $db->quote($plan_id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }

        return $result;
    }

    public function addBuildingProgressPhoto()
    {
        // Хелпер аплоадера картинок
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmls_image.php');

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cardnum', 0, 'uint');
        $compid = $app->input->get('compid', 0, 'uint');
        $viewName = $app->input->get('view', 'building', 'string');


        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }


        // Шаг.1. Загрузка изображения(й) на сервер ----------------------------
        $config = array(
            'prefix_name'               =>  'bprgs',
            'use_prefix_as_imagename'   =>  FALSE,
            'store_path'                =>  'images/objects/',
            'width'                     =>  640,
            'height'                    =>  480,
            'resize'                    =>  TRUE,
            'types'                     =>  array(2, 3),
            'create_thumb'              =>  FALSE
        );
        $avatar = SttnmlsHelperImage::getInstance();
        $avatar->setConfig($config);
        $result = $avatar->upload('bprogress');

        $this->_errors = $avatar->getErrors();
        if(!$result OR $this->_errors) { return FALSE; }


        // Шаг.2. Запись информации о фото в базу данных для объекта -----------
        $new_object_images = array();
        $current_date = date('d.m.Y');
        foreach($result as $upimage) {
            $image = new stdClass();
            $image->src = $upimage->output_filename;
            $image->name = 'Фото';
            $new_object_images[$current_date][] = $image;
        }

        // Получение картинок объекта ------------------------------------------
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('progress_photos'));
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('comp') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $building_progress_images = unserialize($db->loadResult());

        if($building_progress_images) {
            foreach($new_object_images as $date => $photos) {
                foreach($photos as $photo) {
                    $building_progress_images[$date][] = $photo;
                }
            }
        } else {
            $building_progress_images = $new_object_images;
        }


        // Обновление информации о картинках в БД объекта
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsbuildings'));
        $query->set($db->quoteName('progress_photos') . '=' . $db->quote(serialize($building_progress_images)));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('comp') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $db->execute();

        return $result;
    }




    function save()
    {
        // Joomla инстанции
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Получение из запроса сохраняемых данных объекта
        $data = $app->input->post->get('jform', array(), 'array');

        $data['sdan'] = $app->input->post->get('sdan', 0, 'uint');

        // Подготовка данных для сохранения
        $data['cityid'] = ((!isset($data['cityid']) ? -1 : $data['cityid']));

        if(isset($data['htypeid']) && $data['htypeid']) {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName(array('ID', 'SHORTNAME', 'NAME', 'CITYID')));
            $query->from($db->quoteName('#__sttnmlsvocalldata'));
            $query->where($db->quoteName('RAZDELID') . '=' . $db->quote(4));
            $query->where($db->quoteName('ID') . '=' . $db->quote($data['htypeid']));
            $db->setQuery($query);

            $result = $db->loadObject();

            if($result && $result->CITYID != $data['cityid']) {
                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('ID'));
                $query->from($db->quoteName('#__sttnmlsvocalldata'));
                $query->where($db->quoteName('SHORTNAME') . '=' . $db->quote($result->SHORTNAME));
                $query->where($db->quoteName('CITYID') . '=' . $db->quote($data['cityid']));
                $htypeid = $db->loadResult();

                $data['htypeid'] = (($htypeid) ? $htypeid : $data['htypeid']);
            }
        }


        $data['type'] = json_encode($data['type']);
        $data['pay'] = json_encode($data['pay']);
        $data['banks'] = json_encode($data['banks']);

        $pictures = $app->input->post->get('pics', array(), 'array');
        $picnames = $app->input->post->get('picnames', array(), 'array');

        if($pictures) {
            $data['PICTURES'] = implode(';', $pictures);
            $data['PICNAMES'] = implode(';', $picnames);
        }


        // Подготовка сохранения хода строительства
        $building_progress_photos = $app->input->get('progress_photo', array(), 'array');
        if($building_progress_photos) {
            foreach($building_progress_photos as $date => $photos) {
                foreach($photos['image'] as $i => $item) {
                    $image = new stdClass();
                    $image->src = $photos['image'][$i];
                    $image->name = $photos['name'][$i];
                    $data['progress_photos'][$date][] = $image;
                }
            }
        }
        $data['progress_photos'] = serialize($data['progress_photos']);

        $object = array();
        if($data['cardnum'] && $data['compid']) {
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsbuildings'));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($data['cardnum']));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($data['compid']));
            $db->setQuery($query);
            $object = $db->loadObject();
        }


        if($_FILES['proj_doc']['size'] > 0 && $_FILES['proj_doc']['error'] == 0) {
            $whitelist = array(".doc", ".docx", ".pdf", ".rtf", ".jpg", ".jpeg", ".png", ".JPG", ".JPEG");
            $file_up = 0;
            $extension = '';
            foreach ($whitelist as $item) {
                if(preg_match("/$item\$/i", $_FILES['proj_doc']['name'], $ext))
                {
                    $file_up = 1;
                    $extension = $ext[0];
                }
            }
            if($file_up == 1) {
                $uploaddir = JPATH_ROOT . '/files/buildings/';
                if(!file_exists($uploaddir)) {
                    try {
                        mkdir($uploaddir, 0755, TRUE);
                    } catch(Exception $e) {}
                }

                $newFileName = date('YmdHis') . rand(10,100) . $extension;
                $uploadfile = $uploaddir . $newFileName;
                if(move_uploaded_file($_FILES['proj_doc']['tmp_name'], $uploadfile)) {
                    $data['proj_file'] = $newFileName;
                }

                if($object) {
                    $proj_file_path = $uploaddir . $object->proj_file;
                    if($object->proj_file != '' && file_exists($proj_file_path)) {
                        try {
                            unlink($proj_file_path);
                        } catch(Exception $e) {}
                    }
                }
            }
        }

        if($_FILES['dogovor_file']['size'] > 0 && $_FILES['dogovor_file']['error'] == 0) {
            $whitelist = array(".doc", ".docx", ".pdf", ".rtf", ".jpg", ".jpeg", ".png", ".JPG", ".JPEG");
            $file_up = 0;
            $extension = '';
            foreach ($whitelist as $item) {
                if(preg_match("/$item\$/i", $_FILES['dogovor_file']['name'], $ext)) {
                    $file_up = 1;
                    $extension = $ext[0];
                }
            }
            if($file_up == 1) {
                $uploaddir = JPATH_ROOT . '/files/buildings/';
                if(!file_exists($uploaddir)) {
                    try {
                        mkdir($uploaddir, 0755, TRUE);
                    } catch(Exception $e) {}
                }

                $newFileName = date('YmdHis') . rand(10,100) . $extension;
                $uploadfile = $uploaddir . $newFileName;
                if(move_uploaded_file($_FILES['dogovor_file']['tmp_name'], $uploadfile)) {
                    $data['dog_file'] = $newFileName;
                }

                if($object) {
                    $dog_file_path = $uploaddir . $object->dog_file;
                    if($object->dog_file != '' && file_exists($dog_file_path)) {
                        try {
                            unlink($dog_file_path);
                        } catch(Exception $e) {}
                    }
                }
            }
        }

        $this->store($data);
    }

    public function save_building_plan()
    {
        require_once( JPATH_COMPONENT . '/helpers/sttnmls_image.php' );
        require_once( JPATH_COMPONENT . '/helpers/sttnmlshelper.php' );

        $_allowed_save = FALSE;

        // JOOMLA Instance
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $image_instance = SttnmlsHelperImage::getInstance();
        $image_config = array(
            'prefix_name'               =>  'bplans',
            'use_prefix_as_imagename'   =>  FALSE,
            'store_path'                =>  'images/objects/',
            'width'                     =>  640,
            'height'                    =>  480,
            'resize'                    =>  TRUE,
            'types'                     =>  array(2, 3),
            'create_thumb'              =>  FALSE
        );
        $image_instance->setConfig($image_config);

        // Get input vars
        $plan_id = $app->input->post->get('bplan_id', 0, 'uint');
        $cn = $app->input->get('cn', 0, 'uint');
        $cid = $app->input->get('cid', 0, 'uint');
        $rooms = $app->input->post->get('rooms', 0, 'uint');
        $sall = $app->input->post->get('sall', 0, 'uint');
        $szhil = $app->input->post->get('szhil', 0, 'uint');
        $skitch = $app->input->post->get('skitch', 0, 'uint');
        $san = $app->input->post->get('san', 0, 'uint');
        $bal = $app->input->post->get('bal', 0, 'uint');
        $price = $app->input->post->get('price', 0, 'uint');

        if(!$cn OR !$cid OR !$sall OR !$szhil OR !$skitch OR !$price) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        // 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        // 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        // 3. Проверка возможности редактирования объекта пользователем(агентом)
        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cn));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - ошибка
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        $uploaded_image = $image_instance->upload('photo');
        $photo = (($uploaded_image && isset($uploaded_image[0]->output_filename)) ? $uploaded_image[0]->output_filename : '');

        if($plan_id && $photo != '') {
            $building_plan = $this->getBuildingPlan($plan_id);
            if($building_plan) {
                $image_full_file_path = JPATH_ROOT . '/' . $image_config['store_path'] . $building_plan->photos;
                if(file_exists($image_full_file_path)) {
                    try {
                        unlink($image_full_file_path);
                    } catch(Exception $e) {}
                }
            }
        }

        $query = $db->getQuery(TRUE);
        if($plan_id) {
            $query->update($db->quoteName('#__sttnmlsbplan'));
            $query->where($db->quoteName('id') . '=' . $db->quote($plan_id));
        } else {
            $query->insert($db->quoteName('#__sttnmlsbplan'));
        }
        $query->set($db->quoteName('bid') . '=' . $db->quote($cn));
        $query->set($db->quoteName('cid') . '=' . $db->quote($cid));
        $query->set($db->quoteName('sall') . '=' . $db->quote($sall));
        $query->set($db->quoteName('szhil') . '=' . $db->quote($szhil));
        $query->set($db->quoteName('skitch') . '=' . $db->quote($skitch));
        $query->set($db->quoteName('san') . '=' . $db->quote($san));
        $query->set($db->quoteName('bal') . '=' . $db->quote($bal));
        $query->set($db->quoteName('price') . '=' . $db->quote($price));
        $query->set($db->quoteName('rooms') . '=' . $db->quote($rooms));
        if($photo != '') {
            $query->set($db->quoteName('photos') . '=' . $db->quote($photo));
        }
        $db->setQuery($query);
        $db->execute();

        return TRUE;
    }


    public function removeBuildingPlan()
    {
        require_once( JPATH_COMPONENT . '/helpers/sttnmls_image.php' );
        require_once( JPATH_COMPONENT . '/helpers/sttnmlshelper.php' );

        // JOOMLA Instance
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        // Get input vars
        $plan_id = $app->input->post->get('bplan_id', 0, 'uint');
        $cn = $app->input->get('cn', 0, 'uint');
        $cid = $app->input->get('cid', 0, 'uint');


        if(!$plan_id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        // 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        // 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        // 3. Проверка возможности редактирования объекта пользователем(агентом)
        if($plan_id) {
            // Получение информации об объекте
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsbuildings'));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cn));
            $db->setQuery($query);
            $object = $db->loadObject();

            // Если объект не существует - ошибка
            if(!$object) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
                return FALSE;
            }

            // Проверка администратор ли пользователь (возможность редактирования в системе)?
            $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

            // Если не админ системы
            if(!$_allowed_save) {

                // Если агент пытается редактировать "чужой" объект - ошибка
                // 2 случая:
                //       1) агент из другой организации по отношению к объекту;
                //       2) агент из правильной организации, но не является хозяином объекта либо боссом
                if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                    $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                    return FALSE;
                }

                // Если все ОК, разрешаем сохранить данные
                $_allowed_save = TRUE;
            }
        }

        // Если не получили право сохранения до сих пор - ошибка
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        $query = $db->getQuery(TRUE);
        $query->delete($db->quoteName('#__sttnmlsbplan'));
        $query->where($db->quoteName('id') . '=' . $db->quote($plan_id));
        $db->setQuery($query);

        return $db->execute();
    }


    public function removeBuildingProgressImage()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $image_id = $app->input->get('idx', 0, 'uint');
        $image_date = $app->input->get('image_date', '', 'string');
        $viewName = $app->input->get('view', 'apart', 'string');


        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Получение массива картинок объекта
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName(array('progress_photos')));
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $building_progress_images = unserialize($db->loadResult());

        if(!$building_progress_images) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_OBJECT_IMAGES');
            return FALSE;
        }

        if(isset($building_progress_images[$image_date]) && isset($building_progress_images[$image_date][$image_id])) {
            // Попытка удалить файл картинки с сервера -------------------------
            $image_filename = $building_progress_images[$image_date][$image_id]->src;
            try {
                $image_path = JPATH_ROOT . '/images/objects/' . $image_filename;
                if(file_exists($image_path)) {
                    unlink($image_path);
                }
            } catch (Exception $e) {}

            // Удаление картинки из списка для объекта -------------------------
            unset($building_progress_images[$image_date][$image_id]);
            if(!$building_progress_images[$image_date]) {
                unset($building_progress_images[$image_date]);
            }


            // Обновление информации о картинках в БД объекта
            $query = $db->getQuery(TRUE);
            $query->update($db->quoteName('#__sttnmlsbuildings'));
            $query->set($db->quoteName('progress_photos') . '=' . $db->quote(serialize($building_progress_images)));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
            $db->setQuery($query);
            $db->execute();

            return TRUE;
        }

        return FALSE;
    }

    public function removeBuildingProjectFile()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Получение имени файла для объекта
        if(trim($object->proj_file) != '') {

            // Удаление файла с сервера
            $project_file_full_path = JPATH_ROOT . '/files/buildings/' . $object->proj_file;
            if(file_exists($project_file_full_path)) {
                try {
                    unlink($project_file_full_path);
                } catch(Exception $e) {}
            }

            // Удаление информации о файле из объекта
            $query->update($db->quoteName('#__sttnmlsbuildings'));
            $query->set($db->quoteName('proj_file') . '=""');
            $query->set($db->quoteName('project') . '=""');
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
            $db->setQuery($query);

            return $db->execute();
        }
        return FALSE;
    }

    public function removeBuildingDogovorFile()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Получение имени файла для объекта
        if(trim($object->dog_file) != '') {

            // Удаление файла с сервера
            $dogovor_file_full_path = JPATH_ROOT . '/files/buildings/' . $object->dog_file;
            if(file_exists($dogovor_file_full_path)) {
                try {
                    unlink($dogovor_file_full_path);
                } catch(Exception $e) {}
            }

            // Удаление информации о файле из объекта
            $query->update($db->quoteName('#__sttnmlsbuildings'));
            $query->set($db->quoteName('dog_file') . '=""');
            $query->set($db->quoteName('dogovor') . '=""');
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
            $db->setQuery($query);

            return $db->execute();
        }
        return FALSE;
    }

    public function rotateBuildingProgressImage()
    {
        // Хелпер аплоадера картинок
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmls_image.php');

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $idx = $app->input->get('idx', -1, 'int');
        $image_date = $app->input->get('image_date', '', 'string');
        $direction = $app->input->get('direction', 'left', 'string');
        $viewName = $app->input->get('view', 'building', 'string');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum OR $idx == -1) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Получение массива картинок объекта
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName(array('progress_photos')));
        $query->from($db->quoteName('#__sttnmlsbuildings'));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $building_progress_images = unserialize($db->loadResult());

        if(!$building_progress_images) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_OBJECT_IMAGES');
            return FALSE;
        }

        if(isset($building_progress_images[$image_date]) && isset($building_progress_images[$image_date][$idx])) {
            $src_image = JPATH_ROOT . '/images/objects/' . $building_progress_images[$image_date][$idx]->src;

            // Поворот изображения и сохранение ------------------------------------
            $dst_image = SttnmlsHelperImage::getInstance();
            if(!$dst_image->rotateImage($src_image, (($direction == 'right') ? -90 : 90))) {
                $this->_errors = $dst_image->getErrors();
                return FALSE;
            }

            return TRUE;
        }

        return FALSE;
    }
	
	function getOptWmaterial()
	{
		$params = JComponentHelper::getParams('com_sttnmls');
		$arr = $params->get('wmaterial');
		$where = ' ID in (' . implode(',', $arr) . ')';

		$db = $this->getDbo();
		$query = $db->getQuery(true);
        $query->select('NAME as text, ID as value');
		$query->from(' `#__sttnmlsvocalldata` ');
		$query->where(' RAZDELID=8 ');
		if($arr && count($arr)>0)
			$query->where($where);
		$query->order('text');
		$db->setQuery($query);
		$res=$db->loadAssocList();
		$options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
		if(is_array($res)) {
			foreach($res as $row)
			{
				$options[] = JHTML::_('select.option', $row['value'], $row['text']);
			} 
		}
		return JHTML::_('select.genericlist',  $options, 'jform[wmaterid]', 'size="1" class="validate-sttnum"', 'value', 'text', $this->_item->WMATERID);
	}
	function getOptHtype()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if($this->_item->CITYID){
	        $query->select('NAME as text, ID as value');
			$query->where(' CITYID=' . $db->quote($this->_item->CITYID));
		}
		else {
	        $query->select('NAME as text, min(ID) as value');
			$query->group('NAME');
		}
		$query->from(' `#__sttnmlsvocalldata` ');
		$query->where(' RAZDELID=4 ');
		$query->order('text');
		$db->setQuery($query);
		$res=$db->loadAssocList();
		if(!$res)
		{
			$query = $db->getQuery(true);
			$query->select('NAME as text, min(ID) as value');
			$query->group('NAME');
			$query->from(' `#__sttnmlsvocalldata` ');
			$query->where(' RAZDELID=4 ');
			$query->order('text');
			$db->setQuery($query);
			$res=$db->loadAssocList();
		}
		$options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
		if(is_array($res)) {
			foreach($res as $row)
			{
				$options[] = JHTML::_('select.option', $row['value'], $row['text']);
			} 
		}
		return JHTML::_('select.genericlist',  $options, 'jform[htypeid]', 'size="1" class="validate-sttnum"', 'value', 'text', $this->_item->HTYPEID);
	}
	function getCheckAr()
	{
		$res = new stdClass;
		$res->checks1 = array(JText::_('COM_STTNMLS_CHEKS11'),JText::_('COM_STTNMLS_CHEKS12'),JText::_('COM_STTNMLS_CHEKS13'),JText::_('COM_STTNMLS_CHEKS14'),JText::_('COM_STTNMLS_CHEKS15'));
		$res->checks2 = array(JText::_('COM_STTNMLS_CHEKS21'),JText::_('COM_STTNMLS_CHEKS22'),JText::_('COM_STTNMLS_CHEKS23'),JText::_('COM_STTNMLS_CHEKS24'),JText::_('COM_STTNMLS_CHEKS25'));
		return $res;
	}
	
	
	function del_plan()
	{
		$db = $this->getDbo();
		$db->setQuery('DELETE FROM #__sttnmlsbplan WHERE id='.$db->quote($_REQUEST['id']));
		$db->query();
		$app = JFactory::getApplication();
		$app->redirect("/component/sttnmls/?view=building&task=edit&cn=".$_REQUEST['cn']."&cid=".$_REQUEST['cid']."&pg=plan");
	}
	
	
	
	
	function edit_plan()
	{
		$file_update=0;
		if($_REQUEST['del_photo']==1)
		{
			$photo='';
			$file_update=1;
		}
		
		
		
		if($_FILES['photo']['size']>0 && $_FILES['photo']['error']==0)
		{
			$whitelist = array(".jpg", ".jpeg", ".png", ".JPG", ".JPEG", ".gif", ".bmp");
			$file_up=0;
			$extension='';
			foreach ($whitelist as $item)
			{
			 	if(preg_match("/$item\$/i", $_FILES['photo']['name'], $ext))
			 	{
			 		$file_up=1;
					$extension=$ext[0];
			 	}
			}
			if($file_up==1)
			{
				$uploaddir=$_SERVER['DOCUMENT_ROOT'].'/buildings_files/';
				$newFileName=date('YmdHis').rand(10,100).$extension;
				$uploadfile = $uploaddir.$newFileName;
				if(move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile))
				{
					$photo=$newFileName;	
				}
			}
			$file_update=1;
		}
		
		$_REQUEST['sall']=str_replace(",",".", $_REQUEST['sall']);
		$_REQUEST['szhil']=str_replace(",",".", $_REQUEST['szhil']);
		$_REQUEST['skitch']=str_replace(",",".", $_REQUEST['skitch']);
		$_REQUEST['price']=str_replace(",",".", $_REQUEST['price']);
		
		
		$_REQUEST['sall']=(float)$_REQUEST['sall'];
		$_REQUEST['szhil']=(float)$_REQUEST['szhil'];
		$_REQUEST['skitch']=(float)$_REQUEST['skitch'];
		$_REQUEST['price']=(float)$_REQUEST['price'];
		$db = $this->getDbo();
		
		
		if($file_update==1)
		{
			$db->setQuery('UPDATE `#__sttnmlsbplan` set sall="'.$_REQUEST['sall'].'", szhil="'.$_REQUEST['szhil'].'", skitch="'.$_REQUEST['skitch'].'", san="'.$_REQUEST['san'].'", bal="'.$_REQUEST['bal'].'", price="'.$_REQUEST['price'].'", photos="'.$photo.'", type="'.$_REQUEST['type'].'" WHERE id="'.$_REQUEST['id'].'"');
		}else{
			$db->setQuery('UPDATE `#__sttnmlsbplan` set sall="'.$_REQUEST['sall'].'", szhil="'.$_REQUEST['szhil'].'", skitch="'.$_REQUEST['skitch'].'", san="'.$_REQUEST['san'].'", bal="'.$_REQUEST['bal'].'", price="'.$_REQUEST['price'].'", type="'.$_REQUEST['type'].'" WHERE id="'.$_REQUEST['id'].'"');
		}
		
		$db->query();
		
		$app = JFactory::getApplication();
		$app->redirect("/component/sttnmls/?view=building&task=edit&cn=".$_REQUEST['jform']['cardnum']."&cid=".$_REQUEST['jform']['compid']."&pg=plan");
	}
	
	
	
	
	function add_plan()
	{
		$photo='';
		if($_FILES['photo']['size']>0 && $_FILES['photo']['error']==0)
		{
			$whitelist = array(".jpg", ".jpeg", ".png", ".JPG", ".JPEG", ".gif", ".bmp");
			$file_up=0;
			$extension='';
			foreach ($whitelist as $item)
			{
			 	if(preg_match("/$item\$/i", $_FILES['photo']['name'], $ext))
			 	{
			 		$file_up=1;
					$extension=$ext[0];
			 	}
			}
			if($file_up==1)
			{
				$uploaddir=$_SERVER['DOCUMENT_ROOT'].'/buildings_files/';
				$newFileName=date('YmdHis').rand(10,100).$extension;
				$uploadfile = $uploaddir.$newFileName;
				if(move_uploaded_file($_FILES['photo']['tmp_name'], $uploadfile))
				{
					$photo=$newFileName;	
				}
			}
		}
		$_REQUEST['sall']=str_replace(",",".", $_REQUEST['sall']);
		$_REQUEST['szhil']=str_replace(",",".", $_REQUEST['szhil']);
		$_REQUEST['skitch']=str_replace(",",".", $_REQUEST['skitch']);
		$_REQUEST['price']=str_replace(",",".", $_REQUEST['price']);
		
		$_REQUEST['sall']=(float)$_REQUEST['sall'];
		$_REQUEST['szhil']=(float)$_REQUEST['szhil'];
		$_REQUEST['skitch']=(float)$_REQUEST['skitch'];
		$_REQUEST['price']=(float)$_REQUEST['price'];
		$db = $this->getDbo();
		$db->setQuery('INSERT INTO `#__sttnmlsbplan` (bid, sall, szhil, skitch, san, bal, price, photos, type, cid) VALUES("'.$_REQUEST['jform']['cardnum'].'", "'.$_REQUEST['sall'].'", "'.$_REQUEST['szhil'].'", "'.$_REQUEST['skitch'].'", "'.$_REQUEST['san'].'", "'.$_REQUEST['bal'].'", "'.$_REQUEST['price'].'", "'.$photo.'", "'.$_REQUEST['type'].'", "'.$_REQUEST['jform']['compid'].'")');
		$db->query();
		
		$app = JFactory::getApplication();
		$app->redirect("/component/sttnmls/?view=building&task=edit&cn=".$_REQUEST['jform']['cardnum']."&cid=".$_REQUEST['jform']['compid']."&pg=plan");
	}
	
	
	
	
	

}
