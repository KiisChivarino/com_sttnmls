<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
$app = JFactory::getApplication();

?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<div class="sttnmls">
	<h1><?php echo JText::_('COM_STTNMLS_ROBOKASSA_HEAD'); ?></h1>
	
<?
    // регистрационная информация (логин, пароль #1)
    // registration info (login, password #1)
    $mrh_login = $this->robo->login;
    $mrh_pass1 = $this->robo->pass1;

    // номер заказа
    // number of order
    $inv_id = $this->invid;;

    // описание заказа
    // order description
    $inv_desc = JText::sprintf('COM_STTNMLS_ROBOKASSA_DESCR',$app->getCfg('sitename'));

    // сумма заказа
    // sum of order
    $def_sum = "100";

    // тип товара
    // code of goods
    //$shp_item = 2;

    // язык
    // language
    $culture = "ru";

    // кодировка
    // encoding
    $encoding = "utf-8";

    // формирование подписи
    // generate signature
    $crc  = md5("$mrh_login::$inv_id:$mrh_pass1");

    // HTML-страница с кассой
    // ROBOKASSA HTML-page
    print "<html><script language=JavaScript ".
          "src='https://auth.robokassa.ru/Merchant/PaymentForm/FormFLS.js?".
          "MerchantLogin=$mrh_login&DefaultSum=$def_sum&InvoiceID=$inv_id".
          "&Description=$inv_desc&SignatureValue=$crc".
          "&Culture=$culture&Encoding=$encoding'></script></html>";

?>
    
  <?php  //<div>
		/* 
		$i=0;
		foreach($this->robo->sums as $value) { 
			$desc = JText::sprintf('COM_STTNMLS_ROBOKASSA_DESCR',$app->getCfg('sitename'));
			$sign = md5($this->robo->login.":". $value .":".$this->invid.":".$this->robo->pass1);
			$i = ($i+1)%2;
			?>
			<div class="payform<?php echo $i; ?>">
				<form action="<?php echo $this->robo->server; ?>" method="post" class="robo_form">
					<input type="hidden" name="FinalStep" value="1" />
					<input type="hidden" name="MrchLogin" value="<?php echo $this->robo->login; ?>" />
					<input type="hidden" name="OutSum" value="<?php echo $value; ?>" />
					<input type="hidden" name="InvId" value="<?php echo $this->invid; ?>" />
					<input type="hidden" name="Desc" value="<?php echo $desc; ?>" />
					<input type="hidden" name="SignatureValue" value="<?php echo $sign; ?>" />
					<input type="submit" class="button sbgreen" value="<?php echo $value.' руб.'; ?>" />
				</form>
			</div>
		<?php } */
	//</div>?>
</div>
