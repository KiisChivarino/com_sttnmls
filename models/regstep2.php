<?php
defined('_JEXEC') or die('Restricted access');

class SttNmlsModelRegstep2 extends JModelLegacy {

	private $_user;
	
	public function getUser()
	{
		$lang		= JFactory::getLanguage();
		$lang->load('com_users', JPATH_BASE);
		$app = JFactory::getApplication();
		$token = JRequest::getVar('token', null, 'request', 'alnum');
		if ($token === null || strlen($token) !== 32) {
			$app->redirect('index.php', JText::_('JINVALID_TOKEN'));
			return true;
		}
		$db	= $this->getDbo();
		$db->setQuery(
			'SELECT '.$db->quoteName('id').' FROM '.$db->quoteName('#__users') .
			' WHERE '.$db->quoteName('activation').' = '.$db->Quote($token) .
			' AND '.$db->quoteName('block').' = 1' .
			' AND '.$db->quoteName('lastvisitDate').' = '.$db->Quote($db->getNullDate())
		);
		$userId = (int) $db->loadResult();
		if (!$userId) {
			$app->redirect('index.php',JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
			return false;
		}
		$user = JFactory::getUser($userId);

		if ($user === false) {
			$app->redirect('index.php', JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError()));
			return true;
		}
		$this->_user=$user;
		return $user;
	}	
	public function getProfile()
	{
		$db=$this->getDbo();
		$userid=$this->_user->get('id');
		$db->setQuery(
			'SELECT profile_value FROM #__user_profiles' .
			' WHERE user_id = '.$db->quote($userid)." AND profile_key = 'profile.realtor'" .
			' ORDER BY ordering'
		);
		$result = $db->loadResult();
		if(!$result) return 2;
		else return json_decode($result, true);
	}
	public function getFirms($cat=0)
	{
		require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
		return SttNmlsHelper::getOptFirms(0,'',0,$cat);
	}
	public function getCatf()
	{
		require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
		return SttNmlsHelper::getCatfOptions(JText::_('COM_STTNMLS_SELECT0'));
	}
}
