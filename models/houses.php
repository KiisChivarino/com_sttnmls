<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/helpers/class.sttnmlslist.php');

class SttnmlsModelHouses extends SttnmlsModelList
{
	// Индекс объекта в избранном (1-5)
	protected $favoriteObjectType = 3;
	// Название таблицы объектов в зависимости от отображаемой модели
	protected $objectsTableName = '#__sttnmlshouses';

	protected function getDefaultFiltersValue()
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$filters = array(
			// ОПИСАНИЕ ФИЛЬТРОВ:
			//    [имя_поля_фильтра]    =>  [массив значений]
			//
			//    (!) - обязательное значение фильтра
			//    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
			//
			// ЗНАЧЕНИЯ ФИЛЬТРОВ:
			//      "type"      -   (!) тип значения фильтра
			//      "value"     -   (!) значение фильтра по умолчанию
			//
			//      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
			//
			//      [!] >>>
			//      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
			//                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
			//
			//      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
			//
			//      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
			//                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
			//
			//      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
			//                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
			//                              либо завершенную строку WHERE запроса
			//      <<< [!]
			//

			'mw'            =>  array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'mw', 'valueFromTable' => '#__sttnmlsvocalldata', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s) AND ' . $db->quoteName('RAZDELID') . '=' . $db->quote(8)),
			'newbuild'      =>  array('type' => 'bool', 'value' => FALSE),
			'earea_ot'      =>  array('type' => 'uint', 'value' => 0),
			'earea_do'      =>  array('type' => 'uint', 'value' => 0),
		);
		return array_merge(parent::getDefaultFiltersValue(), $filters);
	}

	protected function setPersonalConditions()
	{
		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		// Request VARS ------------------------------------------------------------------------------------------------
		$act = $app->input->getString('act', '');

		// State VARS --------------------------------------------------------------------------------------------------
		$_newbuild = $this->getState('filter.newbuild', FALSE);

		// Раздел SELECT запроса ---------------------------------------------------------------------------------------
		$this->query->select($db->quoteName('c.NAME', 'gorod') . ', ' . $db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d10.NAME', 'tip') . ', ' . $db->quoteName('d8.NAME', 'mat'));
		$this->query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
		$this->query->select('IF(' . $db->quoteName('o.WHAT') . ' = 0, "' . JText::_('COM_STTNMLS_HAUSE') . '", "' . JText::_('COM_STTNMLS_UCH') . '") AS ' . $db->quoteName('object'));


		// Раздел JOIN запроса -----------------------------------------------------------------------------------------
		$this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd10') . ' ON (' . $db->quoteName('o.BTYPEID') . '=' . $db->quoteName('d10.id') . ' AND ' . $db->quoteName('d10.RAZDELID') . '=' . $db->quote(10) . ')');
		$this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON (' . $db->quoteName('o.WMATERID') . '=' . $db->quoteName('d8.id') . ' AND ' . $db->quoteName('d8.RAZDELID') . '=' . $db->quote(8) . ')');


		// Раздел WHERE запроса ----------------------------------------------------------------------------------------
		$this->query->where($db->quoteName('o.WHAT') . (($this->what) ? (($this->what == 20) ? '>=' : '>') : '=') . $db->quote(0));

		//> Передан флаг "Новостройка"
		if($_newbuild) {
			//> Флаг "новостройка" для поиска
			$this->query->where($db->quoteName('o.NEWBUILD') . '=' . $db->quote(1));
		}


		// Учет запрошенного материала стен ----------------------------------------------------------------------------
		$this->setAdditionalWhereFromStateCheckbox($this->query, 'mw', 'o.WMATERID');
		// END Учет запрошенного материала стен ------------------------------------------------------------------------

		// Учет площади участка ----------------------------------------------------------------------------------------
		$this->setAdditionalWhereFromState($this->query, 'earea_ot', 'o.EAREA', '>=');
		$this->setAdditionalWhereFromState($this->query, 'earea_do', 'o.EAREA', '<=');
		// END Учет площади участка ------------------------------------------------------------------------------------

		if($act == 'pdf') {
			//$query = ,"ORDER BY a.WHAT, a.CITYID, a.RAIONID, a.PRICE", $query)));
			$this->query->order($db->quoteName('o.WHAT') . ' ASC, ' . $db->quoteName('o.CITYID') . ' ASC, ' . $db->quoteName('o.RAIONID') . ' ASC, ' . $db->quoteName('o.PRICE'));
		}
	}
}
