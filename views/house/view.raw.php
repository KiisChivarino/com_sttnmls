<?php defined('_JEXEC') or die ('Restricted access');

JLoader::import('joomla.application.component.model');

class SttNmlsViewHouse extends JViewLegacy
{
    function display($tpl = null) 
    {

        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $mHouse = $this->getModel('house');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');
        
        $cardnum = JRequest::getVar('cn', 0);
        $compid = JRequest::getVar('cid', 0);
        $layout = JRequest::getVar('layout', 'edit_photo_list');
        
        // AJAX data switcher
        switch ($layout) {

            case 'default_object_check_state_button':
                $this->item = $mHouse->getItem();

                // Доступ к изменению состояния проверки объекта
                if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
                    $this->_allow_change_state = TRUE;
                } else {
                    $this->_allow_change_state = FALSE;
                }
                break;

            case 'edit_photo_list':
                    if($cardnum == 0 OR $compid == 0) {
                        JError::raiseError('400', JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'));
                        return FALSE;
                    }  
                    
                    $this->item = $mHouse->getObjectImages('house', $compid, $cardnum, TRUE);
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                break;
        }
        
        parent::display($tpl);
    }
}