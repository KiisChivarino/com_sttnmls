<?php defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewComs extends JViewLegacy
{
    function mb_ucfirst($str) {
        return mb_substr(mb_strtoupper($str), 0, 1) . mb_strtolower(mb_substr($str, 1, mb_strlen($str)));
    }

    function display($tpl = null) 
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $doc = JFactory::getDocument();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $mAparts = JModelLegacy::getInstance('aparts', 'SttnmlsModel');
        $this->coms_model = JModelLegacy::getInstance('coms', 'SttnmlsModel');
        $this->params = JComponentHelper::getParams('com_sttnmls');
		$this->user = JFactory::getUser();

        // Данные запроса
        $act = $app->input->get('act', '', 'string');
        $itemid = $app->input->get('Itemid', 0, 'uint');
        $this->raw = (($app->input->getString('format', 'html') == 'raw') ? TRUE : FALSE);

        $this->profile = $mAgent->getProfile(JFactory::getUser()->id);
        $this->access = $this->get('AccessRules');
        $this->items = $this->get('Items');

        if($act == 'pdf')
        {
            $this->agent = $mAgent->getAgentInfoByID();
            $this->setLayout('default');
            echo $this->loadTemplate('print');
            exit;
        }


        $this->countobj = $this->get('Total');
        $this->filtersList = $this->get('FiltersList');
        $this->filtersGETVarsList = $this->get('FiltersGETVarsList');
        $this->noitemtext = '';
        if(!$this->items) {
                $this->noitemtext =  $this->get('NText');
        } 
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->stype = JRequest::getInt('stype',0);
        $this->raions = $this->get('RaionsList');
        $this->mraions = $this->get('MRaionsList');
        $this->cityopt = $this->get('OptCity');
        $this->tp = $this->get('Tp');
        $this->what = JRequest::getInt('what', 0);
        
        
            
        $db->setQuery('SELECT * FROM #__menu WHERE `id`=' . $db->quote($itemid));
        $seo = $db->loadObject();
        $seotxt = json_decode($seo->params);
        $meta_dsc = '';
        $meta_key = '';
        $meta_title = '';
        
        foreach($seotxt as $k=>$v)
        {
            if($k == 'menu-meta_description') {
                $meta_dsc = $v;
            }
            if($k == 'menu-meta_keywords') {
                $meta_key = $v;
            }
            if($k == 'page_title') {
                $meta_title = $v;
            }
        }


        if($meta_dsc)
        {
            $doc->setDescription($meta_dsc);
        }else{
            if(!$this->stype) {
                $doc->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_CM'));
            } else {
                $doc->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_CM1'));
            }
        }

        // Если поиск по ID улицы из GET запроса
        $search_by_street = (($app->input->get('street_id', 0)) ? ' - ' . $mAparts->getStreetNameByID($app->input->get('street_id', 0)) : '');

        if($meta_title != '')
        {
            $doc->setTitle($meta_title . $search_by_street);
        }else{
            if(!$this->stype) {
                $doc->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_CM') . $search_by_street);
            } else {
                $doc->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_CM1') . $search_by_street);
            }
        }
        
        if($meta_key != '') {
            $doc->setMetadata('keywords', $meta_key);
        }

        parent::display($tpl);
    }
}
