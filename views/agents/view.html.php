<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewAgents extends JViewLegacy
{
	function display($tpl = null)
	{
		// JOOMLA Instances
		$app = JFactory::getApplication();
		$mAgents = JModelLegacy::getInstance('agents', 'SttnmlsModel');

		$this->items = $this->get('Items');
		$this->firm_categories = $mAgents->getCategoriesFirmsList();
		$this->firms = $mAgents->getFirmsList($app->input->get('filter_catid', 1, 'uint'));
		$pagination = $this->get('Pagination');
		$this->pagination = $pagination;
		$this->state = $this->get('State');
		$this->params = $this->state->get('parameters.menu');
		$this->_prepareDocument();

		parent::display($tpl);
	}


	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', JText::_('JGLOBAL_ARTICLES'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		} elseif ($app->get('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		} elseif ($app->get('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
