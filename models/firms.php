<?php

defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

class SttNmlsModelFirms extends JModelList{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
			'ID', 'a.ID',
			'kitog', 'r.kitog',
			'realname', 'a.realname',
			'NAME', 'a.NAME',
			'PHONE', 'a.PHONE',
			'ADDPHONE', 'a.ADDPHONE',
			'addr', 'a.addr',
			'EMAIL', 'a.EMAIL',
			'ISITE', 'a.ISITE',
			'gild','a.gild',
			'flag','a.flag',
			'active','a.active',
			'cnt','cnt'
			);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$cat = JRequest::getInt('cat', 0);
		$db = $this->getDbo();
		$db->setQuery('update `#__sttnmlsvocfirms` set realname=NAME where trim(realname)=""');
		$db->query();
		$query = $db->getQuery(true);
		$query->select($this->getState('list.select', 'f.*, count(b.kod) as cnt '));
		$query->from(' `#__sttnmlsvocfirms` AS f ');
		$query->join('LEFT','`#__sttnmlsvocagents` as b on b.COMPID=f.ID and (b.flag=0 or b.flag=2) ');
		$query->join('LEFT','`#__sttnmlsratingf` as r on r.idfirm=f.ID');
		$query->where(' f.flag<3');
		if($cat)
			$query->where(' f.ID in (select idfirm from #__sttnmlsfirmcatf where idcatf='.$db->quote($cat).')');
		$query->where(' f.active=1 ');
		$query->where(' f.ID!=99013 ');
		$this->addWhere('search', 'f.realname LIKE ', $query, true);
		$query->group('f.ID');
		$orderCol = $this->state->get('list.ordering', 'kitog');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if($orderCol=='name') $orderCol='kitog';
		
		if($orderCol!='realname')
		{
			$query->order($db->escape($orderCol.' '.$orderDirn.', f.realname ASC'));
		}else{
			$query->order($db->escape('f.realname '.$orderDirn));
		}
		
		return $query;
	}

	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		$this->setState('list.limit', $limit);

		$this->setFilterState('search','');
		parent::populateState('name', 'asc');

		$limitstart = JRequest::getUInt('limitstart', 0);
		$this->setState('list.start', $limitstart); 
		
	}

	public function setFilterState($par,$default=null)
	{
		$a=$this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
		$this->setState('filter.' .$par, $a);
	}
	public function addWhere($par,$fld,$query,$like=false)
	{
		if($par)
		$search = $this->getState('filter.'.$par);
		if($search)
		{
			if($like) $search = "'%$search%'";
			$query->where($fld . $search );
		}
	}

	public function getPagination() {
		$p = parent::getPagination();
		$cat = JRequest::getInt('cat', 0);
		if($cat) {
			$p->setAdditionalUrlParam('cat', $cat);
		}
		return $p;
	}
	public function getCategories($idfirm)
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		return SttNmlsHelper::getCategories($idfirm);
	}
	public function getBosses($idfirm)
	{
		$ret='';
		if($idfirm){
			$db = $this->getDbo();
			$query = 'select concat(a.SIRNAME," ",a.NAME," ",a.SECNAME) as boss, ID from #__sttnmlsvocagents a where a.boss=1 and a.COMPID='.$db->quote($idfirm).' order by ID';
			$db->setQuery($query);
			$names = $db->loadObjectList();
			if(count($names)) {
				$glue = '';
				foreach ($names as $cat) {
					$linkboss = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent' . '&agent_id=' . $cat->ID . '&comp_id=' . $idfirm);
					$ret .= $glue . '<a href="'.$linkboss.'" target="_blank">'.$cat->boss.'</a>';
					$glue = ', ';
				}
			}
		}
		return $ret;
	}
	public function getTitle()
	{
		$cat = JRequest::getInt('cat', 0);
		$db = $this->getDbo();
		// количество всех активных фирм в категории
		$query = $db->getQuery(true);
		$query->select('count(f.ID) as cnt ');
		$query->from(' `#__sttnmlsvocfirms` AS f ');
		$query->where(' f.active=1 ');
		if($cat) {
			$query->where(' f.ID in (select idfirm from #__sttnmlsfirmcatf where idcatf='.$db->quote($cat).')' );
		}
		$query->where(' f.ID!=99013 ');
		$db->setQuery($query);
		$cnt = $db->loadResult();
		
		
		
		
		
		
		// название категории
		$title = new stdClass();
		if($cat){
			$query = $db->getQuery(true);
			$query->select('name, descr ');
			$query->from(' `#__sttnmlscatf` ');
			$query->where(' id='.$db->quote($cat));
			$db->setQuery($query);
			$r = $db->loadObject();
			$name = $r->name;
			$title->descr = $r->descr;
		}
		else {
			$name = JText::_('COM_STTNMLS_FIRMS_ALLCAT');
		}
		$title->head = $name.' ('.$cnt.')';
		return $title;
	}
	
}

