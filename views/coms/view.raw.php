<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewComs extends JViewLegacy
{
	function display($tpl = null) 
	{
		$this->access = $this->get('AccessRules');
        $items = $this->get('Items');
		$this->countobj = $this->get('Total');
        $this->items = $items;
		$pagination = $this->get('Pagination');
		$state = $this->get('State');
		$this->pagination = $pagination;
		$this->state = $state;
		$this->stype = JRequest::getInt('stype',0);
		$this->raions = $this->get('RaionsList');
		$this->mraions = $this->get('MRaionsList');
		$this->cityopt = $this->get('OptCity');
		$this->tp = $this->get('Tp');
		$this->params = JComponentHelper::getParams('com_sttnmls');
		$this->raw = 1;

		parent::display($tpl);
	}
}
