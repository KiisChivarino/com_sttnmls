jQuery(document).ready(function() {
	var button1 = jQuery('#uploadButton1');
	var button2 = jQuery('#uploadButton2');

	jQuery.ajax_upload(button1, {
		action : 'index.php?option=com_sttnmls&task=uploadfirmlogo&view=firm&format=raw&main=1&agency='+idfirm,
		name : 'myfile',
		onSubmit : function(file, ext) {
			// показываем картинку загрузки файла
			jQuery("#load1").show();
			jQuery(".upload1").hide();
			this.disable();
		},
		onComplete : function(file, response) {
			// убираем картинку загрузки файла
			jQuery("#load1").hide();
			jQuery(".upload1").show();
			this.enable();

			// показываем что файл загружен
			var res=response.split('|')
			jQuery(".editflogoimg1").html(res[0]);
			jQuery(".editflogoimg2").html(res[1]);

		}
	});
	jQuery.ajax_upload(button2, {
		action : 'index.php?option=com_sttnmls&task=uploadfirmlogo&view=firm&format=raw&main=0&agency='+idfirm,
		name : 'myfile',
		onSubmit : function(file, ext) {
			// показываем картинку загрузки файла
			jQuery("#load2").show();
			jQuery(".upload2").hide();
			this.disable();
		},
		onComplete : function(file, response) {
			// убираем картинку загрузки файла
			jQuery("#load2").hide();
			jQuery(".upload2").show();
			this.enable();

			// показываем что файл загружен
			var res=response.split('|')
			jQuery(".editflogoimg1").html(res[0]);
			jQuery(".editflogoimg2").html(res[1]);

		}
	});
});

