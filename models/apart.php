<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');
require_once JPATH_COMPONENT.'/helpers/sttnmlsitem.php';

class SttNmlsModelApart extends SttNmlsModelItem 
{
    public function __construct($config = array()) 
    {
        $this->setTabname('#__sttnmlsaparts');
        $this->setTp('1');
        parent::__construct($config);
    }
    
    function getOptWhat()
    {
        $html='<select class="form-control" name="jform[what]">';
        $s = ' ';
        if($this->_item->WHAT==0) {
            $s = ' selected="selected"';
        }
        $html .= '<option value="0" ' . $s . '>' . JText::_('COM_STTNMLS_APART') . '</option>';
        $s = ' ';
        if($this->_item->WHAT==1) {
            $s = ' selected="selected"';
        }
        $html .= '<option value="1" ' . $s . '>' . JText::_('COM_STTNMLS_ROOM') . '</option>';
        $html .= '</select>';
        return $html;
    }
        
    function getOptWmaterial()
    {
        $params = JComponentHelper::getParams('com_sttnmls');
        $arr = $params->get('wmaterial');
        $where = ' ID in (' . implode(',', $arr) . ')';

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('`NAME` AS text, `ID` AS value');
        $query->from('`#__sttnmlsvocalldata`');
        $query->where('`RAZDELID`="8"');
        if($arr && count($arr)>0) {
            $query->where($where);
        }         
        $query->order('text');
        $db->setQuery($query);
        $res = $db->loadAssocList();
        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
        if(is_array($res)) {
            foreach($res as $row)
            {
                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
            } 
        }
        return JHTML::_('select.genericlist',  $options, 'jform[wmaterid]', 'size="1" class="form-control validate-sttnum"', 'value', 'text', $this->_item->WMATERID);
    }
        
    function getOptHtype()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        if($this->_item->CITYID) {
            $query->select('`NAME` AS text, `ID` AS value');
            $query->where('`CITYID`=' . $db->quote($this->_item->CITYID));
        } else {
            $query->select('`NAME` AS text, min(`ID`) AS value');
            $query->group('`NAME`');
        }
        $query->from('`#__sttnmlsvocalldata`');
        $query->where('`RAZDELID`="4"');
        $query->order('`text`');
        $db->setQuery($query);
        $res = $db->loadAssocList();
        if(!$res) {
            $query = $db->getQuery(true);
            $query->select('`NAME` AS text, min(`ID`) AS value');
            $query->group('`NAME`');
            $query->from('`#__sttnmlsvocalldata`');
            $query->where('`RAZDELID`="4"');
            $query->order('`text`');
            $db->setQuery($query);
            $res = $db->loadAssocList();
        }
        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
        if(is_array($res)) {
            foreach($res as $row)
            {
                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
            } 
        }
        return JHTML::_('select.genericlist',  $options, 'jform[htypeid]', 'size="1" class="form-control validate-sttnum"', 'value', 'text', $this->_item->HTYPEID);
    }
    
    
    function getCheckAr()
    {
        $res = new stdClass;
        $res->checks1 = array(JText::_('COM_STTNMLS_CHEKS11'),JText::_('COM_STTNMLS_CHEKS12'),JText::_('COM_STTNMLS_CHEKS13'),JText::_('COM_STTNMLS_CHEKS14'),JText::_('COM_STTNMLS_CHEKS15'));
        $res->checks2 = array(JText::_('COM_STTNMLS_CHEKS21'),JText::_('COM_STTNMLS_CHEKS22'),JText::_('COM_STTNMLS_CHEKS23'),JText::_('COM_STTNMLS_CHEKS24'),JText::_('COM_STTNMLS_CHEKS25'));
        return $res;
    }
        
    function save()
    {
        // Joomla инстанции
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Получение из запроса сохраняемых данных объекта
        $data = $app->input->post->get('jform', array(), 'array');

        // Подготовка данных для сохранения
        $data['cityid'] = ((!isset($data['cityid']) ? -1 : $data['cityid']));
        $data['rooms'] = ((!isset($data['rooms']) OR !$data['rooms']) ? 1 : $data['rooms']);

        if(isset($data['htypeid']) && $data['htypeid']) {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName(array('ID', 'SHORTNAME', 'NAME', 'CITYID')));
            $query->from($db->quoteName('#__sttnmlsvocalldata'));
            $query->where($db->quoteName('RAZDELID') . '=' . $db->quote('4'));
            $query->where($db->quoteName('ID') . '=' . $db->quote($data['htypeid']));
            $db->setQuery($query);

            $result = $db->loadObject();

            if($result && $result->CITYID != $data['cityid']) {
                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('ID'));
                $query->from($db->quoteName('#__sttnmlsvocalldata'));
                $query->where($db->quoteName('SHORTNAME') . '=' . $db->quote($result->SHORTNAME));
                $query->where($db->quoteName('CITYID') . '=' . $db->quote($data['cityid']));
                $htypeid = $db->loadResult();

                $data['htypeid'] = (($htypeid) ? $htypeid : $data['htypeid']);
            }
        }

        $_new_build = $app->input->post->get('newbuild', 0);
        $_bal = $app->input->post->get('bal', 0);
		$_encumbrance = $app->input->post->get('encumbrance', 0);
        $_lodj = $app->input->post->get('lodj', 0);
        $_pvh = $app->input->post->get('pvh', 0);
        $_export = $app->input->post->get('export', 0);
        $_exclusive = $app->input->post->get('EXCLUSIVE', 0);
		//$_close = $app->input->post->get('close', 0);
		//$_realtors_base = $app->input->post->get('realtors_base', 0);
        $target_agent = $app->input->post->get('fromag', 0, 'uint');
        $pictures = $app->input->post->get('pics', array(), 'array');
        $picnames = $app->input->post->get('picnames', array(), 'array');
        $checks1 = $app->input->post->get('checks1', array(), 'array');
        $checks2 = $app->input->post->get('checks2', array(), 'array');
		
		//доступ к базе
		$_open_and_realtors = $app->input->post->get('open_and_realtors', 0);
		$_access_base = $app->input->post->get('access_base', 0);
		
		switch ((int)$_access_base + (int)$_open_and_realtors){
			case 1:
				$data['close'] = 1;
				$data['realtors_base'] = 0;
				break;
			case 2:
				$data['close'] = 0;
				$data['realtors_base'] = 1;
				break;
			case 3:
				$data['close'] = 0;
				$data['realtors_base'] = 0;
				break;
			case 4:
				$data['close'] = 0;
				$data['realtors_base'] = 2;
				break;
			default:
				//$data['close'] = 0;
				//$data['realtors_base'] = 0;
				//break;
				echo 'Ошибка при определении доступа записи к базе';
				exit;
		}
		
        $data['NEWBUILD'] = (($_new_build) ? 1 : 0);
        $data['encumbrance'] = (($_encumbrance) ? 1 : 0);
		$data['BAL'] = (($_bal) ? 1 : 0);
        $data['LODJ'] = (($_lodj) ? 1 : 0);
        $data['PVH'] = (($_pvh) ? 1 : 0);
        $data['export'] = (($_export) ? 1 : 0);
        $data['EXCLUSIVE'] = (($_exclusive) ? 1 : 0);
		
        //$data['close'] = (($_close) ? 1 : 0);
		//$data['realtors_base'] = (($_realtors_base) ? 1 : 0);

        if($target_agent) {
            $data['fromag'] = $target_agent;
        }

        if($pictures) {
            $data['PICTURES'] = implode(';', $pictures);
            $data['PICNAMES'] = implode(';', $picnames);
        }

        $data['CHECKS1'] = implode('/', $checks1);
        $data['CHECKS2'] = implode('/', $checks2);

        // Сохранение данных
        $this->store($data);
    }
        
        /**
         * Возвращает список активных жалоб
         * 
         * @return  array or bool
         */
        public function getActiveComplaint()
        {
            $result = FALSE;
                        
            if($this->_item) {
                $db = JFactory::getDbo();
                $query = 'SELECT * FROM `#__sttnmlscompl` WHERE `status`=1 AND `typeobj`=1 AND `compid`="' . $this->_item->COMPID . '" AND `cardnum`="' . $this->_item->CARDNUM . '"';
                $db->setQuery($query);
                $result = $db->loadObjectList();
            }
            return $result;
        }
        
        /**
         * Возвращает информацию об агенте
         * 
         * @return  bool or object
         */
        public function getAgentInfo()
        {
            $result = FALSE;
            
            if($this->_item) {
                $db = JFactory::getDbo();
                $query = 'SELECT * FROM `#__sttnmlsvocagents` WHERE `COMPID`="'.$this->_item->COMPID.'" AND `ID`="'.$this->_item->AGENTID.'"';
                $db->setQuery($query);
                $result = $db->loadObject();
            }
            return $result;
        }

        /**
         * Возвращает список застройщиков из категории
         *
         * @return array
         */
        public function getBuilderList()
        {
            $result = array();

            // JOOMLA Instances
            $db = JFactory::getDbo();
            $params = JComponentHelper::getParams('com_sttnmls');

            $builders_category = $params->get('builders_category', 0);
            if($builders_category) {
                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('vf') . '.*');
                $query->from($db->quoteName('#__sttnmlsvocfirms', 'vf'));
                $query->join('LEFT', $db->quoteName('#__sttnmlsfirmcatf', 'fc') . ' ON(' . $db->quoteName('fc.idfirm') . '=' . $db->quoteName('vf.ID') . ')');
                $query->where($db->quoteName('fc.idcatf') . '=' . $db->quote($builders_category));
                $query->order($db->quoteName('vf.NAME') . ' ASC');
                $db->setQuery($query);

                $result = $db->loadObjectList('ID');
            }
            return $result;
        }



        public function getBuildingList($builder = 0)
        {
            $builders_id = array();

            // JOOMLA Instances
            $db = JFactory::getDbo();

            if(!$builder) {
                $builder_list = $this->getBuilderList();
                foreach($builder_list as $item) {
                    $builders_id[] = $item->ID;
                }
            } else {
                $builders_id[] = $builder;
            }

            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsbuildings'));
            $query->where($db->quoteName('comp') . ' IN(' . implode(',', $builders_id) . ')');
            $query->where($db->quoteName('flag') . ' NOT IN(3,4)');
            $db->setQuery($query);

            return $db->loadObjectList();
        }
}
