<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewBuilding extends JViewLegacy
{
	function display($tpl = null) 
	{
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $agent_model = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $building_model = JModelLegacy::getInstance('building', 'SttNmlsModel');
        $buildings_model = JModelLegacy::getInstance('buildings', 'SttNmlsModel');
        $order_model =  JModelLegacy::getInstance('order', 'SttNmlsModel');


        // Vars from request
        $layout = $app->input->get('layout', '', 'string');

        $this->item = $this->get('Item');
        $this->stype = (($this->item->VARIANT == 2) ? 1 : 0);
        $this->udobstva = $this->get('Udobstva');
        $this->imgpath = $this->get('Imgpath');
        $this->tp = $this->get('Tp');
        $this->agthumb = $this->get('ComUserThumb');

        $this->builders_list = $buildings_model->getBuilderFirmsList();
        $this->buildings_classes_list = $buildings_model->getBuildingsClassesList();
        $this->buildings_furnish_list = $buildings_model->getBuildingsFurnishList();
        $this->wmater = (($params->get('wmaterial') != '') ? $buildings_model->getOptionsByID($params->get('wmaterial'), 8) : FALSE);
        $this->buildings_types = $building_model->getBuildingsTypes();
        $this->payment_types = $building_model->getPaymentTypes();
        $this->banks_list = $building_model->getBanksList();
        $this->item_building_plans = $building_model->getBuildingPlans($this->item->CARDNUM, $this->item->comp);

		if($layout == 'edit') {
            $this->optwmaterial = $this->get('OptWmaterial');
            $this->optcity = $this->get('OptCity');

//                print_r($this->item);




//			$this->checks2=$chs->checks2;$this->opttype = $this->get('OptType');
//            $this->optpay = $this->get('OptPay');
//
//            $this->optcomp = $this->get('OptComp');
//            $this->optbanks = $this->get('OptBanks');
//
//            $this->optfurnish = $this->get('OptFurnish');

//            $this->opthtype = $this->get('OptHtype');
//            $this->optrooms = $this->get('OptRooms');
//            $chs = $this->get('CheckAr');
//            $this->checks1=$chs->checks1;
		} else {
			if(!$this->item) {
                header("HTTP/1.0 404 Not Found");
            }
			$this->userperm = $this->get('UserPerm');
			$document = JFactory::getDocument();
			$document->setTitle($this->get('Title'));
			$document->setDescription($this->get('Desc'));
		}
		parent::display($tpl);
	}
}
