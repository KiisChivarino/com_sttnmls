<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewAgents extends JViewLegacy
{
    function display($tpl = null)
    {
        // JOOMLA instance
        $app = JFactory::getApplication();
        $mAgents = JModelLegacy::getInstance('agents', 'SttnmlsModel');

        //
        $layout = $app->input->get('layout', '', 'string');

        // AJAX data switcher
        switch ($layout) {
            case 'default_select_compid':
                    $this->firms = $mAgents->getFirmsList($app->input->get('filter_catid', 0, 'uint'));
                    $this->state = $this->get('State');
                break;

            default:
                exit;
        }

        parent::display($tpl);


    }

}