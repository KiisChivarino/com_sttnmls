<?php
defined('_JEXEC') or die('Restricted access');

class SttNmlsModelOrder extends JModelLegacy {
	public $_item;

	public function getItem()
	{
            $user = JFactory::getUser();
            $userid	= $user->get('id');
            
            //если незарегистрированный пользователь, то пустые поля
            if(!$userid)
            {
                $this->_item = new stdClass();
                $this->_item->name  = '';
                $this->_item->phone = '';
                $this->_item->email = '';
                $this->_item->hour1 = 8;
                $this->_item->hour2 = 20;
                return $this->_item;
            }
            //если зарегистрированный пользователь, то находим его последнюю заявку и оттуда берем все данные
            $db = $this->getDbo();
            $db->setQuery('SELECT `name`, `phone`, `email`, `hour1`, `hour2` FROM `#__sttnmlsorder` WHERE `userid`=' . $db->quote($userid) . ' ORDER BY `id` DESC ', 0, 1);
            $this->_item = $db->loadObject();
            if(count($this->_item) > 0) {
                return $this->_item;
            }

            //если нет заявок, берем имя, телефон и почту из таблицы агентов или пользователей
            $query = $db->getQuery(true);
            $query->select('a.NAME AS name, u.name AS jname, a.EMAIL AS email, u.email AS jemail, a.PHONE AS phone, 8 AS hour1, 24 AS hour2 ');
            $query->from(' #__users AS u ');
            $query->join('LEFT','`#__sttnmlsvocagents` AS a ON u.id = a.userid');
            $query->where('u.id = '.$db->quote($userid));
            $db->setQuery($query);
            $this->_item = $db->loadObject();
            if(!$this->_item->name && $this->_item->jname) {
                $this->_item->name = $this->_item->jname;
            }
            if(!$this->_item->email && $this->_item->jemail) {
                $this->_item->email = $this->_item->jemail;
            }
            return $this->_item;
	}
        
	public function save()
	{
		$app = JFactory::getApplication();
        $params = JComponentHelper::getParams('com_sttnmls');
        $plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
        $plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
        $plg_recaptcha_params = new JRegistry($plg_recaptcha->params);
		
		$user = JFactory::getUser();
		$userid	= $user->get('id');
        // Защита повторной отправки формы
        JSession::checkToken() or $app->redirect($refLink,$app->enqueueMessage(JText::_('JINVALID_TOKEN'),'error'));

        // Проверка капчи, если включена
        if(!$userid){
			if ($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
				// Подключение плагина капчи
				JPluginHelper::importPlugin('captcha');

				// Подключение диспетчера
				$dispatcher = JEventDispatcher::getInstance();

				$post = JFactory::getApplication()->input->post;
				$chk_result = $dispatcher->trigger('onCheckAnswer', $post->get('recaptcha_response_field'));
				if(!$chk_result[0]){
					JError::raiseWarning(1001, JText::_('COM_STTNMLS_MESSAGE_ERROR_CAPTCHA_CODE'));
					return;
				}
			}
		}
		
		$data  = JRequest::getVar('jform', array(), 'post', 'array');
        //проверка данных
		$typeobj=0;
		$table='';
		if(!$data['obj']) $app->redirect($refLink, $app->enqueueMessage(JText::_('COM_STTNMLS_OBJNOTFOUND'),'error'));
		if(!$data['cid']) $app->redirect($refLink, $app->enqueueMessage(JText::_('COM_STTNMLS_OBJNOTFOUND'),'error'));
		if(!$data['cn'])  $app->redirect($refLink,  $app->enqueueMessage(JText::_('COM_STTNMLS_OBJNOTFOUND'),'error'));
		if($data['obj']=='apart'){
			$typeobj=1;
			$table='`#__sttnmlsaparts`';
		}
		if($data['obj']=='com'){
			$typeobj=2;
			$table='`#__sttnmlscommerc`';
		}
		if($data['obj']=='garage'){
			$typeobj=3;
			$table='`#__sttnmlsgarages`';
		}
		if($data['obj']=='house'){
			$typeobj=4;
			$table='`#__sttnmlshouses`';
		}
        if($data['obj']=='building'){
			$typeobj=5;
			$table='`#__sttnmlsbuildings`';
		}
		if(!$typeobj) $app->redirect($refLink, $app->enqueueMessage(JText::_('COM_STTNMLS_OBJNOTFOUND'),'error'));
		//определяем адрес адресата
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.CARDNUM, f.EMAIL as firmemail, ag.EMAIL as agentemail, u.email as jemail, gr.mdays as mdays,  TIMESTAMPDIFF(MINUTE,a.DATEINP,now())/24/60 as alldays ');
		$query->from($table . ' AS a ');
		$query->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.ID');
		$query->join('LEFT','`#__sttnmlsvocagents` as ag on a.COMPID=ag.COMPID and a.AGENTID=ag.ID');
		$query->join('LEFT','`#__sttnmlsgroup` as gr on gr.id=ag.grp');
		$query->join('LEFT','`#__users` AS u on u.id=ag.userid');
		$query->where('a.COMPID='.$db->quote($data['cid']));
		$query->where('a.CARDNUM='.$db->quote($data['cn']));
		$db->setQuery($query);
		$res=$db->loadObject();
		if(!$res) $app->redirect($refLink, $app->enqueueMessage(JText::_('COM_STTNMLS_OBJNOTFOUND'),'error'));
		$email=$res->jemail;
		if(!$email) $email = $res->agentemail;
		if(!$email) $email = $res->firmemail;
		if(!$email) $app->redirect($refLink, $app->enqueueMessage(JText::_('COM_STTNMLS_EMAILNOTFOUND'),'error'));
		//сохраняем заявку
		$query = $db->getQuery(true);
		$query->insert('#__sttnmlsorder');
		$query->set('userid='.$db->quote($userid));
		$query->set('typeobj='.$db->quote($typeobj));
		$query->set('compid='.$db->quote($data['cid']));
		$query->set('cardnum='.$db->quote($data['cn']));
		$query->set('name='.$db->quote($data['name']));
		$query->set('phone='.$db->quote($data['phone']));
		$query->set('email='.$db->quote($data['email']));
		$h1=$data['hour1'];
		$h2=$data['hour2'];
		if($h1<0) $h1=0;
		if($h1>23) $h1=8;
		if($h2<0) $h2=0;
		if($h2>24) $h2=24;
		if($h1>=$h2){
			$h2=20;
			$h1=8;
		}
		$data['hour1']=$h1;
		$data['hour2']=$h2;
		$query->set('hour1='.$db->quote($h1));
		$query->set('hour2='.$db->quote($h2));
		$query->set('sendto='.$db->quote($email));
		$params = JComponentHelper::getParams('com_sttnmls');
		$sendadmin = $params->get('adminemail', '');
		$query->set('sendadmin='.$db->quote($sendadmin));
		$date = JFactory::getDate();
		$query->set('dateinp='.$db->quote($date->toSql()));
		$db->setQuery($query);
		$db->query();
        
        //выбираем почту для новостроек
        $mFirm = JModelLegacy::getInstance('firm', 'SttnmlsModel');
        $noBuildingMail = $mFirm->getFirmInfoByID($data['cid'])->noBuildingMail;
        $buildingsmail = $params->get('buildingsmail', '');
        if ($data['obj']=='building'){
            if ($res->firmemail and $noBuildingMail==="0") $email = $res->firmemail; //для агенств без запрета отправляем на почту фирмы
            else {
                ($buildingsmail) ? $email = $buildingsmail : $email = $sendadmin;
            } //для агенств с запертом отправляем на специальную почту 
        }
        
        //отправляем письмо
        $this->sendmail($data, $email, $sendadmin, $res->mdays<=$res->alldays);
		
        //сообщаем, что письмо отправлено
		//$app->redirect(JRoute::_('index.php?option=com_sttnmls&view=order&layout=confirmed&tmpl=component'));
		$app->redirect($refLink,JText::_('COM_STTNMLS_MESSAGE_INFO_ORDER_SEND_SUCCESSFULL'));
        return;
	}
	public function sendmail($data,$sendto,$sendadmin, $sendsobst = true)
	{
		$app = JFactory::getApplication();
		$mailfrom = $app->getCfg('mailfrom');
		$fromname = $app->getCfg('fromname');
        ($data['obj']=='building') ? $orderSubject = 'COM_STTNMLS_ORDER_SUBJECT_FOR_BUILDING' : $orderSubject = 'COM_STTNMLS_ORDER_SUBJECT';
		$subject=JText::_($orderSubject);
		$sendfrom = '';
		if($data['email']) $sendfrom = 'или по почте '.$data['email'];
		$url = JURI::root().JRoute::_('index.php?option=com_sttnmls&view=aparts&type=card&cn=' . $data['cn'] . '&cid=' . $data['cid'], false);
		if($data['obj']=='com')
			$url = JURI::root().JRoute::_('index.php?option=com_sttnmls&view=coms&type=card&cn=' . $data['cn'] . '&cid=' . $data['cid'], false);
		if($data['obj']=='garage')
			$url = JURI::root().JRoute::_('index.php?option=com_sttnmls&view=garages&type=card&cn=' . $data['cn'] . '&cid=' . $data['cid'], false);
		if($data['obj']=='house')
			$url = JURI::root().JRoute::_('index.php?option=com_sttnmls&view=houses&type=card&cn=' . $data['cn'] . '&cid=' . $data['cid'], false);
        if($data['obj']=='building')
			$url = JURI::root().JRoute::_('index.php?option=com_sttnmls&view=building&type=card&cn=' . $data['cn'] . '&cid=' . $data['cid'], false);
		$url = str_replace('http://', '|', $url);
		$url = str_replace('//', '/', $url);
		$url = str_replace('|','http://', $url);
        $data['url'] = $url;
        $data['sendfrom'] = $sendfrom;
        $data['fromname'] = $fromname;
		if ($data['obj']=='building') 
            $body=JText::sprintf('COM_STTNMLS_ORDER_BODY_FOR_BUILDING',$data['name'],$url,$data['title'],$data['phone'],$data['hour1'],$data['hour2'],$sendfrom,$fromname);
        else
            $body=JText::sprintf('COM_STTNMLS_ORDER_BODY',$data['name'],$url,$data['phone'],$data['hour1'],$data['hour2'],$sendfrom,$fromname);
		
            $mail = JFactory::getMailer();
            $mail->addRecipient($sendto);
            $mail->setSender(array($mailfrom, $fromname));
            $mail->setSubject($subject);
            $mail->setBody($body);
            $mail->isHtml(TRUE);
            if($sendsobst) {
                //	хозяину объекта шлем, если срок модерирования прошел
                $sent = $mail->Send();
            }
		if($sendadmin)
		{
			$mail = JFactory::getMailer();
			$mail->addRecipient($sendadmin);
			$mail->setSender(array($mailfrom, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
            $mail->isHtml(TRUE);
			$sent = $mail->Send();
		}
		// если объект на модерации, то отправлям заявку и просмотревшим модераторам (тем, кто подтвердил, что это не шаблон)
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		if(SttNmlsHelper::checkObjModer($data['obj'], $data['cn'], $data['cid'])) {
			$typeobj=1;
			$tablename='sttnmlsaparts';
			if($data['obj']=='com') { $typeobj=2; $tablename='sttnmlscommerc';}
			if($data['obj']=='garage') { $typeobj=3; $tablename='sttnmlsgarages';}
			if($data['obj']=='house') { $typeobj=4; $tablename='sttnmlshouses';}
            if($data['obj']=='building') { $typeobj=5; $tablename='sttnmlsbuildings';}
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select('a.userid');
			$query->from('#__'.$tablename.' as o');
			$query->join('LEFT','`#__sttnmlsvocagents` as a on a.ID=o.AGENTID and a.COMPID=o.COMPID');
			$query->where(' o.COMPID='.$db->quote($data['cid']));
			$query->where(' o.CARDNUM='.$db->quote($data['cn']));
			$db->setQuery($query);
			$userid = $db->loadResult();
			$query = $db->getQuery(true);
			$query->select('a.userid, u.email');
			$query->from('#__sttnmlsmoder as a');
			$query->join('LEFT','`#__users` as u on a.userid=u.id');
			$query->where(' a.compid='.$db->quote($data['cid']));
			$query->where(' a.cardnum='.$db->quote($data['cn']));
			$query->where(' a.typeobj='.$db->quote($typeobj));
			if($userid)	$query->where(' a.userid<>'.$db->quote($userid));
			$query->where(' a.q1=1 ');
			$db->setQuery($query);
			$items=$db->loadObjectList();
                if(count($items)){
                    foreach($items as $item){
                        $mail = JFactory::getMailer();
                        $mail->addRecipient($item->email);
                        $mail->setSender(array($mailfrom, $fromname));
                        $mail->setSubject($subject);
                        $mail->setBody($body);
                        $mail->isHtml(TRUE);
                        $sent = $mail->Send();
                    }
                }
		}
	}
}
