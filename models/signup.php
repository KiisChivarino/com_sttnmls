<?php defined('_JEXEC') or die('Restricted access');

require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once( JPATH_SITE . '/components/com_users/models/registration.php' );

class SttnmlsModelSignup extends UsersModelRegistration
{
    private $_user;
    private $_token;
    
    public function activate($token)
    {
        $config = JFactory::getConfig();
        $userParams = JComponentHelper::getParams('com_users');
        JFactory::getLanguage()->load('com_users', JPATH_BASE);
        $db = $this->getDbo();

        // Get the user id based on the token.
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('id'))
                ->from($db->quoteName('#__users'))
                ->where($db->quoteName('activation') . ' = ' . $db->quote($token))
                ->where($db->quoteName('block') . ' = ' . 1)
                ->where($db->quoteName('lastvisitDate') . ' = ' . $db->quote($db->getNullDate()));
        $db->setQuery($query);

        try
        {
            $userId = (int) $db->loadResult();
        }
        catch (RuntimeException $e)
        {
            $this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
            return false;
        }

        // Check for a valid user id.
        if (!$userId)
        {
            $this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
            return false;
        }

        // Activate the user.
        $user = JFactory::getUser($userId);

        $user->set('activation', '');
        $user->set('block', '0');

        // Store the user object.
        if (!$user->save())
        {
            $this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));

            return false;
        }

        return $user;
    }
    
    /**
     * Получение информации об агенте по его UserID
     * 
     * @param       integer         $id - ID агента
     * @return      boolean
     */
    public function getAgentInfoByUserID($id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from('`#__sttnmlsvocagents`');
        $query->where('`userid`=' . $db->quote($id));
        $db->setQuery($query);
        
        return $db->loadObject();
    }
    
    /**
     * Получение информации о фирме по ее ID
     * 
     * @param       integer         $id - ID фирмы
     * @return      object
     */
    public function getFirmInfoByID($id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from('`#__sttnmlsvocfirms`');
        $query->where('`id`=' . $db->quote($id));
        $db->setQuery($query);
        
        return $db->loadObject();
    }
    
    public function getToken($token = '')
    {
        // Подгрузка хелпера валидации
        require_once(JPATH_COMPONENT . '/helpers/sttnmlsvalidate.php');
        
        $app = JFactory::getApplication();
        
        $this->_token = (($token != '') ? $token : $app->input->getVar('token', ''));
        
        if($this->_token == '' OR !SttnmlsValidateHelper::valid_md5($this->_token)) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_TOKEN'), 'error');
            $app->redirect(JRoute::_('index.php?option=com_sttnmls&view=signup'));
            return FALSE;
        }
        
        return $this->_token;
    }
    
    public function getUser()
    {
        $app = JFactory::getApplication();
        $lang = JFactory::getLanguage();
        $lang->load('com_users', JPATH_BASE);
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->clear()
                ->select('`id`')
                ->from($db->quoteName('#__users'))
                ->where('`activation`=' . $db->quote($this->_token))
                ->where('`block`="1"')
                ->where('`lastvisitDate`=' . $db->quote($db->getNullDate()));
        $db->setQuery($query);
        $user_id = $db->loadResult();
        
        if (!$user_id) {
            $app->enqueueMessage(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'), 'error');
            $app->redirect('index.php');
            return FALSE;
        }
        
        $this->_user = JFactory::getUser($user_id);

        if ($this->_user === FALSE) {
            $app->enqueueMessage(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED'), 'error');
            $app->redirect('index.php');
            return FALSE;
        }
        return $this->_user;
    }
    
    public function getProfile()
    {
        $db = $this->getDbo();
        $user_id = $this->_user->id;
        $db->setQuery(
                'SELECT profile_value FROM #__user_profiles ' .
                'WHERE user_id = ' . $db->quote($user_id) . " AND profile_key = 'profile.realtor' " .
                'ORDER BY ordering'
        );
        $result = $db->loadResult();
        if(!$result) {
            return 2;
        } else { 
            return json_decode($result, TRUE);
        }
    }
    
    public function getCatf()
    {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
        return SttNmlsHelper::getCatfOptions(JText::_('COM_STTNMLS_SELECT0'));
    }
    
    public function getFirmsList($cat = 0)
    {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
        return SttNmlsHelper::getOptFirms(0, '', 0, $cat);
    }
            
    public function register($temp)
    {
        $params = JComponentHelper::getParams('com_users');
        JFactory::getLanguage()->load('com_users', JPATH_BASE);

        // Initialise the table with JUser.
        $user = new JUser;
        $data = (array) $this->getData();

        // Merge in the registration data.
        foreach ($temp as $k => $v)
        {
            $data[$k] = $v;
        }

        // Prepare the data for the user object.
        $data['email'] = JStringPunycode::emailToPunycode($data['email1']);
        $data['password'] = $data['password1'];
        $useractivation = $params->get('useractivation');
        $sendpassword = $params->get('sendpassword', 1);

        // Check if the user needs to activate their account.
        if (($useractivation == 1) OR ($useractivation == 2))
        {
            $data['activation'] = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
            $data['block'] = 1;
        }

        // Bind the data.
        if (!$user->bind($data))
        {
            $this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));

            return false;
        }

        // Store the data.
        if (!$user->save())
        {
            $this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));

            return false;
        }
        
        $config = JFactory::getConfig();
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $profile_type = $data['profile']['realtor'];
        
        // Регистрация типа записи пользователя --------------------------------
        $query->clear()
                ->delete($db->quoteName('#__user_profiles'))
                ->where('`user_id`=' . $db->quote($user->id))
                ->where('`profile_key`="profile.realtor"');
        $db->setQuery($query);
        $db->execute();
        
        $query->clear()
                ->insert($db->quoteName('#__user_profiles'))
                ->columns($db->quoteName(array('user_id', 'profile_key', 'profile_value', 'ordering')))
                ->values($db->quote($user->id) . ', "profile.realtor", ' . $db->quote(json_encode((string)$profile_type)) . ', 1');
        $db->setQuery($query);
        $db->execute();
        // END: Регистрация типа записи пользователя ---------------------------
        

        // Compile the notification mail values.
        $data = $user->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['siteurl'] = JUri::root();

        // Handle account activation/confirmation emails.
        if ($useractivation == 2)
        {
            // Set the link to confirm the user email.
            $uri = JUri::getInstance();
            $base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
            $data['activate'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'signup', '&layout=activate&token=' . $data['activation']);

            $emailSubject = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            if ($sendpassword)
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
                    $data['name'],
                    $data['sitename'],
                    $data['activate'],
                    $data['siteurl'],
                    $data['username'],
                    $data['password_clear']
                );
            }
            else
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
                    $data['name'],
                    $data['sitename'],
                    $data['activate'],
                    $data['siteurl'],
                    $data['username']
                );
            }
        }
        elseif ($useractivation == 1)
        {
            // Set the link to activate the user account.
            $uri = JUri::getInstance();
            $base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
            $data['activate'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'signup', '&layout=activate&token=' . $data['activation']);

            $emailSubject = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            if ($sendpassword)
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
                    $data['name'],
                    $data['sitename'],
                    $data['activate'],
                    $data['siteurl'],
                    $data['username'],
                    $data['password_clear']
                );
            }
            else
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
                    $data['name'],
                    $data['sitename'],
                    $data['activate'],
                    $data['siteurl'],
                    $data['username']
                );
            }
        }
        else
        {
            $emailSubject = JText::sprintf(
                'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                $data['name'],
                $data['sitename']
            );

            if ($sendpassword)
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_BODY',
                    $data['name'],
                    $data['sitename'],
                    $data['siteurl'],
                    $data['username'],
                    $data['password_clear']
                );
            }
            else
            {
                $emailBody = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',
                    $data['name'],
                    $data['sitename'],
                    $data['siteurl']
                );
            }
        }

        // Send the registration email.
        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

        // Send Notification mail to administrators
        if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
        {
            $emailSubject = JText::sprintf(
                    'COM_USERS_EMAIL_ACCOUNT_DETAILS',
                    $data['name'],
                    $data['sitename']
            );

            $emailBodyAdmin = JText::sprintf(
                    'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
                    $data['name'],
                    $data['username'],
                    $data['siteurl']
            );

            // Get all admin users
            $query->clear()
                    ->select($db->quoteName(array('name', 'email', 'sendEmail')))
                    ->from($db->quoteName('#__users'))
                    ->where($db->quoteName('sendEmail') . ' = ' . 1);

            $db->setQuery($query);

            try
            {
                $rows = $db->loadObjectList();
            }
            catch (RuntimeException $e)
            {
                $this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

                return false;
            }

            // Send mail to all superadministrators id
            foreach ($rows as $row)
            {
                $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

                // Check for an error.
                if ($return !== true)
                {
                    $this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

                    return false;
                }
            }
        }

        // Check for an error.
        if ($return !== true)
        {
            $this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

            // Send a system message to administrators receiving system mails
            $db = JFactory::getDbo();
            $query->clear()
                    ->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
                    ->from($db->quoteName('#__users'))
                    ->where($db->quoteName('block') . ' = ' . (int) 0)
                    ->where($db->quoteName('sendEmail') . ' = ' . (int) 1);
            $db->setQuery($query);

            try
            {
                $sendEmail = $db->loadColumn();
            }
            catch (RuntimeException $e)
            {
                $this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

                return false;
            }

            if (count($sendEmail) > 0)
            {
                $jdate = new JDate;

                // Build the query to add the messages
                foreach ($sendEmail as $userid)
                {
                    $values = array(
                        $db->quote($userid),
                        $db->quote($userid),
                        $db->quote($jdate->toSql()),
                        $db->quote(JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')),
                        $db->quote(JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username']))
                    );
                    $query->clear()
                            ->insert($db->quoteName('#__messages'))
                            ->columns($db->quoteName(array('user_id_from', 'user_id_to', 'date_time', 'subject', 'message')))
                            ->values(implode(',', $values));
                    $db->setQuery($query);

                    try
                    {
                        $db->execute();
                    }
                    catch (RuntimeException $e)
                    {
                        $this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

                        return false;
                    }
                }
            }

            return false;
        }

        if ($useractivation == 1)
        {
            return "useractivate";
        }
        elseif ($useractivation == 2)
        {
            return "adminactivate";
        }
        else
        {
            return $user->id;
        }
    }
    
    
    public function save($user_id, $data)
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $params = JComponentHelper::getParams('com_sttnmls');
        JFactory::getLanguage()->load('com_users', JPATH_BASE);

        if(!isset($data['fam'])) {
            $data['fam'] = '';
        }
        if(!isset($data['compid'])) {
            $data['compid'] = $params->get('compidsob', 0);
        }
        
        // Поиск указанной при регистрации фирмы
        $firm = $this->getFirmInfoByID($data['compid']);
        if($firm) {
            if(trim($firm->realname)) {
                $data['firmname'] = trim($firm->realname);
            } else {
                $data['firmname'] = trim($firm->name);
            }
        } else {
            $data['firmname'] = '';
        }
        
        // Подготовка данных для сохранения
        $juser = JFactory::getUser($user_id);
        $agent = $this->getAgentInfoByUserID($user_id);
        if($agent) {
            $query->update('`#__sttnmlsvocagents`');
            $query->where('`id`=' . $db->quote($agent->id));
            $query->where('`compid`=' . $db->quote($agent->compid));
        } else {
            $query->insert('`#__sttnmlsvocagents`');
            $query->set('`compid`=' . $db->quote($data['compid']));
            $query->set('`id`=' . $db->quote($user_id*100));
        }
        $query->set('`sirname`=' . $db->quote($data['fam']));
        $query->set('`name`=' . $db->quote($data['name']));
        $query->set('`secname`=' . $db->quote($data['name2']));
        $query->set('`phone`=' . $db->quote($data['phone']));
        $query->set('`email`=' . $db->quote($juser->email));
        $query->set('`userid`=' . $db->quote($user_id));
        $query->set('`fullinf`=' . $db->quote(sprintf('%s %s %s, %s', $data['fam'], $data['name'], $data['name2'], $data['phone'])));
        
        $compidsliv = $params->get('compidsliv', 0);
        if($data['realtor'] == 1) {
            $grp = $params->get('groupid2', 0);
            if($compidsliv && $compidsliv == $data['compid']) {
                $query->set('`flag`="2"');
            } else {
                $query->set('`flag`="1"');
            }
        } else {
            $grp = $params->get('groupid1', 0);
            $query->set('`flag`="2"');
        }
        
        $query->set('`grp`=' . $db->quote($grp));
        $db->setQuery($query);
        $db->execute();

        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__users'));
        $query->set($db->quoteName('name') . '=' . $db->quote($data['fam'] . ' ' . $data['name'] . ' ' . $data['name2']));
        $query->where($db->quoteName('id') . '=' . $db->quote($user_id));
        $db->setQuery($query);
        $db->execute();

        $data['user_id'] = $user_id;

        $this->sendmail($data);
        
        
        $app->enqueueMessage(JText::_('COM_USERS_REGISTRATION_ACTIVATE_SUCCESS'), 'notice');
        $link1 = SttNmlsHelper::getSEFUrl('com_users', 'login');
        $returnURL = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile');
        $link = new JURI($link1);
        $link->setVar('return', base64_encode($returnURL)); 
        $app->redirect($link);
        return TRUE;
    }
    
    public function sendmail($data)
    {
        $app = JFactory::getApplication();
        $mailfrom = $app->getCfg('mailfrom');
        $fromname = $app->getCfg('fromname');
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $db = JFactory::getDbo();

        $agent = $mAgent->getAgentInfoByID($data['user_id']);

        $emailSubject = JText::_('COM_STTNMLS_EMAIL_SIGNUP_ADMIN_NOTIFICATION_SUBJECT');
        switch ($data['realtor']) {
            case 1:
                $emailBody = JText::sprintf(
                    'COM_STTNMLS_EMAIL_SIGNUP_ADMIN_NOTIFICATION_REALTOR_BODY',
                    $agent->EMAIL,
                    $agent->SIRNAME,
                    $agent->NAME,
                    $agent->SECNAME,
                    $agent->PHONE,
                    $agent->firmname
                );
                break;
            case 2:
                $emailBody = JText::sprintf(
                    'COM_STTNMLS_EMAIL_SIGNUP_ADMIN_NOTIFICATION_SPECIALIST_BODY',
                    $agent->EMAIL,
                    $agent->SIRNAME,
                    $agent->NAME,
                    $agent->SECNAME,
                    $agent->PHONE,
                    $agent->firmname
                );
                break;
            default:
                $emailBody = JText::sprintf(
                    'COM_STTNMLS_EMAIL_SIGNUP_ADMIN_NOTIFICATION_SOBST_BODY',
                    $agent->EMAIL,
                    $agent->NAME,
                    $agent->SECNAME,
                    $agent->PHONE
                );
                break;
        }

        // get all admin users
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')));
        $query->from($db->quoteName('#__users'));
        $query->where($db->quoteName('sendEmail') . '="1"');
        $db->setQuery( $query );
        $rows = $db->loadObjectList();

        // Send mail to all users with users creating permissions and receiving system emails
        foreach( $rows as $row )
        {
            $mail = JFactory::getMailer();
            $mail->addRecipient($row->email);
            $mail->setSender(array($mailfrom, $fromname));
            $mail->setSubject($emailSubject);
            $mail->setBody($emailBody);
            $sent = $mail->Send();
        }
    }
}