<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
$plugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha', 0));
if($plugin) {
	$captcha = JCaptcha::getInstance($plugin);
}
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<div class="sendorder">
	<form class="editform form-validate" action="index.php" method="post">
	<input type="hidden" name="option" value="com_sttnmls"/>
	<input type="hidden" name="view" value="order"/>
	<input type="hidden" name="task" value="save"/>
	<input type="hidden" name="jform[obj]" value="<?php echo JRequest::getVar('obj', ''); ?>"/>
	<input type="hidden" name="jform[cid]" value="<?php echo JRequest::getVar('cid', ''); ?>"/>
	<input type="hidden" name="jform[cn]" value="<?php echo JRequest::getVar('cn', ''); ?>"/>
	<?php echo JHTML::_( 'form.token' ); ?>
	<div class="orderfield">
		<label for="jform_name"><?php echo JText::_('COM_STTNMLS_NAME'); ?><span>*</span></label>
		<input id="jform_name" class="required" type="text" name="jform[name]" value="<?php echo $this->item->name;?>" size="20" />
	</div>
	<div class="orderfield">
		<label for="jform_phone"><?php echo JText::_('COM_STTNMLS_PHONE'); ?><span>*</span></label>
		<input id="jform_phone" class="required" type="text" name="jform[phone]" value="<?php echo $this->item->phone;?>" size="20" />
	</div>
	<div class="orderfield">
		<?php
		$options1 = array();
		for($a=0;$a<24;$a++)
		{
			$options1[] = JHTML::_('select.option', $a, $a);
		}	
		$hour1=JHTML::_('select.genericlist',  $options1, 'jform[hour1]', 'size="1" style="width:47px;"', 'value', 'text', $this->item->hour1);
		$options2 = array();
		for($a=1;$a<25;$a++)
		{
			$options2[] = JHTML::_('select.option', $a, $a);
		}	
		$hour2=JHTML::_('select.genericlist',  $options2, 'jform[hour2]', 'size="1" style="width:47px;"', 'value', 'text', $this->item->hour2);
		?>
		<label for="jformhour1"><?php echo JText::_('COM_STTNMLS_CALL_FROM'); ?></label>
		<?php echo $hour1; ?>
		<label for="jformhour2" style="width: 35px;"><?php echo JText::_('COM_STTNMLS_CALL_TO'); ?></label>
		<?php echo $hour2; ?>
	</div>
	<div class="orderfield">
		<label for="jform_email"><?php echo JText::_('COM_STTNMLS_EMAIL'); ?></label>
		<input id="jform_email" class="validate-email" type="text" name="jform[email]" value="<?php echo $this->item->email;?>" size="20" />
	</div>
	<?php if(isset($captcha)){ ?>
		<div class="orderfield" style="min-height: 40px;">
			<label><?php echo JText::_('COM_STTNMLS_ANTISPAM'); ?></label>
			<?php echo $captcha->display('captcha', 0); ?>
		</div>
	<?php } ?>
	<div class="orderfield" style="padding-top: 20px;">
		<label>&nbsp;</label>
		<input type="submit" class="validate sbgreen" value="<?php echo JText::_('COM_STTNMLS_SEND'); ?>">
	</div>
	</form>
</div>
