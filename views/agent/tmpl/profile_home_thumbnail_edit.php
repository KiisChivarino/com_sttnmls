<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="com_sttnmls_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php echo JText::_('COM_STTNMLS_LABEL_THUMBNAIL_SELECT') ?>
                </h4>
            </div>
            <div class="modal-body">
                <p class="text-justify" style="margin-bottom: 20px;">
                    <?php echo JText::_('COM_STTNMLS_LABEL_CHANGE_THUMBNAIL_DESC') ?>
                </p>
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <img id="target_image" src="<?php echo $this->avatar ?>" alt="" />
                    </div>

                    <div class="col-md-5 col-sm-6 col-xs-12">
                        <div style="width:64px;height:64px;overflow: hidden" id="wrapper_preview">
                            <img src="<?php echo $this->avatar ?>" id="preview" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <form
                    action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '', 'profile') ?>"
                    method="post"
                    id="com_sttnmls_thumbnail_form"
                >
                    <?php echo JHtml::_( 'form.token' ); ?>
                    <input type="hidden" name="task" value="json.createAvatarThumbnail" />
                    <input type="hidden" id="crop_x" name="x" value="0" />
                    <input type="hidden" id="crop_y" name="y" value="0" />
                    <input type="hidden" id="crop_w" name="w" value="0" />
                    <input type="hidden" id="crop_h" name="h" value="0" />

                    <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_STTNMLS_LABEL_SAVE_CHANGES') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var wrapper_w = $('#wrapper_preview').width();
        var wrapper_h = $('#wrapper_preview').height();

        $('#target_image').Jcrop({
            minSize: [64, 64],
            aspectRatio: 1,
            onChange: showPreview,
            onSelect: showPreview,
            setSelect: [0, 0, 128, 128]
        });

        $('#com_sttnmls_thumbnail_form').on('submit', function(e){
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function(response) {
                    if(!response.result) {
                        alert(response.message);
                    } else {
                        alert('<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_SAVED') ?>');
                        window.location.reload();
                    }
                }
            });
            e.preventDefault();
        });

        function showPreview(coords)
        {
            var target_w = $('#target_image').width();
            var target_h = $('#target_image').height();
            var rx = wrapper_w / coords.w;
            var ry = wrapper_h / coords.h;

            $('#crop_x').val(coords.x);
            $('#crop_y').val(coords.y);
            $('#crop_w').val(coords.w);
            $('#crop_h').val(coords.h);

            $('#preview').css({
                width: Math.round(rx * target_w) + 'px',
                height: Math.round(ry * target_h) + 'px',
                marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                marginTop: '-' + Math.round(ry * coords.y) + 'px'
            });
        }
    });
</script>

