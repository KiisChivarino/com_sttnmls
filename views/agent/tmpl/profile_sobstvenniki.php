<?php defined('_JEXEC') or die('Restricted access');

$params = JComponentHelper::getParams('com_sttnmls');

?>

<h4><?php echo JText::_('COM_STTNMLS_TAB_SOB_HEAD'); ?></h4>
<hr />
<p class="well well-sm">
    <?php echo JText::_('COM_STTNMLS_TAB_SOB_HEAD2'); ?>
</p>

<div class="row">
    <div class="col-md-2">
        <span class="image-sobstvennik">&nbsp;</span>
    </div>
    <div class="col-md-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo JText::_('COM_STTNMLS_LABEL_SELL') ?>
            </div>
            <div class="panel-body">
                <a href="javascript:void(0);" id="aparts_0_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="aparts" data-stype="0" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_APARTS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntkv ?></span>
                </a>
                
                <a href="javascript:void(0);" id="aparts_0_1" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="aparts" data-stype="0" data-what="1" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_ROOMS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntkm ?></span>
                </a>
                
                <a href="javascript:void(0);" id="houses_0_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="houses" data-stype="0" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_HOUSES'); ?>
                    <span class="badge"><?php echo $this->item->sobcnths ?></span>
                </a>
                
                <a href="javascript:void(0);" id="houses_0_1" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="houses" data-stype="0" data-what="1" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_LANDS_AREAS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntuch ?></span>
                </a>
                
                <a href="javascript:void(0);" id="garages_0_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="garages" data-stype="0" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_GARAGES'); ?>
                    <span class="badge"><?php echo $this->item->sobcntgr ?></span>
                </a>
                
                <a href="javascript:void(0);" id="coms_0_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="coms" data-stype="0" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_COMMERC'); ?>
                    <span class="badge"><?php echo $this->item->sobcntcom ?></span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo JText::_('COM_STTNMLS_LABEL_RENT') ?>
            </div>
            <div class="panel-body">
                <a href="javascript:void(0);" id="aparts_1_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="aparts" data-stype="1" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_APARTS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntkvar ?></span>
                </a>
                
                <a href="javascript:void(0);" id="aparts_1_1" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="aparts" data-stype="1" data-what="1" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_ROOMS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntkmar ?></span>
                </a>
                
                <a href="javascript:void(0);" id="houses_1_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="houses" data-stype="1" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_HOUSES'); ?>
                    <span class="badge"><?php echo $this->item->sobcnthsar ?></span>
                </a>
                
                <a href="javascript:void(0);" id="houses_1_1" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="houses" data-stype="1" data-what="1" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_LANDS_AREAS'); ?>
                    <span class="badge"><?php echo $this->item->sobcntuchar ?></span>
                </a>
                
                <a href="javascript:void(0);" id="garages_1_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="garages" data-stype="1" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_GARAGES'); ?>
                    <span class="badge"><?php echo $this->item->sobcntgrar ?></span>
                </a>
                
                <a href="javascript:void(0);" id="coms_1_0" class="btn btn-default btn-block btn-sm col-sm-9 getObjectsList" data-container="object-list-container-sobstvenniki" data-table="coms" data-stype="1" data-what="0" data-sobstvennik="1">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_COMMERC'); ?>
                    <span class="badge"><?php echo $this->item->sobcntcomar ?></span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <?php echo JText::_('COM_STTNMLS_TAB_SOB_ACTIV') . $this->item->activity; ?><br />
        <?php echo JText::_('COM_STTNMLS_TAB_SOB_ACTIV_DESC');
        if($params->get('linktabsob', '')) { ?>
                <a href="<?php echo $params->get('linktabsob', ''); ?>">
                        <?php echo JText::_('COM_STTNMLS_TAB_SOB_ACTIV_MORE'); ?>
                </a>
        <?php }  ?>
    </div>
</div>