<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Подключение плагинов раздела CAPTCHA
JPluginHelper::importPlugin('captcha');

// Load JOOMLA core frameworks
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// RECAPTCHA Instance
$plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
$plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
$plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript("http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU");
$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$script .= <<<JS
    var OBJECT_TYPE = 3;
    var CARDNUM="{$this->item->CARDNUM}";
    var COMPID="{$this->item->COMPID}";	
    var g_u = "{$this->item->gorod}, {$this->item->ulica}";
    ymaps.ready(init);

    jQuery(document).ready(function($){
        $('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});
    });
    
JS;
if($this->item->gorod == SttNmlsLocal::TText('COM_STTNMLS_OBL'))
    $script .= ' var _zoom = "13";' . "\n";
else
    $script .= ' var _zoom = "15";' . "\n";
$doc->addScriptDeclaration($script);
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.inputmask.js");
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.yandex.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);


// Инициализация капчи
if($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
    // TODO: Работает только с версией reCAPTCHA 2.0. Хак для использования
    //       в нескольких формах на странице

    $plg_recaptcha_pub_key = $plg_recaptcha_params->get('public_key');
    $plg_recaptcha_theme = $plg_recaptcha_params->get('theme2');

    $doc->addScript(($app->isSSLConnection() ? 'https' : 'http') . '://www.google.com/recaptcha/api.js?hl=ru-RU&onload=onloadCallback&render=explicit');
    $captcha_code = <<<JS
        var onloadCallback = function() {
            grecaptcha.render("captcha_user_request_form", {sitekey: "{$plg_recaptcha_pub_key}", theme: "{$plg_recaptcha_theme}", size: "compact"});
            grecaptcha.render("captcha_rcall_form", {sitekey: "{$plg_recaptcha_pub_key}", theme: "{$plg_recaptcha_theme}", size: "compact"});
        };

JS;
    $doc->addScriptDeclaration($captcha_code);
}


$pics = (trim($this->item->PICTURES) != '') ? explode(';',$this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';',$this->item->PICNAMES) : '';
$socbut = $params->get('socbut', '');

$region = $params->get('region', 'Курская область');
$kursk = $params->get('kursk', 'Курск');
$kurske = $params->get('kurske', 'Курске');
$regione = $params->get('regione', 'Курской области');

?>

<div class="object-card-wrapper">
    <article class="clearfix">
        <!-- SIDEBAR -->
        <div class="col-sm-4 col-md-4">
            <?php if ($pics != ''): ?>
                <div class="sttnmls_photo">
                <?php foreach($pics as $i => $photo): ?>
                    <a rel="group1" class="fbox <?php echo (($i > 0) ? 'small' : 'big' )?>" title="" href="<?php echo SttnmlsHelper::getLinkPhoto($photo, 'big'); ?>">
                        <img class="thumb img-thumbnail" src="<?php echo SttnmlsHelper::getLinkPhoto($photo, (($i > 0) ? 'smallcrop' : 'big' )); ?>"<?php echo ((isset($picnames[$i]) && trim($picnames[$i]) != '') ? ' title="' . trim($picnames[$i]) . '"' : '') ?> />
                    </a>
                    <?php $i++ ?>
                <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>
            <?php endif; ?>
            <?php if($this->item->VTOUR && $this->item->VTOUR!='sWebLabel1') : ?>
                <div>
                    <noindex>
                        <a rel="rel nofollow" href="<?php echo $this->item->VTOUR; ?>" target="_blank">
                            <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/3dm.png"/>
                        </a>
                    </noindex>
                </div>
            <?php endif; ?>
            </div>
        <!-- /SIDEBAR -->
        
        <!-- CONTENT -->
        <div class="col-sm-8 col-md-8">
            <a name="TopShowObject"></a>
        
        <?php if( $user->authorise('core.admin') OR $this->userperm == 1) : ?>
            <?php if ($this->item->isArchiveMode): ?>
                <div class="sttnmls_photo_manager well well-sm text-right" style="margin-bottom: 2px;">
                    <div class="alert">
                        <?php
                        $this->item->archiver_url = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&comp_id=' . $this->item->archiver_compid . '&agent_id=' . $this->item->archiver_iid, 'default');
                        echo SttNmlsLocal::parseString(JText::_('COM_STTNMLS_LABEL_ARCHIVER'), $this->item)
                        ?>
                    </div>

                    <a
                        href="javascript:void(0);"
                        class="btn btn-success btn-xs repairObjectFromArchive"
                        data-oid="<?php echo $this->item->oid ?>"
                        data-view="garage"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="<?php echo JText::_('COM_STTNMLS_LABEL_REPAIR_FROM_ARCHIVE'); ?>"
                        data-confirm-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM'); ?>"
                    >
                        <span class="glyphicon glyphicon-repeat"></span>
                        <?php echo JText::_('COM_STTNMLS_LABEL_REPAIR_FROM_ARCHIVE'); ?>
                    </a>

                    <a
                        href="javascript:void(0);"
                        class="btn btn-danger btn-xs removeObjectFromArchive"
                        data-oid="<?php echo $this->item->oid ?>"
                        data-view="garage"
                        data-toggle="tooltip"
                        data-placement="top"
                        title="<?php echo JText::_('COM_STTNMLS_LABEL_DELETE_FROM_ARCHIVE'); ?>"
                        data-confirm-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM_REMOVE_OBJECT_FROM_ARCHIVE'); ?>"
                    >
                        <span class="glyphicon glyphicon-remove"></span>
                        <?php echo JText::_('COM_STTNMLS_LABEL_DELETE_FROM_ARCHIVE'); ?>
                    </a>
                </div>
            <?php else: ?>
                <!-- Панель управления объектом -->
                <?php $linkedit = JRoute::_('index.php?option=com_sttnmls&view=garage&task=edit&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID, TRUE, -1); ?>
                <div class="sttnmls_photo_manager well well-sm text-right" style="margin-bottom: 2px;">

                    <a href="javascript:void(0);" class="btn btn-info btn-xs changeObjectChangeDate" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHANGE_DATE') ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="garage">
                        <span class="glyphicon glyphicon-time"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?>
                    </a>

                    <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-xs" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT') ?>
                    </a>

                    <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=garage&task=removeObject&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID) ?>" class="btn btn-danger btn-xs" onclick="if(!confirm('<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM') ?>')) return false;">
                        <span class="glyphicon glyphicon-remove"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_DELETE') ?>
                    </a>
                </div>
                <div class="sttnmls_photo_manager well well-sm text-right">
                    <a href="javascript:void(0);" class="btn btn-success btn-xs changeObjectCreateDate" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_FLUSH_CREATE_DATE', $params->get('dateprice', 0)) ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="garage">
                        <span class="glyphicon glyphicon-calendar"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_FLUSH_DATE') ?>
                    </a>

                    <?php echo $this->loadTemplate('object_promotion_topbar_button') ?>
                </div>
                <!-- //Панель управления объектом -->
            <?php endif; ?>
        <?php endif; ?>


        <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 1) : ?>
            <div class="alert alert-warning">
                <?php echo JText::_('COM_STTNMLS_PREMODER_'); ?>
            </div>
        <?php endif; ?>

            
            <h1>
                <?php echo $this->get('Head'); ?>
            </h1>
        <?php if($this->activeComplaints) : ?>
            <p class="lead">
                <a href="#complaints" class="label label-danger">
                    [<?php echo JText::_('COM_STTNMLS_LABEL_COMPLAINTS_EXIST_ACTIVE') ?>]
                </a>
            </p>
        <?php endif; ?>

            <a href="#YMapsID" class="object-address-link" style="color: #777;">
            <?php
                if($this->item->gorod == $kursk) {
                    echo $kursk;
                } else if($this->item->gorod == $region) {
                    echo $region;
                } else {
                    echo $this->item->gorod;
                }
                if($this->item->raion != '' && $this->item->RAIONID > 1) {
                    $r = str_replace('  ', ' ', $this->item->raion);
                    $r = mb_ereg_replace('/(р-н|район)+/gi', 'р-н', $this->item->raion);
                    echo ', ' . $r;
                }
                if($this->item->mraion != '') {
                    echo ', микрорайон ' . $this->item->mraion;
                }
            ?>
            </a>&nbsp;
            <a href="#YMapsID" class="object-address-link">
                На карте
            </a>
            
            <p class="text-justify">
                <span>
                    <strong><?php echo $this->item->tip ?></strong>
                    <br />
                    <?php
                        echo $this->item->gorod . ', ' . $this->item->raion . ', ' . $this->item->ulica;
                    ?>
                </span>
                <?php if($this->item->EXCLUSIVE > 0): ?>
                    <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/icon_rating.png" />
                <?php endif; ?>
            </p>
            
        <?php if($this->item->mdays <= $this->item->alldays OR $this->userperm == 1 OR $this->userperm > 2) : ?>
            <h3><?php echo JText::_('COM_STTNMLS_DOP'); ?>:</h3>
            <p class="text-justify">
                <?php echo str_replace('..', '.', $this->item->MISC . '.'); ?>
            </p>
        <?php endif; ?>


        <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 0): ?>
            <p>&nbsp;</p>
        <?php else : ?>
            <div id="dopinfo"<?php echo (($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 2) ? ' class="hide"' : '') ?>>
                <h3><?php echo JText::_('COM_STTNMLS_DOP'); ?>:</h3>
                <p class="text-justify">
                    <?php echo str_replace('..', '.', $this->item->MISC . '.'); ?>
                </p>
            </div>
        <?php endif; ?>
            
        <?php 
            $showContactInfo = FALSE; 
            if($this->myagent && $this->item->COMPID == $this->myagent->COMPID) {
            // Пользователь - есть член "агентства", владеющего объектом

                // 1. У объекта разрешено отображение контактов собственника
                if($this->item->sob_visible == 1) {
                    $showContactInfo = TRUE;
                }

                // 2. Пользователь - владелец объекта
                if($this->item->AGENTID == $this->myagent->ID) {
                    $showContactInfo = TRUE;
                }

                // 3. Пользователь - руководитель "агентства", владеющего объектом
                if($this->myagent->boss == 1) {
                    $showContactInfo = TRUE;
                }

                $perm = explode(',', $this->agent->prava);
                if(in_array($this->myagent->kod, $perm)) {
                    $showContactInfo = TRUE;
                }
            }
        ?>
          
        <?php if($showContactInfo) : ?>
        <!-- Разрешено отображение контактов собственника -->
            <p>
            <?php if($this->item->sob_tel != ''): ?>
                <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_PHONE') ?>:</strong> <?php echo $this->item->sob_tel; ?><br />
            <?php endif; ?>

            <?php if($this->item->sob_tel2 != ''): ?>
                <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_PHONE_ADDITIONAL') ?>:</strong> <?php echo $this->item->sob_tel2; ?><br />
            <?php endif; ?>

            <?php if($this->item->sob_fio != ''): ?>
                <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_FIO') ?>:</strong> <?php echo $this->item->sob_fio;?>
            <?php endif; ?>
            </p>
        <!-- //Разрешено отображение контактов собственника -->
        <?php endif; ?>
            
            <!--↓Для агентов↓--> 
        <?php if ($currentAgent->COMPID != $compidsob AND !$user->guest) : ?>
			<h3><?php echo JText::_('Для агентов'); ?>:</h3>
			<p class="text-jusify">
				<?php if ($this->item->owners_number):?>
					<span class="realtors_item_title">Число собственников: </span>
					<span class="realtors_item_content"><?php echo $this->item->owners_number;?></span><br/>
				<?php endif;?>
				<?php if ($this->item->encumbrance!==NULL):?>
					<span class="realtors_item_title">Обременение: </span>
					<span class="realtors_item_content"><?php echo (($this->item->encumbrance==1) ? 'есть' : 'нет');?></span><br/>
				<?php endif;?>
				<?php if ($this->item->deposit_amount):?>
					<span class="realtors_item_title">Размер задатка: </span>
					<span class="realtors_item_content"><?php echo number_format($this->item->deposit_amount,0, ' ', ' ') ; ?></span>
					<span class="ruble_realtors">a</span>
					<br/>
				<?php endif;?>
				<?php if ($this->item->rollback_size):?>
					<span class="realtors_item_title">Размер отката: </span>
					<span class="realtors_item_content"><?php echo number_format($this->item->rollback_size,0, ' ', ' ') ; ?></span>
					<span class="ruble_realtors ruble">a</span>
					<br/>
				<?php endif;?>
				<?php if ($this->item->note_for_realtors):?>
					<span class="realtors_item_title">Примечание: </span>
					<!--<hr/>-->
					<span style="font-style: italic;"><?php echo $this->item->note_for_realtors;?></span><br/>
				<?php endif;?>
			</p>
		<?php endif;?>
            <!--↑для агентов↑-->
            
            <div class="sttnmls_price" style="margin-bottom: 10px;">
                <div class="pricecard">
                    <?php echo number_format($this->item->PRICE,0, ' ', ' ') ; ?>
                    <span class="ruble">a</span>
                </div>
                <div class="clearfix"></div>
            </div>
                    
            <div class="sttnmls_published_info" style="margin-bottom: 10px;">
                <div class="calendar">
                    <b><?php echo JText::_('COM_STTNMLS_ADDED'); ?>:</b> <span class="datecard"><?php echo $this->item->datecr ?></span><br />
                    <b><?php echo JText::_('COM_STTNMLS_UPDATED'); ?>:</b> <span class="datecard"><?php echo $this->item->datemd ?></span>
                </div>
                <div class="visitors">
                    <b><?php echo JText::_('COM_STTNMLS_WATCHCNT'); ?>:</b> <span><?php echo $this->item->watchcount ?></span>
                </div>
                <div class="clearfix"></div>
            </div>
        
            <div class="sttnmls_user_actions row" style="margin-bottom: 10px;">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <a href="javascript:void(0);" class="addToFavorite btn btn-warning btn-block" data-type="garages" data-tp="<?php echo $this->tp; ?>" data-cid="<?php echo $this->item->COMPID; ?>" data-cn="<?php echo $this->item->CARDNUM; ?>">В избранное</a>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <a href="javascript:void(0);" role="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#rcallModalForm" data-keyboard="true">Заявка на показ</a>
                </div>

            <?php if($this->_allow_change_state) : ?>
                <div class="col-md-4 col-sm-12 col-xs-12" id="object_state_container">
                    <?php echo $this->loadTemplate('object_check_state_button') ?>
                </div>
            <?php endif; ?>
            </div>
            <div id="rcallModalForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rcallModalLabel" aria-hidden="true">
            <?php echo $this->loadTemplate('rcall_form') ?>    
            </div>
            <hr />
            <div class="sttnmls_contact_info" style="margin-bottom: 10px;">
                <h3><?php echo JText::_('COM_STTNMLS_CONTS'); ?>:</h3>

            <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 0) : ?>
                <div class="alert alert-warning">
                    <?php echo JText::_('COM_STTNMLS_PREMODER'); ?>
                </div>
            <?php else : ?>
                <div id="contacts" <?php echo (($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 2) ? ' class="hide"' : '') ?>>
                    <div class="media">
                        <div class="media-left">
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent' . '&agent_id=' . $this->item->AGENTID . '&comp_id=' . $this->item->COMPID) ?>">
                                <img class="thumbnail" src="<?php echo $this->agthumb; ?>" alt="" />
                            </a>
                        </div>
                        <div class="media-body">
                            <?php
                            $agent = trim($this->item->agentinf);
                            $t = array();
                            if(strpos($agent, ',')) {
                                $t = explode(',', $agent);
                            } else {
                                if(strpos($agent, '<br/>')) {
                                    $t = explode('<br/>', $agent);
                                }
                                if(strpos($agent, '<br>')) {
                                    $t = explode('<br>', $agent);
                                }
                            }
                            if ($t && count($t) > 0) {
                                $t[0] = '<b>' . $t[0] . '</b>';
                            }
                            echo implode('<br />', $t);
                            ?>
                        </div>
                    </div>

                    <div class="media">
                        <div class="media-left">
                            <?php if ($this->item->COMPID == $params->get('compidsob', 0)): ?>
                                <span>
                                    <img class="thumbnail" src="<?php echo $this->item->imgfirm ?>" alt="" />
                                </span>
                            <?php else: ?>
                                <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID) ?>"<?php if($this->item->firm_url != ''){ echo ' target="_blank"'; } ?>>
                                    <img class="thumbnail" src="<?php echo $this->item->imgfirm ?>" alt="" />
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <b><?php echo $this->item->firm ?></b>
                            <br/>
                            <?php echo $this->item->firmphone ?>
                            <br/>
                            <?php echo $this->item->firmphone2 ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


                <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && ($this->userperm == 2 OR $this->userperm == 3)) : ?>
                <div class="sttnmls_moderator">
                    <?php if($this->userperm == 2) : ?>
                    <a href="javascript:void(0);" class="btn btn-warning btn-block btn-lg showModerateContacts">
                        <?php echo JText::_('COM_STTNMLS_SHOWCONT') ?>
                    </a><br />
                    <div class="well well-sm" id="moderform">
                        <form class="editform form-horizontal" action="<?php echo JRoute::_('index.php?option=com_sttnmls&Itemid=' . $menu->id, TRUE, -1) ?>" method="post">
                            <div class="form-group">
                                <label class="control-label col-sm-6 col-md-4">
                                    <?php echo JText::_('COM_STTNMLS_Q1') ?>
                                </label>
                                <div class="col-sm-6 col-md-4">
                                    <?php echo $this->item->q1opt ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-sm-6 col-md-4">
                                    <?php echo JText::_('COM_STTNMLS_Q2') ?>
                                </label>
                                <div class="col-sm-6 col-md-4">
                                    <?php echo $this->item->q2opt ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-6 col-sm-6 col-md-offset-4 col-md-6">
                                    <button type="button" id="objectSendModeratorInfo" class="btn btn-success" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="garage" data-otype="4">
                                        <?php echo JText::_('COM_STTNMLS_SEND'); ?>
                                    </button>
                                </div>
                            </div>
                        </form>                        
                    </div>
                    <?php endif; ?>
                    
                    <?php if($this->userperm == 3) : ?>
                    <?php echo $this->item->listmoder ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            </div>
            <div class="sttnmls_map">
                <div id="YMapsID"></div>
            </div>
            <div class="sttnmls_complaint">
                <a href="javascript:void(0);" class="showComplaintForm  btn btn-danger btn-block"><?php echo JText::_('COM_STTNMLS_SENDOTZ'); ?></a>
                <div id="complaint_form">
                    <?php echo $this->loadTemplate('complaint'); ?>
                </div>
            </div>
            
        <?php if($this->activeComplaints && count($this->activeComplaints) > 0) : ?>
            <div class="sttnmls_complaints_list" style="margin-top: 20px;">                
            <?php foreach($this->activeComplaints as $item) : ?>
                <div>
                    <p><?php echo JText::_('COM_STTNMLS_LABEL_COMPLAINT_REASON_' . $item->rs) ?>:</p>
                    <?php echo $item->desc; ?><br />
                    <b>Проверено:</b> <?php if($item->status==0){ echo "Нет <img style='position:relative; top:4px;' src='http://www.iconsearch.ru/uploads/icons/crystalclear/16x16/edit_remove.png' alt='unapproved'/>"; }else{ echo "Да <img src='http://kurskmetr.ru/administrator/templates/isis/images/admin/icon-16-allow.png' alt='approved'/> [Объект не показывается в общем списке]"; }?><br />
                    <span style="color:#999;"><b>Дата:</b> <?php echo date('d.m.Y H:i:s',strtotime($item->dateinp)); ?></span>
                </div>
            <?php endforeach; ?>
            </div>            
        <?php endif; ?>

            <?php if( $user->authorise('core.admin') OR $this->userperm == 1) : ?>
                <div id="promotion_object" class="promotion-object">
                    <a name="promotion"></a>
                    <?php echo $this->loadTemplate('object_promotion') ?>
                </div>
            <?php endif; ?>
            <div class="sttnmls_socbuttons">
                <?php echo $socbut;?>
            </div>
        </div>
        <!-- /CONTENT -->

    </article>
    
    
    <div class="col-md-12">
        <?php echo $this->loadTemplate('similar_objects') ?>
        
        <div class="sttnmls_navigation text-center" style="margin-top: 20px;padding-top: 10px;border-top: 1px solid #CCC;">
        <?php
            $this->stype = (intval($this->item->VARIANT) == 2) ? 1 : 0;

            $footer = array();
            $footer[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '', 'default', array('view' => 'garages', 'stype' => $this->stype)) . '">' . SttNmlsLocal::TText('COM_STTNMLS_OBL').'</a>';
            if($this->item->gorod != SttNmlsLocal::TText('COM_STTNMLS_OBL')) { $footer[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&city_id=' . $this->item->CITYID, 'default', array('view' => 'garages', 'stype' => $this->stype)) . '">' . $this->item->gorod . '</a>'; }
            if($this->item->raion != '' && $this->item->RAIONID>1){ $footer[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&raion_id=' . $this->item->RAIONID, 'default', array('view' => 'garages', 'stype' => $this->stype)) . '">' . $this->item->raion . JText::_('COM_STTNMLS_RAION_M') . '</a>'; }
            if($this->item->mraion != ''){ $footer[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&mraion_id=' . $this->item->MRAIONID, 'default', array('view' => 'garages', 'stype' => $this->stype)) . '">' . $this->item->mraion . '</a>'; }
            if($this->item->ulica != ''){ $footer[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&street_id=' . $this->item->STREETID, 'default', array('view' => 'garages', 'stype' => $this->stype)) . '">' . $this->item->ulica . '</a>'; }

            echo implode(' &rarr; ', $footer);
        ?>
        </div>
    </div>

<?php if($this->item->flag != 2 OR ($this->item->flag == 2 && $this->item->mdays <= $this->item->alldays)) : ?>
    <div class="fixed-contacts">
        <div class="media">
            <div class="media-left">
                <div class="media-object">
                    <a class="img-thumbnail" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $this->item->AGENTID . '&comp_id=' . $this->item->COMPID) ?>">
                        <img src="<?php echo $this->agthumb; ?>" alt="" />
                    </a>
                </div>
            </div>
            <div class="media-body">
                <?php
                $agent = trim($this->item->agentinf);
                $t = array();
                if(strpos($agent, ',')) {
                    $t = explode(',', $agent);
                } else {
                    if(strpos($agent, '<br/>')) {
                        $t = explode('<br/>', $agent);
                    }
                    if(strpos($agent, '<br>')) {
                        $t = explode('<br>', $agent);
                    }
                }
                if ($t && count($t) > 0) {
                    $t[0] = '<b>' . $t[0] . '</b>';
                }
                isset($t[1]) && ($t[1] = '<a href="tel:'.str_replace('["-", "(", ")", " "]', "", $t[1]).'">'.$t[1].'</a>');
                echo implode('<br />', $t);
                ?>
            </div>
            <div class="media-right">
                <div class="media-object">
                    <?php if ($this->item->COMPID == $params->get('compidsob', 0)): ?>
                        <span>
                        <img class="thumbnail" src="<?php echo $this->item->imgfirm ?>" alt="" />
                    </span>
                    <?php else: ?>
                        <a class="img-thumbnail" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID) ?>"<?php if($this->item->firm_url != ''){ echo ' target="_blank"'; } ?>>
                            <img src="<?php echo $this->item->imgfirm ?>" alt="" />
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>