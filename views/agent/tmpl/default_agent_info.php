<?php defined('_JEXEC') or die('Restricted access');
// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());
 $doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.agent_table_info.js?');
// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);

// VK Applet Widget
$_show_vk_widget = ($params->get('comments.vk_widget_enabled', 0) && $params->get('comments.vk_widget_appId', 0));
if($_show_vk_widget) {
    $vk_widget_appId = $params->get('comments.vk_widget_appId', 0);
    $vk_widget_msg_limit = $params->get('comments.vk_widget_msg_limit', 10);
$js_vk_applet = <<<JS
    VK.init({apiId: {$vk_widget_appId}, onlyWidgets: true});
    VK.Widgets.Comments("vk_comments", {limit: {$vk_widget_msg_limit}, attach: "*"});

JS;
    $doc->addScript('//vk.com/js/api/openapi.js?120');
    $doc->addScriptDeclaration($js_vk_applet);
}

//
$socbut = $params->get('socbut', '');
$gild = $params->get('gild', '');
$gildurl = $params->get('gildurl', '');

$linkfirm = ($this->item->article_url != '') ? $this->item->article_url : SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&agency=' . $this->item->COMPID);

?>
<div class="container-fluid" itemscope itemtype="http://schema.org/Person">
    <!-- ROW #1: Agent info -->
    <div class="row">
        <div class="col-md-12">
            <h1 itemprop="name">
                <?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME) ?>
                <?php if($this->isAdmin):?><span class="badge all-limit"><?php echo JText::sprintf('%d / %d', $this->item->st_cntall, $this->item->limitobj); ?></span><?php endif;?>
            </h1>
            <div class="row">
                <!-- COL #1: Sidebar -->
                <div class="col-sm-4 col-md-3" style="text-align: center;">
                    <p class="text-center">
                        <img class="img-thumbnail avatar" alt="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME) ?>" src="<?php echo $this->avatar ?>" itemprop="image"/>
                    </p>
                    <p class="text-center">
                        <a href="<?php echo 'tel:'.$this->item->PHONE?>" title = "Телефон для связи" class="btn btn-success btn-block"><strong><?php echo $this->item->PHONE ?></strong></a><br/>
                        <?php if($this->item->ADDPHONE and ereg("[^ ]", $this->item->ADDPHONE)) :?>
						<a href="<?php echo 'tel:'.$this->item->ADDPHONE?>" title = "Телефон для связи" class="btn btn-success btn-block"><strong><?php echo $this->item->ADDPHONE ?></strong></a>
						<?php endif;?>
					</p>
					<div style="display: inline-block">
						
						<?php if (ereg("[^ ]", $this->item->SKYPE)) : ?>
							<a href="skype:<?php echo $this->item->SKYPE;?>?call" onclick="return false;" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> в Скайпе" 
							target="_blank"><span class="social skype"></span></a>
						<?php endif;?>
						<?php if (ereg("[^ ]", $this->item->vk)) : ?>
							<a href="<?php echo $this->getSocialHref($this->item->vk, 'vk.com'); ?>" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> во Вконтакте"
							target="_blank"><span class="social vk"></span></a>
						<?php endif;?>
						
						<?php if (ereg("[^ ]", $this->item->ok)) : ?>
							<a href="<?php echo $this->getSocialHref($this->item->ok, 'ok.ru');?>" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> в Одноклассниках"
							target="_blank"><span class="social ok"></span></a>
						<?php endif;?>
						
						<?php if (ereg("[^ ]", $this->item->fb)) : ?>
							<a href="<?php echo $this->getSocialHref($this->item->fb, 'facebook.com');?>" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> в Фейсбуке"
							target="_blank"><span class="social fb"></span></a>
						<?php endif;?>
						
						<?php if (ereg("[^ ]", $this->item->instagram)) : ?>
							<a href="<?php echo $this->getSocialHref($this->item->instagram, 'instagram.com');?>" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> в Инстаграме"
							target="_blank"><span class="social instagram"></span></a>
						<?php endif;?>
						
						<?php if (ereg("[^ ]", $this->item->twitter)) :?>
							<a href="<?php  echo $this->getSocialHref($this->item->twitter, 'twitter.com');?>" 
							title="<?php printf('%s %s %s', $this->item->SIRNAME, $this->item->NAME, $this->item->SECNAME)?> в Твитере"
							target="_blank"><span class="social twitter"></span></a>
						<?php endif;?>
						
						</div>
                    <?php if($this->item->gild == 1 && $gild && $gildurl) : ?>
                        <p class="text-center">
                            <noindex>
                                <a href="<?php echo $gildurl ?>" rel="nofollow" >
                                    <img  src="<?php echo JURI::root() . $params->get('guild_logo', 'components/com_sttnmls/assets/images/kogr.png') ?>" alt="<?php echo $gild ?>" title="<?php echo $gild ?>">
                                </a>
                            </noindex>
                        </p>
                    <?php endif; ?>
                </div>
                <!-- /COL #1: Sidebar -->

                <!-- COL #2: Objects counters -->
                <div class="col-sm-8 col-md-9">
                    <?php $link = JRoute::_('index.php?option=com_sttnmls&view=aparts&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID, TRUE, -1) ?>
                    <div style="color: #365d7e;">
                        <span class="showSellTable showTableLable selected">Продажа
                            <span class=""><?php echo '('.$this->item->st_cntsale.')'; ?></span>
                        </span>
                        <span class="showRentTable showTableLable">Аренда
                            <span><?php echo '('.$this->item->st_cntrent.')'; ?></span>
                        </span>
                    </div>
                    <div class="table-responsive">
                        <table class="table" id="newSellTable">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="text-center" style="width: 30px;">&nbsp;</th>
                                    <?php if($this->canViewRealtorsBase) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>">
                                                <span class="glyphicon glyphicon-flag"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
									<?php if($this->canViewExpired) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?>">
                                                <span class="glyphicon glyphicon-time"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?>">
                                                <span class="glyphicon glyphicon-lock"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Квартиры продажа-->
                                <tr id="flatsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID ) ?>">
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>">
                                            <?php echo $this->item->cntkv ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkv_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkv_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkv_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Квартиры аренда-->
                                <tr id="flatsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntkvar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkvar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkvar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkvar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты продажа-->
                                <tr id="roomsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo $this->item->cntkm ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkm_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkm_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkm_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты аренда-->
                                <tr id="roomsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntkmar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkmar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkmar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkmar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома продажа-->
                                <tr id="housesSale">
                                    <td>
                                        <a class="text-center" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" >
                                            <?php echo ($this->item->cnths - $this->item->cntuch) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_realtors_and_open=1') ?>" >
                                                <?php echo ($this->item->cnths_r - $this->item->cntuch_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_expired=1') ?>" >
                                                <?php echo ($this->item->cnths_expired - $this->item->cntuch_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_closed=1') ?>" >
                                                <?php echo ($this->item->cnths_c - $this->item->cntuch_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома аренда-->
                                <tr id="housesRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0' . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0'. '&stype=1') ?>" >
                                            <?php echo ($this->item->cnthsar - $this->item->cntuchar) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_realtors_and_open=1'. '&stype=1') ?>" >
                                                <?php echo ($this->item->cnthsar_r - $this->item->cntuchar_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_expired=1'. '&stype=1') ?>" >
                                                <?php echo ($this->item->cnthsar_expired - $this->item->cntuchar_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_closed=1') ?>" >
                                                <?php echo ($this->item->cnthsar_c - $this->item->cntuchar_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки продажа-->
                                <tr id="landsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo $this->item->cntuch ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntuch_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntuch_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntuch_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки аренда-->
                                <tr id="landsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1' . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1' . '&stype=1') ?>" >
                                            <?php echo $this->item->cntuchar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors_and_open=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи продажа-->
                                <tr id="garagesSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" >
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" >
                                            <?php echo $this->item->cntgr ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntgr_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntgr_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>">
                                                <?php echo $this->item->cntgr_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи аренда-->
                                <tr id="garagesRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntgrar ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors_and_open=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость продажа-->
                                <tr id="comsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" >
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" >
                                            <?php echo $this->item->cntcom ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntcom_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntcom_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" >
                                                <?php echo $this->item->cntcom_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость аренда-->
                                <tr id="comsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntcom1 ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntcomar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntcom1_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntcomar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /COL #2: Objects counters -->
            </div>
        </div>
    </div>
    <!-- /ROW #1: Agent info -->
    <hr />
    <!-- ROW #2: Firm info -->
    <div class="row">
        <div class="col-sm-4 col-md-3 text-center">
            <?php if ($this->item->COMPID == $params->get('compidsob', 0)): ?>
                <span>
                    <img class="img-thumbnail avatar" alt="<?php echo $this->item->firm ?>" src="<?php echo $this->item->imgfirm; ?>" itemprop="image" />
                </span>
            <?php else: ?>
                <a href="<?php echo $linkfirm ?>" target="_blank">
                    <img class="img-thumbnail avatar" alt="<?php echo $this->item->firm ?>" src="<?php echo $this->item->imgfirm; ?>" itemprop="image" />
                </a>
            <?php endif; ?>
        </div>
        <div class="col-sm-8 col-md-9">
            <?php if ($this->item->COMPID == $params->get('compidsob', 0)): ?>
                <span class="h3">
                    <?php echo $this->item->firm ?>
                </span>
            <?php else: ?>
                <a class="h3" href="<?php echo $linkfirm ?>" target="_blank">
                    <?php echo $this->item->firm ?>
                </a>
            <?php endif; ?>
            <p class="text-justify">
                <?php echo ' ' . $this->item->firmaddr . ' ' . $this->item->fphone ?>
            </p>
        </div>
    </div>
    <!-- /ROW #2: Firm info -->
    <hr />
    <!-- ROW #3: Text -->
    <div class="row">
        <div class="col-md-12">
            <p class="text-center">
                <b><i><?php echo SttNmlsLocal::GetText('Вы можете оставить свой отзыв о специалистах по недвижимости kurska на портале portal'); ?></i></b>
            </p>
            <?php if($_show_vk_widget) : ?>
            <div id="vk_comments"></div>
            <?php endif; ?>
            <p style="padding-top: 8px;">
                <?php echo $socbut;?>
            </p>
        </div>
    </div>
    <!-- /ROW #3: Text -->
</div>
