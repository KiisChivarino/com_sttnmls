<?php defined('_JEXEC') or die('Restricted access');

// Подключение хелперов
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$session = JFactory::getSession();

$last_usluga = '';


// Расчет тарификации агента
$pay_by_day = round($this->agent_tariff->mfee / $this->agent_tariff->days, 2);
$pay_by_month = round($pay_by_day * 30);
$total_period = floor($this->balans->balans / $pay_by_day);


?>

<h4><?php echo JText::_('COM_STTNMLS_LABEL_INFO') ?></h4>
<hr />
<?php if($this->item->grp > 0 && $this->agent_tariff) : ?>
    <h5 class="well well-sm">    
        <?php echo JText::sprintf('COM_STTNMLS_LABEL_ACCOUNT_TARIFF', $this->agent_tariff->name) ?><br />
        <small><?php echo JText::sprintf('COM_STTNMLS_LABEL_ACCOUNT_TARIFF_DESC', $pay_by_month, $pay_by_day) ?></small><br />
        <?php if($this->agent_tariff->mfee != 0) : ?>
        <small><?php echo JText::sprintf('COM_STTNMLS_LABEL_ACCOUNT_TARIFF_COUNTDOWN', $total_period); ?></small>
        <?php endif; ?>
    </h5>
    <?php if($this->agent_tariff->fulldesc != '') : ?>
    <p class="text-justify">
        <?php echo $this->agent_tariff->fulldesc ?>
    </p>
    <?php endif; ?>
<?php else : ?>    
    <div class="alert alert-danger">
        <?php echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
    </div>
<?php endif; ?>

<h4 style="margin-top: 50px;"><?php echo JText::_('COM_STTNMLS_LABEL_CHANGE_TARIFF') ?></h4>
<hr />
<?php if($this->tariffs_list) : ?>
<div class="row">
    <?php foreach($this->tariffs_list as $i => $tariff) : ?>
    <div class="col-xs-6 col-sm-4 col-md-3">
        <?php
            $available_panel_class = array(
                    'success',
                    'info',
                    'warning',
                    'default',
                    'primary'
                );
            $color = ((isset($available_panel_class[$i])) ? $available_panel_class[$i] : 'default');
        ?>
        <div class="panel panel-<?php echo $color ?> tariff">
            <div class="panel-heading">
                <h4 class="text-center">
                    <?php echo $tariff->name;?>
                </h4>
            </div>
            <div class="panel-body text-center">
                <p class="lead">
                    <strong>
                        <?php echo JText::sprintf('COM_STTNMLS_LABEL_ACCOUNT_TARIFF_PRICE', $tariff->mfee) ?>
                    </strong>
                </p>
            </div>
            <ul class="list-group list-group-flush text-center">
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_6', $tariff->limitobj) ?>
                </li>
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_1', $tariff->days) ?>
                </li>
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_2', $tariff->mdays) ?>
                </li>
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_3', $tariff->points) ?>
                </li>
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_4', $tariff->seecont) ?><br />
                    <span class="label label-<?php echo (($tariff->seecont) ? 'success' : 'danger') ?>">
                        <span class="glyphicon <?php echo (($tariff->seecont) ? 'glyphicon-ok' : 'glyphicon-remove') ?>"></span>
                    </span>
                </li>
                <li class="list-group-item">
                    <?php echo JText::sprintf('COM_STTNMLS_LABEL_TARIFF_OPTION_5', $tariff->seexpired) ?><br />
                    <span class="label label-<?php echo (($tariff->seexpired) ? 'success' : 'danger') ?>">
                        <span class="glyphicon <?php echo (($tariff->seexpired) ? 'glyphicon-ok' : 'glyphicon-remove') ?>"></span>
                    </span>
                </li>
            </ul>
            <div class="panel-footer">
            <?php if($this->agent_tariff) : ?>  
                <?php if(round($this->item->balance) >= $tariff->mfee) : ?>
                    <?php if($tariff->mfee == 0) : ?>
                    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-commercial-services&act=buy&id=' . $tariff->id . '&kod=' . $this->item->kod, 'profile') ?>" class="btn btn-success btn-block">
                        <span class="glyphicon glyphicon-check"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_SELECT') ?>
                    </a>
                    <?php else : ?>
                    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-commercial-services&act=buy&id=' . $tariff->id . '&kod=' . $this->item->kod, 'profile') ?>" class="btn btn-success btn-block">
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_BUY') ?>
                    </a>
                    <?php endif; ?>
                <?php else : ?>
                    <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=robokassa&layout=default');?>" class="btn btn-warning btn-block">
                        <span class="glyphicon glyphicon-plus"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_REFILL') ?>
                    </a>
                    <br />
                    <div class="well well-sm text-center">
                        <?php echo JText::_('COM_STTNMLS_LABEL_NO_MONEY') ?>
                    </div>
                <?php endif; ?>
                
            <?php else : ?>    
                <?php if($tariff->mfee > 0) : ?>
                <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-commercial-services&act=buy&id=' . $tariff->id . '&kod=' . $this->item->kod, 'profile') ?>" class="btn btn-success btn-block">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <?php echo JText::_('COM_STTNMLS_BUTTON_BUY') ?>
                </a>
                <?php else : ?>
                <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-commercial-services&act=buy&id=' . $tariff->id . '&kod=' . $this->item->kod, 'profile') ?>" class="btn btn-success btn-block">
                    <span class="glyphicon glyphicon-check"></span>
                    <?php echo JText::_('COM_STTNMLS_BUTTON_SELECT') ?>
                </a>
                <?php endif; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>


<div style="margin-top: 20px;">
    <?php echo $params->get('desc_plat','');?>
</div>

<h4><?php echo JText::_('COM_STTNMLS_LABEL_COMMERCIAL_SERVICES_IN_USE') ?></h4>
<hr />
<?php if($this->item->platusl) : ?>
<ul class="list-group">
    <?php foreach($this->item->platusl as $item) : ?>
    <?php if($last_usluga != $item->usluga) :?>
    <li class="list-group-item" id="u_<?php echo $item->id ?>">
        <span class="badge"><?php echo date('d.m.Y H:i:s',strtotime($item->dateinp)); ?></span>
        <b><?php echo JText::_('COM_STTNMLS_PLATUSL_' .  strtoupper($item->usluga)); ?></b><br />
        <?php echo SttNmlsPlatusl::getText($item); ?><br />
        <div class="text-right">
            <a href="javascript:void(0);" class="btn btn-danger btn-sm doServiceOff" data-uid="<?php echo $item->id ?>">
                <span class="glyphicon glyphicon-remove"></span>
                <?php echo JText::_('COM_STTNMLS_PLATUSL_OFF'); ?>
            </a>
        </div>
    </li>
    <?php $last_usluga = $item->usluga ?>
    <?php endif; ?>
    <?php endforeach; ?>
</ul>
<?php else : ?>
    <?php echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
<?php endif; ?>
