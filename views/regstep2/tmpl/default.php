<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<script type="text/javascript">
window.addEvent('domready', function(){
		document.formvalidator.setHandler('sttnum', function(value) {
			var v=value.replace(',', '.');
			if(parseFloat(v)==0) return false;
			regex=/^-?\d+((\.|\,)\d+)?$/;
			return regex.test(v);
		}
		)
	}
);
</script>
<?php if($this->realtor==2){ ?>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#nofirm').attr('checked','checked');
	jQuery('#jformcatf').change(function() {
		cat=jQuery('#jformcatf').val();
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_sttnmls&task=ajax_firmlist&format=raw",
			data: {cat: cat}
		}).done(function( data ) {
			jQuery("#firmlist").html(data);
			jQuery('#nofirm').attr('checked','checked');
			jQuery('#jformcompid').change(nofirm);
		});		
	});
	jQuery('#jformcompid').change(nofirm);
	function nofirm(){
		if(jQuery('#jformcompid').val()==0)
			jQuery('#nofirm').attr('checked','checked');
		else
			jQuery('#nofirm').removeAttr('checked');		
	}
});
</script>
<?php } ?>
<div class="sttnmls">
	<H2>
		<span class="clr1">Регистрация,</span><span class="clr2"> Шаг №2.  
		<?php if($this->realtor==1){ 
			$pictclass="realtor2b";
			$str1 = SttNmlsLocal::TText('COM_STTNMLS_AG1DESC');?>
			<?php echo JText::_('COM_STTNMLS_IREALTOR'); ?>
			
		<?php } else if($this->realtor==0) { 
			$pictclass="realtor1b";
			$str1 = JText::_('COM_STTNMLS_AG2DESC');?>
			<?php echo JText::_('COM_STTNMLS_ISOB'); ?>
			
		<?php } else if($this->realtor==2) { 
			$pictclass="realtor3b";
			$str1 = JText::_('COM_STTNMLS_AG3DESC');?>
			<?php echo JText::_('COM_STTNMLS_ISPEC'); ?>
			
		<?php } ?>
		</span>
	</H2>
	<form id="member-registration" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate">
		<input type="hidden" name="jform[userid]" id="jform_name" value="<?php echo $this->userid; ?>"/>
		<input type="hidden" name="jform[email]" value="<?php echo $this->email; ?>"/>
		<input type="hidden" name="jform[realtor]" id="jform_profile_realtor" value="<?php echo $this->realtor; ?>">
		<input type="hidden" name="tokenreg" value="<?php echo $this->token; ?>">
		<div><?php echo $str1; ?></div>
		<div class="regf1">
			<div class="regf2">
				<?php if($this->realtor==1 || $this->realtor==2){ ?>
					<div class="reginput">
						<label id="jform_fam-lbl" for="jform_fam" class="hasTip required width100" title="">
							<?php echo JText::_('COM_STTNMLS_AGFAM'); ?><span class="star">*</span>
						</label>
						<input type="text" name="jform[fam]" class="required" id="jform_fam" value="" size="30">
					</div>
				<?php } ?>
				<div class="reginput">
					<label id="jform_name-lbl" for="jform_name" class="hasTip required width100" title="">
						<?php echo JText::_('COM_STTNMLS_AGNAME'); ?><span class="star">*</span>
					</label>
					<input type="text" name="jform[name]" class="required" id="jform_name" value="" size="30">
				</div>
                
                <?php if ($this->realtor!=0){ ?>
                
				<div class="reginput">
					<label id="jform_name2-lbl" for="jform_name2" class="hasTip required width100" title="">
						<?php echo JText::_('COM_STTNMLS_AGNAME2'); ?><span class="star">*</span>
					</label>
					<input type="text" name="jform[name2]" class="required" id="jform_name2" value="" size="30">
				</div>
                <?php
				}else{
				?>
                <div class="reginput" style="display:none;">
					<label id="jform_name2-lbl" for="jform_name2" class="hasTip required width100" title="">
						<?php echo JText::_('COM_STTNMLS_AGNAME2'); ?><span class="star">*</span>
					</label>
					<input type="text" name="jform[name2]" class="required" id="jform_name2" value=" " size="30">
				</div>
                
                <?php
				}
				?>
                
                
				<div class="reginput">
					<label id="jform_phone-lbl" for="jform_phone" class="hasTip required width100" title="">
						<?php echo JText::_('COM_STTNMLS_AGPHONE'); ?><span class="star">*</span>
					</label>
					<input type="text" name="jform[phone]" class="required" id="jform_phone" value="" size="30">
				</div>
				<?php if($this->realtor==2){ ?>
					<div class="reginput">
						<label id="jform_catf-lbl" for="jformcatf" class="hasTip required width100" title="">
							<?php echo JText::_('COM_STTNMLS_CATF'); ?>
						</label>
						<?php echo $this->catf; ?>
					</div>
				<?php } ?>
				<?php if($this->realtor==1 || $this->realtor==2){ ?>
					<div class="reginput">
						<label id="jform_compid-lbl" for="jformcompid" class="hasTip <?php if($this->realtor==1){ ?>required <?php } ?> width100" title="">
							<?php echo JText::_('COM_STTNMLS_AGENC'); ?><?php if($this->realtor==1){ ?><span class="star">*</span><?php } ?>
						</label>
						<div id="firmlist">
							<?php echo $this->firms; ?>
						</div>
					</div>
				<?php } ?>
				<?php if($this->realtor==2){ ?>
				<div class="reginput" style="padding-left:30px;">
					<input type="checkbox" id="nofirm" name="jform[nofirm]"><?php echo JText::_('COM_STTNMLS_NOFIRM'); ?></input>
				</div>
				<?php } ?>
				<div class="reginputs">
					<div style="padding-top:10px;"><input type="submit" class="validate sbgreen" value="Зарегистрироваться"/></div>
					<input type="hidden" name="option" value="com_sttnmls" />
					<input type="hidden" name="task" value="regcomplete" />
					<?php echo JHtml::_('form.token');?>
				</div>
			</div>
			<div class="<?php echo $pictclass;?>">
			</div>
		</div>
	</form>

</div>
