<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>
<div class="com_sttnmls row">
<?php echo $this->loadTemplate((JRequest::getString('format', 'raw') == 'raw') ? 'print' : 'normal'); ?>
</div>