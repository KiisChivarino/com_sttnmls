<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
$plugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha', 0));
if($plugin) {
	$captcha = JCaptcha::getInstance($plugin);
}
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<div class="sttnmls"> 
	<h3><?php echo JText::_('COM_STTNMLS_SBERS_HEAD'); ?></h3>
	<form action="<?php echo JRoute::_('index.php?option=com_sttnmls&view=sber',false); ?>" class="editform form-validate" method="post" name="adminForm" id="adminForm">
		<input type="hidden" name="jform[id]" id="jform_id" value="0">
		<input type="hidden" name="task" value="savesber">
		<?php echo JHtml::_('form.token');?>
		<div class="editb2" style="width:500px;">
				<b><?php echo JText::_('COM_STTNMLS_FIO'); ?></b>
				<input type="text" id="jform_fio" name="jform[fio]" value="" size="50" class="required" />
		</div>
		<div class="clear"></div>
		<div class="editb2" style="width:500px;">
				<b><?php echo JText::_('COM_STTNMLS_PHONE'); ?></b>
				<input type="text" id="jform_phone" name="jform[phone]" value="" size="50" class="required" />
		</div>
		<div class="clear"></div>
		<?php if(isset($captcha)){ ?>
			<div class="editb2" style="min-height: 40px;">
				<b style="width:250px;"><?php echo JText::_('COM_STTNMLS_ANTISPAM2'); ?></b>
				<?php echo $captcha->display('captcha', 0); ?>
			</div>
			<div class="clear"></div>
		<?php } ?>
		
		<label id="jform_fio-lbl" for="jform_fio" class=""><?php echo JText::_('COM_STTNMLS_SBER_ERROR_FIO'); ?></label>
		<label id="jform_phone-lbl" for="jform_phone" class=""><?php echo JText::_('COM_STTNMLS_SBER_ERROR_PHONE'); ?></label>
		<input name="save" type="submit" class="button validate sbgreen" style="padding:5px 10px !important;" value="<?php echo JText::_('COM_STTNMLS_SEND'); ?>"/>
	</form>

</div>
