<?php defined('_JEXEC') or die('Restricted access');

class SttnmlsImageObject
{
    public $original_filename = '';
    public $output_filename = '';
    public $full_store_path = '';
    public $url_path = '';
    public $width = 0;
    public $height = 0;
    public $alt = '';
    public $title = '';
    public $thumb_output_filename = '';
    public $thumb_full_store_path = '';
    public $thumb_url_path = '';
    public $thumb_width = 0;
    public $thumb_height = 0;
}

class SttnmlsHelperImage
{
    private static $instance = NULL;

    private static $IMAGE_FILE_EXT = array(
                        1       =>  '.gif',
                        2       =>  '.jpg',
                        3       =>  '.png'
                    );


    protected $use_prefix_as_imagename = TRUE;
    protected $create_thumb = FALSE;

    protected $prefix_name = '';
    protected $thumb_prefix_name = 'thumb_';

    protected $store_path = 'images/';
    protected $thumb_store_path = 'images/';

    protected $width = 640;
    protected $thumb_width = 64;

    protected $height = 480;
    protected $thumb_height = 64;

    protected $types = array(2);            // 1 -gif, 2 - jpg, 3 - png
    protected $max_file_size = 0;   // 5мб
    protected $resize = FALSE;


    private $_errors = array();

    private function __construct() {}
    private function __clone() {}


    public static function getInstance()
    {
        if(NULL === self::$instance) {
            self::$instance = new self();
        }

        self::$instance->max_file_size = JComponentHelper::getParams('com_sttnmls')->get('max_upload_image_file_size', 8192) * 1024;

        return self::$instance;
    }

    public function setConfig($config = array())
    {
        if($config && count($config) > 0) {
            foreach ($config as $var => $val) {
                if(isset($this->$var)) {
                    $this->$var = $val;
                }
            }
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

    
    /**
     * Консолидирует ошибки
     * 
     * @param   string  $error_msg_alias - алиас ошибки из языкового файла
     * @return  boolean
     */
    private function raiseEx()
    {   
        $args = func_get_args();
        $error_msg_alias = array_shift($args);
        
        if(count($args) > 0) {
            $this->_errors[] = vsprintf(JText::_($error_msg_alias), $args);
        } else {
            $this->_errors[] = JText::_($error_msg_alias);
        }
        
        return TRUE;
    }
    
    public function getErrors() {
        return $this->_errors;
    }
    
    /**
     * Загрузка картинок
     * 
     * @param   string      $block - название инпута из которого загружается картинка
     * @return              boolean|\SttnmlsImageObject
     */
    public function upload($block = 'files')
    {
        $result = array();
        
        // Проверка передачи файлов --------------------------------------------
        if(!$_FILES OR !isset($_FILES[$block]) OR !isset($_FILES[$block]['name'])) {
        // Файлы не передавались
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_FILES_UPLOADED');
            return FALSE;
        }
        $images_count = count($_FILES[$block]['name']);

        // Проверка директории хранения файла
        $image_full_path = JPATH_ROOT . '/' . $this->store_path;
        if(!is_dir($image_full_path)) {
            // Попытка создания директориии
            try {
                mkdir($image_full_path);
            } catch (Exception $e) {
                $this->raiseEx($e->getMessage());
                return FALSE;
            }
        }

        // Проверка директории хранения миниатюр, если миниатюра должна быть создана
        $thumb_image_full_path = JPATH_ROOT . '/' . $this->thumb_store_path;
        if($this->create_thumb && !is_dir($thumb_image_full_path)) {
            // Попытка создание директории хранения миниатюр
            try {
                mkdir($thumb_image_full_path);
            } catch (Exception $e) {
                $this->raiseEx($e->getMessage());
                return FALSE;
            }

        }

        for($i = 0; $i < $images_count; $i++)
        {
            // Проверка загружаемого файла
            if(!isset($_FILES[$block]['name'][$i])) {
            // Имя файла не установлено
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_WHILE_UPLOAD_FILE');
                continue;
            }

            if(trim($_FILES[$block]['name'][$i]) == '') {
                continue;
            }
            
            // Проверка размера файла
            if($_FILES[$block]['size'][$i] > $this->max_file_size) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_FILE_TOO_BIG', $_FILES[$block]['name'][$i], round($this->max_file_size / 1024));
                continue;
            }
            
            // Получение размера изображения и определение его типа
            $image_size = getimagesize($_FILES[$block]['tmp_name'][$i]);
            if(!$image_size) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_FILE_CORRUPTED', $_FILES[$block]['name'][$i]);
                continue;
            }
            
            // Проверка разрешенности формата
            if(!in_array($image_size[2], $this->types)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_UNSUPPORTED_FILE_FORMAT', $_FILES[$block]['name'][$i]);
                continue;
            }

            // Подготовка имени файла и пути
            if($this->use_prefix_as_imagename && $images_count == 1 && $this->prefix_name != '') {
                $store_image_name = $this->prefix_name . self::$IMAGE_FILE_EXT[$image_size[2]];
                $thumb_store_image_name = $this->thumb_prefix_name . self::$IMAGE_FILE_EXT[$image_size[2]];
            } else {
                $store_image_name = (($this->prefix_name) ? $this->prefix_name . '_' : '') . md5(uniqid()) . self::$IMAGE_FILE_EXT[$image_size[2]];
                $thumb_store_image_name = (($this->thumb_prefix_name) ? $this->thumb_prefix_name . '_' : '') . md5(uniqid()) . self::$IMAGE_FILE_EXT[$image_size[2]];
            }

            // Удаление существующего файла картинки
            $full_store_path = $image_full_path . $store_image_name;
            if(file_exists($full_store_path)) {
                try {
                    unlink($full_store_path);
                } catch (Exception $e) {
                    $this->raiseEx($e->getMessage());
                    return FALSE;
                }
            }

            // Удаление существующего файла миниатюры
            $thumb_full_store_path = $thumb_image_full_path . $thumb_store_image_name;
            if(file_exists($thumb_full_store_path)) {
                try {
                    unlink($thumb_full_store_path);
                } catch (Exception $e) {
                    $this->raiseEx($e->getMessage());
                    return FALSE;
                }
            }

            try {
                // Если требуется создание миниатюры
                if($this->create_thumb) {
                    $this->resizeImage($_FILES[$block]['tmp_name'][$i], $this->thumb_width, $this->thumb_height, $thumb_full_store_path);
                    $this->cropImage($thumb_full_store_path, 0, 0, $this->thumb_width, $this->thumb_height, $thumb_full_store_path);
                }

                // Если необходимо масштабирование картинки
                if($this->resize) {
                    if($this->resizeImage($_FILES[$block]['tmp_name'][$i], $this->width, $this->height, $full_store_path)) {
                        unlink($_FILES[$block]['tmp_name'][$i]);
                    }
                } else {
                    move_uploaded_file($_FILES[$block]['tmp_name'][$i], $full_store_path);
                }
            } catch (Exception $e) {
                $this->raiseEx($e->getMessage());
                return FALSE;
            }
            
            $image_size = getimagesize($full_store_path);
            $thumb_image_size = (($this->create_thumb) ? getimagesize($thumb_full_store_path) : array());
                        
            $image_object = new SttnmlsImageObject();
            $image_object->original_filename = $_FILES[$block]['name'][$i];
            $image_object->output_filename = $store_image_name;
            $image_object->thumb_output_filename = $thumb_store_image_name;
            $image_object->full_store_path = $full_store_path;
            $image_object->thumb_full_store_path = $thumb_full_store_path;
            $image_object->url_path = JURI::root() . $this->store_path . $store_image_name;
            $image_object->thumb_url_path = JURI::root() . $this->thumb_store_path . $thumb_store_image_name;
            $image_object->width = $image_size[0];
            $image_object->height = $image_size[1];
            $image_object->thumb_width = (($this->create_thumb) ? $thumb_image_size[0] : 0);
            $image_object->thumb_height = (($this->create_thumb) ? $thumb_image_size[1] : 0);
            
            $result[] = $image_object;
        }
        return $result;
    }
    
    /**
     * Функция изменения размера картинки (ресайзинг)
     * 
     * @param   string      $src_image
     * @param   uint        $w
     * @param   uint        $h
     * @param   string      $dst_image
     * @return  boolean
     */
    private function resizeImage($src_image, $w, $h, $dst_image) 
    {
        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD');
            return FALSE;
        }

        // Получение информации об обрабатываемой картинке
        list($src_width, $src_height, $src_type) = getimagesize($src_image);
        switch ($src_type) {
            case 1: $im = imagecreatefromgif($src_image); break;
            case 2: $im = imagecreatefromjpeg($src_image);  break;
            case 3: $im = imagecreatefrompng($src_image); break;
            default: return FALSE;
        }
        
        // Изображение меньше, чем указанное для ресайза
        if ($src_width <= $w && $src_height <= $h) {
            // Да - не изменяем его размер
            $nHeight = $src_height;
            $nWidth = $src_width;
        } else {
            $nWidth = $w;
            $nHeight = $src_height * ($w / $src_width);

        // Нет - вычисление размера с учетом пропорций
//            if (($w / $imgInfo[0]) < ($h / $imgInfo[1])) {
//            // Вычисление, если ширина больше высоты
//                $nWidth = $w;
//                $nHeight = $imgInfo[1] * ($w / $imgInfo[0]);
//            } else {
//            // Вычисление для остальных случаев
//                $nWidth = $imgInfo[0] * ($h/$imgInfo[1]);
//                $nHeight = $h;
//            }
        }
        // Округление до целых чисел значений ширины и высоты
        $nWidth = round($nWidth);
        $nHeight = round($nHeight);

        // Создание "полотна" под картинку
        $newImg = imagecreatetruecolor($nWidth, $nHeight);
        
        // Если картинка формата GIF или PNG, то устанавливается прозрачность, если нужно
        if(($src_type == 1) OR ($src_type == 3)) {
            imagealphablending($newImg, FALSE);
            imagesavealpha($newImg, TRUE);
            $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
            imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
        }
        // Создание картинки с новыми параметрами - ресайзинг
        imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $src_width, $src_height);
        
        // Рендеринг картинки в нужном формате
        switch ($src_type) {
            case 1: imagegif($newImg, $dst_image); break;
            case 2: imagejpeg($newImg, $dst_image, 100);  break;
            case 3: imagepng($newImg, $dst_image); break;
            default: 
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_RESIZE_FAILED');
                return FALSE;
        }
        
        return TRUE;
    }

    /**
     * Функция кропинга изображений
     *
     * @param       resource    $src_image - Картинка источник
     * @param       int         $x - координата Х относительно левого верхнего угла
     * @param       int         $y - координата Y относительно левого верхнего угла
     * @param       int         $dst_width - ширина конечного изображения
     * @param       int         $dst_height - высота конечного изображения
     * @param       string      $dst_image - путь к конечному файлу изображения
     * @return      bool
     */
    private function cropImage($src_image, $x, $y, $dst_width, $dst_height, $dst_image)
    {
        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD');
            return FALSE;
        }

        // Получение информации об обрабатываемой картинке
        list($src_width, $src_height, $src_type) = getimagesize($src_image);
        switch ($src_type) {
            case 1: $im = imagecreatefromgif($src_image); break;
            case 2: $im = imagecreatefromjpeg($src_image);  break;
            case 3: $im = imagecreatefrompng($src_image); break;
            default: return FALSE;
        }

        // Неверные параметры размеров изображения и изображения в целом
        if(!$dst_width OR !$dst_height OR !$src_width OR !$src_height) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_IMAGE_SRC');
            return FALSE;
        }

        // Вычисление размера конечного изображения по оси Х
        // для случая если ширина целевого изображения больше исходного
        //  - уменьшение
        if($x + $dst_width > $src_width) {
            $dst_width = $src_width - $x;
        }

        // Вычисление размера конечного изображения по оси Y
        // для случая если ширина целевого изображения больше исходного
        //  - уменьшение
        if($y + $dst_height > $src_height) {
            $dst_height = $src_height - $y;
        }

        // Создание "полотна" под картинку
        $newImg = imagecreatetruecolor($dst_width, $dst_height);

        // Если картинка формата GIF или PNG, то устанавливается прозрачность, если нужно
        if(($src_type == 1) OR ($src_type == 3)) {
            imagealphablending($newImg, FALSE);
            imagesavealpha($newImg, TRUE);
            $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
            imagefilledrectangle($newImg, 0, 0, $dst_width, $dst_height, $transparent);
        }
        // Создание картинки с новыми параметрами - ресайзинг
        imagecopy($newImg, $im, 0, 0, $x, $y, $dst_width, $dst_height);

        // Рендеринг картинки в нужном формате
        switch ($src_type) {
            case 1: imagegif($newImg, $dst_image); break;
            case 2: imagejpeg($newImg, $dst_image, 100);  break;
            case 3: imagepng($newImg, $dst_image); break;
            default:
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_RESIZE_FAILED');
                return FALSE;
        }

        return TRUE;
    }
    
    /**
     * Функция поворота изображения на заданный угол
     * 
     * @param   string      $src_image - полный путь до картинки
     * @param   integer     $angel - угол поворота. отрицательный - вправо
     * @return  boolean
     */
    public function rotateImage($src_image, $angle = -90) 
    {
        // Проверка установки библиотеки GD для PHP ----------------------------
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD');
            return FALSE;
        }
        
        // Проверка существования исходного файла ------------------------------
        if (!file_exists($src_image)) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_IMAGES');
            return FALSE;
        }
        
        // Получение информации об обрабатываемой картинке ---------------------
        $imgInfo = getimagesize($src_image);
        switch ($imgInfo[2]) {
            case 1: $im = imagecreatefromgif($src_image); break;
            case 2: $im = imagecreatefromjpeg($src_image);  break;
            case 3: $im = imagecreatefrompng($src_image); break;
            default: return FALSE;
        }
        
        // Поворот изображения на указанный угол -------------------------------
        $rotated_image = imagerotate($im, $angle, 0);
        
        // Сохранение повернутой картинки --------------------------------------
        switch ($imgInfo[2]) {
            case 1: imagegif($rotated_image, $src_image); break;
            case 2: imagejpeg($rotated_image, $src_image, 100); break;
            case 3: imagepng($rotated_image, $src_image, 9); break;
            default: return FALSE;
        }
        
        return TRUE;
    }
    
}