<?php
defined('_JEXEC') or die ('Restricted access');

require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

class SttNmlsViewAgent extends JViewLegacy
{
    function display($tpl = null)
    {
        $this->app = JFactory::getApplication();
        $this->doc = JFactory::getDocument();
        $this->user = JFactory::getUser();
        $mAgent = $this->getModel('agent');
        $this->layout = $this->app->input->get('layout', 'default', 'string');

        // Loggined users only
        if(!$this->user->id) {
            jexit();
        }


        switch ($this->layout) {

            case 'profile_home_avatar_edit':
                $this->id = $this->app->input->get('id', '', 'string');
                $this->avatar = $mAgent->getAvatarTempImage($this->id);

                if(!$this->avatar) {
                    jexit();
                }
                break;

        }

        parent::display($tpl);
    }
}