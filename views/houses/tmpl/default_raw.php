<?php defined('_JEXEC') or die('Restricted access');

    foreach ($this->items as $k => $item) {
        $db = JFactory::getDbo();
        $query = 'SELECT * FROM #__sttnmlscompl WHERE status="1" AND typeobj="4" AND compid="' . $item->COMPID . '" AND cardnum="' . $item->CARDNUM . '"';
        $db->setQuery($query);
        $compl = $db->loadObjectList();
        if(count($compl) > 0)
        {
            unset($this->items[$k]);
            $this->countobj--;
        }
    }
    
    $cookie = JFactory::getApplication()->input->cookie;
        $cookie->set('viewType', 0, 0);
        $filter_view_type = SttNmlsHelper::getViewType();
        $listViewParametr =  JRequest::getString('list', false);
    
?>

<div class="<?php echo (($listViewParametr == 1 or $filter_view_type == 1) ? 'row' : 'col-md-12');?>">
    <h1>
    <?php 
        $title = '';
        if(JRequest::getString('titleh1') != '') {
            $title = JRequest::getString('titleh1'); 
        } else {
            $title = JText::_('COM_STTNMLS_HOUSES_HEAD' . $this->stype);
        }
        printf('%s (%d)', $title, $this->countobj);
    ?>
    </h1>
    
    <div class="row-fluid">
        <?php 
        echo $this->loadTemplate('normal_' . (($listViewParametr == 1 or $filter_view_type == 1) ? 'list' : 'standart') . '_view');
        //echo $this->loadTemplate('normal_standart_view'); ?>
    </div>
</div>