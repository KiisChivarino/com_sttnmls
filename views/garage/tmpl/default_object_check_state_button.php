<?php defined('_JEXEC') or die; ?>
<?php if($this->_allow_change_state) : ?>
<a href="javascript:void(0);" role="button" class="setObjectValidateState btn btn-<?php echo (($this->item->checked) ? 'primary' : 'default') ?> btn-block" data-view="garage" data-cid="<?php echo $this->item->COMPID ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-state="<?php echo $this->item->checked ?>">
    <?php if($this->item->checked) : ?>
    <span class="glyphicon glyphicon-ok"></span>
    <?php endif; ?>
    <?php echo JText::_('COM_STTNMLS_LABEL_VALIDATE_OBJECT_BUTTON') ?>
</a>
<?php endif; ?>