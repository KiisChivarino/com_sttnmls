<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;

$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();
?>
<table class="objects-table table table-striped table-hover" data-view="standart">
    <thead>
        <tr>
            <th class="text-center">
            <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_DATA', 'o.DATEINP', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'o.DATEINP') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_PHOTO', 'PICCOUNT', $listDirn, $listOrder); ?>
                
            <?php if($orderCol=='PICCOUNT') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_OBJECTDESC', 'o.AAREA', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'o.AAREA') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center" style="width:100px;">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_PRICE', 'o.PRICE', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'o.PRICE') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th style="width: 33px; padding: 0;"></th>
        </tr>
    </thead>
    <tbody>
<?php if($this->items) : ?>
    <?php foreach ($this->items as $item) : ?>
    <?php
        if($item->isArchiveMode) {
            $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garage', '&view=garage&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID . '&archive=1');
        } else {
            $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garage', '&view=garage&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);
        }
        $linkstreet = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&street_id=' . $item->STREETID);
        $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garage' , '&task=edit&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);

        $style = '';
        if($item->upobj) {
            $style .= ' promotion';
        }
        if($item->expired != 1) {
            $style .= ' expired';
        }
        if($item->EXCLUSIVE == 3) {
            $style .= ' exclusive';
        }
    ?>
        <tr id="<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>" class="object text-center text-middle<?php echo $style ?>">
            <!-- Колонка даты и посещений -->
            <td style="width: 85px;">
                <div>
                    <?php echo SttNmlsHelper::GetRuDate($item->DATEINP); ?>
                </div>

            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="text-center">
                    <a href="javascript:void(0);" class="color-grey changeObjectCreateDate" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_FLUSH_CREATE_DATE', $params->get('dateprice', 0)) ?>" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="garage">
                        <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_FLUSH_DATE') ?>
                    </a>
                </div>
            <?php endif; ?>
                
                <div class="wcnt">
                    <i class="glyphicon icon-wcnt"></i>
                    <?php echo $item->watchcount; ?>
                </div>
                
            <?php if($item->close == 1) : ?>
                <?php //TODO: Что это? ?>
                <div style="text-align:center; padding-top:10px;">
                    <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                        <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>
                    </div>
                </div>
            <?php endif; ?>
			
			<?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 1)) : ?>
				<div style="text-align:center;">                    
                    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&f_realtors=1') ?>" 
                       class="label label-info" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>"
                    ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE');?></a>
                </div>
            <?php endif; ?>
			
			<?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 2)) : ?>
				<div style="text-align:center;">
                    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&f_realtors_and_open=1') ?>" 
                       class="label label-primary" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE') ?>"
                    ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE');?></a>
                </div>
            <?php endif; ?>

            <?php if($item->expired != 1) : ?>
                <div style="text-align:center;">
                    <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                        <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_EXPIRED') ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($item->checked == 1) : ?>
                <div style="text-align:center;">
                    <span class="label label-success">
                        <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHECKED') ?>
                    </span>
                </div>
            <?php endif; ?>

            </td>
            <!-- /Колонка даты и посещений -->
            
            <!-- Колонка фото -->
            <td style="width: 150px;">
                <div class="photo-container">
                    <a href="<?php echo $link; ?>" target="_blank" class="img-thumbnail">
                    <?php 
                        $pics = explode(';', $item->PICTURES);
                        $wm = '';
                        if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
                        $img_url = SttNmlsHelper::getLinkPhoto($pics[0], 'smallcrop' . $wm);
                    ?>
                        <img src="<?php echo $img_url; ?>" alt="" />
                    </a>
                    <?php/* if($item->editable && ($item->flag == 1 OR $item->flag == 2) && !$item->upobj) : ?>
                        <?php $linkedit = JRoute::_('index.php?option=com_sttnmls&view=garage&task=edit&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID, false); ?>
                        <a class="promotion picplat" href="<?php echo $link . '#promotion'; ?>" target="_blank" title="<?php echo JText::_('COM_STTNMLS_PLAT'); ?>">
                            &nbsp;
                        </a>
                    <?php endif; ?>
                    <?php if($item->upobj) : ?>
                        <span class="promotion promoted"></span>
                    <?php endif; */?>
                </div>
            </td>
            <!-- /Колонка фото -->
            
            <!-- Колонка описание -->
            <td class="text-left text-middle">
                <a href="<?php echo $link; ?>" class="objectdesc" target="_blank" title="">
                    <?php echo $item->tip ?>
                </a>
                
                <p class="objectdesc_advanced">
                <?php
                    $s = $item->gorod;
                    if($item->raion != '' && $item->RAIONID>1) {
                        $r = str_replace('р-н', '', $item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($item->mraion != '') {
                        $s .= ', ' . $item->mraion;
                    }
                    echo $s;
                ?>
                    <br />
                    <a href="<?php echo $linkstreet; ?>" style="padding-left:0; font-size:14px;" title="<?php echo JText::_('COM_STTNMLS_ULICA_DESC') ?>">
                        <b><?php echo $item->ulica; ?></b>
                    </a>
                    <br />
                <?php if($item->EXCLUSIVE > 0) : ?>
                    <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/icon_rating.png" style="position:relative; top:2px;"/>
                <?php endif; ?>
                <?php echo $item->firm; ?>
                </p>
            </td>
            <!-- /Колонка описание -->
            
            <!-- Колонка цены -->
            <td>
                <div class="price">
                <?php echo number_format($item->PRICE, 0, ' ', ' '); ?>
                </div>
                
            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <?php if ($item->isArchiveMode): ?>
                    <div class="text-center" style="margin-top: 30px;">
                        <a
                            href="javascript:void(0);"
                            class="btn btn-success btn-xs repairObjectFromArchive"
                            data-oid="<?php echo $item->oid ?>"
                            data-view="garage"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="<?php echo JText::_('COM_STTNMLS_LABEL_REPAIR_FROM_ARCHIVE'); ?>"
                            data-confirm-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM'); ?>"
                        >
                            <span class="glyphicon glyphicon-repeat"></span>
                        </a>

                        <a
                            href="javascript:void(0);"
                            class="btn btn-danger btn-xs removeObjectFromArchive"
                            data-oid="<?php echo $item->oid ?>"
                            data-view="garage"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="<?php echo JText::_('COM_STTNMLS_LABEL_DELETE_FROM_ARCHIVE'); ?>"
                            data-confirm-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM_REMOVE_OBJECT_FROM_ARCHIVE'); ?>"
                        >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                <?php else: ?>
                    <div class="text-center" style="margin-top: 30px;">
                        <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-xs" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>

                        <a href="javascript:void(0);" class="btn btn-info btn-xs changeObjectChangeDate" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHANGE_DATE') ?>" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="garage">
                            <span class="glyphicon glyphicon-time"></span>
                        </a>

                        <a href="javascript:void(0);" class="btn btn-danger btn-xs removeObjectFromList" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="garage">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>    
                
            </td>
            <!-- /Колонка цены -->
            
            <!-- Кололнка избранное -->
            <td>
                <a href="javascript:void(0);" class="addToFavorite" data-tp="<?php echo $this->tp; ?>" data-cid="<?php echo $item->COMPID; ?>" data-type="garages" data-cn="<?php echo $item->CARDNUM; ?>">&nbsp;</a>
            </td>
            <!-- /Кололнка избранное -->
        </tr>
        
    <?php endforeach; ?>
<?php else : ?>
    <?php if($this->noitemtext) : ?>
        <tr>
            <td colspan="5"><?php echo $this->noitemtext; ?></td>
        </tr>
    <?php endif; ?>
<?php endif; ?>
    </tbody>
</table>