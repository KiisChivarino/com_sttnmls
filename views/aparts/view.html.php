<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewAparts extends JViewLegacy
{
    function display($tpl = null)
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $mAparts = JModelLegacy::getInstance('aparts', 'SttnmlsModel');
        $this->params = JComponentHelper::getParams('com_sttnmls');
		$this->user = JFactory::getUser();
        // Данные запроса
        $act = $app->input->get('act', '', 'string');
        $itemid = $app->input->get('Itemid', 0, 'uint');
        $this->raw = (($app->input->getString('format', 'html') == 'raw') ? TRUE : FALSE);

        $this->profile = $mAgent->getProfile(JFactory::getUser()->id);
        $this->access = $this->get('AccessRules');
        $this->items = $this->get('Items');


        if($act == 'pdf') {
            $this->agent = $mAgent->getAgentInfoByID();
            $this->setLayout('default');
            echo $this->loadTemplate('print');
            exit;
        }


        $this->countobj = $this->get('Total');
        $this->filtersList = $this->get('FiltersList');
        $this->filtersGETVarsList = $this->get('FiltersGETVarsList');
        $this->noitemtext = '';
        if(!$this->items) {
            $this->noitemtext =  $this->get('NText');
        }
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->stype = JRequest::getInt('stype',0);
        $this->raions = $this->get('RaionsList');
        $this->mraions = $this->get('MRaionsList');
        $this->cityopt = $this->get('OptCity');
        $this->tp = $this->get('Tp');
        $this->what = JRequest::getInt('what', 0);



        $db = JFactory::getDbo();
        $db->setQuery('SELECT * FROM #__menu WHERE id="' . $itemid . '"');
        $seo = $db->loadObject();
        $seotxt = json_decode($seo->params);
        $meta_dsc = '';
        $meta_key = '';
        $meta_title = '';
        foreach($seotxt as $k=>$v)
        {
            if($k == 'menu-meta_description')
            {
                $meta_dsc = $v;
            }
            if($k == 'menu-meta_keywords')
            {
                $meta_key = $v;
            }
            if($k == 'page_title')
            {
                $meta_title = $v;
            }
        }


        if($meta_dsc!='')
        {
            $document->setDescription($meta_dsc);
        }else{
            if(!$this->stype) {
                $document->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_AP'));
            } else {
                $document->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_AP1'));
            }
        }

        // Если поиск по ID улицы из GET запроса
        $search_by_street = (($app->input->get('street_id', 0)) ? ' - ' . $mAparts->getStreetNameByID($app->input->get('street_id', 0)) : '');

        if($meta_title!='')
        {
            $document->setTitle($meta_title . $search_by_street);
        }else{
            if(!$this->stype) {
                $document->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_AP') . $search_by_street);
            } else {
                $document->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_AP1') . $search_by_street);
            }
        }
        if($meta_key!='')
        {
            $document->setMetadata('keywords', $meta_key);
        }

        parent::display($tpl);
    }
}
