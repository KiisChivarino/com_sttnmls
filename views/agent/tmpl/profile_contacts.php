<?php defined('_JEXEC') or die('Restricted access'); ?>

<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_sttnmls') ?>" method="post" class="form-horizontal form-validate">
    <input type="hidden" name="jform[userid]" id="jform_userid" value="<?php echo $this->user->id; ?>"/>
    <input type="hidden" name="jform[realtor]" id="jform_profile_realtor" value="<?php echo $this->realtor; ?>">
    <input type="hidden" name="option" value="com_sttnmls" />
    <input type="hidden" name="task" value="saveprofile" />
    <?php echo JHtml::_('form.token');?>

    <h4><?php echo JText::_('COM_STTNMLS_LABEL_ACCOUNT_TYPE') ?></h4>
    <hr />
    
    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php
                switch($this->realtor) {
                    case 1: $class = 'image-realtor'; break;
                    case 2: $class = 'image-specialist'; break;
                    default: $class = 'image-sobstvennik'; break;
                }
            ?>
            <div id="account_type_image" class="<?php echo $class ?>"></div>
        </label>
        <div class="col-sm-6 col-md-9" style="padding-top: 20px;">
            <div class="radio">
                <label>
                    <input type="radio" name="jform_profile_realtor" value="1"<?php echo (($this->realtor == 1) ? ' checked="checked"' : '') ?> class="account_type_selector" data-img-class="image-realtor" />
                    <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_REALTOR'))) ?>
                </label>
            </div>

            <div class="radio">
                <label>
                    <input type="radio" name="jform_profile_realtor" value="0"<?php echo ((!$this->realtor) ? ' checked="checked"' : '') ?> class="account_type_selector" data-img-class="image-sobstvennik" />
                    <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SOBSTVENNIK'))) ?>
                </label>
            </div>

            <div class="radio">
                <label>
                    <input type="radio" name="jform_profile_realtor" value="2"<?php echo (($this->realtor == 2) ? ' checked="checked"' : '') ?> class="account_type_selector" data-img-class="image-specialist" />
                    <?php printf('%s - %s', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SPECIALIST'))) ?>
                </label>
            </div>
        </div>
    </div>

    <h4><?php echo JText::_('COM_STTNMLS_LABEL_PROFILE_SETTINGS') ?></h4>
    <hr />

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_LASTNAME') ?>
            <span class="important">*</span>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[fam]" class="form-control required" id="jform_fam" value="<?php echo $this->item->SIRNAME; ?>" placeholder="<?php echo JText::_('COM_STTNMLS_LABEL_LASTNAME') ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_FIRSTNAME') ?>
            <span class="important">*</span>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[name]" class="form-control required" id="jform_name" value="<?php echo $this->item->NAME; ?>" placeholder="<?php echo JText::_('COM_STTNMLS_LABEL_FIRSTNAME') ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_MIDDLENAME') ?>
            <span class="important">*</span>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[name2]" class="form-control required" id="jform_name2" value="<?php echo $this->item->SECNAME; ?>" placeholder="<?php echo JText::_('COM_STTNMLS_LABEL_MIDDLENAME') ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_PHONE') ?>
            <span class="important">*</span>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[phone]" class="form-control required phone" id="jform_phone" value="<?php echo $this->item->PHONE; ?>" placeholder="<?php echo JText::_('COM_STTNMLS_LABEL_PHONE') ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_PHONE_ADDITIONAL') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[addphone]" class="form-control phone" id="jform_addphone" value="<?php echo $this->item->ADDPHONE; ?>" placeholder="">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_ORGANIZATION') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <?php echo $this->firms; ?>
        </div>
    </div>

    <?php if($this->item->seecont) : ?>
    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-6 col-md-offset-3 col-md-6">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="jform[nosend]" value="1" <?php if($this->item->nosend) { echo ' checked="checked"'; } ?> />
                    <?php echo JText::_('COM_STTNMLS_NOSEND'); ?>
                </label>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <h4><?php echo JText::_('COM_STTNMLS_SETUP_REG'); ?></h4>
    <hr />

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_LOGIN') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[login]" id="jform_login" class="form-control required" value="<?php echo $this->user->get('username'); ?>" />
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_LABEL_EMAIL') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[email]" id="jform_email" class="form-control validate-email" value="<?php echo $this->item->EMAIL; ?>" />
        </div>
    </div>
	<!--Социальный профиль-->
	<h4><?php echo JText::_('COM_STTNMLS_SOCIAL_PROFILE'); ?></h4>
    <hr />
	<div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_SKYPE'); ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[SKYPE]" id="jform_login" class="form-control" value="<?php echo $this->item->SKYPE; ?>" />
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_VK') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[vk]" id="jform_vk" class="form-control" value="<?php echo $this->item->vk; ?>" />
        </div>
    </div>
	
	<div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_OK') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[ok]" id="jform_ok" class="form-control" value="<?php echo $this->item->ok; ?>" />
        </div>
    </div>
	
	<div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_FB') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[fb]" id="jform_fb" class="form-control" value="<?php echo $this->item->fb; ?>" />
        </div>
    </div>
	
	<div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_INSTAGRAM') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[instagram]" id="jform_instagram" class="form-control" value="<?php echo $this->item->instagram; ?>" />
        </div>
    </div>
	
	<div class="form-group">
        <label class="control-label col-sm-6 col-md-3">
            <?php echo JText::_('COM_STTNMLS_TWITTER') ?>
        </label>
        <div class="col-sm-6 col-md-4">
            <input type="text" name="jform[twitter]" id="jform_twitter" class="form-control" value="<?php echo $this->item->twitter; ?>" />
        </div>
    </div>

    
    <?php if($this->realtor > 0) : ?>
    <h4><?php echo JText::_('COM_STTNMLS_LABEL_PERMISSIONS_TO_CLOSE_OBJECTS') ?></h4>
    <hr />

    <div class="form-group">
        <label class="control-label col-sm-6 col-md-3" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_SELECT_AGENTS_DESC') ?>">
            <?php echo JText::_('COM_STTNMLS_LABEL_SELECT_AGENTS') ?>
        </label>
        <div class="col-sm-6 col-md-4">
        <?php if($this->firm_agents) : ?>
            <?php $prava = explode(",",$this->item->prava); ?>
            <select multiple="multiple" name="jform[prava][]" id="jform_prava" class="form-control">
            <?php foreach($this->firm_agents as $agent) : ?>
                <?php if($agent->kod == $this->item->kod) { continue; } ?>
                <option value="<?php echo $agent->kod ?>"<?php if(in_array($agent->kod, $prava)){ echo ' selected="selected"'; }?>><?php echo $agent->SIRNAME . ' ' . $agent->NAME . ' ' . $agent->SECNAME; ?></option>
            <?php endforeach; ?>
            </select>
        <?php endif; ?>    
        </div>
    </div>
    <?php endif; ?>

    <hr />

    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-6 col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-success btn-lg">
                <span class="glyphicon glyphicon-ok"></span> 
                <?php echo JText::_('COM_STTNMLS_BUTTON_SAVE') ?>
            </button>
        </div>
    </div>
</form>