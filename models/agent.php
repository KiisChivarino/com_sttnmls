<?php
defined('_JEXEC') or die('Restricted access');

require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
require_once JPATH_COMPONENT_SITE . '/helpers/class.sttnmlsimage.php';

class SttNmlsModelAgent extends JModelLegacy
{
    protected $_errors = array();

    public $_item;

    /**
    * Консолидирует ошибки
    * 
    * @param   string  $error_msg_alias - алиас ошибки из языкового файла
    * @return
    */
    private function raiseEx()
    {   
        $args = func_get_args();
        $error_msg_alias = array_shift($args);
        $app = JFactory::getApplication();
        
        if(count($args) > 0) {
            $this->_errors[] = vsprintf(JText::_($error_msg_alias), $args);
            $app->enqueueMessage(vsprintf(JText::_($error_msg_alias), $args), 'error');
        } else {
            $this->_errors[] = JText::_($error_msg_alias);
            $app->enqueueMessage(JText::_($error_msg_alias), 'error');
        }
        
        return;
    }


    /**
     * Загрузка картинки на сайт
     *
     * @return          bool|SttnmlsImageObject
     */
    public function uploadImage()
    {
        $user = JFactory::getUser();

        // Проверка авторизации
        if(!$user->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE'));
            return false;
        }

        try {
            $config = array(
                'use_prefix_as_imagename' => false,
                'store_path' => 'images/avatar/temp/',
                'width' => 800,
                'height' => 800,
                'create_thumb' => false
            );
            $image = SttnmlsHelperImage::getInstance();
            $image->setConfig($config);
            $result = $image->upload();
            if(!$result) {
                $this->setError($image->getError());
            }
            return $result;
        } catch (Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }
    }

    public function getAvatarTempImage($file_id, $absolute_path = false)
    {
        $temp_path = JPATH_ROOT . '/images/avatar/temp/';

        if(!$file_id OR !file_exists($temp_path . $file_id . '.jpg')) {
            return false;
        }

        if($absolute_path) {
            return $temp_path . $file_id . '.jpg';
        } else {
            return JUri::root() . '/images/avatar/temp/' . $file_id . '.jpg';
        }
    }


    public function addPhoto()
    {
        $viewName = JRequest::getVar('view', 'agent');
        $user_id	= JFactory::getUser()->get('id');

        // Пользователь вошел --------------------------------------------------
        if($user_id) {
        // Пользователь имеет ID
            $config = array(
                'prefix_name'               =>  'av' . $user_id,
                'thumb_prefix_name'         =>  'thumb_av' . $user_id,
                'use_prefix_as_imagename'   =>  true,
                'store_path'                =>  'images/avatar/',
                'thumb_store_path'          =>  'images/avatar/',
                'width'                     =>  800,
                'height'                    =>  800,
                'thumb_width'               =>  64,
                'thumb_height'              =>  64,
                'resize'                    =>  true,
                'create_thumb'              =>  true
            );
            $avatar = SttnmlsHelperImage::getInstance();
            $avatar->setConfig($config);
            $result = $avatar->upload();

            $this->_errors = $avatar->getErrors();
            if(!$result OR $this->_errors) { return FALSE; }

            // Получение ФИО агента
            $db = $this->getDbo();
            $db->setQuery('SELECT SIRNAME, NAME, SECNAME FROM #__sttnmlsvocagents WHERE `userid`=' . $db->quote($user_id));
            $res = $db->loadObject();
        
            $result[0]->alt = $res->SIRNAME . ' ' . $res->NAME . ' ' . $res->SECNAME;           
		
            return $result;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return true;
        }
    }

    public function createAvatar()
    {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $absolute_path = JPATH_ROOT . '/images/avatar/temp/';

        $image = $app->input->get('id', '', 'string');
        $thumb_x = $app->input->get('x', 0, 'uint');
        $thumb_y = $app->input->get('y', 0, 'uint');
        $thumb_w = $app->input->get('w', 0, 'uint');
        $thumb_h = $app->input->get('h', 0, 'uint');

        // Проверка авторизации
        if(!$user->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE'));
            return false;
        }

        // Проверка размеров изображения
        if(!$thumb_w OR !$thumb_h) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_THUMBNAIL_WIDTH_HEIGHT_PARAMS'));
            return false;
        }

        $src_image = $absolute_path . $image . '.jpg';
        if(!$image OR !file_exists($src_image)) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_IMAGE_SRC'));
            return false;
        }


        $avatar = SttnmlsHelperImage::getInstance();

        $config = array(
            'prefix_name'               =>  'av' . $user->id,
            'thumb_prefix_name'         =>  'thumb_av' . $user->id,
            'use_prefix_as_imagename'   =>  true,
            'store_path'                =>  'images/avatar/',
            'thumb_store_path'          =>  'images/avatar/',
            'width'                     =>  250,
            'height'                    =>  250,
            'thumb_width'               =>  64,
            'thumb_height'              =>  64,
            'resize'                    =>  true,
            'create_thumb'              =>  true
        );
        $avatar->setConfig($config);

        if(!$avatar->cropResizeAvatarImage($src_image, $thumb_x, $thumb_y, $thumb_w, $thumb_h)) {
            $this->setError($avatar->getError());
            return false;
        }

        try {
            if(file_exists($src_image)) {
                unlink($src_image);
            }
        } catch(Exception $e) {
        }

        return true;
    }
    
    
    public function getFirmAgentsList($comp_id = 0, $activeOnly = TRUE)
    {
        $result = array();
        if($comp_id > 0) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('ag') . '.*, ' . $db->quoteName('ju.username', 'login'));
            $query->from($db->quoteName('#__sttnmlsvocagents', 'ag'));
            $query->join('LEFT', $db->quoteName('#__users', 'ju') . ' ON(' . $db->quoteName('ag.userid') . '=' . $db->quoteName('ju.id') . ')');
            $query->where($db->quoteName('ag.COMPID') . '=' . $db->quote($comp_id));
            $query->where($db->quoteName('ag.flag') . '<3');
            if($activeOnly) {
                $query->where($db->quoteName('ag.userid') . '>0');
            }
            $query->order($db->quoteName('ag.boss') . ' DESC, ' . $db->quoteName('SIRNAME') . ' ASC');
            $db->setQuery($query);            
            $result = $db->loadObjectList();
            
            if($result) {
                // Подключение хелпера
                require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
                
                foreach($result as $item) {
                    $item->foto = Avatar::getUserThumbAvatar($item->userid);
                    $item->bigFoto = Avatar::getUserAvatar($item->userid);
                }
            }
        }
        return $result;
    }
    
    /**
     * Возвращает информацию по агенту
     * 
     * @var         integer     $user_id - ID конкретного пользователя
     * @return      bool or object
     */
    public function getAgentInfoByID($user_id = 0, $_auto_current_user = true)
    {
        if($_auto_current_user) {
            $user_id = ((!$user_id) ? JFactory::getUser()->id : $user_id);
        }
        
        if(!$user_id) {
            $this->errors[] = 'COM_STTNMLS_MESSAGE_ERROR_AGENT_NOT_FOUND';
            return FALSE;
        }
                
        $db = $this->getDbo();
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('a') . '.*, ' . $db->quoteName('f.name', 'firmname'));
        $query->select($db->quoteName('f.phone', 'firmphone') . ', ' . $db->quoteName('f.addphone', 'firmaddphone'));
        $query->select($db->quoteName('f.email', 'firmemail') . ', ' . $db->quoteName('f.isite', 'firmsite'));
        $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON(' . $db->quoteName('a.compid') . '=' . $db->quoteName('f.id') . ')');
        $query->where($db->quoteName('userid') . '=' . $db->quote($user_id));
        $db->setQuery($query);

        return $db->loadObject();
    }
    
    /**
     * Получение данных об агенте в конкретной организации
     * 
     * @param       integer         $compid - 
     * @param       integer         $agent_id - 
     * @return      bool|object
     */
    public function getAgentByFirmID($compid, $agent_id = 0)
    {
        $agent_id = (($agent_id > 0) ? $agent_id : JFactory::getUser()->id);

        if(!$agent_id) {
            return NULL;
        }
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('a') . '.*, ' . $db->quoteName('f.name', 'firmname'));
        $query->select($db->quoteName('f.phone', 'firmphone') . ', ' . $db->quoteName('f.addphone', 'firmaddphone'));
        $query->select($db->quoteName('f.email', 'firmemail') . ', ' . $db->quoteName('f.isite', 'firmsite'));
        $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON(' . $db->quoteName('a.compid') . '=' . $db->quoteName('f.id') . ')');
        $query->where($db->quoteName('a.kod') . '=' . $db->quote($agent_id));
        $query->where($db->quoteName('a.COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);

        return $db->loadObject();
    }
    
    
    function save()
    {
        $data  = JRequest::getVar('jform', array(), 'post', 'array');
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $userid	= $user->get('id');
        if(!$userid OR !$data) {
            $this->setError( JText::_('COM_STTNMLS_MESSAGE_ERROR_SAVE_DATA'));
            return false;
        }
        
        if($data['password'] && $data['password'] != $data['password2'])
        {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_PASSWORD_NOT_MATCH'));
            return false;
        }
        
        $db = $this->getDbo();
        $db->setQuery(
            'SELECT `profile_value` FROM `#__user_profiles` ' .
            'WHERE `user_id` = ' . $db->quote($userid) . " AND `profile_key` = 'profile.realtor' " .
            'ORDER BY ordering'
        );
        $oldrealtor = $db->loadResult();
        
        if(!$oldrealtor) {
            $oldrealtor = 2;
        }
        
	    $db->setQuery('SELECT `compid` FROM `#__sttnmlsvocagents` WHERE `userid` = ' . $db->quote($userid));
        $oldcompid = $db->loadResult();
                
	    if(!$oldcompid) {
            $oldcompid = $params->get('compidsob', 99003);
        }
                
        // обновим таблицу агентов
        if(!isset($data['fam'])) {
            $data['fam'] = '';
        }
        if(!isset($data['nosend'])) {
            $data['nosend'] = 0;
        }
        $query = $db->getQuery(true);
        $query->update('`#__sttnmlsvocagents`');
        $query->where('`userid`=' . $db->quote($userid));
		
        $ud['prava'] = implode(",", $_REQUEST['jform']['prava']);
        $query->set('`prava`=' . $db->quote($ud['prava']));
        $query->set('`addphone`=' . $db->quote($data['addphone']));
        $query->set('`sirname`=' . $db->quote($data['fam']));
        $query->set('`name`=' . $db->quote($data['name']));
        $query->set('`secname`=' . $db->quote($data['name2']));
        $query->set('`phone`=' . $db->quote($data['phone']));
        $query->set('`email`=' . $db->quote($data['email']));
        $query->set('`nosend`=' . $db->quote($data['nosend']));
        $query->set('`fullinf`=' . $db->quote($data['fam'] . ' ' . $data['name'] . ' ' . $data['name2'] . ',' . $data['phone']));
		$query->set('`SKYPE`=' . $db->quote($data['SKYPE']));
		$query->set('`vk`=' . $db->quote($data['vk']));
		$query->set('`ok`=' . $db->quote($data['ok']));
		$query->set('`fb`=' . $db->quote($data['fb']));
		$query->set('`instagram`=' . $db->quote($data['instagram']));
		$query->set('`twitter`=' . $db->quote($data['twitter']));
                
        $compidsliv = $params->get('compidsliv', '0');
        
        if($data['realtor'] > 0 && $data['compid'] != $oldcompid) {
            //поменялась фирма
            $query->set('`compid`=' . $db->quote($data['compid']));
            if($data['compid'] != $compidsliv) {
                $query->set('`flag`=1');
            }
            
            // отправляем письмо администратору
            $subject = JText::_('COM_STTNMLS_EMAIL_CHANGE_FIRM_SUBJECT');
            $link = JURI::root() . '/administrator/index.php?option=com_sttnmls&view=agents';
            $username = $data['fam'] . ' ' . $data['name'];
            $db->setQuery('SELECT NAME FROM #__sttnmlsvocfirms WHERE ID = '.$db->quote($data['compid']));
            $firmname = $db->loadResult();
            if(!$firmname) {
                $firmname = JText::_('COM_STTNMLS_MESSAGE_ERROR_FIRM_NOT_FOUND');
            }
            $body = JText::sprintf('COM_STTNMLS_EMAIL_CHANGE_FIRM_BODY', $username, $firmname, $link);
            $this->sendmail($subject, $body);
        }
        
        if($data['realtor'] == 0){
            //для собственников своя фирма
            $query->set('`compid`=' . $db->quote($params->get('compidsob', 0)));
        }
        
        $db->setQuery($query);
        $db->query();
        
        // обновим пользователя
        $ud = array();
        $ud['id'] = $userid;
        $ud['email'] = $data['email'];
        if($data['password']){
            $ud['password'] = $data['password'];
            $ud['password2'] = $data['password2'];
        }
        $ud['username'] = $data['login'];
        if(trim($data['fam'])) {
            $ud['name'] = $data['fam'] . ' ' . $data['name'] . ' ' . $data['name2'];
        } else {
            $ud['name'] = $data['name'] . ' ' . $data['name2'];
        }
        $ud['phone'] = $data['phone'];
	
        if($user->bind($ud)){
            $query = $db->getQuery(true);
            $query->update('`#__users`');
            $query->set('`name`=' . $db->quote($user->name));
            $query->set('`username`=' . $db->quote($user->username));
            $query->set('`password`=' . $db->quote($user->password));
            $query->set('`email`=' . $db->quote($user->email));
            $query->where('`id`=' . $db->quote($userid));
            $db->setQuery($query);
            $db->query();
        }        
        
        if($oldrealtor != $data['realtor'])
        {
            $db->setQuery(
                'DELETE FROM #__user_profiles WHERE user_id = ' . $userid .
                " AND profile_key LIKE 'profile.realtor'"
            ); 
            $db->query();
            $tuples = array();
            $order = 1; 
            $tuples[] = '(' . $userid . ', ' . $db->quote('profile.realtor') . ', ' . $db->quote(json_encode($data['realtor'])) . ', ' . $order++ . ')';
            $db->setQuery('INSERT INTO #__user_profiles VALUES ' . implode(', ', $tuples));
            $db->query();
        }
        return TRUE;
    }
    
    /**
     * Возвращает информацию о тарифе
     * 
     * @param   integer     $tariff_id - ID тарифа
     * @return  object
     */
    public function getTariffInfo($tariff_id)
    {
        $result = FALSE;
        
        if($tariff_id) {
            $db = JFactory::getDbo();
            $db->setQuery('SELECT * FROM `#__sttnmlsgroup` WHERE `id`=' . $db->quote($tariff_id));
            $result = $db->loadObject();
        }
        return $result;
    }
    
    /**
     * Возвращает список возможных тарифных планов
     * 
     * @param   bool        $only_active - получить только активные планы
     * @return  array of objects
     */
    public function getTariffsList($only_active = TRUE)
    {
        $db = JFactory::getDbo();
        
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from('`#__sttnmlsgroup`');
        if($only_active) {
            $query->where('`active`="1"');
        }
        $db->setQuery($query);
        return $db->loadObjectList();
    }
    
    
    /**
     * Возвращает информацию о тарифе агента
     * 
     * @param   integer     $user_id - ID пользователя
     * @return  boolean | object
     */
    public function getAgentTariff($user_id = 0)
    {
        $result = FALSE;
        $user_id = (($user_id > 0) ? $user_id : JFactory::getUser()->id);
                
        if($user_id > 0) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('a.grp') . ', ' . $db->quoteName('g') . '.*, ' . $db->quoteName('gb') . '.*, UNIX_TIMESTAMP(' . $db->quoteName('gb.datebuy') . ') AS dtb');
            $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON (' . $db->quoteName('a.grp') . '=' . $db->quoteName('g.id') . ')');
            $query->join('LEFT', $db->quoteName('#__sttnmlsgroupbuy', 'gb') . ' ON (' . $db->quoteName('a.userid') . '=' . $db->quoteName('gb.user_id') . ')');
            $query->where($db->quoteName('a.userid') . '=' . $db->quote($user_id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }
        return $result;
    }
    
    /**
     * Возвращает информационные поля по балансу пользователя
     * 
     * @param   integer     $user_id - [опционально] ID пользователя
     * @return  bool | object
     */
    public function getAgentBalanceInfo($user_id = 0)
    {
        $result = FALSE;
        $user_id = (($user_id > 0) ? $user_id : JFactory::getUser()->id);
        
        if($user_id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from('`#__sttnmlsbalans`');
            $query->where('`userid` = ' . $db->quote($user_id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }
        return $result;
    }
    
    
    
    public function getItem()
    {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $mailfrom = $app->getCfg('mailfrom');
        $fromname = $app->getCfg('fromname');
        $params = JComponentHelper::getParams('com_sttnmls');
        $redirect_url = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile&opentab=tab-commercial-services');

        // Параметры запроса
        $act = $app->input->get('act', '', 'string');
        $id = $app->input->get('id', 0, 'uint');
        $kod = $app->input->get('kod', 0, 'uint');
        $layout = $app->input->get('layout', 'default', 'string');
        $agent_id = $app->input->get('agent_id', 0, 'uint');
        $comp_id = $app->input->get('comp_id', 0, 'uint');


        if($act == 'buy' && (int)$id > 0)
        {
            // Получение информации о новом тарифе
            $new_tariff = $this->getTariffInfo($id);
            if(!$new_tariff) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN_TARIFF');
                $app->redirect($redirect_url);
                return;
            }
            
            // Если тариф отключен от фронтенда - его нельзя использовать
            if($new_tariff->active == 0) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_TARIFF_UNAVALABLE');
                $app->redirect($redirect_url);
                return;
            }
            
            // Работа с балансом пользователя
            $balance = $this->getAgentBalanceInfo($user->id);
            if(!$balance) {
                // Еще не существует записи о балансе пользователя - создать
                $query = $db->getQuery(TRUE);
                $query->insert($db->quoteName('#__sttnmlsbalans'));
                $query->columns($db->quoteName(array('userid', 'balans', 'dateinp')));
                $query->values($db->quote($user->id) . ', ' . $db->quote('0.00') . ', NOW()');
                $db->setQuery($query);
                $db->execute();

                $balance = $this->getAgentBalanceInfo($user->id);
            }
            
            // Обработка ошибок баланса и недостаточности средств на счете
            if(!$balance OR $new_tariff->mfee > $balance->balans) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_MONEY');
                $app->redirect($redirect_url);
                return;
            }
            
                        
            // Выбор добавление/изменение тарифа пользователя
            $current_tariff = $this->getAgentTariff($user->id);
            $query = $db->getQuery(TRUE);

            // Проверка существования записи в таблице купленных групп
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('id'));
            $query->from($db->quoteName('#__sttnmlsgroupbuy'));
            $query->where($db->quoteName('user_id') . '=' . $user->id);
            $db->setQuery($query);
            $hasGroupBuy = $db->loadResult();


            if($hasGroupBuy) {
                $query->update($db->quoteName('#__sttnmlsgroupbuy'));
                $query->set($db->quoteName('datebuy') . '=NOW()');
                $query->set($db->quoteName('datepay') . '=NOW()');
                $query->set($db->quoteName('grp') . '=' . $db->quote($new_tariff->id));
                $query->where($db->quoteName('user_id') . '=' . $db->quote($user->id));
            } else {
                $query->insert($db->quoteName('#__sttnmlsgroupbuy'));
                $query->columns($db->quoteName(array('user_id', 'datebuy', 'datepay', 'grp', 'kodag', 'sendmail')));
                $query->values($db->quote($user->id) . ', NOW(), NOW(), ' . $db->quote($new_tariff->id) . ', ' . $db->quote($kod) . ', 0');
            }
            $db->setQuery($query);
            $db->execute();
            		
            // Получение информации о текущем тарифе пользователя
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName(array('kod', 'EMAIL', 'grp')));
            $query->from($db->quoteName('#__sttnmlsvocagents'));
            $query->where($db->quoteName('kod') . '=' . $db->quote($kod));
            $db->setQuery($query);
            $current_group = $db->loadObject();
            
            $price = round($new_tariff->mfee / 30, 2);
            
            if($current_group->grp != $new_tariff->id) {
                // Обновление информации об активности пользователя
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlsvocagents'));
                $query->set($db->quoteName('activity') . '=' . $db->quoteName('activity') . '+' . $new_tariff->points);
                $query->where($db->quoteName('kod') . '=' . $db->quote($kod));
                $db->setQuery($query);
                $db->execute();
                    
                // Обновление баланса пользователя
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlsbalans'));
                $query->set($db->quoteName('balans') . '=' . $db->quoteName('balans') . '-' . $price);
                $query->where($db->quoteName('userid') . '=' . $db->quote($user->id));
                $db->setQuery($query);
                $db->execute();

                // Логирование операции
                $query = $db->getQuery(TRUE);
                $query->insert($db->quoteName('#__sttnmlsbalanslog'));
                $query->columns($db->quoteName(array('userid', 'summa', 'prim', 'status', 'dateinp')));
                $query->values($db->quote($user->id) . ', ' . $db->quote('-' . $price) . ', ' . $db->quote('Списание за аккаунт ' . $new_tariff->name) . ', ' . $db->quote(1) . ', NOW()');
                $db->setQuery($query);
                $db->execute();




                // Обработка объектов пользователя. Требования по задаче #124

                //-> Квартиры и комнаты
                // Запрашиваем квартиры с лимитом на новый
                // тариф
                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('CARDNUM'));
                $query->from($db->quoteName('#__sttnmlsaparts'));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($user->id * 100));
                $query->where($db->quoteName('flag') . ' IN (1,2)');
                $query->order($db->quoteName('DATECOR') . ' DESC');
                $query->setLimit($new_tariff->limitobj);
                $db->setQuery($query);
                $aparts_list_for_save = $db->loadColumn();

                // Закрываем объекты пользователя за пределами лимита
                // тарифа "реэлтор-халявщик"
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlsaparts'));
                $query->set($db->quoteName('close') . '=' . $db->quote(1));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($user->id * 100));
                $query->where($db->quoteName('flag') . ' IN (1,2)');
                if($aparts_list_for_save) {
                    $query->where($db->quoteName('CARDNUM') . ' NOT IN (' . implode(',', $aparts_list_for_save) . ')');
                }
                $db->setQuery($query);
                $db->execute();


                //-> Дома и участки
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlshouses'));
                $query->set($db->quoteName('close') . '=' . $db->quote(1));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($user->id * 100));
                $query->where($db->quoteName('flag') . ' IN (1,2)');
                $db->setQuery($query);
                $db->execute();

                //-> Коммерческая недвижимость
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlscommerc'));
                $query->set($db->quoteName('close') . '=' . $db->quote(1));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($user->id * 100));
                $query->where($db->quoteName('flag') . ' IN (1,2)');
                $db->setQuery($query);
                $db->execute();

                //-> Гаражи
                $query = $db->getQuery(TRUE);
                $query->update($db->quoteName('#__sttnmlsgarages'));
                $query->set($db->quoteName('close') . '=' . $db->quote(1));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($user->id * 100));
                $query->where($db->quoteName('flag') . ' IN (1,2)');
                $db->setQuery($query);
                $db->execute();
            } else {
                if($current_tariff)
                {
                    $razn = 30-round((time() - $current_tariff->dtb) / 86400);
                    $points = round($new_tariff->points / 30 * (30 - $razn));
                    $query = $db->getQuery(TRUE);
                    $query->update($db->quoteName('#__sttnmlsvocagents'));
                    $query->set($db->quoteName('activity') . '=' . $db->quoteName('activity') . '+' . (int)$points);
                    $query->where($db->quoteName('kod') . '=' . $db->quote($kod));
                    $db->setQuery($query);
                    $db->execute();
                }
            }
			
            if($new_tariff->mail_desc!='')
            {
                $sendadmin = $current_group->EMAIL;
                $body = nl2br($new_tariff->mail_desc);
                if($sendadmin) {
                    $mail = JFactory::getMailer();
                    $mail->isHTML(true);
                    $mail->Encoding = 'base64';
                    $mail->addRecipient($sendadmin);
                    $mail->setSender(array($mailfrom, $fromname));
                    $mail->setSubject("Покупка платного аккаунта");
                    $mail->setBody($body);
                    $mail->Send();
                }
            }
		
            // Обновление информации о тарифе пользователя
            $query = $db->getQuery(TRUE);
            $query->update($db->quoteName('#__sttnmlsvocagents'));
            $query->set($db->quoteName('grp') . '=' . $db->quote($new_tariff->id));
            $query->where($db->quoteName('kod') . '=' . $db->quote($kod));
            $db->setQuery($query);
            $db->execute();
            
            // Редирект на страницу тарифов
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_INFO_TARIFF_SUCCESSFULL_CHANGED'), 'notice');
            $app->redirect($redirect_url);
            return;
        }

                
        if($layout == 'profile' && !$user->id) {
            echo '1';
            return;
        }
        if(!$agent_id && $layout != 'profile') {
            echo '2';
            return;
        }

        if(!$comp_id && $layout != 'profile') {
            echo '3';
            return;
        }
        
        $query = $db->getQuery(true);
        $query->select($db->quoteName('a') . '.*');
        $query->select($db->quoteName('f.NAME', 'firm') . ', ' . $db->quoteName('f.addr', 'firmaddr'));
        $query->select($db->quoteName('f.article_url', 'article_url') . ', ' . $db->quoteName('f.gild', 'gild'));
        $query->select($db->quoteName('f.PHONE', 'fphone') . ', ' . $db->quoteName('f.EMAIL', 'femail'));
        $query->select($db->quoteName('g.days', 'gdays') . ', ' . $db->quoteName('g.mdays', 'mdays'));
        $query->select($db->quoteName('g.limitobj', 'limitobj') . ', ' . $db->quoteName('g.name', 'profilename') . ', ' . $db->quoteName('g.seecont') . ', ' . $db->quoteName('g.seexpired'));
        $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON (' . $db->quoteName('a.COMPID') . '=' . $db->quoteName('f.id') . ')');
        $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON (' . $db->quoteName('g.id') . '=' . $db->quoteName('a.grp') . ')');
        if($layout != 'profile') {
            $query->where($db->quoteName('a.ID') . '=' . $db->quote($agent_id));
            $query->where($db->quoteName('a.COMPID') . '=' . $db->quote($comp_id));
        } else {
            $query->where($db->quoteName('a.userid') . '=' . $db->quote($user->id));
            $query->where($db->quoteName('a.userid') . '> 0');
        }
        $db->setQuery($query);
        $this->_item = $db->loadObject();
        
        if($this->_item)
        {
            //
            require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
		
            $this->_item->imgfirm = Avatar::getLinkFirmLogo($this->_item->COMPID,0);
            $this->_item->st_cntwatch = 0;

            $res = $this->getCnt('aparts', ' VARIANT<>2  AND WHAT=0 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('aparts', ' VARIANT<>2  AND WHAT=0 AND close=0 ', true);
            $this->_item->cntkv = $res->cnt;
            $this->_item->cntkv_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('aparts', ' VARIANT=2  AND WHAT=0 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('aparts', ' VARIANT=2  AND WHAT=0 AND close=0 ', true);
            $this->_item->cntkvar = $res->cnt;
            $this->_item->cntkvar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT>0 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('aparts',' VARIANT<>2  AND WHAT>0 AND close=0 ', true);
            $this->_item->cntkm = $res->cnt;
            $this->_item->cntkm_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT>0 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('aparts',' VARIANT=2  AND WHAT>0 AND close=0 ', true);
            $this->_item->cntkmar = $res->cnt;
            $this->_item->cntkmar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $this->_item->cntar = $this->getCnt('aparts',' VARIANT=2 AND close=0 AND realtors_base<>1 ')->cnt;
            $this->_item->cntar_expired = $this->getCnt('aparts',' VARIANT=2 AND close=0 ', true)->cnt;

            $this->_item->cntcom = $this->getCnt('commerc',' VARIANT<>2 AND close=0 AND realtors_base<>1 ')->cnt;
            $this->_item->cntcom_expired = $this->getCnt('commerc',' VARIANT<>2 AND close=0 ', true)->cnt;

            $this->_item->cntcom1 = $this->getCnt('commerc',' VARIANT=2 AND close=0 AND realtors_base<>1 ')->cnt;
            $this->_item->cntcom1_expired = $this->getCnt('commerc',' VARIANT=2 AND close=0 ', true)->cnt;
            $this->_item->st_cntwatch += $this->getCnt('commerc',' VARIANT=2 AND close=0 AND realtors_base<>1 ')->watch;

            $res = $this->getCnt('commerc',' VARIANT=2 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('commerc',' VARIANT=2 AND close=0 ', true);
            $this->_item->cntcomar = $res->cnt;
            $this->_item->cntcomar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;
			
            $res = $this->getCnt('houses',' VARIANT<>2 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('houses',' VARIANT<>2 AND close=0 ', true);
            $this->_item->cnths = $res->cnt;
            $this->_item->cnths_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('houses',' VARIANT=2 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('houses',' VARIANT=2 AND close=0 ', true);
            $this->_item->cnthsar = $res->cnt;
            $this->_item->cnthsar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('houses',' VARIANT<>2 AND WHAT=1 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('houses',' VARIANT<>2 AND WHAT=1 AND close=0 ', true);
            $this->_item->cntuch = $res->cnt;
            $this->_item->cntuch_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;

            $res = $this->getCnt('houses',' VARIANT=2 AND WHAT=1 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('houses',' VARIANT=2 AND WHAT=1 AND close=0 ', true);
            $this->_item->cntuchar = $res->cnt;
            $this->_item->cntuchar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;
			
            $res = $this->getCnt('garages',' VARIANT<>2 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('garages',' VARIANT=2 AND close=0 ', true);
            $this->_item->cntgr = $res->cnt;
            $this->_item->cntgr_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch;
            
            $res = $this->getCnt('garages',' VARIANT=2 AND close=0 AND realtors_base<>1 ');
            $res_ex = $this->getCnt('garages',' VARIANT=2 AND close=0 ', true);
            $this->_item->cntgrar = $res->cnt;
            $this->_item->cntgrar_expired = $res_ex->cnt;
            $this->_item->st_cntwatch += $res->watch; 
				
			
            $this->_item->st_cntwatch_c=0;
            $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT=0 AND close=1 ');
            $this->_item->cntkv_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT=0 AND close=1 ');
            $this->_item->cntkvar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;

            $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT>0 AND close=1 ');
            $this->_item->cntkm_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT>0 AND close=1 ');
            $this->_item->cntkmar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $this->_item->cntar_c = $this->getCnt('aparts',' VARIANT=2 AND close=1 ')->cnt;

            $res = $this->getCnt('commerc',' VARIANT<>2 AND close=1 ');
            $this->_item->cntcom_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;


            $res = $this->getCnt('commerc',' VARIANT=2 AND close=1 ');
            $this->_item->cntcomar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
			
            $res = $this->getCnt('houses',' VARIANT<>2 AND close=1 ');
            $this->_item->cnths_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('houses',' VARIANT=2 AND close=1 ');
            $this->_item->cnthsar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('houses',' VARIANT<>2 AND WHAT=1 AND close=1 ');
            $this->_item->cntuch_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('houses',' VARIANT=2 AND WHAT=1 AND close=1 ');
            $this->_item->cntuchar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
			
            $res = $this->getCnt('garages',' VARIANT<>2 AND close=1 ');
            $this->_item->cntgr_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
            $res = $this->getCnt('garages',' VARIANT=2 AND close=1 ');
            $this->_item->cntgrar_c = $res->cnt;
            $this->_item->st_cntwatch_c += $res->watch;
			
			
			//↓для базы риэлторов↓
			$this->_item->st_cntwatch_r=0;
            $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT=0 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntkv_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT=0 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntkvar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;

            $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT>0 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntkm_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT>0 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntkmar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $this->_item->cntar_r = $this->getCnt('aparts',' VARIANT=2 AND (realtors_base=1 OR realtors_base=2) ')->cnt;

            $res = $this->getCnt('commerc',' VARIANT<>2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntcom_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;


            $res = $this->getCnt('commerc',' VARIANT=2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntcomar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
			
            $res = $this->getCnt('houses',' VARIANT<>2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cnths_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('houses',' VARIANT=2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cnthsar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('houses',' VARIANT<>2 AND WHAT=1 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntuch_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('houses',' VARIANT=2 AND WHAT=1 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntuchar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
			
            $res = $this->getCnt('garages',' VARIANT<>2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntgr_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
            $res = $this->getCnt('garages',' VARIANT=2 AND (realtors_base=1 OR realtors_base=2) ');
            $this->_item->cntgrar_r = $res->cnt;
            $this->_item->st_cntwatch_r += $res->watch;
			//↑для базы риэлторов↑
			
            
			$this->_item->st_cntall = $this->_item->cntkv + $this->_item->cntkvar + $this->_item->cntkm + $this->_item->cntkmar + $this->_item->cntcom + $this->_item->cntcomar + $this->_item->cnths + $this->_item->cnthsar + $this->_item->cntgr + $this->_item->cntgrar;
            $this->_item->st_cntsale = $this->_item->cntkv + $this->_item->cntkm + $this->_item->cntcom + $this->_item->cnths + $this->_item->cntgr;
            $this->_item->st_cntrent = $this->_item->cntkvar + $this->_item->cntkmar + $this->_item->cntcomar + $this->_item->cnthsar + $this->_item->cntgrar;
            
            //для личного кабинета нужно вывести статистику по объектам и количество устаревших объектов
            if($layout == 'profile') {
                $this->_item->st_cntall = $this->_item->cntkv + $this->_item->cntkvar + $this->_item->cntkm + $this->_item->cntkmar + $this->_item->cntcom + $this->_item->cntcomar + $this->_item->cnths + $this->_item->cnthsar + $this->_item->cntgr + $this->_item->cntgrar;
                $this->_item->st_cntall_c = $this->_item->cntkv_c + $this->_item->cntkvar_c + $this->_item->cntkm_c + $this->_item->cntkmar_c + $this->_item->cntcom_c + $this->_item->cntcomar_c + $this->_item->cnths_c + $this->_item->cnthsar_c + $this->_item->cntgr_c + $this->_item->cntgrar_c;
				$this->_item->st_cntall_r = $this->_item->cntkv_r + $this->_item->cntkvar_r + $this->_item->cntkm_r + $this->_item->cntkmar_r + $this->_item->cntcom_r + $this->_item->cntcomar_r + $this->_item->cnths_r + $this->_item->cnthsar_r + $this->_item->cntgr_r + $this->_item->cntgrar_r;
				
                $this->_item->st_cntsale = $this->_item->cntkv + $this->_item->cntkm + $this->_item->cntcom + $this->_item->cnths + $this->_item->cntgr;
                $this->_item->st_cntrent = $this->_item->cntkvar + $this->_item->cntkmar + $this->_item->cntcomar + $this->_item->cnthsar + $this->_item->cntgrar;
                
                $this->_item->st_cntexpired = 0;
                $this->getStats('aparts');
                $this->getStats('commerc');
                $this->getStats('houses');
                $this->getStats('garages');
                $this->_item->st_cntrequest = 0;
                $this->_item->st_complaint = 0;
                    
                // количество устаревших объектов
                $this->_item->old = array();
                $this->_item->expired = array();
                $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT=0 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'aparts';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="aparts" data-stype="0" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_AP', $res->cnt) . '</a>';
                }
                
                $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT=0 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'aparts';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="aparts" data-stype="1" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_AP_AR', $res->cnt) . '</a>';
                }

                $res = $this->getCnt('aparts',' VARIANT<>2  AND WHAT>0 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'aparts';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 1;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="aparts" data-stype="0" data-what="1">' . JText::plural('COM_STTNMLS_OLDCNT_RM', $res->cnt) . '</a>';
                }
                
                $res = $this->getCnt('aparts',' VARIANT=2  AND WHAT>0 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'aparts';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 1;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="aparts" data-stype="1" data-what="1">' . JText::plural('COM_STTNMLS_OLDCNT_RM_AR', $res->cnt) . '</a>';
                }
                
                $res = $this->getCnt('houses',' VARIANT<>2  AND WHAT<>1 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'houses';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="houses" data-stype="0" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_HS', $res->cnt) . '</a>';
                }
                                
                $res = $this->getCnt('houses',' VARIANT=2  AND WHAT<>1 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'houses';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="houses" data-stype="1" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_HS_AR', $res->cnt) . '</a>';
                }
                
                $res = $this->getCnt('houses',' VARIANT<>2  AND WHAT=1 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'houses';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 1;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="houses" data-stype="0" data-what="1">' . JText::plural('COM_STTNMLS_OLDCNT_UCH', $res->cnt) . '</a>';
                }
                                
                $res = $this->getCnt('houses',' VARIANT=2  AND WHAT=1 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'houses';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 1;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="houses" data-stype="1" data-what="1">' . JText::plural('COM_STTNMLS_OLDCNT_UCH_AR', $res->cnt) . '</a>';
                }

                $res = $this->getCnt('garages',' VARIANT<>2 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'garages';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="garages" data-stype="0" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_GR', $res->cnt) . '</a>';
                }
                                
                $res = $this->getCnt('garages',' VARIANT=2 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'garages';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="garages" data-stype="1" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_GR_AR', $res->cnt) . '</a>';
                }
                
                $res = $this->getCnt('commerc',' VARIANT<>2 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'coms';
                    $o->count = $res->cnt;
                    $o->stype = 0;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="coms" data-stype="0" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_CM', $res->cnt) . '</a>';
                }

                $res = $this->getCnt('commerc',' VARIANT=2 ',true);
                if($res->cnt > 0) {
                    $o = new stdClass();
                    $o->table = 'coms';
                    $o->count = $res->cnt;
                    $o->stype = 1;
                    $o->what = 0;
                    $this->_item->expired[] = $o;

                    $this->_item->old[] = '<a href="javascript:vd();" class="getObjectsList" data-container="object-list-container-home"  data-table="coms" data-stype="1" data-what="0">' . JText::plural('COM_STTNMLS_OLDCNT_CM_AR', $res->cnt) . '</a>';
                }
                                
                // для статистики по собственниками тоже нужно посчитать кол-во
                if($this->_item->seecont) {
                    $res = $this->getCntSobst('aparts',' VARIANT<>2  AND WHAT=0 ');
                    $this->_item->sobcntkv = $res->cnt;

                    $res = $this->getCntSobst('aparts',' VARIANT=2  AND WHAT=0 ');
                    $this->_item->sobcntkvar = $res->cnt;

                    $res = $this->getCntSobst('aparts',' VARIANT<>2  AND WHAT>0 ');
                    $this->_item->sobcntkm = $res->cnt;

                    $res = $this->getCntSobst('aparts',' VARIANT=2  AND WHAT>0 ');
                    $this->_item->sobcntkmar = $res->cnt;

                    $this->_item->sobcntar = $this->getCntSobst('aparts',' VARIANT=2')->cnt;

                    $res = $this->getCntSobst('houses', 'VARIANT<>2 AND WHAT=0');
                    $this->_item->sobcnths = $res->cnt;

                    $res = $this->getCntSobst('houses','VARIANT=2 AND WHAT=0');
                    $this->_item->sobcnthsar = $res->cnt;

                    $res = $this->getCntSobst('houses','VARIANT<>2 AND WHAT>0');
                    $this->_item->sobcntuch = $res->cnt;

                    $res = $this->getCntSobst('houses','VARIANT=2 AND WHAT>0');
                    $this->_item->sobcntuchar = $res->cnt;


                    $res = $this->getCntSobst('commerc','VARIANT<>2');
                    $this->_item->sobcntcom = $res->cnt;

                    $res = $this->getCntSobst('commerc','VARIANT=2');
                    $this->_item->sobcntcomar = $res->cnt;



                    $res = $this->getCntSobst('garages','VARIANT<>2');
                    $this->_item->sobcntgr = $res->cnt;

                    $res = $this->getCntSobst('garages','VARIANT=2');
                    $this->_item->sobcntgrar = $res->cnt;
                }
                
                // финансы
                $db->setQuery('SELECT `balans` FROM `#__sttnmlsbalans` WHERE `userid`=' . $db->quote($user->id));
                $balance = $db->loadResult();
                
                $this->_item->balance = 0;
                if($balance) {
                    $this->_item->balance = $balance;
                }
                
                $db->setQuery('SELECT * FROM `#__sttnmlsbalanslog` WHERE `status`>0 AND `userid`=' . $db->quote($user->id) . ' ORDER BY `dateinp` DESC',0,15);
                $res = $db->loadObjectList();
                $this->_item->balancelog = $res;
                
                $db->setQuery('SELECT * FROM `#__sttnmlsplatusl` WHERE `userid`=' . $db->quote($user->id) . ' ORDER BY `usluga` ASC, `dateinp` DESC');
                $res = $db->loadObjectList();
                $this->_item->platusl = $res;
				
				
                $db->setQuery('SELECT * FROM `#__sttnmlscompl` WHERE `kodagent`=' . $db->quote($this->_item->kod) . ' AND `status`=1 AND `del` <> 1');
                $res = $db->loadObjectList();
                
                foreach ( $res as $i => $item ) {
                    if($item->typeobj==1)
                    {
                        $query	= 'SELECT * FROM #__sttnmlsaparts WHERE flag!=4 AND CARDNUM="' . $item->cardnum . '" AND COMPID="' . $item->compid . '"';
                        $db->setQuery($query);
                        $obj	= $db->loadObjectList();	
                    }
                    if($item->typeobj==2)
                    {
                        $query	= 'SELECT * FROM #__sttnmlscommerc WHERE flag!=4 AND CARDNUM="' . $item->cardnum . '" AND COMPID="' . $item->compid . '"';
                        $db->setQuery($query);
                        $obj	= $db->loadObjectList();
                    }
                    if($item->typeobj==3)
                    {
                        $query	= 'SELECT * FROM #__sttnmlsgarages WHERE flag!=4 AND CARDNUM="' . $item->cardnum . '" AND COMPID="' . $item->compid . '"';
                        $db->setQuery($query);
                        $obj	= $db->loadObjectList();
                    }
                    if($item->typeobj==4)
                    {
                        $query	= 'SELECT * FROM #__sttnmlshouses WHERE flag!=4 AND CARDNUM="' . $item->cardnum . '" AND COMPID="' . $item->compid . '"';
                        $db->setQuery($query);
                        $obj	= $db->loadObjectList();
                    }
                    if(count($obj)==0)
                    {
                        unset($res[$i]);
                    }
                }
                $this->_item->st_complaint = count($res);
            }
        }
        return $this->_item;
    }
    
    
	public function getStats($table)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(a.CARDNUM ) ');
		$query->from(' #__sttnmls'.$table.' AS a ');
		$query->where(' a.AGENTID='.$db->quote($this->_item->ID));
		$query->where(' a.COMPID='.$db->quote($this->_item->COMPID));
		$query->where(' a.INET=1');
		$query->where(' a.flag<3');
		$params = JComponentHelper::getParams('com_sttnmls');
		$days = $params->get('days', '30');
		if($this->_item->gdays) $days=$this->_item->gdays;
		$query->where(' a.DATECOR<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)');
		$db->setQuery($query);
		$this->_item->st_cntexpired += $db->loadResult(); //просроченные объекты
	}
	
	public function getCnt($tablename, $where = '', $old = false)
	{
        // JOOMLA Instances
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $days = $params->get('days', '30');

		$query = $db->getQuery(TRUE);
        $query->select('COUNT(' . $db->quoteName('CARDNUM') . ') AS ' . $db->quoteName('cnt'));
        $query->select('SUM(' . $db->quoteName('watchcount') . ') AS ' . $db->quoteName('watch'));
        $query->from($db->quoteName('#__sttnmls' . $tablename));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($this->_item->ID));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($this->_item->COMPID));
        $query->where($db->quoteName('INET') . '=' . $db->quote(1));
        $query->where($db->quoteName('flag') . '< 3');

        if($this->_item->gdays) {
            $days = $this->_item->gdays;
        }

        if($old) {
            $query->where($db->quoteName('DATECOR') . '<= DATE_SUB(NOW(), INTERVAL ' . (int)$days . ' day)');
        } else {
            $query->where($db->quoteName('DATECOR') . '> DATE_SUB(NOW(), INTERVAL ' . (int)$days . ' day)');
            $query->where($db->quoteName('DATEINP') . '< DATE_SUB(NOW(), INTERVAL ' . (int) $this->_item->mdays . ' day)');
        }

        if($where != '') {
            $query->where($where);
        }
		$db->setQuery($query);
		return $db->loadObject();
	}



	public function getCntSobst($tablename, $where = '')
	{
        // JOOMLA Instances
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $compidsob = $params->get('compidsob', '0');
        $days = $params->get('days', '30');

        $query = $db->getQuery(TRUE);
        $query->select('COUNT(' . $db->quoteName('o.CARDNUM') . ') AS ' . $db->quoteName('cnt'));
        $query->select('SUM(' . $db->quoteName('o.watchcount') . ') AS ' . $db->quoteName('watch'));
        $query->from($db->quoteName('#__sttnmls' . $tablename, 'o'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON (' . $db->quoteName('o.COMPID') . '=' . $db->quoteName('f.id') . ')');
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'ag') . ' ON (' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('f.id') . ' AND ' . $db->quoteName('ag.id') . '=' . $db->quoteName('o.AGENTID') . ')');
        $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON (' . $db->quoteName('g.id') . '=' . $db->quoteName('ag.grp') . ')');
        $query->where($db->quoteName('ag.flag') . '< 3');
        $query->where($db->quoteName('ag.flag') . '<> 1');
        $query->where($db->quoteName('o.flag') . '< 3');
        $query->where('IFNULL(' . $db->quoteName('f.ID') . ', -1)=' . $db->quote($compidsob));
        $query->where($db->quoteName('o.close') . '=' . $db->quote(0));

		if(!$this->_item->seexpired)
		{
            $query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0)>0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ', 0)<=0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . (int)$days . ' day)))');
		}

        if($where != '') {
            $query->where($where);
        }
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function logInfo ($text, $type = 'message') {
		$file = JPATH_ROOT . "/logs/sttnmls.log";
		$date = JFactory::getDate ();

		$fp = fopen ($file, 'a');
		fwrite ($fp, "\n\n" . $date->toFormat ('%Y-%m-%d %H:%M:%S'));
		fwrite ($fp, "\n" . $type . ': ' . $text);
		fclose ($fp);
	} 

	public function getComUserAvatar($user_id = 0)
	{
        $user_id = (($user_id) ? $user_id : $this->_item->userid);
		$this->_item->prof_link = '';
		return Avatar::getUserAvatar($user_id);
	}

	public function getTitle()
	{
		return $this->_item->SIRNAME.' '.$this->_item->NAME.' '.$this->_item->SECNAME.'. '.$this->_item->firm;
	}
	public function getDesc()
	{
		return $this->_item->firm.'. Телефон для связи:'.$this->_item->PHONE.SttNmlsLocal::GetText(' Информация о специалистах по недвижимости kurska на портале portal.');
	}
        
	
        
	public function getProfile($user_id = 0)
    {
        $db = $this->getDbo();
        $user_id = (($user_id > 0) ? $user_id : JFactory::getUser()->id);
        if ($user_id) {
            $db->setQuery(
                'SELECT `profile_value` FROM `#__user_profiles` ' .
                'WHERE `user_id` = ' . $db->quote($user_id) . " AND `profile_key` = \"profile.realtor\" " .
                'ORDER BY `ordering`'
            );
            $result = $db->loadResult();
            return ($result) ? json_decode($result, TRUE) : 2;
        }
        return NULL;
	}
        
	public function getFirms()
	{
		require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
		return SttNmlsHelper::getOptFirms($this->_item->COMPID);
	}

	
	public function sendmail($subject,$body)
	{
		$app = JFactory::getApplication();
		$mailfrom = $app->getCfg('mailfrom');
		$fromname = $app->getCfg('fromname');
		$params = JComponentHelper::getParams('com_sttnmls');
		$sendadmin = $params->get('adminemail', '');
		if(!$sendadmin) {
			$db = JFactory::getDbo();
			// get all admin users
			$query = 'SELECT name, email, sendEmail, id' .
						' FROM #__users' .
						' WHERE sendEmail=1';
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
			// Send mail to all users with users creating permissions and receiving system emails
			foreach( $rows as $row )
			{
				$usercreator = JFactory::getUser($id = $row->id);
				if ($usercreator->authorise('core.create', 'com_users'))
				{
					$mail = JFactory::getMailer();
					$mail->isHTML(true);
					$mail->addRecipient($row->email);
					$mail->setSender(array($mailfrom, $fromname));
					$mail->setSubject($subject);
					$mail->setBody($body);
					$sent = $mail->Send();
				}
			}  
		} else {
			$mail = JFactory::getMailer();
			$mail->isHTML(true);
			$mail->addRecipient($sendadmin);
			$mail->setSender(array($mailfrom, $fromname));
			$mail->setSubject($subject);
			$mail->setBody($body);
			$sent = $mail->Send();
		}
		
	}
}
