<?php defined('_JEXEC') or die('Restricted access');

//
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$limitobj = SttNmlsHelper::checkLimit();
// Load JCROP jquery plugin
$this->doc->addStyleSheet(JUri::root() . 'components/com_sttnmls/assets/css/jquery.jcrop.css');
$this->doc->addScript(JUri::root() . 'components/com_sttnmls/assets/js/jquery.jcrop.min.js');

?>
<!--↓-- Форма выбора для добавления объекта --↓-->
<div class="well well-sm text-center">
    <form  method="GET" action="/kvartira.html" onsubmit="return checkProfileHomeForm(this);" target="_self" id="profile_home_form">
        <div class="radio_buttons edit-submit-mobile">
            <div class="btn btn-default btn-sm">
                <input type="radio" name="stype" value="0" id="radio1" checked />
                <label for="radio1">Продажа</label>
            </div>
            <div class="btn btn-default btn-sm">
                <input type="radio" name="stype" value="1" id="radio2" />
                <label for="radio2">Аренда</label>
            </div>
        </div>
        <select class="btn btn-default edit-submit-mobile" name="view">
            <option value="apart" what="0">Квартира</option>
            <option value="apart" what="1">Комната</option>
            <option value="house" what="0">Дом</option>
            <option value="house" what="1">Участок</option>
            <option value="garage">Гараж</option>
            <option value="com">Коммерческая недвижимость</option>
        </select>
                
        <input type="hidden" name="what" value="0"/>
        <input type="hidden" name="task" value="edit"/>
        <input type="hidden" name="cn" value="0"/>
        <input type="hidden" name="cid" value="<?=$this->item->COMPID?>"/>
            
        <button class="btn btn-success btn-sm edit-submit-mobile">Добавить объявление</button>
    </form>
</div>
<script>    
    
</script>
<!--↑-- Форма выбора для добавления объекта --↑-->

<h3>
    <?php echo $this->item->SIRNAME . ' ' . $this->item->NAME . ' ' . $this->item->SECNAME ?>:
    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&opentab=tab-commercial-services', 'profile') ?>"><?php echo $this->item->profilename ?></a>
    <span class="badge all-limit"><?php echo JText::sprintf('%d / %d', $this->item->st_cntall, $this->item->limitobj); ?></span>
</h3>

<div class="row">
    <div class="col-md-3">
        <div class="sttnmls_photo lk">
            <div class="image">
                <img class="img-thumbnail" alt="<?php echo $this->item->SIRNAME . ' ' . $this->item->NAME . ' ' . $this->item->SECNAME ?>" src="<?php echo $this->avatar; ?>?t=<?php echo time() ?>" style="width:160px;" />
            </div>
            <div class="upload_image">
                <span class="btn btn-block btn-success btn-sm fileinput-button">
                    <span class="glyphicon glyphicon-open"></span>
                    <span><?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE_PHOTO') ?></span>
                    <input
                        type="file"
                        id="agentphoto"
                        name="files[]"
                        data-url="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&task=json.uploadAvatarImage', 'profile') ?>"
                    />
                </span>
            </div>
        </div>
    </div>
    
    <div class="col-md-9">
                <div class="row">
                    <?php $link = JRoute::_('index.php?option=com_sttnmls&view=aparts&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID, TRUE, -1) ?>
                <div class="col-md-12">    
                    <div style="color: #365d7e;">
                        <span class="showSellTable showTableLable selected">Продажа
                            <span class=""><?php echo '('.$this->item->st_cntsale.')'; ?></span>
                        </span>
                        <span class="showRentTable showTableLable">Аренда
                            <span><?php echo '('.$this->item->st_cntrent.')'; ?></span>
                        </span>
                    </div>
                    <div class="table-responsive">                        
                        <table class="table" id="newSellTable">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="text-center" style="width: 30px;">&nbsp;</th>
                                    <?php if($this->canViewRealtorsBase) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>">
                                                <span class="glyphicon glyphicon-flag"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
									<?php if($this->canViewExpired) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?>">
                                                <span class="glyphicon glyphicon-time"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?>">
                                                <span class="glyphicon glyphicon-lock"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Квартиры продажа-->
                                <tr id="flatsSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntkv ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntkv_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntkv_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntkv_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Квартиры аренда-->
                                <tr id="flatsRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntkvar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntkvar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntkvar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntkvar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты продажа-->
                                <tr id="roomsSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1">
                                            <?php echo $this->item->cntkm ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntkm_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntkm_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntkm_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты аренда-->
                                <tr id="roomsRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntkmar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntkmar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntkmar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntkmar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома продажа-->
                                <tr id="housesSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo ($this->item->cnths - $this->item->cntuch) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1" data-realtors="1">
                                                <?php echo ($this->item->cnths_r - $this->item->cntuch_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo ($this->item->cnths_expired - $this->item->cntuch_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo ($this->item->cnths_c - $this->item->cntuch_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома аренда-->
                                <tr id="housesRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo ($this->item->cnthsar - $this->item->cntuchar) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1" data-realtors="1">
                                                <?php echo ($this->item->cnthsar_r - $this->item->cntuchar_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo ($this->item->cnthsar_expired - $this->item->cntuchar_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=0&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo ($this->item->cnthsar_c - $this->item->cntuchar_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки продажа-->
                                <tr id="landsSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1">
                                            <?php echo $this->item->cntuch ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntuch_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntuch_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntuch_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки аренда-->
                                <tr id="landsRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1') ?>" 
                                        data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1">
                                            <?php echo $this->item->cntuchar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntuchar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntuchar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&what=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntuchar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи продажа-->
                                <tr id="garagesSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntgr ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntgr_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntgr_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntgr_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи аренда-->
                                <tr id="garagesRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home<?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntgrar ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntgrar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntgrar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntgrar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость продажа-->
                                <tr id="comsSale">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                        data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID) ?>" 
                                        data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntcom ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntcom_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntcom_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntcom_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость аренда-->
                                <tr id="comsRent" style="display: none">
                                    <td>
                                        <a class="getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                        <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1') ?>" 
                                        data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1">
                                            <?php echo $this->item->cntcom1 ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_realtors=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1" data-realtors="1">
                                                <?php echo $this->item->cntcomar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_expired=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1" data-expired="1">
                                                <?php echo $this->item->cntcom1_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->canViewClosed) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs getObjectsList" href="#object-list-container-home
                                            <?php //echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->COMPID . '&agent=' . $this->item->ID . '&stype=1&f_closed=1') ?>" 
                                            data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1" data-closed="1">
                                                <?php echo $this->item->cntcomar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <?php if($this->item->flag == 3 OR $this->item->flag == 4) : ?>
        <a href="/lichnyj-kabinet.html?act=unblock" class="btn btn-warning btn-block btn-lg">
            <span class="glyphicon glyphicon-check"></span>
            <?php echo JText::_('COM_STTNMLS_LABEL_UNLOCK_ACCOUNT_AND_FREE_AGENT') ?>
        </a>
    <?php endif; ?>
    <?php if($limitobj == 0) : ?>
        <div class="alert alert-warning">
        <?php echo JText::sprintf('COM_STTNMLS_MESSAGE_INFO_OBJECTS_LIMITS', $this->item->limitobj); ?>
        </div>
    <?php endif; ?>

    <?php if($this->item->expired) : ?>
        <div class="well well-lg">
            <div>
                <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_OBJECT_SHOW_DATE_EXPECTED'); ?>:

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="width:25px;"></th>
                            <th style="width:100px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->item->expired as $item) : ?>
                        <tr>
                            <td>
                                <?php echo JText::_('COM_STTNMLS_LABEL_EXPIRED_' . strtoupper($item->table) . '_' . $item->stype . '_' . $item->what) ?>
                            </td>
                            <td class="text-center">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-link btn-xs getObjectsList hasTooltip"
                                    data-container="object-list-container-home"
                                    data-table="<?php echo $item->table ?>"
                                    data-stype="<?php echo $item->stype ?>"
                                    data-what="<?php echo $item->what ?>"
                                    data-expired="1"
                                    title="<?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?>"
                                >
                                    <?php echo $item->count ?>
                                </a>
                            </td>
                            <td class="text-center">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-info btn-xs changeExpiredObjectChangeDate hasTooltip"
                                    data-container="object-list-container-home"
                                    data-table="<?php echo $item->table ?>"
                                    data-stype="<?php echo $item->stype ?>"
                                    data-what="<?php echo $item->what ?>"
                                    data-message="<?php echo JText::sprintf('COM_STTNMLS_MESSAGE_CONFIRM_CHANGE_ALL_EXPIRED_OBJECT_IN_PART', JText::_('COM_STTNMLS_LABEL_EXPIRED_' . strtoupper($item->table) . '_' . $item->stype . '_' . $item->what)) ?>"
                                    title="<?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?>"
                                >
                                    <span class="glyphicon glyphicon-time"></span>
                                    <span class="sr-only"><?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td class="text-right">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-primary btn-xs changeExpiredObjectChangeDateAll"
                                    data-container="object-list-container-home"
                                    data-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_CONFIRM_CHANGE_ALL_EXPIRED_OBJECT') ?>"
                                >
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE_ALL') ?>
                                </a>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?> 

        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_ORGANIZATION') ?>:</b>
        <?php if ($this->item->firm) : ?>            
            <a target="_blank" class="btn btn-link btn-xs" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID) ?>">
                <span class="glyphicon glyphicon-info-sign"></span>
                <?php echo $this->item->firm; ?>
            </a>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-contacts', 'profile') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-th-list"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE') ?>
            </a>

            <?php if($this->item->flag == 1) : ?>
            <span class="label label-danger">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                <?php echo JText::_('COM_STTNMLS_MESSAGE_WARNING_NOT_VERIFIED') ?>
            </span>
            <?php endif; ?>

        <?php else : ?>
            <?php echo JText::_('JNO') ?>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm', 'edit') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-plus"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_CREATE') ?>
            </a>
        <?php endif; ?>
        </p>

    <?php if($this->item->boss) : ?>
        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_ORGANIZATION') ?>:</b>
            <?php 
                $org_info = array();
                if($this->item->fphone != '') { $org_info[] = $this->item->fphone; }
                if($this->item->femail != '') { $org_info[] = $this->item->femail; }
                $org_text = implode(', ', $org_info);
                if($org_text == '') { $org_text = JText::_('COM_STTNMLS_LABEL_NO_INFO'); }
                echo $org_text;
            ?>
            <?php if(SttNmlsHelper::chkUserEditFirm($this->item->COMPID)) : ?>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID, 'edit') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-edit"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT'); ?>
            </a>
            <?php endif; ?>
        </p>
    <?php endif; ?>

        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS') ?>:</b>

            <?php
                $agent_info = array();
                if($this->item->PHONE != '') { $agent_info[] = $this->item->PHONE; }
                if($this->item->EMAIL != '') { $agent_info[] = $this->item->EMAIL; }
                $agent_text = implode(', ', $agent_info);
                if($agent_text == '') { $agent_text = JText::_('COM_STTNMLS_LABEL_NO_INFO'); }
                echo $agent_text;
            ?>

            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-contacts', 'profile') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-edit"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT'); ?>
            </a>

        </p>

    </div>
</div>