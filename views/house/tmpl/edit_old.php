<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.core.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.widget.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.position.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.autocomplete.js');
$doc->addScript("components/com_sttnmls/assets/js/ajaxupload.js");
$script = ' var viewname = "house";
	var cardnum="'.$this->item->CARDNUM.'";
	var compid="'.$this->item->COMPID.'";';
$doc->addScriptDeclaration($script);
$doc->addScript("components/com_sttnmls/assets/js/edit.js");
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
$pics = (trim($this->item->PICTURES) != '') ? explode(';',$this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';',$this->item->PICNAMES) : '';
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<style>
.alert-error{
	padding:10px;
	color: #b94a48;
	background-color: #f2dede;
	border:1px solid #eed3d7;
	-webkit-border-radius: 3px;
border-radius: 3px;
	background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
	-webkit-background-size: 40px 40px;
	-moz-background-size: 40px 40px;
	-o-background-size: 40px 40px;
}
.editb2{
	width:320px !important;
}
.editb3{
	width: 320px !important;
}

.editb1{
	width: 320px !important;
}

.editb2 select{
	width: 170px !important;
}
.editb4{
	width: 150px !important;
}
td{
	vertical-align: top !important;
}
</style>
<div class="sttnmls">
	<?php $what=$this->what==0?0:1; ?>
	<?php $stype=$this->stype==0?0:1; ?>
	<?php if($this->item->CARDNUM==0) { ?>
			<h2><?php echo JText::_('COM_STTNMLS_EDITNEWHOUSE'.$what.$stype); ?></h2>
	<?php } else {
		if($this->item->flag==2) { ?>
			<h2><?php echo JText::_('COM_STTNMLS_EDITHOUSE'.$what.$stype); ?></h2>
		<?php } else {?>
			<h3><?php echo JText::_('COM_STTNMLS_NOEDIT'); ?></h3>
	<?php } }?>
	<form class="editform form-validate" action="index.php" method="post">
	<input type="hidden" name="option" value="com_sttnmls"/>
	<input type="hidden" name="view" value="house"/>
	<input type="hidden" name="task" value="save"/>
	<input type="hidden" name="stype" value="<?php echo $this->stype; ?>"/>
	<input type="hidden" name="jform[cardnum]" value="<?php echo $this->item->CARDNUM; ?>"/>
	<input type="hidden" name="jform[compid]" value="<?php echo $this->item->COMPID; ?>"/>
	<input type="hidden" name="jform[agentid]" value="<?php echo $this->item->AGENTID; ?>"/>
	<input type="hidden" name="jform[what]" value="<?php echo $what; ?>"/>
	<input type="hidden" name="what" value="<?php echo $what; ?>"/>
	<input type="hidden" id="f_raionid" value="<?php echo $this->item->RAIONID; ?>"/>
	<input type="hidden" id="f_mraionid" value="<?php echo $this->item->MRAIONID; ?>"/>
	<input type="hidden" id="f_street" value="<?php echo $this->item->ulica; ?>"/>
	<input type="hidden" id="streetid" name="jform[streetid]" value="<?php echo $this->item->STREETID; ?>"/>
	<?php echo JHTML::_( 'form.token' ); ?>
		<?php if($this->item->CARDNUM) {?>
			
			<?php if($this->item->CARDNUM) {
				if($this->item->flag==2) { ?>
					<input name="save" type="submit" class="button validate sbgreen" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
					<input name="apply" type="submit" class="button validate" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
					<input name="addnew" type="submit" class="button validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
				<?php } } else { ?>
					<input name="save" type="submit" class="button validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
					<input name="newobj" type="submit" class="button validate sbgreen" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
			<?php } ?>
			<input name="cancel" type="submit" class="button sbred" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
			<div style="padding-top: 10px; padding-bottom:10px;"><?php echo JText::_('COM_STTNMLS_EDIT_DESC'); ?></div>


			<div class="editfoto" style="margin-bottom:10px;">
				<b style="padding-bottom:6px; margin-left:3px; display:inline-block;"><?php echo JText::_('COM_STTNMLS_PHOTO'); ?></b>
				<div class="clear"></div>
				<div style="position:relative; left:-4px;">
					<div id="imgfilemain"><?php echo SttNmlsHelper::getEditPhoto('house', $this->item->CARDNUM, $this->item->COMPID,true); ?></div>
					<span><?php echo JText::_('COM_STTNMLS_FILE_DESC'); ?><br />Общий размер файлов не должен превышать 8MB</span>
					<div id="uploadButton">
						<div class="uploadbutton"><?php echo JText::_('COM_STTNMLS_UPLOAD'); ?></div>
						<div id="load" style="display:none"><div class="loadgif"></div></div>
					</div>
					<div id="imgfiles"><?php echo SttNmlsHelper::getEditPhoto('house', $this->item->CARDNUM, $this->item->COMPID); ?></div>
				</div>
			</div>
		<?php }?>


		<table width="100%">
		<tr>
		<td valign="top">

		<div class="editb1" style="padding-bottom:0px;">
			<b style="padding-bottom:6px; margin-left:3px; display:inline-block;"><?php echo JText::_('COM_STTNMLS_PARHOUSE'); ?></b>
			<div class="clear"></div>
			<?php if(!$what){ ?>
				<div class="editb2">
					<b><?php echo JText::_('COM_STTNMLS_BTYPE'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
					<?php echo $this->optbtype;?>
				</div>
				<div class="editb2">
					<b><?php echo JText::_('COM_STTNMLS_EAREA'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
					<input type="text" size="5" id="earea" name="jform[earea]" class="validate-sttnum required" value="<?php echo $this->item->EAREA ?>" /> <?php echo JText::_('COM_STTNMLS_SOTOK'); ?>
				</div>
			<?php } else { ?>
				<div class="editb2">
					<b><?php echo JText::_('COM_STTNMLS_EAREA'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
					<input type="text" size="5" id="earea" name="jform[earea]" class="validate-sttnum required" value="<?php echo $this->item->EAREA ?>" /> <?php echo JText::_('COM_STTNMLS_SOTOK'); ?>
				</div>
				<div class="editb2">
					<b class="option" style="color:#666;"><?php echo JText::_('COM_STTNMLS_BTYPE'); ?></b>
					<?php echo $this->optbtype;?>
				</div>
			<?php }?>
			<div class="clear"></div>
			<?php if(!$what){ ?>
				<div class="editb2">
					<b><?php echo JText::_('COM_STTNMLS_CNT_STAGE'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
					<select class="required" id="hstage" name="jform[hstage]" size="1" style="width: 50px;">
						<option value="">-</option>;
						<?php for($i=1;$i<4;$i++) {
							echo '<option value="' .$i .'"';
							if($this->item->HSTAGE==$i) echo ' selected="selected"';
							echo '>' .$i.'</option>';
						} ?>
					</select>
				</div>
				<div class="editb2">
					<b><?php echo JText::_('COM_STTNMLS_MW'); ?> <span style="color:#b94a48; display:inline;">*</span></b> <?php echo $this->optwmaterial;?>
				</div>
				<div class="clear"></div>
				<div class="editb2">
					<b style="float:left; padding-right: 5px;"><?php echo JText::_('COM_STTNMLS_EDIT_AREA'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
					<div class="area" style="position:relative; left:-4px;"><input type="text" size="4" style="width:35px;" id="aarea" name="jform[aarea]" class="validate-sttnum required" value="<?php echo $this->item->AAREA ?>" /> / <br/><?php echo JText::_('COM_STTNMLS_EDIT_AAREA'); ?></div> 
					<div class="area"><input type="text" size="4" style="width:35px;" id="larea" name="jform[larea]" class="validate-numeric" value="<?php echo $this->item->LAREA ?>" /> / <br/><?php echo JText::_('COM_STTNMLS_EDIT_LAREA'); ?></div> 
					<div class="area"><input type="text" size="4" style="width:35px;" id="karea" name="jform[karea]" class="validate-numeric" value="<?php echo $this->item->KAREA ?>" /><br/><?php echo JText::_('COM_STTNMLS_EDIT_KAREA'); ?></div>
				</div>
				<div class="clear"></div>
			<?php }?>
		</div>
		<?php if(!$what){ ?>
			<div class="editb1">
				<div class="editb2">
					<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_ROOF'); ?></b>
					<?php echo $this->optroof;?>
				</div>
				<div class="editb2">
					<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_HEAT'); ?></b>
					<?php echo $this->optheat;?>
				</div>
				<div class="clear"></div>
				<div class="editb2">
					<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_FOUNDAT'); ?></b>
					<?php echo $this->optfoundat;?>
				</div>
				<div class="editb2">
					<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_WC'); ?></b>
					<?php echo $this->optwc;?>
				</div>
				<div class="clear"></div>
				<div class="editb2">
					<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_WATER'); ?></b>
					<?php echo $this->optwater;?>
				</div>
				<div class="clear"></div>
			</div>
		<?php }?>
		<div class="editb1" style="height:100px;">
			<b style="padding-bottom:6px; margin-left:3px; display:inline-block;"><?php echo JText::_('COM_STTNMLS_ADDRES'); ?></b>
			<div class="clear"></div>
			<div class="editb2">
				<b><?php echo JText::_('COM_STTNMLS_CITY'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
			<select id="cityid" name="jform[cityid]" class="inputbox" onchange="changecity();">
				<?php echo JHtml::_('select.options', $this->optcity, 'value', 'text', $this->item->CITYID);?>
			</select>
			</div>
			<div class="editb2">
				<b class="option" style="color:#666;"><?php echo JText::_('COM_STTNMLS_RAION'); ?></b> <span id="raion"></span>
			</div>
			<div class="clear"></div>
			<div class="editb2">
				<b class="option" style="color:#666"><?php echo JText::_('COM_STTNMLS_MRAION'); ?></b> <span id="mraion"></span>
			</div>
			<div class="clear"></div>

			<div class="editb2">
				<b><?php echo JText::_('COM_STTNMLS_STREETNP'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
				<?php if($this->item->CARDNUM) {?>
					<input class="required ui-widget-content" type="text" name="street" id="T4" value="<?php echo $this->item->ulica; ?>" onblur="if(this.value.replace(/^\s+|\s+$/g, '')=='') this.value='<?php echo $this->item->ulica; ?>';" onfocus="if(this.value=='<?php echo $this->item->ulica; ?>') this.value='';" size="22"/>
					<button class="butclr" onclick="jQuery('#T4').val('');return false;"></button>
				<?php }else { ?>
					<input class="required ui-widget-content" type="text" name="street" id="T4" value="" size="22"/>
					<button class="butclr" onclick="jQuery('#T4').val('');return false;"></button>
				<?php } ?>
				<div id="filter_street"></div>
			</div>

			<?php if(!$what){ ?>
				<div class="editb2">
					<b class="option" style="color:#666;"><?php echo JText::_('COM_STTNMLS_HAAP'); ?></b>
					<input type="text" size="4" id="haap" name="jform[haap]" value="<?php echo $this->item->HAAP ?>" />
				</div>
			<?php }?>
			<div class="editb3" style="padding-bottom:0px;">
				<b class="option" style="padding-left:0px;"><?php echo JText::_('COM_STTNMLS_EDIT_PRICE'); ?> <span style="color:#b94a48; display:inline;">*</span></b>
				<input type="text" size="20" id="price" onkeyup="price_kor('price');" name="jform[price]" class="validate-sttnum required" value="<?php echo $this->item->PRICE ?>" align="right"/>
				<?php echo JText::_('COM_STTNMLS_RUB'); ?>
				<br><input type="checkbox" value="1" name="checkprice"/><?php echo JText::_('COM_STTNMLS_SQMPRICE'); ?>
			</div>
			<div class="editb3" style="padding-bottom:0px;">
                
                <?php
                $db = JFactory::getDbo();
                $query	= 'SELECT * FROM j25_sttnmlshouses WHERE compid="'.$this->item->COMPID.'" AND cardnum="'.$this->item->CARDNUM.'"';
                $db->setQuery($query);
                $compl	= $db->loadObject();
                ?>            
                <input type="checkbox" value="1" name="EXLUSIVE" <?php if($compl->EXCLUSIVE>0){?> checked="checked"<?php }?>/>
                <b class="option" style="padding-left:0px; position:relative; top:2px;"><img src="http://kurskmetr.ru/images/rating.png" style="position:relative; top:2px;"/> Эксклюзив (договореннось с владельцем на продажу)</b>
            </div>
            
            <div class="editb3" style="padding-bottom:0px;">      
                <input type="checkbox" value="1" name="close" <?php if($this->item->close!=0){?> checked="checked"<?php }?>/>
                <b class="option" style="padding-left:0px; position:relative; top:2px;"><img src="http://kurskmetr.ru/images/zam.png" style="position:relative; top:2px; margin-right:6px;"/>Внутренняя база</b> 
            </div>

            <div class="editb3">      
                <input type="checkbox" value="1" name="export" <?php if($this->item->export!=0 || $_REQUEST['cn']==0){?> checked="checked"<?php }?>/>
                <b class="option" style="padding-left:0px; position:relative; top:2px;">Отправить на свой сайт</b> 
            </div>
            <script type="text/javascript">

            	function str_replace ( search, replace, subject ) {
					if(!(replace instanceof Array)){
						replace=new Array(replace);
						if(search instanceof Array){
							while(search.length>replace.length){
								replace[replace.length]=replace[0];
							}
						}
					}
					if(!(search instanceof Array))search=new Array(search);
					while(search.length>replace.length){
						replace[replace.length]='';
					}
					if(subject instanceof Array){
						for(k in subject){
							subject[k]=str_replace(search,replace,subject[k]);
						}
						return subject;
					}
					for(var k=0; k<search.length; k++){
						var i = subject.indexOf(search[k]);
						while(i>-1){
							subject = subject.replace(search[k], replace[k]);
							i = subject.indexOf(search[k],i);
						}
					}
					return subject;
				}


            	function XFormatPrice(_number)
				{
				    var decimal=0;
				    var separator=' ';
				    var decpoint = '.';
				    var format_string = '#';
				    _number=str_replace(" ","",_number);
				    var r=parseFloat(_number)
				    var exp10=Math.pow(10,decimal);
				    r=Math.round(r*exp10)/exp10;
				    rr=Number(r).toFixed(decimal).toString().split('.');
				    b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
				    r=(rr[1]?b+ decpoint +rr[1]:b);
				    return format_string.replace('#', r);
				}
				function price_kor(id)
				{
					if(XFormatPrice(jQuery("#"+id).val())=='NaN')
					{
						jQuery("#"+id).val('');
					}else{
						jQuery("#"+id).val(XFormatPrice(jQuery("#"+id).val()));
					}
					
				}
            </script>
            <?php
			$db = JFactory::getDbo();
			$user = JFactory::getUser();
			$userid = $user->get('id');
			$query	= 'SELECT * FROM j25_sttnmlsvocagents WHERE userid="'.$userid.'"';
            $db->setQuery($query);
            $agen	= $db->loadObject();
			if($agen->COMPID!='99001'){
			?>
			<b style="padding-bottom:6px; margin-left:3px; display:inline-block; padding-top:10px;">Контакты клиента (скрыто)</b>
            <div class="editb3" style="padding-bottom:0px;">   
                <input type="text" value="<?php echo $this->item->sob_tel;?>" name="sob_tel" id="jform_phone" style="width:120px; background:url(http://www.iconsearch.ru/uploads/icons/ledicons/16x16/mobile_phone.png) no-repeat 5px 2px #fff;; border:1px solid #999; padding:2px 2px 2px 25px" placeholder="+7 (___) ___-__-__" />
            </div>
            <div class="editb3" style="padding-bottom:0px;">    
                <input type="text" value="<?php echo $this->item->sob_tel2;?>" name="sob_tel2" id="jform_phone2" style="width:120px; background:url(http://www.iconsearch.ru/uploads/icons/ledicons/16x16/mobile_phone.png) no-repeat 5px 2px #fff;; border:1px solid #999; padding:2px 2px 2px 25px" placeholder="+7 (___) ___-__-__" />
            </div>
            <div style="clear:both;"></div>
            
            <div class="editb3">
                    
                <input type="text" style="width:290px; background:url(http://www.iconsearch.ru/uploads/icons/vaga/16x16/user.png) no-repeat 5px 2px #fff;; border:1px solid #999; padding:2px 2px 2px 25px" value="<?php echo $this->item->sob_fio;?>" placeholder="ФИО клиента" name="sob_fio"/>
            </div>
            
            <!--<div class="editb3">       
                <input type="checkbox" value="1" name="sob_visible" checked="checked"/>
                <b class="option" style="padding-left:0px;">Показывать контакты собственника другим агентам</b>  
            </div>-->
            <?php
			}
			?>
            
		<div class="clear"></div>
		<div style="display:none;">
		<label for="earea"><?php echo JText::_('COM_STTNMLS_ERROR_EAREA'); ?></label>
		<?php if(!$what){ ?>
			<label for="jformBTYPEID"><?php echo JText::_('COM_STTNMLS_ERROR_BTYPEID'); ?></label>
			<label for="hstage"><?php echo JText::_('COM_STTNMLS_ERROR_HSTAGE'); ?></label>
			<label for="jformwmaterid"><?php echo JText::_('COM_STTNMLS_ERROR_WMATERID'); ?></label>
		<?php } ?>
		<label for="T4"><?php echo JText::_('COM_STTNMLS_ERROR_STREET'); ?></label>
		<label for="aarea"><?php echo JText::_('COM_STTNMLS_ERROR_AAREA'); ?></label>
		<label for="larea"><?php echo JText::_('COM_STTNMLS_ERROR_LAREA'); ?></label>
		<label for="karea"><?php echo JText::_('COM_STTNMLS_ERROR_KAREA'); ?></label>
		<label for="price"><?php echo JText::_('COM_STTNMLS_ERROR_PRICE'); ?></label>
		</div>
		</div>




		</td>
		<td>





		<div class="editb1" style="height:160px;">
			<b style="padding-bottom:6px; margin-left:3px; display:inline-block;"><?php echo JText::_('COM_STTNMLS_DOP'); ?>:</b>
			<div class="clear"></div>
            <div class="editb2">
				<table class="editb4">
				<?php foreach($this->checks1 as $key=>$r) {
					echo '<tr><td><b class="option" style="color:#666;">'.$r.'</b></td><td>';
					if(stripos($this->item->HCHECKS1, $r)===false) {
						echo '<input type="checkbox" value="'.$r.'" name="checks1[]"/>';
					}
					else {
						echo '<input type="checkbox" value="'.$r.'" name="checks1[]" checked="checked"/>';
					}
					echo '</td></tr>';
				}
				?>
				</table>
				<table class="editb4">
				<?php foreach($this->checks2 as $key=>$r) {
					echo '<tr><td width="20"></td><td><b class="option" style="color:#666;">'.$r.'</b></td><td>';
					if(stripos($this->item->HCHECKS1, $r)===false) {
						echo '<input type="checkbox" value="'.$r.'" name="checks1[]"/>';
					}
					else {
						echo '<input type="checkbox" value="'.$r.'" name="checks1[]" checked="checked"/>';
					}
					echo '</td></tr>';
				}
				?>
				</table>
			</div>
			<div class="editb2">
				<textarea name="jform[misc]" style="width:320px;"><?php echo $this->item->MISC; ?></textarea>
			</div>
			<?php
			$db = JFactory::getDbo();


			$user = JFactory::getUser();
			$userid = $user->get('id');
			$query	= 'SELECT * FROM j25_sttnmlsvocagents WHERE userid="'.$userid.'" AND COMPID="'.$_REQUEST['cid'].'"';
			$boss	= $db->loadObjectList();


			$query	= 'SELECT * FROM j25_sttnmlsvocagents WHERE COMPID="'.$_REQUEST['cid'].'"';
			$db->setQuery($query);
			$bs	= $db->loadObjectList();
			$params = JComponentHelper::getParams('com_sttnmls');
			if($params->get('showfrom','')==1 || $boss[0]->boss==1){
			?>

			<div class="editb2">
				От имени какого агента создать объект
				
				<select name="fromag">
					<option value="0">От себя</option>
					<?php
					foreach($bs as $b)
					{
						?>
							<option value="<?php echo $b->ID;?>" <?php if($this->item->AGENTID==$b->ID){ ?> selected="selected" <?php }?>><?php echo $b->SIRNAME." ".$b->NAME." ".$b->SECNAME;?></option>
						<?php
					}
					?>
				</select>
			</div>
			<?php
			}
			?>
		</div>
		
        
		</td>
		</tr>
		</table>
	<?php if($this->item->CARDNUM) {
			if($this->item->flag==2) { ?>
				<input name="save" type="submit" class="button validate sbgreen" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
				<input name="apply" type="submit" class="button validate" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
				<input name="addnew" type="submit" class="button validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
	<?php } } else { ?>
		
		<input name="newobj" type="submit" class="button validate sbgreen" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
		<input name="save" type="submit" class="button validate" value="Сохранить без фото"/>
	<?php } ?>
	<input name="cancel" type="submit" class="button sbred" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
	<div style="padding-top: 10px;"><?php echo JText::_('COM_STTNMLS_EDIT_DESC'); ?></div>	
	</form>
</div>
