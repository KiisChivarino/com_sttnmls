<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class SttNmlsModelList extends JModelList 
{
    private $_tabname;
    private $_tp; //тип объекта для добавления в избранное
    private $_upd=false;
    private $_moder=false;
    private $_seexpired=false;
    
    public function getModer()
    {
        $result = FALSE;
        $user = JFactory::getUser();
        
        if($user->id){
            $db = $this->getDbo();
            $query = $db->getQuery(true);
            $query->select('g.seecont, g.seexpired');
            $query->from(' #__sttnmlsvocagents AS a ');
            $query->join('LEFT','`#__sttnmlsgroup` as g on g.id=a.grp');
            $query->where('a.userid=' . $db->quote($user->id));
            $query->where('g.seecont=1');
            $db->setQuery($query);
            $result = $db->loadObject();
            if($result->seecont) {
                $this->_moder = TRUE;
                $this->_seexpired = $result->seexpired;
            }
        }
        return $result;
    }
    
    
    
    protected function getListQuery()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = $this->getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $iAmSuperAdmin	= $user->authorise('core.admin');
        $days = $params->get('days', '30');
        $agency_sobstv = $params->get('compidsob', 99001);
        $f_exclusive_only_nmls = $params->get('f_exclusive_only_nmls', 0);

        // Request VARS
        $format = $app->input->get('format', 'html', 'string');
        $street = $app->input->get('streets', 0, 'uint');
        $agency = $app->input->get('agency', 0, 'uint');
        $agent = $app->input->get('agent', 0, 'uint');
        $phone = $app->input->get('phone', '', 'string');
        $excluzive = $app->input->get('excluzive', 0, 'uint');
        $close = $app->input->get('close', 0, 'uint');
        $sobst = $app->input->get('sobst', 0, 'uint');
        $price_day = $app->input->getBool('price_day', FALSE);
        $price_hour = $app->input->getBool('price_hour', FALSE);
        $stype = $app->input->get('stype', 0, 'uint');
        $view = $app->input->get('view', 'aparts', 'string');
        $filter_rooms = $app->input->get('filter_rooms', array(), 'array');
        $all = $app->input->get('all', 0, 'uint');
        $what = $app->input->get('what', 0, 'uint');
        $act = $app->input->get('act', '', 'string');
        $myne = $app->input->get('myne', '', 'string');

        $newbuild = $app->input->get('newbuild', 0, 'uint');
        $builder = $app->input->get('builder', 0, 'uint');
        $building = $app->input->get('building', 0, 'uint');


        if(!$user->id){
            $editable = ' 0 as editable, ';
        } else {
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsvocagents'));
            $query->where($db->quoteName('userid') . '=' . $db->quote($user->id));
            $db->setQuery($query);
            $myagents	= $db->loadObject();

            if($iAmSuperAdmin) {
                $editable = ' 1 as editable, ';
            }else{
                if($view != 'buildings')
                {
                    if($myagents->boss == 1) {
                        $editable = ' CASE WHEN ' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID) . ' THEN 1 ELSE 0 END AS ' . $db->quoteName('editable') . ', ';
                    } else {
                        $editable = ' CASE WHEN ' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ' THEN 1 ELSE 0 END AS ' . $db->quoteName('editable') . ', ';
                    }
                }else{
                    if($myagents->boss == 1) {
                        $editable = ' CASE WHEN (' . $db->quoteName('a.comp') . '=' . $db->quote($myagents->COMPID) . ' OR ' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID) . ') THEN 1 ELSE 0 END AS ' . $db->quoteName('editable') . ', ';
                    } else {
                        $editable = ' CASE WHEN ' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ' THEN 1 ELSE 0 END AS ' . $db->quoteName('editable') . ', ';
                    }

                }
            }
        }
        $query = $db->getQuery(TRUE);
        $query->select($this->getState('list.select', 'a.*, ' . $editable . ' IF(a.DATEINP<DATE_SUB(NOW(), INTERVAL IFNULL(g.mdays,0) day),f.NAME,"") as firm, m.NAME as mraion ' . $this->getDopSelect()));

        if($this->_tabname == '#__sttnmlsbuildings') {
            $query->select('COUNT(' . $db->quoteName('barel.builder') . ') AS ' . $db->quoteName('aparts_counts'));
        }

        $query->from($db->quoteName($this->_tabname, 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('a.CITYID') . '=' . $db->quoteName('c.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'r') . ' ON ' . $db->quoteName('a.RAIONID') . '=' . $db->quoteName('r.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('a.STREETID') . '=' . $db->quoteName('s.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON ' . $db->quoteName('a.COMPID') . '=' . $db->quoteName('f.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'ag') . ' ON ' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('f.id') . ' AND ' . $db->quoteName('ag.id') . '=' . $db->quoteName('a.agentid'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON ' . $db->quoteName('g.id') . '=' . $db->quoteName('ag.grp'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'm') . ' ON ' . $db->quoteName('a.MRAIONID') . '=' . $db->quoteName('m.id'));
        $query->where($db->quoteName('ag.flag') . '<3 AND ' . $db->quoteName('ag.flag') . '<>1');

        if($app->input->get('UPD', '', 'string')) {
            $this->setState('list.start', 0);
        }

        // @TODO: Удалить этот фрагмент как ненужный
        if($filter_rooms && $view != 'buildings') {
            if(count($filter_rooms) == 1 && $filter_rooms[0] != 'null' && $filter_rooms[0]) {
                //JRequest::setVar('filter_rooms', $_REQUEST['filter_rooms']);
                $app->setUserState($this->context . '.filter.rooms', null);
                $this->setState('filter.rooms', array());

                if($filter_rooms[0] >= 4)
                {
                    $query->where($db->quoteName('a.ROOMS') . '>=' . $db->quote($filter_rooms[0]));
                } else {
                    $query->where($db->quoteName('a.ROOMS') . '=' . $db->quote($filter_rooms[0]));
                }
            }
        }
        // ------------------------------XXX----------------
        

        if($app->input->get('RST', '', 'string') != '')
        {
            $this->resetFilter();
            $this->setState('list.start', 0);
            if($street) {
                $app->input->set('streets', 0);
                $street = 0;
            }
            if($agency) {
                $app->input->set('agency', 0);
                $agency = 0;
            }
            if($agent) {
                $app->input->set('agent', 0);
                $agent = 0;
            }
            if($newbuild) {
                $app->input->set('newbuild', 0);
                $newbuild = 0;
            }
            if($excluzive) {
                $app->input->set('excluzive', 0);
                $excluzive = 0;
            }
            if($close) {
                $app->input->set('close', 0);
                $close = 0;
            }
            if($price_day) {
                $app->input->set('price_day', FALSE);
                $price_day = FALSE;
            }
            if($price_hour) {
                $app->input->set('price_hour', FALSE);
                $price_hour = FALSE;
            }
        }
		
        if($app->input->get('filter_street', '', 'string') == JText::_('COM_STTNMLS_STREET_SELECT') OR $street) {
            $app->setUserState($this->context . '.filter.street', '');
            $this->setState('filter.street', '');
            JRequest::setVar('filter_street', '');
        }
        
        if($street) {
            $query->where($db->quoteName('a.streetid') . '=' . $db->quote($street));
        }

        if($agency) {
            if($view == 'buildings') {
                if($agency) {
                    $query->where($db->quoteName('a.comp') . '=' . $db->quote($agency));
                }
            } else {
                $query->where($db->quoteName('a.COMPID') . '=' . $db->quote($agency));
            }
        }

        if($newbuild) {
            $query->where($db->quoteName('a.NEWBUILD') . '=' . $db->quote($newbuild));
        }

        if($excluzive) {
            if($f_exclusive_only_nmls) {
                $query->where($db->quoteName('a.EXCLUSIVE') . '=' . $db->quote(3));
            } else {
                $query->where($db->quoteName('a.EXCLUSIVE') . '>0');
            }
        }

        if(in_array($view, array('aparts', 'coms')) && $stype) {
            if($price_day && $price_hour) {
                $query->where($db->quoteName('a.PRICE_DAY') . '>0 OR ' . $db->quoteName('a.PRICE_HOUR') . '>0');
            } else {
                if($price_day) {
                    $query->where($db->quoteName('a.PRICE_DAY') . '>0');
                }
                if($price_hour) {
                    $query->where($db->quoteName('a.PRICE_HOUR') . '>0');
                }
            }
        }

        if($myagents) {
            if($close) {
                if($myagents->boss == 1) {
                    $query->where('((' . $db->quoteName('a.agents') . ' LIKE "%|' . $myagents->kod . '|%" AND ' . $db->quoteName('a.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID) . ' AND ' . $db->quoteName('a.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('a.AGENTID') . '=' . $db->quote($myagents->ID) . ' AND ' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID) . ' AND ' . $db->quoteName('a.close') . '=' . $db->quote(1) . '))');
                } else {
                    $query->where('((' . $db->quoteName('a.agents') . ' LIKE "%|' . $myagents->kod . '|%" AND ' . $db->quoteName('a.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('a.AGENTID') . '=' . $db->quote($myagents->ID) . ' AND ' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID) . ' AND ' . $db->quoteName('a.close') . '=' . $db->quote(1) . '))');
                }
            } else {
                $query->where('(' . $db->quoteName('a.agents') . ' LIKE "%|' . $myagents->kod . '|%" OR ' . $db->quoteName('a.close') . '=' . $db->quote(0) . ' OR (' . $db->quoteName('a.AGENTID') . '=' . $db->quote($myagents->ID) . ' AND ' . $db->quoteName('a.COMPID') . '=' . $db->quote($myagents->COMPID).'))');
            }
        } else {
            $query->where($db->quoteName('a.close') . '=' . $db->quote(0));
        }

		
        if($agent) {
            $query->where($db->quoteName('a.AGENTID') . '=' . $db->quote($agent));
            // если список по одному агенту, и не он смотрит список, то в списке не должно быть объявлений на премодерации
            $query->where('(' . $db->quoteName('a.DATEINP') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.mdays') . ', 0) day) OR ' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ')');
        }

        if($phone) {
            $db2 = $this->getDbo();
            $query2 = $db2->getQuery(true);
            $query2->select($this->getState('list.select', 'a.*, f.NAME as firm, f.realname '));
            $query2->from(' `#__sttnmlsvocagents` AS a ');
            $query2->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.id');
            $query2->join('LEFT','`#__sttnmlsgroup` as g on g.id=a.grp');
            $query2->where('a.flag<3');
            $query2->where('a.flag<>1');
            $query2->where('trim(a.PHONE)<>""');
            $query2->where('a.COMPID<>' . $db->quote($agency_sobstv));
            $query2->where('f.active=1 ');
            $query2->where('a.grp<>2');
            $query2->where('a.SIRNAME <> ""');
            $query2->where('replace(replace(replace(replace(a.phone," ",""),")",""),"(",""),"-","") LIKE "%' . $phone . '%"');
            $db2->setQuery($query2);
            $idsob	= $db2->loadObjectList();
            if(count($idsob)>0) {
                $qr = ' (';
                foreach($idsob as $ids) {
                    $qr .= '(a.COMPID="' . $ids->COMPID . '" AND a.AGENTID="' . $ids->ID . '") OR ';
                }
                $qr .= ' replace(replace(replace(replace(replace(a.sob_tel,"+","")," ",""),")",""),"(",""),"-","") LIKE "%' . $phone . '%" OR replace(replace(replace(replace(replace(a.sob_tel2,"+","")," ",""),")",""),"(",""),"-","") LIKE "%' . $phone . '%"';
                $qr = str_replace(" OR OR","",$qr);
                $qr .= ') ';
                $query->where(' '.$qr.' ');
            }else{
                $qr = ' ( replace(replace(replace(replace(replace(a.sob_tel,"+","")," ",""),")",""),"(",""),"-","") LIKE "%' . $phone . '%" OR replace(replace(replace(replace(replace(a.sob_tel2,"+","")," ",""),")",""),"(",""),"-","") LIKE "%' . $phone . '%") ';
                $query->where(' '.$qr.' ');
            }
        }

        $compidsob = $params->get('compidsob', '0');
        $sobstneprod = FALSE;

        if($format == 'raw') {
            if($sobst) {
                $query->where(' f.ID=' . $db->quote($compidsob));
                if($this->_seexpired) {
                    $sobstneprod = TRUE;
                }
            } else {
                if(!$builder OR !$building) {
                    $sobstneprod = TRUE;
                    $query->where('(ag.userid=' . $db->quote($user->id) . ' AND ag.userid>0)');
                }
            }
        }
        $this->setDopWhere($query);
        // Район -------------------------------------------------------
        $this->addWhereCheckbox('my_check', 'a.RAIONID', $query);
        // Микрорайон --------------------------------------------------
        $this->addWhereCheckbox('my_MRcheck', 'a.MRAIONID', $query);
				
        if($view == 'buildings') {
            $this->addWhere('price_ot', 'a.pricefrom>=', $query);
            $this->addWhere('price_do', 'a.pricefrom<=', $query);
        } else {
            $this->addWhere('price_ot', 'a.PRICE>=', $query);
            $this->addWhere('price_do', 'a.PRICE<=', $query);
        }
		
        if($view == 'buildings') {
            $this->addWhereCheckbox('comp', 'a.comp', $query);
            $this->addWhereCheckbox('furnish', 'a.furnish', $query);
            $this->addWhereCheckbox('class', 'a.class', $query);
            $this->addWhereCheckbox('wmaterid', 'a.WMATERID', $query);
            $this->addWhere('dateyear', 'a.dateyear<=', $query, FALSE, 'array');
            $this->addWhere('datekvar', 'a.datekvar<=', $query, FALSE, 'array');
        }
		
        $this->addWhere('city', 'a.CITYID=', $query);
        $this->addWhere('street', 's.NAME LIKE ', $query, TRUE);
        $query->where('a.flag<3');
		
        $neprod = FALSE;
        if($this->_moder) {
            $search = $this->getState('filter.sobst', array());
            if(count($search)>1) $query->where('f.ID=' . $db->quote($compidsob));
            $search = $this->getState('filter.neprod', array());
            if(count($search)>1) {
                $neprod = TRUE;
            }
        }
        
        // продолжительность показа
        if(!$neprod && !$all && !$agent && $format!='raw')
        {			
            if ($view != 'buildings') {
                $query->where('((IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
            }
        } 
		
        if($agent) {
            $query->where('((IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)) or ag.userid=' . $db->quote($user->id) . ' )');
        } 
        
        if(!$sobstneprod && $format == 'raw') {
            $query->where('((IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        }
        
        if($neprod  && $format != 'raw') {
            $query->where('((IFNULL(g.days,0)>0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        }
		
        $ifdlit = ' IF((IFNULL(g.days,0)>0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)), "0", "1") as ifdlit ';
        $query->select($ifdlit);
        if($format == 'raw' && $sobst) {
            $query->order($db->escape(' a.DATEINP DESC '));
        } else {
            $orderCol = $this->state->get('list.ordering');
            $orderDirn = $this->state->get('list.direction');
			
            $exclsort = ' a.upobj DESC, ';
            if($params->get('exclsort',0)) $exclsort = ' a.upobj DESC, IF(`EXCLUSIVE`=3,0,1), ' ;
            if($orderCol=='' || $orderCol=='ordering' ) {
                if($view == 'buildings') {
                    $query->order($db->escape(' a.dateyear ASC, a.datekvar ASC '));
                } else {
                    $query->order($db->escape($exclsort . 'PICCOUNT>0 DESC, DATEINP DESC '));
                }
            } else if($orderCol == 'PICCOUNT' && strtoupper($orderDirn) == 'DESC') {
                $query->order($db->escape($exclsort . 'PICCOUNT>0 DESC, DATEINP DESC '));
            } else if($orderCol == 'a.DATEINP' && strtoupper($orderDirn) == 'ASC') {
                if($view == 'buildings') {
                    $query->order($db->escape(' a.dateyear DESC, a.datekvar DESC '));
                } else {
                    $query->order($db->escape(' a.DATEINP DESC '));
                }
            } else if($orderCol == 'a.DATEINP' && strtoupper($orderDirn) == 'DESC') {
                if($view == 'buildings') {
                    $query->order($db->escape(' a.dateyear ASC, a.datekvar ASC '));
                } else {
                    $query->order($db->escape(' a.DATEINP ASC '));
                }	
            } else {
                if($view == 'buildings') {
                    if($orderCol == 'a.AAREA') {
                        $orderCol = 'a.areafrom';
                    }

                    if($orderCol == 'a.PRICE') {
                        $orderCol = 'a.pricefrom';
                    }
                }
                $query->order($db->escape($orderCol . ' ' . $orderDirn));
            }
        }

        if($what == 20 && $what == 'houses')
        {
            $query = str_replace("a.WHAT>0", "a.WHAT>=0", $query);
        }
		
        if($act == 'pdf') {
            if($view == 'houses') {
                $query = str_replace("c.NAME as gorod,", "c.NAME as gorod, ag.SIRNAME as sir, ag.NAME as nm, ag.PHONE as ph, ag.ADDPHONE as adph,",str_replace("  AND  a.WHAT=0","",str_replace("ORDER BY  a.upobj DESC, PICCOUNT>0 DESC, DATEINP DESC","ORDER BY a.WHAT, a.CITYID, a.RAIONID, a.PRICE", $query)));
            }
            if($view == 'aparts') {
                $query = str_replace("c.NAME as gorod,", "c.NAME as gorod, ag.SIRNAME as sir, ag.NAME as nm, ag.PHONE as ph, ag.ADDPHONE as adph,",str_replace("  AND  a.WHAT=0","",str_replace("ORDER BY  a.upobj DESC, PICCOUNT>0 DESC, DATEINP DESC","ORDER BY a.WHAT, a.CITYID, a.RAIONID, a.ROOMS, ulica, a.PRICE", $query)));
            }
            if($view == 'garages') {
                $query = str_replace("c.NAME as gorod,", "c.NAME as gorod, ag.SIRNAME as sir, ag.NAME as nm, ag.PHONE as ph, ag.ADDPHONE as adph,",str_replace("  AND  a.WHAT=0","",str_replace("ORDER BY  a.upobj DESC, PICCOUNT>0 DESC, DATEINP DESC","ORDER BY a.CITYID, a.RAIONID, a.PRICE", $query)));
            }
            if($view == 'coms') {
                $query = str_replace("c.NAME as gorod,", "c.NAME as gorod, ag.SIRNAME as sir, ag.NAME as nm, ag.PHONE as ph, ag.ADDPHONE as adph,",str_replace("  AND  a.WHAT=0","",str_replace("ORDER BY  a.upobj DESC, PICCOUNT>0 DESC, DATEINP DESC","ORDER BY a.OBJID, a.CITYID, a.RAIONID, a.PRICE", $query)));
            }
            if($myne == '1' && !$sobst) {
                $query = str_replace("AND  a.flag<3", 'AND  a.flag<4 AND  a.WHAT=' . $db->quote($what) . ' AND a.AGENTID=' . $db->quote($myagents->ID) . ' AND a.COMPID=' . $db->quote($myagents->COMPID), $query);
                $query = str_replace("  AND  ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL 30 day)))", " ", $query);
            }
            if($sobst) {
                $query = str_replace("AND  a.flag<3", 'AND  a.flag<3 AND f.ID=' . $db->quote($agency_sobstv) . ' AND a.WHAT=' . $db->quote($what), $query);
                $query = str_replace("  AND  ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL 30 day)))", " ", $query);
            }
        }
        
        if(isset($_REQUEST['fltr_view']) && $_REQUEST['fltr_view'] == 1)
        {
            $query=str_replace("PICCOUNT>0 DESC, ","",$query);
        }
        
        if(count($_REQUEST['filter_rooms'])>0 && $_REQUEST['view']=='buildings')
        {
            if($_REQUEST['filter_rooms'][0]==null)
            {
                unset($_REQUEST['filter_rooms'][array_search(null,$_REQUEST['filter_rooms'])]);
            }
            $r='AND a.ROOMS in ('.implode(",",$_REQUEST['filter_rooms']).')';
            $q='AND (';
            foreach($_REQUEST['filter_rooms'] as $fr)
            {
                $q.='a.type LIKE "%,'.$fr.',%" OR ';
            }
            $q.='||)';
            $q=str_replace(' OR ||','',$q);
            $query=str_replace($r,$q,$query);
        }

        if($view == 'buildings') {
            $query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.builder') . '=' . $db->quoteName('a.comp') . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quoteName('a.CARDNUM') . ')');
            $query->group($db->quoteName('a.comp') . ', ' . $db->quoteName('a.cardnum'));
        }

        // Поиск по застройщику
        if($view == 'aparts' && $builder && $building) {
            $query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.aparts_compid') . '=' . $db->quoteName('a.COMPID') . ' AND ' . $db->quoteName('barel.aparts_cardnum') . '=' . $db->quoteName('a.CARDNUM') . ')');
            $query->where('(' . $db->quoteName('barel.builder') . '=' . $db->quote($builder) . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quote($building) . ')');
        }
						
//        echo (string)$query;exit;
        return $query;
    }
	
    public function getUrlff($qar,$key) {
        $a=$qar;
        // агенство нельзя сбросить, если есть агент
        if($key!='agency' || !isset($a['agent']))
                unset($a[$key]);
        $uri = JFactory::getURI ();
        $base	= $uri->toString(array('scheme', 'host', 'port', 'path')); 
        $s = $uri->buildQuery($a);
        if($s) $url = $base.'?'.$s;
        else $url = $base;
        return base64_encode($url);
    }
    
    public function getFilterList()
    {
        $fl_value = array('city' => 0,'street' => '', 'htype' => array(), 'rooms' => array(), 'my_checkH' => array(), 'my_check' => array(), 'my_MRcheck' => array(), 'my_checkMW' => array(), 'area_ot' => 0, 'area_do' => 0, 'kitch_ot' => 0, 'kitch_do' => 0, 'earea_ot' => 0, 'earea_do' => 0, 'price_ot' => 0, 'price_do' => 0, 'sobst' => array(), 'neprod' => array(), 'comp' => array(), 'class' => array(), 'wmaterid' => array(), 'dateyear' => array(), 'datekvar' => array(), 'furnish' => array());
        $fl = array();
        $qar = array();
        $qar['view'] = JRequest::getVar('view', 'aparts');
        $qar['stype'] = JRequest::getInt('stype', 0);
        if(!$qar['stype']) unset($qar['stype']);
        $qar['what'] = JRequest::getInt('what', 0);
        if(!$qar['what']) unset($qar['what']);
				
        $qar['all'] = JRequest::getInt('all', 0);
        if(!$qar['all']) unset($qar['all']);
        $qar['agency'] = JRequest::getInt('agency', 0);
        if(!$qar['agency']) unset($qar['agency']);
        $qar['newbuild'] = JRequest::getInt('newbuild', 0);
        if(!$qar['newbuild']) unset($qar['newbuild']);

        $qar['excluzive'] = JRequest::getInt('excluzive', 0);
        if(!$qar['excluzive']) unset($qar['excluzive']);
        $qar['close'] = JRequest::getInt('close', 0);
        if(!$qar['close']) unset($qar['close']);

        $qar['price_day'] = JRequest::getInt('price_day', 0);
        if(!$qar['price_day']) unset($qar['price_day']);

        $qar['price_hour'] = JRequest::getInt('price_hour', 0);
        if(!$qar['price_hour']) unset($qar['price_hour']);

        $qar['agent'] = JRequest::getInt('agent', 0);
        if(!$qar['agent']) unset($qar['agent']);
        $qar['streets'] = JRequest::getInt('streets', 0);
        if(!$qar['streets']) unset($qar['streets']);
        if(isset($qar['agency']) && $qar['agency']) {
            $filter = new stdClass();
            $filter->name = JText::_('COM_STTNMLS_FILTER_NAME_AGENCY');
            $filter->value = $this->getFilterValue('#__sttnmlsvocfirms', 'realname', 'ID', array($qar['agency']));
            $filter->returnURL = $this->getUrlff($qar, 'agency');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }
        
        if(isset($qar['agent']) && $qar['agent']) {
            $filter = new stdClass();
            $filter->name = JText::_('COM_STTNMLS_FILTER_NAME_AGENT');
            $filter->value = $this->getFilterValue('#__sttnmlsvocagents', 'concat(SIRNAME," ",NAME," ",SECNAME)', 'ID', array($qar['agent']),'COMPID='.$qar['agency']);
            $filter->returnURL = $this->getUrlff($qar, 'agent');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }
		
        if(isset($qar['streets']) && $qar['streets']) {
            $filter = new stdClass();
            $filter->name = JText::_('COM_STTNMLS_FILTER_NAME_STREET');
            $filter->value = $this->getFilterValue('#__sttnmlsvocstreet', 'NAME', 'ID', array($qar['streets']));
            $filter->returnURL = $this->getUrlff($qar, 'streets');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }
		
        foreach ($fl_value as $key => $value) {
            $a = $this->getState('filter.'.$key, -1);

            if($a!=-1 && $a != $value) {
                if(is_array($a) && count($a)==1 && ($a[0]=='null' || !$a[0])) {
                        continue;
                }
                if($key=='rooms' && !$a[0]) {
                        continue;
                }
				
                $filter = new stdClass();
                $filter->name = JText::_('COM_STTNMLS_FILTER_NAME_'.strtoupper($key));
                switch ($key) {
                    case 'city':
                            $filter->value = $this->getFilterValue('#__sttnmlsvoccity', 'NAME', 'ID', array($a));
                            break;
                    case 'my_check':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocraion', 'NAME', 'ID', $a);
                            break;
                    case 'my_MRcheck':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocmraion', 'NAME', 'ID', $a);
                            break;

                    case 'class':
                            $filter->value = $this->getFilterValue('#__sttnmlsclass', 'title', 'id', $a);
                            break;
                    case 'furnish':
                            $filter->value = $this->getFilterValue('#__sttnmlsfurnish', 'title', 'id', $a);
                            break;

                    case 'comp':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocfirms', 'NAME', 'ID', $a);
                            break;
                    case 'wmaterid':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocalldata', 'NAME', 'ID', $a,'RAZDELID=8');
                            break;

                    case 'my_checkMW':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocalldata', 'NAME', 'ID', $a,'RAZDELID=8');
                            break;
                    case 'htype':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocalldata', 'NAME', 'ID', $a);
                            break;
                    case 'my_checkH':
                            $filter->value = $this->getFilterValue('#__sttnmlsvocalldata', 'NAME', 'ID', $a);
                            break;
                    case 'sobst':
                            $filter->value = '';
                            break;
                    case 'neprod':
                            $filter->value = '';
                            break;
                    case 'excluzive':
                            $filter->value = '';
                            break;
                    case 'price_day':
                            $filter->value = '';
                            break;
                    case 'price_hour':
                            $filter->value = '';
                            break;


                    default:
                            if(is_array($a)) {
                                    $filter->value = '';
                                    $d = '';
                                    $cnt=0;
                                    foreach ($a as $item) {
                                            if($item!='null') {
                                                    $filter->value .= $d.$item;
                                                    $d=', ';
                                                    $cnt++;
                                            }
                                    }
                                    if($cnt>1) $filter->value = '('.$filter->value.')';
                            } else {
                                    $filter->value = $a;
                            }
                            break;
                }
                $filter->returnURL = $this->getUrlff($qar, $key);
                $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter&key='.$key,false);
                $fl[] = $filter;
            }
        }
				
        if(isset($qar['newbuild']) && $qar['newbuild']) {
            $filter = new stdClass();
            $filter->name = JText::_('COM_STTNMLS_FILTER_NAME_NEWBUILD');
            $filter->value = '';
            $filter->returnURL = $this->getUrlff($qar, 'newbuild');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }

        if(isset($qar['excluzive']) && $qar['excluzive']) {
            $filter = new stdClass();
            $filter->name = 'Эксклюзив';
            $filter->value = '';
            $filter->returnURL = $this->getUrlff($qar, 'excluzive');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }

        if(isset($qar['close']) && $qar['close']) {
            $filter = new stdClass();
            $filter->name = 'Внутренняя база';
            $filter->value = '';
            $filter->returnURL = $this->getUrlff($qar, 'close');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }

        if(isset($qar['price_day']) && $qar['price_day']) {
            $filter = new stdClass();
            $filter->name = 'Посуточно';
            $filter->value = '';
            $filter->returnURL = $this->getUrlff($qar, 'price_day');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }

        if(isset($qar['price_hour']) && $qar['price_hour']) {
            $filter = new stdClass();
            $filter->name = 'Почасово';
            $filter->value = '';
            $filter->returnURL = $this->getUrlff($qar, 'price_hour');
            $filter->link = JRoute::_('index.php?option=com_sttnmls&task=rfilter',false);
            $fl[] = $filter;
        }

        $ret = '';
        $p='';
        $d='';
        if(count($fl) > 0) {
            $ret='<div class="well">';
            if(isset($qar['agent'])) unset($qar['agent']);
            if(isset($qar['agency'])) unset($qar['agency']);
            if(isset($qar['streets'])) unset($qar['streets']);
            if(isset($qar['newbuild'])) unset($qar['newbuild']);
            if(isset($qar['excluzive'])) unset($qar['excluzive']);
            if(isset($qar['price_day'])) unset($qar['price_day']);
            if(isset($qar['price_hour'])) unset($qar['price_hour']);
            if(isset($qar['close'])) unset($qar['close']);
            $returnUrl = $this->getUrlff($qar, '');
            $link = JRoute::_('index.php?option=com_sttnmls&task=rfilter&key=resetall',false) . '&view=' . $qar['view'] . '&return=' . $returnUrl;
            foreach($fl as $item) {
                $ret .= sprintf('<a href="%s&view=%s&return=%s&option=com_sttnmls" title="%s"><b>%s</b>: %s [<b style="color:red;">x</b>]</a>, ', $item->link, $qar['view'], $item->returnURL, JText::_('COM_STTNMLS_FILTER_DELKRIT'), $item->name, $item->value);
            }

            global $resetall_link;
            $resetall_link=$link;
            $ret .= '<a href="'.$link.'" >'.  mb_strtoupper(JText::_('COM_STTNMLS_FILTER_RESET_ALL')).' </a>';
            $p = JText::_('COM_STTNMLS_FILTER_MESS2').'<a href="'.$link.'" >'.JText::_('COM_STTNMLS_FILTER_RESET_ALL').' </a>';
            $ret .= '</div>';
        }
        $this->_noitemtext = '<p style="text-align:center;width:100%;padding-top:20px;">'.JText::_('COM_STTNMLS_FILTER_MESS1').'<br/>'.$p.'</p>';
        return $ret;
    }
    
    public function getNText() {
        return $this->_noitemtext;
    }
    
    public function getFilterValue($table,$fld,$wh,$ar,$dopwhere='') {
        $db = $this->getDbo();
        $query = $db->getQuery(TRUE);
        $query->select($fld);
        $query->from($table);
        $where = '';
        $d='';
        $cnt=0;
        foreach ($ar as $key => $value) {
            if($value!='null') {
                $where .= $d.$db->quote($value);
                $d=',';
                $cnt++;
            }
        }
        if($where)
                $query->where ($wh.' in ('.$where.')');
        if($dopwhere)
                $query->where ($dopwhere);
        $db->setQuery($query);
        $res = $db->loadColumn();
        $ret = implode(', ', $res);
//		if($cnt>1) $ret = '('.$ret.')';
        return $ret;
    }
    
    
	
    public function getCountobj()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = $this->getDbo();
        $user = JFactory::getUser();
        $userid = $user->id;
        $params = JComponentHelper::getParams('com_sttnmls');
        $iAmSuperAdmin	= $user->authorise('core.admin');
        $days = $params->get('days', '30');
        $agency_sobstv = $params->get('compidsob', 99001);
        $f_exclusive_only_nmls = $params->get('f_exclusive_only_nmls', 0);

        // Request VARS
        $format = $app->input->get('format', 'html', 'string');
        $view = $app->input->get('view', 'aparts', 'string');

        $newbuild = $app->input->get('newbuild', 0, 'uint');
        $builder = $app->input->get('builder', 0, 'uint');
        $building = $app->input->get('building', 0, 'uint');

        $query2	= 'SELECT * FROM '.$this->_tabname.' WHERE close=1';
        $db->setQuery($query2);
        $closeobjects	= $db->loadObjectList();
        if(count($closeobjects)>0)
        {
            foreach($closeobjects as $co)
            {
                $kd=Array();
                $query3='SELECT * FROM #__sttnmlsvocagents WHERE COMPID="'.$co->COMPID.'" AND ID="'.$co->AGENTID.'"';
                $db->setQuery($query3);
                $kods	= $db->loadObjectList();
                foreach($kods as $k)
                {
                    $kdd=explode(",",$k->prava);
                    foreach($kdd as $kk)
                    {
                        $kd[]=$kk;	
                    }
                }
                $koda="||".implode("||",array_unique($kd))."||";
                $query3='UPDATE '.$this->_tabname.' set agents="'.$koda.'" WHERE CARDNUM="'.$co->CARDNUM.'" AND COMPID="'.$co->COMPID.'" AND CITYID="'.$co->CITYID.'"';
                $db->setQuery($query3);
                $db->execute();
            }
        }

        if(!$userid){
            $editable=' 0 as editable, ';
        } else {
            if($iAmSuperAdmin) $editable = ' 1 as editable, ';
            else $editable = ' case when ag.userid='.$db->quote($userid).' then 1 else 0 end as editable, ';
        }
        $query = $db->getQuery(true);
        $query->select($this->getState('list.select', 'a.*, '.$editable.' IF(a.DATEINP<DATE_SUB(NOW(), INTERVAL IFNULL(g.mdays,0) day),f.NAME,"") as firm, m.NAME as mraion ' . $this->getDopSelect()));
        $query->from($db->quoteName($this->_tabname) . ' AS a');
        $query->join('LEFT','`#__sttnmlsvoccity` as c on a.CITYID=c.id');
        $query->join('LEFT','`#__sttnmlsvocraion` as r on a.RAIONID=r.id');
        $query->join('LEFT','`#__sttnmlsvocstreet` as s on a.STREETID=s.id');
        $query->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.id');
        $query->join('LEFT','`#__sttnmlsvocagents` as ag on ag.COMPID=f.id and ag.id=a.agentid');
        $query->join('LEFT','`#__sttnmlsgroup` as g on g.id=ag.grp');
        $query->join('LEFT','`#__sttnmlsvocmraion` as m on a.MRAIONID=m.id');
        $query->where(' ag.flag<3');
        $query->where(' ag.flag<>1');
        if(JRequest::getVar('UPD', ''))
        {
            $this->setState('list.start', 0);
        }
		
        if($_REQUEST['filter_rooms'] && $_REQUEST['view'] != 'buildings')
        {
            $rm = implode(",", $_REQUEST['filter_rooms']);
            if(count($_REQUEST['filter_rooms']) == 1)
            {
                if($_REQUEST['filter_rooms'] >= 4)
                {
                    $query->where(' a.ROOMS >="'.$_REQUEST['filter_rooms'].'"');
                }else{
                    $query->where(' a.ROOMS ="'.$_REQUEST['filter_rooms'].'"');
                }
            }
        }
		
        $street = JRequest::getInt('streets', 0);
        $agency = JRequest::getInt('agency', 0);
        $newbuild = JRequest::getInt('newbuild', 0);
        $excluzive = JRequest::getInt('excluzive', 0);
        $close = JRequest::getInt('close', 0);
        $agent = JRequest::getInt('agent', 0);
        $sobst = JRequest::getInt('sobst', 0);

        if(JRequest::getVar('RST', ''))
        {
            $this->resetFilter();
            $this->setState('list.start', 0);
            if($street) {
                JRequest::setVar('streets');
                $street = 0;
            }
			
            if($agency) {
                JRequest::setVar('agency');
                $agency = 0;
            }
            
            if($agent) {
                    JRequest::setVar('agent');
                    $agent = 0;
            }
            
            if($newbuild) {
                JRequest::setVar('newbuild');
                $newbuild = 0;
            }
            
            if($excluzive) {
                JRequest::setVar('excluzive');
                $excluzive = 0;
            }
            
            if($close) {
                JRequest::setVar('close');
                $close = 0;
            }
        }
        
        if(JRequest::getVar('filter_street','','post')==JText::_('COM_STTNMLS_STREET_SELECT') || $street)
        {
            $app->setUserState($this->context . '.filter.street', '');
            $this->setState('filter.street', '');
            JRequest::setVar('filter_street', '');
        } 
        
        if($street) $query->where(' a.streetid='.$db->quote($street));
        if($agency)
        {
            if($_REQUEST['view']=='buildings')
            {
                $query->where(' a.comp='.$db->quote($agency));
            }else{
                $query->where(' a.COMPID='.$db->quote($agency));
            }
        }
        if($newbuild) $query->where(' a.NEWBUILD='.$db->quote($newbuild));
        if($excluzive) {
            if($f_exclusive_only_nmls) {
                $query->where($db->quoteName('a.EXCLUSIVE') . '=' . $db->quote(3));
            } else {
                $query->where($db->quoteName('a.EXCLUSIVE') . '>0');
            }
        }

        $query2	= 'SELECT * FROM #__sttnmlsvocagents WHERE userid="'.$userid.'"';
        $db->setQuery($query2);
        $myagents	= $db->loadObject();
		
        if($close){
            if($myagents->boss==1)
            {
                $query->where(' ((a.agents LIKE "%|'.$myagents->kod.'|%" AND a.close=1) OR (a.COMPID="'.$myagents->COMPID.'" AND a.close=1) OR (a.AGENTID="'.$myagents->ID.'" AND a.COMPID="'.$myagents->COMPID.'" AND a.close=1))');
            }else{
                $query->where(' ((a.agents LIKE "%|'.$myagents->kod.'|%" AND a.close=1) OR (a.AGENTID="'.$myagents->ID.'" AND a.COMPID="'.$myagents->COMPID.'" AND a.close=1))');
            }
        }else{
            $query->where(' (a.agents LIKE "%|'.$myagents->kod.'|%" OR a.close=0 OR (a.AGENTID="'.$myagents->ID.'" AND a.COMPID="'.$myagents->COMPID.'"))');
        }
		
        if($agent) {
            $query->where(' a.AGENTID='.$db->quote($agent));
            // если список по одному агенту, и не он смотрит список, то в списке не должно быть объявлений на премодерации
            $query->where('( a.DATEINP<DATE_SUB(NOW(), INTERVAL IFNULL(g.mdays,0) day) or ag.userid='.$db->quote($userid).' )');
        }

        if($_REQUEST['phone'])
        {
            $db2 = $this->getDbo();
            $query2 = $db2->getQuery(true);
            $query2->select($this->getState('list.select', 'a.*, f.NAME as firm, f.realname '));
            $query2->from(' `#__sttnmlsvocagents` AS a ');
            $query2->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.id');
            $query2->join('LEFT','`#__sttnmlsgroup` as g on g.id=a.grp');
            $query2->where(' a.flag<3');
            $query2->where(' a.flag<>1');
            $query2->where(' trim(a.PHONE)<>""');
            $query2->where(' a.COMPID<>' . $db->quote($agency_sobstv));
            $query2->where(' f.active=1 ');
            $query2->where(' a.grp<>2');
            $query2->where(' a.SIRNAME!="" ');
            $query2->where(' replace(replace(replace(replace(a.phone," ",""),")",""),"(",""),"-","") LIKE "%'.$_REQUEST['phone'].'%"');
            $db2->setQuery($query2);
            $idsob	= $db2->loadObjectList();
            if(count($idsob)>0)
            {
                $qr=' (';
                foreach($idsob as $ids)
                {
                    $qr.='(a.COMPID="'.$ids->COMPID.'" AND a.AGENTID="'.$ids->ID.'") OR ';
                }
                $qr.=' replace(replace(replace(replace(replace(a.sob_tel,"+","")," ",""),")",""),"(",""),"-","") LIKE "%'.$_REQUEST['phone'].'%" OR replace(replace(replace(replace(replace(a.sob_tel2,"+","")," ",""),")",""),"(",""),"-","") LIKE "%'.$_REQUEST['phone'].'%"';
                $qr=str_replace(" OR OR","",$qr);
                $qr.=') ';
                $query->where(' '.$qr.' ');
            }else{
                $qr =' ( replace(replace(replace(replace(replace(a.sob_tel,"+","")," ",""),")",""),"(",""),"-","") LIKE "%'.$_REQUEST['phone'].'%" OR replace(replace(replace(replace(replace(a.sob_tel2,"+","")," ",""),")",""),"(",""),"-","") LIKE "%'.$_REQUEST['phone'].'%") ';
                $query->where(' '.$qr.' ');
            }
        }

        $params = JComponentHelper::getParams('com_sttnmls');
        $compidsob = $params->get('compidsob', '0');
        $sobstneprod = false;
        if($format=='raw') {
            if($sobst){
                $query->where(' f.ID='.$db->quote($compidsob));
                if($this->_seexpired) $sobstneprod = true;
            } else {
                if(!$builder OR !$building) {
                    $sobstneprod = TRUE;
                    $query->where('(ag.userid=' . $db->quote($user->id) . ' AND ag.userid>0)');
                }
            }
        }
        $this->setDopWhere($query);
        // Район -------------------------------------------------------
        $this->addWhereCheckbox('my_check', 'a.RAIONID', $query);
        // Микрорайон --------------------------------------------------
        $this->addWhereCheckbox('my_MRcheck', 'a.MRAIONID', $query);
        if($_REQUEST['view']=='buildings')
        {
            $this->addWhere('price_ot','a.pricefrom>=',$query);
            $this->addWhere('price_do','a.pricefrom<=',$query);
        }else{
            $this->addWhere('price_ot','a.PRICE>=',$query);
            $this->addWhere('price_do','a.PRICE<=',$query);
        }
        $this->addWhere('city','a.CITYID=',$query);
        $this->addWhere('street','s.NAME LIKE ',$query, true);
        $query->where(' a.flag<3 ');
        
        if($_REQUEST['view']=='buildings')
        {
            $this->addWhereCheckbox('comp','a.comp',$query);
            $this->addWhereCheckbox('furnish','a.furnish',$query);
            $this->addWhereCheckbox('class','a.class',$query);
            $this->addWhereCheckbox('wmaterid','a.WMATERID',$query);
            $this->addWhere('dateyear','a.dateyear<=',$query, false, 'array');
            $this->addWhere('datekvar','a.datekvar<=',$query, false, 'array');
        }
        
        $neprod = false;
        if($this->_moder){
            $search = $this->getState('filter.sobst',array());
            if(count($search)>1) $query->where(' f.ID='.$db->quote($compidsob));
            $search = $this->getState('filter.neprod',array());
            if(count($search)>1) $neprod=true;
        }
        // продолжительность показа
        $all = JRequest::getInt('all', 0);
        $days = $params->get('days', '30');
        if(!$neprod && !$all && !$agent && $format!='raw')
        {
            if ($_REQUEST['view'] != 'buildings') {
                $query->where(' ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
            }
        } 
        
        if($agent)
        {
            $query->where(' ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)) or ag.userid='.$db->quote($userid).' )');
        }
        
        if(!$sobstneprod && $format=='raw')
        {
            $query->where(' ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        }
        
        if($neprod  && $format!='raw')
        {
            $query->where(' ((IFNULL(g.days,0)>0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        }
        
        $ifdlit = ' IF((IFNULL(g.days,0)>0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)), "0", "1") as ifdlit ';
        $query->select($ifdlit);
        if($format=='raw' && $sobst) {
            $query->order($db->escape(' a.DATEINP DESC '));
        } else {
            $orderCol = $this->state->get('list.ordering');
            $orderDirn = $this->state->get('list.direction');
            
            $exclsort = ' a.upobj DESC, ';
            if($params->get('exclsort',0)) $exclsort = ' a.upobj DESC, IF(`EXCLUSIVE`=3,0,1), ' ;
            if($orderCol=='' || $orderCol=='ordering' ) {
                $query->order($db->escape($exclsort.'PICCOUNT>0 DESC, DATEINP DESC '));
            }
            else if($orderCol=='PICCOUNT' && strtoupper($orderDirn)=='DESC'){
                $query->order($db->escape($exclsort.'PICCOUNT>0 DESC, DATEINP DESC '));
            }
            else if($orderCol=='a.DATEINP' && strtoupper($orderDirn)=='ASC'){
                $query->order($db->escape(' a.DATEINP DESC '));
            }else if($orderCol=='a.DATEINP' && strtoupper($orderDirn)=='DESC'){
                $query->order($db->escape(' a.DATEINP ASC '));	
            } else
                $query->order($db->escape($orderCol.' '.$orderDirn));
        }

        if($_REQUEST['what']==20 && $_REQUEST['view']=='houses')
        {
            $query=str_replace("a.WHAT>0","a.WHAT>=0",$query);	
        }
		
        if(count($_REQUEST['filter_rooms'])>0 && $_REQUEST['view']=='buildings')
        {
            if($_REQUEST['filter_rooms'][0]==null)
            {
                unset($_REQUEST['filter_rooms'][array_search(null,$_REQUEST['filter_rooms'])]);
            }
            $r='AND a.ROOMS in ('.implode(",",$_REQUEST['filter_rooms']).')';
            $q='AND (';
            foreach($_REQUEST['filter_rooms'] as $fr)
            {
                $q.='a.type LIKE "%,'.$fr.',%" OR ';
            }
            $q.='||)';
            $q=str_replace(' OR ||','',$q);
            $query=str_replace($r,$q,$query);
        }

        // Поиск по застройщику
        if($view == 'aparts' && $builder && $building) {
            $query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.aparts_compid') . '=' . $db->quoteName('a.COMPID') . ' AND ' . $db->quoteName('barel.aparts_cardnum') . '=' . $db->quoteName('a.CARDNUM') . ')');
            $query->where('(' . $db->quoteName('barel.builder') . '=' . $db->quote($builder) . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quote($building) . ')');
        }

        $db->setQuery($query);
        $countobjects	= $db->loadObjectList();

        return count($countobjects);
    }
	
    public function setUpd()
    {
        if(JRequest::getVar('UPD', '')){
            $this->_upd = true;
        }
        // разрешить обновление фильтров, если запущено прямо из меню, а не при переходе на страницу
        if(JRequest::getVar('limitstart', 'no') == 'no' && JRequest::getVar('start', 'no') == 'no'){
            $this->_upd = false;
        }

        if(isset($_REQUEST['RST']))
        {
            $this->_upd = true;
        }
    }
    
    protected function populateState($ordering = null, $direction = null)
    {
        // Initialise variables.
        $app	= JFactory::getApplication();
        $this->setUpd();
        $format = JRequest::getVar('format','html');
        $sobst = JRequest::getInt('sobst', 0);

        if($format=='raw') {
            if($sobst){
                $this->setState('list.limit', 500);
            } else {
                $this->setState('list.limit', 1000);
            }
            $this->setState('list.start', 0);
        } else {
            if($_REQUEST['act']!='pdf')
            {
                // List state information
                $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
                $this->setState('list.limit', $limit);

                $limitstart = JRequest::getUInt('limitstart', 0);
                $this->setState('list.start', $limitstart);
            }
        }

        $orderCol = $this->getUserStateFromRequest($this->context . '.order', 'filter_order', 'ordering');
        $this->setState('list.ordering', $orderCol);

        $listOrder = $this->getUserStateFromRequest($this->context . '.orderdir', 'filter_order_Dir', $direction);
        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
            $listOrder = 'ASC';
        }
        $this->setState('list.direction', $listOrder);

        if($this->_moder) {
            $this->setFilterState('sobst',array());
            $this->setFilterState('neprod',array());
        }
 
        // Хак меню выбора по типам объектов коммерческой недвижимости ---------
        if(isset($_REQUEST['filter_htype']) && !is_array($_REQUEST['filter_htype']) && JRequest::getInt('filter_htype', 0)) {
            $this->_upd = TRUE;
            $this->setFilterState('htype', array(0 => 'null', 1 => JRequest::getInt('filter_htype')));
            $this->_upd = FALSE;
        } else {
            $this->setFilterState('htype', array());
        }
        // ---------------------------------------------------------------------
        
        $this->setFilterState('rooms', array());
        $this->setFilterState('my_check', array());
        $this->setFilterState('my_MRcheck', array());
        $this->setFilterState('my_checkMW', array());
        $this->setFilterState('my_checkH', array());
        $this->setFilterState('area_ot', 0);
        $this->setFilterState('area_do', 0);
        $this->setFilterState('kitch_ot', 0);
        $this->setFilterState('kitch_do', 0);
        $this->setFilterState('earea_ot', 0);
        $this->setFilterState('earea_do', 0);
        $this->setFilterState('price_ot', 0);
        $this->setFilterState('price_do', 0);
        $this->setFilterState('view_type', 0);

        $this->setFilterState('excluzive', array());
        $this->setFilterState('close', array());

		
        if($_REQUEST['view'] == 'buildings')
        {
            $this->setFilterState('comp', array());
            $this->setFilterState('class', array());
            $this->setFilterState('furnish', array());
            $this->setFilterState('datekvar', array());
            $this->setFilterState('dateyear', array());
            $this->setFilterState('wmaterid', array());
        }
		
        $this->setFilterState('street','');
        $params = JComponentHelper::getParams('com_sttnmls');
//		$this->setFilterState('city', $params->get('cityid', '4601'));
        $this->setFilterState('city', 0);
    } 
    
    public function setFilterState($par, $default = null)
    {
        if($this->_upd) {
            $a = $this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
        } else {
            $app = JFactory::getApplication();
            $a = $app->getUserState($this->context . '.filter.'.$par);
        }
        if(is_array($default) && !is_array($a)) {
            $d=array();
            $d[]=$a;
        }
        else $d=$a;
        
        $this->setState('filter.' .$par, $d);
    }
    
    public function addWhereCheckbox($par,$fld,$query)
    {
        $format = JRequest::getVar('format','html');
        if($format=='raw') return;
        $where = '';
        $search = $this->getState('filter.'.$par);
        if (!empty($search)) {
            foreach($search as $key=>$checkbox)
            {
                if(intval($checkbox)){ $where .= intval($checkbox).','; }
            }
            $where=substr($where,0,strlen($where)-1);
            if ($where != '') $query->where($fld .' in (' . $where . ')');
        }
    }
    
    public function addWhere($par,$fld,$query,$like=false, $arr=false)
    {
        $format = JRequest::getVar('format','html');
        if($format=='raw') return;
        if($par)
        $search = $this->getState('filter.'.$par);
        if($search)
        {
            if($arr)
            {
                if($search[0]>0)
                {
                    $query->where($fld . $search[0] );	
                }
            }else{
                if($like) $search = "'$search%'";
                $query->where($fld . $search );	
            }
        }
    }
	
    public function resetFilter($key='')
    {
        $app = JFactory::getApplication();
        if($key=='city' || !$key) {
            $app->setUserState($this->context . '.filter.city', 0);
            $this->setState('filter.city', 0);
        }
        
        if($_REQUEST['view']=='buildings')
        {
            if($key=='comp' || !$key) {
                    $app->setUserState($this->context . '.filter.comp', null);
                    $this->setState('filter.comp', -1);
            }
            if($key=='class' || !$key) {
                    $app->setUserState($this->context . '.filter.class', null);
                    $this->setState('filter.class', -1);
            }
            if($key=='datekvar' || !$key) {
                    $app->setUserState($this->context . '.filter.datekvar', null);
                    $this->setState('filter.datekvar', -1);
            }
            if($key=='dateyear' || !$key) {
                    $app->setUserState($this->context . '.filter.dateyear', null);
                    $this->setState('filter.dateyear', -1);
            }
            if($key=='wmaterid' || !$key) {
                    $app->setUserState($this->context . '.filter.wmaterid', null);
                    $this->setState('filter.wmaterid', -1);
            }
            if($key=='furnish' || !$key) {
                    $app->setUserState($this->context . '.filter.furnish', null);
                    $this->setState('filter.furnish', -1);
            }
        }

        if($key=='htype' || !$key) {
            $app->setUserState($this->context . '.filter.htype', null);
            $this->setState('filter.htype', array());
        }
        if($key=='rooms' || !$key) {
            $app->setUserState($this->context . '.filter.rooms', null);
            $this->setState('filter.rooms', array());
        }
        if($key=='my_check' || !$key) {
            $app->setUserState($this->context . '.filter.my_check', null);
            $this->setState('filter.my_check', array());
        }
        if($key=='my_checkH' || !$key) {
            $app->setUserState($this->context . '.filter.my_checkH', null);
            $this->setState('filter.my_checkH', array());
        }
        if($key=='my_MRcheck' || !$key) {
            $app->setUserState($this->context . '.filter.my_MRcheck', null);
            $this->setState('filter.my_MRcheck', array());
        }
        if($key=='my_checkMW' || !$key) {
            $app->setUserState($this->context . '.filter.my_checkMW', null);
            $this->setState('filter.my_checkMW', array());
        }
        if($key=='area_ot' || !$key) {
            $app->setUserState($this->context . '.filter.area_ot', null);
            $this->setState('filter.area_ot', 0);
        }
        if($key=='area_do' || !$key) {
            $app->setUserState($this->context . '.filter.area_do', null);
            $this->setState('filter.area_do', 0);
        }
        if($key=='kitch_ot' || !$key) {
            $app->setUserState($this->context . '.filter.kitch_ot', null);
            $this->setState('filter.kitch_ot', 0);
        }
        if($key=='kitch_do' || !$key) {
            $app->setUserState($this->context . '.filter.kitch_do', null);
            $this->setState('filter.kitch_do', 0);
        }
        if($key=='earea_ot' || !$key) {
            $app->setUserState($this->context . '.filter.earea_ot', null);
            $this->setState('filter.earea_ot', 0);
        }
        if($key=='earea_do' || !$key) {
            $app->setUserState($this->context . '.filter.earea_do', null);
            $this->setState('filter.earea_do', 0);
        }
        if($key=='price_ot' || !$key) {
            $app->setUserState($this->context . '.filter.price_ot', null);
            $this->setState('filter.price_ot', 0);
        }
        if($key=='price_do' || !$key) {
            $app->setUserState($this->context . '.filter.price_do', null);
            $this->setState('filter.price_do', 0);
        }


        if(!$key) {
            $app->setUserState($this->context . '.order', 'ordering');
            $app->setUserState($this->context . '.orderdir', '');
        }
        if(!$key) {
            $this->setState('list.ordering', 'ordering');
            $this->setState('list.direction', '');
        }


        if($key=='sobst' || !$key) {
            $app->setUserState($this->context . '.filter.sobst', null);
            $this->setState('filter.sobst', array());
        }
        if($key=='neprod' || !$key) {
            $app->setUserState($this->context . '.filter.neprod', null);
            $this->setState('filter.neprod', array());
        }
        if($key=='street' || !$key) {
            $app->setUserState($this->context . '.filter.street', '');
            $this->setState('filter.street', '');
        }


        if($key=='excluzive' || !$key) {
            $app->setUserState($this->context . '.filter.excluzive', '');
            $this->setState('filter.excluzive', '');
        }

        if($key=='price_day' OR !$key) {
            $app->setUserState($this->context . '.filter.price_day', 0);
            $this->setState('filter.price_day', 0);
        }

        if($key=='price_hour' OR !$key) {
            $app->setUserState($this->context . '.filter.price_hour', 0);
            $this->setState('filter.price_hour', 0);
        }

        if($key=='close' || !$key) {
            $app->setUserState($this->context . '.filter.close', '');
            $this->setState('filter.close', '');
        }
    }
	
    public function getRaions()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select(' a.ID as value, a.NAME as text ');
        $query->from(' #__sttnmlsvocraion as a ');
        $this->addWhere('city','a.CITYID=',$query);
        $query->where(' a.CITYID<>-1 ');
        $query->order('CITYID');
        $query->order('ID');
        $db->setQuery($query);
        return $db->loadObjectList();
    }
    
    public function getMRaions()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select(' a.ID as value, a.NAME as text ');
        $query->from(' #__sttnmlsvocmraion as a ');
        $this->addWhereCheckbox('my_check', 'a.RAIONID', $query);
        $this->addWhere('city','a.CITYID=',$query);
        $query->where(' a.CITYID<>-1 ');
        $db->setQuery($query);
        return $db->loadObjectList();
    }
    
    public function getOptCity()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        return SttNmlsHelper::getOptCity();
    }
        
    public function setTabname($s)
    {
        $this->_tabname=$s;
    }
    
    public function getTp()
    {
        return $this->_tp;
    }
    
    public function setTp($s)
    {
        $this->_tp=$s;
    }
    
    public function getDopSelect()
    {
        // для неквартир эту функцию нужно переопределить в модели
        return ',c.NAME as gorod,
                    r.NAME as raion,
                    concat(s.NAME,if(a.HAAP <> " ",	CONCAT(",",a.HAAP), "")) as ulica,
                    if(a.WHAT = 0,"квартира","комната") AS object,
                    d4.NAME as tip,
                    CONCAT(a.STAGE, "/",a.HSTAGE,"-",d8.SHORTNAME) AS planirovka,
        CONCAT(TRUNCATE(a.AAREA,0), "/", TRUNCATE(a.LAREA,0), "/", TRUNCATE(a.KAREA,0)) AS o_zh_k ';
    }
    
    public function setDopWhere($query)
    {
        // здесь указываем дополнительные условия и джойны для запроса
        // для неквартир эту функцию нужно переопределить в модели
        $query->join('LEFT','`#__sttnmlsvocalldata` as d4 on a.HTYPEID=d4.id and d4.RAZDELID=4');
        $query->join('LEFT','`#__sttnmlsvocalldata` as d8 on a.WMATERID=d8.id and d8.RAZDELID=8');
        // Описание комнат ---------------------------------------------
        $this->addWhereCheckbox('rooms', 'a.ROOMS', $query);
        // Материал стен -----------------------------------------------
        $this->addWhereCheckbox('my_checkMW', 'a.WMATERID', $query);



        if($_REQUEST['view']=='buildings')
        {
            $this->addWhere('area_ot','a.areafrom>=',$query);
            $this->addWhere('area_do','a.areafrom<=',$query);
        }else{
            $this->addWhere('area_ot','a.AAREA>=',$query);
            $this->addWhere('area_do','a.AAREA<=',$query);
        }
		
        if(JRequest::getInt('stype',0)) $query->where(' a.VARIANT=2 ');
        else $query->where(' a.VARIANT<>2 ');
        $what = JRequest::getInt('what',0);
        if($what==10) {
            $query->where(' a.NEWBUILD>0 ');
            $this->addWhere('kitch_ot','a.KAREA>=',$query);
            $this->addWhere('kitch_do','a.KAREA<=',$query);
            return;
        }
		
        if($what) {
            if($what==20)
            {
                $query->where(' a.WHAT>=0 ');
            }else{
                $query->where(' a.WHAT>0 ');
            }
        }
        else {
            $query->where(' a.WHAT=0 ');
            $this->addWhere('kitch_ot','a.KAREA>=',$query);
            $this->addWhere('kitch_do','a.KAREA<=',$query);
        }
        return;
    }
    
    public function getPagination() 
    {
        $p = parent::getPagination();
        $street = JRequest::getInt('streets', 0);
        $agency = JRequest::getInt('agency', 0);
        $agent = JRequest::getInt('agent', 0);
        $newbuild = JRequest::getInt('newbuild', 0);
        $excluzive = JRequest::getInt('excluzive', 0);
        $price_day = JRequest::getInt('price_day', 0);
        $price_hour = JRequest::getInt('price_hour', 0);
        $close = JRequest::getInt('close', 0);

        $sobst = JRequest::getInt('sobst', 0);
        if($street) {
                $p->setAdditionalUrlParam('streets', $street);
        }
        if($agency) {
                $p->setAdditionalUrlParam('agency', $agency);
        }
        if($agent) {
                $p->setAdditionalUrlParam('agent', $agent);
        }
        if($newbuild) {
                $p->setAdditionalUrlParam('newbuild', $newbuild);
        }

        if($excluzive) {
                $p->setAdditionalUrlParam('excluzive', $excluzive);
        }

        if($price_day) {
            $p->setAdditionalUrlParam('price_day', $price_day);
        }

        if($price_hour) {
            $p->setAdditionalUrlParam('price_hour', $price_hour);
        }

        if($close) {
                $p->setAdditionalUrlParam('close', $close);
        }

        if($sobst) {
                $p->setAdditionalUrlParam('sobst', $sobst);
        }
        return $p;
    }
    
    /**
     * Получение свойств объектов по их ID
     * 
     * @param   integer|array of integer    $option_id - ID свойства/свойств
     * @return  object|array of object
     */
    public function getOptionsByID($option_id, $razdel = 0)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsvocalldata'));
        $query->order($db->quoteName('NAME') . ' ASC');
        if($razdel) {
            $query->where($db->quoteName('RAZDELID') . '=' . $db->quote($razdel));
        }

        if(is_array($option_id) && count($option_id) > 0) {
            $query->where($db->quoteName('ID') . ' IN (' . implode(',', $option_id) . ')');
            $db->setQuery($query);
            $result = $db->loadObjectList();
        } else {
            $query->where($db->quoteName('ID') . '=' . $db->quote($option_id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }
        return $result;
    }
    
    public function logInfo ($text, $type = 'message') {
        $file = JPATH_ROOT . "/logs/sttnmls.log";
        $date = JFactory::getDate ();

        $fp = fopen ($file, 'a');
        fwrite ($fp, "\n\n" . $date->toFormat ('%Y-%m-%d %H:%M:%S'));
        fwrite ($fp, "\n" . $type . ': ' . $text);
        fclose ($fp);
    } 
}
