<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;

// Getting vars
$street = $app->input->getUint('street_id', 0);
$agency = $app->input->getUint('agency', 0);
$agent = $app->input->getUint('agent', 0);
$stype = $app->input->getUint('stype', 0);
$what = $app->input->getUint('what', 0);

//$cookie = JFactory::getApplication()->input->cookie;
//$cookie->set('viewType', "");
$filter_view_type = SttNmlsHelper::getViewType();

// Обработка непродленных объектов
$_have_expired_objects = false;
$expired_compid = 0;
if($this->items) {
    foreach($this->items as $item) {
        if(!$item->expired && ($item->flag == 1 OR $item->flag == 2)) {
            $_have_expired_objects = true;
            $expired_compid = $item->COMPID;
            break;
        }
    }
}

?>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <h1>
                <?php
                $title = '';
                if(JRequest::getString('titleh1') != '') {
                    $title = JRequest::getString('titleh1');
                } else {
                    switch ($what) {
                        case 0:
                            $title = JText::_('COM_STTNMLS_APARTS_HEAD' . $stype);
                            break;

                        case 10:
                            $title = JText::_('COM_STTNMLS_APARTS_HEADN' . $stype);
                            break;

                        default:
                            $title = JText::_('COM_STTNMLS_APARTS_HEADR' . $stype);

                            break;
                    }
                }
                printf('%s (%d)', $title, $this->countobj);
                ?>
            </h1>
        </div>
        <form action="<?php echo JUri::current() ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal filters" target="_self">
            <?php if($street) : ?>
                <input type="hidden" name="streets" value="<?php echo $street; ?>">
            <?php endif; ?>
            <?php if($agency) : ?>
                <input type="hidden" name="agency" value="<?php echo $agency; ?>">
            <?php endif; ?>
            <?php if($agent) : ?>
                <input type="hidden" name="agent" value="<?php echo $agent; ?>">
            <?php endif; ?>
            <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
            <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />

            <?php echo (($this->raw) ? '' : $this->loadTemplate('filter')) ?>

            <div class="row-fluid clearfix view-type-select" style="margin: 10px 0;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 col-xs-3 text-left">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default btn-sm<?php echo (($filter_view_type == 0) ? ' active' : '') ?>" data-toggle="tooltip" data-placement="top" title="Стандартный вид">
                                    <input type="radio" name="filter_easy" value="0" autocomplete="off"<?php echo (($filter_view_type == 0) ? ' checked="checked"' : '') ?> />
                                    <span class="glyphicon glyphicon-th-list"></span>
                                </label>
                                <label class="btn btn-default btn-sm<?php echo (($filter_view_type == 1) ? ' active' : '') ?>" data-toggle="tooltip" data-placement="top" title="Адаптивный вид">
                                    <input type="radio" name="filter_easy" value="1" autocomplete="off"<?php echo (($filter_view_type == 1) ? ' checked="checked"' : '') ?> />
                                    <span class="glyphicon glyphicon-th"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-8 col-xs-9 text-right">
                            <?php if ($_have_expired_objects && !$this->access->isAdmin): ?>
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-info btn-sm changeExpiredObjectChangeDate"
                                    data-table="aparts"
                                    data-stype="<?php echo $this->stype ?>"
                                    data-compid="<?php echo $expired_compid ?>"
                                    data-message="<?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE_ALL'); ?>"
                                >
                                    <span class="glyphicon glyphicon-time"></span>
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE_ALL'); ?>
                                </a>
                            <?php endif; ?>

                            <div class="btn-group">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">
                                        <span class="glyphicon glyphicon-print"></span>
                                        <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRINT_ALL', $this->countobj) ?> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu left-dropdown-menu" role="menu">
                                        <li>
                                            <a href="javascript:void(0);" onclick="document.getElementById('adminForm').target='_blank'; document.getElementById('typepdf').value='cli'; document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_CLIENTS') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" onclick="document.getElementById('adminForm').target='_blank'; document.getElementById('typepdf').value=''; document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_AGENTS') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" onclick=" document.getElementById('adminForm').target='_blank'; document.getElementById('typepdf').value='agcli';  document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_AGENTS_WITH_CONTACTS') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">
                                        <span class="glyphicon glyphicon-star"></span>
                                        <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRINT_FAVORITES', 'countFavorTp1', 0) ?> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu right-dropdown-menu" role="menu">
                                        <li>
                                            <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&format=raw&typepdf=cli&print=1&tp=1') ?>">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_CLIENTS') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&format=raw&print=1&tp=1') ?>">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_AGENTS') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a  target="_blank" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites', '&format=raw&typepdf=agcli&print=1&tp=1') ?>">
                                                <?php echo JText::_('COM_STTNMLS_LABEL_FAVORITES_PRINT_AGENTS_WITH_CONTACTS') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <?php echo $this->loadTemplate('normal_' . (($filter_view_type == 1) ? 'list' : 'standart') . '_view'); ?>
            </div>

            <div class="row-fluid text-center">
                <?php echo $this->pagination->getListFooter(); ?>
            </div>

        </form>
    </div>
</div>
<div class="col-md-12">
    <?php
	if($this->pagination->get('pages.current',1) == 1) {
            $db = JFactory::getDbo();
            $query	= 'SELECT * FROM #__content WHERE id="'.JRequest::getVar('article_id').'"';
            $db->setQuery($query);
            $aftertext	= $db->loadObjectList();
            echo $aftertext[0]->introtext."<br>".$aftertext[0]->fulltext;
	}
    ?>
</div>