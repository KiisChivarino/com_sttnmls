<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);


// Additional component JS
$doc->addScript("http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU");
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.yandex.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.agent_table_info.js?');

$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$addr = str_replace('"',"'",$this->item->addr);
$script .= <<<JS
    var g_u = "{$addr}";
    var _zoom = "15"; 
    ymaps.ready(init);
    jQuery('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});

JS;
$doc->addScriptDeclaration($script);



$socbut = $params->get('socbut', '');
$gild = $params->get('gild', '');
$gildurl = $params->get('gildurl', '');

?>

<div class="container-fluid">
    <!-- ROW #1: Page header -->
    <div class="row">
        <div class="col-md-12">
            <h1><?php echo $this->item->NAME; ?></h1>
            <p>
                <?php echo JText::_('COM_STTNMLS_SPHER'); ?>: <?php echo $this->cats; ?>
            </p>
        <?php if(SttNmlsHelper::chkUserEditFirm($this->item->ID) OR SttNmlsHelper::chkUserEditFirm(0)) : ?>
            <div class="well well-sm">
                <?php if(SttNmlsHelper::chkUserEditFirm($this->item->ID)) : ?>
                    <a class="btn btn-warning btn-xs" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->ID, 'edit') ?>">
                        <span class="glyphicon glyphicon-edit"></span>
                        <?php echo JText::_('COM_STTNMLS_FIRM_LINK_EDIT'); ?>
                    </a>
                <?php endif; ?>

                <?php if(SttNmlsHelper::chkUserEditFirm(0)) : ?>
                    <a class="btn btn-success btn-xs" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '', 'edit') ?>">
                        <span class="glyphicon glyphicon-plus"></span>
                        <?php echo JText::_('COM_STTNMLS_BUTTON_CREATE_NEW_FIRM') ?>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        </div>
    </div>
    <!-- /ROW #1: Page header -->

    <!-- ROW #2: Page header -->
    <div class="row">
        <div class="col-md-12">
            <h3><?php echo JText::_('COM_STTNMLS_LABEL_ADDRESS') ?></h3>
            <div class="row">
                <!-- Col #1: Yandex map -->
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div id="YMapsID" style="height:215px;"></div>
                    </div>
                </div>
                <!-- /Col #1: Yandex map -->

                <!-- Col #2: Firm contacts info -->
                <div class="col-sm-6 col-md-8">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <!--<img src="/components/com_sttnmls/assets/images/adres.png" />-->
                            <span class="firm glyphicon glyphicon-flag"></span>
                            <?php if($this->item->addr == '') : ?>
                                <?php echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
                            <?php else : ?>
                                <?php echo $this->item->addr ?>
                            <?php endif; ?>
                        </li>
                        <li class="list-group-item">
                            <!--<img src="/components/com_sttnmls/assets/images/telefon.png" />-->                            
                            <span class="firm glyphicon glyphicon-phone"></span>
                            <?php if($this->item->PHONE == '' && $this->item->ADDPHONE == '') : ?>
                                <?php //echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
                            <?php else : ?>
                                <?php //printf('%s %s', $this->item->PHONE, $this->item->ADDPHONE) ?>
                                <a href="<?php echo 'tel:'.$this->item->PHONE?>" title = "Телефон для связи" class="btn btn-success"><strong><?php echo $this->item->PHONE ?></strong></a>
                                <?php if($this->item->ADDPHONE and ereg("[^ ]", $this->item->ADDPHONE)) :?>
                                <a href="<?php echo 'tel:'.$this->item->ADDPHONE?>" title = "Скайп" class="btn btn-success"><strong><?php  echo $this->item->ADDPHONE ?></strong></a>
                                <?php endif;?>
                            <?php endif; ?>
                        </li>
                        <li class="list-group-item">
                            <!--<img  src="/components/com_sttnmls/assets/images/email.png" />-->
                            <span class="firm glyphicon glyphicon-envelope"></span>
                            <?php if($this->item->EMAIL == '') : ?>
                                <?php echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
                            <?php else : ?>
                                <a href="<?php printf('mailto:%s', $this->item->EMAIL) ?>">
                                    <?php echo $this->item->EMAIL ?>
                                </a>
                            <?php endif; ?>
                        </li>
                        <?php if($this->item->ISITE == '') : ?>
                        <?php else : ?>
                        <li class="list-group-item">
                            <!--<img  src="/components/com_sttnmls/assets/images/site.png" />-->
                            <span class="firm glyphicon glyphicon-link"></span>
                            
                                 
                                <!-- noindex -->
                                <a href="<?php echo ((preg_match('#^(http|https)#i', $this->item->ISITE)) ? '' : 'http://') . $this->item->ISITE  ?>" rel="nofollow">
                                    <?php echo $this->item->ISITE ?>
                                </a>
                                <!-- noindex -->
                            
                        </li>
                        <?php endif; ?>
                    <?php if ($this->item->gild == 1 && $gild && $gildurl) : ?>
                        <li class="list-group-item">
                            <img  src="<?php echo JURI::root() . $params->get('guild_logo', 'components/com_sttnmls/assets/images/kogr2.png') ?>" />
                            <!-- noindex -->
                            <a href="<?php echo $gildurl ?>" rel="nofollow" >
                                <?php echo $gild ?>
                            </a>
                            <!-- /noindex -->
                        </li>
                    <?php endif; ?>
                    </ul>
                </div>
                <!-- /Col #2: Firm contacts info -->
            </div>
        </div>
    </div>
    <!-- /ROW #2: Page header -->

    <!-- ROW #3: Objects counters -->
    <div class="row">
        <div class="col-sm-4 col-md-3">
            <h3>&nbsp;</h3>
            <div class="thumbnail">
                <img class="imgfirm" src="<?php echo $this->item->imgfirm; ?>" alt="<?php echo $this->item->NAME ?>" />
            </div>
        </div>

        <div class="col-sm-8 col-md-9">
            <h3><?php echo JText::_('COM_STTNMLS_LABEL_FIRM_OBJECTS') ?></h3>
            <div style="color: #365d7e;">
                <span class="showSellTable showTableLable selected">Продажа
                    <span class=""><?php echo '('.$this->item->st_cntsale.')'; ?></span>
                </span>
                                             
                <span class="showRentTable showTableLable">Аренда
                    <span><?php echo '('.$this->item->st_cntrent.')'; ?></span>
                </span>
            </div>
            <div class="table-responsive">
            <table class="table" id="newSellTable">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="text-center" style="width: 30px;">&nbsp;</th>
                                    <?php if($this->canViewRealtorsBase) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>">
                                                <span class="glyphicon glyphicon-flag"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
									<?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?>">
                                                <span class="glyphicon glyphicon-time"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_EXPIRED') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <th class="text-center" style="width: 30px;">
                                            <div class="hasTooltip" title="<?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?>">
                                                <span class="glyphicon glyphicon-lock"></span>
                                                <span class="sr-only"><?php echo JText::_('COM_STTNMLS_LABEL_STATE_HIDE') ?></span>
                                            </div>
                                        </th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <!--Квартиры продажа-->
                                <tr id="flatsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID ) ?>">
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID) ?>">
                                            <?php echo $this->item->cntkv ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkv_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkv_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkv_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Квартиры аренда-->
                                <tr id="flatsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_APARTS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntkvar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkvar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkvar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkvar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты продажа-->
                                <tr id="roomsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo $this->item->cntkm ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&what=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkm_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&what=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkm_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&what=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkm_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Комнаты аренда-->
                                <tr id="roomsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_ROOMSS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntkmar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntkmar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntkmar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&agency=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntkmar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома продажа-->
                                <tr id="housesSale">
                                    <td>
                                        <a class="text-center" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0') ?>" >
                                            <?php echo ($this->item->cnths - $this->item->cntuch) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_realtors_and_open=1') ?>" >
                                                <?php echo ($this->item->cnths_r - $this->item->cntuch_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_expired=1') ?>" >
                                                <?php echo ($this->item->cnths_expired - $this->item->cntuch_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_closed=1') ?>" >
                                                <?php echo ($this->item->cnths_c - $this->item->cntuch_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Дома аренда-->
                                <tr id="housesRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0' . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_HOUSES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0'. '&stype=1') ?>" >
                                            <?php echo ($this->item->cnthsar - $this->item->cntuchar) ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_realtors_and_open=1'. '&stype=1') ?>" >
                                                <?php echo ($this->item->cnthsar_r - $this->item->cntuchar_r) ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_expired=1'. '&stype=1') ?>" >
                                                <?php echo ($this->item->cnthsar_expired - $this->item->cntuchar_expired) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=0&f_closed=1') ?>" >
                                                <?php echo ($this->item->cnthsar_c - $this->item->cntuchar_c) ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки продажа-->
                                <tr id="landsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1') ?>" >
                                            <?php echo $this->item->cntuch ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntuch_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntuch_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntuch_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Участки аренда-->
                                <tr id="landsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1' . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_UCHS'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1' . '&stype=1') ?>" >
                                            <?php echo $this->item->cntuchar ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_realtors_and_open=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_expired=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&agency=' . $this->item->ID . '&what=1&f_closed=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntuchar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи продажа-->
                                <tr id="garagesSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID) ?>" >
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID) ?>" >
                                            <?php echo $this->item->cntgr ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntgr_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntgr_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_closed=1') ?>">
                                                <?php echo $this->item->cntgr_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Гаражи аренда-->
                                <tr id="garagesRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('COM_STTNMLS_GARAGES'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntgrar ?>
                                        </a>
                                    </td>
									
									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_realtors_and_open=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_expired=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&agency=' . $this->item->ID . '&f_closed=1' . '&stype=1') ?>" >
                                                <?php echo $this->item->cntgrar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость продажа-->
                                <tr id="comsSale">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID) ?>" >
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID) ?>" >
                                            <?php echo $this->item->cntcom ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntcom_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&f_expired=1') ?>" >
                                                <?php echo $this->item->cntcom_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&f_closed=1') ?>" >
                                                <?php echo $this->item->cntcom_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <!--Коммерческая недвижимость аренда-->
                                <tr id="comsRent" style="display: none">
                                    <td>
                                        <a class="" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo JText::_('Коммерческая'); ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&stype=1') ?>" >
                                            <?php echo $this->item->cntcom1 ?>
                                        </a>
                                    </td>

									<?php if($this->canViewRealtorsBase) : ?>
										<td class="text-center">
											<a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&stype=1&f_realtors_and_open=1') ?>" >
                                                <?php echo $this->item->cntcomar_r ?>
                                            </a>
										</td>
									<?php endif; ?>
									
                                    <?php if($this->isAdmin OR $this->canViewExpired) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&stype=1&f_expired=1') ?>" >
                                                <?php echo $this->item->cntcom1_expired ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>

                                    <?php if($this->isAdmin OR $this->item->showclose) : ?>
                                        <td class="text-center">
                                            <a class="btn btn-link btn-xs " href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&agency=' . $this->item->ID . '&stype=1&f_closed=1') ?>" >
                                                <?php echo $this->item->cntcomar_c ?>
                                            </a>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
    <!-- /ROW #3: Objects counters -->

    <?php if($this->item->fil1 OR $this->item->fil2 OR $this->item->fil3) : ?>
    <!-- ROW #4: Filials -->
    <div class="row">
        <div class="col-md-12">
            <h3><?php echo JText::_('COM_STTNMLS_LABEL_FILIALS') ?></h3>
        <?php if($this->item->fil1) : ?>
            <p class="text-justify">
                <?php echo $this->item->fil1 ?>
            </p>
        <?php endif; ?>

        <?php if($this->item->fil2) : ?>
            <p class="text-justify">
                <?php echo $this->item->fil2 ?>
            </p>
        <?php endif; ?>

        <?php if($this->item->fil3) : ?>
            <p class="text-justify">
                <?php echo $this->item->fil3 ?>
            </p>
        <?php endif; ?>
        </div>
    </div>
    <!-- /ROW #4: Filials -->
    <?php endif; ?>

    <?php if($this->item->descr) : ?>
    <!-- ROW #5: Firm description -->
    <div class="row">
        <div class="col-md-12">
            <h3><?php echo JText::_('COM_STTNMLS_LABEL_ADDITIONAL_INFO') ?></h3>
            <div>
                <?php echo $this->item->descr; ?>
            </div>
        </div>
    </div>
    <!-- /ROW #5: Firm description -->
    <?php endif; ?>

    <!-- ROW #6: Members -->
    <div class="row">
        <div class="col-md-12">
            <h3><?php echo JText::_('COM_STTNMLS_SOTR'); ?></h3>
            <ul class="list-group">
            <?php if($this->agents) : ?>
                <?php foreach($this->agents as $idx => $agent) : ?>
                
                <?php
                    if($agent->boss == 1 && $agent->userid == $this->user->id && $this->user->id > 0) {
                        $this->isAdmin = 1;
                    }

                    // Пропустить отображение неподтвержденного агента, если пользователь не босс и не админ
                    if($agent->flag == 1 && !$this->isAdmin) {
                        continue;
                    }
                ?>


                <li class="list-group-item" id="row<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>">
                    <div class="row">
                        <div class="col-md-1">
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $agent->ID . '&comp_id=' . $this->item->ID) ?>">
                                <img class="firm normal img-rounded" src="<?php echo $agent->foto; ?>" alt="" />
                                <img class="firm big img-rounded" src="<?php echo $agent->bigFoto; ?>" alt="" />
                            </a>
                        </div>
                        <div class="col-md-8">
                            <a class="firm fio" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $agent->ID . '&comp_id=' . $this->item->ID) ?>">
                                <?php printf('%s %s %s', $agent->SIRNAME, $agent->NAME, $agent->SECNAME) ?>
                            </a>

                        <?php if($this->isAdmin && ($agent->boss OR $agent->flag == 1 OR $agent->userid)) : ?>
                            <div class="firm btn-group" role="group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expand="false">
                                    <span class="glyphicon glyphicon-cog"></span>
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE') ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php if($agent->boss) : ?>
                                    <?php // Если босс, то текущий агент в перечислении - босс, добавить кнопку смены должности ?>
                                    <li>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target=".changeAgentPostContainer<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>">
                                            <span class="glyphicon glyphicon-king"></span>
                                            <?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE_POST') ?>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if($agent->flag == 1) : ?>
                                    <li>
                                        <a href="javascript:void(0);" class="confirmAgent" data-compid="<?php echo $this->item->ID ?>" data-agent-id="<?php echo $agent->kod ?>">
                                            <span class="glyphicon glyphicon-flag"></span>
                                            <?php echo JText::_('COM_STTNMLS_BUTTON_CONFIRM_AGENT') ?>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if($agent->userid) : ?>
                                    <li>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target=".changeAgentPasswordContainer<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                            <?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE_PASSWORD') ?>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php if(!$agent->boss && $agent->userid) : ?>
                                    <li>
                                        <a class="removeAgent" href="javascript:void(0);" data-compid="<?php echo $this->item->ID ?>" data-agent-id="<?php echo $agent->kod ?>">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <?php echo JText::_('COM_STTNMLS_BUTTON_REMOVE_AGENT') ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <?php if($this->isAdmin) : ?>
                            <?php if($agent->boss) : ?>
                                <!-- Form edit agent title -->
                                <div class="modal fade changeAgentPostContainer<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>" tabindex="-1" role="dialog" aria-labelledby="changeAgentPostLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <div class="h4">
                                                    <?php printf('<b>%s:</b> <i>%s %s %s</i>', JText::_('COM_STTNMLS_LABEL_CHANGE_POST'), $agent->SIRNAME, $agent->NAME, $agent->SECNAME)  ?>
                                                </div>
                                            </div>
                                            <div class="modal-body form-horizontal">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6 col-md-4">
                                                        <?php echo JText::_('COM_STTNMLS_LABEL_AGENT_POST') ?>
                                                    </label>
                                                    <div class="col-sm-6 col-md-8">
                                                        <input type="text" class="form-control input-sm changeAgentPostInput<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>" name="post_title" value="<?php echo (($agent->boss_title != '') ? $agent->boss_title : JText::_('COM_STTNMLS_LABEL_BOSS'))  ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success btn-sm changeAgentPost" data-compid="<?php echo $this->item->ID ?>" data-agent-id="<?php echo $agent->kod ?>" >
                                                    <span class="glyphicon glyphicon-ok"></span>
                                                    <?php echo JText::_('COM_STTNMLS_BUTTON_SAVE') ?>
                                                </button>
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                    <?php echo JText::_('COM_STTNMLS_BUTTON_CANCEL') ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Form edit agent title -->
                            <?php endif; ?>

                            <?php if($agent->userid) : ?>
                                <!-- Form agent change password -->
                                <div class="modal fade changeAgentPasswordContainer<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>" tabindex="-1" role="dialog" aria-labelledby="changeAgentPasswordLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <div class="h4">
                                                    <?php printf('<b>%s:</b> <i>%s %s %s</i>', JText::_('COM_STTNMLS_LABEL_CHANGE_PASSWORD'), $agent->SIRNAME, $agent->NAME, $agent->SECNAME)  ?>
                                                </div>
                                            </div>

                                            <div class="modal-body form-horizontal">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6 col-md-4">
                                                        <?php echo JText::_('COM_STTNMLS_LABEL_LOGIN') ?>
                                                    </label>
                                                    <div class="col-sm-6 col-md-8">
                                                        <input type="text" class="form-control input-sm" name="login" value="<?php echo $agent->login ?>" disabled="disabled" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-6 col-md-4">
                                                        <?php echo JText::_('COM_STTNMLS_LABEL_NEW_PASSWORD') ?>
                                                    </label>
                                                    <div class="col-sm-6 col-md-8">
                                                        <input type="password" class="form-control input-sm changeAgentPasswordInput<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>" name="password" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success btn-sm changeAgentPassword" data-compid="<?php echo $this->item->ID ?>" data-agent-id="<?php echo $agent->kod ?>" data-userid="<?php echo $agent->userid ?>" >
                                                    <span class="glyphicon glyphicon-ok"></span>
                                                    <?php echo JText::_('COM_STTNMLS_BUTTON_SAVE') ?>
                                                </button>
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                    <?php echo JText::_('COM_STTNMLS_BUTTON_CANCEL') ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Form agent change password -->
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if($this->isAdmin && !$agent->userid) : ?>
                            <!-- FLAG: No linked -->
                            <div>
                                <span class="label label-warning">
                                    <?php echo JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_NO_LINKET_TO_JOOMLA_USERS') ?>!
                                </span>
                            </div>
                            <!-- /FLAG: No linked -->
                        <?php endif; ?>

                        <?php if($agent->flag == 1 && $this->isAdmin) : ?>
                            <div>
                                <span class="label label-danger">
                                    <?php echo JText::_('COM_STTNMLS_MESSAGE_ERROR_AGENT_NOT_CONFIRMED') ?>
                                </span>
                            </div>
                        <?php endif; ?>

                        <?php if($agent->boss) : ?>
                            <!-- FLAG: Boss -->
                            <div>
                                <span class="firm boss label label-default agent_post<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>">
                                    <?php echo (($agent->boss_title != '') ? $agent->boss_title : JText::_('COM_STTNMLS_LABEL_BOSS'))  ?>
                                </span>
                            </div>

                            <!-- /FLAG: Boss -->
                        <?php endif; ?>

                        </div>
                        <div class="col-md-3 text-center">
                            <a href="<?php echo 'tel:'.$agent->PHONE?>" title = "Телефон для связи" class="btn btn-success btn-block"><strong><?php echo $agent->PHONE ?></strong></a>                           
                            
                            <?php if($agent->ADDPHONE and ereg("[^ ]", $agent->ADDPHONE)) :?>
                            <a href="<?php echo 'tel:'.$agent->ADDPHONE?>" title = "Скайп" class="btn btn-success btn-block"><strong><?php  echo $agent->ADDPHONE ?></strong></a>
                            <?php endif;?>
                            <?php /*if($agent->SKYPE and ereg("[^ ]", $agent->SKYPE)) :?>
                            <a href="<?php echo 'skype:'.$agent->SKYPE?>" title = "Скайп" class="btn btn-success btn-block"><strong><?php  echo $agent->SKYPE ?></strong></a>
                            <?php endif;*/?>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
            <?php else : ?>
                <li class="list-group-item">
                    <?php echo JText::_('COM_STTNMLS_LABEL_NO_MEMBERS') ?>
                </li>
            <?php endif; ?>
            </ul>
        </div>
    </div>
    <!-- /ROW #6: Members -->

    <!-- ROW #7: SEO Firm contacts -->
    <div class="row">
        <div class="col-md-12">
            <div itemscope itemtype="http://schema.org/LocalBusiness">
                <?php $linkfirm = ($this->item->article_url != '') ? $this->item->article_url : SttnmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->ID) ?>
                <a itemprop="url" href="<?php echo $linkfirm; ?>">
                    <span itemprop="name"><strong><?php echo $this->item->NAME; ?></strong></span>
                </a><br />
                <div itemprop="address"><?php echo $this->item->addr; ?></div>
                <div itemprop="telephone"><?php echo $this->item->PHONE; ?></div>
                <div itemprop="email">
                    <a href="mailto:<?php echo $this->item->EMAIL ?>">
                        <?php echo $this->item->EMAIL; ?>
                    </a>
                </div>
                <p style="padding-top: 8px;">
                    <?php echo $socbut;?>
                </p>
            </div>
        </div>
    </div>
    <!-- /ROW #7: SEO Firm contacts -->
</div>

