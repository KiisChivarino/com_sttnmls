<?php

defined('_JEXEC') or die;

abstract class SttNmlsPlatusl 
{
	public static function getTypeObjByView($view)
    {
        $types_views_relations = [
            'apart'     =>  1,
            'aparts'    =>  1,
            'com'       =>  2,
            'coms'      =>  2,
            'garage'    =>  3,
            'garages'   =>  3,
            'house'     =>  4,
            'houses'    =>  4,
        ];
        return ((isset($types_views_relations[$view])) ? $types_views_relations[$view] : 0);
    }

    public static function getTableNameByTypeObj($type_id)
    {
        $types_tablesname_relations = [
            1           =>  '#__sttnmlsaparts',
            2           =>  '#__sttnmlscommerc',
            3           =>  '#__sttnmlsgarages',
            4           =>  '#__sttnmlshouses',
        ];
        return ((isset($types_tablesname_relations[$type_id])) ? $types_tablesname_relations[$type_id] : '');
    }

    public static function getTableNameByView($view)
    {
        $tablesname_views_relations = [
            'apart'     =>  '#__sttnmlsaparts',
            'aparts'    =>  '#__sttnmlsaparts',
            'com'       =>  '#__sttnmlscommerc',
            'coms'      =>  '#__sttnmlscommerc',
            'garage'    =>  '#__sttnmlsgarages',
            'garages'   =>  '#__sttnmlsgarages',
            'house'     =>  '#__sttnmlshouses',
            'houses'    =>  '#__sttnmlshouses',
        ];
        return ((isset($tablesname_views_relations[$view])) ? $tablesname_views_relations[$view] : 0);
    }

    public static function addUsl($usl, $par) 
    {
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        
        if(!$user->id) {
            return FALSE;
        }
                
        // оплата услуги
        if($usl == 'upobj') {
            $upobj = $params->get('upobj', 30);
                
            // проверка баланса
            $view = $par->view;

            $tabname = self::getTableNameByView($view);
            $typeobj = self::getTypeObjByView($view);
            
            $link = JRoute::_('index.php?option=com_sttnmls&view=' . $view . '&type=card&cn=' . $par->cardnum . '&cid=' . $par->compid, TRUE, -1);
                           
            if(self::changeBalance($user->id, $upobj, 'Поднятие <a href="' . $link . '" target="_blank">объявления</a> вверх. ')) {
                $db = JFactory::getDbo();
                // запись в таблицу услуг
                $db->setQuery('DELETE FROM `#__sttnmlsplatusl` WHERE `userid`=' . $db->quote($user->id) . ' AND `usluga`=' . $db->quote($usl) . ' AND `typeobj`=' . $db->quote($typeobj) . ' AND `compid`=' . $db->quote($par->compid) . ' AND `cardnum`=' . $db->quote($par->cardnum));
                $db->execute();
                $db->setQuery('INSERT INTO `#__sttnmlsplatusl` (`userid`, `usluga`, `typeobj`, `compid`, `cardnum`, `status`, `dateinp`, `datepay`) ' . 
                                'VALUES (' . $db->quote($user->id) . ', ' .
                                             $db->quote($usl) . ', ' .
                                             $db->quote($typeobj) . ', ' .
                                             $db->quote($par->compid) . ', ' .
                                             $db->quote($par->cardnum) . ', ' .
                                             '1, now(), now())');
                $db->execute();
                // запись в таблицу объектов
                $db->setQuery('UPDATE `' . $tabname . '` SET `upobj`=now() WHERE `COMPID`=' . $db->quote($par->compid) . ' AND `CARDNUM`=' . $db->quote($par->cardnum));
                $db->execute();
                return TRUE;
            } else {
                return FALSE;
            }
        }
        return FALSE;
    }
        
    public static function changeBalance($user_id, $summa, $log)
    {
        if(!$user_id) {
            return FALSE;
        }
        
        $db = JFactory::getDbo();
        $db->setQuery('SELECT `balans` FROM `#__sttnmlsbalans` WHERE `userid`=' . $db->quote($user_id));
        $balance = $db->loadResult();
        if(!$balance && $summa > 0) {
            return FALSE;
        }
           
        if($balance < $summa) {
            return FALSE;
        }
            
        $db->setQuery('INSERT INTO `#__sttnmlsbalanslog` (`status`, `summa`, `userid`, `prim`, `dateinp`) VALUES (1, ' . $db->quote(0-$summa) . ', ' . $db->quote($user_id) . ', ' . $db->quote($log) . ', now() )');
        $db->execute();
                
        $db->setQuery('UPDATE `#__sttnmlsbalans` SET `dateinp`=NOW(), `balans`=`balans`-' . $summa . ' WHERE `userid`=' . $db->quote($user_id));
        $db->execute();
        return TRUE;
    }
	
    public static function getText($item)
    {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

        // возвращает текстовую формулировку услуги
        $txt = '';
        $view = 'aparts';
        
        switch($item->typeobj) {
            case 2: $view = 'coms'; break;
            case 3: $view = 'garages'; break;
            case 4: $view = 'houses'; break;
        }
        
        if($item->usluga == 'upobj') {
            $txt = JText::_('COM_STTNMLS_PLATUSL_OBJECT');
            $link = SttNmlsHelper::getSEFUrl('com_sttnmls', $view, '&type=card&cn=' . $item->cardnum . '&cid=' . $item->compid);
            $txt .= ' <a href="' . $link . '" target="_blank">' . $link . '</a>';
        }
        return $txt;
    }

    public static function deleteUpobj($cardnum, $compid, $view)
    {
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        if(!$user->id) {
            return FALSE;
        }

        $typeobj = self::getTypeObjByView($view);

        // Получение ID услуги
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('id'));
        $query->from($db->quoteName('#__sttnmlsplatusl'));
        $query->where($db->quoteName('userid') . '=' . $db->quote($user->id));
        $query->where($db->quoteName('usluga') . '=' . $db->quote('upobj'));
        $query->where($db->quoteName('typeobj') . '=' . $db->quote($typeobj));
        $query->where($db->quoteName('compid') . '=' . $db->quote($compid));
        $query->where($db->quoteName('cardnum') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $id = $db->loadResult();

        // Удаление услуги
        if($id) {
            return self::delete($id);
        }
        return FALSE;
    }
        
    public static function delete($id) 
    {
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        if(!$user->id) {
            return FALSE;
        }

        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsplatusl'));
        $query->where($db->quoteName('id') . '=' . $db->quote($id));
        $db->setQuery($query);
        $item = $db->loadObject();
        if(!$item) {
            return FALSE;
        }

        $tabname = self::getTableNameByTypeObj($item->typeobj);

        if($item->usluga == 'upobj') {
            // Обновление объекта в таблицу объектов
            $query = $db->getQuery(TRUE);
            $query->update($db->quoteName($tabname));
            $query->set($db->quoteName('upobj') . '=' . $db->quote(''));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($item->compid));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($item->cardnum));
            $db->setQuery($query);
            $db->execute();
        }

        $query = $db->getQuery(TRUE);
        $query->delete($db->quoteName('#__sttnmlsplatusl'));
        $query->where($db->quoteName('userid') . '=' . $db->quote($user->id));
        $query->where($db->quoteName('id') . '=' . $db->quote($id));
        $db->setQuery($query);
        $db->execute();
        if($db->getErrorMsg()) {
            return FALSE;
        }
        return TRUE;
    }
	
    public static function check($item)
    {
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        if(!$user->id) {
            return FALSE;
        }
            
        if($item->usluga == 'upobj') {
            $db->setQuery('SELECT * FROM `#__sttnmlsplatusl` WHERE `userid`=' . $db->quote($user->id) . ' AND `usluga`=' . $db->quote($item->usluga) . ' AND `typeobj`=' . $db->quote($item->typeobj).' AND `compid`=' . $db->quote($item->compid) . ' AND `cardnum`=' . $db->quote($item->cardnum));
            $ret = $db->loadObject();            
            if($ret) {
                return TRUE;
            }
        }
        return FALSE;
    }
	
    public static function recalc()
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // Сущности фреймворка
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

        // Расценка на услугу продвижения объекта
        $price['upobj'] = $params->get('upobj', '30');

        // Получение списка активных услуг пользователей
        $query = $db->getQuery(TRUE);
        $query->select('*, TIMESTAMPDIFF(MINUTE, `datepay`, now()) AS `minutepay`');
        $query->from($db->quoteName('#__sttnmlsplatusl'));
        $db->setQuery($query);
        $items = $db->loadObjectList();

        // Случайным образом примерно раз в 10 вызовов обнуление флага поднятия объекта.
        // это нужно чтобы удалить разрушенные связи между объектами и несуществующими услугами.
        // например, если запись из таблицы услуг удалена, а объект еще поднят

        // Флаг принудительного обновления поля upobj у объектов
        $_UPOBJDEL = FALSE;
        if(rand(0, 100) > 90) {
            $query = $db->getQuery(TRUE);
            $_UPOBJDEL = TRUE;

            // Обнуление флага в таблице квартир и комнат
            $query->clear()
                ->update($db->quoteName('#__sttnmlsaparts'))
                ->set($db->quoteName('upobj') . '=NULL');
            $db->setQuery($query)->execute();

            // Обнуление флага в таблице коммерческой недвижимости
            $query->clear()
                ->update($db->quoteName('#__sttnmlscommerc'))
                ->set($db->quoteName('upobj') . '=NULL');
            $db->setQuery($query)->execute();

            // Обнуление флага в таблице гаражей
            $query->clear()
                ->update($db->quoteName('#__sttnmlsgarages'))
                ->set($db->quoteName('upobj') . '=NULL');
            $db->setQuery($query)->execute();

            // Обнуление флага в таблице домов и участков
            $query->clear()
                ->update($db->quoteName('#__sttnmlshouses'))
                ->set($db->quoteName('upobj') . '=NULL');
            $db->setQuery($query)->execute();
        }

        // Если активных услуг нет -> выход
        if(!$items) {
            return FALSE;
        }

        // Обход всех активных услуг
        foreach ($items as $item) {
            // Обработка единственной услуги - продвижения объекта
            if($item->usluga == 'upobj')
            {
                $view = 'aparts';
                switch($item->typeobj) {
                    case 2:
                            $tabname = '#__sttnmlscommerc';
                        break;

                    case 3:
                            $tabname = '#__sttnmlsgarages';
                        break;

                    case 4:
                            $tabname = '#__sttnmlshouses';
                        break;

                    default:
                            $tabname = '#__sttnmlsaparts';
                        break;
                }
                
                // Проверка наличия объекта, для которого активна услуга
                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('CARDNUM'));
                $query->from($db->quoteName($tabname));
                $query->where($db->quoteName('flag') . '< 3');
                $query->where($db->quoteName('COMPID') . '=' . $db->quote($item->compid));
                $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($item->cardnum));
                $db->setQuery($query);
                $result = $db->loadResult();


                if($result) {
                    // Если установлен флаг обновления объектов
                    if($_UPOBJDEL) {
                        // Обновление у объекта поля UPOBJ
                        $query = $db->getQuery(TRUE);
                        $query->update($db->quoteName($tabname));
                        $query->set($db->quoteName('upobj') . '=' . $db->quote($item->dateinp));
                        $query->where($db->quoteName('COMPID') . '=' . $db->quote($item->compid));
                        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($item->cardnum));
                        $db->setQuery($query)->execute();
                    }

                    // если прошли сутки после оплаты
                    if($item->minutepay > (24 * 60)) {
                        $link = SttNmlsHelper::getSEFUrl('com_sttnmls', $view, '&type=card&cn=' . $item->cardnum . '&cid=' . $item->compid);
                            
                        //если хватает денег, то снимаем со счета
                        //если не хватает денег, отключаем услугу
                        if(self::changeBalance($item->userid, $price['upobj'], 'Поднятие объявления вверх. <a href="' . $link . '" target="_blank">Объект</a>')) {
                            // запись в таблицу услуг
                            $query = $db->getQuery(TRUE);
                            $query->update($db->quoteName('#__sttnmlsplatusl'));
                            $query->set($db->quoteName('status') . '=1');
                            $query->set($db->quoteName('datepay') . '=NOW()');
                            $query->where($db->quoteName('id') . '=' . $db->quote($item->id));
                            $db->setQuery($query)->execute();

                            // запись в таблицу объектов
                            $query->clear()
                                ->update($db->quoteName($tabname))
                                ->set($db->quoteName('upobj') . '=' . $db->quote($item->dateinp))
                                ->where($db->quoteName('COMPID') . '=' . $db->quote($item->compid))
                                ->where($db->quoteName('CARDNUM') . '=' . $db->quote($item->cardnum));
                            $db->setQuery($query)->execute();
                        } else {
                            // не хватает денег
                            $query = $db->getQuery(TRUE);
                            $query->update($db->quoteName('#__sttnmlsplatusl'));
                            $query->set($db->quoteName('status') . '=0');
                            $query->where($db->quoteName('id') . '=' . $db->quote($item->id));
                            $db->setQuery($query)->execute();

                            $query->clear()
                                ->update($db->quoteName($tabname))
                                ->set($db->quoteName('upobj') . '=NULL')
                                ->where($db->quoteName('COMPID') . '=' . $db->quote($item->compid))
                                ->where($db->quoteName('CARDNUM') . '=' . $db->quote($item->cardnum));
                            $db->setQuery($query)->execute();
                        }
                    }
                }
            }
        }
        return TRUE;
    }
}