<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;
$sort_icon = ' <span class="glyphicon glyphicon-sort-by-attributes' . (($orderDirn == 'desc') ? '-alt' : '') . '"></span>';

?>

<div class="row-fluid" style="margin-bottom: 20px;">
    <h5><?php echo JText::_('COM_STTNMLS_LABEL_SORT_BY_FIELD') ?>:</h5>
    <ul class="nav nav-pills">
        <li role="presentation"<?php echo (($orderCol == 'o.DATEINP') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_DATE') . (($listOrder == 'a.DATEINP') ? $sort_icon : ''), 'o.DATEINP', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'PICCOUNT') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PHOTO') . (($listOrder == 'PICCOUNT') ? $sort_icon : ''), 'PICCOUNT', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'o.AAREA') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_SQUARE') . (($listOrder == 'o.AAREA') ? $sort_icon : ''), 'o.AAREA', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'o.PRICE') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PRICE') . (($listOrder == 'o.PRICE') ? $sort_icon : ''), 'o.PRICE', $listDirn, $listOrder); ?>
        </li>
    </ul>
</div>
<!-- Objects Container -->
<div class="clearfix objects-list">
<?php if($this->items) : ?>
    <?php $my_inc = 0; ?>
    <?php foreach ($this->items as $item) : ?>
    <?php
        $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'com', '&view=com&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);
        $linkstreet = SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&street_id=' . $item->STREETID);
        $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'com' , '&task=edit&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);
        $my_inc++;

        if($item->upobj) $style .= ' upobj';
        if($item->ifdlit!=1) $style = 'neprod';
    ?>
    <div class="col-sm-6 col-md-4 object" id="<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>">
        <div class="thumbnail object-com">
            <?php
                $exclusive = '';
                if($item->EXCLUSIVE == 3) $exclusive = 'exclusive';
                if($item->upobj) $exclusive .= ' upobj';
                if($exclusive) $exclusive =' class="' . $exclusive . '"';
            ?>
            <div class="object-photo text-center">
                <a href="<?php echo $link; ?>" <?php echo $exclusive; ?> target="_blank">
                    <?php
                    $pics = explode(';', $item->PICTURES);
                    $wm = '';
                    if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
                    $img_url = SttNmlsHelper::getLinkPhoto($pics[0], 'editfirst' . $wm);
                    ?>
                    <img class="img-rounded" src="<?php echo $img_url; ?>" alt="" />
                </a>
                <?php/* if($item->editable && ($item->flag == 1 OR $item->flag == 2) && !$item->upobj) : ?>
                    <a class="upobj picplat" href="<?php echo $link . '#plat'; ?>" target="_blank" title="<?php echo JText::_('COM_STTNMLS_PLAT'); ?>">
                        &nbsp;
                    </a>
                <?php endif; */?>
                <a href="<?php echo $link ?>" class="btn btn-success btn-sm object-view-button" target="_self">
                    <span class="glyphicon glyphicon-search"></span>
                    <?php echo JText::_('COM_STTNMLS_LABEL_VIEW') ?>
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $link; ?>" class="objectdesc object-link" target="_self" title="">
                    <?php echo (($item->object) ? $item->object : JText::_('COM_STTNMLS_COMNED')) ?>
                    <?php if(round($item->SQUEAR, 0) > 0) : ?>
                        <?php printf('<b>%d</b>м<sup>2</sup>', round($item->SQUEAR, 0)) ?>
                        <br />
                    <?php endif; ?>
                    <?php if(round($item->EAREA, 0) > 0) : ?>
                        <?php printf('%s <b>%d</b> %s', JText::_('COM_STTNMLS_UCH'), round($item->EAREA, 0), JText::_('COM_STTNMLS_SOTOK')) ?>
                        <br />
                    <?php endif; ?>
                </a>
                <div class="object-public-info">
                    <p class="to-left">
                        <span class="glyphicon glyphicon-calendar"></span>
                        <?php echo SttNmlsHelper::GetRuDate($item->DATEINP); ?>
                    </p>
                    <p class="to-right">
                        <span class="glyphicon glyphicon-eye-open"></span>
                        <?php echo $item->watchcount; ?>
                    </p>
                </div>
                <div class="statusLabels">
                    <?php if($item->close == 1) : ?>
                        <div style="text-align:center;">
                            <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 1)) : ?>
                        <div style="text-align:center;">                    
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&f_realtors=1') ?>" 
                               class="label label-info" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>"
                            ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE');?></a>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 2)) : ?>
                        <div style="text-align:center;">
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&f_realtors_and_open=1') ?>" 
                               class="label label-primary" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE') ?>"
                            ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE');?></a>
                        </div>
                    <?php endif; ?>
                    
                    <?php if($item->expired != 1) : ?>
                        <div style="text-align:center;">
                            <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_EXPIRED') ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if($item->checked == 1) : ?>
                        <div style="text-align:center;">
                            <span class="label label-success">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHECKED') ?>
                            </span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="object-description">
                <?php
                    $s = $item->gorod;
                    if($item->raion != '' && $item->RAIONID>1) {
                        $r = str_replace('р-н', '', $item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($item->mraion != '') {
                        $s .= ', ' . $item->mraion;
                    }
                    echo $s;
                ?>
                    <br />
                    <a href="<?php echo $linkstreet; ?>" title="<?php echo JText::_('COM_STTNMLS_ULICA_DESC') ?>">
                        <b><?php echo $item->ulica; ?></b>
                    </a>
                    <br />
                <?php if($item->EXCLUSIVE > 0) : ?>
                    <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/icon_rating.png" style="position:relative; top:2px;"/>
                <?php endif; ?>
                <?php echo $item->firm; ?>
            </div>
            <div class="object-price">
                <div class="price text-right clearfix">
                    <?php if($this->stype) : ?>
                        <?php if($item->PRICE) : ?>
                            <div class="value">
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_BY_MONTH_VALUE', number_format($item->PRICE, 0, ' ', ' ')) ?>
                            </div>
                        <?php endif; ?>

                        <?php if($item->PRICE_DAY) : ?>
                            <div class="value">
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_BY_DAY_VALUE', number_format($item->PRICE_DAY, 0, ' ', ' ')) ?>
                            </div>
                        <?php endif; ?>

                        <?php if($item->PRICE_HOUR) : ?>
                            <div class="value">
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_BY_HOUR_VALUE', number_format($item->PRICE_HOUR, 0, ' ', ' ')) ?>
                            </div>
                        <?php endif; ?>

                    <?php else : ?>
                        <span class="value">
                        <?php echo number_format($item->PRICE, 0, ' ', ' '); ?>
                        </span>
                    <?php endif; ?>

                    <?php if($item->percent > 0) : ?>
                        <div class="pull-left">
                            <span class="label label-danger">
                                <span class="glyphicon glyphicon-arrow-up"></span>
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_MORE_MIDDLE', round($item->percent, 2)) ?>
                            </span>
                        </div>
                    <?php endif; ?>
                    <?php if($item->percent < 0) : ?>
                        <div class="pull-left">
                            <span class="label label-success">
                                <span class="glyphicon glyphicon-arrow-down"></span>
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_LESS_MIDDLE', round($item->percent, 2)*-1) ?>
                            </span>
                        </div>
                    <?php endif; ?>
                    <a href="javascript:void(0);" class="favorite-star to-left addToFavorite" data-toggle="button" data-tp="<?php echo $this->tp; ?>" data-cid="<?php echo $item->COMPID; ?>" data-type="houses" data-cn="<?php echo $item->CARDNUM; ?>" aria-pressed="false" autocomplete="off">
                        <span class="glyphicon glyphicon-star"></span>
                    </a>
                </div>
            </div>
            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="picsmang" style="top:15px; width:100px; position:relative; left:-18px;">
                    <a href="<?php echo $linkedit; ?>" class="picedit" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>"></a>
                    <a class="picdate" title="<?php echo JText::_('COM_STTNMLS_EDITDATE'); ?>" onclick="changeDate(<?php echo $item->CARDNUM; ?>, <?php echo $item->COMPID; ?>,'house');"></a>
                    <a class="picdelete" title="<?php echo JText::_('COM_STTNMLS_DELETE'); ?>" onclick="return deleteobj(<?php echo $item->CARDNUM; ?>, <?php echo $item->COMPID; ?>,'house');"></a>
                </div>
            <?php endif; ?>
            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="text-center edit-button-block">
                    <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-sm" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <a href="javascript:void(0);" class="btn btn-info btn-sm changeObjectChangeDate" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHANGE_DATE') ?>" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="com">
                        <span class="glyphicon glyphicon-time"></span>
                    </a>

                    <a href="javascript:void(0);" class="btn btn-danger btn-sm removeObjectFromList" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="com">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    
    <?php endforeach; ?>
<?php else : ?>
    <?php if($this->noitemtext) : ?>
    <p class="text-center">
        <?php echo $this->noitemtext; ?>
    </p>
    <?php endif; ?>   
<?php endif; ?>
</div>
<!-- /Objects Container -->