<?php defined('_JEXEC') or die ('Restricted access'); 
    $params = JComponentHelper::getParams('com_sttnmls');
    
    $gorod = '';
    $object = '';
    $raion = '';
    $ulica = '';
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <title><?php echo JText::_('COM_STTNMLS_LABEL_PRINT') ?></title>
        <link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    </head>
    <body>
        <div style="font-family:Arial; width:900px; margin:auto; font-size:11px;">
            <?php if($this->agent) : ?>
            <h2 style="text-align:right;">
                <?php printf('%s %s %s, %s, %s', $this->agent->SIRNAME, $this->agent->NAME, $this->agent->SECNAME, $this->agent->PHONE, $this->agent->EMAIL) ?>
            </h2>
        <?php else : ?>
            <h2 style="text-align:right;">
                <?php printf('%s, %s, %s, %s', $params->get('site.title'), $params->get('site.phone'), $params->get('site.email'), $params->get('site.url')) ?>
            </h2>
        <?php endif; ?>
            <br />
            <br />
            
            <table width="100%" style="margin-top:5px; font-size:12px;" cellpadding="5" cellspacing="0" border="0">
                <tr style="background:#333; color:#fff; text-align:center; font-weight:bold;">
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <th>#</th>
                <?php endif; ?>
                    <th>Адрес</th>
                    <th>Тип</th>
                    <th>Мат-л стен</th>
                    <th>Этаж</th>
                    <th>Размер</th>
                    <th>Цена</th>
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <th>Агент</th>
                <?php endif; ?>
                </tr>
                
            <?php if($this->items) : ?>
                <?php foreach($this->items as $item) : ?>    
                <?php if($item->gorod != $gorod) : ?>
                <!-- Группировка ГОРОД -->
                <?php
                    $raion = '';
                    $gorod = $item->gorod;
                ?>
                <tr>
                    <td colspan="13" style="border-bottom:1px solid #666; background:#dfdfdf;">
                        <h3 style="padding:0px; margin:0px;">
                            <?php echo $item->gorod ?>
                        </h3>
                    </td>
                </tr>
                <!-- //Группировка ГОРОД -->
                <?php endif; ?>
                
                <?php if($item->raion != $raion) : ?>
                <!-- Группировка РАЙОН -->
                <?php
                    $ulica = '';
                    $raion = $item->raion;
                ?>    
                    <?php if($item->raion != 'Нет сведений') : ?>
                    <tr>
                        <td colspan="13" style="border-bottom:1px solid #666; background:#dfdfdf;">
                            <h3 style="padding:0px; margin:0px;">
                                <?php printf('%s - %s', $item->gorod, $item->raion) ?>
                            </h3>
                        </td>
                    </tr>
                    <?php endif; ?>
                <!-- Группировка РАЙОН -->
                <?php endif; ?>
                
                <?php
                    $dateinp = explode(" ", $item->DATEINP);
                    $dateinp = explode("-", $dateinp[0]);
                    $datecor = explode(" ", $item->DATECOR);
                    $datecor = explode("-", $datecor[0]);
                    
                    if($item->adph != '') {
                        $item->adph = ', ' . $item->adph;
                    }
                    
                    $ch = '';
                    if($item->GCHECKS1 != '' && $item->GCHECKS1 != '/') {
                        $item->GCHECKS1 .= "/";
                        $item->GCHECKS1 = str_replace("//", "", $item->GCHECKS1);
                        $item->GCHECKS1 = str_replace("/", " / ", $item->GCHECKS1);
                        
                        if($_REQUEST['typepdf'] == 'cli')
                        {
                            $ch = '<div style="color:#444">' . $item->GCHECKS1 . '</div>';
                        }else{
                            $ch = '<div style="border-top:1px solid #bbb; padding-top:5px; margin-top:10px;">' . $item->GCHECKS1 . '</div>';
                        }
                    }
                    
                    if($item->shorttip) {
                        $item->shorttip = 'Не указано';
                    }
                    
                    $mat = $this->garages_model->getOptionsByID($item->GTYPEID);
                    $matt = ((!$mat->SHORTNAME) ? 'Не указано' : $mat->SHORTNAME);
                    
                    $stage = ((!$item->STAGE OR !$item->HSTAGE) ? 'Не указано' : $item->STAGE . ' / ' . $item->HSTAGE);
                ?>
                <tr>
                    <?php if($_REQUEST['typepdf'] != 'cli') : ?>                    
                    <td rowspan="2" style="width:150px; text-align:center; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;" valign="middle">
                        <b><?php echo $item->firm ?></b>
                        <br />(<?php echo $item->COMPID ?>)
                        <br />
                        <span style="padding:4px 0px; display:block">
                            <b>Объект #:</b> 
                            <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=garage&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID) ?>" target="_blank">
                                <?php echo $item->CARDNUM ?>
                            </a>
                        </span>
                        <span style="font-size:11px;">
                            <?php printf('%s.%s.%s&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;%s.%s.%s', $dateinp[2], $dateinp[1], $dateinp[0], $datecor[2], $datecor[1], $datecor[0]) ?>
                        </span>
                    </td>
                    <?php endif; ?>
                    
                    <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <td style="border-right:1px solid #666; width:150px; vertical-align:top !important;" valign="top">
                        <h4 style="padding:0px; margin:0px;">
                            <?php echo str_replace(',', ', ', $item->ulica) ?>
                        </h4>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $item->shorttip ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $matt ?>
                    </td>
                    <td style="border-right:1px solid #666; width:60px; text-align:center;">
                        <?php echo $stage ?>
                    </td>
                    <td style="border-right:1px solid #666; width:20px; text-align:center;">
                        <?php printf('%dx%dx%d', $item->GWIDTH, $item->GLONG, $item->GHEIGHT) ?>
                    </td>
                    <td style="width:70px; text-align:center; width:100px;">
                        <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                    </td>
                    <?php else : ?>
                        <?php if($ch != '') : ?>
                        <td style="width:150px; vertical-align:top !important;" valign="top">
                            <h4 style="padding:0px; margin:0px;">
                                <?php echo str_replace(',', ', ', $item->ulica) ?>
                            </h4>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $item->shorttip ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $matt ?>
                        </td>
                        <td style="width:60px; text-align:center;">
                            <?php echo $stage ?>
                        </td>
                        <td style="width:20px; text-align:center;">
                            <?php printf('%dx%dx%d', $item->GWIDTH, $item->GLONG, $item->GHEIGHT) ?>
                        </td>
                        <td style="width:70px; text-align:center; width:100px;">
                            <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                        </td>
                        <?php else : ?>
                        <td style="border-bottom:1px solid #666; width:150px; vertical-align:top !important;" valign="top">
                            <h4 style="padding:0px; margin:0px;">
                                <?php echo str_replace(',', ', ', $item->ulica) ?>
                            </h4>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $item->shorttip ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $matt ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:60px; text-align:center;">
                            <?php echo $stage ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:20px; text-align:center;">
                            <?php printf('%dx%dx%d', $item->GWIDTH, $item->GLONG, $item->GHEIGHT) ?>
                        </td>
                        <td style="width:70px; text-align:center; border-bottom:1px solid #666; width:100px;">
                            <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                        </td>
                        <?php endif; ?>
                    <?php endif; ?>
                        
                    <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                        <?php $access = explode(',', $this->agent->prava); ?>

                            <td rowspan="2" style="width:200px; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;">
                                <?php printf('%s %s', $item->sir, $item->nm) ?>
                                <br /><b><?php echo $item->ph . $item->adph ?></b>
                            <?php if((($item->AGENTID == $this->agent->ID && $item->COMPID == $this->agent->COMPID) OR in_array($this->agent->kod, $access) OR ($this->agent->boss == 1 && $item->COMPID == $this->agent->COMPID)) && JFactory::getUser()->id > 0) : ?>
                                <br />
                                <br /><b>Контакты собственника:</b>
                                <br /><?php echo $item->sob_fio ?>
                                <br /><?php echo $item->sob_tel ?>
                                <br /><?php echo $item->sob_tel2 ?>
                            <?php endif; ?>
                            </td>
                    <?php endif; ?>
                </tr>
                
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                <tr>
                    <td colspan="6" style="border-top:1px solid #666; border-bottom:1px solid #666; height:50px; width:400px;">
                        <?php echo $item->MISC . $ch ?>
                    </td>
                </tr>
                <?php else : ?>
                    <?php if($ch != '') : ?>
                    <tr>
                        <td colspan="6" style="width:400px; border-bottom:1px solid #777;">
                            <?php echo $ch ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endif; ?>
                    
                <?php endforeach; ?>
            <?php endif; ?>
            </table>
        </div>
    </body>
</html>