<?php

defined('_JEXEC') or die('Restricted access');

class SttNmlsModelOrds extends JModelList{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
			'id', 'a.id',
			'fio', 'fio',
			'phone', 'phone',
			'email', 'email',
			'variant', 'a.variant',
			'typeobj', 'a.typeobj',
			'watchcount', 'a.watchcount',
			'dateinp', 'a.dateinp',
			'descr', 'a.descr'
			);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select($this->getState('list.select', 'a.*, concat(ag.SIRNAME," ",ag.NAME," ",ag.SECNAME) as fio, ag.PHONE as phone, ag.EMAIL as email, ag.ID as agentid, ag.COMPID as compid, f.realname, f.NAME as firmname  '));
		$query->from('`#__sttnmlszayavka` AS a ');
		$query->leftJoin('#__sttnmlsvocagents as ag on a.userid = ag.userid ');
		$query->leftJoin('#__sttnmlsvocfirms as f on f.ID = ag.COMPID ');
		$orderCol = $this->state->get('list.ordering', 'realname');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if($orderCol=='name') $orderCol='realname';
		$query->order($db->escape($orderCol.' '.$orderDirn));
//		$sql = (string) $query;
//		echo $sql;

		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		$this->setState('list.limit', $limit);

		$this->setFilterState('search','');
		parent::populateState('dateinp', 'desc');

		$limitstart = JRequest::getUInt('limitstart', 0);
		$this->setState('list.start', $limitstart); 
		
	} 
	public function setFilterState($par,$default=null)
	{
		$a=$this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
		$this->setState('filter.' .$par, $a);
	}

//	public function getPagination() {
//		$p = parent::getPagination();
//		$cat = JRequest::getInt('cat', 0);
//		if($cat) {
//			$p->setAdditionalUrlParam('cat', $cat);
//		}
//		return $p;
//	}
}

