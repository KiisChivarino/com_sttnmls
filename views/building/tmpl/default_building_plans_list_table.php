<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>

<table class="table table-striped">
    <thead>
    <tr>
        <th></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_WC_TYPE') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_BALKON') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_PRICE_QM') ?></th>
        <th>Стоимость</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($this->item_building_plans[$this->current_rooms] as $item): ?>
        <tr>
            <td style="width:64px;">
                <a class="fbox small" href="<?php echo SttNmlsHelper::getLinkPhoto($item->photos, 'big', 'images/objects/') . '&ts=' . time() ?>">
                    <img class="thumb img-thumbnail" src="<?php echo JURI::root() . 'components/com_sttnmls/assets/images/icon_planirovka.png' ?>" alt="<?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_' . $this->current_rooms) ?>" title="<?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_' . $this->current_rooms) ?>" />
                </a>
            </td>
            <td><?php printf('%s / %s / %s', $item->sall, $item->szhil, $item->skitch) ?></td>
            <td><?php echo (($item->san) ? 'Совм' : 'Разд') ?></td>
            <td><?php echo (($item->bal) ? 'Есть' : 'Нет') ?></td>
            <td><b><?php echo number_format($item->price, 0, ' ', ' ') ?></b></td>
            <td><b><?php echo number_format(($item->price * $item->sall), 0, ' ', ' ') ?></b></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
