<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');
require_once JPATH_COMPONENT.'/helpers/sttnmlsitem.php';

class SttNmlsModelCom extends SttNmlsModelItem {
    public function __construct($config = array()) 
    {
        $this->setTabname('#__sttnmlscommerc');
        $this->setTp('4');
        parent::__construct($config);
    }
        
    function getOptWmaterial()
    {
        $params = JComponentHelper::getParams('com_sttnmls');
        $arr = $params->get('wmaterial');
        $where = ' ID in (' . implode(',', $arr) . ')';

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('NAME as text, ID as value');
        $query->from(' `#__sttnmlsvocalldata` ');
        $query->where(' RAZDELID=8 ');
        $query->order('text');
        $db->setQuery($query);
        $res = $db->loadAssocList();
        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
        if(is_array($res)) {
            foreach($res as $row)
            {
                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
            } 
        }
        return JHTML::_('select.genericlist',  $options, 'jform[wmaterid]', 'class="form-control validate-sttnum"', 'value', 'text', $this->_item->WMATERID);
    }    
    
    function getOptObj()
    {
        return $this->getOpt(19, 'OBJID');
    }
    function getOptUsage()
    {
        return $this->getOpt(20, 'USAGEID');
    }
        
        
	public function getTitle()
	{
		return $this->getTitleItem($this->_item);
	}

	private function getTitleItem($item)
	{
        $pTtl = '';
        $pTtl .= ($item->VARIANT == 2) ? 'Сдаются' : 'Продаются';
        $pTtl .= (isset($item->object) && $item->object != '' && $item->object != JText::_('COM_STTNMLS_HTYPE168')) ? ' '.$item->object : ' '.JText::_('COM_STTNMLS_OBJECTM');
        //$pTtl .= ((isset($item->raion) && $item->raion != '' && $item->raion != JText::_('COM_STTNMLS_NOSV')) ? ', '.$item->raion.JText::_('COM_STTNMLS_RAION_M') : '');
        $pTtl .= (isset($item->ulica) && $item->ulica != '' && $item->ulica != JText::_('COM_STTNMLS_NOSV')) ? ', '.$item->ulica : '';
		return $pTtl;
	}

	public function getDesc()
	{
		$dopinfo = '';
        if (isset($this->_item->CCHECKS1) && $this->_item->CCHECKS1 != '')
        {
            $dopinfo .= ', ';
            $t = explode('/', $this->_item->CCHECKS1);
            $dopinfo .= rtrim(implode(', ', $t), ', ');
        }

        return trim($this->_item->MISC).$dopinfo;
	}
	public function getDesc2()
	{
		$dopinfo = '';
        if (isset($this->_item->CCHECKS1) && $this->_item->CCHECKS1 != '')
        {
            $dopinfo .= ', ';
            $t = explode('/', $this->_item->CCHECKS1);
            $dopinfo .= rtrim(implode(', ', $t), ', ');
        }

        return trim($this->_item->MISC).$dopinfo;
	}
	public function setDopJoin($query)
	{
		$query->join('LEFT','`#__sttnmlsvocalldata` as d on a.OBJID=d.id');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d20 on a.USAGEID=d20.id and d20.RAZDELID=20');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d8 on a.WMATERID=d8.id and d8.RAZDELID=8');
		return;
	}

	public function getDopSelect()
	{
		return ',c.NAME as gorod,
			r.NAME as raion,
			m.NAME as mraion,
			concat(s.NAME,if(a.HAAP <> " ",	CONCAT(",",a.HAAP), "")) as ulica,
			d20.NAME as usagename,
			d8.NAME as mat,
			d.NAME as object ';
	}
	function getCheckAr()
	{
		$res = new stdClass;
		$res->checks1 = array(JText::_('COM_STTNMLS_COMCHEKS11'),JText::_('COM_STTNMLS_COMCHEKS12'),JText::_('COM_STTNMLS_COMCHEKS13'),JText::_('COM_STTNMLS_COMCHEKS14'));
		$res->checks2 = array(JText::_('COM_STTNMLS_COMCHEKS21'),JText::_('COM_STTNMLS_COMCHEKS22'),JText::_('COM_STTNMLS_COMCHEKS23'),JText::_('COM_STTNMLS_COMCHEKS24'));
		return $res;
	}
	
	
	function save()
    {
        //
		$app = JFactory::getApplication();

        //


		$data  = $app->input->post->get('jform', array(), 'array');

        if(!isset($data['cityid']) OR !$data['cityid']) {
            $data['cityid'] = -1;
        }

        $_export = $app->input->post->get('export', 0);
        $_exclusive = $app->input->post->get('EXCLUSIVE', 0);
        //$_close = $app->input->post->get('close', 0);
        $_encumbrance = $app->input->post->get('encumbrance', 0);
		$target_agent = $app->input->post->get('fromag', 0, 'uint');
        $checks1 = $app->input->post->get('checks1', array(), 'array');
        $checks2 = $app->input->post->get('checks2', array(), 'array');

        if($target_agent) {
            $data['fromag'] = $target_agent;
        }

		//дополнительные опции
        $data['export'] = (($_export) ? 1 : 0);
        $data['EXCLUSIVE'] = (($_exclusive) ? 1 : 0);
		$data['encumbrance'] = (($_encumbrance) ? 1 : 0);
        //$data['close'] = (($_close) ? 1 : 0);
        $data['CCHECKS1'] = implode('/', $checks1) . '/' . implode('/', $checks2);
		
		//доступ к базе
		$_open_and_realtors = $app->input->post->get('open_and_realtors', 0);
		$_access_base = $app->input->post->get('access_base', 0);
		
		switch ((int)$_access_base + (int)$_open_and_realtors){
			case 1:
				$data['close'] = 1;
				$data['realtors_base'] = 0;
				break;
			case 2:
				$data['close'] = 0;
				$data['realtors_base'] = 1;
				break;
			case 3:
				$data['close'] = 0;
				$data['realtors_base'] = 0;
				break;
			case 4:
				$data['close'] = 0;
				$data['realtors_base'] = 2;
				break;
			default:
				//$data['close'] = 0;
				//$data['realtors_base'] = 0;
				//break;
				echo 'Ошибка при определении доступа записи к базе';
				exit;
		}
		
        // Сохранение данных
		$this->store($data);
	}

	function getMailBody()
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_sttnmls');

		// VARS
		$stype = $app->input->get('stype', 0, 'uint');

		$query = $db->getQuery(TRUE);
		$query->select($db->quoteName('o') . '.*, ' . $db->quoteName('c.NAME', 'gorod'));
		$query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
		$query->select($db->quoteName('d8.NAME', 'mat') . ', ' . $db->quoteName('d20.NAME', 'usagename'));
		$query->select($db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d.NAME', 'object'));
		$query->from($db->quoteName('#__sttnmlscommerc', 'o'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('o.CITYID') . '=' . $db->quoteName('c.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'r') . ' ON ' . $db->quoteName('o.RAIONID') . '=' . $db->quoteName('r.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('o.STREETID') . '=' . $db->quoteName('s.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd') . ' ON ' . $db->quoteName('o.OBJID') . '=' . $db->quoteName('d.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd20') . ' ON ' . $db->quoteName('o.USAGEID') . '=' . $db->quoteName('d20.id') . ' AND ' . $db->quoteName('d20.RAZDELID') . '=' . $db->quote(20));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON ' . $db->quoteName('o.WMATERID') . '=' . $db->quoteName('d8.id') . ' AND ' . $db->quoteName('d8.RAZDELID') . '=' . $db->quote(8));
		$query->where($db->quoteName('o.COMPID') . '=' . $db->quote($this->_item->compid));
		$query->where($db->quoteName('o.CARDNUM') . '=' . $db->quote($this->_item->cardnum));
		$db->setQuery($query);
		$row = $db->loadObject();

		if(!$row) {
			return '';
		}

		foreach($row as $key => $value) {
			$data[$key] = $value;
		}

		$data['object_title'] = $this->getTitleItem($row);
		$data['mat'] = ((isset($row->mat) && $row->mat != JText::_('COM_STTNMLS_NOSV')) ? $row->mat : 'undefined') . ', ';
		$data['squear'] = ((isset($row->SQUEAR) && number_format($row->SQUEAR, 0) > 0) ? 'площадь: ' . number_format($row->SQUEAR, 0) . ' кв.м' : 'undefined') . ', ';
		$data['hstage'] = ((isset($row->HSTAGE) && (int)$row->HSTAGE) ? $row->HSTAGE . ' этаж(а)' : 'undefined') . ', ';
		$data['earea'] = ((isset($row->EAREA) && (int)$row->EAREA) ? 'участок: ' . number_format($row->EAREA, 0) . ' соток' : 'undefined') . ', ';
		$data['usagename'] = ((isset($row->usagename) && $row->usagename != JText::_('COM_STTNMLS_HTYPE168')) ? 'используется как ' . $row->usagename : 'undefined') . ', ';
		$data['dopinfo'] = ((isset($row->CCHECKS1) && $row->CCHECKS1 != '') ? rtrim(implode(', ', explode('/', $row->CCHECKS1)), ', ') : 'undefined') . '.';
		$data['price'] = number_format($row->PRICE/1000,3, ' ', ' ');
		$data['object_link'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'coms', '&type=card&cn=' . $this->_item->cardnum . '&cid=' . $this->_item->compid);


		return SttNmlsLocal::parseString(JText::_('COM_STTNMLS_EMAIL_ITEM_ADD_ADMIN_NOTIFICATION_BODY_COM'), $data);
	}
}
