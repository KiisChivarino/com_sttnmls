<?php 
defined('_JEXEC') or die ('Restricted access');

//Подключение файла контроллера
require_once ( JPATH_COMPONENT.'/controller.php');

if(JRequest::getVar('type','')=='card')
{
    if(JRequest::getVar('view','')=='aparts')
        JRequest::setVar('view','apart');
    if(JRequest::getVar('view','')=='coms')
        JRequest::setVar('view','com');
    if(JRequest::getVar('view','')=='houses')
        JRequest::setVar('view','house');
    if(JRequest::getVar('view','')=='garages')
        JRequest::setVar('view','garage');
}
//Проверка или требуется определённый контроллер
//if($controller = JRequest::getVar('controller','')) {
//    require_once ( JPATH_COMPONENT . '/controllers/' . $controller.'.php');
//}

//Создание класса компонента
//$classname = 'SttNmlsController' . $controller;
//$controller = new $classname();

//Выполнить задачу запроса
//$controller->execute(JRequest::getVar ('task'));

//Переадресация
//$controller->redirect();

$controller = JControllerLegacy::getInstance('Sttnmls');
$controller->execute(JFactory::getApplication()->input->get('task', 'display'));
$controller->redirect();