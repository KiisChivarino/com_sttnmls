<?php defined('_JEXEC') or die('Restricted access'); ?>

<div class="media">
    <div class="media-left">
        <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=robokassa&layout=default'); ?>" class="img-thumbnail">
            <img src="<?php echo JURI::root() . '/components/com_sttnmls/assets/images/icon_rub.jpg' ?>" class="media-object" alt="" />
        </a>
        
    </div>
    <div class="media-body">
        <h4><?php echo JText::sprintf('COM_STTNMLS_LABEL_YOUR_BALANS', $this->item->balance) ?></h4>
        <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=robokassa&layout=default') ?>" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus"></span>
            <?php echo JText::_('COM_STTNMLS_BUTTON_REFILL') ?>
        </a>
    </div>
</div>

<h4><?php echo JText::_('COM_STTNMLS_LABEL_BALANCE_LOG'); ?></h4>
<hr />

<?php if($this->item->balancelog) : ?>
<ul class="list-group">
    <?php foreach($this->item->balancelog as $item) : ?>
    <li class="list-group-item">
        <span>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_DATE') ?>:</b>
            <?php echo date('d.m.Y H:i:s', strtotime($item->dateinp)) ?>
        </span>
        <br />
        <span>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_DESCRIPTION') ?>:</b>
            <?php echo $item->prim; ?>
        </span>
        <span class="badge badge-<?php echo (($item->summa > 0) ? 'success' : 'danger') ?>">
            <?php echo $item->summa . ' руб.'; ?>
        </span>
    </li>
    <?php endforeach; ?>
</ul>
<?php else : ?>
<div class="alert alert-info">
    <?php echo JText::_('COM_STTNMLS_LABEL_NO_INFO') ?>
</div>
<?php endif; ?>