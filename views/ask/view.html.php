<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewAsk extends JViewLegacy
{
	function display($tpl = null) 
	{
		$user = JFactory::getUser();
		$this->user=$user;
		$app	= JFactory::getApplication();
		$db=JFactory::getDbo();
		
		$params = JComponentHelper::getParams('com_sttnmls');

		
		
		if($_REQUEST['act']=='edit_answer')
		{
			$db->setQuery('UPDATE #__sttnmlsaskanswer set text='.$db->quote($_REQUEST['text']).', date_edit=NOW() WHERE id='.$db->quote($_REQUEST['answer_id']));
			$db->query();
			echo '<div style="background:#BDFFC3; padding:10px; border:1px solid #0f0; font-weight:bold;">Сообщение отредактировано</div>';
		}
		
		

		if($_REQUEST['act']=='send_question')
		{
			$db->setQuery('INSERT INTO #__sttnmlsaskquestion VALUES("", '.$db->quote($_REQUEST['cat_id']).', '.$db->quote($user->id).', '.$db->quote($_REQUEST['qustion']).', NOW())');
			$db->query();
			echo '<div style="background:#BDFFC3; padding:10px; border:1px solid #0f0; font-weight:bold;">Спасибо, Ваш вопрос добавлен</div>';
			$db->setQuery('select * from #__sttnmlsaskcat WHERE id='.$db->quote($_REQUEST['cat_id']));
			$cat= $db->loadObject();
			$us=explode(",", $cat->experts);
			$query	= 'SELECT DISTINCT a.id, a.name, a.EMAIL, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.kod IN ('.implode(",",$us).') ORDER BY ag.SIRNAME';	
			$db->setQuery($query);
			$users= $db->loadObjectList();
			
			$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$user->id.'" ORDER BY ag.SIRNAME';
			$db->setQuery($query);
			$userwho	= $db->loadObject();
			foreach($users as $usr)
			{
				$headers= "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=utf-8\r\n";
				$headers .= "From: KurskMetr <noreply@kurskmetr.ru>\r\n";
				$subject=$params->get('mail_subj_new');
				$body=$params->get('mail_new');
				$body=str_replace("[name]",$usr->agentname,$body);
				$body=str_replace('[link]',"<a href='http://kurskmetr.ru/expert.html?cat_id=".$_REQUEST['cat_id']."'>Перейти в категорию</a>",$body);
				$body=str_replace("[who]",$userwho->agentname,$body);
				$body=str_replace("[text]",$_REQUEST['qustion'],$body);
				mail($usr->EMAIL,$subject,$body, $headers);
			}
            header('Location: /expert.html?view=ask&cat_id='.$_REQUEST['cat_id']);
		}
		
		

		
		
		if($_REQUEST['act']=='send_comm')
		{
			if($_REQUEST['text'])
			{
				$db->setQuery('INSERT INTO #__sttnmlsaskanswer VALUES("", '.$db->quote($_REQUEST['quest_id']).', '.$db->quote($user->id).', '.$db->quote($_REQUEST['text']).', NOW(), "", '.$db->quote($_REQUEST['answer_id']).', "0")');
				$db->query();
				echo '<div style="background:#BDFFC3; padding:10px; border:1px solid #0f0; font-weight:bold;">Спасибо, Ваш ответ добавлен</div>';
				
				$db->setQuery('select * from #__sttnmlsaskanswer WHERE id='.$db->quote($_REQUEST['answer_id']));
				$cat= $db->loadObject();
				
				
				$query	= 'SELECT DISTINCT a.id, a.name, a.EMAIL, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$cat->from.'" ORDER BY ag.SIRNAME';	
				$db->setQuery($query);
				$users= $db->loadObjectList();
				
				$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$user->id.'" ORDER BY ag.SIRNAME';
				$db->setQuery($query);
				$userwho	= $db->loadObject();
				foreach($users as $usr)
				{
					$subject=$params->get('mail_subj_comment');
					$body=$params->get('mail_comment');
					$headers= "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=utf-8\r\n";
					$headers .= "From: KurskMetr <noreply@kurskmetr.ru>\r\n";
					$body=str_replace("[name]",$usr->agentname,$body);
					$body=str_replace('[link]',"<a href='http://kurskmetr.ru/expert.html?cat_id=".$_REQUEST['cat_id']."#qss_".$_REQUEST['quest_id']."'>Перейти к вопросу</a>",$body);
					$body=str_replace("[who]",$userwho->agentname,$body);
					$body=str_replace("[text]",$_REQUEST['text'],$body);
					mail($usr->EMAIL,$subject,$body, $headers);
				}
			}else{
				echo '<div style="background:#E8C3BD; padding:10px; border:1px solid #f00; font-weight:bold;">Введите текст ответа</div>';
			}
		}
		
		

		if($_REQUEST['act']=='send_answer')
		{
			if($_REQUEST['text'])
			{
				$db->setQuery('INSERT INTO #__sttnmlsaskanswer VALUES("", '.$db->quote($_REQUEST['quest_id']).', '.$db->quote($user->id).', '.$db->quote($_REQUEST['text']).', NOW(), "", "0", "0")');
				$db->query();
				echo '<div style="background:#BDFFC3; padding:10px; border:1px solid #0f0; font-weight:bold;">Спасибо, Ваш ответ добавлен</div>';
				
				$db->setQuery('select * from #__sttnmlsaskquestion WHERE id='.$db->quote($_REQUEST['quest_id']));
				$cat= $db->loadObject();
				
				
				$query	= 'SELECT DISTINCT a.id, a.name, a.EMAIL, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$cat->from.'" ORDER BY ag.SIRNAME';	
				$db->setQuery($query);
				$users= $db->loadObjectList();
				
				$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$user->id.'" ORDER BY ag.SIRNAME';
				$db->setQuery($query);
				$userwho	= $db->loadObject();
				foreach($users as $usr)
				{
					$subject=$params->get('mail_subj_answer');
					$body=$params->get('mail_answer');
					$headers= "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=utf-8\r\n";
					$headers .= "From: KurskMetr <noreply@kurskmetr.ru>\r\n";
					$body=str_replace("[name]",$usr->agentname,$body);
					$body=str_replace('[link]',"<a href='http://kurskmetr.ru/expert.html?cat_id=".$cat->cat_id."#qss_".$_REQUEST['quest_id']."'>Перейти к вопросу</a>",$body);
					$body=str_replace("[who]",$userwho->agentname,$body);
					$body=str_replace("[text]",$_REQUEST['text'],$body);
					mail($usr->EMAIL,$subject,$body, $headers);
				}
			}else{
				echo '<div style="background:#E8C3BD; padding:10px; border:1px solid #f00; font-weight:bold;">Введите текст ответа</div>';
			}
		}
		 
		
		
		$db->setQuery('select * from #__sttnmlsaskcat ');
		$items= $db->loadObjectList();
		foreach($items as $k=>$v)
		{
			if($v->experts!='')
			{
				$us=explode(",", $v->experts);
				$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.kod IN ('.implode(",",$us).') ORDER BY ag.SIRNAME';
				
				$db->setQuery($query);
				$users	= $db->loadObjectList();
				$items[$k]->experts=$users;
			}
			$db->setQuery('select count(*) as count from #__sttnmlsaskquestion WHERE cat_id="'.$v->id.'" ORDER BY date DESC');
			$questionscount=$db->loadObject();
			$items[$k]->questionscount=$questionscount;
			
			
			
			$db->setQuery('select count(*) as count from #__sttnmlsaskanswer WHERE quest_id IN (SELECT id from #__sttnmlsaskquestion WHERE cat_id="'.$v->id.'" ORDER BY date DESC) and parent=0');
			$answerscount=$db->loadObject();
			$items[$k]->answerscount=$answerscount;
		}
		$this->items=$items;
		if(isset($_REQUEST['cat_id']))
		{
			$db->setQuery('select * from #__sttnmlsaskcat WHERE id="'.$_REQUEST['cat_id'].'"');
			$item= $db->loadObject();
			if($item->experts!='')
			{
				$us=explode(",", $item->experts);
				$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.kod IN ('.implode(",",$us).') ORDER BY ag.SIRNAME';
				
				$db->setQuery($query);
				$users	= $db->loadObjectList();
				$item->experts=$users;
			}
			$this->item=$item;
			
			$db->setQuery('select * from #__sttnmlsaskquestion WHERE cat_id="'.$_REQUEST['cat_id'].'" ORDER BY date DESC');
			$questions=$db->loadObjectList();
			foreach($questions as $k=>$v)
			{
				$db->setQuery('select * from #__sttnmlsaskanswer WHERE quest_id="'.$v->id.'" AND parent=0 ORDER BY id ASC');
				$answer= $db->loadObjectList();
				foreach($answer as $t=>$an)
				{
					$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$an->from.'" ORDER BY ag.SIRNAME';
					$db->setQuery($query);
					$users	= $db->loadObject();
					$answer[$t]->userinfo=$users;
				}
				$questions[$k]->answers=$answer;
				$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$v->from.'" ORDER BY ag.SIRNAME';
				$db->setQuery($query);
				$users	= $db->loadObject();
				$questions[$k]->userinfo=$users;
			}
			$this->questions=$questions;
			
			
			
			$document = JFactory::getDocument();
			if($this->item->title)
			{
				$document->setTitle($this->item->title);
			}
			if($this->item->description)
			{
				$document->setDescription($this->item->description);
			}
			if($this->item->keyword)
			{
				$document->setMetadata('keywords', $this->item->keyword);
			}
		}
		$_SESSION['rand']=rand(10000,99999);
		
		
		parent::display($tpl);
	}
}
