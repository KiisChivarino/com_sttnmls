<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewHouses extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->access = $this->get('AccessRules');
        $this->items = $this->get('Items');
        $this->countobj = $this->get('Total');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->stype = JRequest::getInt('stype',0);
        $this->raions = $this->get('RaionsList');
        $this->mraions = $this->get('MRaionsList');
        $this->cityopt = $this->get('OptCity');
        $this->tp = $this->get('Tp');
        $this->params = JComponentHelper::getParams('com_sttnmls');
        $this->raw = 1;

        parent::display($tpl);
    }
}
