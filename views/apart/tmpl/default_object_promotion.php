<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php' );

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');

$balance = SttNmlsHelper::getBalance();

$url_promotion_turn_on = JRoute::_('index.php?option=com_sttnmls&view=aparts&type=card&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID . '&task=objectPromotionOn', TRUE, -1);
$url_promotion_turn_off = JRoute::_('index.php?option=com_sttnmls&view=aparts&type=card&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID . '&task=objectPromotionOff', TRUE, -1);


$o = new stdClass();
$o->usluga = 'upobj';
$o->typeobj = 1;
$o->cardnum = $this->item->CARDNUM;
$o->compid = $this->item->COMPID;

?>
<?php if(!SttNmlsPlatusl::check($o)) : ?>
    <?php if($balance >= $params->get('upobj', 30)) : ?>
        <a href="<?php echo $url_promotion_turn_on ?>" class="btn btn-success btn-block"  onclick="return confirm('<?php echo JText::_('COM_STTNMLS_PLAT_SHURE');?>');" style="white-space: normal;">
            <span class="glyphicon glyphicon-arrow-up"></span> <?php echo JText::_('COM_STTNMLS_PLAT_OBJ_UP'); ?>
        </a>
    <?php else : ?>
        <div class="alert alert-warning">
            <?php echo JText::_('COM_STTNMLS_NOMONEY');?>
        </div>
        <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=robokassa&layout=default', TRUE, -1); ?>" class="btn btn-warning btn-block btn-lg" style="white-space: normal;">
            <span class="glyphicon glyphicon-plus"></span>
            <?php echo JText::_('COM_STTNMLS_PAY_NOW');?>
        </a>
    <?php endif; ?>
<?php else : ?>
    <a href="<?php echo $url_promotion_turn_off ?>" class="btn btn-default btn-block" onclick="return confirm('<?php echo JText::_('COM_STTNMLS_PLAT_OFF_SHURE');?>');" style="white-space: normal;">
        <?php echo JText::_('COM_STTNMLS_PLATUSL_OFF'); ?>
    </a>
<?php endif; ?>

<p class="text-justify">
    <?php echo JText::sprintf('COM_STTNMLS_PLAT_OBJ_UP_DESC', $params->get('upobj', 30));?>
</p>
