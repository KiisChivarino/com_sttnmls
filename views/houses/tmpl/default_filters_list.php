<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );


// Get JOOMLA core classes
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');


$filters = array();

if($this->filtersList) {
    foreach($this->filtersList as $filter) {
        if(is_array($filter->value)) {
            $filter->value = implode(', ', $filter->value);
        }

        $str = (($filter->isFlag) ? '<b>' . $filter->name . '</b>' : '<b>' . $filter->name . ':</b> <span>' . $filter->value . '</span>');

        if($filter->link == '') {
            $filters[] = $str;
        } else {
            $filters[] = '<a href="' . $filter->link . '">' . $str . ' [<span style="color:red">x</span>]</a>';
        }
    }
}


?>

<?php if($filters) : ?>
<div class="well">
    <?php echo implode(', ', $filters) ?>
</div>
<?php endif; ?>


