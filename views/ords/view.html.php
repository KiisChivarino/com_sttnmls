<?php
defined('_JEXEC') or die ('Restricted access');
require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
require_once JPATH_COMPONENT.'/helpers/sttnmlshelper.php';

class SttNmlsViewOrds extends JViewLegacy
{
	function display($tpl = null) 
	{
		// JOOMLA Instances
		$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');

		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->agentBalanceInfo = $mAgent->getAgentBalanceInfo();
		$this->params = $this->state->get('parameters.menu');

		if($this->params) {
			$this->_prepareDocument();
		}

		parent::display($tpl);
	}

	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$title	= null;

		$title = $this->params->get('page_title', '');
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$document = JFactory::getDocument();
		$document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$document->setMetadata('robots', $this->params->get('robots'));
		}
	} 
}
