<?php defined('_JEXEC') or die;

abstract class SttnmlsValidateHelper {
    
    public static function integer($val) {
        return preg_match('/^[\-+]?[0-9]+$/', $val);
    }
    
    public static function is_natural($val) {
        return ctype_digit((string) $val); 
    }
    
    public static function is_natural_no_zero($val) {
        return ($val != 0 && ctype_digit((string) $val));
    }
    
    public static function alpha_dash($str)
    {
        return (bool) preg_match('/^[a-z0-9_-]+$/i', $str);
    }
    
    public static function alpha_dash_ru_advanced($str)
    {
        return (bool) preg_match("/^[a-zа-яё0-9., ()\[\]_-]+$/ui", $str);
    }
    
    public static function valid_url($str)
    {
        if (empty($str))
        {
            return FALSE;
        }
        elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches))
        {
            if (empty($matches[2]))
            {
                return FALSE;
            } elseif ( ! in_array($matches[1], array('http', 'https'), TRUE)) {
                return FALSE;
            }
            $str = $matches[2];
        }

        $str = 'http://'.$str;

        // There's a bug affecting PHP 5.2.13, 5.3.2 that considers the
        // underscore to be a valid hostname character instead of a dash.
        // Reference: https://bugs.php.net/bug.php?id=51192
        if (version_compare(PHP_VERSION, '5.2.13', '==') OR version_compare(PHP_VERSION, '5.3.2', '=='))
        {
            sscanf($str, 'http://%[^/]', $host);
            $str = substr_replace($str, strtr($host, array('_' => '-', '-' => '_')), 7, strlen($host));
        }

        return (filter_var($str, FILTER_VALIDATE_URL) !== FALSE);
    }
    
    public static function valid_email($str)
    {
        return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }
    
    /**
     * Valid_md5 - метод класса для проверки соответствия строки коду md5
     *
     * @access  public
     * @param   string  $str
     * @return  bool
     */
    public static function valid_md5($str)
    {
        return ( ! preg_match('/^[a-f0-9]{32}$/i', $str)) ? FALSE : TRUE;
    }
    // --------------------------------------------------------------------


}