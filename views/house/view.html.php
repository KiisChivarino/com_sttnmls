<?php defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewHouse extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->_allow_change_state = FALSE;

        // JOOMLA Instances
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $apart_model = JModelLegacy::getInstance('apart', 'SttNmlsModel');
        $agent_model = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $order_model =  JModelLegacy::getInstance('order', 'SttNmlsModel');
        $params = JComponentHelper::getParams('com_sttnmls');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');

        // Доступ к изменению состояния проверки объекта
        if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
            $this->_allow_change_state = TRUE;
        }
        
        $this->item = $this->get('Item');
        $this->activeComplaints = $this->get('ActiveComplaint');
        $this->agent = $this->get('AgentInfo');
        $this->myagent = $apart_model->findAgentByID($user->id);
        $this->order_info = $order_model->getItem();
        $this->stype = (($this->item->VARIANT == 2) ? 1 : 0);
        $this->imgpath = $this->get('Imgpath');
        $this->tp = $this->get('Tp');
        $this->agthumb = $this->get('ComUserThumb');
        $this->what = $this->get('What');
        $task = JRequest::getVar('task', '');    
            
        if($task == 'edit')
        {
            $this->optwmaterial = $this->get('OptWmaterial');
            $this->optheat = $this->get('OptHeat');
            $this->optwc = $this->get('OptWc');
            $this->optwater = $this->get('OptWater');
            $this->optroof = $this->get('OptRoof');
            $this->optfoundat = $this->get('OptFoundat');
            $this->optbtype = $this->get('OptBtype');
            $this->optcity = $this->get('OptCity');
            $this->firm_agents = $agent_model->getFirmAgentsList($_REQUEST['cid']);
            $chs = $this->get('CheckAr');
            $this->checks1 = $chs->checks1;
            $this->checks2 = $chs->checks2;
        } else {
            if(!$this->item) {
                header("HTTP/1.0 404 Not Found");
            }
            $this->userperm = $this->get('UserPerm');
            $doc->setTitle($this->get('Title'));
            $doc->setDescription($this->get('Desc'));
            $this->arLink = $this->get('SimilarObjects');
        }

        parent::display($tpl);
    }
}
