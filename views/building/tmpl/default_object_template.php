<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Подключение плагинов раздела CAPTCHA
JPluginHelper::importPlugin('captcha');

// Load JOOMLA core frameworks
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// RECAPTCHA Instance
$plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
$plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
$plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript("http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU");
$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$script .= <<<JS
    var OBJECT_TYPE = 1;
    var CARDNUM="{$this->item->CARDNUM}";
    var COMPID="{$this->item->COMPID}";	
    var g_u = "{$this->item->gorod}, {$this->item->ulica}";
    ymaps.ready(init);

    jQuery(document).ready(function($){
        $.ajax({
            type: "POST",
            url: ROOT_URI + "index.php?option=com_sttnmls&view=aparts&format=raw",
            data: {
                stype: 0,
                what: 0,
                builder: {$this->item->comp},
                building: {$this->item->CARDNUM}
            },
            dataType: 'HTML',
            success: function(response) {
                $('#aparts').html(response);
            }
        });

        $('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});
    });
    
JS;
if($this->item->gorod == SttNmlsLocal::TText('COM_STTNMLS_OBL'))
	$script .= ' var _zoom = "13";' . "\n";
else
	$script .= ' var _zoom = "15";' . "\n";
$doc->addScriptDeclaration($script);
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.inputmask.js");
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.yandex.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);

// Инициализация капчи
if($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
    // TODO: Работает только с версией reCAPTCHA 2.0. Хак для использования
    //       в нескольких формах на странице

    $plg_recaptcha_pub_key = $plg_recaptcha_params->get('public_key');
    $plg_recaptcha_theme = $plg_recaptcha_params->get('theme2');

    $doc->addScript(($app->isSSLConnection() ? 'https' : 'http') . '://www.google.com/recaptcha/api.js?hl=ru-RU&onload=onloadCallback&render=explicit');
    $captcha_code = <<<JS
        var onloadCallback = function() {
            grecaptcha.render("captcha_user_request_form", {sitekey: "{$plg_recaptcha_pub_key}", theme: "{$plg_recaptcha_theme}", size: "compact"});
            grecaptcha.render("captcha_rcall_form", {sitekey: "{$plg_recaptcha_pub_key}", theme: "{$plg_recaptcha_theme}", size: "compact"});
        };

JS;
    $doc->addScriptDeclaration($captcha_code);
}


$pics = (trim($this->item->PICTURES) != '') ? explode(';',$this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';',$this->item->PICNAMES) : '';
$socbut = $params->get('socbut', '');
$region = $params->get('region', 'Курская область');
$kursk = $params->get('kursk', 'Курск');
$kurske = $params->get('kurske', 'Курске');
$regione = $params->get('regione', 'Курской области');

$kv = Array('','I',"II","III","IV");

$building_progress_images_list = (($this->item->progress_photos != '') ? unserialize($this->item->progress_photos) : array());

?>

<div class="object-card-wrapper">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#main" aria-controls="main" role="tab" data-toggle="tab">
                Общая информация
            </a>
        </li>
        <?php if($this->item_building_plans) : ?>
        <li role="presentation">
            <a href="#bplans" aria-controls="bplans" role="tab" data-toggle="tab">
                Планировки
            </a>
        </li>
        <?php endif; ?>

        <?php if($building_progress_images_list) : ?>
        <li role="presentation">
            <a href="#progress" aria-controls="progress" role="tab" data-toggle="tab">
                Ход строительства
            </a>
        </li>
        <?php endif; ?>

        <?php if($this->item->aparts_counts) : ?>
        <li role="presentation">
            <a href="#aparts" class="tab-danger" aria-controls="aparts" role="tab" data-toggle="tab">
                В продаже:
                <?php echo $this->item->aparts_counts ?>
            </a>
        </li>
        <?php endif; ?>
    </ul>

    <div class="tab-content">
        <!-- TAB #1: Main -->
        <div role="tabpanel" class="tab-pane active" id="main">
            <article class="clearfix">
                <div class="row">
                    <!-- SIDEBAR -->
                    <div class="col-sm-4 col-md-4 col-sttnmls-photo">
                        <?php if ($pics != ''): ?>
                            <div class="sttnmls_photo">
                                <?php foreach($pics as $i => $photo): ?>
                                    <a rel="group1" class="fbox <?php echo (($i > 0) ? 'small' : 'big' )?>" title="" href="<?php echo SttnmlsHelper::getLinkPhoto($photo, 'big'); ?>">
                                        <img class="thumb img-thumbnail" src="<?php echo SttnmlsHelper::getLinkPhoto($photo, (($i > 0) ? 'smallcrop' : 'big' )); ?>"<?php echo ((isset($picnames[$i]) && trim($picnames[$i]) != '') ? ' title="' . trim($picnames[$i]) . '"' : '') ?> />
                                    </a>
                                    <?php $i++ ?>
                                <?php endforeach; ?>
                                <div class="clearfix"></div>
                            </div>
                        <?php endif; ?>
                        <?php if($this->item->VTOUR && $this->item->VTOUR!='sWebLabel1') : ?>
                            <div>
                                <noindex>
                                    <a rel="rel nofollow" href="<?php echo $this->item->VTOUR; ?>" target="_blank">
                                        <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/3dm.png"/>
                                    </a>
                                </noindex>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- /SIDEBAR -->

                    <!-- CONTENT -->
                    <div class="col-sm-8 col-md-8">
                        <a name="TopShowObject"></a>

                        <?php if( $user->authorise('core.admin') OR $this->userperm == 1) : ?>
                            <!-- Панель управления объектом -->
                            <?php $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&layout=edit&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->comp); ?>
                            <div class="sttnmls_photo_manager well well-sm text-right" style="margin-bottom: 2px;">

                                <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-xs" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT') ?>
                                </a>

                                <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=building&task=removeObject&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID) ?>" class="btn btn-danger btn-xs" onclick="if(!confirm('<?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_CONFIRM') ?>')) return false;">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_DELETE') ?>
                                </a>
                            </div>
                            <!-- //Панель управления объектом -->
                        <?php endif; ?>

                        <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 1) : ?>
                            <div class="alert alert-warning">
                                <?php echo JText::_('COM_STTNMLS_PREMODER_'); ?>
                            </div>
                        <?php endif; ?>


                        <h1><?php echo $this->item->title ?></h1>
                        <p class="text-justify"><?php echo $this->item->fulltitle ?></p>


                        <?php if($this->activeComplaints) : ?>
                            <p class="lead">
                                <a href="#complaints" class="label label-danger">
                                    [<?php echo JText::_('COM_STTNMLS_LABEL_COMPLAINTS_EXIST_ACTIVE') ?>]
                                </a>
                            </p>
                        <?php endif; ?>
                        <a href="#YMapsID" class="object-address-link" style="color: #777;">
                            <?php
                            if($this->item->gorod == $kursk) {
                                echo $kursk;
                            } else if($this->item->gorod == $region) {
                                echo $region;
                            } else {
                                echo $this->item->gorod;
                            }
                            if($this->item->raion != '' && $this->item->RAIONID > 1) {
                                $r = str_replace('р-н', '', $this->item->raion);
                                $r = str_replace('район', '', $this->item->raion);
                                $r = str_replace('  ', ' ', $this->item->raion);
                                echo ', ' . $r .' р-н';
                            }
                            if($this->item->mraion != '') {
                                echo ', микрорайон ' . $this->item->mraion;
                            }
                            ?>
                        </a>&nbsp;
                        <a href="#YMapsID" class="object-address-link">
                            На карте
                        </a>

                        <div style="margin-top:20px;">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_CITY') ?></th>
                                    <td><?php echo $this->item->gorod ?></td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_RAION') ?></th>
                                    <td><?php echo $r ?></td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_ADDRESS') ?></th>
                                    <td><?php echo $this->item->ulica . (($this->item->haap) ? ', ' . $this->item->haap : '') ?></td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_DATE') ?></th>
                                    <td>
                                        <?php printf('%s кв. %s г.', $kv[$this->item->datekvar], $this->item->dateyear) ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_PROJECT_DOCS') ?></th>
                                    <td>
                                        <?php
                                            $link = '';
                                            if($this->item->proj_file != '') {
                                                $link = JURI::root() . 'files/buildings/' . $this->item->proj_file;
                                            } else {
                                                if(preg_match('#(http|https):\/\/#i', $this->item->project)) {
                                                    $link = $this->item->project;
                                                }
                                            }
                                        ?>
                                        <?php if($link != '') : ?>
                                            <a class="btn btn-sm btn-success" href="<?php echo $link ?>" target="_blank">
                                                <span class="glyphicon glyphicon-briefcase"></span>
                                                <span class="sr-only"><?php echo $item_doc->title ?></span>
                                            </a>
                                        <?php else : ?>
                                            -
                                        <?php endif; ?>
                                    </td>
                                </tr>

                                <?php if($this->buildings_classes_list && isset($this->buildings_classes_list[$this->item->CLASS])) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_CLASS') ?></th>
                                    <td>
                                        <?php
                                            $building_class = $this->buildings_classes_list[$this->item->CLASS];
                                            echo $building_class->title;
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <?php if($this->wmater) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_FILTER_NAME_WMATERID') ?></th>
                                    <td>
                                        <?php
                                            foreach($this->wmater as $item) {
                                                if($item->ID == $this->item->WMATERID) {
                                                    echo $item->NAME;
                                                }
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_TOTAL_STAGES') ?></th>
                                    <td><?php echo $this->item->HSTAGE ?></td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_APARTS') ?></th>
                                    <td><?php echo $this->item->flatcount ?></td>
                                </tr>

                                <?php if($this->buildings_types) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_APARTS_ROOMS_COUNT') ?></th>
                                    <td>
                                        <?php
                                            $rooms_count = json_decode($this->item->type);
                                            $rc = array();
                                            if($rooms_count) {
                                                foreach($rooms_count as $c) {
                                                    if(isset($this->buildings_types[$c])) {
                                                        $rc[] = $this->buildings_types[$c]->title;
                                                    }
                                                }
                                            }
                                            echo implode(', ', $rc);
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?></th>
                                    <td>
                                        <?php printf('От %s до %s м<sup>2</sup>', $this->item->areafrom, $this->item->areato) ?>
                                    </td>
                                </tr>

                                <?php if($this->buildings_furnish_list && isset($this->buildings_furnish_list[$this->item->furnish])) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_FURNISH') ?></th>
                                    <td>
                                        <?php
                                            $building_furnish = $this->buildings_furnish_list[$this->item->furnish];
                                            echo $building_furnish->title;
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_PRICE_QM') ?></th>
                                    <td>
                                        <?php printf('От %s до %s руб.', number_format($this->item->pricefrom, 0, ',', ' '), number_format($this->item->priceto, 0, ',', ' ')) ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2" class="text-center"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_COMMERCIAL_OBJECTS') ?></th>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_COUNT') ?></th>
                                    <td>
                                        <?php echo $this->item->comcount ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?></th>
                                    <td>
                                        <?php printf('От %s до %s м<sup>2</sup>', $this->item->comareafrom, $this->item->comareato) ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_PRICE_QM') ?></th>
                                    <td>
                                        <?php printf('От %s до %s руб.', number_format($this->item->compricefrom, 0, ',', ' '), number_format($this->item->compriceto, 0, ',', ' ')) ?>
                                    </td>
                                </tr>

                                <?php if($this->payment_types) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_PAYMENT') ?></th>
                                    <td>
                                        <?php
                                            $payment_types = json_decode($this->item->pay);
                                            $pt = array();
                                            if($payment_types) {
                                                foreach($payment_types as $t) {
                                                    if(isset($this->payment_types[$t])) {
                                                        $pt[] = $this->payment_types[$t]->title;
                                                    }
                                                }
                                            }
                                            echo implode(', ', $pt);
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_CONTRACT') ?></th>
                                    <td>
                                        <?php
                                            $link = '';
                                            if($this->item->dog_file != '') {
                                                $link = JURI::root() . 'files/buildings/' . $this->item->dog_file;
                                            } else {
                                                if(preg_match('#(http|https):\/\/#i', $this->item->dogovor)) {
                                                    $link = $this->item->dogovor;
                                                }
                                            }
                                        ?>
                                        <?php if($link != '') : ?>
                                            <a class="btn btn-sm btn-success" href="<?php echo $link ?>" target="_blank">
                                                <span class="glyphicon glyphicon-briefcase"></span>
                                                <span class="sr-only"><?php echo $item_doc->title ?></span>
                                            </a>
                                        <?php else : ?>
                                            -
                                        <?php endif; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_PARKING') ?></th>
                                    <td><?php echo $this->item->park ?></td>
                                </tr>

                                <?php if($this->builders_list && isset($this->builders_list[$this->item->comp])) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_BUILDER') ?></th>
                                    <td>
                                        <?php
                                            $builder = $this->builders_list[$this->item->comp];
                                        ?>
                                        <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $builder->ID) ?>" target="_blank">
                                            <?php echo $builder->NAME ?>
                                        </a>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <?php if($this->banks_list) : ?>
                                <tr>
                                    <th><?php echo JText::_('COM_STTNMLS_LABEL_ACCREDITATION') ?></th>
                                    <td>
                                        <?php
                                            $banks = json_decode($this->item->banks);
                                            $items = array();
                                            if($banks) {
                                                foreach($banks as $b) {
                                                    if(isset($this->banks_list[$b])) {
                                                        $items[] = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->banks_list[$b]->ID) . '" target="_blank">' . $this->banks_list[$b]->NAME . '</a>';
                                                    }
                                                }
                                            }
                                            echo implode(', ', $items);
                                        ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                            </table>
                        </div>

                        <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 0): ?>
                            <p>
                                <?php echo $this->udobstva; ?>
                            </p>
                        <?php else : ?>
                            <div id="dopinfo"<?php echo (($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 2) ? ' class="hide"' : '') ?>>
                                <h3><?php echo JText::_('COM_STTNMLS_DOP'); ?>:</h3>
                                <div><?php echo $this->item->MISC ?></div>
                            </div>
                        <?php endif; ?>

                        <?php
                        $showContactInfo = FALSE;
                        if($this->myagent && $this->item->COMPID == $this->myagent->COMPID) {
                            // Пользователь - есть член "агентства", владеющего объектом

                            // 1. У объекта разрешено отображение контактов собственника
                            if($this->item->sob_visible == 1) {
                                $showContactInfo = TRUE;
                            }

                            // 2. Пользователь - владелец объекта
                            if($this->item->AGENTID == $this->myagent->ID) {
                                $showContactInfo = TRUE;
                            }

                            // 3. Пользователь - руководитель "агентства", владеющего объектом
                            if($this->myagent->boss == 1) {
                                $showContactInfo = TRUE;
                            }

                            $perm = explode(',', $this->agent->prava);
                            if(in_array($this->myagent->kod, $perm)) {
                                $showContactInfo = TRUE;
                            }
                        }
                        ?>

                        <?php if($showContactInfo) : ?>
                            <!-- Разрешено отображение контактов собственника -->
                            <p>
                                <?php if($this->item->sob_tel != ''): ?>
                                    <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_PHONE') ?>:</strong> <?php echo $this->item->sob_tel; ?><br />
                                <?php endif; ?>

                                <?php if($this->item->sob_tel2 != ''): ?>
                                    <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_PHONE_ADDITIONAL') ?>:</strong> <?php echo $this->item->sob_tel2; ?><br />
                                <?php endif; ?>

                                <?php if($this->item->sob_fio != ''): ?>
                                    <strong><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_SOBSTVENNIK_FIO') ?>:</strong> <?php echo $this->item->sob_fio;?>
                                <?php endif; ?>
                            </p>
                            <!-- //Разрешено отображение контактов собственника -->
                        <?php endif; ?>

                        <div class="sttnmls_published_info" style="margin-bottom: 10px;">
                            <div class="calendar">
                                <b><?php echo JText::_('COM_STTNMLS_ADDED'); ?>:</b> <span class="datecard"><?php echo $this->item->datecr ?></span><br />
                                <b><?php echo JText::_('COM_STTNMLS_UPDATED'); ?>:</b> <span class="datecard"><?php echo $this->item->datemd ?></span>
                            </div>
                            <div class="visitors">
                                <b><?php echo JText::_('COM_STTNMLS_WATCHCNT'); ?>:</b> <span><?php echo $this->item->watchcount ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="sttnmls_user_actions row" style="margin-bottom: 10px;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="javascript:void(0);" role="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#rcallModalForm" data-keyboard="true">Заявка на показ</a>
                            </div>
                        </div>
                        <div id="rcallModalForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rcallModalLabel" aria-hidden="true">
                            <?php echo $this->loadTemplate('rcall_form') ?>
                        </div>
                        <hr />
                    <?php if ($user->authorise('core.admin')):?>
                        <div class="sttnmls_contact_info" style="margin-bottom: 10px;">
                            <h3><?php echo JText::_('COM_STTNMLS_CONTS'); ?>:</h3>


                            <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 0) : ?>
                                <div class="alert alert-warning">
                                    <?php echo JText::_('COM_STTNMLS_PREMODER'); ?>
                                </div>
                            <?php else : ?>
                                <div id="contacts" <?php echo (($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && $this->userperm == 2) ? ' class="hide"' : '') ?>>
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->comp) ?>" target="_blank">
                                                <img class="thumbnail" src="<?php echo $this->item->imgfirm ?>" alt="" />
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <b><?php echo $this->item->FIRMNAME ?></b>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if($this->item->flag == 2 && $this->item->mdays > $this->item->alldays && ($this->userperm == 2 OR $this->userperm == 3)) : ?>
                                <div class="sttnmls_moderator">
                                    <?php if($this->userperm == 2) : ?>
                                        <a href="javascript:void(0);" class="btn btn-warning btn-block btn-lg showModerateContacts">
                                            <?php echo JText::_('COM_STTNMLS_SHOWCONT') ?>
                                        </a><br />
                                        <div class="well well-sm" id="moderform">
                                            <form class="editform form-horizontal" action="<?php echo JRoute::_('index.php?option=com_sttnmls&Itemid=' . $menu->id, TRUE, -1) ?>" method="post">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-6 col-md-4">
                                                        <?php echo JText::_('COM_STTNMLS_Q1') ?>
                                                    </label>
                                                    <div class="col-sm-6 col-md-4">
                                                        <?php echo $this->item->q1opt ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-6 col-md-4">
                                                        <?php echo JText::_('COM_STTNMLS_Q2') ?>
                                                    </label>
                                                    <div class="col-sm-6 col-md-4">
                                                        <?php echo $this->item->q2opt ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-6 col-sm-6 col-md-offset-4 col-md-6">
                                                        <button type="button" id="objectSendModeratorInfo" class="btn btn-success" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="apart" data-otype="1">
                                                            <?php echo JText::_('COM_STTNMLS_SEND'); ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>


                                    <?php if($this->userperm == 3) : ?>
                                        <?php echo $this->item->listmoder ?>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif;?>
                        <div class="sttnmls_map">
                            <div id="YMapsID"></div>
                        </div>
                        <div class="sttnmls_complaint">
                            <a href="javascript:void(0);" class="showComplaintForm btn btn-danger btn-block"><?php echo JText::_('COM_STTNMLS_SENDOTZ'); ?></a>
                            <div id="complaint_form">
                                <?php echo $this->loadTemplate('complaint'); ?>
                            </div>
                        </div>

                        <?php if($this->activeComplaints && count($this->activeComplaints) > 0) : ?>
                            <div class="sttnmls_complaints_list" style="margin-top: 20px;">
                                <?php foreach($this->activeComplaints as $item) : ?>
                                    <div>
                                        <p><?php echo JText::_('COM_STTNMLS_LABEL_COMPLAINT_REASON_' . $item->rs) ?>:</p>
                                        <?php echo $item->desc; ?><br />
                                        <b>Проверено:</b> <?php if($item->status==0){ echo "Нет <img style='position:relative; top:4px;' src='http://www.iconsearch.ru/uploads/icons/crystalclear/16x16/edit_remove.png' alt='unapproved'/>"; }else{ echo "Да <img src='http://kurskmetr.ru/administrator/templates/isis/images/admin/icon-16-allow.png' alt='approved'/> [Объект не показывается в общем списке]"; }?><br />
                                        <span style="color:#999;"><b>Дата:</b> <?php echo date('d.m.Y H:i:s',strtotime($item->dateinp)); ?></span>
                                    </div>

                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                        <div class="sttnmls_socbuttons">
                            <?php echo $socbut;?>
                        </div>
                    </div>
                    <!-- /CONTENT -->
                </div>
            </article>
        </div>
        <!-- /TAB #1: Main -->


        <!-- TAB #2: Progress -->
        <?php if($this->item_building_plans) : ?>
        <div role="tabpanel" class="tab-pane" id="bplans">
            <?php if(isset($this->item_building_plans[0])) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_0') ?></h4>
                        <hr />

                        <?php
                            $this->current_rooms = 0;
                            echo $this->loadTemplate('building_plans_list_table');
                        ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($this->item_building_plans[1])) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_1') ?></h4>
                        <hr />

                        <?php
                            $this->current_rooms = 1;
                            echo $this->loadTemplate('building_plans_list_table');
                        ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($this->item_building_plans[2]) && $this->item_building_plans[2]) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_2') ?></h4>
                        <hr />

                        <?php
                            $this->current_rooms = 2;
                            echo $this->loadTemplate('building_plans_list_table');
                        ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($this->item_building_plans[3])) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_3') ?></h4>
                        <hr />

                        <?php
                            $this->current_rooms = 3;
                            echo $this->loadTemplate('building_plans_list_table');
                        ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($this->item_building_plans[4])) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_4') ?></h4>
                        <hr />

                        <?php
                            $this->current_rooms = 4;
                            echo $this->loadTemplate('building_plans_list_table');
                        ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <!-- /TAB #2: Progress -->

        <!-- TAB #3: BPlans -->
        <?php if($building_progress_images_list) : ?>
        <div role="tabpanel" class="tab-pane" id="progress">
            <?php echo $this->loadTemplate('building_progress_photo_list') ?>
        </div>
        <?php endif; ?>
        <!-- /TAB #3: BPlans -->

        <?php if($this->item->aparts_counts) : ?>
        <div role="tabpanel" class="tab-pane" id="aparts">

        </div>
        <?php endif; ?>

    </div>
</div>