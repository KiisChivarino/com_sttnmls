<?php
defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<div class="sttnmls"> 
	
    

    
    <?php
	if(isset($_REQUEST['cat_id']) && $_REQUEST['cat_id']!=0)
	{
		$cat_id=$_REQUEST['cat_id'];
		?>
        	<h2 style="float:left;"><?php echo $this->item->name;?></h2>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'ask', '&view=ask&cat_id=' . $cat_id ) ?>#ask_q" class="btn2 btn btn-default btn-sm" style="float:right; position:relative; top:12px;" id="btn_main">Задать вопрос</a>
            <div style="clear:both;"></div>
        	<a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'ask', '&view=ask&cat_id=' . $this->item->parent) ?>">Вернуться на уровень выше</a>
        <?php
	}else{
		$cat_id=0;
		?>
        <h2 style="float:left;">Вопрос-ответ</h2>
        <div style="clear:both;"></div>
        
        
        <div class="question_block" id="quest_main" style="border-top:5px solid #333; padding-bottom:0px; margin-top:20px; margin-bottom:20px; display:none;">
            <div style="font-size:14px; font-weight:bold; text-transform:uppercase">Задать вопрос</div>
            
            <?php if($this->user->id){?>
            <form method="post" onsubmit="return false;" id="new_quest_main">
                <div style="font-style:italic; padding-bottom:5px">
                	Выберите категорию
                </div>
                <select name="cat_id" id="cat_id_main">
                	<option value="0" disabled="disabled" selected="selected">Выберите категорию</option>
                    <?php
					foreach($this->items as $cat)
    				{
						?>
                        <option value="<?php echo $cat->id;?>"><?php echo $cat->name;?></option>
                        <?php
					}
					?>
                </select>
                <div style="font-style:italic; padding-bottom:5px">
                	Введите текст вопроса
                </div>
                <textarea name="qustion" id="question_main" style="width:675px; height:100px; border:1px solid #999; padding:5px;"></textarea>
                <div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form_main();">Отправить</div>
                <input type="hidden" name="act" value="send_question" />
            </form>
            <script language="javascript">
				function send_form_main()
				{
					err='';
					if(document.getElementById('question_main').value=='')
					{
						
						err+='Введите текст вопроса';
					}
					if(document.getElementById('cat_id_main').value=='0')
					{
						
						err+="\r\nВыберите категорию";
					}
					if(err=='')
					{
						document.getElementById('new_quest_main').submit();	
					}else{
						alert(err);
					}
				}
			</script>
             <?php
			}else{
			?>	
			
            <?php
			}
			?>
        </div>
       
        <?php
	}
	if(isset($_REQUEST['cat_id']) && $_REQUEST['cat_id']!=0)
	{
		if($this->item->fulldescription)
		{
			?>
            	<div style="font-size:12px; line-height:155%; padding-bottom:20px; padding-top:20px;"><?php echo $this->item->fulldescription;?></div>
            <?php	
		}
		if($this->item->experts)
		{
			?>
			<div style="font-size:14px; font-weight:bold; padding-bottom:10px;">Эксперты</div>
			<div class="sttonline">
				<p>
				<?php
				$count = 0;
				$i=0;
				foreach ( $this->item->experts as $user )
				{
					$link = '';
					$a = '';
					$avatar = Avatar::getUserThumbAvatar($user->id);
					$name = $user->name;
					if($user->agentid) {
						$name = $user->agentname;
						$link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $user->agentid . '&comp_id=' . $user->compid) .'" title="'.htmlspecialchars($name).'">';
						$a = '</a>';
					}
				?>
						<?php echo $link; ?>
							<img width="64" height="64" src="<?php echo $avatar; ?>" alt="<?php echo htmlspecialchars($name); ?>" style="padding: 2px; border: solid 1px #ccc;" />
						<?php echo $a; ?>
					<?php
					$i++;
					$count++;
				}
					?>
				</p>
			</div>
			<?php
		}
		?>
        <?php
		if($this->questions)
		{
			
			$pad=0;
			
			function view_comments($id, $q, $user, $pad, $r)
			{
				
				
				$db=JFactory::getDbo();	
				
				$db->setQuery('select * from #__sttnmlsaskanswer WHERE parent="'.$id.'" ORDER BY id ASC');
				$answer= $db->loadObjectList();
				foreach($answer as $t=>$an)
				{
					$query	= 'SELECT DISTINCT a.id, a.name, concat(ifnull(ag.SIRNAME,""), " ", ifnull(ag.NAME,"") , " ", ifnull(ag.SECNAME,"")) as agentname, ifnull(ag.ID,0) as agentid, ifnull(ag.COMPID,0) as compid FROM #__users AS a left join #__sttnmlsvocagents as ag on a.id=ag.userid WHERE a.block=0 AND ag.userid="'.$an->from.'" ORDER BY ag.SIRNAME';
					$db->setQuery($query);
					$users	= $db->loadObject();
					$answer[$t]->userinfo=$users;
				}
				
				
				foreach($answer as $a)
						{
							if($r==0)
							{
								$back="#fff";
								$r++;	
							}else{
								$back="#EBEBEB";
								$r=0;
							}
							if($pad>=1)
							{
								if($pad>3)
								{
									$pad=3;	
								}
								$pd=90*$pad;
								$wd=500-$pd;
								$wd2=400-$pd;
							}else{
								$pd="10";
								$wd=500;
								$wd2=400;
							}
						?>
                        <div style="border-bottom:1px solid #cfcfcf; padding:10px; width:<?php echo $wd;?>px; margin-left:<?php echo $pd;?>px;">
                        <?php
                            $link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $a->userinfo->agentid . '&comp_id=' . $a->userinfo->compid) . '" title="'.htmlspecialchars($a->userinfo->agentname).'">';
						?>
                        <?php echo $link;?>
                        <div style="float:left">
							<?php
                            $avatar = Avatar::getUserThumbAvatar($a->userinfo->id);
                            ?>
                            <img width="64" height="64" src="<?php echo $avatar; ?>" alt="<?php echo htmlspecialchars($name); ?>" style="padding: 2px; border: solid 1px #ccc;" />
                        </div>
                        </a>
                        <div style="float:left; margin-left:10px; width:<?php echo $wd2;?>px;">
                        	<div style="font-size:11px; color:#666">
								<?php echo  substr($a->date, 0, -3);?>
                            	<?php
								if($a->date_edit!='0000-00-00 00:00:00')
								{
								?>
                                	[Отредактировано: <?php echo substr($a->date_edit, 0, -3);?>]
                                <?php	
								}
								?>
                            </div>
                            
                            
                            <?php/*?><b>От: <?php echo $link.$a->userinfo->agentname;?></a></b>*/?>
                            <div style=" padding-top:5px; font-style:italic;">
								<?php echo $a->text;?>
                            </div>
                            
                            <?php
							
							
							
							if($user->id==$a->from && $user->id)
							{
							?>
                            <span style="color:#09C; font-size:11px; font-weight:bold; cursor:pointer; border-bottom:1px dotted #09c; display:inline-block;" onclick="show_answer('ans_<?php echo $a->id;?>');">Редактировать</span>
                            
                            <?php
							}
							?>
                            <?php
							if(($user->id==$a->from || $user->id==$q->from) && $user->id)
							{
							?>
                            <span style="margin-left:10px; color:#09C; font-size:11px; font-weight:bold; cursor:pointer; border-bottom:1px dotted #09c; display:inline-block;" onclick="show_answer('com_<?php echo $a->id;?>');">Комментировать</span>
                            
                            <?php
							}
							?>
                            <?php
							if($user->id==$a->from && $user->id)
							{
							?>
                            	
                                <div id="ans_<?php echo $a->id;?>" style="display:none;">
                                	<form method="post" id="form_<?php echo $a->id;?>">
                                        <input type="hidden" name="answer_id" value="<?php echo $a->id;?>" />
                                        Введите текст ответа
                                        <textarea name="text" id="text_<?php echo $a->id;?>" style="width:100%; height:100px; border:1px solid #999; padding:5px;"><?php echo $a->text;?></textarea>
                                        <div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form_edit('<?php echo $a->id;?>');">Отправить</div>
                                        <input type="hidden" name="act" value="edit_answer" />
                                    </form>
                                </div>
                            <?php	
							}
							?>
                            <?php
							if(($user->id==$a->from || $user->id==$q->from) && $user->id)
							{
							?>
								<div id="com_<?php echo $a->id;?>" style="display:none;">
									<form method="post" id="formcom_<?php echo $a->id;?>">
                                    	<input type="hidden" name="quest_id" value="<?php echo $q->id;?>" />
										<input type="hidden" name="answer_id" value="<?php echo $a->id;?>" />
										Введите текст комментария
										<textarea name="text" id="textcom_<?php echo $a->id;?>" style="width:100%; height:100px; border:1px solid #999; padding:5px;"></textarea>
										<div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form_com('<?php echo $a->id;?>');">Отправить</div>
										<input type="hidden" name="act" value="send_comm" />
									</form>
								</div>
							<?php
							}
							?>
                        </div>
                        
                        <div style="clear:both"></div>
                        
                        </div>
                        <?php
						$pad++;
						view_comments($a->id, $q, $user, $pad, $r);
						?>
                        <?php
						}
			}

			?>
			<div style="font-size:14px; font-weight:bold; padding-bottom:10px; padding-top:20px;">Вопросы</div>
			<?php
			foreach($this->questions as $q)
			{
				?>
            <hr/>    
                <div class="question_block" id="qss_<?php echo $q->id;?>" style="margin: 15px 0">
                	<?php 
                    $link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id='. $q->userinfo->agentid . '&comp_id=' . $q->userinfo->compid) .'" title="'.htmlspecialchars($q->userinfo->agentname).'">';
                    /*if($anonym==1 || $q->from==$this->user->id)*/ echo $link;
                    ?>
                        <div style="float:left; background:#FFF;" >
                            <?php
                            $anonym=1;
                            if($anonym==1 || $q->from==$this->user->id)
                            {
                            $avatar = Avatar::getUserThumbAvatar($q->userinfo->id);
                            ?>
                                <img width="64" height="64" src="<?php echo $avatar; ?>" alt="<?php echo htmlspecialchars($name); ?>" style="padding: 2px; border: solid 1px #ccc;" />
                            <?php
                            }else{
                            ?>
                                <img width="64" height="64" src="http://www.iconsearch.ru/uploads/icons/basicset/64x64/user_64.png" />
                            <?php
                            }
                            ?>
                        </div>
                    <?//if($anonym==1 || $q->from==$this->user->id){?>
                    </a>
                    <?//}?>
                    <div style="float:left; margin-left:10px; width:590px; background:#FFF;">
                    	<div style="font-size:11px; color:#666"><?php echo substr($q->date, 0, -3);?></div>
                        <?php/*
                            $link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id='. $q->userinfo->agentid . '&comp_id=' . $q->userinfo->compid) .'" title="'.htmlspecialchars($q->userinfo->agentname).'">';
							if($anonym==1 || $q->from==$this->user->id)
							{
							?>
                            <b>От: <?php echo $link.$q->userinfo->agentname;?></a></b>
                            <?php
							}else{
							?>
                            <b>От: Анонимный пользователь</b>
                            <?php
							}
							*/?>
                        <div style=" padding-top:5px; font-weight:bold;">
                        	<?php echo $q->text;?>
                        </div>
                        <?php
						$r=0;
						foreach($q->answers as $a)
						{
							$pad=0;
							
						?>
                        <div style="border-bottom:1px solid #cfcfcf; padding:10px; width:585px;">
                        
                        <?php
                            $link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id='. $a->userinfo->agentid . '&comp_id=' . $a->userinfo->compid) .'" title="'.htmlspecialchars($a->userinfo->agentname).'">';
						?>
                        
                        <?php echo $link;?>
                            <div style="float:left">
							<?php
                            $avatar = Avatar::getUserThumbAvatar($a->userinfo->id);
                            ?>
                            <img width="64" height="64" src="<?php echo $avatar; ?>" alt="<?php echo htmlspecialchars($name); ?>" style="padding: 2px; border: solid 1px #ccc;" />
                            </div>
                        </a>
                        
                        <div style="float:left; margin-left:10px; width:450px;">
                        	<div style="font-size:11px; color:#666">
								<?php echo substr($a->date, 0, -3)?>
                            	<?php
								if($a->date_edit!='0000-00-00 00:00:00')
								{
								?>
                                	[Отредактировано: <?php echo substr($a->date_edit, 0, -3);?>]
                                <?php	
								}
								?>
                            </div>
                            
                            
                            <?php/*?><b>От: <?php echo $link.$a->userinfo->agentname;?></a></b><?php*/?>
                            
                            <div style=" padding-top:5px; font-style:italic;">
								<?php echo $a->text;?>
                            </div>
                            
                            <?php
							if($this->user->id==$a->from && $this->user->id)
							{
							?>
                            <span style="color:#09C; font-size:11px; font-weight:bold; cursor:pointer; border-bottom:1px dotted #09c; display:inline-block;" onclick="show_answer('ans_<?php echo $a->id;?>');">Редактировать</span>
                            
                            <?php
							}
							?>
                            <?php
							if(($this->user->id==$a->from || $this->user->id==$q->from) && $this->user->id)
							{
							?>
                            <span style="margin-left:10px; color:#09C; font-size:11px; font-weight:bold; cursor:pointer; border-bottom:1px dotted #09c; display:inline-block;" onclick="show_answer('com_<?php echo $a->id;?>');">Комментировать</span>
                            
                            <?php
							}
							?>
                            <?php
							if($this->user->id==$a->from && $this->user->id)
							{
							?>
                            	
                                <div id="ans_<?php echo $a->id;?>" style="display:none;">
                                	<form method="post" id="form_<?php echo $a->id;?>">
                                        <input type="hidden" name="answer_id" value="<?php echo $a->id;?>" />
                                        Введите текст ответа
                                        <textarea name="text" id="text_<?php echo $a->id;?>" style="width:100%; height:100px; border:1px solid #999; padding:5px;"><?php echo $a->text;?></textarea>
                                        <div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form_edit('<?php echo $a->id;?>');">Отправить</div>
                                        <input type="hidden" name="act" value="edit_answer" />
                                    </form>
                                </div>
                            <?php	
							}
							?>
                            <?php
							if(($this->user->id==$a->from || $this->user->id==$q->from) && $this->user->id)
							{
							?>
								<div id="com_<?php echo $a->id;?>" style="display:none;">
									<form method="post" id="formcom_<?php echo $a->id;?>">
                                    	<input type="hidden" name="quest_id" value="<?php echo $q->id;?>" />
										<input type="hidden" name="answer_id" value="<?php echo $a->id;?>" />
										Введите текст комментария
										<textarea name="text" id="textcom_<?php echo $a->id;?>" style="width:100%; height:100px; border:1px solid #999; padding:5px;"></textarea>
										<div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form_com('<?php echo $a->id;?>');">Отправить</div>
										<input type="hidden" name="act" value="send_comm" />
									</form>
								</div>
							<?php
							}
							?>
                        </div>
                        
                        
                        
                        <div style="clear:both"></div>
                        </div>
                        <?php
						$pad++;
						view_comments($a->id, $q, $this->user, $pad, $r);
						?>
                        <?php
						}
						?>
                        <script language="javascript">
							function send_form_edit(id)
							{
								err='';
								if(document.getElementById('text_'+id).value=='')
								{
									
									err+='Введите текст ответа';
								}
								if(err=='')
								{
									document.getElementById('form_'+id).submit();	
								}else{
									alert(err);
								}
							}
							
							function send_form_com(id)
							{
								err='';
								if(document.getElementById('textcom_'+id).value=='')
								{
									
									err+='Введите текст комментария';
								}
								if(err=='')
								{
									document.getElementById('formcom_'+id).submit();	
								}else{
									alert(err);
								}
							}
							
							function send_answer_all(id)
							{
								err='';
								if(document.getElementById('answer_text_'+id).value=='')
								{
									
									err+='Введите текст ответа';
								}
								if(err=='')
								{
									document.getElementById('send_answer_'+id).submit();	
								}else{
									alert(err);
								}
							}
						
							
						</script>  
                    </div>
                	<div style="clear:both;"></div>
                    
                    
                    
                    <div style="">
						<?php
						$answer=0;
						foreach ( $this->item->experts as $user )
						{
							if($user->id==$this->user->id)
							{
								$answer=1;
							}
						}
						if(($answer==1 || $q->from==$this->user->id) && $this->user->id!=0)
						{
						?>
                        <div style="font-size:14px; font-weight:bold; padding-bottom:0px; padding-top:20px;">Ответить</div>
                        <form method="post" style="padding-top:0px;" id="send_answer_<?php echo $q->id;?>">
                            <input type="hidden" name="quest_id" value="<?php echo $q->id;?>" />
                            Введите текст ответа
                            <textarea name="text" id="answer_text_<?php echo $q->id;?>" style="width:100%; height:100px; border:1px solid #999; padding:5px;"></textarea>
                            <div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_answer_all('<?php echo $q->id;?>');">Отправить</div>
                            <input type="hidden" name="act" value="send_answer" />
                        </form>
                        <?php	
						}
						?>
                    </div>
                    
                    
                </div>
                
                
                
                	
                <?php
			}
			?>
			<?php
		}
		?>
        <?php
	}
    foreach($this->items as $cat)
    {
		if($cat->parent==$cat_id)
		{
			?>
            <div style="border-bottom:solid 1px #efefef; padding-bottom:10px;">
			<h3 style="float:left"><a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'ask', '&view=ask&cat_id=' . $cat->id) ?>"><?php echo $cat->name;?></a></h3>
            
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'ask', '&view=ask&cat_id=' . $cat->id) ?>#ask_q" class="btn2 btn btn-default btn-sm" style="float:right; position:relative; top:12px;" id="btn_main">Задать вопрос</a>
            <div style="clear:both"></div>
            <div style="position:relative; top:-5px; padding-bottom:10px; font-size:11px; color:#666">
            Всего вопросов: <?php echo $cat->questionscount->count;?> | 
			Всего ответов: <?php echo $cat->answerscount->count;?>
            </div>
            <?php if($cat->smalldescription)
			{
			?>
                <div style="font-size:12px; line-height:155%; padding-bottom:20px;">
                    <?php echo $cat->smalldescription;?>
                </div>
            <?php
			}
			?>
            <?php
			if($cat->experts)
			{
				?>
				<div style="font-size:14px; font-weight:bold; padding-bottom:10px;">Эксперты</div>
				<div class="sttonline">
					<p>
					<?php
					$count = 0;
					$i=0;
					foreach ( $cat->experts as $user )
					{
						$link = '';
						$a = '';
						$avatar = Avatar::getUserThumbAvatar($user->id);
						$name = $user->name;
						if($user->agentid) {
							$name = $user->agentname;
							$link = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id='. $user->agentid . '&comp_id=' . $user->compid) .'" title="'.htmlspecialchars($name).'">';
							$a = '</a>';
						}
					?>
							<?php echo $link; ?>
								<img width="64" height="64" src="<?php echo $avatar; ?>" alt="<?php echo htmlspecialchars($name); ?>" style="padding: 2px; border: solid 1px #ccc;" />
							<?php echo $a; ?>
						<?php
						$i++;
				
						$count++;
					}
						?>
					</p>
				</div>
				<?php
			}
			?>
            </div>
            <?php
		}
    }
    ?>
    
    <?php
	if(isset($_REQUEST['cat_id']) && $_REQUEST['cat_id']!=0)
	{
	?>
        <div class="question_block" id="ask_q" style="border-top:5px solid #333; padding-bottom:0px; margin-top:20px;">
            <div style="font-size:14px; font-weight:bold; text-transform:uppercase">Задать вопрос</div>
            
            <?php if($this->user->id){?>
            <form method="post" onsubmit="return false;" id="new_quest">
            	<input type="hidden" name="cat_id" value="<?php echo $this->item->id;?>" />
                <div style="font-style:italic; padding-bottom:5px">
                	Введите текст вопроса
                </div>
                <textarea name="qustion" id="question" style="width:675px; height:100px; border:1px solid #999; padding:5px;"></textarea>
                <div class="btnask btn btn-default btn-sm" style="display:block; width: 100px;" onclick="send_form();">Отправить</div>
                <input type="hidden" name="act" value="send_question" />
            </form>
            <script language="javascript">
				function send_form()
				{
					err='';
					if(document.getElementById('question').value=='')
					{
						
						err+='Введите текст вопроса';
					}
					if(err=='')
					{
						document.getElementById('new_quest').submit();	
					}else{
						alert(err);
					}
				}
			</script>
            <?php
			}else{
			?>
            <div style="padding-top:10px; padding-bottom:10px; font-style:italic;">
            	Для того чтобы задать вопрос пожалуйста <a href="<?php echo SttNmlsHelper::getSEFUrl('com_users', 'registration')?>">зарегистрируйтесь</a> или авторизируйтесь.
            </div>
            <?php	
			}
			?>
        </div>
    <?php
	}
	?>
    
    
</div>
