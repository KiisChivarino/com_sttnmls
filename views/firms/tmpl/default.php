<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once( JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');


// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);


$customScript = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$doc->addScriptDeclaration($customScript);

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$sort_icon = ' <span class="glyphicon glyphicon-sort-by-attributes' . (($listDirn == 'desc') ? '-alt' : '') . '"></span>';

$params = JComponentHelper::getParams('com_sttnmls');
$gild=$params->get('gild', '');
$gildurl=$params->get('gildurl', '');

?>
<div class="container-fluid">
    <!-- ROW #1: Page head -->
    <div class="row">
        <h2><?php echo $this->title->head; ?></h2>
        <?php if($this->title->descr != '') : ?>
        <p class="text-justify">
            <?php echo $this->title->descr; ?>
        </p>
        <?php endif; ?>

        <?php if(SttNmlsHelper::chkUserEditFirm(0)) : ?>
        <div class="well well-sm">
            <a class="btn btn-success btn-xs" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&layout=edit') ?>">
                <span class="glyphicon glyphicon-plus"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_CREATE_NEW_FIRM') ?>
            </a>
        </div>
        <?php endif; ?>
    </div>
    <!-- /ROW #1: Page head -->

    <!-- ROW #2: Search form -->
    <div class="row">
        <div class="thumbnail">
            <div class="caption">
                <form class="form-inline" action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firms') ?>" method="post" name="adminForm1" id="filterForm">
                    <div class="form-group">
                        <label>
                            <?php echo JText::_('COM_STTNMLS_LABEL_SEARCH_BY_TITLE') ?>
                        </label>
                        <div class="input-group">
                            <input type="text" id="searchFirmInput" class="form-control"  name="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" placeholder="<?php echo JText::_('COM_STTNMLS_SEARCH') ?>" autocomplete="off" />
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default">
                                    <span class="glyphicon glyphicon-search"></span>
                                    <?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
                                </button>
                                <button type="button" class="btn btn-default" onclick="document.id('searchFirmInput').value='';this.form.submit();">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /ROW #2: Search form -->

    <!-- ROW #3: Firms list -->
    <div class="row">
        <form action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firms') ?>" method="post" name="adminForm" id="adminForm">
            <input type="hidden" name="filter_search" value="<?php $this->escape($this->state->get('filter.search')); ?>" />
            <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
            <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
            <input type="hidden" name="limitstart" id="limitstart_t" value="0"/>
            <?php echo JHtml::_('form.token'); ?>


            <div class="col-md-12">
                <!-- ROW #3.1: Sort -->
                <div class="row">
                    <div class="h5"><?php echo JText::_('COM_STTNMLS_LABEL_SORT_BY_FIELD') ?>:</div>
                    <ul class="nav nav-pills">
                        <li role="presentation"<?php echo  (($listOrder == 'kitog') ? ' class="active"' : '') ?>>
                            <?php echo JHtml::_('grid.sort',  'рейтингу' . (($listOrder == 'kitog') ? $sort_icon : ''), 'kitog', $listDirn, $listOrder); ?>
                        </li>
                        <li role="presentation"<?php echo (($listOrder == 'realname') ? ' class="active"' : '') ?>>
                            <?php echo JHtml::_('grid.sort', JText::_('COM_STTNMLS_FIRMS_SORT1') . (($listOrder == 'realname') ? $sort_icon : ''), 'realname', $listDirn, $listOrder); ?>
                        </li>
                        <li role="presentation"<?php echo (($listOrder == 'cnt') ? ' class="active"' : '') ?>>
                            <?php echo JHtml::_('grid.sort', JText::_('COM_STTNMLS_FIRMS_SORT2') . (($listOrder == 'cnt') ? $sort_icon : ''), 'cnt', $listDirn, $listOrder); ?>
                        </li>
                    </ul>
                </div>
                <!-- /ROW #3.1: Sort -->

                <!-- ROW #3.2: Firms list -->
                <div class="row" style="margin-top: 20px;">
                <?php if($this->items) : ?>
                    <ul class="list-group">
                    <?php foreach($this->items as $item) : ?>
                        <?php
                            $link = (($item->article_url != '') ? $item->article_url : SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $item->ID));
                            $logo = Avatar::getLinkFirmLogo($item->ID, 0);

                            $name = $item->realname;
                            if(!$name) {
                                $name = $item->NAME;
                            }
                            if(!$name) {
                                $name = JText::_('COM_STTNMLS_FIRMS_NONAME');
                            }
                        ?>
                        <li class="list-group-item">
                            <!-- Firm logo -->
                            <div class="col-md-2 text-center">
                                <a href="<?php echo $link ?>" target="_blank">
                                    <img class="img-thumbnail" src="<?php echo $logo ?>" alt="" />
                                </a>
                            <?php if ($item->gild==1 && $gild && $gildurl) : ?>
                                <br/>
                                <br/>
                                <!-- noindex -->
                                <a href="<?php echo $gildurl ?>" rel="nofollow" title="<?php echo $gild ?>">
                                    <img src="<?php echo JURI::root() . $params->get('guild_logo', 'components/com_sttnmls/assets/images/kogr2.png') ?>" />
                                </a>
                                <!-- /noindex -->
                            <?php endif; ?>
                            </div>
                            <!-- /Firm logo -->

                            <!-- Firm Info -->
                            <div class="col-md-10">
                                <div class="h4">
                                    <a href="<?php echo $link ?>" target="_blank">
                                        <?php echo $name ?>
                                        <?php if(SttNmlsHelper::chkUserEditFirm($item->ID)) : ?>
                                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&layout=edit&agency=' . $item->ID) ?>" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-edit"></span>
                                                <?php echo JText::_('COM_STTNMLS_FIRM_LINK_EDIT') ?>
                                            </a>
                                        <?php endif; ?>
                                    </a>
                                    <br />
                                    <small>
                                        <strong style="color: #777;"><?php echo $this->model->getCategories($item->ID); ?></strong>
                                    </small>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="glyphicon glyphicon-map-marker"></span> <?php echo (($item->addr == '') ? '-' : $item->addr) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="glyphicon glyphicon-earphone"></span> <?php echo rtrim((($item->PHONE == '') ? '-' : $item->PHONE) . ', ' . $item->ADDPHONE, ', ') ; ?><br />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="glyphicon glyphicon-envelope"></span> <?php if($item->EMAIL == '') { echo '-'; } else { ?><a href="mailto:<?php echo $item->EMAIL ?>" ><?php echo $item->EMAIL; ?></a><?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                    <?php if($item->ISITE != '') : ?>
                                        <span class="glyphicon glyphicon-globe"></span>
                                        <!-- noindex -->
                                        <a target="_blank" href="<?php echo 'http://' . str_replace('http://', '', $item->ISITE); ?>" rel="nofollow">
                                            <?php echo $item->ISITE; ?>
                                        </a>
                                        <!-- /noindex -->
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="glyphicon glyphicon-user"></span>
                                        <?php echo JText::_('COM_STTNMLS_FIRMS_RUK') ?>:
                                        <?php
                                            $bosses = $this->model->getBosses($item->ID);
                                            echo (($bosses != '') ? $bosses : '-');
                                        ?>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="glyphicon glyphicon-tag"></span>
                                        <?php echo JText::_('COM_STTNMLS_FIRMS_SOTR'); ?>: <?php echo $item->cnt; ?>
                                    </div>
                                </div>

                            </div>
                            <!-- /Firm Info -->
                            <div class="clearfix"></div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                <?php else : ?>


                <?php endif; ?>
                    <div class="pagination">
                        <?php echo str_replace('document.adminForm.limitstart.value','document.getElementById(\'limitstart_t\').value',$this->pagination->getListFooter()); ?>
                    </div>
                </div>
                <!-- /ROW #3.2: Firms list -->
            </div>
        </form>
    </div>
    <!-- /ROW #3: Firms list -->
</div>

