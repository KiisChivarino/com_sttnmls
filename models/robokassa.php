<?php

defined('_JEXEC') or die('Restricted access');

class SttNmlsModelRobokassa extends JModelLegacy {

	function __construct() {
		parent::__construct();
	}
	
	function getInvid() {
		$user = JFactory::getUser();
		$userid = $user->get('id');
		if(!$userid) return 0;
		$db = $this->getDbo();
		$db->setQuery('delete from #__sttnmlsbalanslog where summa=0 and status=0 and dateinp<date_sub(now(), interval 1 day)');
		$db->execute();
		$db->setQuery('insert into #__sttnmlsbalanslog (userid, dateinp) values ('.$db->quote($userid).',now())');
		$db->execute();
		return $db->insertid();
	}
}

