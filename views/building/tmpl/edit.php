<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$editor = JFactory::getEditor();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}


// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload-ui.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/bootstrap-select.min.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/ajax-bootstrap-select.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$script .= <<<JS
    var viewname = "apart";
    var cardnum="{$this->item->CARDNUM}";
    var compid="{$this->item->COMPID}";

    jQuery(document).ready(function($){
        document.formvalidator.setHandler('streetid', function(value) {
            var streetid = $('#streetid').val() || 0;
            var result = (streetid > 0);

            if(result === false) {
                $('#street_selector').closest('div').addClass('invalid');
            } else {
                $('#street_selector').closest('div').removeClass('invalid');
            }

            return result;
        });

        document.formvalidator.setHandler('sttnum', function(value) {
			value = str_replace(" ","",value);
			var v = value.replace(',', '.');
			if(parseFloat(v) == 0) return false;
			regex = /^-?\d+((\.|\,)\d+)?$/;
			return regex.test(v);
        });
    });

JS;

$doc->addScriptDeclaration($script);
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery-ui.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.iframe-transport.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.fileupload.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.numeric.extensions.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/ajax-bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/edit.js?' . $component_version);

$pics = (trim($this->item->PICTURES) != '') ? explode(';', $this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';', $this->item->PICNAMES) : '';

$this->item->ulica = explode(',', $this->item->ulica);
$active_tab = $app->input->get('tab', '', 'string');

?>

<div class="com_sttnmls sttnmls_profile" style="margin-bottom: 20px;">
    <div class="object-card-edit-wrapper">
    <?php if($this->item->CARDNUM == 0) : ?>
        <h2><?php echo JText::_('COM_STTNMLS_EDITNEWBUILDING'); ?></h2>
    <?php else : ?>
        <?php if($this->item->flag == 2) : ?>
            <h2><?php echo JText::_('COM_STTNMLS_EDITBUILDING'); ?></h2>
        <?php else: ?>
            <h3><?php echo JText::_('COM_STTNMLS_NOEDIT'); ?></h3>
        <?php endif; ?>
    <?php endif; ?>

        <form class="form-horizontal form-validate" action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&task=save') ?>" method="post" autocomplete="off" novalidate="novalidate" enctype="multipart/form-data">
            <?php echo JHTML::_( 'form.token' ); ?>
            <input type="hidden" name="stype" value="0" />
            <input type="hidden" name="jform[cardnum]" value="<?php echo $this->item->CARDNUM; ?>"/>
            <input type="hidden" name="jform[compid]" value="<?php echo $this->item->COMPID; ?>"/>
            <input type="hidden" name="jform[agentid]" value="<?php echo $this->item->AGENTID; ?>"/>
            <input type="hidden" id="f_raionid" value="<?php echo $this->item->RAIONID; ?>"/>
            <input type="hidden" id="f_mraionid" value="<?php echo $this->item->MRAIONID; ?>"/>
            <input type="hidden" id="f_street" value="<?php echo $this->item->ulica[0]; ?>"/>
            <input type="hidden" id="streetid" name="jform[streetid]" class="validate-streetid" value="<?php echo (($this->item->STREETID) ? $this->item->STREETID : 0); ?>"/>


            <div class="well well-sm text-right">
                <?php if($this->item->CARDNUM) : ?>
                    <?php if($this->item->flag == 2) : ?>
                        <input name="save" type="submit" class="btn btn-success validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                        <input name="apply" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
                        <input name="addnew" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
                    <?php endif; ?>
                <?php else : ?>
                    <input name="save" type="submit" class="btn btn-success validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                    <input name="newobj" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
                <?php endif; ?>
                <input name="cancel" type="submit" class="btn btn-danger sbred" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
            </div>

            <ul class="nav nav-tabs">
                <!-- TABEX #1: Main info -->
                <li role="presentation"<?php echo (($active_tab == '' OR !$this->item->CARDNUM) ? ' class="active"' : '') ?>>
                    <a href="#sttnmls-object-info" id="tab-sttnmls-object-info" role="tab" data-toggle="tab">
                        <b><?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INFO') ?></b>
                    </a>
                </li>
                <!-- /TABEX #1: Main info -->

                <?php if($this->item->CARDNUM) : ?>
                    <!-- TABEX #2: Photos -->
                    <li role="presentation"<?php echo (($active_tab == 'photo') ? ' class="active"' : '') ?>>
                        <a href="#sttnmls-photo" id="tab-sttnmls-photo" role="tab" data-toggle="tab">
                            <b><?php echo JText::_('COM_STTNMLS_LABEL_PHOTO') ?></b>
                        </a>
                    </li>
                    <!-- /TABEX #2: Photos -->

                    <!-- TABEX #3: Building plans -->
                    <li role="presentation"<?php echo (($active_tab == 'building_plans') ? ' class="active"' : '') ?>>
                        <a href="#sttnmls-building-plans" id="tab-sttnmls-building-plans" role="tab" data-toggle="tab">
                            <b><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS') ?></b>
                        </a>
                    </li>
                    <!-- /TABEX #3: Building plans -->

                    <!-- TABEX #4: Building progress -->
                    <li role="presentation"<?php echo (($active_tab == 'building_progress') ? ' class="active"' : '') ?>>
                        <a href="#sttnmls-building-progress" id="tab-sttnmls-building-progress" role="tab" data-toggle="tab">
                            <b><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PROGRESS') ?></b>
                        </a>
                    </li>
                    <!-- /TABEX #4: Building progress -->
                <?php endif; ?>
            </ul>

            <!-- TABS Container -->
            <div class="tab-content" id="tab-sttnmls">

                <!-- TAB #1: Home params -->
                <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == '' OR !$this->item->CARDNUM) ? ' active in' : '') ?>" id="sttnmls-object-info" aria-labelledby="tab-sttnmls-object-info">
                    <div class="tab-content-wrapper">
                        <!-- ROW #1 -->
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_HOME') ?></h4>
                                <hr />

                                <!-- VAR #1: Buildings title -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_TITLE') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="title" name="jform[title]" class="form-control required" value='<?php echo str_replace("'",'"',$this->item->title); ?>' />
                                    </div>
                                </div>
                                <!-- /VAR #1: Buildings title -->

                                <!-- VAR #2: Buildings fulltitle -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_TITLE_FULL') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="fulltitle" name="jform[fulltitle]" class="form-control required" value='<?php echo str_replace("'",'"',$this->item->fulltitle) ?>' />
                                    </div>
                                </div>
                                <!-- /VAR #2: Buildings fulltitle -->

                                <!-- VAR #3: Building class -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_CLASS') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                                        if($this->buildings_classes_list) {
                                            foreach($this->buildings_classes_list as $item) {
                                                $options[] = JHtml::_('select.option', $item->id, $item->title);
                                            }
                                        }
                                        echo JHTML::_('select.genericlist',  $options, 'jform[class]', ' class="form-control validate-sttnum"', 'value', 'text', $this->item->CLASS);
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #3: Building class -->

                                <!-- VAR #4: Wmater -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_MW') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = array('text' => JText::_('COM_STTNMLS_MW'), 'value' => 0);
                                        if($this->wmater) {
                                            foreach($this->wmater as $item) {
                                                $options[] = JHtml::_('select.option', $item->ID, $item->NAME);
                                            }
                                        }
                                        echo JHTML::_('select.genericlist',  $options, 'jform[wmaterid]', ' class="form-control validate-sttnum"', 'value', 'text', $this->item->WMATERID);
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #4: Wmater -->

                                <!-- VAR #5: Stages -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_TOTAL_STAGES') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <input type="text" id="hstage" name="jform[hstage]" class="form-control validate-sttnum required" value="<?php echo $this->item->HSTAGE ?>" />
                                    </div>
                                </div>
                                <!-- /VAR #5: Stages -->

                                <!-- VAR #6: Building date -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_DATE') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', 0, 'Квартал');
                                        $options[] = JHtml::_('select.option', 1, 'I');
                                        $options[] = JHtml::_('select.option', 2, 'II');
                                        $options[] = JHtml::_('select.option', 3, 'III');
                                        $options[] = JHtml::_('select.option', 4, 'IV');

                                        echo JHtml::_('select.genericlist', $options, 'jform[datekvar]', ' class="form-control validate-sttnum"', 'value', 'text', $this->item->datekvar);
                                    ?>
                                    </div>

                                    <div class="col-md-2 col-sm-3 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', 0, 'Год');

                                        $Y = date('Y') - 2;
                                        for($i = 0; $i < 11; $i++) {
                                            $options[] = JHtml::_('select.option', $Y, $Y);
                                            $Y++;
                                        }
                                        echo JHtml::_('select.genericlist', $options, 'jform[dateyear]', ' id="dateyear" class="form-control validate-sttnum"', 'value', 'text', $this->item->dateyear);
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #6: Building date -->

                                <!-- VAR #7: Buildings project docs -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_PROJECT_DOCS') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <div id="attached_project_file">
                                            <?php echo $this->loadTemplate('project_file_input') ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #7: Buildings project docs -->
                            </div>
                        </div>
                        <!-- /ROW #1 -->

                        <!-- ROW #2 -->
                        <div class="row" style="margin-top:10px;">
                            <div class="col-md-12">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_APARTS') ?></h4>
                                <hr />

                                <!-- VAR #8: Aparts count -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_APARTS') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <input type="text" id="flatcount" name="jform[flatcount]" class="form-control validate-sttnum required" value="<?php echo $this->item->flatcount ?>" />
                                    </div>
                                </div>
                                <!-- /VAR #8: Aparts count -->

                                <!-- VAR #9: Aparts rooms counts -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_APARTS_ROOMS_COUNT') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <?php if($this->buildings_types) : ?>
                                            <?php $this->item->type = (($this->item->type != '') ? json_decode($this->item->type) : array()); ?>
                                            <?php foreach($this->buildings_types as $item) : ?>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="jform[type][]" value="<?php echo $item->id ?>"<?php echo ((in_array($item->id, $this->item->type)) ? ' checked="checked"' : '')?> />
                                                    <?php echo $item->title ?>
                                                </label>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            -
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!-- /VAR #9: Aparts rooms counts -->

                                <!-- VAR #10: Aparts Squear -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVOT') ?></div>
                                            <input type="text" id="areafrom" name="jform[areafrom]" class="form-control validate-sttnum required" value="<?php echo $this->item->areafrom ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVDO') ?></div>
                                            <input type="text" id="areato" name="jform[areato]" class="form-control validate-sttnum required" value="<?php echo $this->item->areato ?>" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #10: Aparts Squear -->

                                <!-- VAR #11: Aparts prices -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PRICE') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVOT') ?></div>
                                            <input type="text" id="pricefrom" name="jform[pricefrom]" class="form-control validate-sttnum required" value="<?php echo $this->item->pricefrom ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVDO') ?></div>
                                            <input type="text" id="priceto" name="jform[priceto]" class="form-control validate-sttnum required" value="<?php echo $this->item->priceto ?>" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #11: Aparts prices -->

                                <!-- VAR #12: Aparts furnish -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_FURNISH') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                                        if($this->buildings_furnish_list) {
                                            foreach($this->buildings_furnish_list as $item) {
                                                $options[] = JHtml::_('select.option', $item->id, $item->title);
                                            }
                                        }
                                        echo JHTML::_('select.genericlist',  $options, 'jform[furnish]', ' class="form-control validate-sttnum"', 'value', 'text', $this->item->furnish);
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #12: Aparts furnish -->
                            </div>
                        </div>
                        <!-- /ROW #2 -->

                        <!-- ROW #3 -->
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_COMMERCIAL_OBJECTS') ?></h4>
                                <hr />

                                <!-- VAR #13: Commercial objects count -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_COMMERCIAL_OBJECTS_COUNT') ?>
                                    </label>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <input type="text" id="comcount" name="jform[comcount]" class="form-control" value="<?php echo $this->item->comcount ?>" />
                                    </div>
                                </div>
                                <!-- /VAR #13: Commercial objects count -->

                                <!-- VAR #14: Commercial Squear -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVOT') ?></div>
                                            <input type="text" id="areafrom" name="jform[comareafrom]" class="form-control" value="<?php echo $this->item->areafrom ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVDO') ?></div>
                                            <input type="text" id="areato" name="jform[comareato]" class="form-control" value="<?php echo $this->item->areato ?>" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #14: Commercial Squear -->

                                <!-- VAR #15: Commercial prices -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PRICE') ?>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVOT') ?></div>
                                            <input type="text" id="pricefrom" name="jform[compricefrom]" class="form-control" value="<?php echo $this->item->compricefrom ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><?php echo JText::_('COM_STTNMLS_NOTVDO') ?></div>
                                            <input type="text" id="priceto" name="jform[compriceto]" class="form-control" value="<?php echo $this->item->compriceto ?>" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #15: Commercial prices -->
                            </div>
                        </div>
                        <!-- /ROW #3 -->

                        <!-- ROW #4 -->
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_ADDRESS') ?></h4>
                                <hr />

                                <!-- VAR #16: City -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_CITY') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <select id="cityid" name="jform[cityid]" class="form-control" onchange="changecity();">
                                            <?php echo JHtml::_('select.options', $this->optcity, 'value', 'text', $this->item->CITYID);?>
                                        </select>
                                    </div>
                                </div>
                                <!-- /VAR #16: City -->

                                <!-- VAR #17: Raion -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_RAION') ?>
                                    </label>
                                    <div class="col-sm-6 col-md-7" id="raion"></div>
                                </div>
                                <!-- /VAR #17: Raion -->

                                <!-- VAR #18: MRaion -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_MRAION') ?>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12" id="mraion"></div>
                                </div>
                                <!-- /VAR #18: MRaion -->

                                <!-- VAR #19: Street -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_STREET') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <select id="street_selector" class="selectpicker form-control">
                                            <?php if($this->item->STREETID && $this->item->STREETID != -1) : ?>
                                                <option value="<?php echo $this->item->STREETID ?>" selected="selected"><?php echo $this->item->ulica[0] ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- /VAR #19: Street -->

                                <!-- VAR #20: Building complete -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_COMPLETE') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="sdan" value="1" id="sdan" <?php echo (($this->item->sdan == 1) ? ' checked="checked"' : '') ?>  />
                                                <?php echo JText::_('JYES') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #20: Building complete -->

                                <!-- VAR #21: Building number -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_HOUSE_NUMBER_FUTURE') ?>
                                    </label>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <input type="text" id="haap" class="form-control" name="jform[haap]" value="<?php echo $this->item->HAAP ?>" />
                                    </div>
                                </div>
                                <!-- /VAR #21: Building number -->
                            </div>
                        </div>
                        <!-- /ROW #4 -->

                        <!-- ROW #5 -->
                        <div class="row">
                            <div class="col-md-12">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_ADDITIONAL_INFO') ?></h4>
                                <hr />

                                <!-- VAR #22: Payment types -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PAYMENT') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <?php if($this->payment_types) : ?>
                                            <?php $this->item->pay = (($this->item->pay != '') ? json_decode($this->item->pay) : array()); ?>
                                            <?php foreach($this->payment_types as $item) : ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="jform[pay][]" value="<?php echo $item->id ?>"<?php echo ((in_array($item->id, $this->item->pay)) ? ' checked="checked"' : '')?> />
                                                        <?php echo $item->title ?>
                                                    </label>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            -
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!-- /VAR #22: Payment types -->

                                <!-- VAR #23: Contract -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_CONTRACT') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <div id="attached_dogovor_file">
                                            <?php echo $this->loadTemplate('dogovor_file_input') ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /VAR #23: Contract -->

                                <!-- VAR #24: Parking -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PARKING') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="park" class="form-control" name="jform[park]" value="<?php echo $this->item->park ?>" />
                                    </div>
                                </div>
                                <!-- /VAR #24: Parking -->

                                <!-- VAR #25: Builder firm -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDER') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                    <?php
                                        $options = array();
                                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                                        if($this->builders_list) {
                                            foreach($this->builders_list as $item) {
                                                $options[] = JHtml::_('select.option', $item->ID, $item->realname);
                                            }
                                        }
                                        echo JHTML::_('select.genericlist',  $options, 'jform[comp]', ' class="form-control validate-sttnum"', 'value', 'text', $this->item->comp);
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #25: Builder firm -->

                                <!-- VAR #26: Banks list -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_ACCREDITATION') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                    <?php
                                        $options = array();
                                        if($this->banks_list) {
                                            foreach($this->banks_list as $item) {
                                                $options[] = JHtml::_('select.option', $item->ID, $item->realname);
                                            }
                                        } else {
                                            $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                                        }
                                        echo JHTML::_('select.genericlist',  $options, 'jform[banks][]', ' class="form-control" multiple="multiple"', 'value', 'text', json_decode($this->item->banks));
                                    ?>
                                    </div>
                                </div>
                                <!-- /VAR #26: Banks list -->

                                <!-- VAR #27: Description -->
                                <div class="form-group">
                                    <label class="col-md-5 col-sm-6 col-xs-12 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_DESCRIPTION') ?>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <?php echo $editor->display('jform[misc]', $this->item->MISC, '100%', '300', '60', '20', FALSE ); ?>
                                    </div>
                                </div>
                                <!-- /VAR #27: Description -->
                            </div>
                        </div>
                        <!-- /ROW #5 -->
                    </div>
                </div>
                <!-- /TAB #1: Home params -->

                <?php if($this->item->CARDNUM) : ?>
                    <!-- TAB #2: Photo -->
                    <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == 'photo') ? ' active in' : '') ?>" id="sttnmls-photo" aria-labelledby="tab-sttnmls-photo">
                        <div class="tab-content-wrapper">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <h4><?php echo JText::_('COM_STTNMLS_LABEL_ADD_PHOTO') ?></h4>
                                    <hr />

                                    <div class="sttnmls_photo">
                                        <div class="upload_image">
                                        <span class="btn btn-block btn-success btn-lg fileinput-button">
                                            <span class="glyphicon glyphicon-open"></span>
                                            <span><?php echo JText::_('COM_STTNMLS_LABEL_ADD_PHOTO') ?></span>
                                            <input type="file" id="objectPhoto" name="files[]" data-url="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&task=uploadObjectPhoto&format=raw') ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-compid="<?php echo $this->item->COMPID ?>" data-view="building" multiple />
                                        </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-8">
                                    <h4><?php echo JText::_('COM_STTNMLS_LABEL_SORT_ORDER') ?></h4>
                                    <hr />
                                    <div class="well">
                                        <div id="images-list">
                                            <?php echo $this->loadTemplate('photo_list') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /TAB #2: Photo -->


                    <!-- TAB #3: Building plans -->
                    <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == 'building_plans') ? ' active in' : '') ?>" id="sttnmls-building-plans" aria-labelledby="tab-sttnmls-building-plans">
                        <div class="tab-content-wrapper">

                            <!-- ROW #1 -->
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void();" class="btn btn-sm btn-success addBuildingPlan" data-toggle="modal" data-target="#building_form_container" data-view="building" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->comp ?>">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        <?php echo JText::_('COM_STTNMLS_LABEL_ADD_BUILDING_PLAN') ?>
                                    </a>
                                </div>
                            </div>
                            <!-- ROW #1 -->

                            <!-- ROW #2 -->
                            <div class="row">
                                <div id="bulding_plans_list" class="col-md-12">
                                    <?php echo $this->loadTemplate('building_plans_list') ?>
                                </div>
                            </div>
                            <!-- /ROW #2 -->

                        </div>
                    </div>
                    <!-- /TAB #3: Building plans -->

                    <!-- TAB #4: Building progress -->
                    <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == 'building_progress') ? ' active in' : '') ?>" id="sttnmls-building-progress" aria-labelledby="tab-sttnmls-building-progress">
                        <div class="tab-content-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sttnmls_building_progress_photo">
                                        <div class="upload_image">
                                        <span class="btn btn-success btn-sm fileinput-button">
                                            <span class="glyphicon glyphicon-open"></span>
                                            <span><?php echo JText::_('COM_STTNMLS_LABEL_ADD_PHOTO') ?></span>
                                            <input type="file" id="objectBuildingProgressPhoto" name="bprogress[]" data-url="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&task=json.uploadBuildingProgressPhoto') ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-compid="<?php echo $this->item->comp ?>" data-view="building" multiple />
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-12">
                                    <div id="images-building-progress-list">
                                        <?php echo $this->loadTemplate('building_progress_photo_list') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /TAB #4: Building progress -->


                <?php endif; ?>


            </div>
            <!-- /TABS Container -->

            <div class="well well-sm text-right" style="margin-top:20px;">
                <?php if($this->item->CARDNUM) : ?>
                    <?php if($this->item->flag == 2) : ?>
                        <input name="save" type="submit" class="btn btn-success validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                        <input name="apply" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
                        <input name="addnew" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
                    <?php endif; ?>
                <?php else : ?>
                    <input name="save" type="submit" class="btn btn-success validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                    <input name="newobj" type="submit" class="btn btn-default validate" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
                <?php endif; ?>
                <input name="cancel" type="submit" class="btn btn-danger sbred" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
            </div>

        </form>
        <div id="building_form_container" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="buildingFormTitle"></div>
    </div>
</div>













