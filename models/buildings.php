<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/helpers/class.sttnmlslist.php');

class SttnmlsModelBuildings extends SttnmlsModelList
{
    // Индекс объекта в избранном (1-5)
    protected $favoriteObjectType = 1;
    // Название таблицы объектов в зависимости от отображаемой модели
    protected $objectsTableName = '#__sttnmlsbuildings';


    protected function getDefaultFiltersValue()
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        $filters = array(
            // ОПИСАНИЕ ФИЛЬТРОВ:
            //    [имя_поля_фильтра]    =>  [массив значений]
            //
            //    (!) - обязательное значение фильтра
            //    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
            //
            // ЗНАЧЕНИЯ ФИЛЬТРОВ:
            //      "type"      -   (!) тип значения фильтра
            //      "value"     -   (!) значение фильтра по умолчанию
            //
            //      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
            //
            //      [!] >>>
            //      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
            //                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
            //
            //      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
            //
            //      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
            //                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
            //
            //      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
            //                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
            //                              либо завершенную строку WHERE запроса
            //      <<< [!]
            //

            'rooms'         =>  array('type' => 'array', 'value' => array('null')),
            'mw'            =>  array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'mw', 'valueFromTable' => '#__sttnmlsvocalldata', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s) AND ' . $db->quoteName('RAZDELID') . '=' . $db->quote(8)),
            'comp'          =>  array('type' => 'uint', 'value' => 0, 'targetFilterKey' => 'comp', 'valueFromTable' => '#__sttnmlsvocfirms', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),
            'class'         =>  array('type' => 'uint', 'value' => 0, 'targetFilterKey' => 'class', 'valueFromTable' => '#__sttnmlsclass', 'valueField' => $db->quoteName('title'), 'where' => $db->quoteName('id') . '=' . $db->quote('%s')),
            'furnish'       =>  array('type' => 'uint', 'value' => 0, 'targetFilterKey' => 'furnish', 'valueFromTable' => '#__sttnmlsfurnish', 'valueField' => $db->quoteName('title'), 'where' => $db->quoteName('id') . '=' . $db->quote('%s')),
            'dateyear'      =>  array('type' => 'uint', 'value' => 0),
            'datekvar'      =>  array('type' => 'uint', 'value' => 0),
            'hide_complete' =>  array('type' => 'uint', 'value' => 1)
        );
        return array_merge(parent::getDefaultFiltersValue(), $filters);
    }

    /**
     * Возвращает список организаций из категории застройщики
     *
     * @return  bool|object
     */
    public function getBuilderFirmsList()
    {
        $result = FALSE;

        // JOOMLA Instances
        $params = JComponentHelper::getParams('com_sttnmls');
        $db = JFactory::getDbo();
        $builders_category = $params->get('builders_category', 0);

        if($builders_category) {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('f') . '.*');
            $query->from($db->quoteName('#__sttnmlsvocfirms', 'f'));
            $query->join('LEFT', $db->quoteName('#__sttnmlsfirmcatf', 'fc') . ' ON (' . $db->quoteName('f.ID') . '=' . $db->quoteName('fc.idfirm') . ')');
            $query->where($db->quoteName('fc.idcatf') . '=' . $db->quote($builders_category));
            $query->order($db->quoteName('f.realname'));
            $db->setQuery($query);
            $result = $db->loadObjectList('ID');
        }
        return $result;
    }

    /**
     * Получает список классов строений
     *
     * @return  object
     */
    public function getBuildingsClassesList()
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsclass'));
        $query->order($db->quoteName('position') . ' ASC');
        $db->setQuery($query);

        return $db->loadObjectList('id');
    }

    /**
     * Получает список вариантов отделки строений
     *
     * @return  object
     */
    public function getBuildingsFurnishList()
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsfurnish'));
        $query->order($db->quoteName('position') . ' ASC');
        $db->setQuery($query);

        return $db->loadObjectList('id');
    }

    protected function setPersonalConditions()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

        // State VARS --------------------------------------------------------------------------------------------------
        $filter_rooms = $this->getState('filter.rooms', array());
        $filter_hide_complete = $this->getState('filter.hide_complete', 1);


        // Раздел SELECT запроса ---------------------------------------------------------------------------------------
        $this->query->select($db->quoteName('c.NAME', 'gorod') . ', ' . $db->quoteName('r.NAME', 'raion'));
        $this->query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
        $this->query->select('COUNT(' . $db->quoteName('ap.cardnum') . ') AS ' . $db->quoteName('aparts_counts'));

        // Раздел JOIN запроса -----------------------------------------------------------------------------------------
        $this->query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.builder') . '=' . $db->quoteName('o.comp') . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quoteName('o.CARDNUM') . ')');
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsaparts', 'ap') . ' ON(' . $db->quoteName('ap.COMPID') . '=' . $db->quoteName('barel.aparts_compid') . ' AND ' . $db->quoteName('ap.CARDNUM') . '=' . $db->quoteName('barel.aparts_cardnum')  . ' AND ' . $db->quoteName('ap.close') . '=' . $db->quote(0) . ' AND ' . $db->quoteName('ap.flag') . '<3 AND ((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('ap.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('ap.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . $params->get('days', 30) . ' day)))' . ')');

        // Раздел WHERE запроса ----------------------------------------------------------------------------------------
        $this->query->where($db->quoteName('o.WHAT') . (($this->what) ? '>' : '=') . $db->quote(0));


        // Учет запрошенного кол-ва комнат в объектах ------------------------------------------------------------------
        if($filter_rooms && count($filter_rooms) > 1) {
            //> Удаление нулевого значения из массива комнат
            foreach($filter_rooms as $k => $v) {
                if($v == 'null') {
                    unset($filter_rooms[$k]);
                }
            }

            $t = '';
            foreach($filter_rooms as $k => $v) {
                $t .= $db->quoteName('o.type') . ' LIKE ' . $db->quote('%' . $v . '%') . ' OR ';
            }
            if($t != '') {
                $this->query->where('(' . rtrim($t, ' OR ') . ')');
            }
        }
        // END Учет запрошенного кол-ва комнат в объектах --------------------------------------------------------------

        // Учет параметра скрыть сданные -------------------------------------------------------------------------------
        if($filter_hide_complete) {
            $this->query->having(
                '(' .
                    $db->quoteName('o.sdan') . '=' . $db->quote(0) . ' OR ' .
                    '(' .
                        $db->quoteName('o.sdan') . '=' . $db->quote(1) . ' AND ' .
                        $db->quoteName('aparts_counts') . '>' . $db->quote(0) .
                    ')' .
                ')'
            );
        }
        // END Учет параметра скрыть сданные ---------------------------------------------------------------------------


        $this->setAdditionalWhereFromState($this->query, 'dateyear', 'o.dateyear', '<=');
        $this->setAdditionalWhereFromState($this->query, 'datekvar', 'o.datekvar', '<=');
        $this->setAdditionalWhereFromState($this->query, 'comp', 'o.comp');
        $this->setAdditionalWhereFromState($this->query, 'furnish', 'o.furnish');
        $this->setAdditionalWhereFromState($this->query, 'class', 'o.class');

        // Учет запрошенного материала стен ----------------------------------------------------------------------------
        $this->setAdditionalWhereFromStateCheckbox($this->query, 'mw', 'o.WMATERID');
        // END Учет запрошенного материала стен ------------------------------------------------------------------------

        $this->query->group($db->quoteName('o.comp') . ', ' . $db->quoteName('o.cardnum'));
    }
}
