<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/helpers/class.sttnmlslist.php');

class SttnmlsModelGarages extends SttnmlsModelList
{
	// Индекс объекта в избранном (1-5)
	protected $favoriteObjectType = 5;
	// Название таблицы объектов в зависимости от отображаемой модели
	protected $objectsTableName = '#__sttnmlsgarages';

	protected function getDefaultFiltersValue()
	{
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$filters = array(
			// ОПИСАНИЕ ФИЛЬТРОВ:
			//    [имя_поля_фильтра]    =>  [массив значений]
			//
			//    (!) - обязательное значение фильтра
			//    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
			//
			// ЗНАЧЕНИЯ ФИЛЬТРОВ:
			//      "type"      -   (!) тип значения фильтра
			//      "value"     -   (!) значение фильтра по умолчанию
			//
			//      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
			//
			//      [!] >>>
			//      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
			//                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
			//
			//      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
			//
			//      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
			//                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
			//
			//      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
			//                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
			//                              либо завершенную строку WHERE запроса
			//      <<< [!]
			//
			'gtypes'       	=>  array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'gtypes', 'valueFromTable' => '#__sttnmlsvocalldata', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s) AND ' . $db->quoteName('RAZDELID') . '=' . $db->quote(15)),

		);
		return array_merge(parent::getDefaultFiltersValue(), $filters);
	}

	protected function setPersonalConditions()
	{
		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		// Request VARS ------------------------------------------------------------------------------------------------
		$act = $app->input->getString('act', '');

		// Раздел SELECT запроса ---------------------------------------------------------------------------------------
		$this->query->select($db->quoteName('c.NAME', 'gorod') . ', ' . $db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d.NAME', 'tip') . ', ' . $db->quoteName('d.SHORTNAME', 'shorttip'));
		$this->query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));

		// Раздел JOIN запроса -----------------------------------------------------------------------------------------
		$this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd') . ' ON (' . $db->quoteName('o.GTYPEID') . '=' . $db->quoteName('d.id') . ')');


		// Раздел WHERE запроса ----------------------------------------------------------------------------------------

		// Учет запрошенного типа гаража -------------------------------------------------------------------------------
		$this->setAdditionalWhereFromStateCheckbox($this->query, 'gtypes', 'o.GTYPEID');
		// END Учет запрошенного типа гаража ---------------------------------------------------------------------------

		if($act == 'pdf') {
			$this->query->order($db->quoteName('o.CITYID') . ' ASC, ' . $db->quoteName('o.RAIONID') . ' ASC, ' . $db->quoteName('o.PRICE'));
		}
	}


	public function getGtypes()
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(TRUE);
        $query->select($db->quoteName('ID', 'value') . ', ' . $db->quoteName('NAME', 'text'));
		$query->from($db->quoteName('#__sttnmlsvocalldata'));
		$query->where($db->quoteName('RAZDELID') . '=' . $db->quote(15));
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
