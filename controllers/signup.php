<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/controller.php');
require_once(JPATH_COMPONENT . '/helpers/sttnmlsvalidate.php');

class SttnmlsControllerSignup extends SttNmlsController
{
    public function activate()
    {
        $app = JFactory::getApplication();
        $model = $this->getModel('Signup', 'SttnmlsModel');
        JFactory::getLanguage()->load('com_users', JPATH_BASE);
        
        // Защита повторной отправки формы
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        
        // Если пользователь вошел, то ошибка
        if(JFactory::getUser()->id) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_LOGGED_IN'), 'error');
            $app->redirect('index.php');
            return FALSE;
        }
        
        // Получение данных формы
        $data = $this->input->post->get('jform', array(), 'array');
        if(!$data) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'));
            $app->redirect('index.php');
            return FALSE;
        }
        
        // Получение проверенного токена активации
        $token = $model->getToken();
        
        // Выполнение активации пользователяы
        $activated = $model->activate($token);
        if($activated === FALSE) {
            $app->enqueueMessage(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError()));
            $app->redirect('index.php');
            return FALSE;
        }
        // ID нового пользователя
        $user_id = $activated->id;
               
        // Сохранение информации о пользователе в таблицах компонента
        $model->save($user_id, $data);
    }
    
    public function register()
    {
        $app = JFactory::getApplication();
        $params = JComponentHelper::getParams('com_sttnmls');
        $plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
        $plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
        $plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

        // Защита повторной отправки формы
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Проверка капчи, если включена
        if ($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
            // Подключение плагина капчи
            JPluginHelper::importPlugin('captcha');

            // Подключение диспетчера
            $dispatcher = JEventDispatcher::getInstance();

            $post = JFactory::getApplication()->input->post;
            $chk_result = $dispatcher->trigger('onCheckAnswer', $post->get('recaptcha_response_field'));
            if(!$chk_result[0]){
                JError::raiseWarning(1001, JText::_('COM_STTNMLS_ANTISPAMBAD'));
                return;
            }
        }

        // Если пользователь вошел, то ошибка
        if(JFactory::getUser()->id) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_LOGGED_IN'), 'error');
            $app->redirect('index.php');
            return FALSE;
        }

        // Подключение модели компонента регистрации
        jimport('joomla.application.component.model');
        JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_users/models');
        JForm::addFormPath(JPATH_SITE . '/components/com_sttnmls/models/forms');
        JFactory::getLanguage()->load('com_users', JPATH_BASE);
        $model = $this->getModel('Signup', 'SttnmlsModel');

        // Модернизация данных формы в зависимости от режима
        $requestData = $this->input->post->get('jform', array(), 'array');
        $requestData['email2'] = $requestData['email1'];
        $requestData['name'] = $requestData['email1'];
        $requestData['username'] = $requestData['email1'];
        
        if(!isset($requestData['profile']) OR !isset($requestData['profile']['realtor']) OR $requestData['profile']['realtor'] == 0) {
            $pwd = JUserHelper::genRandomPassword();
            $requestData['password1'] = $pwd;
            $requestData['password2'] = $pwd;
        }
        $this->input->post->set('jform', $requestData, 'array');
        
        // Получение списка полей формы
        $form = $model->getForm();

        if(!$form) {
            $app->enqueueMessage($model->getError());
            $app->redirect('index.php');
            return FALSE;
        }

        // Валидация формы регистрации
        $data = $model->validate($form, $requestData);

        if($data === FALSE) {
            $errors = $model->getErrors();
        
            for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
                if ($errors[$i] instanceof Exception) {
                    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
                } else {
                    $app->enqueueMessage($errors[$i], 'warning');
                }
            }
            
            $app->setUserState('com_sttnmls.registration.data', $requestData);

            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=signup', FALSE));
            
            return FALSE;
        }
        $data['profile']['realtor'] = ((isset($requestData['profile']) && isset($requestData['profile']['realtor']) && SttnmlsValidateHelper::is_natural($requestData['profile']['realtor'])) ? (int)$requestData['profile']['realtor'] : 0);

        // Результат регистрации пользователя
        $result = $model->register($data);
                
        if ($result === FALSE)
        {
            $app->setUserState('com_sttnmls.registration.data', $data);

            $this->setMessage($model->getError(), 'warning');
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=signup', FALSE));

            return FALSE;
        }

        // Flush the data from the session.
        $app->setUserState('com_sttnmls.registration.data', NULL);

        // Redirect to the profile screen.
        if ($result === 'adminactivate') {
            $app->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_VERIFY'));
            $app->redirect();
        } elseif ($result === 'useractivate') {
            $app->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE'));
            $app->redirect();
        } else {
            $this->setMessage(JText::_('COM_USERS_REGISTRATION_SAVE_SUCCESS'));
            $this->setRedirect(JRoute::_('index.php?option=com_users&view=login', FALSE));
        }
        
        return TRUE;
    }
    
    
}