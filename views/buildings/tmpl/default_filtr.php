<?php

// no direct access
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.core.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.widget.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.position.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.autocomplete.js');

$filter_rooms = $this->state->get('filter.rooms', array());
$filter_my_checkMW = $this->state->get('filter.my_checkMW', array());
$filter_my_check = $this->state->get('filter.my_check', array());
$filter_my_MRcheck = $this->state->get('filter.my_MRcheck', array());
$filter_area_ot = $this->state->get('filter.area_ot', 0);
$filter_area_do = $this->state->get('filter.area_do', 0);
$filter_kitch_ot = $this->state->get('filter.kitch_ot', 0);
$filter_kitch_do = $this->state->get('filter.kitch_do', 0);
$filter_price_ot = $this->state->get('filter.price_ot', 0);
$filter_price_do = $this->state->get('filter.price_do', 0); 



$filter_comp=$this->state->get('filter.comp', 0);
$filter_class=$this->state->get('filter.class', 0);
$filter_wmaterid=$this->state->get('filter.wmaterid', 0);
$filter_datekvar=$this->state->get('filter.datekvar', array());
$filter_dateyear=$this->state->get('filter.dateyear', array());
$filter_furnish=$this->state->get('filter.furnish', 0);






$city = 0;
$link = JRoute::_('index.php?option=com_sttnmls&view=aparts', false);
if ($this->stype) $class='stype1';
else $class='stype0';
?>


<input type="hidden" name="filter_my_checkMW[]" value="null"/>
<input type="hidden" name="filter_rooms[]" value="null"/>
<input type="hidden" name="filter_my_check[]" value="null"/>
<input type="hidden" name="filter_my_MRcheck[]" value="null"/>
<div class="<?php echo $class; ?>"><img src="/components/com_sttnmls/assets/images/home-share.png" style="margin:0px" alt="Продажа" />
	Продажа
</div>

<div style="float:right;"></div>
<div class="realty_filter" style="clear:both">
	<table><input type="hidden" id="city_field" name="city" value="<?php echo $city; ?>" />
	<input type="hidden" name="stype" value="<?php echo $this->stype; ?>" />
    <tr>
    	<td valign="top" style="width:10px; padding:0px 10px;">
        
				<select name="filter_city" id="filter_city" style="width:190px; margin-bottom:5px;" class="inputbox" onchange="changecity(this);jQuery('input#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');">
					<?php echo JHtml::_('select.options', $this->cityopt, 'value', 'text', $this->state->get('filter.city'));?>
				</select>
			
           
            
            
			<?php
				$street = $this->state->get('filter.street');
				if(!$street) $street = JText::_('COM_STTNMLS_STREET_SELECT');
			?><div id="filter_street" style="margin-left:-190px; margin-top:10px;"></div>
			<div class="ui-widget" style="margin:10px 0px;">
            
				<input class="ui-widget-content" type="text" name="filter_street" id="T4" style="width:180px;" value="<?php echo $street; ?>" onblur="if(this.value.replace(/^\s+|\s+$/g, '')=='') this.value='<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>';" onfocus="if(this.value=='<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>') this.value='';" size="30"/>
			</div>
			<button class="butclr" onclick="jQuery('#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');return false;"></button>
        </td>
        <td valign="top" style="width:10px; padding:0px 10px; white-space:nowrap;">
        	<div id="htypes" style="display: none;"></div>
        	<strong><?php echo JText::_('COM_STTNMLS_ROOMS'); ?></strong><br/>
			<input type="checkbox" name="filter_rooms[]" value="1"<?php if(array_search(1, $filter_rooms)!==false) echo ' checked="checked"'; ?> > 1<br>
            <input type="checkbox" name="filter_rooms[]" value="2"<?php if(array_search(2, $filter_rooms)!==false) echo ' checked="checked"'; ?> > 2<br>
            <input type="checkbox" name="filter_rooms[]" value="3"<?php if(array_search(3, $filter_rooms)!==false) echo ' checked="checked"'; ?> > 3<br>
            <input type="checkbox" name="filter_rooms[]" value="4"<?php if(array_search(4, $filter_rooms)!==false) echo ' checked="checked"'; ?> > 4+
        </td>
        <td>
        	<div style="padding-bottom:10px; padding-left:3px;"><strong style="margin-bottom:4px; display:inline-block;">Площадь квартиры</strong><br />
            <?php
			$params = JComponentHelper::getParams('com_sttnmls');
			?>
				От: <select name="filter_area_ot" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
					<?php 
					
						$sq=explode(",",$params->get('sq_aparts_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
							echo '>'.$i.'';
						}
					?>
				</select>
				&nbsp;До: 
				<select name="filter_area_do" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
					<?php 
						$sq=explode(",",$params->get('sq_aparts_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
							echo '>'.$i.'';
						}
					?>
			</select> м<sup>2</sup>
            </div>
            <div style="padding-bottom:0px; padding-left:3px;"><strong style="margin-bottom:4px; display:inline-block;">Стимость квартиры</strong><br />
            От: 
            <select name="filter_price_ot" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
				<?php
				$prices='5000,8000,10000,12000,15000,17000,20000,25000,30000,35000,40000,50000,60000';
				$array_price = explode(",",$prices);
				foreach($array_price as $one_price)
				{
					echo'<option value="'.$one_price.'" ';
					if($filter_price_ot == $one_price && $filter_price_ot != 0) echo 'selected';
					echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
				} ?>
				</select>
				До: <select name="filter_price_do" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
				<?php
                foreach($array_price as $one_price)
                {
                    echo'<option value="'.$one_price.'" ';
                    if($filter_price_do == $one_price && $filter_price_do != 0) echo 'selected';
                    echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                } ?>
                </select> за м<sup>2</sup>
            </div>
        </td>
        <td width="120" style=" text-align: center;"><br>
			<input name="UPD" type="submit" class="button red"  value="<?php echo JText::_('COM_STTNMLS_SEARCH'); ?>"/><br><br>
			
            <?php
			global $resetall_link;
			?>
            <input name="RST" class="button silver" onclick="document.location='<?php echo $resetall_link;?>'; return false;" type="submit" value="<?php echo JText::_('COM_STTNMLS_RESET'); ?>"/>
            
            
            
            
		</td>
    </tr>
    </table>

        
        
		<?php
			$d1='';
			$d2=' style="display: none;"';
			
				$d2='';
				$d1=' style="display: none;"';

		?>
		<div class="updopfltr" <?php echo $d2; ?> >Скрыть дополнительные параметры</div>
		<div class="dopfilter" <?php echo $d2; ?> >
            <div style="float:left; width:210px;">
             <strong><?php echo JText::_('COM_STTNMLS_OKRUG'); ?></strong><br/>
			<div id="raion" style="height:40px;">
				<?php
				foreach($this->raions as $raion) { ?>
					<input type="checkbox" class="r_check" name="filter_my_check[]" value="<?php echo $raion->value; ?>" <?php if(array_search($raion->value, $filter_my_check)!==false) echo ' checked="checked" '; ?> onclick="clickraion();"/>
					<?php echo $raion->text; ?>
					<br/>
				<?php } ?>
			</div>
            <strong><?php echo JText::_('COM_STTNMLS_MRAIONS'); ?></strong><br/>
				<div id="mraion" style="height:50px;">
					<?php
					foreach($this->mraions as $mraion) { ?>
						<input type="checkbox" class="mr_check" name="filter_my_MRcheck[]" value="<?php echo $mraion->value; ?>" <?php if(array_search($mraion->value, $filter_my_MRcheck)!==false) echo ' checked="checked" '; ?>/>
						<?php echo $mraion->text; ?>
						<br/>
					<?php } ?>
				</div>
            </div>
            
            
            <div style="float:left; width:210px;">
                <div style="padding-bottom:10px;">
                    <strong>Застройщик</strong><br/>
                    <?php
                        $db = JFactory::getDbo();
                        $query = $db->getQuery(true);
                        $query->select('realname as text, ID as value');
                        $query->from(' `#__sttnmlsvocfirms` ');
                        $query->where(' ID IN (SELECT idfirm FROM j25_sttnmlsfirmcatf WHERE idcatf=15)');
                        $query->order('NAME');
                        $db->setQuery($query);
                        $res=$db->loadAssocList();
                        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
                        if(is_array($res)) {
                            foreach($res as $row)
                            {
                                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
                            } 
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_comp', 'size="1" style="width:180px;"', 'value', 'text', $filter_comp);
                    ?>
                </div>
                
                <div style="padding-bottom:10px;">
                    <strong>Отделка</strong><br/>
                    <?php
                        $db = JFactory::getDbo();
                        $query = $db->getQuery(true);
                        $query->select('title as text, id as value');
                        $query->from(' `#__sttnmlsfurnish` ');
                        $query->order('position');
                        $db->setQuery($query);
                        $res=$db->loadAssocList();
                        $options=Array();
                        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
                        if(is_array($res)) {
                            foreach($res as $row)
                            {
                                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
                            } 
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_furnish', 'size="1" style="width:180px;"', 'value', 'text', $filter_furnish);
                    ?>
                </div>
                <div style="padding-bottom:10px;">
                    <strong>Дата сдачи до:</strong><br/>
                    <select name="filter_datekvar" id="datekvar" style="width:75px;">
                        <option value="0">Квартал</option>
                        <option value="1" <?php if(array_search(1, $filter_datekvar)!==false){ ?> selected="selected"<?php } ?>>I</option>
                        <option value="2" <?php if(array_search(2, $filter_datekvar)!==false){ ?> selected="selected"<?php } ?>>II</option>
                        <option value="3" <?php if(array_search(3, $filter_datekvar)!==false){ ?> selected="selected"<?php } ?>>III</option>
                        <option value="4" <?php if(array_search(4, $filter_datekvar)!==false){ ?> selected="selected"<?php } ?>>IV</option>
                    </select>
                    <select name="filter_dateyear" id="dateyear" class="validate-sttnum" style="width:75px;">
                        <option value="0">Год</option>
                        <?php
                            $y=date("Y");
                            for($a=0;$a<11;$a++)
                            {
                                $sel='';
                                if(array_search($y+$a, $filter_dateyear)!==false)
                                {
                                    $sel='selected="selected"';	
                                }
                                ?>
                                <option value="<?php echo $y+$a;?>" <?php echo $sel;?>><?php echo $y+$a;?></option>
                                <?php	
                            }
                        ?>
                    </select>
                </div>
            </div>
            
            <div class="linediv" style="width:210px; float:left;">
                <div style="padding-bottom:10px;">

                </div>
                <div style="padding-bottom:10px;">
                    <strong>Класс жилья</strong><br/>
                    <?php
                        $db = JFactory::getDbo();
                        $query = $db->getQuery(true);
                        $query->select('title as text, id as value');
                        $query->from(' `#__sttnmlsclass` ');
                        $query->order('position');
                        $db->setQuery($query);
                        $res=$db->loadAssocList();
                        $options=Array();
                        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
                        if(is_array($res)) {
                            foreach($res as $row)
                            {
                                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
                            } 
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_class', 'size="1" style="width:180px;"', 'value', 'text', $filter_class);
                    ?>
                </div>
            </div>

			
		</div>
		<div class="dndopfltr" <?php echo $d1; ?> >Показать дополнительные параметры</div>
	</div>
