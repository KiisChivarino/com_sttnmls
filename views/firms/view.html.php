<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewFirms extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->items = $this->get('Items');
        $this->title = $this->get('Title');
        $this->state = $this->get('State');
        $this->pagination = $this->get('Pagination');
        $this->model = $this->getModel();
        $session = JFactory::getSession();
        $uri = JFactory::getURI ();
        $url = $uri->toString(array('scheme', 'host', 'port', 'path', 'query', 'fragment')); 
        $session->set('firm.returnurl', $url);
        parent::display($tpl);
    }
}
