<?php defined('_JEXEC') or die('Restricted access');

//
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();

// Load JCROP jquery plugin
$this->doc->addStyleSheet(JUri::root() . 'components/com_sttnmls/assets/css/jquery.jcrop.css');
$this->doc->addScript(JUri::root() . 'components/com_sttnmls/assets/js/jquery.jcrop.min.js');

?>

<h3>
    <?php echo $this->item->SIRNAME . ' ' . $this->item->NAME . ' ' . $this->item->SECNAME ?>:
    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&opentab=tab-commercial-services', 'profile') ?>"><?php echo $this->item->profilename ?></a>
</h3>
<div class="row">
    <div class="col-md-3">
        <div class="sttnmls_photo">
            <div class="image">
                <img class="img-thumbnail" alt="<?php echo $this->item->SIRNAME . ' ' . $this->item->NAME . ' ' . $this->item->SECNAME ?>" src="<?php echo $this->avatar; ?>?t=<?php echo time() ?>" style="width:160px;" />
            </div>
            <div class="upload_image">
                <span class="btn btn-block btn-success btn-sm fileinput-button">
                    <span class="glyphicon glyphicon-open"></span>
                    <span><?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE_PHOTO') ?></span>
                    <input
                        type="file"
                        id="agentphoto"
                        name="files[]"
                        data-url="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&task=json.uploadAvatarImage', 'profile') ?>"
                    />
                </span>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="row">
            <?php
                $limitobj = SttNmlsHelper::checkLimit();
                //$onclick = (($limitobj == 0) ? ' onclick="alert(\''.JText::_('COM_STTNMLS_ERROR_LIMIT').'\');return false;"' : '');
                $link_aparts = SttNmlsHelper::getSEFUrl('com_sttnmls', 'apart', '&view=apart&task=edit&cn=0&cid=' . $this->item->COMPID);
                $link_houses = SttNmlsHelper::getSEFUrl('com_sttnmls', 'house', '&view=house&task=edit&cn=0&cid=' . $this->item->COMPID);
                $link_garages = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garage', '&view=garage&task=edit&cn=0&cid=' . $this->item->COMPID);
                $link_commerc = SttNmlsHelper::getSEFUrl('com_sttnmls', 'com', '&view=com&task=edit&cn=0&cid=' . $this->item->COMPID);
            ?>  
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <?php echo JText::_('COM_STTNMLS_LABEL_SELL') ?>
                    </div>
                    <div class="panel-body">
                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="aparts_0_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_APARTS'); ?>
                                <span class="badge"><?php echo $this->item->cntkv + $this->item->cntkv_c; ?></span>
                            </a>
                            <a href="<?php echo $link_aparts ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="aparts_0_1" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="aparts" data-stype="0" data-what="1" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_ROOMS'); ?>
                                <span class="badge"><?php echo $this->item->cntkm  +$this->item->cntkm_c; ?></span>
                            </a>
                            <a href="<?php echo $link_aparts ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="houses_0_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_HOUSES'); ?>
                                <span class="badge"><?php echo $this->item->cnths + $this->item->cnths_c-$this->item->cntuch; ?></span>
                            </a>
                            <a href="<?php echo $link_houses . '&what=0' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="houses_0_1" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="houses" data-stype="0" data-what="1" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_LANDS_AREAS'); ?>
                                <span class="badge"><?php echo $this->item->cntuch + $this->item->cntuch_c; ?></span>
                            </a>
                            <a href="<?php echo $link_houses . '&what=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="garages_0_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="garages" data-stype="0" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_GARAGES'); ?>
                                <span class="badge"><?php echo $this->item->cntgr + $this->item->cntgr_c; ?></span>
                            </a>
                            <a href="<?php echo $link_garages ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="coms_0_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="coms" data-stype="0" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_COMMERC'); ?>
                                <span class="badge"><?php echo $this->item->cntcom + $this->item->cntcom_c; ?></span>
                            </a>
                            <a href="<?php echo $link_commerc ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo JText::_('COM_STTNMLS_LABEL_RENT') ?>
                    </div>
                    <div class="panel-body">
                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="aparts_1_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_APARTS'); ?>
                                <span class="badge"><?php echo $this->item->cntkvar + $this->item->cntkvar_c; ?></span>
                            </a>
                            <a href="<?php echo $link_aparts . '&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="aparts_1_1" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="aparts" data-stype="1" data-what="1" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_ROOMS'); ?>
                                <span class="badge"><?php echo $this->item->cntkmar + $this->item->cntkmar_c; ?></span>
                            </a>
                            <a href="<?php echo $link_aparts . '&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="houses_1_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_HOUSES'); ?>
                                <span class="badge"><?php echo $this->item->cnthsar + $this->item->cnthsar_c-$this->item->cntuchar; ?></span>
                            </a>
                            <a href="<?php echo $link_houses . '&what=0&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="houses_1_1" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="houses" data-stype="1" data-what="1" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_LANDS_AREAS'); ?>
                                <span class="badge"><?php echo $this->item->cntuchar + $this->item->cntuchar_c; ?></span>
                            </a>
                            <a href="<?php echo $link_houses . '&what=1&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="garages_1_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="garages" data-stype="1" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_GARAGES'); ?>
                                <span class="badge"><?php echo $this->item->cntgrar + $this->item->cntgrar_c; ?></span>
                            </a>
                            <a href="<?php echo $link_garages . '&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                        <div class="btn-group btn-block" role="group">
                            <a href="#object-list-container-home" id="coms_1_0" class="btn btn-default btn-sm col-sm-9 col-xs-9 getObjectsList" data-container="object-list-container-home" data-table="coms" data-stype="1" data-what="0" data-src-profile="1">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_COMMERC'); ?>
                                <span class="badge"><?php echo $this->item->cntcomar + $this->item->cntcomar_c; ?></span>
                            </a>
                            <a href="<?php echo $link_commerc . '&stype=1' ?>" class="btn btn-success btn-sm col-sm-3 col-xs-3"<?php //echo $onclick ?>>
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_TOTAL') ?>
                <span class="badge"><?php echo JText::sprintf('%d / %d', $this->item->st_cntall, $this->item->limitobj); ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_EXPIRED') ?>
                <span class="badge"><?php echo JText::sprintf('%d', $this->item->st_cntexpired); ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_WATCH_COUNT') ?>
                <span class="badge"><?php echo $this->item->st_cntwatch ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_CLOSED_OBJECTS') ?>
                <span class="badge"><?php echo $this->item->st_cntall_c ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_CLOSED_OBJECTS_WATCH_COUNT') ?>
                <span class="badge"><?php echo $this->item->st_cntwatch_c ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_RCALL_COUNT') ?>
                <span class="badge"><?php echo $this->item->st_cntrequest ?></span>
            </li>

            <li class="list-group-item">
                <?php echo JText::_('COM_STTNMLS_LABEL_COMPLAINTS_COUNT') ?>
                <span class="badge"><?php echo $this->item->st_complaint ?></span>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <?php if($this->item->flag == 3 OR $this->item->flag == 4) : ?>
        <a href="/lichnyj-kabinet.html?act=unblock" class="btn btn-warning btn-block btn-lg">
            <span class="glyphicon glyphicon-check"></span>
            <?php echo JText::_('COM_STTNMLS_LABEL_UNLOCK_ACCOUNT_AND_FREE_AGENT') ?>
        </a>
    <?php endif; ?>

    <?php if($limitobj == 0) : ?>
        <div class="alert alert-warning">
        <?php echo JText::sprintf('COM_STTNMLS_MESSAGE_INFO_OBJECTS_LIMITS', $this->item->limitobj); ?>
        </div>
    <?php endif; ?>

    <?php if($this->item->expired) : ?>
        <div class="well well-lg">
            <div>
                <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_OBJECT_SHOW_DATE_EXPECTED'); ?>:

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="width:25px;"></th>
                            <th style="width:100px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->item->expired as $item) : ?>
                        <tr>
                            <td>
                                <?php echo JText::_('COM_STTNMLS_LABEL_EXPIRED_' . strtoupper($item->table) . '_' . $item->stype . '_' . $item->what) ?>
                            </td>
                            <td class="text-center">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-link btn-xs getObjectsList hasTooltip"
                                    data-container="object-list-container-home"
                                    data-table="<?php echo $item->table ?>"
                                    data-stype="<?php echo $item->stype ?>"
                                    data-what="<?php echo $item->what ?>"
                                    data-expired="1"
                                    title="<?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?>"
                                >
                                    <?php echo $item->count ?>
                                </a>
                            </td>
                            <td class="text-center">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-info btn-xs changeExpiredObjectChangeDate hasTooltip"
                                    data-container="object-list-container-home"
                                    data-table="<?php echo $item->table ?>"
                                    data-stype="<?php echo $item->stype ?>"
                                    data-what="<?php echo $item->what ?>"
                                    data-message="<?php echo JText::sprintf('COM_STTNMLS_MESSAGE_CONFIRM_CHANGE_ALL_EXPIRED_OBJECT_IN_PART', JText::_('COM_STTNMLS_LABEL_EXPIRED_' . strtoupper($item->table) . '_' . $item->stype . '_' . $item->what)) ?>"
                                    title="<?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?>"
                                >
                                    <span class="glyphicon glyphicon-time"></span>
                                    <span class="sr-only"><?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE') ?></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td class="text-right">
                                <a
                                    href="javascript:void(0);"
                                    class="btn btn-primary btn-xs changeExpiredObjectChangeDateAll"
                                    data-container="object-list-container-home"
                                    data-message="<?php echo JText::_('COM_STTNMLS_MESSAGE_CONFIRM_CHANGE_ALL_EXPIRED_OBJECT') ?>"
                                >
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_OBJECT_CHANGE_DATE_ALL') ?>
                                </a>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?> 

        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_ORGANIZATION') ?>:</b>
        <?php if ($this->item->firm) : ?>            
            <a target="_blank" class="btn btn-link btn-xs" href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID) ?>">
                <span class="glyphicon glyphicon-info-sign"></span>
                <?php echo $this->item->firm; ?>
            </a>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-contacts', 'profile') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-th-list"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_CHANGE') ?>
            </a>

            <?php if($this->item->flag == 1) : ?>
            <span class="label label-danger">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                <?php echo JText::_('COM_STTNMLS_MESSAGE_WARNING_NOT_VERIFIED') ?>
            </span>
            <?php endif; ?>

        <?php else : ?>
            <?php echo JText::_('JNO') ?>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm', 'edit') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-plus"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_CREATE') ?>
            </a>
        <?php endif; ?>
        </p>

    <?php if($this->item->boss) : ?>
        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS_ORGANIZATION') ?>:</b>
            <?php 
                $org_info = array();
                if($this->item->fphone != '') { $org_info[] = $this->item->fphone; }
                if($this->item->femail != '') { $org_info[] = $this->item->femail; }
                $org_text = implode(', ', $org_info);
                if($org_text == '') { $org_text = JText::_('COM_STTNMLS_LABEL_NO_INFO'); }
                echo $org_text;
            ?>
            <?php if(SttNmlsHelper::chkUserEditFirm($this->item->COMPID)) : ?>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $this->item->COMPID, 'edit') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-edit"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT'); ?>
            </a>
            <?php endif; ?>
        </p>
    <?php endif; ?>

        <p>
            <b><?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS') ?>:</b>

            <?php
                $agent_info = array();
                if($this->item->PHONE != '') { $agent_info[] = $this->item->PHONE; }
                if($this->item->EMAIL != '') { $agent_info[] = $this->item->EMAIL; }
                $agent_text = implode(', ', $agent_info);
                if($agent_text == '') { $agent_text = JText::_('COM_STTNMLS_LABEL_NO_INFO'); }
                echo $agent_text;
            ?>

            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&opentab=tab-contacts', 'profile') ?>" class="btn btn-primary btn-xs">
                <span class="glyphicon glyphicon-edit"></span>
                <?php echo JText::_('COM_STTNMLS_BUTTON_EDIT'); ?>
            </a>

        </p>

    </div>
</div>

