<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);
$doc->addScriptDeclaration('var ROOT_URI = "' . JURI::root() . '";' . "\n");

?>
<div class="container-fluid com_sttnmls">
    <!-- ROW #1: Filters -->
    <div class="row">
        <div class="col-md-12 ">
            <h1><?php echo SttNmlsLocal::TText('COM_STTNMLS_AGENTS_HEAD'); ?></h1>

            <div class="well well-sm" style="padding-top: 25px;">
                <form action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'agents') ?>" class="form-validate form-horizontal" method="post" name="adminForm" id="adminForm">
                    <input type="hidden" name="filter" value="1"/>
                    <input type="hidden" name="filter_boss" value="0"/>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="filter_compid-lbl" for="filter_compid" class="control-label col-md-4 col-sm-4">
                                <?php echo JText::_('COM_STTNMLS_AGENC'); ?>
                            </label>
                            <div class="col-md-8 col-sm-8">
                                <?php if($this->firm_categories) : ?>
                                <select id="filter_catid" name="filter_catid" class="form-control input-sm">
                                    <?php foreach($this->firm_categories as $category) : ?>
                                        <option value="<?php echo $category->id ?>" <?php echo (($category->id == $this->state->get('filter.catid')) ? ' selected="selected"' : '') ?>><?php echo $category->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-sm-offset-4 col-md-8 col-sm-8">
                                <div id="firms_container">
                                    <?php echo $this->loadTemplate('select_compid');  ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label id="jform_fio-lbl" for="filter_fio" class="control-label col-md-4 col-sm-4">
                                <?php echo JText::_('COM_STTNMLS_FIOSEARCH'); ?>
                            </label>
                            <div class="col-md-8 col-sm-8">
                                <input type="text" class="form-control input-sm" name="filter_fio" id="filter_fio" value="<?php echo $this->state->get('filter.fio');  ?>" placeholder="Например: Иванов Иван"/>
<!--                                <div class="help-block">-->
<!--                                    --><?php //echo JText::_('COM_STTNMLS_FIOSEARCH_DESC'); ?>
<!--                                </div>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="jform_phone-lbl" for="filter_phone" class="control-label col-md-4 col-sm-4">
                                <?php echo JText::_('COM_STTNMLS_PHONESEARCH'); ?>
                            </label>
                            <div class="col-md-8  col-sm-8">
                                <input type="text" name="filter_phone" class="validate-numeric form-control input-sm" id="filter_phone" value="<?php echo $this->state->get('filter.phone');  ?>" placeholder="Например: 555"/>
<!--                                <div class="help-block">-->
<!--                                    --><?php //echo JText::_('COM_STTNMLS_PHONESEARCH_DESC'); ?>
<!--                                </div>-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-sm-8 col-md-offset-4 col-sm-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <?php $bosses = $this->state->get('filter.boss', FALSE); ?>
                                        <input type="checkbox" id="filter_boss" name="filter_boss" value="1"<?php echo (($bosses) ? 'checked="checked"' : '') ?> /> <?php echo JText::_('COM_STTNMLS_RUKS'); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-sm-offset-4 col-md-4 col-sm-4">
                            <button class="btn btn-success btn-block validate">
                                <span class="glyphicon glyphicon-search"></span>
                                <?php echo JText::_('COM_STTNMLS_SEARCH'); ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /ROW #1: Filters -->

    <!-- ROW #2: Agents list -->
    <div class="row">
        <div class="col-md-12">
            <?php if($this->items) : ?>
            <ul class="list-group">
            <?php foreach($this->items as $agent) : ?>
                <?php
                    $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $agent->ID . '&comp_id=' . $agent->COMPID);
                ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-2 col-xs-12 text-center">
                            <a href="<?php echo $link; ?>">
                                <img class="img-rounded" src="<?php echo Avatar::getUserThumbAvatar($agent->userid); ?>" alt="" />
                            </a>
                        </div>
                        <div class="col-md-4 col-xs-10">
                            <a class="h4" href="<?php echo $link ?>">
                                <?php printf('%s %s %s', $agent->SIRNAME, $agent->NAME, $agent->SECNAME) ?>
                            </a>
                        <?php if($agent->boss) : ?>
                            <!-- FLAG: Boss -->
                            <div>
                                <span class="label label-default agent_post<?php printf('%s-%s', $this->item->ID, $agent->kod) ?>">
                                    <?php echo (($agent->boss_title != '') ? $agent->boss_title : JText::_('COM_STTNMLS_LABEL_BOSS'))  ?>
                                </span>
                            </div>
                            <!-- /FLAG: Boss -->
                        <?php endif; ?>

                        <?php if($agent->firm != '') : ?>
                            <div class="h6">
                                <span class="glyphicon glyphicon-tag"></span>
                                <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $agent->COMPID)?>">
                                    <b><?php echo $agent->firm ?></b>
                                </a>
                            </div>
                        <?php endif; ?>
                        </div>
                        <div class="col-md-4 col-md-offset-2 col-xs-12 text-right">
                            <a class="btn btn-success btn-block" href="tel:<?php echo str_replace('["-", "(", ")"]', "", trim($agent->PHONE)); ?>"><?php echo trim($agent->PHONE); ?></a>
                            <?php if(strcasecmp(trim($agent->ADDPHONE), "") != 0): ?>
                                <a class="btn btn-success btn-block" href="tel:<?php echo str_replace('["-", "(", ")"]', "", trim($agent->ADDPHONE)); ?>"><?php echo trim($agent->ADDPHONE); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
            </ul>
            <?php else : ?>
                <?php echo JText::_('COM_STTNMLS_LABEL_NO_MEMBERS') ?>
            <?php endif; ?>
        </div>
    </div>
    <!-- /ROW #2: Agents list -->

    <!-- ROW #3: Pagination -->
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->pagination->getListFooter(); ?>
        </div>
    </div>
    <!-- /ROW #3: Pagination -->
</div>
