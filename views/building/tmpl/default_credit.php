<?php
// no direct access
defined('_JEXEC') or die;
$db = JFactory::getDbo();
$sql = 'select * from #__sttnmlscredit';
$db->setQuery($sql);
$credit = $db->loadObject();
if($credit) {
	$this->creditdoptext=$credit->doptext;
	$n = $credit->srok*12;
	$i = $credit->stavka/12/100;
	$k = $i*pow((1+$i),$n)/(pow((1+$i), $n)-1);
	$price = $this->item->PRICE*(100-$credit->perv)/100;
	$payment = round($k*$price);
?>
	<div class="credit">
		<div class="credit1">
			<?php echo JText::_('COM_STTNMLS_CREDIT_SUM').'<span>'.number_format($payment/1000,3, ' ', ' ').'<sup>*</sup></span>'.JText::_('COM_STTNMLS_CREDIT_MES'); ?><br/>
			<?php echo JText::plural('COM_STTNMLS_CREDIT_SROK',$credit->srok); ?><br/>
			<?php echo JText::plural('COM_STTNMLS_CREDIT_PERV',$credit->perv); ?><br/>
			<?php echo JText::plural('COM_STTNMLS_CREDIT_STAVKA',$credit->stavka); ?><br/>
		</div>
		<div class="creditlogo">
			<?php if($credit->logo) { ?>
				<?php if($credit->url) { ?>
					<a href="<?php echo $credit->url; ?>" target="_blank">
				<?php } ?>
				<img src="<?php echo $credit->logo; ?>" />
				<?php if($credit->url) { ?>
					</a>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
<?php } ?>