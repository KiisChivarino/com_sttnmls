<?php defined('_JEXEC') or die('Restricted access'); ?>

<h3><?php echo JText::_('COM_STTNMLS_LINK_OBJ');?></h3>
<div class="row objects-list-sm">            
<?php foreach ($this->arLink as $o) : ?>    
    <div class="col-md-3 object" id="<?php echo $o->item->COMPID . '_' . $o->item->CARDNUM ?>">
        <div class="thumbnail similar-object">
            <div class="object-photo text-center">
                <a href="<?php echo $o->url ?>" target="_blank">
                    <img class="img-rounded" src="<?php echo $o->linkimg ?>" alt="" />
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $o->url; ?>" class="objectdesc object-link" target="_self" title="">
                <?php if($this->what == 1) : ?>
                    <?php echo $o->item->object ?>
                <?php else : ?>
                    <?php printf('%s %s', $o->item->tip, $o->item->mat) ?>
                <?php endif; ?>
                </a>
            </div>
            <div class="object-description">
                <?php
                    $s = $o->item->gorod;
                    if($o->item->raion != '' && $o->item->RAIONID>1) {
                        $r = str_replace('р-н', '', $o->item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($o->item->mraion != '') {
                        $s .= ', ' . $o->item->mraion;
                    }
                    echo $s;
                ?>
                <?php if($this->what <> 1) { printf('<b>Площадь:</b> %s м<sup>2</sup>', $o->item->AAREA); } ?><br />
                <?php printf('<b>Участок:</b> %s соток', $o->item->EAREA) ?><br />
                <?php printf('<b>Цена:</b> %s руб.', number_format($o->item->PRICE, 0, ' ', ' ')) ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
