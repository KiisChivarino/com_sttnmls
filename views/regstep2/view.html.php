<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewRegstep2 extends JViewLegacy
{
	function display($tpl = null) 
	{
		$user = $this->get('User');
		$model = $this->getModel();
		$this->email = $user->get('email');
		$this->userid = $user->get('id');
		$this->realtor = $this->get('Profile');
		$this->catf = $this->get('Catf');
		if($this->realtor==1) {
			$params = JComponentHelper::getParams('com_sttnmls');
			$cat = $params->get('catfrealtor',0);
			$this->firms = $model->getFirms($cat);
		} else {
			$this->firms = $model->getFirms();
		}
		if($this->realtor==2) {
			$this->firms = str_replace('validate-sttnum required', '', $this->firms);
		}
		$this->token = JRequest::getVar('token', null, 'request', 'alnum');

		parent::display($tpl);
	}
}
