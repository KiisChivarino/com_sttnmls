<?php defined('_JEXEC') or die ('Restricted access');

JLoader::import('joomla.application.component.model');

class SttNmlsViewGarage extends JViewLegacy
{
    function display($tpl = null) 
    {  
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $mGarage = $this->getModel('garage');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');
        
        $cardnum = JRequest::getVar('cn', 0);
        $compid = JRequest::getVar('cid', 0);
        $layout = JRequest::getVar('layout', 'edit_photo_list');
        
        // AJAX data switcher
        switch ($layout) {

            case 'default_object_check_state_button':
                $this->item = $mGarage->getItem();

                // Доступ к изменению состояния проверки объекта
                if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
                    $this->_allow_change_state = TRUE;
                } else {
                    $this->_allow_change_state = FALSE;
                }
                break;

            case 'edit_photo_list':
                    if($cardnum == 0 OR $compid == 0) {
                        JError::raiseError('400', JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'));
                        return FALSE;
                    }  
                    
                    $this->item = $mGarage->getObjectImages('garage', $compid, $cardnum, TRUE);
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                break;
        }
        
        parent::display($tpl);
    }
}