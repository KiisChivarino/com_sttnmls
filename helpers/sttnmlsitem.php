<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT_SITE . '/helpers/sttnmlshelper.php');

jimport( 'joomla.database.table' );

class SttNmlsModelItem extends JModelLegacy {
    
    protected static $VIEWS_TABLE = array(
                    'apart'         =>      '#__sttnmlsaparts',
                    'building'      =>      '#__sttnmlsbuildings',
                    'com'           =>      '#__sttnmlscommerc',
                    'garage'        =>      '#__sttnmlsgarages',
                    'house'         =>      '#__sttnmlshouses'
                );
    
    
    private $_tabname;
    private $_tp; //тип объекта для добавления в избранное
    
    protected $_events = array();
    protected $_errors = array();
    
    public $_item;
    
    
    /**
    * Консолидирует ошибки
    * 
    * @param   string  $error_msg_alias - алиас ошибки из языкового файла
    * @return
    */
    protected function raiseEx()
    {   
        $args = func_get_args();
        $error_msg_alias = array_shift($args);
        $app = JFactory::getApplication();
        
        if(count($args) > 0) {
            $this->_errors[] = vsprintf(JText::_($error_msg_alias), $args);
            $app->enqueueMessage(vsprintf(JText::_($error_msg_alias), $args), 'error');
        } else {
            $this->_errors[] = JText::_($error_msg_alias);
            $app->enqueueMessage(JText::_($error_msg_alias), 'error');
        }
    }
    
    /**
    * Консолидирует события
    * 
    * @param   string  $event_msg_alias - алиас ошибки из языкового файла
    * @return
    */
    protected function raiseEvent()
    {   
        $args = func_get_args();
        $event_msg_alias = array_shift($args);
        $app = JFactory::getApplication();
        
        if(count($args) > 0) {
            $this->_events[] = vsprintf(JText::_($event_msg_alias), $args);
            $app->enqueueMessage(vsprintf(JText::_($event_msg_alias), $args));
        } else {
            $this->_events[] = JText::_($event_msg_alias);
            $app->enqueueMessage(JText::_($event_msg_alias));
        }
    }
    
    public function getEvents() 
    {
        return $this->_events;
    }
    
    public function getErrors() 
    {
        return $this->_errors;
    }
    
      
    public function getItem()
    {
        $compid = JRequest::getInt('cid',0);
        $cardnum = JRequest::getInt('cn',0);
        $task = JRequest::getVar('task', '');
        $_isArchiveMode = JFactory::getApplication()->input->get('archive', 0, 'uint');

        if($_isArchiveMode) {
            $this->_tabname = $this->_tabname . '_archive';
        }

        if($task == 'edit')
        {
            require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
            $limitobj = SttNmlsHelper::checkLimit();
            /*if(!$limitobj && !$cardnum) {
                $app = JFactory::getApplication();
                $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile');
                $app->redirect($redirect, JText::_('COM_STTNMLS_ERROR_LIMIT'));
                return null;				
            }*/

            //здесь должны устанавливаться $compid и AGENTID текущего пользователя
            if(!$cardnum){
                //создание нового объекта
                $this->_item = new stdClass;
                $this->_item->VARIANT = JRequest::getInt('stype',0)+1;
                $this->_item->WHAT = 0;
                $this->_item->CARDNUM = $cardnum;
                return $this->_item;
            }
        }
        if(!$compid OR (!$cardnum && $task != 'edit')) return null;
        $this->getItemC($compid, $cardnum);


        /*Проверка на "свой" объект*/
        $db = $this->getDbo();
        $user = JFactory::getUser();
        $userid = $user->get('id');
        $query2	= 'SELECT * FROM #__sttnmlsvocagents WHERE userid="'.$userid.'"';
        $db->setQuery($query2);
        $myagents = $db->loadObject();
        if($this->_item->close == 1)
        {
            if($this->_item->AGENTID==$myagents->ID)
            {
                $yes = 1;
            }else{
                $yes = 0;
            }
            
            if($yes == 0)
            {
                $kods = explode("||",$this->_item->agents);
                foreach($kods as $k)
                {
                    if($myagents->kod == $k)
                    {
                        $yes=1;	
                    }
                }
            }
            if($myagents->boss == 1 && $this->_item->COMPID == $myagents->COMPID)
            {
                $yes = 1;	
            }
            if($yes == 0)
            {
                return false;
            }
        }
        /*Конец проверки на "свой" объект*/


        return $this->_item;
    }
    
    /**
     * Возвращает информацию по агенту
     * 
     * @var $user_id        int - ID конкретного пользователя
     * @return              bool or object
     */
    public function getAgentInfoByID($user_id = 0) 
    {
        $user_id = ((!$user_id) ? JFactory::getUser()->get('id') : $user_id);
        
        if(!$user_id) {
            $this->errors[] = 'COM_STTNMLS_MESSAGE_ERROR_AGENT_NOT_FOUND';
            return FALSE;
        }
                
        $db = $this->getDbo();
        $db->setQuery('SELECT a.*, f.name AS firmname, f.phone AS firmphone, f.addphone AS firmaddphone, f.email AS firmemail, f.isite AS firmsite FROM #__sttnmlsvocagents AS a LEFT JOIN #__sttnmlsvocfirms AS f ON a.compid=f.id WHERE `userid`=' . $db->quote($user_id));
        
        return $db->loadObject();
    }
    
    /**
     * Получение картинок объекта
     * 
     * @param   string      $view
     * @param   integer     $compid
     * @param   integer     $cardnum
     * @return  object
     */
    public function getObjectImages($view, $compid, $cardnum, $asString = FALSE)
    {
        $result = FALSE;
        
        if($compid && $cardnum && isset(self::$VIEWS_TABLE[$view])) {
            $db = JFactory::getDbo();
            $db->setQuery('SELECT `COMPID`, `PICTURES`, `PICNAMES` FROM ' . $db->quoteName(self::$VIEWS_TABLE[$view]) . ' WHERE `CARDNUM`=' . $db->quote($cardnum) . ' AND `COMPID`=' . $db->quote($compid));
            
            $r = $db->loadObject();
            if($r) {
                if($r->PICTURES != '') {
                    if(!$asString) { 
                        $pics = explode(';', $r->PICTURES);
                        $pic_names = explode(';', $r->PICNAMES);
                        $pic_count = count($pics);

                        if($pic_count > 0) {
                            foreach($pics as $i => $item) {
                                $image = new stdClass();
                                $image->src = $item;
                                $image->title = ((isset($pic_names[$i])) ? $pic_names[$i] : 'Фото');
                                $result[] = $image;
                            }
                        }
                    } else {
                        $result = new stdClass();
                        $result->PICTURES = $r->PICTURES;
                        $result->PICNAMES = $r->PICNAMES;
                    }
                }
            }
        }
        return $result;
    }
    
    /**
     * Загружает фотографии объектов
     * 
     * @return              bool
     */
    public function addPhoto()
    {	
        // Хелпер аплоадера картинок
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmls_image.php');

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cardnum', 0, 'uint');
        $compid = $app->input->get('compid', 0, 'uint');
        $viewName = $app->input->get('view', 'apart', 'string');


        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }


        // Шаг.1. Загрузка изображения(й) на сервер ----------------------------
        $config = array(
            'prefix_name'               =>  'obj_',
            'use_prefix_as_imagename'   =>  FALSE,
            'store_path'                =>  'images/objects/',
            'width'                     =>  640,
            'height'                    =>  480,
            'resize'                    =>  TRUE,
            'types'                     =>  array(2, 3),
            'create_thumb'              =>  FALSE
        );
        $avatar = SttnmlsHelperImage::getInstance();
        $avatar->setConfig($config);
        $result = $avatar->upload();
        
        $this->_errors = $avatar->getErrors();
        if(!$result OR $this->_errors) { return FALSE; }
            
        
        // Шаг.2. Запись информации о фото в базу данных для объекта -----------
        $new_object_images = array();
        foreach($result as $upimage) {
            $image = new stdClass();
            $image->src = $upimage->output_filename;
            $image->name = 'Фото';
            $new_object_images[] = $image;
        }
        
        // Получение картинок объекта ------------------------------------------
        $object_images = $this->getObjectImages($viewName, $compid, $cardnum);
        if($object_images) {
            $object_images = array_merge($object_images, $new_object_images);
        } else {
            $object_images = $new_object_images;
        }

        $pics = array();
        $picnames = array();
        foreach($object_images as $img) {
            $pics[] = $img->src;
            $picnames[] = ((trim($img->title) != '') ? $img->title : 'Фото');
        } 


        // Обновление информации о картинках в БД объекта
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName($this->_tabname));
        $query->set($db->quoteName('PICCOUNT') . '=' . $db->quote(count($pics)));
        $query->set($db->quoteName('PICTURES') . '=' . $db->quote(implode(';', $pics)));
        $query->set($db->quoteName('PICNAMES') . '=' . $db->quote(implode(';', $picnames)));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $db->execute();
        
        return $result;
    }
    
    /**
     * Поворот заданного изображения объекта на 90 градусов
     * 
     * @param   string      $direction - [left|right] Направление поворота
     * @return  boolean
     */
    public function rotateObjectImage()
    {
        // Хелпер аплоадера картинок
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmls_image.php');

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $idx = $app->input->get('idx', -1, 'int');
        $direction = $app->input->get('direction', 'left', 'string');
        $viewName = $app->input->get('view', 'apart', 'string');


        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum OR $idx == -1) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }


        // Получение списка картинок для объекта -------------------------------
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('PICTURES'));
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $db->setQuery($query);
        $res = $db->loadObject();
		
        if(!$res OR $res->PICTURES == '') {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_IMAGES');
            return FALSE;
        }
        
        // Проверка существования в массиве картинок нужной --------------------
        $pics = explode(';', $res->PICTURES);
        
        if(!isset($pics[$idx]) OR $pics[$idx] == '') {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_IMAGES');
            return FALSE;
        }
        $src_image = JPATH_ROOT . '/images/objects/' . $pics[$idx];
                
        // Поворот изображения и сохранение ------------------------------------
        $dst_image = SttnmlsHelperImage::getInstance();
        if(!$dst_image->rotateImage($src_image, (($direction == 'right') ? -90 : 90))) {
            $this->_errors = $dst_image->getErrors();
            return FALSE;
        }
        
        return TRUE;
    }
    
    
    /**
     * Удаляет картинки из объекта
     * 
     * @return  bool
     */
    public function removeObjectImage()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $image_id = $app->input->get('idx', 0, 'uint');
        $viewName = $app->input->get('view', 'apart', 'string');


        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }


        $current_images = $this->getObjectImages($viewName, $compid, $cardnum, TRUE);
        if(!$current_images) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_OBJECT_IMAGES');
            return FALSE;
        }
        
        $pics = explode(';', $current_images->PICTURES);
        $picnames = explode(';', $current_images->PICNAMES);
        
        if(isset($pics[$image_id])) {
            // Попытка удалить файл картинки с сервера -------------------------
            $image_filename = $pics[$image_id];
            try {
                $image_path = JPATH_ROOT . '/images/objects/' . $image_filename;                
                if(file_exists($image_path)) {
                    unlink($image_path);
                }
            } catch (Exception $e) {}
            
            // Удаление картинки из списка для объекта -------------------------
            unset($pics[$image_id]);
            if(isset($picnames[$image_id])) {
                unset($picnames[$image_id]);
            }


            // Обновление информации о картинках в БД объекта
            $query = $db->getQuery(TRUE);
            $query->update($db->quoteName($this->_tabname));
            $query->set($db->quoteName('PICCOUNT') . '=' . $db->quote(count($pics)));
            $query->set($db->quoteName('PICTURES') . '=' . $db->quote(implode(';', $pics)));
            $query->set($db->quoteName('PICNAMES') . '=' . $db->quote(implode(';', $picnames)));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
            $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
            $db->setQuery($query);
            $db->execute();

            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
     * Модерирование объекта агентом
     * 
     * @return  boolean
     */
    public function doObjectModerate()
    {
        $db = JFactory::getDbo();
        $user_id = JFactory::getUser()->id;
        $params = JComponentHelper::getParams('com_sttnmls');
        $halyavagroup = $params->get('halyavagroup', '0');
        $realtority = $params->get('realtority', '100');
        
        $typeobj = JRequest::getInt('typeobj', 1);
        $cardnum = JRequest::getInt('cardnum', 0);
        $compid = JRequest::getInt('compid', 0);
        $viewName = JRequest::getVar('view', 'apart');
        $q1 = JRequest::getInt('q1', 0);
        $q2 = JRequest::getInt('q2', 0);
        
        // Проверка залогиненности пользователя --------------------------------
        if(!$user_id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }
        
        // Проверка переданных параметров формы модерирования ------------------
        if($q1 == 0 && $q2 == 0) {
            $this->raiseEx('COM_STTNMLS_ERROR_USER_Q1Q2');
            return FALSE;
        }
                
        // Проверка прав пользователя ------------------------------------------
        $db->setQuery('SELECT a.kod, a.activity, g.seecont FROM `#__sttnmlsvocagents` AS a LEFT JOIN `#__sttnmlsgroup` AS g ON g.id=a.grp WHERE a.userid=' . $db->quote($user_id));
        $agent = $db->loadObject();
        
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_NOT_FOUND');
            return FALSE;
        }
        
        if($agent->seecont == 1 && $agent->activity > 0)
        {
            // Получение информации о модерировании этого объекта агентом ------
            $db->setQuery('SELECT a.* FROM `#__sttnmlsmoder` AS a WHERE a.userid=' . $db->quote($user_id) . ' AND a.compid=' . $db->quote($compid) . ' AND a.cardnum=' . $db->quote($cardnum) . ' AND `typeobj`=' . $db->quote($typeobj));
            $moder = $db->loadObject();
            
            $query = $db->getQuery(true);
            if(!$moder) {
                // Объект не модерировался агентом -----------------------------
                $query->insert('`#__sttnmlsmoder`');
                $query->set('`userid`=' . $db->quote($user_id));
                $query->set('`kodagent`=' . $db->quote($agent->kod));
                $query->set('`typeobj`=' . $db->quote($typeobj));
                $query->set('`compid`=' . $db->quote($compid));
                $query->set('`cardnum`=' . $db->quote($cardnum));
            } else {
                // Объект модерировался агентом --------------------------------
                if($moder->q1 == 0 && $moder->q2 == 0) {
                    $query->update('`#__sttnmlsmoder`');
                    $query->where('`id`=' . $db->quote($moder->id));
                } else {
                    $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_MODERATE_ONE_TIME');
                    return FALSE;
                }
            }
            $query->set('`q1`=' . $db->quote($q1));
            $query->set('`q2`=' . $db->quote($q2));
            $query->set('`dateinp`=now()');
            $db->setQuery($query);
            $db->execute();
            
            // Обновление баллов активности агента -----------------------------
            $ball = 0;
            if($q1) { $ball++; }
            if($q2) { $ball++; }

            $query = $db->getQuery(TRUE);
            $query->update($db->quoteName('#__sttnmlsvocagents'));
            $query->set($db->quoteName('activity') . '=' . $db->quote($agent->activity + $ball));
            $query->where($db->quoteName('kod') . '=' . $db->quote($agent->kod));
            $db->setQuery($query);
            $db->execute();
            
            // Обработка штрафов за риэлторство и шаблон -----------------------
            if($q1 == 2 OR $q2 == 2) {                                
                $query = $db->getQuery(true);
                $query->select('a.penalty, ag.kod AS kodagent, ag.penalty AS penaltyagent, ag.realtority');
                $query->from($db->quoteName($this->_tabname) . ' AS a');
                $query->join('LEFT','`#__sttnmlsvocagents` AS ag ON a.AGENTID=ag.id AND a.COMPID=ag.COMPID');
                $query->where('a.COMPID=' . $db->quote($compid));
                $query->where('a.CARDNUM=' . $db->quote($cardnum));
                $db->setQuery($query);
                $item = $db->loadObject();
                
                
                $url = JRoute::_('index.php?option=com_sttnmls&view=' . $viewName . '&cn=' . $cardnum . '&cid=' . $compid, TRUE, -1);
                $url = '<a href="' . $url . '">' . $url . '</a>';
                if($q1 == 2 && $item) {
                    $db->setQuery('UPDATE `#__sttnmlsvocagents` SET `penalty`=' . $db->quote($item->penaltyagent + 1) . ' WHERE `kod`=' . $db->quote($item->kodagent));
                    $db->query();
                    
                    $db->setQuery('UPDATE ' . $this->_tabname . ' SET `penalty`=' . $db->quote($item->penalty + 1) . ' WHERE `COMPID`=' . $db->quote($compid) . ' AND `CARDNUM`=' . $db->quote($cardnum));
                    $db->query();
                    
                    $this->sendmailadmin(JText::_('COM_STTNMLS_Q1_EMAIL_SUBJECT'), JText::sprintf('COM_STTNMLS_Q1_EMAIL_BODY', $url));
                }
                
                
                if($q2 == 2 && $item) {
                    if($realtority > 0 && $item->realtority + 1 >= $realtority && $halyavagroup > 0) {
                        // перемещаем собственника в халявщики-риэлторы
                        $db->setQuery('UPDATE `#__sttnmlsvocagents` SET `realtority`=' . $db->quote($item->realtority + 1) . ', `grp`=' . $db->quote($halyavagroup).' WHERE `kod`=' . $db->quote($item->kodagent));
                    } else {
                        $db->setQuery('UPDATE `#__sttnmlsvocagents` SET `realtority`=' . $db->quote($item->realtority + 1) . ' WHERE `kod`=' . $db->quote($item->kodagent));
                    }
                    $db->query();
                    $this->sendmailadmin(JText::_('COM_STTNMLS_Q2_EMAIL_SUBJECT'), JText::sprintf('COM_STTNMLS_Q2_EMAIL_BODY', $url));
                }
            }
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_SUCCESSFULL_MODERATE');
            return TRUE;
        } 
        
        $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
        return FALSE;
    }
    
    /**
    * Поиск агента по ID
    * 
    * @param   int $id
    * @return  bool or object
    */
    public function findAgentByID($id = 0) {
        $result = FALSE;
        if($id > 0) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from('#__sttnmlsvocagents');
            $query->where('`userid`=' . $db->quote($id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }
        return $result;
    }

    
    public function store($data)
    {
        // Подгрузка хелпера
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // Локальны переменные
        $search_phones = array();
        $_allowed_save = FALSE;
        $_new_object = FALSE;

        // Joomla инстанции
        $app = JFactory::getApplication();
        $date = JFactory::getDate();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $mFirm = JModelLegacy::getInstance('firm', 'SttnmlsModel');

        // Полученные из запроса "управляющих" данных
        $viewName = $app->input->get('view', $this->default_view, 'string');
        $stype = $app->input->get('stype', 0, 'uint');
        $what = $app->input->get('what', 0, 'uint');
        $compid = ((isset($data['compid']) && $data['compid']) ? $data['compid'] : 0);
        $cardnum = ((isset($data['cardnum']) && $data['cardnum']) ? $data['cardnum'] : 0);
        $cancel = $app->input->get('cancel', '', 'string');
        $fromag = $app->input->get('fromag', 0, 'uint');

        // Для квартир-новостроек поля застройщика
        $_new_build = $app->input->getBool('newbuild', FALSE);
        $builder = $app->input->get('builder', 0, 'uint');
        $building = $app->input->get('building', 0, 'uint');

        if($cancel) {
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile');
            $app->redirect($redirect);
            return FALSE;
        }

        // Флаг операции добавление/редактирование
        $_new_object = (($cardnum) ? FALSE : TRUE);

        $data['price'] = ((isset($data['price']) && $data['price']) ? str_replace(' ', '', $data['price']) :  0);

        if(isset($data['price_day']) && $data['price_day']) {
            $data['price_day'] = str_replace(' ', '', $data['price_day']);
        }
        if(isset($data['price_hour']) && $data['price_hour']) {
            $data['price_hour'] = str_replace(' ', '', $data['price_hour']);
        }

        if(isset($data['pricecom']) && $data['pricecom']) {
            $data['pricecom'] = str_replace(' ', '', $data['pricecom']);
        }
        if(isset($data['priceqm']) && $data['priceqm']) {
            $data['priceqm'] = str_replace(' ', '', $data['priceqm']);
        }

        if(isset($data['pricefrom']) && $data['pricefrom']) {
            $data['pricefrom'] = str_replace(' ', '', $data['pricefrom']);
        }
        if(isset($data['priceto']) && $data['priceto']) {
            $data['priceto'] = str_replace(' ', '', $data['priceto']);
        }

        if(isset($data['compricefrom']) && $data['compricefrom']) {
            $data['compricefrom'] = str_replace(' ', '', $data['compricefrom']);
        }
        if(isset($data['compriceto']) && $data['compriceto']) {
            $data['compriceto'] = str_replace(' ', '', $data['compriceto']);
        }



        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        // 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
            $app->redirect($redirect);
            return FALSE;
        }

        // 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
            $app->redirect($redirect);
            return FALSE;
        }


        // 3. Проверка лимитов пользователя при создании объекта
        if($_new_object) {
            if(!(SttNmlsHelper::checkLimit()>-1)) {
                if (!($data['close']==1 or $data['realtors_base']==1)){ //ЛМВ: заглушка, чтобы не выскакивала ошибка для скрытых и риэлторских объектов
                    $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_LIMIT_OBJECT_EXCEEDED');
                    $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what . '&limit=' . SttNmlsHelper::checkLimit());
                    $app->redirect($redirect);
                    return FALSE;
                }
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }


        // 4. Проверка возможности редактирования объекта пользователем(агентом)
        if(!$_new_object) {
            // Получение информации об объекте
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName($this->_tabname));
            $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
            $db->setQuery($query);
            $object = $db->loadObject();

            // Если объект не существует - ошибка
            if(!$object) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
                $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
                $app->redirect($redirect);
                return FALSE;
            }

            // Проверка администратор ли пользователь (возможность редактирования в системе)?
            $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

            // Если не админ системы
            if(!$_allowed_save) {

                // Если агент пытается редактировать "чужой" объект - ошибка
                // 2 случая:
                //       1) агент из другой организации по отношению к объекту;
                //       2) агент из правильной организации, но не является хозяином объекта либо боссом
                if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                    $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                    $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
                    $app->redirect($redirect);
                    return FALSE;
                }

                // Если все ОК, разрешаем сохранить данные
                $_allowed_save = TRUE;
            }
        }

        // Если не получили право сохранения до сих пор - ошибка
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
            $app->redirect($redirect);
            return FALSE;
        }

        // Если улица не передана - ошибка
        if(isset($data['streetid']) && !$data['streetid']) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_FIELD_STREET');
            if($_new_object) {
                $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
            } else {
                $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit&cn=' . $cardnum . '&cid=' . $compid);
            }
            $app->redirect($redirect);
            return FALSE;
        }

        // Проверка цены объекта
        if(isset($data['price']) && $data['price']) {
            $min_price = $params->get($viewName . '_min_price_' . (($stype) ? 'rent' : 'sale'), 0);
            if($min_price && $data['price'] && $data['price'] < $min_price) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_PRICE_TOO_SMALL', $min_price);
                if($_new_object) {
                    $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
                } else {
                    $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit&cn=' . $cardnum . '&cid=' . $compid);
                }
                $app->redirect($redirect);
                return FALSE;
            }
        }

        if($fromag) {
            if($fromag != $agent->ID) {
                $target_agent = $this->getAgentInfoByID($fromag);
                if($target_agent) {
                    $agent = $target_agent;
                }
            }
        }
        if(isset($data['fromag'])) {
            unset($data['fromag']);
        }

        // Подготовка данных
        if($_new_object) {
            if(isset($data['dateinp'])) { unset($data['dateinp']); }
            $data['exchdate'] = $date->toSql();

            // Вычисление уникального идентификатора объекта
            $query = $db->getQuery(TRUE);
            $query->select('MAX(' . $db->quoteName('cardnum') . ')');
            $query->from($db->quoteName($this->_tabname));
            $db->setQuery($query);
            $cardnum = (int)$db->loadResult();
            $cardnum++;

            $data['cardnum'] = $cardnum;
            $data['compid'] = $agent->COMPID;


            $data['PICCOUNT'] = 0;
            $data['PICTURES'] = '';
            $data['PICNAMES'] = '';
        } else {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName(array('PICTURES', 'PICNAMES')));
            $query->from($db->quoteName($this->_tabname));
            $query->where($db->quoteName('cardnum') . '=' . $db->quote($data['cardnum']));
            $query->where($db->quoteName('compid') . '=' . $db->quote($data['compid']));
            $db->setQuery($query);

            $object = $db->loadObject();

            if(trim($object->PICTURES) != '') {
                $pictures = explode(';', $object->PICTURES);
                $picnames = $app->input->get('picnames', array(), 'array');

                if($pictures) {
                    $tmp = array();
                    foreach($pictures as $idx => $fn) {
                        $tmp[$idx] = ((isset($picnames[$idx])) ? $picnames[$idx] : 'Фото');
                    }
                    $data['picnames'] = implode(';', $tmp);
                }
            }
        }

        // Обновление информации об агенте
        $data['agentid'] = $agent->ID;
        $data['agentname'] = $agent->SIRNAME . ' ' . $agent->NAME . ' ' . $agent->SECNAME;
        $data['firmname'] = $agent->firmname;

        if(trim($agent->PHONE) != '') { $data['agentname'] .= ', ' . $agent->PHONE; }
        if(trim($agent->ADDPHONE) != '') { $data['agentname'] .= ', ' . $agent->ADDPHONE; }
        if(trim($agent->EMAIL) != '') { $data['agentname'] .= ', ' . $agent->EMAIL; }
        if(trim($agent->firmphone) != '') { $data['firmname'] .= ', ' . $agent->firmphone; }
        if(trim($agent->firmaddphone) != '') { $data['firmname'] .= ', ' . $agent->firmaddphone; }
        if(trim($agent->firmemail) != '') { $data['firmname'] .= ', ' . $agent->firmemail; }
        if(trim($agent->firmsite) != '') { $data['firmname'] .= ', ' . $agent->firmsite; }


        if(isset($data['datecor'])) { unset($data['datecor']); }

        $data['inet'] = 1;
        $data['variant'] = $stype + 1;
        $data['flag'] = 2;

        $data['sob_fio'] = $app->input->post->get('sob_fio', '', 'string');
        $data['sob_tel'] = $app->input->post->get('sob_tel', '', 'string');
        $data['sob_tel2'] = $app->input->post->get('sob_tel2', '', 'string');
        $data['sob_visible'] = $app->input->post->get('sob_visible', 0, 'uint');



        // Подготовка телефонов для поиска -----------------------------------------------------------------------------
        $search_phones[] = $data['sob_tel'];
        $search_phones[] = $data['sob_tel2'];
        $data['phones'] = preg_replace('/[\s\(\)-]/', '', implode('|', $search_phones));

        // Подготовка запроса сохранения данных в БД
        $query = $db->getQuery(true);
        if($_new_object) {
            $query->insert($db->quoteName($this->_tabname));
            $query->set($db->quoteName('DATEINP') . '=NOW()');
        } else {
            $query->update($db->quoteName($this->_tabname));
            $query->where($db->quoteName('cardnum') . '=' . $db->quote($data['cardnum']));
            $query->where($db->quoteName('compid') . '=' . $db->quote($data['compid']));
        }
        $query->set($db->quoteName('DATECOR') . '=NOW()');

        $this->_item = new stdClass();
        foreach($data as $key => $val)
        {
            if(!$_new_object && ($key == 'cardnum' OR $key == 'compid')) {
                continue;
            }

            $query->set($db->quoteName($key) . '=' . $db->quote($val));
            $this->_item->$key = $val;
        }
        $db->setQuery($query);
        $db->execute();

        if($db->getErrorNum()) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_WHILE_SQL_EXECUTING', $db->getErrorMsg());
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit&cn=' . $data['cardnum'] . '&cid=' . $data['compid'] . '&stype=' . $stype);
            $app->redirect($redirect);
            return FALSE;
        }

        $this->sendmail();

        if($viewName == 'apart' && $data['compid'] && $data['cardnum']) {
            // Удаление связей между квартирой и затройщиком
            $query = $db->getQuery(TRUE);
            $query->delete($db->quoteName('#__sttnmls_buildings_aparts_relations'));
            $query->where($db->quoteName('aparts_compid') . '=' . $db->quote((int)$data['compid']));
            $query->where($db->quoteName('aparts_cardnum') . '=' . $db->quote((int)$data['cardnum']));
            $db->setQuery($query);
            $db->execute();

            if($_new_build && $builder && $building) {
                $query = $db->getQuery(TRUE);
                $query->insert($db->quoteName('#__sttnmls_buildings_aparts_relations'));
                $query->columns($db->quoteName(array('builder', 'builder_cardnum', 'aparts_compid', 'aparts_cardnum')));
                $query->values($db->quote($builder) . ', ' . $db->quote($building) . ', ' . $db->quote($data['compid']) . ', ' . $db->quote($data['cardnum']));
                $db->setQuery($query);
                $db->execute();
            }
        }


        if($_new_object) {
            $action = (($app->input->post->get('save', '', 'string') != '') ? 'SAVE' : '');
            $action = (($app->input->post->get('newobj', '', 'string') != '') ? 'SAVE_AND_ADD_PHOTO' : $action);

            switch($action) {
                case 'SAVE_AND_ADD_PHOTO':
                        $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit' . '&cn=' . $data['cardnum'] . '&cid=' . $data['compid'] . '&stype=' . $stype . '&tab=photo');
                    break;

                default:
                        $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
            }
        } else {
            $action = (($app->input->post->get('save', '', 'string') != '') ? 'SAVE' : '');
            $action = (($app->input->post->get('apply', '', 'string') != '') ? 'APPLY' : $action);
            $action = (($app->input->post->get('addnew', '', 'string') != '') ? 'SAVE_AND_NEW' : $action);



            switch($action) {
                case 'APPLY':
                        $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit&cn=' . $data['cardnum'] . '&cid=' . $data['compid'] . '&stype=' . $stype);
                    break;


                case 'SAVE_AND_NEW':
                        $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&task=edit' . '&stype=' . $stype);
                    break;

                default:
                        $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype . '&what=' . $what);
                    break;
            }
        }

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_SAVED');
        $app->redirect($redirect);
        return TRUE;
    }
    
    /**
     * Удаление объекта
     * 
     * @return boolean
     */
    function removeObject()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php' );


        //
        $_allowed_save = FALSE;

        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Удаление подключенных услуг на объект
        SttNmlsPlatusl::deleteUpobj($cardnum, $compid);


        switch($this->_tabname) {

            case '#__sttnmlsbuildings':
                $tObject  = JTable::getInstance('buildings', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivebuildings', 'SttnmlsTable');
                break;

            case '#__sttnmlscommerc':
                $tObject  = JTable::getInstance('commerc', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivecommerc', 'SttnmlsTable');
                break;

            case '#__sttnmlsgarages':
                $tObject  = JTable::getInstance('garages', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivegarages', 'SttnmlsTable');
                break;

            case '#__sttnmlshouses':
                $tObject  = JTable::getInstance('houses', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivehouses', 'SttnmlsTable');
                break;



            default:
                $tObject  = JTable::getInstance('aparts', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archiveaparts', 'SttnmlsTable');
                break;
        }

        $object->EXCHDATE = JDate::getInstance('now', SttNmlsHelper::getTimeZone())->toSql(true);
        $object->archiver_id = $user->id;

        if($tArchive->bind($object)) {
            $tArchive->store();
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN');
            return false;
        }

        if($tObject->delete($object->oid)) {
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_SUCCESSFULL_REMOVED');
            return true;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }



        // Получение списка картинок для объекта -------------------------------
//        $query = $db->getQuery(true);
//        $query->select('`PICTURES`');
//        $query->from($this->_tabname);
//        $query->where('`cardnum`=' . $db->quote($cardnum));
//        $query->where('`compid`=' . $db->quote($compid));
//        $db->setQuery($query);
//
//        $p = $db->loadObject();
//        if($p) {
//            // Удаление картинок, если они есть, с сервера ----------------------
//            $pics = explode(';', $p->PICTURES);
//            if(count($pics) > 0) {
//                try
//                {
//                    foreach($pics as $pic_filename)
//                    {
//                        if($pic_filename != '' && file_exists(JPATH_ROOT . 'images/objects/' . $pic_filename)) {
//                            unlink(JROOT_PATH . 'images/objects/' . $pic_filename);
//                        }
//                    }
//                } catch (Exception $e) {
//                    $this->raiseEx($e->getMessage());
//                    return FALSE;
//                }
//            }
//        }



        // Удаление связей ЗАСТРОЙЩИК<->КВАРТИРА
//        if($object){
//            $query = $db->getQuery(TRUE);
//
//            if($this->_tabname == '#__sttnmlsaparts') {
//                $query->delete($db->quoteName('#__sttnmls_buildings_aparts_relations'));
//                $query->where($db->quoteName('aparts_compid') . '=' . $db->quote((int)$compid));
//                $query->where($db->quoteName('aparts_cardnum') . '=' . $db->quote((int)$cardnum));
//                $db->setQuery($query);
//                $db->execute();
//            }
//            if($this->_tabname == '#__sttnmlsbuildings') {
//                $query->delete($db->quoteName('#__sttnmls_buildings_aparts_relations'));
//                $query->where($db->quoteName('builder') . '=' . $db->quote((int)$object->comp));
//                $query->where($db->quoteName('builder_cardnum') . '=' . $db->quote((int)$cardnum));
//                $db->setQuery($query);
//                $db->execute();
//            }
//
//            JLog::addLogger(
//                array(
//                    'text_file' => 'com_sttnmls.php.log'
//                )
//            );
//            JLog::add('Fn: removeObject(); COMPID: ' . $compid . '; CARDNUM: ' . $cardnum . '; COMP: ' . $object->comp . '; QUERY: ' . (string)$query, JLog::WARNING, 'com_sttnmls');
//        }




        
        // Удаление объекта
//        $query = $db->getQuery(true);
//        $query->update($db->quoteName($this->_tabname));
//        $query->set('`flag`=4');
//        $query->where('`cardnum`=' . $db->quote($cardnum));
//        $query->where('`compid`=' . $db->quote($compid));
//        $query->where('`flag`=2 OR `flag`=1');
//        $db->setQuery($query);
//        $db->query();
//
//        if($db->getAffectedRows()) {
//            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_SUCCESSFULL_REMOVED');
//            return TRUE;
//        } else {
//            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
//            return FALSE;
//        }
    }

    public function repairObjectFromArchive($view, $oid)
    {
        $user = JFactory::getUser();

        // ID объекта должен быть передан
        if(!$oid OR !$view) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_PARAMS_ERROR'));
            return false;
        }

        // Подключение таблиц
        switch($view) {

            case 'com':
                $tObject = JTable::getInstance('commerc', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivecommerc', 'SttnmlsTable');
                break;

            case 'garage':
                $tObject = JTable::getInstance('garages', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivegarages', 'SttnmlsTable');
                break;

            case 'house':
                $tObject = JTable::getInstance('houses', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archivehouses', 'SttnmlsTable');
                break;

            default:
                $tObject = JTable::getInstance('aparts', 'SttnmlsTable');
                $tArchive = JTable::getInstance('archiveaparts', 'SttnmlsTable');
                break;
        }

        if(!$tObject OR !$tArchive) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN'));
            return false;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return false;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return false;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)
        $tArchive->load($oid);
        if(!$tArchive->oid) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND'));
            return false;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $tArchive->COMPID OR ($agent->COMPID == $tArchive->COMPID && !$agent->boss && $agent->ID != $tArchive->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return false;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = true;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return false;
        }
        unset($tArchive->archiver_id);

        if($tObject->bind($tArchive)) {
            $tObject->store();
            $tArchive->delete($oid);
        } else {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN'));
            return false;
        }

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_SUCCESSFULL_REMOVED');
        return true;
    }

    public function removeObjectFromArchive($view, $oid)
    {
        $user = JFactory::getUser();

        // ID объекта должен быть передан
        if(!$oid OR !$view) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_PARAMS_ERROR'));
            return false;
        }

        // Подключение таблиц
        switch($view) {

            case 'com':
                $tArchive = JTable::getInstance('archivecommerc', 'SttnmlsTable');
                break;

            case 'garage':
                $tArchive = JTable::getInstance('archivegarages', 'SttnmlsTable');
                break;

            case 'house':
                $tArchive = JTable::getInstance('archivehouses', 'SttnmlsTable');
                break;

            default:
                $tArchive = JTable::getInstance('archiveaparts', 'SttnmlsTable');
                break;
        }

        if(!$tArchive) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN'));
            return false;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return false;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return false;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)
        $tArchive->load($oid);
        if(!$tArchive->oid) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND'));
            return false;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $tArchive->COMPID OR ($agent->COMPID == $tArchive->COMPID && !$agent->boss && $agent->ID != $tArchive->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return false;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = true;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return false;
        }

        // Удаление картинок, если они есть, с сервера ----------------------
        $pics = explode(';', $tArchive->PICTURES);
        if(count($pics) > 0) {
            try
            {
                foreach($pics as $pic_filename)
                {
                    if($pic_filename != '' && file_exists(JPATH_ROOT . 'images/objects/' . $pic_filename)) {
                        unlink(JROOT_PATH . 'images/objects/' . $pic_filename);
                    }
                }
            } catch (Exception $e) {
                $this->raiseEx($e->getMessage());
                return  false;
            }
        }

        // Удаляем привязки новостроек к квартирам
        if($view == 'apart') {
            $db = $this->getDbo();
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('#__sttnmls_buildings_aparts_relations'));
            $query->where($db->quoteName('aparts_compid') . '=' . $db->quote($tArchive->COMPID));
            $query->where($db->quoteName('aparts_cardnum') . '=' . $db->quote($tArchive->CARDNUM));
            $db->setQuery($query);
            $db->execute();
        }
        if($view == 'building') {
            $db = $this->getDbo();
            $query = $db->getQuery(true);
            $query->delete($db->quoteName('#__sttnmls_buildings_aparts_relations'));
            $query->where($db->quoteName('builder') . '=' . $db->quote($tArchive->comp));
            $query->where($db->quoteName('builder_cardnum') . '=' . $db->quote($tArchive->CARDNUM));
            $db->setQuery($query);
            $db->execute();
        }

        // Удаляем сам объект
        if($tArchive->delete($oid)) {
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_SUCCESSFULL_REMOVED');
            return true;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return false;
        }
    }
    
    /**
     * Изменение даты создания объекта
     * 
     * @return boolean
     */
    function changeObjectCreateDate()
    {
        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');

        //
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Проверка заполненности поля в админке -------------------------------
        if($params->get('dateprice', '') == '') {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SERVICE_UNAVALABLE');
            return FALSE;
        }

        // Получение информации о балансе пользователя
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsbalans'));
        $query->where($db->quoteName('userid') . '=' . $db->quote($agent->userid));
        $db->setQuery($query);
        $balans = $db->loadObject();
          
        // Проверка баланса для выполнения операции ----------------------------
        if($balans->balans < $params->get('dataprice', 0)) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_MONEY_WITH_PRICE', $params->get('dateprice', ''));
            return FALSE;
        }

        // Обновление баланса пользователя -------------------------------------
        $query = $db->getQuery(true);
        $query->set($db->quoteName('balans') . '=' . $db->quoteName('balans') . '-' . $params->get('dateprice', 0));
        $query->where($db->quoteName('userid') . '=' . $db->quote($agent->userid));
        $query->update($db->quoteName('#__sttnmlsbalans'));
        $db->execute();

        // Логирование операции ------------------------------------------------
        $bl = JTable::getInstance('balanslog', 'SttnmlsTable');
        $data = array(
            'userid'    =>  $agent->userid,
            'summa'     =>  '-' . $params->get('dateprice', ''),
            'prim'      =>  'Списание за сброс даты добавления объекта',
            'status'    =>  1,
            'dateinp'   =>  JDate::getInstance('now', SttNmlsHelper::getTimeZone())->toSql(true)
        );

        if($bl->bind($data)) {
            $bl->store();
        }

        // Изменения в объекте -------------------------------------------------
        $query = $db->getQuery(true);
        $query->update($db->quoteName($this->_tabname));
        $query->set('`DATECOR`=NOW()');
        $query->set('`DATEINP`=NOW()');
        $query->where('`cardnum`=' . $db->quote($cardnum));
        $query->where('`compid`=' . $db->quote($compid));
        $db->setQuery($query);
        $db->execute();
            
        if($db->getAffectedRows()) {
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_CREATEDATE_SUCCESSFULL_UPDATED');
            return TRUE;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        } 
    }
    
    function changeObjectEditDate()
    {
        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        //
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('compid', 0, 'uint');

        // Если не передана идентификационная информация объекта - ошибка
        if(!$compid OR !$cardnum) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }

        //-> 2. Проверка агента (корректность пользовательских данных)
        $agent = $this->getAgentInfoByID($user->id);
        if(!$agent) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_CORRUPTED', $user->id);
            return FALSE;
        }


        //-> 3. Проверка возможности редактирования объекта пользователем(агентом)

        // Получение информации об объекте
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName($this->_tabname));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cardnum));
        $db->setQuery($query);
        $object = $db->loadObject();

        // Если объект не существует - ошибка
        if(!$object) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND');
            return FALSE;
        }

        // Проверка администратор ли пользователь (возможность редактирования в системе)?
        $_allowed_save = $user->authorise('core.edit', 'com_sttnmls');

        // Если не админ системы
        if(!$_allowed_save) {

            // Если агент пытается редактировать "чужой" объект - ошибка
            // 2 случая:
            //       1) агент из другой организации по отношению к объекту;
            //       2) агент из правильной организации, но не является хозяином объекта либо боссом
            if($agent->COMPID != $object->COMPID OR ($agent->COMPID == $object->COMPID && !$agent->boss && $agent->ID != $object->AGENTID)) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_OBJECT_YOU_NOT_OWNER');
                return FALSE;
            }

            // Если все ОК, разрешаем сохранить данные
            $_allowed_save = TRUE;
        }

        // Если не получили право сохранения до сих пор - доступ запрещен
        if(!$_allowed_save) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }
        
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->update($db->quoteName($this->_tabname));
        $query->set('`DATECOR`=NOW()');
        $query->where('`cardnum`=' . $db->quote($cardnum));
        $query->where('`compid`=' . $db->quote($compid));
        $db->setQuery($query);
        $db->query();
        
        if($db->getAffectedRows()) {
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_OBJECT_EDITDATE_SUCCESSFULL_UPDATED');
            return TRUE;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }    
            
    }
    
    function getOpt($razdel,$fname, $class="validate-sttnum")
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('NAME as text, ID as value');
        $query->from(' `#__sttnmlsvocalldata` ');
        $query->where(' RAZDELID=' . $db->quote($razdel));
        $query->where(' SHORTNAME<>"-" ');
        $query->order('text');
        $db->setQuery($query);
        $res = $db->loadAssocList();
        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
        if(is_array($res)) {
            foreach($res as $row)
            {
                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
            } 
        }
        return JHTML::_('select.genericlist',  $options, 'jform['.$fname.']', 'class="form-control '.$class.'"', 'value', 'text', $this->_item->$fname);
    }
    
	
	public function getItemC($compid,$cardnum)
	{
		$db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');
        $_isArchiveMode = JFactory::getApplication()->input->get('archive', 0, 'uint');

		$db->setQuery('update '.$db->quoteName($this->_tabname).' set watchcount=watchcount+1 where COMPID='.$db->quote($compid).' and CARDNUM='.$db->quote($cardnum));
		$db->query();

		$query = $db->getQuery(TRUE);
        $query->select($db->quoteName('a') . '.*');
        $query->select('if(a.ORIENT <> "", CONCAT("'.JText::_('COM_STTNMLS_OKOLO').' ", a.ORIENT), "") AS orientir');
        $query->select('DATE_FORMAT(a.DATEINP,"%d.%m.%Y") AS datecr');
		$query->select('DATE_FORMAT(a.DATECOR,"%d.%m.%Y") AS datemd');
		$query->select('ag.userid as agentuserid');
		$query->select('concat(ag.SIRNAME, " ", ag.NAME , " ", ag.SECNAME , "<br/>", ag.PHONE , "<br/>", ag.ADDPHONE) as agentinf');
		$query->select('IFNULL(' . $db->quoteName('gr.mdays') . ', 0) AS ' . $db->quoteName('mdays') . ',  TIMESTAMPDIFF(MINUTE, a.DATEINP, NOW())/24/60 as alldays ');
		$query->select('f.article_url as firm_url ');
		$query->select('f.NAME as firm, f.PHONE as firmphone, f.ADDPHONE as firmphone2 ' . $this->getDopSelect());
        $query->select((($_isArchiveMode) ? '1' : '0') . ' AS ' . $db->quoteName('isArchiveMode'));
        if($_isArchiveMode) {
            $query->select(
                'CONCAT(' .
                    $db->quoteName('archiver.SIRNAME') . ', " ", ' .
                    $db->quoteName('archiver.NAME') . ', " ", ' .
                    $db->quoteName('archiver.SECNAME') .
                ') AS ' . $db->quoteName('archiver')
            );
            $query->select($db->quoteName('archiver.COMPID', 'archiver_compid'));
            $query->select($db->quoteName('archiver.ID', 'archiver_iid'));
        }
        $query->from($db->quoteName($this->_tabname) . ' AS a ');
		$query->join('LEFT','`#__sttnmlsvoccity` as c on a.CITYID=c.id');
		$query->join('LEFT','`#__sttnmlsvocraion` as r on a.RAIONID=r.id');
		$query->join('LEFT','`#__sttnmlsvocmraion` as m on a.MRAIONID=m.id');
		$query->join('LEFT','`#__sttnmlsvocstreet` as s on a.STREETID=s.id');
		$query->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.id');
		$query->join('LEFT','`#__sttnmlsvocagents` as ag on a.AGENTID=ag.id and a.COMPID=ag.COMPID');
		$query->join('LEFT','`#__sttnmlsgroup` as gr on gr.id=ag.grp');
        if($_isArchiveMode) {
            $query->join(
                'LEFT',
                $db->quoteName('#__sttnmlsvocagents', 'archiver') . ' on ' .
                $db->quoteName('a.archiver_id') . '=' . $db->quoteName('archiver.userid')
            );
        }

        // Отображение связанных квартир в новостройках (КВАРТИРА<->ЗАСТРОЙЩИК)
        if($this->_tabname == '#__sttnmlsaparts') {
            $query->select($db->quoteName('barel.builder') . ', ' . $db->quoteName('barel.builder_cardnum', 'building') . ', ' . $db->quoteName('building.title', 'building_title'));
            $query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.aparts_compid') . '=' . $db->quoteName('a.COMPID') . ' AND ' . $db->quoteName('barel.aparts_cardnum') . '=' . $db->quoteName('a.CARDNUM') . ')');
            $query->join('LEFT', $db->quoteName('#__sttnmlsbuildings', 'building') . ' ON(' . $db->quoteName('barel.builder') . '=' . $db->quoteName('building.comp') . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quoteName('building.CARDNUM') . ')');
        }

        if($this->_tabname == '#__sttnmlsbuildings') {
            $query->select('COUNT(' . $db->quoteName('ap.cardnum') . ') AS ' . $db->quoteName('aparts_counts'));
            $query->join('LEFT', $db->quoteName('#__sttnmls_buildings_aparts_relations', 'barel') . ' ON(' . $db->quoteName('barel.builder') . '=' . $db->quoteName('a.comp') . ' AND ' . $db->quoteName('barel.builder_cardnum') . '=' . $db->quoteName('a.CARDNUM') . ')');
            $query->join('LEFT', $db->quoteName('#__sttnmlsaparts', 'ap') . ' ON(' . $db->quoteName('ap.COMPID') . '=' . $db->quoteName('barel.aparts_compid') . ' AND ' . $db->quoteName('ap.CARDNUM') . '=' . $db->quoteName('barel.aparts_cardnum') . ' AND ' . $db->quoteName('ap.close') . '=' . $db->quote(0) . ' AND ' . $db->quoteName('ap.flag') . '<3 AND ((IFNULL(' . $db->quoteName('gr.days') . ', 0) > 0 AND ' . $db->quoteName('ap.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('gr.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('gr.days') . ',0)<=0 AND ' . $db->quoteName('ap.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . $params->get('days', 30) . ' day)))' . ')');
            $query->group($db->quoteName('a.comp') . ', ' . $db->quoteName('a.cardnum'));
        } else {
            $query->select('IF(' . $db->quoteName('a.date_checked_to') . '>' . ' NOW(), "1", "0") AS ' . $db->quoteName('checked'));
        }
        // -------------------------------------------------------------------------------------------------------------

		$this->setDopJoin($query);

        if($this->_tabname == '#__sttnmlsbuildings') {
            $query->where($db->quoteName('a.comp') . '=' . $db->quote($compid));
        } else {
            $query->where($db->quoteName('a.COMPID') . '=' . $db->quote($compid));
        }

		$query->where(' a.CARDNUM='.$db->quote($cardnum));
		$query->where(' a.flag<3 ');
		$db->setQuery($query);
		$this->_item=$db->loadObject();

//        print_r((string)$query);exit;


		if($this->_item) {
			//$this->_item->MISC = $this->mb_ucfirst(trim($this->_item->MISC));
			$this->_item->datecr = $this->rusdate(strtotime($this->_item->datecr));
			$this->_item->datemd = $this->rusdate(strtotime($this->_item->datemd));
			require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
			
			//print_r($_REQUEST);
			
			if($_REQUEST['view']=='building')
			{
				$this->_item->imgfirm = Avatar::getLinkFirmLogo($this->_item->comp,0);
				
				$db = JFactory::getDbo();
				$query	= 'SELECT * FROM #__sttnmlsvocfirms WHERE ID="'.$this->_item->comp.'"';
				$db->setQuery($query);
				$fr	= $db->loadObjectList();
				$this->_item->FIRMNAME = $fr[0]->NAME . '<br>' . $fr[0]->PHONE . '<br>' . $fr[0]->ADDPHONE;
			}else{
				$this->_item->imgfirm = Avatar::getLinkFirmLogo($this->_item->COMPID,0);
				$this->_item->FIRMNAME = $this->_item->firm . ' ' . $this->_item->firmphone . ' ' . $this->_item->firmphone2;
			}
			
			//if($this->_item->EXCLUSIVE!=3) $this->_item->EXCLUSIVE = 0; //сбрасывало флаг в edit
		}
		return $this->_item;
	}

	public function getUserPerm()
	{
		//возвращает 0 - никаких прав, 1 - хозяин объекта, 
		//2 - не хозяин, но модератор, 3 - модератор, но уже нажимал кнопку "показать контакты"
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // JOOMLA Instances
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

		if(!$this->_item OR !$user->id) { return 0; }

        $agent_info = $this->getAgentInfoByID();


        // Проверка является ли пользователь собственником объекта
        if($agent_info) {
            if($this->_item->COMPID == $agent_info->COMPID) {
                if($this->_item->AGENTID == $agent_info->ID OR $agent_info->boss) {
                    return 1;
                }
            }
        }

        // Проверка прав по тарифу на просмотр контактов собственника
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName(array('a.activity', 'g.seecont')));
        $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON(' . $db->quoteName('g.id') . '=' . $db->quoteName('a.grp') . ')');
		$query->where($db->quoteName('a.userid') . '=' . $db->quote($user->id));
        $db->setQuery($query);
        $tariff_access = $db->loadObject();

		// Подготовка опций формы модерирования
		$this->_item->q1 = 0;
		$this->_item->q2 = 0;
		$r = SttNmlsHelper::getOptQ(0,0);
		$this->_item->q1opt = $r->q1opt;
		$this->_item->q2opt = $r->q2opt;
		$this->_item->listmoder = '';

		if($tariff_access && $tariff_access->seecont == 1 && $tariff_access->activity > 0)
		{
            switch($this->_tabname) {
                case '#__sttnmlscommerc': $typeobj = 2; break;
                case '#__sttnmlsgarages': $typeobj = 3; break;
                case '#__sttnmlshouses': $typeobj = 4; break;
                default: $typeobj = 1; break;
            }

            // Получение списка модерирования
			$this->_item->listmoder = SttNmlsHelper::getListModer($typeobj, $this->_item->COMPID, $this->_item->CARDNUM);

            // Проверка модерировал ли пользователь объект или нет
            $query = $db->getQuery(TRUE);
            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsmoder'));
            $query->where($db->quoteName('userid') . '=' . $db->quote($user->id));
            $query->where($db->quoteName('compid') . '=' . $db->quote($this->_item->COMPID));
            $query->where($db->quoteName('cardnum') . '=' . $db->quote($this->_item->CARDNUM));
            $query->where($db->quoteName('typeobj') . '=' . $db->quote($typeobj));
            $db->setQuery($query);
            $moderation = $db->loadObject();

            if(!$moderation) {
                return 2;
            }

			$r = SttNmlsHelper::getOptQ($moderation->q1, $moderation->q2);
			$this->_item->q1opt=$r->q1opt;
			$this->_item->q2opt=$r->q2opt;
			$this->_item->q1=$moderation->q1;
			$this->_item->q2=$moderation->q2;
			return 3;
		}
		return 0;
	}

    public function getComUserThumb()
	{
		require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
		return Avatar::getUserThumbAvatar($this->_item->agentuserid);
	}
    function mb_ucfirst ($word)
    {
        return mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr(mb_convert_case($word, MB_CASE_LOWER, 'UTF-8'), 1, mb_strlen($word), 'UTF-8');
    }
    private function rusdate($d, $format = 'j %MONTH% Y', $offset = 0)
    {
        $montharr = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $dayarr = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $d += 3600 * $offset;

        $sarr = array('/%MONTH%/i', '/%DAYWEEK%/i');
        $rarr = array( $montharr[date("m", $d) - 1], $dayarr[date("N", $d) - 1] );

        $format = preg_replace($sarr, $rarr, $format);
        return date($format, $d);
    }
	public function getImgpath(){
		$inpath = "images/sttnmls/";
        $image_folder = JPATH_ROOT . '/' . $inpath;
        if (!is_dir($image_folder)) {
            mkdir($image_folder);
        }
		if($this->_item->flag==2 || $this->_item->flag==4)
			return JURI::root(). $inpath;
		$params = JComponentHelper::getParams('com_sttnmls');
		$imgpath=$params->get('imgpath', 'http://www.nmls.ru/mlsfiles/images/');
		return $imgpath;
	}
	public function getUdobstva()
	{
		$udobstva = '';
		$g='';
		if (isset($this->_item->CHECKS1)) {
			$udobstva=rtrim($this->_item->CHECKS1,'/');
			$g='/';
		}
		if (isset($this->_item->CHECKS2))
			$udobstva .= $g . rtrim($this->_item->CHECKS2,'/');
		
		$uarr = explode('/', $udobstva);
		if (count($uarr) > 0) { $udobstva = implode (', ', $uarr); }
		$udobstva = $this->mb_ucfirst(trim($udobstva));
		return $udobstva;
	}
	public function setTabname($s)
	{
		$this->_tabname=$s;
	}
	public function getTp()
	{
		return $this->_tp;
	}
	public function setTp($s)
	{
		$this->_tp=$s;
	}
	public function getTitle()
	{
		if($_REQUEST['view']=='building')
		{
			return $this->_item->title;
		}else{
			if($this->_item->WHAT>0)
			{
				if ($this->_item->VARIANT == 2)
				{
					return JText::_('COM_STTNMLS_SDAMKM').': ком.: '.$this->_item->ROOMS.", ".$this->_item->o_zh_k." кв.м, ".$this->_item->gorod.", ".$this->_item->ulica;
				} else {
					return JText::_('COM_STTNMLS_PRODAMKM').': ком.: '.$this->_item->ROOMS.", ".$this->_item->o_zh_k." кв.м, ".$this->_item->gorod.", ".$this->_item->ulica;
				}
			}
			else {
				if ($this->_item->VARIANT == 2)
				{
					return JText::_('COM_STTNMLS_SDAMKV').': '.$this->_item->ROOMS."-комнатная, ".$this->_item->o_zh_k." кв.м, ".$this->_item->gorod.", ".$this->_item->ulica;
				} else {
					return JText::_('COM_STTNMLS_PRODAMKV').': '.$this->_item->ROOMS."-комнатная, ".$this->_item->o_zh_k." кв.м, ".$this->_item->gorod.", ".$this->_item->ulica;
				}
			}
		}
		
	}
	public function getDesc()
	{
		if($_REQUEST['view']=='building')
		{
			return $this->_item->fulltitle;
		}else{
			if($this->_item->WHAT>0)
			{
				return 'ком.: '.$this->_item->ROOMS.", на ".$this->_item->ulica.", ".$this->_item->STAGE.'/'.$this->_item->HSTAGE."-".$this->_item->mat.", площадь: ".$this->_item->o_zh_k." кв.м, цена: ".number_format($this->_item->PRICE/1000,3, ' ', ' ')." рублей.";
			}
			else
			{
				return $this->_item->ROOMS."-комнатная квартира, на ".$this->_item->ulica.", ".$this->_item->STAGE.'/'.$this->_item->HSTAGE."-".$this->_item->mat.", площадь: ".$this->_item->o_zh_k." кв.м, цена: ".number_format($this->_item->PRICE/1000,3, ' ', ' ')." рублей.";			
			}
		}
	}
	
	public function getDesc2()
	{
		if($this->_item->WHAT>0)
		{
			return "<center><b>Комнат: ".$this->_item->ROOMS."</b></center><span style='font-size:11px;'><span style='color:#444;'>".$this->_item->ulica.", ".$this->_item->STAGE.'/'.$this->_item->HSTAGE."-".$this->_item->mat."</span><br><b>Площадь:</b> ".$this->_item->o_zh_k." кв.м<br><b>Цена:</b> ".number_format($this->_item->PRICE/1000,3, ' ', ' ')." руб.</span>";		
			
		}
		else
		{
			return "<center><b>".$this->_item->ROOMS."-комнатная квартира</b></center><span style='font-size:11px;'><span style='color:#444;'>".$this->_item->ulica.", ".$this->_item->STAGE.'/'.$this->_item->HSTAGE."-".$this->_item->mat."</span><br><b>Площадь:</b> ".$this->_item->o_zh_k." кв.м<br><b>Цена:</b> ".number_format($this->_item->PRICE/1000,3, ' ', ' ')." руб.</span>";			
		}	
	}
	
	
	
	
	public function getOptCity()
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		return SttNmlsHelper::getOptCity(false);
	}
	public function getDopSelect()
	{
		// для неквартир эту функцию нужно переопределить в модели
		return ',c.NAME as gorod,
			r.NAME as raion,
			m.NAME as mraion,
			concat(s.NAME,if(a.HAAP <> " ",	CONCAT(",",a.HAAP), "")) as ulica,
			if(a.WHAT = 0,"квартира","комната") AS object,
			d4.NAME as tip,
			d8.NAME as mat,
			CONCAT(a.STAGE, "/",a.HSTAGE,"-",d8.SHORTNAME) AS planirovka,
            CONCAT(TRUNCATE(a.AAREA,0), "/", TRUNCATE(a.LAREA,0), "/", TRUNCATE(a.KAREA,0)) AS o_zh_k ';
	}
	public function setDopJoin($query)
	{
		// здесь указываем дополнительные условия и джойны для запроса
		// для неквартир эту функцию нужно переопределить в модели
		$query->join('LEFT','`#__sttnmlsvocalldata` as d4 on a.HTYPEID=d4.id and d4.RAZDELID=4');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d8 on a.WMATERID=d8.id and d8.RAZDELID=8');
		return;
	}
                
        

	
	
	
	

	public function mainPhoto($flnum)
	{
		$viewName = JRequest::getVar('view', 'apart');
		$cardnum = JRequest::getInt('cardnum', 0);
		$compid = JRequest::getInt('compid', 0);
		//проверка прав
		$agent = $this->getAgent(false,false);
		if(!$agent) return JText::_('COM_STTNMLS_ACCESSEDIT');
		$user = JFactory::getUser();
		if($agent->COMPID!=$compid && !$user->authorise('core.admin', 'com_sttnmls')) return JText::_('COM_STTNMLS_ACCESSEDIT');
		$db = $this->getDbo();
		switch ($viewName) {
			case 'apart':
				$tbl='#__sttnmlsaparts';
				break;
			case 'building':
					$tbl='#__sttnmlsbuildings';
					break;
			case 'com':
				$tbl='#__sttnmlscommerc';
				break;
			case 'garage':
				$tbl='#__sttnmlsgarages';
				break;
			case 'house':
				$tbl='#__sttnmlshouses';
				break;
			default:
				$tbl='#__sttnmlsaparts';
				break;
		}
		$db->setQuery('select PICTURES, PICNAMES from '.$db->quoteName($tbl).' where CARDNUM='.$db->quote($cardnum).' and COMPID='.$db->quote($compid));
		$res=$db->loadObject();
		if($res) {
			$pics = explode(';', $res->PICTURES);
			$inpath = "images/sttnmls/";
		    $image_folder = JPATH_ROOT . '/' . $inpath;
			if (!is_dir($image_folder)) {
				mkdir($image_folder);
			}
			

			$pfg=Array();
			foreach($pics as $p)
			{
				if($pics[$flnum]==$p)
				{
					$pfg[]=$p;
				}
			}
			foreach($pics as $p)
			{
				if($pics[$flnum]!=$p)
				{
					$pfg[]=$p;
				}
			}
			$pcs = implode(';', $pfg);


			$db->setQuery('update '.$db->quoteName($tbl).' set PICTURES="'.$pcs.'" where CARDNUM='.$db->quote($cardnum).' and COMPID='.$db->quote($compid));
			$db->query();
			if($db->getErrorNum()) return $db->getErrorMsg();
		}
		return ''; //все хорошо
	}



	protected function sendmail()
	{
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

		$params = JComponentHelper::getParams('com_sttnmls');
        $compidsob = $params->get('compidsob', 0);
        $mailfrom = $app->getCfg('mailfrom');
        $fromname = $app->getCfg('fromname');


        //проверяем - от собственника ли объявление
		if($this->_item->compid != $compidsob) {
            return FALSE;
        }

		//рассылка уведомлений о добавлении объекта
		$subject = SttNmlsLocal::parseString(JText::_('COM_STTNMLS_EMAIL_ITEM_ADD_ADMIN_NOTIFICATION_SUBJECT'));

		$body = $this->getMailBody();
		if(!$body) {
            return;
        }

		// список модераторов, которые должны получить уведомление
		$query = $db->getQuery(TRUE);
		$query->select($db->quoteName('u.email'));
		$query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON ' . $db->quoteName('g.id') . '=' . $db->quoteName('a.grp'));
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON ' . $db->quoteName('u.id') . '=' . $db->quoteName('a.userid'));
		$query->where($db->quoteName('g.seecont') . '=' . $db->quote(1));
		$query->where($db->quoteName('a.nosend') . '=' . $db->quote(0));
		$query->where('TRIM(' . $db->quoteName('u.email') . ') <> ""');
		$db->setQuery($query);
		$emails_list = $db->loadColumn();

        // Список организаций, которые должны получить уведомление
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('EMAIL', 'email'));
        $query->from($db->quoteName('#__sttnmlsvocfirms'));
        $query->where('TRIM(' . $db->quoteName('EMAIL') . ') <> ""');
        $query->where($db->quoteName('sendmail') . '>' . $db->quote(0));
        $db->setQuery($query);
        $rows = $db->loadColumn();

        if($rows) {
            foreach($rows as $row) {
                $emails_list[$row] = 1;
            }
        }

        if($emails_list) {
            foreach($emails_list as $email => $value) {
                $mail = JFactory::getMailer();
                $mail->addRecipient($email);
                $mail->setSender(array($mailfrom, $fromname));
                $mail->setSubject($subject);
                $mail->setBody($body);
                $mail->isHtml(TRUE);
                $mail->Send();
            }
        }
	}

    /**
     * Возвращает тело сообщения для квартир и комнат. Для других объектов переопределяется в их моделях
     *
     * @return  string
     */
	protected function getMailBody()
	{
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

		// для неквартир эту функцию нужно переопределить в модели
		$stype = $app->input->get('stype', 0, 'uint');
		$what = $app->input->get('what', 0, 'uint');


		$query = $db->getQuery(TRUE);
        $query->select($db->quoteName('o') . '.*, ' . $db->quoteName('c.NAME', 'gorod'));
        $query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
        $query->select($db->quoteName('d8.NAME', 'mat') . ', ' . $db->quoteName('d4.NAME', 'tip'));
        $query->select('CONCAT(TRUNCATE(' . $db->quoteName('o.AAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.LAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.KAREA') . ', 0)) AS ' . $db->quoteName('area'));
        $query->from($db->quoteName('#__sttnmlsaparts', 'o'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('o.CITYID') . '=' . $db->quoteName('c.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('o.STREETID') . '=' . $db->quoteName('s.id'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd4') . ' ON ' . $db->quoteName('o.HTYPEID') . '=' . $db->quoteName('d4.id') . ' AND ' . $db->quoteName('d4.RAZDELID') . '=' . $db->quote(4));
        $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON ' . $db->quoteName('o.WMATERID') . '=' . $db->quoteName('d8.id') . ' AND ' . $db->quoteName('d8.RAZDELID') . '=' . $db->quote(8));
        $query->where($db->quoteName('o.COMPID') . '=' . $db->quote($this->_item->compid));
        $query->where($db->quoteName('o.CARDNUM') . '=' . $db->quote($this->_item->cardnum));
		$db->setQuery($query);
		$row = $db->loadObject();

		if(!$row) {
            return '';
        }

        // Подготовка массива значений для вставки в сообщение
        $region = $params->get('region', 'Курская область');
        $kursk = $params->get('kursk', 'Курск');
        $kurske = $params->get('kurske', 'Курске');
        $regione = $params->get('regione', 'Курской области');

        if($row->gorod == $kursk) {
            $data['gorode'] = $kurske;
        } else if($row->gorod == $region) {
            $data['gorode'] = $regione;
        } else {
            $data['gorode'] = $row->gorod;
        }

        foreach($row as $key => $value) {
            $data[$key] = $value;
        }

        $data['price'] = number_format($row->PRICE/1000,3, ' ', ' ');
        $data['street'] = $row->ulica . (($row->orientir) ? ', ' . $row->orientir : '');
        $data['object_link'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&type=card&cn=' . $this->_item->cardnum . '&cid=' . $this->_item->compid);

        return SttNmlsLocal::parseString(JText::_('COM_STTNMLS_EMAIL_ITEM_ADD_ADMIN_NOTIFICATION_BODY_APART_' . $what . '_' . $stype), $data);
	}
	
	
    /**
     * 
     * @return string
     */
    function getSimilarObjects()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        $result = array();
        $_isArchiveMode = JFactory::getApplication()->input->get('archive', 0, 'uint');

        if(!$_isArchiveMode) {
            //поиск похожих объектов
            $count = 4;
            $db = $this->getDbo();
            $typeobj=1;
            if($this->_tabname == '#__sttnmlscommerc' OR $this->_tabname == '#__sttnmlscommerc_archive') $typeobj=2;
            if($this->_tabname=='#__sttnmlsgarages' OR $this->_tabname == '#__sttnmlsgarages_archive') $typeobj=3;
            if($this->_tabname=='#__sttnmlshouses' OR $this->_tabname == '#__sttnmlshouses_archive') $typeobj=4;
            $arObj = array();
            $arLink = array();
            $viewName='apart';

            // для квартир
            if($typeobj == 1) {
                $streetid = $this->_item->STREETID;
                // по числу комнат и улице
                $this->addarObj($arObj, ' WHAT='.$db->quote($this->_item->WHAT).' AND STREETID='.$db->quote($streetid).' AND ROOMS = '.$db->quote($this->_item->ROOMS),' ord ', $count, ', ABS(PRICE-'.(int)$this->_item->PRICE.') AS ord ' );
                // по числу комнат и в м-р и этажность
                $this->addarObj($arObj, ' WHAT='.$db->quote($this->_item->WHAT).' AND STREETID<>'.$db->quote($streetid).' AND MRAIONID='.$db->quote($this->_item->MRAIONID).' AND ROOMS = '.$db->quote($this->_item->ROOMS).' AND HSTAGE = '.$db->quote($this->_item->HSTAGE),' ord ', $count, ', ABS(PRICE-'.(int)$this->_item->PRICE.') AS ord ' );
                // по числу комнат и в р-не и этажность
                $this->addarObj($arObj, ' WHAT='.$db->quote($this->_item->WHAT).' AND MRAIONID<>'.$db->quote($this->_item->MRAIONID).' AND RAIONID='.$db->quote($this->_item->RAIONID).' AND ROOMS = '.$db->quote($this->_item->ROOMS).' AND HSTAGE = '.$db->quote($this->_item->HSTAGE),' ord ', $count, ', ABS(PRICE-'.(int)$this->_item->PRICE.') AS ord ' );
                // по числу комнат в городе и этажность
                $this->addarObj($arObj, ' WHAT='.$db->quote($this->_item->WHAT).' AND RAIONID<>'.$db->quote($this->_item->RAIONID).' AND ROOMS = '.$db->quote($this->_item->ROOMS).' AND CITYID = '.$db->quote($this->_item->CITYID),' ord ', $count, ', ABS(PRICE-'.(int)$this->_item->PRICE.') AS ord ' );
                require_once JPATH_COMPONENT.'/models/apart.php';
                $model = new SttNmlsModelApart();
            }

            // для ком. недвиж
            if($typeobj == 2) {
                $sq1 = round(0.8 * $this->_item->SQUEAR);
                $sq2 = round(1.2 * $this->_item->SQUEAR);
                // по типу предложения и по площади и улице
                $this->addarObj($arObj,' USAGEID='.$db->quote($this->_item->USAGEID).' and `SQUEAR`>='.$db->quote($sq1).' and `SQUEAR`<='.$db->quote($sq2).' and STREETID='.$db->quote($streetid),' DATEINP DESC ', $count);
                // по типу предложения и по площади и в м-р
                $this->addarObj($arObj,' USAGEID='.$db->quote($this->_item->USAGEID).' and `SQUEAR`>='.$db->quote($sq1).' and `SQUEAR`<='.$db->quote($sq2).' and STREETID<>'.$db->quote($streetid).' and MRAIONID='.$db->quote($this->_item->MRAIONID),' DATEINP DESC ', $count);
                // по типу предложения и по площади и в р-не
                $this->addarObj($arObj,' USAGEID='.$db->quote($this->_item->USAGEID).' and `SQUEAR`>='.$db->quote($sq1).' and `SQUEAR`<='.$db->quote($sq2).' and MRAIONID<>'.$db->quote($this->_item->MRAIONID).' and RAIONID='.$db->quote($this->_item->RAIONID),' DATEINP DESC ', $count);
                // по типу предложения и по площади в городе
                $this->addarObj($arObj,' USAGEID='.$db->quote($this->_item->USAGEID).' and `SQUEAR`>='.$db->quote($sq1).' and `SQUEAR`<='.$db->quote($sq2).' and RAIONID<>'.$db->quote($this->_item->RAIONID).' and CITYID='.$db->quote($this->_item->CITYID),' DATEINP DESC ', $count);
                require_once JPATH_COMPONENT.'/models/com.php';
                $model = new SttNmlsModelCom();
                $viewName='com';
            }

            // для гаражей
            if($typeobj == 3) {
                // поиск по м-р
                $this->addarObj($arObj,' MRAIONID='.$db->quote($this->_item->MRAIONID),' DATEINP DESC ', $count);
                // поиск по р-ну
                $this->addarObj($arObj,' MRAIONID<>'.$db->quote($this->_item->MRAIONID).' and RAIONID='.$db->quote($this->_item->RAIONID),' DATEINP DESC ', $count);
                // поиск по городу
                $this->addarObj($arObj,' RAIONID<>'.$db->quote($this->_item->RAIONID).' and CITYID='.$db->quote($this->_item->CITYID),' DATEINP DESC ', $count);
                require_once JPATH_COMPONENT.'/models/garage.php';
                $model = new SttNmlsModelGarage();
                $viewName='garage';
            }

            // для домов и участков
            if($typeobj == 4) {
                // для участков - площадь участка
                if($this->_item->WHAT==1) {
                    $this->addarObj($arObj,'WHAT='.$db->quote($this->_item->WHAT),' ord ', $count, ', ABS(EAREA-'.(int)$this->_item->EAREA.') as ord ');
                }
                // для домов - по цене
                if($this->_item->WHAT==0) {
                    $this->addarObj($arObj,'WHAT='.$db->quote($this->_item->WHAT),' ord ', $count, ', ABS(PRICE-'.(int)$this->_item->PRICE.') as ord ' );
                }
                require_once JPATH_COMPONENT.'/models/house.php';
                $model = new SttNmlsModelHouse();
                $viewName='house';
            }



            foreach ($arObj as $value) {
                $o = new stdClass();
                $o->item = $model->getItemC($value->COMPID, $value->CARDNUM);
                $o->desc = htmlspecialchars($model->getDesc());
                $o->pics = explode(';', $o->item->PICTURES);
                $o->linkimg = SttNmlsHelper::getLinkPhoto($o->pics[0], 'big');
                $o->url = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName, '&view=' . $viewName . '&type=card&cn=' . $value->CARDNUM . '&cid=' . $value->COMPID);
                $o->desc = $model->getDesc2();

                $result[] = $o;
            }
        }
        return $result;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function addarObj(&$arObj, $where, $order, $count, $select='')
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

		// добавление объектов в массив похожих объектов
		if(count($arObj) >= $count) {
            return FALSE;
        }

		$days = $params->get('days', '30');

		$query = $db->getQuery(true);
        $query->select($db->quoteName('a') . '.*' . $select);
        $query->from($db->quoteName($this->_tabname, 'a'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'ag') . ' ON ' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('a.COMPID') . ' AND ' . $db->quoteName('ag.id') . '=' . $db->quoteName('a.AGENTID'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON ' . $db->quoteName('g.id') . '=' . $db->quoteName('ag.grp'));
		$query->where('a.flag<3 AND ' . $db->quoteName('a.close') . '=' . $db->quote(0));
		$query->where('a.VARIANT=' . $db->quote($this->_item->VARIANT));
		$query->where('ag.flag<3');
		$query->where('(a.CARDNUM<>' . $db->quote($this->_item->CARDNUM) . ' or a.COMPID<>' . $db->quote($this->_item->COMPID) . ') ');
		$query->where($where);
//		$query->where(' ( (IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
		$query->order($order);
		$sql = (string) $query;
		$db->setQuery($query,0,$count);
		$res = $db->loadObjectList();                
		foreach ($res as $value) {
			$arObj[] = $value;
			if(count($arObj) >= $count)	break;
		}
		return;
	}

    public function setObjectCheckState()
    {
        $_allow_change_state = FALSE;

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $component_params = JComponentHelper::getParams('com_sttnmls');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');

        // VARs from request
        $view = $app->input->getString('view', 'apart');
        $cid = $app->input->getUint('cid', 0);
        $cn = $app->input->getUint('cn', 0);
        $current_state = $app->input->getBool('state', FALSE);


        // Проверка прав на выполнение операции
        if($_is_admin OR ($component_params->get('object_moderation.moderator', 0) > 0 && $user->id == $component_params->get('object_moderation.moderator', 0))) {
            $_allow_change_state = TRUE;
        }

        if(!$_allow_change_state) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }

        // Проверка таблицы
        if(!isset(self::$VIEWS_TABLE[$view])) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_UNKNOWN_OBJECT_TYPE');
            return FALSE;
        }

        // Изменение состояния проверки объекта
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName(self::$VIEWS_TABLE[$view]));
        if($current_state) {
            $query->set($db->quoteName('date_checked_to') . '=' . $db->quote('0000-00-00'));
        } else {
            $query->set($db->quoteName('date_checked_to') . '= DATE_ADD(NOW(), INTERVAL ' . $component_params->get('object_moderation.check_period', 14) . ' DAY)');
        }
        $query->set($db->quoteName('checked_by') . '=' . $db->quote($user->id));
        $query->set($db->quoteName('date_check') . '= NOW()');
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($cid));
        $query->where($db->quoteName('CARDNUM') . '=' . $db->quote($cn));
        $db->setQuery($query);

        if(!$db->execute()) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SAVE_DATA');
            return FALSE;
        }

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_SAVED');
        return TRUE;
    }
	
}
