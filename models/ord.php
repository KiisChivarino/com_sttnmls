<?php
defined('_JEXEC') or die('Restricted access');

class SttNmlsModelOrd extends JModelLegacy
{
	protected $_events = array();
	protected $_errors = array();

	public $_item;

	/**
	 * Консолидирует ошибки
	 *
	 * @param   string  $error_msg_alias - алиас ошибки из языкового файла
	 * @return
	 */
	protected function raiseEx()
	{
		$args = func_get_args();
		$error_msg_alias = array_shift($args);
		$app = JFactory::getApplication();

		if(count($args) > 0) {
			$this->_errors[] = vsprintf(JText::_($error_msg_alias), $args);
			$app->enqueueMessage(vsprintf(JText::_($error_msg_alias), $args), 'error');
		} else {
			$this->_errors[] = JText::_($error_msg_alias);
			$app->enqueueMessage(JText::_($error_msg_alias), 'error');
		}
	}

	/**
	 * Консолидирует события
	 *
	 * @param   string  $event_msg_alias - алиас ошибки из языкового файла
	 * @return
	 */
	protected function raiseEvent()
	{
		$args = func_get_args();
		$event_msg_alias = array_shift($args);
		$app = JFactory::getApplication();

		if(count($args) > 0) {
			$this->_events[] = vsprintf(JText::_($event_msg_alias), $args);
			$app->enqueueMessage(vsprintf(JText::_($event_msg_alias), $args));
		} else {
			$this->_events[] = JText::_($event_msg_alias);
			$app->enqueueMessage(JText::_($event_msg_alias));
		}
	}

	public function getEvents()
	{
		return $this->_events;
	}

	public function getErrors()
	{
		return $this->_errors;
	}

	function save()
	{
		// Хелпер аплоадера картинок
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php');

		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
		$params = JComponentHelper::getParams('com_sttnmls');
		$user = JFactory::getUser();

		// Request VARS
		$data = $app->input->post->get('jform', array(), 'array');
		$_delivery = $app->input->post->getBool('delivery', FALSE);

		// Other VARS
		$_isOwner = FALSE;
		$_isNewOrder = TRUE;
		$fromname = $app->getCfg('fromname');
		$mailfrom = $app->getCfg('mailfrom');
		$compidsob = $params->get('compidsob', 0);
		$delivery_price = $params->get('deliveryprice', 0);
		$redirect_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'ords');

		// Проверка логина пользователя
		if(!$user->id) {
			//> Не вошел
			$this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
			return FALSE;
		}

		// Получение информации об агенте
		$current_agent = $mAgent->getAgentInfoByID();
		if(!$current_agent) {
			//> Профиль не найден - ошибка
			$this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AGENT_NOT_FOUND');
			return FALSE;
		}

		if($compidsob) {
			$_isOwner = ($current_agent->COMPID == $compidsob);
		} else {
			$_isOwner = TRUE;
		}

		$_isNewOrder = ((isset($data['id']) && $data['id']) ? FALSE : TRUE);

		$query = $db->getQuery(TRUE);
		if($_isNewOrder) {
			$query->insert($db->quoteName('#__sttnmlszayavka'));
			$query->set($db->quoteName('dateinp') . '=NOW()');
		} else {
			$query->update($db->quoteName('#__sttnmlszayavka'));
			$query->where($db->quoteName('id') . '=' . $db->quote($data['id']));
		}

		$data['descr'] = htmlspecialchars($data['descr']);

		foreach($data as $key => $val) {
			$query->set($db->quoteName($key) . '=' . $db->quote($val));
		}

		$query->set($db->quoteName('userid') . '=' . $db->quote($user->id));
		$db->setQuery($query);
		$db->execute();

		$data['id'] = (($_isNewOrder) ? $db->insertid() : $data['id']);

		// Обработка новой заявки (опция "Разослать уведомления") -> платная рассылка
		if($_isNewOrder && $_delivery) {
			// Получение баланса агента
			$current_agent_balance = $mAgent->getAgentBalanceInfo();

			if($current_agent_balance >= $delivery_price) {

				//> Попытка отправить почту
				try {
					$subject = JText::sprintf('COM_STTNMLS_EMAIL_ORDER_NEW_PAY_SUBJECT', $fromname, JText::_('COM_STTNMLS_ORDS_VARIANT' . $data['variant'] . $data['typeobj']), JText::_('COM_STTNMLS_ORD_SOB' . (($_isOwner) ? '1' : '2')));

					$params = array(
						$fromname,
						'<h2><a href="' . $redirect_link . '">' . JText::_('COM_STTNMLS_ORDS_VARIANT' . $data['variant'] . $data['typeobj']) . ' (' . JText::sprintf('COM_STTNMLS_ORD_NUM', $data['id']) . ')</a></h2>',
						$data['descr'],
						$current_agent->NAME . ' ' . $current_agent->SIRNAME . ', ' . JText::_('COM_STTNMLS_ORD_SOB' . (($_isOwner) ? '1' : '2')),
						$current_agent->PHONE,
						$current_agent->EMAIL,
						SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&opentab=tab-contacts', 'profile')
					);
					$body = vsprintf(JText::_('COM_STTNMLS_EMAIL_ORDER_NEW_BODY'), $params);

					$query = $db->getQuery(TRUE);
					$query->select($db->quoteName('EMAIL'));
					$query->from($db->quoteName('#__sttnmlsvocagents'));
					$query->where('TRIM(' . $db->quoteName('EMAIL') . ') <> ""');
					$query->where($db->quoteName('COMPID') . '<>' . $compidsob);
					$query->where($db->quoteName('nosend') . '=' . $db->quote(0));
					$db->setQuery($query);
					$agent_emails_list = $db->loadObjectList();

					if($agent_emails_list) {
						foreach($agent_emails_list as $row) {
							if($row->EMAIL != '') {
								$mail = JFactory::getMailer();
								$mail->IsHTML();
								$mail->addRecipient($row->EMAIL);
								$mail->setSender(array($mailfrom, $fromname));
								$mail->setSubject($subject);
								$mail->setBody($body);
								$sent = $mail->Send();
							}
						}
					}
				} catch(Exception $e) {
					$this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SEND_MAIL');
					return FALSE;
				}

				SttNmlsPlatusl::changeBalance($user->id, $delivery_price, 'Списание за рассылку заявки по агентам');
			}
		}


		if($_isNewOrder) {
			//> Отправка почты для организаций
			try {
				$subject = JText::sprintf('COM_STTNMLS_EMAIL_ORDER_NEW_SUBJECT', $fromname, JText::_('COM_STTNMLS_ORDS_VARIANT' . $data['variant'] . $data['typeobj']), JText::_('COM_STTNMLS_ORD_SOB' . (($_isOwner) ? '1' : '2')));

				$params = array(
					$fromname,
					'<h2><a href="' . $redirect_link . '">' . JText::_('COM_STTNMLS_ORDS_VARIANT' . $data['variant'] . $data['typeobj']) . ' (' . JText::sprintf('COM_STTNMLS_ORD_NUM', $data['id']) . ')</a></h2>',
					$data['descr'],
					$current_agent->NAME . ' ' . $current_agent->SIRNAME . ', ' . JText::_('COM_STTNMLS_ORD_SOB' . (($_isOwner) ? '1' : '2')),
					$current_agent->PHONE,
					$current_agent->EMAIL,
					SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&opentab=tab-contacts', 'profile')
				);
				$body = vsprintf(JText::_('COM_STTNMLS_EMAIL_ORDER_NEW_BODY'), $params);

				$query = $db->getQuery(TRUE);
				$query->select($db->quoteName('EMAIL'));
				$query->from($db->quoteName('#__sttnmlsvocfirms'));
				$query->where('TRIM(' . $db->quoteName('EMAIL') . ') <> ""');
				$query->where($db->quoteName('active') . '=' . $db->quote(1));
				$query->where($db->quoteName('flag') . '< 3');
				$query->where($db->quoteName('sendmail') . ' IN (' . (($_isOwner) ? '1,3' : '2,3') . ')');
				$db->setQuery($query);
				$firms_emails_list = $db->loadObjectList();

				if($firms_emails_list) {
					foreach($firms_emails_list as $row) {
						if($row->EMAIL != '') {
							$mail = JFactory::getMailer();
							$mail->IsHTML();
							$mail->addRecipient($row->EMAIL);
							$mail->setSender(array($mailfrom, $fromname));
							$mail->setSubject($subject);
							$mail->setBody($body);
							$sent = $mail->Send();
						}
					}
				}

			} catch(Exception $e) {
				$this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SEND_MAIL');
				return FALSE;
			}
		}

		$this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_ORDER_SEND_SUCCESSFULL');
		return TRUE;
	}



	public function getItem()
	{
		$ord = JRequest::getInt('id',0);
		if(!$ord) return null;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
        $query->select('f.* ');
        $query->from(' #__sttnmlszayavka AS f ');
		$query->where(' f.id='.$db->quote($cid));
		$db->setQuery($query);
		$this->_item=$db->loadObject();
		return $this->_item;
	}

	
	function delete(){
		$user = JFactory::getUser();
		$userid = $user->get('id');
		if(!$userid) {
			$this->setError(JText::_('COM_STTNMLS_ERROR_USER'));
			return;
		}
		$id = JRequest::getInt('cid', 0);
		if(!$id) return;
		$db = $this->getDbo();
		$db->setQuery('select userid from #__sttnmlszayavka where id='.$db->quote($id));
		$res = $db->loadResult();
		if(!$res) return;
		if($userid==$res) {
			$db->setQuery('delete from #__sttnmlszayavka where id='.$db->quote($id));
			$db->execute();
		}
	}
}
