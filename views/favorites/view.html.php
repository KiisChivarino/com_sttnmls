<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewFavorites extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->items = $this->get('ObjectsList');
        $this->agent_model = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $this->houses_model = JModelLegacy::getInstance('houses', 'SttnmlsModel');
        $this->coms_model = JModelLegacy::getInstance('coms', 'SttnmlsModel');
        $this->garages_model = JModelLegacy::getInstance('garages', 'SttnmlsModel');
        $this->tp = JRequest::getInt('tp', 0);
        $this->typepdf = JFactory::getApplication()->input->get('typepdf', '', 'string');
        $this->print = JRequest::getInt('print', 0);

        parent::display($tpl);
    }
}
