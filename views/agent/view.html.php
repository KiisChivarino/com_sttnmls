<?php
defined('_JEXEC') or die ('Restricted access');

require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';
require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';

class SttNmlsViewAgent extends JViewLegacy
{
    function display($tpl = null) 
    {
        // JOOMLA Instances
        $this->app = JFactory::getApplication();
        $this->doc = JFactory::getDocument();
        $this->user = JFactory::getUser();
		$this->params = JComponentHelper::getParams('com_sttnmls');
        $mAgent = $this->getModel('agent');

        // VARS
        $this->layout = $this->app->input->getString('layout', 'default');


        $this->item = $this->get('Item');
        if(!$this->item) {
            header("HTTP/1.0 404 Not Found");
            $this->doc->setTitle("Этого агента нет в базе");
        }

        if($this->app->input->get('usloff', 0, 'uint')) {
            // @TODO: Что за херня? Непонятная переменная. Видимо сязано с платными услугами
            SttNmlsPlatusl::delete($this->app->input->get('usloff', 0, 'uint'));
        }


        switch($this->layout) {

            case 'profile':
                    $this->agent_tariff = $this->get('AgentTariff');
                    $this->tariffs_list = $this->get('TariffsList');
                    $this->balans = $this->get('AgentBalanceInfo');
                break;
        }
		
		
        $this->realtor = $this->get('Profile');
        $this->avatar = $this->get('ComUserAvatar');
        $this->firms = $this->get('Firms');
        $this->firm_agents = $this->getModel('agent')->getFirmAgentsList($this->item->COMPID);
        $this->current_agent = $mAgent->getAgentInfoByID();
        $this->isAdmin = $this->user->authorise('core.admin');
        $this->canViewExpired = (($this->current_agent->COMPID == $this->item->COMPID) OR $this->isAdmin);
        $this->canViewClosed = (($this->current_agent->COMPID == $this->item->COMPID) OR $this->isAdmin);
		$this->canViewRealtorsBase = ($this->current_agent->COMPID!=$this->params->get('compidsob','0') and !$this->user->guest);
        $this->errors = $this->get('Errors');
        
        $this->doc->setTitle($this->get('Title'));
        $this->doc->setDescription($this->get('Desc'));
        
        
        parent::display($tpl);
    }
    
    public function getSocialHref($href = '', $social = '') 
    {
        $myString = 'https://'.$social.'/';
        
        //регулярка: должен быть домен из массива доменов, может быть протокол
        $checkForEreg = "(^(http(s)?://)?$social/)|(/)";
        $checkForPreg= "/(^(http(s)?\:\/\/)?$social\/)|(^\/)/";
        
        $socialName = array('vk.com','ok.ru','twitter.com','facebook.com','instagram.com');
        if (!(ereg("[^ ]", $href) or $href)) //проверка на пробелы и пустоту
            return false; 
        else{
            if (in_array($social,$socialName) and ereg($checkForEreg, $href)){ //проверка на внимательность в шаблоне
                return (preg_replace("$checkForPreg", $myString, $href)); //если совпало с регуляркой, меняем ее на свою строку
            }else{
                return($myString.$href); //иначе тупо добавляем свою строку к пользовательской строке
            }
        }
    }
}
