<?php defined('_JEXEC') or die('Restricted access');

class SttnmlsImageObject
{
    public $original_filename = '';
    public $output_filename = '';
    public $output_raw_filename = '';
    public $full_store_path = '';
    public $url_path = '';
    public $width = 0;
    public $height = 0;
    public $alt = '';
    public $title = '';
    public $thumb_output_filename = '';
    public $thumb_full_store_path = '';
    public $thumb_url_path = '';
    public $thumb_width = 0;
    public $thumb_height = 0;
}

class SttnmlsHelperImage
{
    /**
     * @var     self        Инстанс этого класса
     */
    private static $instance = NULL;

    /**
     * @var     array       Идентификаторы картинок
     */
    private static $IMAGE_FILE_EXT = array(
                        1       =>  '.gif',
                        2       =>  '.jpg',
                        3       =>  '.png'
                    );


    /**
     * @var     bool        Использовать префикс в имени файла
     */
    protected $use_prefix_as_imagename = true;

    /**
     * @var     string      Префикс в названии файла изображения
     */
    protected $prefix_name = '';

    /**
     * @var     string      Префикс в названии файла миниатюры изображения
     */
    protected $thumb_prefix_name = 'thumb_';


    /**
     * @var     bool        Создать миниатюру изображения
     */
    protected $create_thumb = false;


    /**
     * @var     string      Относительный путь для хранения изображения
     */
    protected $store_path = 'images/';

    /**
     * @var     string      Относительный путь для хранения миниатюр изображений
     */
    protected $thumb_store_path = 'images/';

    /**
     * @var     int         Ширина изображения
     */
    protected $width = 640;

    /**
     * @var     int         Минимальная ширина изображения
     */
    protected $min_width = 250;

    /**
     * @var     int         Ширина миниатюры изображения
     */
    protected $thumb_width = 64;

    /**
     * @var     int         Высота изображения
     */
    protected $height = 480;

    /**
     * @var     int         Минимальная высота изображения
     */
    protected $min_height = 250;

    /**
     * @var     int         Высота миниатюры изображения
     */
    protected $thumb_height = 64;

    /**
     * @var     array       Допустимые типы изображений для загрузки
     */
    protected $types = array(2);            // 1 -gif, 2 - jpg, 3 - png

    /**
     * @var     int         Максимальный размер в байтах изображения для загрузки
     */
    protected $max_file_size = 0;           // 5мб

    /**
     * @var     bool        Выполнять масштабирование изображения
     */
    protected $resize = false;

    /**
     * @var     int         Метод масштабирования картинки
     */
    protected $resize_method = JImage::SCALE_INSIDE;

    private $_errors = array();

    /**
     * Конструктор недоступен извне
     */
    private function __construct()
    {
    }

    /**
     * Клонировать класс запрещено
     */
    private function __clone()
    {
    }

    /**
     * Получение экземпляра класса
     *
     * @return      SttnmlsHelperImage
     */
    public static function getInstance()
    {
        if(NULL === self::$instance) {
            self::$instance = new self();
        }
        self::$instance->max_file_size = JComponentHelper::getParams('com_sttnmls')->get('max_upload_image_file_size', 8192) * 1024;

        return self::$instance;
    }

    /**
     * Установка свойств класса
     *
     * @since       3.9.0
     * @param       array                   $config             Массив параметров
     */
    public function setConfig($config = array())
    {
        if($config && count($config) > 0) {
            foreach ($config as $var => $val) {
                if(isset($this->$var)) {
                    $this->$var = $val;
                }
            }
        }
    }

    /**
     * Стандартный сеттер
     *
     * @since       3.9.0
     * @param       string                  $name               Название свойства
     * @param       mixed                   $value              Значение свойства
     */
    public function __set($name, $value) {
        $this->$name = $value;
    }

    /**
     * Стандартный геттер
     *
     * @since       3.9.0
     * @param       string                  $name               Название свойства
     * @return      mixed
     */
    public function __get($name) {
        return $this->$name;
    }


    /**
     * Добавлет текст ошибки в массив ошибок
     *
     * @since       3.9.0
     * @param       string      $message        Текст ошибки
     */
    private function setError($message)
    {
        $this->_errors[] = $message;
    }

    /**
     * Возвращает последнюю добавленную ошибку
     *
     * @since       3.9.0
     * @return      mixed
     */
    public function getError()
    {
        return array_pop($this->_errors);
    }

    /**
     * Возвращает массив всех ошибок
     *
     * @since       3.9.0
     * @return      array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * Делает выразание миниатюры из основного аватара
     *
     * @param       string          $src_filename       Имя файла картинки аватара
     * @param       int             $x                  Координата левого верхнего угла выделения по оси Х
     * @param       int             $y                  Координата левого верхнего угла выделения по оси Y
     * @param       int             $width              Ширина выделения
     * @param       int             $height             Высота выделения
     * @return      bool
     */
    public function cropResizeAvatarImage($src_filename, $x, $y, $width, $height)
    {
        $result = array();
        $u = JFactory::getUser();

        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->setError(JText::sprintf('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD'));
            return false;
        }

        // Грузят только залогиненные пользователи
        if(!$u->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_NOT_LOGGED_IN'));
            return false;
        }

        if(!file_exists($src_filename)) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_THUMBNAIL_AVATAR_NOT_EXIST'));
            return false;
        }


        // Проверка директории хранения файла
        $image_full_path = JPATH_ROOT . '/' . $this->store_path;
        if(!is_dir($image_full_path)) {
            // Попытка создания директориии
            try {
                mkdir($image_full_path);
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        // Проверка директории хранения миниатюр, если миниатюра должна быть создана
        $thumb_image_full_path = JPATH_ROOT . '/' . $this->thumb_store_path;
        if($this->create_thumb && !is_dir($thumb_image_full_path)) {
            // Попытка создание директории хранения миниатюр
            try {
                mkdir($thumb_image_full_path);
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        // Подготовка имени файла и пути
        if($this->use_prefix_as_imagename && $this->prefix_name != '') {
            $store_image_name = $this->prefix_name . '.jpg';
            $thumb_store_image_name = $this->thumb_prefix_name . '.jpg';
        } else {
            $raw_image_name = md5(uniqid() . time() . rand(0, 10000));
            $store_image_name = (($this->prefix_name) ? $this->prefix_name . '_' : '') . $raw_image_name . '.jpg';
            $thumb_store_image_name = (($this->thumb_prefix_name) ? $this->thumb_prefix_name : 'thumb_') . $raw_image_name . '.jpg';
        }

        // Удаление существующего файла картинки
        $full_store_path = $image_full_path . $store_image_name;
        if(file_exists($full_store_path)) {
            try {
                unlink($full_store_path);
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        // Удаление существующего файла миниатюры
        $thumb_full_store_path = $thumb_image_full_path . $thumb_store_image_name;
        if(file_exists($thumb_full_store_path)) {
            try {
                unlink($thumb_full_store_path);
            } catch (Exception $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        try {
            // Обработка картинки
            $image = new JImage($src_filename);

            $image->crop($width, $height, $x, $y, false);
            if($this->resize) {
                $image->resize($this->width, $this->height, false, $this->resize_method);
            }
            $image->toFile($full_store_path);

            // Создание миниатюры, если это требуется
            if($this->create_thumb) {
                $thumbs = $image->generateThumbs(
                    array(
                        $this->thumb_width . 'x' . $this->thumb_height
                    ),
                    JImage::CROP_RESIZE
                );
                $thumbs[0]->toFile($thumb_full_store_path);
            }

            return true;
        } catch(Exception $e) {
            $this->serError($e->getMessage());
        }
        return false;
    }
    
    /**
     * Осуществляет загрузку картинок
     *
     * @since       3.9.0
     * @param       string                  $block          Название инпута из которого загружается картинка
     * @return      bool|SttnmlsImageObject
     */
    public function upload($block = 'files')
    {
        $result = array();
        $u = JFactory::getUser();

        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->setError(JText::sprintf('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD'));
            return false;
        }

        // Грузят только залогиненные пользователи
        if(!$u->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_NOT_LOGGED_IN'));
            return false;
        }


        // Проверка передачи файлов --------------------------------------------
        if(!$_FILES OR !isset($_FILES[$block]) OR !isset($_FILES[$block]['name'])) {
        // Файлы не передавались
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_FILES_UPLOADED'));
            return false;
        }
        $images_count = count($_FILES[$block]['name']);

        // Проверка директории хранения файла
        $image_full_path = JPATH_ROOT . '/' . $this->store_path;
        if(!is_dir($image_full_path)) {
            // Попытка создания директориии
            try {
                mkdir($image_full_path);
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        // Проверка директории хранения миниатюр, если миниатюра должна быть создана
        $thumb_image_full_path = JPATH_ROOT . '/' . $this->thumb_store_path;
        if($this->create_thumb && !is_dir($thumb_image_full_path)) {
            // Попытка создание директории хранения миниатюр
            try {
                mkdir($thumb_image_full_path);
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }

        for($i = 0; $i < $images_count; $i++)
        {
            try {
                $current_image_properties = JImage::getImageFileProperties($_FILES[$block]['tmp_name'][$i]);
                $current_file_size = $current_image_properties->filesize;

                // Проверка размера файла
                if($current_file_size > $this->max_file_size) {
                    $this->setError(
                        SttnmlsHelper::parseString(
                            'COM_STTNMLS_MESSAGE_ERROR_FILE_TOO_BIG',
                            array(
                                'filename'      =>      $_FILES[$block]['name'][$i],
                                'max_file_size' =>      round($this->max_file_size / 1024)
                            ),
                            array(),
                            true
                        )
                    );
                    continue;
                }

                // Подготовка имени файла и пути
                if($this->use_prefix_as_imagename && $images_count == 1 && $this->prefix_name != '') {
                    $store_image_name = $this->prefix_name . '.jpg';
                    $thumb_store_image_name = $this->thumb_prefix_name . '.jpg';
                } else {
                    $raw_image_name = md5(uniqid() . time() . rand(0, 10000));
                    $store_image_name = (($this->prefix_name) ? $this->prefix_name . '_' : '') . $raw_image_name . '.jpg';
                    $thumb_store_image_name = (($this->thumb_prefix_name) ? $this->thumb_prefix_name : 'thumb_') . $raw_image_name . '.jpg';
                }

                // Удаление существующего файла картинки
                $full_store_path = $image_full_path . $store_image_name;
                if(file_exists($full_store_path)) {
                    try {
                        unlink($full_store_path);
                    } catch (RuntimeException $e) {
                        $this->setError($e->getMessage());
                        return false;
                    }
                }

                // Удаление существующего файла миниатюры
                $thumb_full_store_path = $thumb_image_full_path . $thumb_store_image_name;
                if(file_exists($thumb_full_store_path)) {
                    try {
                        unlink($thumb_full_store_path);
                    } catch (Exception $e) {
                        $this->setError($e->getMessage());
                        return false;
                    }
                }

                // Обработка картинки
                $upload = new JImage($_FILES[$block]['tmp_name'][$i]);


                // Масштабирование слишком мелких картинок
                if($current_image_properties->width < $this->min_width OR $current_image_properties->height < $this->min_height) {
                    $upload->resize($this->min_width, $this->min_height, false, $this->resize_method);
                }

                // Масштабирование, если размер превышает максимальную ширину или высоту если указан флаг принудительного ресайза
                if($this->resize OR ($current_image_properties->width > $this->width OR $current_image_properties->height > $this->height)) {
                    $upload->resize($this->width, $this->height, false, $this->resize_method);
                }
                $upload->toFile($full_store_path);

                // Создание миниатюры, если это требуется
                if($this->create_thumb) {
                    $thumbs = $upload->generateThumbs(
                        array(
                            $this->thumb_width . 'x' . $this->thumb_height
                        ),
                        JImage::CROP_RESIZE
                    );
                    $thumbs[0]->toFile($thumb_full_store_path);
                }
            } catch (Exception $e) {
                $this->setError($e->getMessage());
                return false;
            }


            $image_size = getimagesize($full_store_path);
            $thumb_image_size = (($this->create_thumb) ? getimagesize($thumb_full_store_path) : array());
                        
            $image_object = new SttnmlsImageObject();
            $image_object->original_filename = $_FILES[$block]['name'][$i];
            $image_object->output_filename = $store_image_name;
            $image_object->output_raw_filename = $raw_image_name;
            $image_object->thumb_output_filename = $thumb_store_image_name;
            $image_object->full_store_path = $full_store_path;
            $image_object->thumb_full_store_path = $thumb_full_store_path;
            $image_object->url_path = JURI::root() . $this->store_path . $store_image_name;
            $image_object->thumb_url_path = JURI::root() . $this->thumb_store_path . $thumb_store_image_name;
            $image_object->width = $image_size[0];
            $image_object->height = $image_size[1];
            $image_object->thumb_width = (($this->create_thumb) ? $thumb_image_size[0] : 0);
            $image_object->thumb_height = (($this->create_thumb) ? $thumb_image_size[1] : 0);
            
            $result[] = $image_object;
        }
        return $result;
    }

    /**
     * Функция поворота изображения на заданный угол
     *
     * @since       3.9.0
     * @param       string                  $src_image      Исходное изображение
     * @param       integer                 $angle          Угол поворота. отрицательный - вправо
     * @return      boolean
     */
    public function rotateImage($src_image, $angle = -90) 
    {
        $u = JFactory::getUser();

        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->setError(JText::sprintf('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD'));
            return false;
        }

        // Проверка установки библиотеки GD для PHP
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            $this->setError(JText::sprintf('COM_STTNMLS_MESSAGE_ERROR_LIB', 'GD'));
            return false;
        }
        
        // Проверка существования исходного файла ------------------------------
        if (!file_exists($src_image)) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_IMAGES'));
            return false;
        }

        try {
            $image = new JImage($src_image);
            $image->rotate($angle, -1, false);
            $image->toFile($src_image);
        } catch(Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }
        
        return true;
    }
    
}