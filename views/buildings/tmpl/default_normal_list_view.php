<?php defined('_JEXEC') or die('Restricted access'); 

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;
$sort_icon = ' <span class="glyphicon glyphicon-sort-by-attributes' . (($orderDirn == 'desc') ? '-alt' : '') . '"></span>';

?>

<div class="row-fluid" style="margin-bottom: 20px;">
    <h5><?php echo JText::_('COM_STTNMLS_LABEL_SORT_BY_FIELD') ?>:</h5>
    <ul class="nav nav-pills">
        <li role="presentation"<?php echo (($orderCol == 'a.DATEINP') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_DATE') . (($listOrder == 'a.DATEINP') ? $sort_icon : ''), 'a.DATEINP', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'PICCOUNT') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PHOTO') . (($listOrder == 'PICCOUNT') ? $sort_icon : ''), 'PICCOUNT', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'a.AAREA') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_SQUARE') . (($listOrder == 'a.AAREA') ? $sort_icon : ''), 'a.AAREA', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'a.PRICE') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PRICE') . (($listOrder == 'a.PRICE') ? $sort_icon : ''), 'a.PRICE', $listDirn, $listOrder); ?>
        </li>
    </ul>
</div>
<!-- Objects Container -->
<div class="clearfix objects-list">
<?php if($this->items) : ?>
    <?php $my_inc = 0; ?>
    <?php foreach ($this->items as $item) : ?>
    <?php
        $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&view=building&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->comp);
        $link_tab_aparts = SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&builder=' . $item->comp . '&building=' . $item->CARDNUM);
        $linkstreet = SttNmlsHelper::getSEFUrl('com_sttnmls', 'buildings', '&street_id=' . $item->STREETID);
        $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&layout=edit&cn=' . $item->CARDNUM . '&cid=' . $item->comp);
    ?>
    <div class="col-sm-6 col-md-4 object" id="<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>">
        <div class="thumbnail">
            <div class="object-photo text-center">
                <a href="<?php echo $link; ?>" target="_blank">
                <?php 
                    $pics = explode(';', $item->PICTURES);
                    $wm = '';
                    if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
                    $img_url = SttNmlsHelper::getLinkPhoto($pics[0], 'editfirst' . $wm);
                ?>
                    <img class="img-rounded" src="<?php echo $img_url; ?>" alt="" />
                </a>
                <a href="<?php echo $link ?>" class="btn btn-success btn-sm object-view-button" target="_self">
                    <span class="glyphicon glyphicon-search"></span>
                    <?php echo JText::_('COM_STTNMLS_LABEL_VIEW') ?>
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $link; ?>" class="objectdesc" target="_blank" title="<?php echo htmlspecialchars($item->title) ?>">
                    <?php echo $item->title ?>
                </a>
                <div class="object-public-info">
                    <p class="to-left">
                        <?php
                            $rooms_chars = array('', 'I', 'II', 'III', 'IV');
                            $item->datekvar = $rooms_chars[$item->datekvar];
                        ?>
                        <span class="glyphicon glyphicon-calendar"></span>
                        <?php printf('<b>%s кв. %d г.</b>', $item->datekvar, $item->dateyear) ?>
                    </p>&nbsp;
                    <p class="to-right">
                        <span class="glyphicon glyphicon-eye-open"></span>
                        <?php echo $item->watchcount; ?>
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="object-description">
                <?php
                    $s = $item->gorod;
                    if($item->raion != '' && $item->RAIONID>1) {
                        $r = str_replace('р-н', '', $item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($item->mraion != '') {
                        $s .= ', ' . $item->mraion;
                    }
                    echo $s;
                ?>
                <br />
                <strong><?php printf('От %d м<sup>2</sup> до %d м<sup>2</sup>', $item->areafrom, $item->areato) ?></strong>

                <?php if($item->aparts_counts) : ?>
                    <br />
                    <a href="<?php echo $link_tab_aparts ?>" target="_blank">
                        <?php echo JText::sprintf('COM_STTNMLS_LABEL_BUILDING_APARTS_COUNT', $item->aparts_counts) ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="object-price">
                <div class="price text-right clearfix">
                    <span class="value">
                        <?php printf('от %s за м<sup>2</sup>', number_format($item->pricefrom, 0, ' ', ' ')) ?>
                    </span>
                </div>
                <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                    <div class="picsmang" style="top:15px; width:100px; position:relative; left:-18px;">
                        <a href="<?php echo $linkedit; ?>" class="picedit" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>"></a>
                        <a class="picdelete" title="<?php echo JText::_('COM_STTNMLS_DELETE'); ?>" onclick="return deleteobj(<?php echo $item->CARDNUM; ?>, <?php echo $item->COMPID; ?>,'building');"></a>
                    </div>
                <?php endif; ?>
            </div>
            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="text-center edit-button-block">
                    <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-xs" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>


                    <a href="javascript:void(0);" class="btn btn-danger btn-xs removeObjectFromList" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="building">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
<?php else : ?>
    <?php if($this->noitemtext) : ?>
    <p class="text-center">
        <?php echo $this->noitemtext; ?>
    </p>
    <?php endif; ?>   
<?php endif; ?>
</div>
<!-- /Objects Container -->