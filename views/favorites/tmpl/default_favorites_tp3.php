<?php defined('_JEXEC') or die('Restricted access');

$params = JComponentHelper::getParams('com_sttnmls');

$agent = $this->agent_model->getAgentInfoByID();

// Информация о тарифе текущего пользователя
$agent_tariff = $this->agent_model->getAgentTariff();

?>

<h2 style="padding-bottom:0px; margin-bottom:10px; font-size:20px; color:#444;">Дома и участки</h2>
<table class="table" width="100%" style="margin-top:5px; font-size:12px;" cellpadding="5" cellspacing="0" border="0">
    <tr style="background:#333; color:#fff; text-align:center; font-weight:bold;">
    <?php if($this->typepdf != 'cli') : ?>
        <th>#</th>
    <?php endif; ?>
        <th>Адрес</th>
        <th>Тип</th>
        <th>Эт-ть<br />Матер</th>
        <th>Пл<br>дом/уч</th>
        <th>Вода</th>
        <th>Отопл</th>
        <th>Канал</th>
        <th>Крыша</th>
        <th>Г</th>
        <th>Эл</th>
        <th>Цена</th>
    <?php if($this->typepdf != 'cli') : ?>
        <th>Агент</th>
    <?php endif; ?>
    <?php if(!$this->print) : ?>
        <th></th>
    <?php endif; ?>
    </tr>
<?php foreach($this->items[3] as $item) : ?>
    <?php
        $dateinp = explode(" ", $item->DATEINP);
        $dateinp = explode("-", $dateinp[0]);
        $datecor = explode(" ", $item->DATECOR);
        $datecor = explode("-", $datecor[0]);

        $item->HSTAGE = (($item->HSTAGE) ? $item->HSTAGE . ' - ' : '');

        if($item->adph != '') {
            $item->adph = '<br />' . $item->adph;
        }

        $item->HSTAGE = ($item->HSTAGE != 0) ? $item->HSTAGE . ' - ' : '';

        if($item->adph != '') {
            $item->adph = ', ' . $item->adph;
        }

        $item->HCHECKS1 = trim($item->HCHECKS1);

        $gaz = "-";
        $el = "-";
        if($item->HCHECKS1)
        {
            $ch = explode("/", $item->HCHECKS1);
            for($y = 0; $y < count($ch); $y++)
            {
                if($ch[$y] == 'газ')
                {
                    $gaz = "+";
                }
                if($ch[$y] == '220В' OR $ch[$y] == '380В')
                {
                    $el = "+";
                }
            }
        }


        $ch = '';
        if($item->HCHECKS1 != '' && $item->HCHECKS1 != '/') {
            $item->HCHECKS1 .= "/";
            $item->HCHECKS1 = str_replace("//", "", $item->HCHECKS1);
            $item->HCHECKS1 = str_replace("/", " / ", $item->HCHECKS1);

            if($this->typepdf == 'cli')
            {
                $ch = '<div style="color:#444">' . $item->HCHECKS1 . '</div>';
            }else{
                $ch = '<div style="border-top:1px solid #bbb; padding-top:5px; margin-top:10px;">' . $item->HCHECKS1 . '</div>';
            }
        }

        $water = $this->houses_model->getOptionsByID($item->WATERID);
        if(!$water->SHORTNAME) {
            $water->SHORTNAME = 'Не указано';
        }

        $HEAT = $this->houses_model->getOptionsByID($item->HEATID);
        if(!$HEAT->SHORTNAME) {
            $HEAT->SHORTNAME = 'Не указано';
        }

        $WC = $this->houses_model->getOptionsByID($item->WCID);
        if(!$WC->SHORTNAME) {
            $WC->SHORTNAME = 'Не указано';
        }

        $roof = $this->houses_model->getOptionsByID($item->ROOFID);
        if(!$roof->SHORTNAME) {
            $roof->SHORTNAME = 'Не указано';
        }

        $f_show_contacts = (($item->flag == 2 && $item->mdays > $item->alldays && (!$agent_tariff OR $agent_tariff->seecont == 0)) ? FALSE : TRUE);
    ?>
    <tbody>
        <tr>
        <?php if($this->typepdf != 'cli') : ?>
            <td rowspan="2" style="width:150px; text-align:center; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;" valign="middle">
                <b><?php echo $item->firm ?></b>
                <br />(<?php echo $item->COMPID ?>)
                <br />
                <span style="padding:4px 0px; display:block">
                    <b>Объект #:</b>
                    <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'house', '&view=house&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID) ?>" target="_blank">
                        <?php echo $item->CARDNUM ?>
                    </a>
                </span>
                <span style="font-size:11px;">
                    <?php printf('%s.%s.%s&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;%s.%s.%s', $dateinp[2], $dateinp[1], $dateinp[0], $datecor[2], $datecor[1], $datecor[0]) ?>
                </span>
            </td>
        <?php endif; ?>

        <?php if($this->typepdf != 'cli') : ?>
            <td style="border-right:1px solid #666; border-bottom:1px solid #666; width:150px; text-align: center; vertical-align:middle !important;">
                <strong style="padding:0px; margin:0px;"><?php echo $item->ulica ?></strong>
            </td>
            <td style="border-right:1px solid #666; width:60px; text-align:center;">
                <?php echo $item->tip ?>
            </td>
            <td style="border-right:1px solid #666; width:60px; text-align:center;">
                <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
            </td>
            <td style="border-right:1px solid #666; width:40px; text-align:center;">
                <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $water->SHORTNAME ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $HEAT->SHORTNAME ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $WC->SHORTNAME ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $roof->SHORTNAME ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $gaz ?>
            </td>
            <td style="border-right:1px solid #666; width:30px; text-align:center;">
                <?php echo $el ?>
            </td>
            <td style="width:70px; text-align:center; width:100px; white-space:nowrap;">
                <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
            </td>
        <?php else : ?>
            <?php if($ch != '') : ?>
                <td style="width:150px; vertical-align:top !important;" valign="top">
                    <strong style="padding:0px; margin:0px;"><?php echo str_replace(',', ', ', $item->ulica)?></strong>
                </td>
                <td style="width:60px; text-align:center;">
                    <?php echo $item->tip ?>
                </td>
                <td style="width:60px; text-align:center;">
                    <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
                </td>
                <td style="width:40px; text-align:center;">
                    <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
                </td>
                <td style="width:30px; text-align:center;">
                    <?php echo $water->SHORTNAME ?>
                </td>
                <td style="width:30px; text-align:center;">
                    <?php echo $HEAT->SHORTNAME ?>
                </td>
                <td style="width:30px; text-align:center;">
                    <?php echo $WC->SHORTNAME ?>
                </td>
                <td style="width:30px; text-align:center;">
                    <?php echo $roof->SHORTNAME ?>
                </td>
                <td style="width:30px; text-align:center;">
                    <?php echo $gaz ?>
                </td>
                <td style="bwidth:30px; text-align:center;">
                    <?php echo $el ?>
                </td>
                <td style="width:70px; text-align:center; width:100px; white-space:nowrap;">
                    <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                </td>
            <?php else : ?>
                <td style="border-bottom:1px solid #666; width:150px; vertical-align:top !important;" valign="top">
                    <strong style="padding:0px; margin:0px;"><?php echo str_replace(',', ', ', $item->ulica)?></strong>
                </td>
                <td style="border-bottom:1px solid #666; width:60px; text-align:center;">
                    <?php echo $item->tip ?>
                </td>
                <td style="border-bottom:1px solid #666; width:60px; text-align:center;">
                    <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
                </td>
                <td style="border-bottom:1px solid #666; width:40px; text-align:center;">
                    <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $water->SHORTNAME ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $HEAT->SHORTNAME ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $WC->SHORTNAME ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $roof->SHORTNAME ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $gaz ?>
                </td>
                <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                    <?php echo $el ?>
                </td>
                <td style="border-bottom:1px solid #666; width:70px; text-align:center; width:100px; white-space:nowrap;">
                    <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                </td>
            <?php endif; ?>
        <?php endif; ?>

        <?php if($this->typepdf != 'cli') : ?>
            <?php $access = (($agent) ? explode(',', $agent->prava) : '');  ?>


            <td rowspan="2" style="width:200px; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;">
                <?php if($f_show_contacts) : ?>
                    <?php printf('%s %s', $item->sir, $item->nm); ?>
                    <br /><b><?php echo $item->ph . $item->adph; ?></b>
                <?php else : ?>
                    -
                <?php endif; ?>

                <?php if((($this->print && $this->typepdf == 'agcli') OR !$this->print) && JFactory::getUser()->id && $agent && (($item->AGENTID == $agent->ID && $item->COMPID == $agent->COMPID) OR in_array($agent->kod, $access) OR ($agent->boss == 1 && $item->COMPID == $agent->COMPID))) : ?>
                    <br />
                    <br /><b>Контакты собственника:</b>
                    <br /><?php echo $item->sob_fio ?>
                    <br /><?php echo $item->sob_tel ?>
                    <br /><?php echo $item->sob_tel2 ?>
                <?php endif; ?>
            </td>
        <?php endif; ?>

        <?php if(!$this->print) : ?>
            <td rowspan="2" style="border-right:1px solid #666; border-bottom:1px solid #666; width:30px; text-align:center;">
                <a class="btn btn-danger btn-xs removeObjectFromFavorites" href="javascript:void(0)" data-cookie-name="cardids<?php printf('%s-%s', $item->COMPID, $item->CARDNUM) ?>">
                    <span class="glyphicon glyphicon-remove"></span><br />
                    <?php echo JText::_('COM_STTNMLS_BUTTON_DELETE') ?>
                </a>

            </td>
        <?php endif; ?>
        </tr>
        <?php if($this->typepdf != 'cli') : ?>
            <tr>
                <td colspan="11" style="border-top:1px solid #666; border-bottom:1px solid #666; height:50px; width:400px;">
                    <?php echo $item->MISC . $ch ?>
                </td>
            </tr>
        <?php else : ?>
            <?php if($ch != '') : ?>
                <tr>
                    <td colspan="11" style="width:400px; border-bottom:1px solid #777;">
                        <?php echo $ch ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>
    </tbody>
<?php endforeach; ?>
</table>
