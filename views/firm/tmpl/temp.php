

<div class="sttnmls">


    <table><tr>
            <th></th>
            <th style="text-align: center"><?php echo JText::_('COM_STTNMLS_AGENTS'); ?></th>
            <th><?php echo JText::_('COM_STTNMLS_PHONES'); ?></th>
            <th style="text-align: center"><?php echo JText::_('COM_STTNMLS_SKYPE'); ?></th>
        </tr>

        <?php


        $user = JFactory::getUser();
        $userid = $user->get('id');
        $isAdmin	= $user->authorise('core.admin');
        $db = JFactory::getDbo();
        $bs=0;
        if($isAdmin==1)
        {
            $bs=1;
        }
        ?>

        <?php foreach($this->agents as $row){
            if($row->boss==1 && $row->userid==$userid && $userid>0)
            {
                $bs=1;
            }

            if($row->boss==1)
            {
                ?>
                <tr>
                    <td class="nmls_agent" style="width:64px;height:64px;">
                        <?php $link = JRoute::_('index.php?option=com_sttnmls&view=agent',false).'&agent_id='. $row->ID . '&comp_id=' . $this->item->ID ?>
                        <a href="<?php echo $link; ?>">
                            <img src="<?php echo $row->foto; ?>" alt="" />
                        </a>
                    </td>
                    <?php
                    if($row->boss_title=='')
                    {
                        $row->boss_title="Руководитель";
                    }
                    ?>
                    <td><a href="<?php echo $link; ?>"><?php echo $row->agname ?></a><br /><span style="color:#666;"><?php echo $row->boss_title; ?></span>
                        <?php
                        if($bs==1)
                        {

                            ?>
                            &nbsp;&nbsp;&nbsp;
                            <span style="color:#333; border-bottom:1px dotted #333; cursor:pointer;" onclick="if(document.getElementById('change_form_<?php echo $row->userid;?>').style.display=='none'){document.getElementById('change_form_<?php echo $row->userid;?>').style.display='block';}else{document.getElementById('change_form_<?php echo $row->userid;?>').style.display='none'}">Изменить должность</span>
                            <div style="display:none;" id="change_form_<?php echo $row->userid;?>">
                                <form method="post">
                                    <input type="hidden" name="act" value="change_dol">
                                    <input type="hidden" name="agentid" value="<?php echo $row->userid;?>">
                                    <input type="text"  name="boss_title" value="<?php echo $row->boss_title; ?>">
                                    <input type="submit" value="Изменить">
                                </form>
                            </div>
                        <?php
                        }
                        ?>



                    </td>
                    <td><?php echo $row->PHONE .'  '. $row->ADDPHONE ?></td>
                    <td><?php echo $row->SKYPE ?></td>
                </tr>
            <?php
            }
            ?>
        <?php } ?>
        <?php foreach($this->agents as $row){
            if($row->boss!=1)
            {
                if($bs==1)
                {
                    ?>
                    <tr>
                        <td class="nmls_agent" style="width:64px;height:64px;">
                            <?php $link = JRoute::_('index.php?option=com_sttnmls&view=agent',false).'&agent_id='. $row->ID . '&comp_id=' . $this->item->ID ?>
                            <a href="<?php echo $link; ?>">
                                <img src="<?php echo $row->foto; ?>" alt="" />
                            </a>
                        </td>
                        <td><a href="<?php echo $link; ?>"><?php echo $row->agname ?></a>
                            <?php
                            if($bs==1)
                            {
                                $db = JFactory::getDbo();
                                $query	= 'SELECT * FROM j25_users WHERE id="'.$row->userid.'"';
                                $db->setQuery($query);
                                $us	= $db->loadObject();
                                ?>





                                <br><a href="/component/sttnmls/?view=firm&agency=<?php echo $_REQUEST['agency'];?>&act=del_ag&agentid=<?php echo $row->kod;?>" onclick="if (confirm('Вы точно хотите удалить агента?')) { }else{ return false; }" style="color:#f00;">Удалить агента</a>

                                <?php
                                if($row->userid>0)
                                {
                                    ?>
                                    &nbsp;&nbsp;&nbsp;
                                    <span style="color:#333; border-bottom:1px dotted #333; cursor:pointer;" onclick="if(document.getElementById('change_form_<?php echo $row->userid;?>').style.display=='none'){document.getElementById('change_form_<?php echo $row->userid;?>').style.display='block';}else{document.getElementById('change_form_<?php echo $row->userid;?>').style.display='none'}">Изменить пароль</span>
                                    <div style="display:none;" id="change_form_<?php echo $row->userid;?>">
                                        <form method="post">
                                            <input type="hidden" name="act" value="change_pass">
                                            <input type="hidden" name="agentid" value="<?php echo $row->userid;?>">
                                            <input type="text" readonly  name="login" value="<?php echo $us->username;?>">
                                            <input type="text" name="password" value="">
                                            <input type="submit" value="Изменить">
                                        </form>
                                    </div>
                                <?php
                                }
                                if($row->flag==1)
                                {
                                    ?>
                                    <br><a href="/component/sttnmls/?view=firm&agency=<?php echo $_REQUEST['agency'];?>&act=confirm&agentid=<?php echo $row->kod;?>" onclick="if (confirm('Вы точно хотите подтвердить агента?')) { }else{ return false; }" style="color:#090;">Подтвердить агента</a>
                                <?php
                                }
                            }
                            ?>
                        </td>
                        <td><?php echo $row->PHONE .'  '. $row->ADDPHONE ?></td>
                        <td><?php echo $row->SKYPE ?></td>
                    </tr>
                <?php
                }else{
                    if($row->flag!=1)
                    {
                        ?>
                        <tr>
                            <td class="nmls_agent" style="width:64px;height:64px;">
                                <?php $link = JRoute::_('index.php?option=com_sttnmls&view=agent',false).'&agent_id='. $row->ID . '&comp_id=' . $this->item->ID ?>
                                <a href="<?php echo $link; ?>">
                                    <img src="<?php echo $row->foto; ?>" alt="" />
                                </a>
                            </td>
                            <td><a href="<?php echo $link; ?>"><?php echo $row->agname ?></a>
                                <?php
                                if($bs==1)
                                {
                                    $db = JFactory::getDbo();
                                    $query	= 'SELECT * FROM j25_users WHERE id="'.$row->userid.'"';
                                    $db->setQuery($query);
                                    $us	= $db->loadObject();
                                    ?>
                                    <br><a href="/component/sttnmls/?view=firm&agency=<?php echo $_REQUEST['agency'];?>&act=del_ag&agentid=<?php echo $row->kod;?>" style="color:#f00;">Удалить агента</a>

                                    <?php
                                    if($row->userid>0)
                                    {
                                        ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <span style="color:#333; border-bottom:1px dotted #333; cursor:pointer;" onclick="if(document.getElementById('change_form_<?php echo $row->userid;?>').style.display=='none'){document.getElementById('change_form_<?php echo $row->userid;?>').style.display='block';}else{document.getElementById('change_form_<?php echo $row->userid;?>').style.display='none'}">Изменить пароль</span>
                                        <div style="display:none;" id="change_form_<?php echo $row->userid;?>">
                                            <form method="post">
                                                <input type="hidden" name="act" value="change_pass">
                                                <input type="hidden" name="agentid" value="<?php echo $row->userid;?>">
                                                <input type="text" readonly  name="login" value="<?php echo $us->username;?>">
                                                <input type="text" name="password" value="">
                                                <input type="submit" value="Изменить">
                                            </form>
                                        </div>
                                    <?php
                                    }
                                    if($row->flag==1)
                                    {
                                        ?>
                                        <br><a href="/component/sttnmls/?view=firm&agency=<?php echo $_REQUEST['agency'];?>&act=confirm&agentid=<?php echo $row->kod;?>" style="color:#090;">Подтвердить агента</a>
                                    <?php
                                    }
                                }
                                ?>
                            </td>
                            <td><?php echo $row->PHONE .'  '. $row->ADDPHONE ?></td>
                            <td><?php echo $row->SKYPE ?></td>
                        </tr>
                    <?php
                    }
                }
            }
            ?>
        <?php } ?>
    </table>
</div>