<?php defined('_JEXEC') or die ('Restricted access');

// Get JOOMLA core classes
$params = JComponentHelper::getParams('com_sttnmls');
$plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
$plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
$plg_recaptcha_params = new JRegistry($plg_recaptcha->params);

$user = JFactory::getUser();
$userid	= $user->get('id');
?>
<div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal form-validate" action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'order', '&task=save') ?>" method="post">
            <input type="hidden" name="option" value="com_sttnmls"/>
            <input type="hidden" name="view" value="order"/>
            <input type="hidden" name="task" value="save"/>
            <input type="hidden" name="jform[obj]" value="apart"/>
            <input type="hidden" name="jform[cid]" value="<?php echo $this->item->COMPID ?>"/>
            <input type="hidden" name="jform[cn]" value="<?php echo $this->item->CARDNUM ?>"/>
            <?php echo JHTML::_( 'form.token' ); ?>
            
            <div class="modal-header">
                <a href="javascript:void(0);" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </a>
                <h4 class="modal-title" id="rcallModalLabel">
                    <?php echo JText::_('COM_STTNMLS_LABEL_RCALL_TITLE') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="jform_name"><?php echo JText::_('COM_STTNMLS_NAME'); ?><span class="required">*</span></label>
                    <div class="col-sm-5">
                        <input id="jform_name" class="form-control required" type="text" name="jform[name]" value="<?php echo $this->order_info->name;?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="jform_phone"><?php echo JText::_('COM_STTNMLS_PHONE'); ?><span class="required">*</span></label>
                    <div class="col-sm-5">
                        <input id="jform_phone" class="form-control required phone" type="text" name="jform[phone]" value="<?php echo $this->order_info->phone;?>" />
                    </div>
                </div>
                <?php
                    $time_from = array();
                    $time_to = array();
                    for($a = 0; $a < 24; $a++) {
                        $time_from[] = JHTML::_('select.option', $a, $a);
                    }
                    for($a = 1; $a < 25; $a++) {
                        $time_to[] = JHTML::_('select.option', $a, $a);
                    }
		?>
		
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="jformhour1"><?php echo JText::_('COM_STTNMLS_CALL_FROM'); ?></label>
                    <div class="col-sm-2">
                    <?php echo JHTML::_('select.genericlist',  $time_from, 'jform[hour1]', 'class="form-control"', 'value', 'text', $this->order_info->hour1); ?>
                    </div>
                    <label class="col-sm-1 control-label" for="jformhour2"><?php echo JText::_('COM_STTNMLS_CALL_TO'); ?></label>
                    <div class="col-sm-2">
                        <?php echo JHTML::_('select.genericlist',  $time_to, 'jform[hour2]', 'class="form-control"', 'value', 'text', $this->order_info->hour2); ?>
                    </div>
                </div>
		
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="jform_email"><?php echo JText::_('COM_STTNMLS_EMAIL'); ?></label>
                    <div class="col-sm-5">
                        <input id="jform_email" class="form-control validate-email" type="text" name="jform[email]" value="<?php echo $this->order_info->email; ?>" />
                    </div>
                </div>

                <?php 
				if(!$userid){
					if($params->get('use_captcha_security', TRUE) && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') : ?>
                    <div class="form-group">
                        <label class="control-label col-sm-5 col-md-4">
                            <?php echo JText::_('COM_STTNMLS_LABEL_CAPTCHA') ?>
                            <span class="required">*</span>
                        </label>
                        <div class="col-sm-8 col-md-8">
                            <div id="captcha_rcall_form"></div>
                        </div>
                    </div>
                <?php endif; }?>
                
            </div>
            
            <div class="modal-footer">
                <input type="submit" class="validate btn btn-success" value="<?php echo JText::_('COM_STTNMLS_SEND'); ?>">
            </div>
        </form>
    </div>
</div>