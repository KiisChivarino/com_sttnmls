<?php

defined('_JEXEC') or die('Restricted access');
require_once JPATH_COMPONENT.'/helpers/sttnmlsitem.php';

class SttNmlsModelHouse extends SttNmlsModelItem 
{
    public function __construct($config = array()) 
    {
        $this->setTabname('#__sttnmlshouses');
        $this->setTp('3');
        parent::__construct($config);
    }
        
    function getOptBtype()
    {
        if($this->getWhat()) {
            return $this->getOpt(10, 'BTYPEID');
        } else {
            return $this->getOpt(10, 'BTYPEID');
	}
    }
        
    function getOptWmaterial()
    {
        $params = JComponentHelper::getParams('com_sttnmls');
        $arr = $params->get('wmaterialh');
        $where = ' ID in (' . implode(',', $arr) . ')';

        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->select('NAME as text, ID as value');
        $query->from(' `#__sttnmlsvocalldata` ');
        $query->where(' RAZDELID=8 ');
        if($arr && count($arr)>0) {
            $query->where($where);
        }
        $query->order('text');
        $db->setQuery($query);
        $res = $db->loadAssocList();
        $options[] = array("text" => JText::_('COM_STTNMLS_SELECT0'), "value" => "0");
        if(is_array($res)) {
            foreach($res as $row)
            {
                $options[] = JHTML::_('select.option', $row['value'], $row['text']);
            } 
        }
        return JHTML::_('select.genericlist',  $options, 'jform[wmaterid]', 'class=" form-control validate-sttnum"', 'value', 'text', $this->_item->WMATERID);
    }
    
    function getOptRoof()
    {
        return $this->getOpt(11, 'ROOFID', '');
    }
    
    function getOptHeat()
    {
        return $this->getOpt(5, 'HEATID', '');
    }
    
    function getOptFoundat()
    {
        return $this->getOpt(12, 'FOUNDATID', '');
    }
    
    function getOptWc()
    {
        return $this->getOpt(7, 'WCID', '');
    }
    
    function getOptWater()
    {
        return $this->getOpt(3, 'WATERID', '');
    }
    
    function save()
    {
        //
        $app = JFactory::getApplication();

        $data = $app->input->post->get('jform', array(), 'array');
        $checkprice = $app->input->post->get('checkprice', 0, 'uint');

        
        // Подготовка поля цена (удаление пробелов)
        if(isset($data['price'])) {
            $data['price'] = str_replace(' ', '', $data['price']);
        }
              
        // Расчет цены за сотку
        if($checkprice) {
            $data['sqmprice'] = $data['price'];
            $data['price'] = $data['price'] * $data['earea'];
        } else {
            if($data['earea']) {
                $data['sqmprice'] = $data['price'] / $data['earea'];
            }
        }
        
        if(!$data['cityid']) {
            $data['cityid'] = -1;
        }

        $_export = $app->input->post->get('export', 0);
        $_exclusive = $app->input->post->get('EXCLUSIVE', 0);
        //$_close = $app->input->post->get('close', 0);
		$_encumbrance = $app->input->post->get('encumbrance', 0);
        $target_agent = $app->input->post->get('fromag', 0, 'uint');
        $checks1 = $app->input->post->get('checks1', array(), 'array');

        if($target_agent) {
            $data['fromag'] = $target_agent;
        }

        //дополнительные опции
        $data['export'] = (($_export) ? 1 : 0);
        $data['EXCLUSIVE'] = (($_exclusive) ? 1 : 0);
        //$data['close'] = (($_close) ? 1 : 0);
		$data['encumbrance'] = (($_encumbrance) ? 1 : 0);
        $data['HCHECKS1'] = implode('/', $checks1);
		
		
		//доступ к базе
		$_open_and_realtors = $app->input->post->get('open_and_realtors', 0);
		$_access_base = $app->input->post->get('access_base', 0);
		
		switch ((int)$_access_base + (int)$_open_and_realtors){
			case 1:
				$data['close'] = 1;
				$data['realtors_base'] = 0;
				break;
			case 2:
				$data['close'] = 0;
				$data['realtors_base'] = 1;
				break;
			case 3:
				$data['close'] = 0;
				$data['realtors_base'] = 0;
				break;
			case 4:
				$data['close'] = 0;
				$data['realtors_base'] = 2;
				break;
			default:
				//$data['close'] = 0;
				//$data['realtors_base'] = 0;
				//break;
				echo 'Ошибка при определении доступа записи к базе';
				exit;
		}
		
        // Сохранение данных
        $this->store($data);
    }
    
    
        
	public function getTitle()
	{
		if ($this->_item->object != JText::_('COM_STTNMLS_UCH'))
            if (mb_strtolower($this->_item->tip) != 'нет')
                $this->_item->object = $this->_item->tip;
        $pTtl = $this->getHead();
        $pTtl .= ', '.number_format($this->_item->PRICE / 1000, 3, ' ', ' ').' рублей.';

		return $pTtl;
	}
	public function getHead()
	{
		return $this->getHeadItem($this->_item);
	}
	private function getHeadItem($item){
        $pTtl = '';
		if ($item->VARIANT == 2)
		{
			$pTtl = JText::_('COM_STTNMLS_SDAMS1').' '.$item->object.", ".$item->ulica;
		} else {
			$pTtl = JText::_('COM_STTNMLS_PRODAMS1').' '.$item->object.", ".$item->ulica;
		}
        if (isset($item->HSTAGE) && intval($item->HSTAGE) > 0){ $pTtl .= ', '.$item->HSTAGE.' этаж(а)'; }
        if (isset($item->AAREA) && number_format($item->AAREA, 0) > 0){ $pTtl .= ', '.number_format($item->AAREA, 0).' кв.м'; }
        if (isset($item->EAREA) && number_format($item->EAREA, 0) > 0){ $pTtl .= ', '.number_format($item->EAREA, 0).' соток'; }

		return $pTtl;
	}
	public function getDesc()
	{
        return $pTtl1 = $this->_item->object . " " . $this->_item->mat . ", " . $this->_item->ulica . ", " . number_format($this->_item->AAREA, 0) . " кв.м, " . number_format($this->_item->EAREA, 0) . " соток, " . number_format($this->_item->PRICE / 1000, 3, ' ', ' ') . " рублей.";
	}
	
	public function getDesc2()
	{
        return $pTtl1 = "<center><b>".$this->_item->object . " " . $this->_item->mat . "</b></center><span style='font-size:11px;'><span style='color:#444;'>" . $this->_item->ulica . "<br><b>Площадь: </b>" . number_format($this->_item->AAREA, 0) . " кв.м<br><b>Участок: </b>" . number_format($this->_item->EAREA, 0) . " соток</span><br><b>Цена:</b> " . number_format($this->_item->PRICE / 1000, 3, ' ', ' ') . " рублей.</span>";
	}
	
	public function setDopJoin($query)
	{
		$query->join('LEFT','`#__sttnmlsvocalldata` as d12 on a.FOUNDATID=d12.id and d12.RAZDELID=12');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d11 on a.ROOFID=d11.id and d11.RAZDELID=11');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d7 on a.WCID=d7.id and d7.RAZDELID=7');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d5 on a.HEATID=d5.id and d5.RAZDELID=5');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d3 on a.WATERID=d3.id and d3.RAZDELID=3');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d10 on a.BTYPEID=d10.id and d10.RAZDELID=10');
		$query->join('LEFT','`#__sttnmlsvocalldata` as d8 on a.WMATERID=d8.id and d8.RAZDELID=8');
		return;
	}

	public function getDopSelect()
	{
		return ',c.NAME as gorod,
			r.NAME as raion,
			m.NAME as mraion,
			concat(s.NAME,if(a.HAAP <> " ",	CONCAT(",",a.HAAP), ""),if(a.ORIENT<>" ",CONCAT(" /",a.ORIENT), "")) as ulica,
			if(a.WHAT = 0,"'.JText::_('COM_STTNMLS_HAUSE').'","'.JText::_('COM_STTNMLS_UCH').'") AS object,
			d12.NAME as fund,
			d11.NAME as krysh,
			d7.NAME as kanal,
			d5.NAME as otopl,
			d3.NAME as voda,
			d10.NAME as tip,
			d8.NAME as mat ';
	}
	
	function getWhat()
	{
		if(!$this->_item->CARDNUM)
		{
			return JRequest::getInt('what', 1);
		} else {
			return $this->_item->WHAT;
		}
	}
	
	
	
	function getCheckAr()
	{
		$res = new stdClass;
		$res->checks1 = array(JText::_('COM_STTNMLS_HCHEKS11'),JText::_('COM_STTNMLS_HCHEKS12'),JText::_('COM_STTNMLS_HCHEKS13'),JText::_('COM_STTNMLS_HCHEKS14'),JText::_('COM_STTNMLS_HCHEKS15'));
		$res->checks2 = array(JText::_('COM_STTNMLS_HCHEKS21'),JText::_('COM_STTNMLS_HCHEKS22'),JText::_('COM_STTNMLS_HCHEKS23'),JText::_('COM_STTNMLS_HCHEKS24'),JText::_('COM_STTNMLS_HCHEKS25'));
		return $res;
	}
	
	function getMailBody()
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_sttnmls');

		// VARS
		$stype = $app->input->get('stype', 0, 'uint');


		$query = $db->getQuery(TRUE);
		$query->select($db->quoteName('o') . '.*, ' . $db->quoteName('c.NAME', 'gorod'));
		$query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
		$query->select('IF(' . $db->quoteName('o.WHAT') . ' = 0, "' . JText::_('COM_STTNMLS_HAUSE') . '", "' . JText::_('COM_STTNMLS_UCH') . '") AS ' . $db->quoteName('object'));
		$query->select($db->quoteName('d12.NAME', 'fund') . ', ' . $db->quoteName('d11.NAME', 'krysh'));
		$query->select($db->quoteName('d7.NAME', 'kanal') . ', ' . $db->quoteName('d5.NAME', 'otopl'));
		$query->select($db->quoteName('d8.NAME', 'mat'));
		$query->from($db->quoteName('#__sttnmlshouses', 'o'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('o.CITYID') . '=' . $db->quoteName('c.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('o.STREETID') . '=' . $db->quoteName('s.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd12') . ' ON ' . $db->quoteName('o.FOUNDATID') . '=' . $db->quoteName('d12.id') . ' AND ' . $db->quoteName('d12.RAZDELID') . '=' . $db->quote(12));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd11') . ' ON ' . $db->quoteName('o.ROOFID') . '=' . $db->quoteName('d11.id') . ' AND ' . $db->quoteName('d11.RAZDELID') . '=' . $db->quote(11));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd7') . ' ON ' . $db->quoteName('o.WCID') . '=' . $db->quoteName('d7.id') . ' AND ' . $db->quoteName('d7.RAZDELID') . '=' . $db->quote(7));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd5') . ' ON ' . $db->quoteName('o.HEATID') . '=' . $db->quoteName('d5.id') . ' AND ' . $db->quoteName('d5.RAZDELID') . '=' . $db->quote(5));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON ' . $db->quoteName('o.WMATERID') . '=' . $db->quoteName('d8.id') . ' AND ' . $db->quoteName('d8.RAZDELID') . '=' . $db->quote(8));
		$query->where($db->quoteName('o.COMPID') . '=' . $db->quote($this->_item->compid));
		$query->where($db->quoteName('o.CARDNUM') . '=' . $db->quote($this->_item->cardnum));
		$db->setQuery($query);
		$row = $db->loadObject();

		if(!$row) {
			return '';
		}

		foreach($row as $key => $value) {
			$data[$key] = $value;
		}

		$data['object_title'] = $this->getHeadItem($row);
		$data['street'] = $row->ulica . ', ';
		$data['mat'] = ((isset($row->mat) && $row->mat != JText::_('COM_STTNMLS_NOSV')) ? $row->mat : 'undefined') . ', ';
		$data['area'] = ((isset($row->AAREA) && (int)$row->AAREA) ? 'площадь: ' . number_format($row->AAREA, 0) . ' кв.м' : 'undefined') . ', ';
		$data['hstage'] = ((isset($row->HSTAGE) && (int)$row->HSTAGE) ? $row->HSTAGE . ' этаж(а)' : 'undefined') . ', ';
		$data['earea'] = ((isset($row->EAREA) && (int)$row->EAREA) ? 'участок: ' . number_format($row->EAREA, 0) . ' соток' : 'undefined') . ', ';
		$data['fundament'] = ((isset($row->fund) && $row->fund != '') ? 'фундамент: ' . $row->fund : 'undefined') . ', ';
		$data['krysh'] = ((isset($row->krysh) && $row->krysh != '') ? 'крыша: ' . $row->krysh : 'undefined') . ', ';
		$data['kanal'] = ((isset($row->kanal) && $row->kanal != '') ? 'канализация: ' . $row->kanal : 'undefined') . ', ';
		$data['otopl'] = ((isset($row->otopl) && $row->otopl != '') ? 'отопление: ' . $row->otopl : 'undefined') . ', ';
		$data['object_link'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&type=card&cn=' . $this->_item->cardnum . '&cid=' . $this->_item->compid);

		return SttNmlsLocal::parseString(JText::_('COM_STTNMLS_EMAIL_ITEM_ADD_ADMIN_NOTIFICATION_BODY_HOUSE'), $data);
	}
}
