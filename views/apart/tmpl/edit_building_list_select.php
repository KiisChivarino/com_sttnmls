<?php defined('_JEXEC') or die('Restricted access');

$options = array();
$options[] = JHTML::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
if($this->building_list) {
    foreach($this->building_list as $building) {
        $options[] = JHTML::_('select.option', $building->CARDNUM, htmlspecialchars($building->title));
    }
}
echo JHTML::_('select.genericlist', $options, 'building', ' class="form-control"', 'value', 'text', (($this->item) ? $this->item->builder_cardnum : NULL), FALSE, FALSE);
