<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');


// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload-ui.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/bootstrap-select.min.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/ajax-bootstrap-select.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$doc->addScriptDeclaration($script);
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery-ui.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.iframe-transport.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.fileupload.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.numeric.extensions.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/ajax-bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/edit.js?' . $component_version);

?>
<div class="com_sttnmls sttnmls_firm_edit" style="margin-bottom: 20px;">
    <div class="container-fluid">
        <div class="row">
            <?php if(!$this->item->ID) : ?>
                <h2><?php echo JText::_('COM_STTNMLS_NEWFIRM'); ?></h2>
            <?php else : ?>
                <h2><?php echo JText::_('COM_STTNMLS_EDITFIRM'); ?></h2>
            <?php endif; ?>
        </div>

        <div class="row">
            <form class="form-validate form-horizontal" action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm')?>" method="post" novalidate="novalidate">
                <input type="hidden" name="task" value="save" />
                <input type="hidden" name="jform[ID]" value="<?php echo (($this->item) ? $this->item->ID : 0) ?>"/>
                <?php echo JHTML::_( 'form.token' ) ?>

                <?php
                    $name = str_replace('"', '&quot', $this->item->NAME);
                    $addr = str_replace('"', '&quot', $this->item->addr);

                    $fil1 = str_replace('"', '&quot', $this->item->fil1);
                    $fil2 = str_replace('"', '&quot', $this->item->fil2);
                    $fil3 = str_replace('"', '&quot', $this->item->fil3);
                ?>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMNAME') ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control required" name="jform[NAME]" id="jform_name" value="<?php echo $name ?>" />
                    </div>
                </div>

                <?php if($this->item->ID) : ?>
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMREALNAME'); ?>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control" disabled="disabled" name="realname" value="<?php echo $this->item->realname ?>" />
                    </div>
                </div>
                <?php endif; ?>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMADDRESS'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control required" name="jform[addr]" id="jform_addr" value="<?php echo $addr; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        Филиал №1
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control" name="jform[fil1]" id="jform_fil1" value="<?php echo $fil1; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        Филиал №2
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control" name="jform[fil2]" id="jform_fil2" value="<?php echo $fil2; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        Филиал №3
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" class="form-control" name="jform[fil3]" id="jform_fil3" value="<?php echo $fil3; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMPHONE'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-4">
                        <input type="text" class="form-control required phone" name="jform[PHONE]" id="jform_phone" value="<?php echo $this->item->PHONE; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMADDPHONE'); ?>
                    </label>
                    <div class="col-sm-6 col-md-4">
                        <input type="text" class="form-control phone" name="jform[ADDPHONE]" id="jform_addphone" value="<?php echo $this->item->ADDPHONE; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMEMAIL'); ?>
                    </label>
                    <div class="col-sm-6 col-md-3">
                        <input type="text" class="form-control" name="jform[EMAIL]" id="jform_email" value="<?php echo $this->item->EMAIL; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMSITE'); ?>
                    </label>
                    <div class="col-sm-6 col-md-3">
                        <input type="text" class="form-control" name="jform[ISITE]" id="jform_isite" value="<?php echo $this->item->ISITE; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMCAT'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <?php echo $this->catf; ?>
                    </div>
                </div>

            <?php if($this->item->ID) : ?>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_LABEL_FIRM_LOGO'); ?>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <div class="sttnmls_photo">
                            <div class="image">
                                <img class="img-thumbnail" alt="<?php echo $name ?>" src="<?php echo $this->item->imgfirm ?>?t=<?php echo time() ?>" />
                            </div>
                            <div class="upload_image" style="margin-top: 10px;">
                                <span class="btn btn-success btn-lg fileinput-button">
                                    <span class="glyphicon glyphicon-open"></span>
                                    <span><?php echo JText::_('COM_STTNMLS_LABEL_ADD_LOGO') ?></span>
                                    <input type="file" id="firmLogo" name="files[]" data-url="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'firms', '&task=uploadFirmLogo&format=raw') ?>" data-agency="<?php echo $this->item->ID ?>" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3">
                        <?php echo JText::_('COM_STTNMLS_FIRMDESCR'); ?>
                    </label>
                    <div class="col-sm-6 col-md-6">
                    <?php
                        echo JFactory::getEditor()->display('jform[descr]', (($this->item) ? $this->item->descr : ''), '670', '400', '60', '20', false );
                    ?>
                    </div>
                </div>
            <?php endif; ?>

                <div class="form-group">
                    <div class="col-sm-offset-6 col-md-offset-3 col-sm-6 col-md-3">
                        <button type="submit" class="btn btn-success">
                            <?php echo JText::_('COM_STTNMLS_BUTTON_SAVE') ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>