<?php defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT . '/helpers/class.sttnmlslist.php');

class SttnmlsModelComs extends SttnmlsModelList
{
    // Индекс объекта в избранном (1-5)
    protected $favoriteObjectType = 4;
    // Название таблицы объектов в зависимости от отображаемой модели
    protected $objectsTableName = '#__sttnmlscommerc';

    protected function getDefaultFiltersValue()
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        $filters = array(
            // ОПИСАНИЕ ФИЛЬТРОВ:
            //    [имя_поля_фильтра]    =>  [массив значений]
            //
            //    (!) - обязательное значение фильтра
            //    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
            //
            // ЗНАЧЕНИЯ ФИЛЬТРОВ:
            //      "type"      -   (!) тип значения фильтра
            //      "value"     -   (!) значение фильтра по умолчанию
            //
            //      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
            //
            //      [!] >>>
            //      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
            //                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
            //
            //      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
            //
            //      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
            //                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
            //
            //      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
            //                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
            //                              либо завершенную строку WHERE запроса
            //      <<< [!]
            //

            'htype'         =>  array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'htype', 'valueFromTable' => '#__sttnmlsvocalldata', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s)'),
            'price_day'     =>  array('type' => 'bool', 'value' => FALSE),
            'price_hour'    =>  array('type' => 'bool', 'value' => FALSE),
            'earea_ot'      =>  array('type' => 'uint', 'value' => 0),
            'earea_do'      =>  array('type' => 'uint', 'value' => 0),
        );
        return array_merge(parent::getDefaultFiltersValue(), $filters);
    }

    protected function setPersonalConditions()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();


        // Request VARS ------------------------------------------------------------------------------------------------
        $act = $app->input->getString('act', '');

        // State VARS --------------------------------------------------------------------------------------------------
        if(isset($_REQUEST['filter_htype']) && !is_array($_REQUEST['filter_htype']) && $app->input->getString('filter_htype', 0)) {
            $this->_updateFilterState = TRUE;
            $this->setFilterState('htype', array(0 => 'null', 1 => $app->input->getUint('filter_htype', 0)));
            $this->_updateFilterState = FALSE;
        }

        $_price_day = $this->getState('filter.price_day', FALSE);
        $_price_hour = $this->getState('filter.price_hour', FALSE);

        // Раздел SELECT запроса ---------------------------------------------------------------------------------------
        $this->query->select($db->quoteName('c.NAME', 'gorod') . ', ' . $db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d.NAME', 'object'));
        $this->query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));

        // Раздел JOIN запроса -----------------------------------------------------------------------------------------
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd') . ' ON (' . $db->quoteName('o.OBJID') . '=' . $db->quoteName('d.id') . ')');


        // Раздел WHERE запроса ----------------------------------------------------------------------------------------

        // Учет посуточной и почасовой аренды для видов КВАРТИРЫ/КОМНАТЫ -----------------------------------------------
        if($this->stype) {
            if($_price_day && $_price_hour) {
                //> Установлены флаги посуточной и почасовой аренды
                $this->query->where($db->quoteName('o.PRICE_DAY') . '>0 OR ' . $db->quoteName('o.PRICE_HOUR') . '>0');
            } else {
                if($_price_day) {
                    //> Установлен флаг посуточной аренды
                    $this->query->where($db->quoteName('o.PRICE_DAY') . '>0');
                }
                if($_price_hour) {
                    //> Установлен флаг почасовой аренды
                    $this->query->where($db->quoteName('o.PRICE_HOUR') . '>0');
                }
            }
        }
        // END Учет посуточной и почасовой аренды для видов КВАРТИРЫ/КОМНАТЫ и КОММЕРЧЕСКАЯ ----------------------------


        // Учет запрошенного материала стен ----------------------------------------------------------------------------
        $this->setAdditionalWhereFromStateCheckbox($this->query, 'htype', 'o.OBJID');
        // END Учет запрошенного материала стен ------------------------------------------------------------------------

        // Учет площади кухни ------------------------------------------------------------------------------------------
        $this->setAdditionalWhereFromState($this->query, 'earea_ot', 'o.EAREA', '>=');
        $this->setAdditionalWhereFromState($this->query, 'earea_do', 'o.EAREA', '<=');
        // END Учет площади кухни --------------------------------------------------------------------------------------

        if($act == 'pdf') {
            $this->query->order($db->quoteName('o.OBJID') . ' ASC, ' . $db->quoteName('o.CITYID') . ' ASC, ' . $db->quoteName('o.RAIONID') . ' ASC, ' . $db->quoteName('o.PRICE'));
        }
    }
}
