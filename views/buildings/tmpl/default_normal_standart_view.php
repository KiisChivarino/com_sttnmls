<?php defined('_JEXEC') or die('Restricted access'); 

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;

$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();
?>

<table class="objects-table table table-striped table-hover" data-view="standart">
    <thead>
        <tr>
            <th class="text-center">
            <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_DATA', 'a.DATEINP', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'a.DATEINP') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_PHOTO', 'PICCOUNT', $listDirn, $listOrder); ?>
                
            <?php if($orderCol=='PICCOUNT') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_OBJECTDESC', 'a.AAREA', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'a.AAREA') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
            <th class="text-center" style="width:130px;">
                <?php echo JHtml::_('grid.sort',  'COM_STTNMLS_LABEL_PRICE_QM_RUB', 'a.PRICE', $listDirn, $listOrder); ?>
            <?php if($orderCol == 'a.PRICE') : ?>
                <span class="glyphicon glyphicon-sort-by-attributes<?php echo (($orderDirn == 'desc') ? '-alt' : '') ?>"></span>
            <?php endif; ?>
            </th>
        </tr>
    </thead>
    <tbody>
<?php if($this->items) : ?>   
    <?php $my_inc = 0; ?>
    <?php foreach ($this->items as $item) : ?> 
    <?php	
        $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&view=building&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->comp);
        $link_tab_aparts = SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', '&builder=' . $item->comp . '&building=' . $item->CARDNUM);
        $linkstreet = SttNmlsHelper::getSEFUrl('com_sttnmls', 'buildings', '&street_id=' . $item->STREETID);
        $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'building', '&layout=edit&cn=' . $item->CARDNUM . '&cid=' . $item->comp);
    ?>
        <tr id="<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>" class="object text-center text-middle">
            <!-- Колонка даты и посещений -->
            <td style="width: 85px;">
                <div>
                    <?php
                        $rooms_chars = array('', 'I', 'II', 'III', 'IV');
                        $item->datekvar = $rooms_chars[$item->datekvar];
                    ?>
                    <div class="rudate">
                        <div class="month">
                            <?php printf('%s кв.<br />%d г.', $item->datekvar, $item->dateyear) ?>
                        </div>
                    </div>
                </div>
                
                <div class="wcnt">
                    <i class="glyphicon icon-wcnt"></i>
                    <?php echo $item->watchcount; ?>
                </div>

                
            <?php if($item->close == 1) : ?>
                <?php //TODO: Что это? ?>
                <div style="text-align:center; padding-top:10px;">
                    <div class="label label-danger" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                        <span class="glyphicon glyphicon-eye-close"></span>
                    </div>
                </div>
            <?php endif; ?>
			
			<?php if(($currentAgent->COMPID != $compidsob AND $app->input->getBool('profile', FALSE)==1) and ($item->realtors_base == 1 or $item->realtors_base == 2)) : ?>
                
				<div style="text-align:center;">
                    <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>">
                        <?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE');?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($item->sdan) : ?>
                <div class="text-center">
                    <span class="label label-success">
                        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_COMPLETE_SIMPLE') ?>
                    </span>
                </div>

            <?php endif; ?>
            </td>
            <!-- /Колонка даты и посещений -->
            
            <!-- Колонка фото -->
            <td style="width: 150px;">
                <div class="photo-container">
                    <a href="<?php echo $link; ?>" target="_blank" class="img-thumbnail">
                    <?php 
                        $pics = explode(';', $item->PICTURES);
                        $wm = '';
                        if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
                        $img_url = SttNmlsHelper::getLinkPhoto($pics[0], 'smallcrop' . $wm);
                    ?>
                        <img src="<?php echo $img_url; ?>" alt="" />
                    </a>
                </div>
            </td>
            <!-- /Колонка фото -->
            
            <!-- Колонка описание -->
            <td class="text-left text-middle">
                <a href="<?php echo $link; ?>" class="objectdesc" target="_blank" title="<?php echo htmlspecialchars($item->title) ?>">
                    <?php echo $item->title ?>
                </a>
                <p class="objectdesc_advanced">
                <?php
                    $s = $item->gorod;
                    if($item->raion != '' && $item->RAIONID>1) {
                        $r = str_replace('р-н', '', $item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($item->mraion != '') {
                        $s .= ', ' . $item->mraion;
                    }
                    echo $s;
                ?>
                    <br />
                    <strong><?php printf('От %d м<sup>2</sup> до %d м<sup>2</sup>', $item->areafrom, $item->areato) ?></strong>
                    <?php if($this->building_types) : ?>
                    <br />
                        <?php
                            $rooms_counts = json_decode($item->type);
                            $rc = array();
                            if($rooms_counts) {
                                foreach($rooms_counts as $r) {
                                    if(isset($this->building_types[$r])) {
                                        $rc[] = $this->building_types[$r]->title;
                                    }
                                }
//                                echo JText::_('COM_STTNMLS_LABEL_APARTS_ROOMS_COUNT') . ': ' . implode(', ', $rc);
                                echo implode(', ', $rc);
                            }
                        ?>
                    <?php endif; ?>

                    <?php if($item->aparts_counts) : ?>
                        <br />
                        <a href="<?php echo $link_tab_aparts ?>" target="_blank">
                            <?php echo JText::sprintf('COM_STTNMLS_LABEL_BUILDING_APARTS_COUNT', $item->aparts_counts) ?>
                        </a>
                    <?php endif; ?>
                </p>
            </td>
            <!-- /Колонка описание -->
            
            <!-- Колонка цены -->
            <td>
                <div class="price">
                    <?php printf('от %s', number_format($item->pricefrom, 0, ' ', ' ')) ?>
                </div>

            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="text-center" style="margin-top: 30px;">
                    <a href="<?php echo $linkedit; ?>" class="btn btn-warning btn-xs" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    
                    <a href="javascript:void(0);" class="btn btn-danger btn-xs removeObjectFromList" data-cn="<?php echo $item->CARDNUM ?>" data-cid="<?php echo $item->COMPID ?>" data-view="building">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            <?php endif; ?>    
                
            </td>
            <!-- /Колонка цены -->
        </tr>
    <?php endforeach; ?>
<?php else : ?>
    <?php if($this->noitemtext) : ?>
        <tr>
            <td colspan="5"><?php echo $this->noitemtext; ?></td>
        </tr>
    <?php endif; ?>    
<?php endif; ?>
    </tbody>
</table>