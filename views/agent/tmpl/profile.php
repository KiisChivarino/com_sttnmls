<?php defined('_JEXEC') or die('Restricted access');


// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$session = JFactory::getSession();
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet(JURI::root() . 'components/com_sttnmls/assets/css/jquery.fileupload.css');
$doc->addStyleSheet(JURI::root() . 'components/com_sttnmls/assets/css/jquery.fileupload-ui.css');
$doc->addStyleSheet(JURI::root() . 'components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);


// Подгрузка дополнительных JS
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery-ui.min.js");
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.iframe-transport.js");
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.fileupload.js");
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.inputmask.js");
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/profile.js?" . $component_version);
$doc->addScript(JURI::root() . "components/com_sttnmls/assets/js/jquery.agent_table_info.js?" . $component_version);
$doc->addScriptDeclaration('var ROOT_URI="' . JURI::root() . '";');



// Если раздел не является пунктом меню - выход
if(!$menu->id) { return; }

$url = JFactory::getURI ()->toString(array('scheme', 'host', 'port', 'path', 'query', 'fragment'));
$session->set('firm.returnurl', $url);
$opentab = JRequest::getVar('opentab', 'tab-home');

?>

<div class="com_sttnmls sttnmls_profile">
    <?php if($this->item->flag == 3 OR $this->item->flag == 4) : ?>
        <?php //TODO: Скорее всего отобразиться криво! ?>
        <div style="z-index:900;position:absolute; left:180px; top:50px; width:330px; height:175px; background-color:rgba(255,255,255,0.8);">
            <a href="<?php echo $cp_url ?>?act=unblock" title="Разблокировать аккаунт и перейти в статус свободного агента">
                <img src="/components/com_sttnmls/assets/images/block.png" style="margin-top:10px; margin-botom:15px; margin-left:10px; margin-right:7px;"/>
            </a>
        </div>
    <?php endif; ?>

    <div role="tabpanel">
        <ul class="nav nav-pills nav-justified" role="tablist">
            <li role="presentation"<?php echo (($opentab == 'tab-home') ? ' class="active"' : '') ?>>
                <a href="#tab-home" aria-controls="tab-home" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECTS') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-contacts') ? ' class="active"' : '') ?>>
                <a href="#tab-contacts" aria-controls="tab-contacts" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-payment-account') ? ' class="active"' : '') ?>>
                <a href="#tab-payment-account" aria-controls="tab-payment-account" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_PAYMENT_ACCOUNT') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-commercial-services') ? ' class="active"' : '') ?>>
                <a href="#tab-commercial-services" aria-controls="tab-commercial-services" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_COMMERCIAL_SERVICES') ?>
                </a>
            </li>

            <?php if($this->item->seecont) : ?>
                <li role="presentation">
                    <a href="#tab-sobstvenniki" aria-controls="tab-sobstvenniki" role="tab" data-toggle="tab">
                        <?php echo JText::_('COM_STTNMLS_LABEL_SOBSTVENNIKI') ?>
                    </a>
                </li>
            <?php endif; ?>

            <li role="presentation">
                <?php
                    $return = base64_encode(JURI::root());
                    $logout_link = JRoute::_('index.php?option=com_users&task=user.logout&' . JSession::getFormToken() . '=1&return=' . $return, false);
                ?>
                <a href="<?php echo $logout_link ?>">
                    <?php echo JText::_('COM_STTNMLS_LABEL_EXIT') ?>
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <!-- TAB: Home -->
            <div role="tabpanel" class="tab-pane<?php echo (($opentab == 'tab-home') ? ' active' : '') ?>" id="tab-home">
                <div class="tab-content-wrapper">
                    <?php echo $this->loadTemplate('home') ?>

                    <div id="object-list-container-home"></div>
                </div>
            </div>
            <!-- //TAB: Home -->

            <!-- TAB: Contacts -->
            <div role="tabpanel" class="tab-pane<?php echo (($opentab == 'tab-contacts') ? ' active' : '') ?>" id="tab-contacts">
                <div class="tab-content-wrapper">
                    <?php echo $this->loadTemplate('contacts') ?>
                </div>
            </div>
            <!-- //TAB: Contacts -->

            <!-- TAB: Payment-account -->
            <div role="tabpanel" class="tab-pane<?php echo (($opentab == 'tab-payment-account') ? ' active' : '') ?>" id="tab-payment-account">
                <div class="tab-content-wrapper">
                    <?php echo $this->loadTemplate('balans') ?>
                </div>
            </div>
            <!-- //TAB: Payment-account -->

            <!-- TAB: Commercial services -->
            <div role="tabpanel" class="tab-pane<?php echo (($opentab == 'tab-commercial-services') ? ' active' : '') ?>" id="tab-commercial-services">
                <div class="tab-content-wrapper">
                    <?php echo $this->loadTemplate('platusl') ?>
                </div>
            </div>
            <!-- //TAB: Commercial services -->

            <?php if($this->item->seecont) : ?>
                <!-- TAB: Sobstvenniki -->
                <div role="tabpanel" class="tab-pane<?php echo (($opentab == 'tab-sobstvenniki') ? ' active' : '') ?>" id="tab-sobstvenniki">
                    <div class="tab-content-wrapper">
                        <?php echo $this->loadTemplate('sobstvenniki') ?>

                        <div id="object-list-container-sobstvenniki"></div>
                    </div>
                </div>
                <!-- //TAB: Sobstvenniki -->
            <?php endif; ?>
        </div>
		<ul class="nav nav-pills nav-justified mobile" role="tablist">
            <li role="presentation"<?php echo (($opentab == 'tab-home') ? ' class="active"' : '') ?>>
                <a href="#tab-home" aria-controls="tab-home" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_OBJECTS') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-contacts') ? ' class="active"' : '') ?>>
                <a href="#tab-contacts" aria-controls="tab-contacts" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_CONTACTS') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-payment-account') ? ' class="active"' : '') ?>>
                <a href="#tab-payment-account" aria-controls="tab-payment-account" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_PAYMENT_ACCOUNT') ?>
                </a>
            </li>

            <li role="presentation"<?php echo (($opentab == 'tab-commercial-services') ? ' class="active"' : '') ?>>
                <a href="#tab-commercial-services" aria-controls="tab-commercial-services" role="tab" data-toggle="tab">
                    <?php echo JText::_('COM_STTNMLS_LABEL_COMMERCIAL_SERVICES') ?>
                </a>
            </li>

            <?php if($this->item->seecont) : ?>
                <li role="presentation">
                    <a href="#tab-sobstvenniki" aria-controls="tab-sobstvenniki" role="tab" data-toggle="tab">
                        <?php echo JText::_('COM_STTNMLS_LABEL_SOBSTVENNIKI') ?>
                    </a>
                </li>
            <?php endif; ?>

            <li role="presentation">
                <?php
                    $return = base64_encode(JURI::root());
                    $logout_link = JRoute::_('index.php?option=com_users&task=user.logout&' . JSession::getFormToken() . '=1&return=' . $return, false);
                ?>
                <a href="<?php echo $logout_link ?>">
                    <?php echo JText::_('COM_STTNMLS_LABEL_EXIT') ?>
                </a>
            </li>
        </ul>
    </div>
</div>


