<?php

defined('_JEXEC') or die('Restricted access');

class SttNmlsModelSbers extends JModelList{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
			'id', 'a.id',
			'fio', 'fio',
			'phone', 'phone',
			'dateinp', 'a.dateinp',
			'descr', 'a.descr'
			);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*');
		$query->from('`#__sttnmlssber` AS a ');
		$query->where(' FALSE ');
		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		$this->setState('list.limit', $limit);

		$this->setFilterState('search','');
		parent::populateState('dateinp', 'desc');

		$limitstart = JRequest::getUInt('start', 0);
		$this->setState('list.start', $limitstart); 
		
	} 
	public function setFilterState($par,$default=null)
	{
		$a=$this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
		$this->setState('filter.' .$par, $a);
	}

}

