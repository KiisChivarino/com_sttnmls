<?php defined('_JEXEC') or die('Restricted access'); ?>

<h3><?php echo JText::_('COM_STTNMLS_LINK_OBJ');?></h3>
<div class="row objects-list-sm">            
<?php foreach ($this->arLink as $o) : ?>    
    <div class="col-md-3 object" id="<?php echo $o->item->COMPID . '_' . $o->item->CARDNUM ?>">
        <div class="thumbnail similar-object">
            <div class="object-photo text-center">
                <a href="<?php echo $o->url ?>" target="_blank">
                    <img class="img-rounded" src="<?php echo $o->linkimg ?>" alt="" />
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $o->url; ?>" class="objectdesc object-link" target="_self" title="">
                    <?php echo (($item->object) ? $item->object : JText::_('COM_STTNMLS_COMNED')) ?>
                    <?php if(round($item->SQUEAR, 0) > 0) : ?>
                        <?php printf('<b>%d</b>м<sup>2</sup>', round($item->SQUEAR, 0)) ?>
                        <br />
                    <?php endif; ?>
                    <?php if(round($item->EAREA, 0) > 0) : ?>
                        <?php printf('%s <b>%d</b> %s', JText::_('COM_STTNMLS_UCH'), round($item->EAREA, 0), JText::_('COM_STTNMLS_SOTOK')) ?>
                        <br />
                    <?php endif; ?>
                </a>
            </div>
            <div class="object-description">
                <?php
                    $s = $o->item->gorod;
                    if($o->item->raion != '' && $o->item->RAIONID>1) {
                        $r = str_replace('р-н', '', $o->item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($o->item->mraion != '') {
                        $s .= ', ' . $o->item->mraion;
                    }
                    echo $s;
                ?>
                <br />
                <?php if($this->what <> 1) { printf('<b>Площадь:</b> %d м<sup>2</sup>', $o->item->SQUEAR); } ?><br />
                <?php if(round($o->item->EAREA, 0) > 0) : ?>
                <?php printf('<b>Участок:</b> %s соток', $o->item->EAREA) ?><br />
                <?php endif; ?>
                <?php printf('<b>Цена:</b> %s руб.', number_format($o->item->PRICE, 0, ' ', ' ')) ?>
            </div>

        </div>
    </div>
<?php endforeach; ?>
</div>
