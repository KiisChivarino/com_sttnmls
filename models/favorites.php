<?php defined('_JEXEC') or die('Restricted access');

class SttnmlsModelFavorites extends JModelList
{
    public function getFavoritesListFromCookie()
    {
        $result = FALSE;
        if($_COOKIE) {
            foreach($_COOKIE as $name => $value) {
                if(preg_match('/cardids([0-9]+)-([0-9]+)/i', $name) && $value) {
                    preg_match('/tp([0-9]+)cid([0-9]+)cn([0-9]+)/i', $_COOKIE[$name], $data);
                    $result[$data[1]][$data[2]][] = $data[3];
                    unset($data);
                }
            }
        }
        return $result;
    }

    public function getObjectsList()
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $favoritesList = $this->getFavoritesListFromCookie();

        $items = array();

        if($favoritesList) {
            foreach($favoritesList as $tp => $cards) {
                // Подготовка списка запроса к БД в зависимости от типа
                $query = $db->getQuery(TRUE);
                switch($tp) {
                    case 1:
                            $query->clear();
                            $query->select('o.*, ' . $db->quoteName('mraion.NAME', 'mraion') . ', ' . $db->quoteName('city.NAME', 'gorod') . ', ' . $db->quoteName('agents.SIRNAME', 'sir') . ', ' . $db->quoteName('agents.NAME', 'nm') . ', ' . $db->quoteName('agents.PHONE', 'ph') . ', ' . $db->quoteName('agents.ADDPHONE', 'adph'));
                            $query->select($db->quoteName('d4.NAME', 'tip'));
                            $query->select($db->quoteName('raion.NAME', 'raion') . ', ' . $db->quoteName('o.ROOMS', 'Комн') . ', 1 AS editable, "aparts" AS model');
                            $query->select('CONCAT(' . $db->quoteName('o.STAGE') . ', "/", ' . $db->quoteName('o.HSTAGE') . ', "-", ' . $db->quoteName('d8.SHORTNAME') . ') AS planirovka');
                            $query->select('CONCAT(TRUNCATE(' . $db->quoteName('o.AAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.LAREA') . ', 0), "/", TRUNCATE(' . $db->quoteName('o.KAREA') . ', 0)) AS o_zh_k');
                            $query->select('IF((IFNULL(' . $db->quoteName('group.days') . ',0) > 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('group.days') . ', 0) <= 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL 30 day)), "0", "1") AS ifdlit');
                            $query->select('IFNULL(' . $db->quoteName('group.mdays') . ', 0) AS ' . $db->quoteName('mdays') . ',  TIMESTAMPDIFF(MINUTE, o.DATEINP, NOW())/24/60 as alldays ');
                            $query->select('CONCAT(' . $db->quoteName('street.NAME') . ', if(' . $db->quoteName('o.HAAP') . '<>" ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS `ulica`');
                            $query->select('IF(' . $db->quoteName('o.WHAT') . '=0, "' . JText::_('COM_STTNMLS_APART') . '", "' . JText::_('COM_STTNMLS_ROOM') . '") AS `object`');
                            $query->select('IF(' . $db->quoteName('o.DATEINP') . ' < DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.mdays') . ', 0) day), firms.NAME, "") AS firm');
                            $query->from($db->quoteName('#__sttnmlsaparts', 'o'));
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'city') . ' ON (' . $db->quoteName('city.ID') . ' = ' . $db->quoteName('o.CITYID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'raion') . ' ON (' . $db->quoteName('raion.ID') . ' = ' . $db->quoteName('o.RAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'mraion') . ' ON (' . $db->quoteName('mraion.id') . ' = ' . $db->quoteName('o.MRAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 'street') . ' ON (' . $db->quoteName('street.ID') . ' = ' . $db->quoteName('o.STREETID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'firms') . ' ON (' . $db->quoteName('firms.ID') . ' = ' . $db->quoteName('o.COMPID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'agents') . ' ON (' . $db->quoteName('agents.COMPID') . ' = ' . $db->quoteName('firms.ID') . ' AND ' . $db->quoteName('agents.ID') . ' = ' . $db->quoteName('o.agentid') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'group') . ' ON (' . $db->quoteName('group.id') . ' = ' . $db->quoteName('agents.grp') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd4') . ' ON (' . $db->quoteName('d4.id') . ' = ' . $db->quoteName('o.HTYPEID') . ' AND ' . $db->quoteName('d4.RAZDELID') . '="4")');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON (' . $db->quoteName('d8.id') . ' = ' . $db->quoteName('o.WMATERID') . ' AND ' . $db->quoteName('d8.RAZDELID') . '="8")');
                        break;

                    case 3:
                            $query->clear();
                            $query->select('o.*, ' . $db->quoteName('mraion.NAME', 'mraion') . ', ' . $db->quoteName('city.NAME', 'gorod') . ', ' . $db->quoteName('agents.SIRNAME', 'sir') . ', ' . $db->quoteName('agents.NAME', 'nm') . ', ' . $db->quoteName('agents.PHONE', 'ph') . ', ' . $db->quoteName('agents.ADDPHONE', 'adph'));
                            $query->select($db->quoteName('d8.NAME', 'mat'));
                            $query->select($db->quoteName('d10.NAME', 'tip'));
                            $query->select($db->quoteName(array('o.COMPID', 'o.CARDNUM', 'o.PICCOUNT', 'o.AGENTNAME', 'o.MISC', 'o.PRICE')));
                            $query->select($db->quoteName('raion.NAME', 'raion') . ', 1 AS editable, "houses" AS model');
                            $query->select('IF((IFNULL(' . $db->quoteName('group.days') . ',0) > 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('group.days') . ', 0) <= 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL 30 day)), "0", "1") AS ifdlit');
                            $query->select('IFNULL(' . $db->quoteName('group.mdays') . ', 0) AS ' . $db->quoteName('mdays') . ',  TIMESTAMPDIFF(MINUTE, o.DATEINP, NOW())/24/60 as alldays ');
                            $query->select('CONCAT(' . $db->quoteName('street.NAME') . ', if(' . $db->quoteName('o.HAAP') . '<>" ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS `ulica`');
                            $query->select('IF(' . $db->quoteName('o.WHAT') . '=0, "' . JText::_('COM_STTNMLS_HAUSE') . '", "' . JText::_('COM_STTNMLS_UCH') . '") AS `object`');
                            $query->select('IF(' . $db->quoteName('o.DATEINP') . ' < DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.mdays') . ', 0) day), firms.NAME, "") AS firm');
                            $query->from($db->quoteName('#__sttnmlshouses', 'o'));
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'city') . ' ON (' . $db->quoteName('city.ID') . ' = ' . $db->quoteName('o.CITYID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'raion') . ' ON (' . $db->quoteName('raion.ID') . ' = ' . $db->quoteName('o.RAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'mraion') . ' ON (' . $db->quoteName('mraion.id') . ' = ' . $db->quoteName('o.MRAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 'street') . ' ON (' . $db->quoteName('street.ID') . ' = ' . $db->quoteName('o.STREETID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'firms') . ' ON (' . $db->quoteName('firms.ID') . ' = ' . $db->quoteName('o.COMPID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'agents') . ' ON (' . $db->quoteName('agents.COMPID') . ' = ' . $db->quoteName('firms.ID') . ' AND ' . $db->quoteName('agents.ID') . ' = ' . $db->quoteName('o.agentid') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'group') . ' ON (' . $db->quoteName('group.id') . ' = ' . $db->quoteName('agents.grp') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd10') . ' ON (' . $db->quoteName('d10.id') . ' = ' . $db->quoteName('o.BTYPEID') . ' AND ' . $db->quoteName('d10.RAZDELID') . '="10")');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd8') . ' ON (' . $db->quoteName('d8.id') . ' = ' . $db->quoteName('o.WMATERID') . ' AND ' . $db->quoteName('d8.RAZDELID') . '="8")');
                        break;

                    case 4:
                            $query->clear();
                            $query->select('o.*, ' . $db->quoteName('mraion.NAME', 'mraion') . ', ' . $db->quoteName('city.NAME', 'gorod') . ', ' . $db->quoteName('agents.SIRNAME', 'sir') . ', ' . $db->quoteName('agents.NAME', 'nm') . ', ' . $db->quoteName('agents.PHONE', 'ph') . ', ' . $db->quoteName('agents.ADDPHONE', 'adph'));
                            $query->select($db->quoteName('raion.SHORTNAME', 'raion') . ', ' . $db->quoteName('alldata.NAME', 'object') . ', 1 AS editable, "coms" AS model');
                            $query->select('IF((IFNULL(' . $db->quoteName('group.days') . ',0) > 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('group.days') . ', 0) <= 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL 30 day)), "0", "1") AS ifdlit');
                            $query->select('CONCAT(' . $db->quoteName('street.NAME') . ', if(' . $db->quoteName('o.HAAP') . '<>" ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), ""), if(' . $db->quoteName('o.ORIENT') . '<>" ", CONCAT(" /", ' . $db->quoteName('o.ORIENT') . '), "")) AS `ulica`');
                            $query->select('IF(' . $db->quoteName('o.DATEINP') . ' < DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.mdays') . ', 0) day), firms.NAME, "") AS firm');
                            $query->select('IFNULL(' . $db->quoteName('group.mdays') . ', 0) AS ' . $db->quoteName('mdays') . ',  TIMESTAMPDIFF(MINUTE, o.DATEINP, NOW())/24/60 as alldays ');
                            $query->from($db->quoteName('#__sttnmlscommerc', 'o'));
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'city') . ' ON (' . $db->quoteName('city.ID') . ' = ' . $db->quoteName('o.CITYID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'raion') . ' ON (' . $db->quoteName('raion.ID') . ' = ' . $db->quoteName('o.RAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'mraion') . ' ON (' . $db->quoteName('mraion.id') . ' = ' . $db->quoteName('o.MRAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 'street') . ' ON (' . $db->quoteName('street.ID') . ' = ' . $db->quoteName('o.STREETID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'firms') . ' ON (' . $db->quoteName('firms.ID') . ' = ' . $db->quoteName('o.COMPID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'agents') . ' ON (' . $db->quoteName('agents.COMPID') . ' = ' . $db->quoteName('firms.ID') . ' AND ' . $db->quoteName('agents.ID') . ' = ' . $db->quoteName('o.agentid') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'group') . ' ON (' . $db->quoteName('group.id') . ' = ' . $db->quoteName('agents.grp') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'alldata') . ' ON (' . $db->quoteName('alldata.ID') . ' = ' . $db->quoteName('o.OBJID') . ')' );
                        break;

                    case 5:
                            $query->clear();
                            $query->select('o.*, ' . $db->quoteName('mraion.NAME', 'mraion') . ', ' . $db->quoteName('city.NAME', 'gorod') . ', ' . $db->quoteName('agents.SIRNAME', 'sir') . ', ' . $db->quoteName('agents.NAME', 'nm') . ', ' . $db->quoteName('agents.PHONE', 'ph') . ', ' . $db->quoteName('agents.ADDPHONE', 'adph'));
                            $query->select($db->quoteName('raion.SHORTNAME', 'raion') . ', 1 as editable, "garages" AS model');
                            $query->select($db->quoteName('alldata.SHORTNAME', 'shorttip') . ', ' . $db->quoteName('alldata.NAME', 'tip'));
                            $query->select('IF((IFNULL(' . $db->quoteName('group.days') . ',0) > 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('group.days') . ', 0) <= 0 AND ' . $db->quoteName('o.DATECOR') . '< DATE_SUB(NOW(), INTERVAL 30 day)), "0", "1") AS ifdlit');
                            $query->select('IF(' . $db->quoteName('o.DATEINP') . ' < DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('group.mdays') . ', 0) day), firms.NAME, "") AS firm');
                            $query->select('CONCAT(' . $db->quoteName('street.NAME') . ', if(' . $db->quoteName('o.HAAP') . '<>" ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS `ulica`');
                            $query->select('IFNULL(' . $db->quoteName('group.mdays') . ', 0) AS ' . $db->quoteName('mdays') . ',  TIMESTAMPDIFF(MINUTE, o.DATEINP, NOW())/24/60 as alldays ');
                            $query->select('"Гараж" AS `object`');
                            $query->from($db->quoteName('#__sttnmlsgarages', 'o'));
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'city') . ' ON (' . $db->quoteName('city.ID') . ' = ' . $db->quoteName('o.CITYID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'raion') . ' ON (' . $db->quoteName('raion.ID') . ' = ' . $db->quoteName('o.RAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'mraion') . ' ON (' . $db->quoteName('mraion.id') . ' = ' . $db->quoteName('o.MRAIONID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 'street') . ' ON (' . $db->quoteName('street.ID') . ' = ' . $db->quoteName('o.STREETID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'firms') . ' ON (' . $db->quoteName('firms.ID') . ' = ' . $db->quoteName('o.COMPID') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'agents') . ' ON (' . $db->quoteName('agents.COMPID') . ' = ' . $db->quoteName('firms.ID') . ' AND ' . $db->quoteName('agents.ID') . ' = ' . $db->quoteName('o.agentid') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'group') . ' ON (' . $db->quoteName('group.id') . ' = ' . $db->quoteName('agents.grp') . ')');
                            $query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'alldata') . ' ON (' . $db->quoteName('alldata.ID') . ' = ' . $db->quoteName('o.GTYPEID') . ')' );
                        break;

                }

                foreach($cards as $cid => $card_ids) {
                    if(count($card_ids) > 0) {
                        $query->where('(' . $db->quoteName('o.COMPID') . ' = ' . $db->quote($cid) . ' AND ' . $db->quoteName('o.CARDNUM') . ' IN (' . implode(', ', $card_ids) . '))', 'OR');
                    }
                }

//                print_r((string)$query);exit;

                $db->setQuery($query);
                $result = $db->loadObjectList();
                if($result) {
                    $items[$tp] = $result;
                }
            }
        }

        return $items;
    }


	public function getData()
	{
        if($_REQUEST['del']=='all')
        {
            ini_set('max_execution_time', 3600);
            ini_set('memory_limit', '256M');
            foreach($_COOKIE as $name => $value){
                if (ereg('cardids[0-9]+',$name) && $value){
                    setcookie($name,'', 1, '/');
                }
            }
            setcookie("fav_nums", "0", 1,'/');
            setcookie("fav_count", "0", 1,'/');
            ?>
            <script>
            document.location='/favorites.html';
            </script>
            <?php
        }

		$cards = array();
		foreach($_COOKIE as $name => $value){
			if (ereg('cardids[0-9]+',$name) && $value){
				ereg("tp([0-9]+)cid([0-9]+)cn([0-9]+)", $_COOKIE[$name], $card);
				$cards[$name]['type'] = $card[1];
				$cards[$name]['cid'] = $card[2];
				$cards[$name]['cn'] = $card[3];
				$card = '';
			}
		}
		$i = 0;
		$items=array();
		$db = $this->getDbo();
		foreach($cards as $card => $values) {
			$i++;
			switch ($values['type']){
				case 1:
					$sql_txt = 'select COMPID,CARDNUM,PICCOUNT,(SELECT VOCRAION.SHORTNAME FROM #__sttnmlsvocraion as VOCRAION WHERE VOCRAION.ID=RAIONID) as raion,CONCAT((SELECT VOCSTREET.NAME FROM #__sttnmlsvocstreet as VOCSTREET WHERE VOCSTREET.ID=STREETID),if(HAAP<>" ",CONCAT(",",HAAP), "")) as ulica,ROOMS as Комн,if(WHAT=0,"'.JText::_('COM_STTNMLS_APART').'","'.JText::_('COM_STTNMLS_ROOM').'") as object, AGENTNAME, MISC, PRICE, "aparts" as model, "' . $card . '" as cname from #__sttnmlsaparts ';
					$task = 'apartsell';
					break;
//				case 2:
//					$sql_txt = 'select COMPID,CARDNUM,PICCOUNT,(SELECT VOCRAION.SHORTNAME FROM #__sttnmlsvocraion as VOCRAION WHERE VOCRAION.ID=RAIONID) as raion,CONCAT((SELECT VOCSTREET.NAME FROM #__sttnmlsvocstreet as VOCSTREET WHERE VOCSTREET.ID=STREETID),if(HAAP<>" ",CONCAT(",",HAAP), "")) as ulica,ROOMS as Комн,if(WHAT=0,"'.JText::_('COM_STTNMLS_APART').'","'.JText::_('COM_STTNMLS_ROOM').'") as object, AGENTNAME, MISC, PRICE, "aparts" as model, "' . $card . '" as cname from #__sttnmlsaparts ';
//					$task = 'roomsell';
//					break;
				case 3:
					$sql_txt = 'select COMPID,CARDNUM,PICCOUNT,(SELECT VOCRAION.SHORTNAME FROM #__sttnmlsvocraion as VOCRAION WHERE VOCRAION.ID=RAIONID) as raion,CONCAT((SELECT VOCSTREET.NAME FROM #__sttnmlsvocstreet as VOCSTREET WHERE VOCSTREET.ID=STREETID),if(HAAP<>" ",CONCAT(",",HAAP), "")) as ulica, if(WHAT=0,"'.JText::_('COM_STTNMLS_HAUSE').'","'.JText::_('COM_STTNMLS_UCH').'") as object, AGENTNAME, MISC, PRICE, "houses" as model, "' . $card . '" as cname from #__sttnmlshouses ';
					$task = 'doma';
					break;
				case 4:
					$sql_txt = 'select COMPID,CARDNUM,PICCOUNT,(SELECT VOCRAION.SHORTNAME FROM #__sttnmlsvocraion as VOCRAION WHERE VOCRAION.ID=RAIONID) as raion, CONCAT((SELECT VOCSTREET.NAME FROM #__sttnmlsvocstreet as VOCSTREET WHERE VOCSTREET.ID=STREETID),if(HAAP<>" ",CONCAT(",",HAAP), ""),if(ORIENT<>" ",CONCAT(" /",ORIENT), "")) as ulica,(SELECT VOCALLDATA.NAME FROM #__sttnmlsvocalldata as VOCALLDATA WHERE VOCALLDATA.ID=OBJID) as object, AGENTNAME, MISC, PRICE, "coms" as model, "' . $card . '" as cname from #__sttnmlscommerc ';
					$task = 'com';
					break;
				case 5:
					$sql_txt = 'select COMPID,CARDNUM,PICCOUNT,(SELECT VOCRAION.SHORTNAME FROM #__sttnmlsvocraion as VOCRAION WHERE VOCRAION.ID=RAIONID) as raion,CONCAT((SELECT VOCSTREET.NAME FROM #__sttnmlsvocstreet as VOCSTREET WHERE VOCSTREET.ID=STREETID),if(HAAP<>" ",CONCAT(",",HAAP), "")) as ulica, "Гараж" as object, AGENTNAME, MISC, PRICE, "garages" as model, "' . $card . '" as cname from #__sttnmlsgarages ';
					$task = 'garage';
					break;
			}
			$q = $sql_txt.' where CARDNUM='.$values['cn'].' AND COMPID='.$values['cid'];
			$query = $db->setQuery($q);
			$res=$db->loadObject();
			if($res) $items[]=$res;
		}
		return $items;
	}
}
