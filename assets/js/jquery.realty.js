(function($){
        
    $.fn.realtyPhotoAutoWidth = function(options){
        var settings = $.extend({
            'smallThumbsMinWidth': 60,
            'smallThumbsMargin': 5
        }, options);
        
        this.each(function(){
            var container = $(this);
            var thumbBox = container.find('.sttnmls_photo');
            var thumbsArr = thumbBox.find('.thumb');
            var thumbsCount = thumbsArr.size();
            
            // Full container width
            var containerWidth = container.width();
            
            // Getting bigThumbs paddings and borders width
            var bigThumbPadding = thumbsArr.eq(0).innerWidth() - thumbsArr.eq(0).width();
            var bigThumbBorder = thumbsArr.eq(0).outerWidth() - thumbsArr.eq(0).innerWidth();
            // Recalc bigThumb width
            var bootstrapImgPadding = bigThumbPadding + bigThumbBorder + 1;
            thumbsArr.eq(0).width(containerWidth - bootstrapImgPadding);
            
            
            // Main container width does not be less then photo thumbs container
            var thumbsPerLine = 1;
            if(containerWidth <= settings.smallThumbsMinWidth){ 
                settings.smallThumbsMinWidth = containerWidth - bootstrapImgPadding; 
            } else {
                // Calculate small thumbs count per line if value = 0
                thumbsPerLine = Math.floor(containerWidth / (settings.smallThumbsMinWidth + settings.smallThumbsMargin + bootstrapImgPadding));
                if(thumbsPerLine < 1){ thumbsPerLine = 1; }
            }
            var totalMargin = (thumbsPerLine - 1) * settings.smallThumbsMargin;
            var totalSmallThumbsWidthOver = (containerWidth - thumbsPerLine * (settings.smallThumbsMinWidth + bootstrapImgPadding) - totalMargin) / thumbsPerLine;
            var totalSmallThumbsWidth = settings.smallThumbsMinWidth + totalSmallThumbsWidthOver;
                        
            for(var i = 1; i < thumbsCount; i++){
                if(i % thumbsPerLine) {
                    thumbsArr.eq(i).width(totalSmallThumbsWidth + 1);
                    thumbsArr.eq(i).parent().css('margin-right', settings.smallThumbsMargin + 'px');
                } else {
                    thumbsArr.eq(i).width(totalSmallThumbsWidth + 0.5);
                }
            }
        });        
        return this;
    };
})(jQuery);