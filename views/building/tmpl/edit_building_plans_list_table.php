<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>

<table class="table table-striped">
    <thead>
    <tr>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_PHOTO') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_SQUEAR') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_WC_TYPE') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_BALKON') ?></th>
        <th><?php echo JText::_('COM_STTNMLS_LABEL_PRICE_QM') ?></th>
        <th>Стоимость</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($this->item_building_plans[$this->current_rooms] as $item): ?>
        <tr>
            <td style="width:15%;">
                <a class="fbox small" href="<?php echo SttNmlsHelper::getLinkPhoto($item->photos, 'big', 'images/objects/') . '&ts=' . time() ?>">
                    <img class="thumb img-thumbnail" src="<?php echo SttNmlsHelper::getLinkPhoto($item->photos, 'smallcrop', 'images/objects/') . '&ts=' . time() ?>" alt="<?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_' . $this->current_rooms) ?>" title="<?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_PLANS_' . $this->current_rooms) ?>" />
                </a>
            </td>
            <td><?php printf('%s / %s / %s', $item->sall, $item->szhil, $item->skitch) ?></td>
            <td><?php echo (($item->san) ? 'Совм' : 'Разд') ?></td>
            <td><?php echo (($item->bal) ? 'Есть' : 'Нет') ?></td>
            <td><?php echo $item->price ?></td>
            <td><?php echo ($item->price * $item->sall) ?></td>
            <td>
                <a href="javascript:void(0);" class="btn btn-xs btn-warning editBuildingPlan" data-view="building" data-bplan_id="<?php echo $item->id ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->comp ?>">
                    <span class="sr-only">edit</span>
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <a href="javascript:void(0);" class="btn btn-xs btn-danger removeBuildingPlan" data-view="building" data-bplan_id="<?php echo $item->id ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->comp ?>">
                    <span class="sr-only">remove</span>
                    <span class="glyphicon glyphicon-remove"></span>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
