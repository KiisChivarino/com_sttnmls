<?php

defined('_JEXEC') or die;

abstract class SttNmlsHelper
{
    /**
     * Возвращает версию компонента
     *
     * @return  string
     */
    public static function getComoponentVersion()
    {
        $xml = JFactory::getXML(JPATH_ADMINISTRATOR . '/components/com_sttnmls/sttnmls.xml');
        $version = (string)$xml->version;
        return $version;
    }

	public static function GetRuDate($d) {
            $now = JFactory::getDate();
            $yesterday = JFactory::getDate('yesterday');
            $dat = JFactory::getDate($d);
            $html = '<span class="rudate"><span class="month">';
            if($dat->format('d/m/Y')==$yesterday->format('d/m/Y')){
                    $html .= JText::_('COM_STTNMLS_YESTERDAY');
            }
            elseif ($dat->format('d/m/Y')==$now->format('d/m/Y')){
                    $html .= JText::_('COM_STTNMLS_TODAY');
            }else{
                    $html .= $dat->format('d') . ' ' . mb_substr($dat->monthToString($dat->format('m'), true),0,3);
            }
            $html .= '</span> <span class="time">';
            $t = $dat->format('H:i');
            if($t != '00:00')
                    $html .= $dat->format('H:i');
            else $html .= '10:10';
            $html .= '</span></span>';
            return $html;
	}


    /**
     * Получат лимиты добавления объектов для пользователя(агента)
     *
     * @param           int     $user_id - ID пользователя в JOOMLA
     * @return          int
     */
    public static function checkLimit($user_id = 0)
    {
        $db = JFactory::getDbo();

        $user_id = (($user_id) ? $user_id : JFactory::getUser()->get('id'));


        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName(array('a.ID', 'a.COMPID', 'g.limitobj')));
        $query->from($db->quoteName('#__sttnmlsvocagents', 'a'));
        $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON (' . $db->quoteName('a.grp') . '=' . $db->quoteName('g.id') .')');
        $query->where($db->quoteName('a.userid') . '=' . $db->quote($user_id));
        $db->setQuery($query);
        $limit = $db->loadObject();

        // Нет записей - нет лимита
        if(!$limit->limitobj) {
            return 0;
        }

        // Получение счетчиков использованных лимитов по разделам объектов

        //-> Квартиры и комнаты
        $query = $db->getQuery(TRUE);
        $query->select('count(' . $db->quoteName('CARDNUM') . ')');
        $query->from($db->quoteName('#__sttnmlsaparts'));
        $query->where($db->quoteName('flag') . '=' . $db->quote('2'));
        $query->where($db->quoteName('close') . '<>' . $db->quote('1'));
		$query->where($db->quoteName('realtors_base') . '<>' . $db->quote('1'));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($limit->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($limit->ID));
        $db->setQuery($query);
        $cnt = (int)$db->loadResult();

        //-> Коммерческая недвижимость
        $query = $db->getQuery(TRUE);
        $query->select('count(' . $db->quoteName('CARDNUM') . ')');
        $query->from($db->quoteName('#__sttnmlscommerc'));
        $query->where($db->quoteName('flag') . '=' . $db->quote('2'));
        $query->where($db->quoteName('close') . '<>' . $db->quote('1'));
		$query->where($db->quoteName('realtors_base') . '<>' . $db->quote('1'));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($limit->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($limit->ID));
        $db->setQuery($query);
        $cnt += (int)$db->loadResult();

        //-> Гаражи
        $query = $db->getQuery(TRUE);
        $query->select('count(' . $db->quoteName('CARDNUM') . ')');
        $query->from($db->quoteName('#__sttnmlsgarages'));
        $query->where($db->quoteName('flag') . '=' . $db->quote('2'));
        $query->where($db->quoteName('close') . '<>' . $db->quote('1'));
		$query->where($db->quoteName('realtors_base') . '<>' . $db->quote('1'));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($limit->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($limit->ID));
        $db->setQuery($query);
        $cnt += (int)$db->loadResult();

        //-> Дома и участки
        $query = $db->getQuery(TRUE);
        $query->select('count(' . $db->quoteName('CARDNUM') . ')');
        $query->from($db->quoteName('#__sttnmlshouses'));
        $query->where($db->quoteName('flag') . '=' . $db->quote('2'));
        $query->where($db->quoteName('close') . '<>' . $db->quote('1'));
		$query->where($db->quoteName('realtors_base') . '<>' . $db->quote('1'));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($limit->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($limit->ID));
        $db->setQuery($query);
        $cnt += (int)$db->loadResult();


        $exceed_limit = $limit->limitobj - $cnt;

        return (($exceed_limit >= 0) ? $exceed_limit : false);
    }

    /**
     * Проверка на мобильность браузера
     *
     * @return bool
     */
    public static function isMobileBrowser()
    {
        // Подклчение класса определения типа браузера
        require_once(JPATH_SITE . '/components/com_sttnmls/helpers/mobile_detect.class.php');
        $browser = new Mobile_Detect();
        return $browser->isMobile();
    }

    /**
     * Возвращает тип отображения содержимого стандартный/мобильный вид
     *
     * @return int
     */
    public static function getViewType()
    {
        //$cookie = JFactory::getApplication()->input->cookie;
        //$view_type = $cookie->get('viewType', NULL);

        //if($view_type === NULL) {
            $view_type = ((self::isMobileBrowser()) ? 1 : 0);
            //$cookie->set('viewType', $view_type, 0);
        //}
        return (int)$view_type;
    }
        
        public static function getOptFirms($compid = 0, $select0 = '', $mode = 0, $cat = 0)
        {
            if(!$select0) {
                $select0 = JText::_('COM_STTNMLS_SELECT0');
            }
            
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('a.name, a.realname AS text, a.ID AS value');
            $query->from('`#__sttnmlsvocfirms` AS a ');
            $query->where('a.active=1');
            if($cat) {
                $query->join('LEFT',' #__sttnmlsfirmcatf AS c ON a.ID=c.idfirm ');
                $query->where('c.idcatf='.$db->quote($cat));
            }
            
            if($mode==1) {
                $params = JComponentHelper::getParams('com_sttnmls');
                $sob = $params->get('compidsob', '0');
                if($sob) {
                    $query->where(' a.ID<> '. $db->quote($sob));
                }
            }
                
            $query->order('text');
            $db->setQuery($query);
            $res = $db->loadAssocList();
            $options[] = array("text" => $select0, "value" => "0");
		
            if(is_array($res)) {
                foreach($res as $row)
                {
                    if(trim($row['text'])) {
                        $options[] = JHTML::_('select.option', $row['value'], $row['text']);
                    } else {
                        $options[] = JHTML::_('select.option', $row['value'], $row['name']);
                    }
                }	
            }
            
            if($mode == 1) {
                return $options;
            }
            return JHTML::_('select.genericlist',  $options, 'jform[compid]', 'size="1" class="form-control validate-sttnum required', 'value', 'text', $compid);
	}
        
        public static function getOptQ($q1, $q2)
	{
            $ret = new stdClass();
            $options1 = array();
            $options1[] = JHTML::_('select.option', 0, JText::_('COM_STTNMLS_Q10'));
            $options1[] = JHTML::_('select.option', 1, JText::_('COM_STTNMLS_Q11'));
            $options1[] = JHTML::_('select.option', 2, JText::_('COM_STTNMLS_Q12'));
            $ret->q1opt=JHTML::_('select.genericlist',  $options1, 'q1', 'class="form-control input-sm" ', 'value', 'text', $q1);
            $options2=array();
            $options2[] = JHTML::_('select.option', 0, JText::_('COM_STTNMLS_Q20'));
            $options2[] = JHTML::_('select.option', 1, JText::_('COM_STTNMLS_Q21'));
            $options2[] = JHTML::_('select.option', 2, JText::_('COM_STTNMLS_Q22'));
            $ret->q2opt=JHTML::_('select.genericlist',  $options2, 'q2', 'class="form-control input-sm" ', 'value', 'text', $q2);
            return $ret;
	}
        
        public static function getListModer($typeobj, $compid, $cardnum)
	{
            $user_id = JFactory::getUser()->id;
            if(!$user_id) {
                return FALSE;
            }
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('a.*, concat(ag.SIRNAME, " ", ag.NAME , " ", ag.SECNAME) AS agentinf, ag.ID as agentid, ag.COMPID AS agentcompid');
            $query->from('`#__sttnmlsmoder` AS a ');
            $query->join('LEFT','`#__sttnmlsvocagents` AS ag ON a.kodagent=ag.kod ');
            $query->where('a.typeobj=' . $db->quote($typeobj));
            $query->where('a.compid=' . $db->quote($compid));
            $query->where('a.cardnum=' . $db->quote($cardnum));
            $query->order('dateinp DESC');
            $db->setQuery($query);
            $moders = $db->loadObjectList();
            
            if(!count($moders)) {
                return FALSE;
            }
	
            $html = '<div class="headlistmoder">' . JText::_('COM_STTNMLS_LISTMODER_HEAD') . '</div>';
            $html .= '<ul class="list-group">';
            foreach ($moders as $item) {
                $html .= '<li class="list-group-item">';
                $date = JFactory::getDate($item->dateinp);
                $html .= '<a href="' . JRoute::_('index.php?option=com_sttnmls&view=agent&agent_id=' . $item->agentid . '&comp_id=' . $item->agentcompid, TRUE, -1) . '" target="_blank">';
                $html .= $item->agentinf;
                $html .= '</a>';
                $html .= '<span class="badge">'.$date->format('d.m.Y H:i').'</span>';
                $html .= '</li>';
            }
            $html .= '</ul>';
            return $html;
	}
        
    public static function getItemid($component, $view, $layout = 'default', $extended_search = array())
    {
        $items = JFactory::getApplication()->getMenu( 'site' )->getItems( 'component', $component );

        foreach ( $items as $item ) {

//            if($item->query['view'] == 'aparts') {
//                print_r($item->query);
//            }

            if($item->query['view'] === $view){
                $item_layout = ((!isset($item->query['layout'])) ? 'default' : $item->query['layout']);
                if($layout === $item_layout) {
                    if($extended_search) {
//                        print_r($item);
//                        echo "\n";

                        $found = 0;
                        foreach($extended_search as $var => $val) {
                            if(isset($item->query[$var]) && $item->query[$var] == $val) {
                                $found++;
                            }
//                            echo '-> ' . $var . '=' . $val . ' > Res: ' . $found . "\n";
                        }

//                        echo '-> ' . ' > ' . var_dump($found);

                        if($found && count($extended_search) == $found) {
                            return $item->id;
                        }

                    } else {
                        return $item->id;
                    }
                }
            }
        }
        return 0;
    }

    public static function getSEFUrl($component, $view, $request = '', $layout = 'default', $extended_search = array())
    {
        $extended_vars = array();

        $itemId = self::getItemid($component, $view, $layout, $extended_search);
        if(!$itemId && $extended_search) {
            foreach($extended_search as $k => $v) {
                $extended_vars[] = $k . '=' . $v;
            }
        }
        return JRoute::_('index.php?option=' . $component . (($itemId > 0) ? '&Itemid=' . $itemId : '&view=' . $view . (($extended_vars) ? '&' . implode('&', $extended_vars) : '') . (($layout != '') ? '&layout=' . $layout :  '')) . $request, FALSE, -1);
    }
        
	public static function getOptCity($init=true)
	{
		$options = array();
		if($init)
			$options[] = array("text" => SttNmlsLocal::TText('COM_STTNMLS_CITYSEL0'), "value" => "0");
		$db = JFactory::getDbo();
		$q = "SELECT ID as value, NAME as text FROM #__sttnmlsvoccity ";
		$q .= "ORDER BY ID ASC";
		$db->setQuery($q);
		$items = $db->loadObjectList();
		foreach ($items as $item) {
			$itemarr = array();
			$itemarr["value"] = $item->value;
			$itemarr["text"] = $item->text;
			$options[] = $itemarr;
		}
		return $options;
	}

	public static function getLinkPhoto($filename, $size='', $custom_path = '')
	{
            if(!trim($filename)) {
                return '/components/com_sttnmls/assets/images/foto_net.png?' . time();
            }
                 
            $inpath = (($custom_path != '') ? $custom_path : 'images/objects/') ;
            $image_folder = JPATH_ROOT . '/' . $inpath;
            if (!is_dir($image_folder)) {
                mkdir($image_folder);
            }
            $params = JComponentHelper::getParams('com_sttnmls');
            $imgpath = $params->get('imgpath', 'http://www.nmls.ru/mlsfiles/images/');
		
            if (file_exists($image_folder . $filename))
            {
                $imgpath = JURI::root() . $inpath;
            }
            if($size == 'smallcrop')
            {
                return '/components/com_sttnmls/helpers/timthumb2.php?src='.$imgpath.$filename.'&w=140&h=95&zc=1';
            }
            if($size == 'smallcropwm')
            {
                return '/components/com_sttnmls/helpers/timthumb2.php?src='.$imgpath.$filename.'&w=140&h=95&zc=1&wm=1';
            }
            if($size == 'editfirst')
            {
                return '/components/com_sttnmls/helpers/timthumb2.php?src='.$imgpath.$filename.'&w=290&h=198&zc=1';
            }
            if($size == 'editsmall')
            {
                return '/components/com_sttnmls/helpers/timthumb2.php?src='.$imgpath.$filename.'&w=60&h=54&zc=1';
            }
            if($size == 'big')
            {
                return '/components/com_sttnmls/helpers/timthumb2.php?src='.$imgpath.$filename.'&w=640';
            }
	}

	public static function getEditPhoto($view, $cardnum, $compid, $main = false, $typeimg)
	{
            $db = JFactory::getDbo();
            switch ($view) {
                case 'apart':
                        $tbl = '#__sttnmlsaparts';
                        break;
                case 'building':
                        $tbl = '#__sttnmlsbuildings';
                        break;
                case 'com':
                        $tbl = '#__sttnmlscommerc';
                        break;
                case 'garage':
                        $tbl = '#__sttnmlsgarages';
                        break;
                case 'house':
                        $tbl = '#__sttnmlshouses';
                        break;
                default:
                        $tbl = '#__sttnmlsaparts';
                        break;
            }
		
            if($typeimg == 'hod')
            {
                $db->setQuery('SELECT PICTURES2, PICNAMES2 FROM '.$db->quoteName($tbl).' WHERE CARDNUM='.$db->quote($cardnum).' AND COMPID='.$db->quote($compid));
                $res = $db->loadObject();
                $html = '';
                if($res)
                {
                    $pics = (trim($res->PICTURES2) != '') ? explode(';',$res->PICTURES2) : array();
                    $picnames = (trim($res->PICNAMES2) != '') ? explode(';',$res->PICNAMES2) : array();
                    if(count($pics)==0 && $main){
                        $html.= '<div class="editfirst" style="text-align:center;"><img style="vertical-align:top;border: #A7A7A7 solid 1px;width:264px;height:198px;" src="'.self::getLinkPhoto($filename, 'editfirst').'" /></div>';
                    }
                    foreach ($pics as $key=>$filename){
                        if($key==0){
                                $size='editfirst';
                                if(!$main) continue;
                        }
                        else{
                                $size='editsmall';
                                if($main) break;
                        }
                        $linkimg = self::getLinkPhoto($filename, $size);
                        $linka = self::getLinkPhoto($filename, 'big');
                        $linkdel = 'javascript:deleteFoto2('.$key.');';
                        $html.='<div class="'.$size.'"><a rel="group_1" class="jcepopup" target="_blank" title="'.$picnames[$key].'" href="'.$linka.'&t='.rand(100000000,99999999999).'">';
                        $html.= '<img style="vertical-align:top;" src="'.$linkimg.'&t='.rand(100000000,99999999999).'" alt="'.$picnames[$key].'" />';
                        $html.='</a><a href="'.$linkdel.'" onclick="return confirm(\''.JText::_('COM_STTNMLS_SHURE', true).'\');">'.JText::_('COM_STTNMLS_DELETE').'</a><br><a href="javascript:chasFoto2('.$key.');"><img src="http://www.iconsearch.ru/uploads/icons/blandet/16x16/arrow_rotate_clockwise.png" /></a><a href="javascript:chasnoFoto2('.$key.');"><img src="http://www.iconsearch.ru/uploads/icons/blandet/16x16/arrow_rotate_anticlockwise.png" /></a>
                                <a href="javascript:mainFoto('.$key.');">Сделать основным</a>
                        </div>';
                    }
                }
            } else {
                $db->setQuery('SELECT PICTURES, PICNAMES FROM '.$db->quoteName($tbl).' WHERE CARDNUM='.$db->quote($cardnum).' AND COMPID='.$db->quote($compid));
                $res = $db->loadObject();
                $html = '';
                
                if($res)
                {
                    $pics = (trim($res->PICTURES) != '') ? explode(';',$res->PICTURES) : array();
                    $picnames = (trim($res->PICNAMES) != '') ? explode(';',$res->PICNAMES) : array();
                    if(count($pics) == 0 && $main) {
                        $linkphoto = self::getLinkPhoto($filename, 'big');
                        $html .= <<<HTML
                            <div class="big text-center">
                                <img class="img-thumbnail" src="{$linkphoto}" alt="" />
                            </div>
HTML;
                    } else {
                        foreach ($pics as $key => $filename) {
                            if($key == 0) {
                                $size = 'big';
                                if(!$main) {
                                    continue;
                                }
                            } else {
                                $size = 'small';
                                if($main) {
                                    break;
                                }
                            }
                            $linkimg = self::getLinkPhoto($filename, $size);
                            $linka = self::getLinkPhoto($filename, 'big');
                            $linkdel = 'javascript:deleteFoto('.$key.');';
                            $mktime = time();
                            $label_confirm_shure = JText::_('COM_STTNMLS_SHURE', TRUE);
                            $label_button_delete = JText::_('COM_STTNMLS_DELETE');
                            
                            $html .= <<<HTML
                                <div class="image-container {$size}">
                                    <a rel="group_1" class="fbox" target="_blank" title="{$picnames[$key]}" href="{$linka}&t={$mktime}">
                                        <img class="img-thumbnail" src="{$linkimg}&t={$mktime}" alt="{$picnames[$key]}" />
                                    </a>
                                    <div class="photo-actions text-center">
                                        <a class="btn btn-success btn-sm" href="javascript:mainFoto({$key});" title="Сделать основным">
                                            <span class="glyphicon glyphicon-flag"></span>
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="{$linkdel}" onclick="return confirm('{$label_confirm_shure}');" title="{$label_button_delete}">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </div>
                                </div>
HTML;
                        }
                    }
                }
            }
            $html .= '';
            $html = str_replace('|', '', $html);
            return $html;
	}
        
	
	
	
	public static function getOptFirms2($compid=0,$select0='',$mode=0,$cat=0){
		if(!$select0) $select0=JText::_('COM_STTNMLS_SELECT0');
		$db = JFactory::getDbo();
		$options[] = array("text" => "Все записи", "value" => "0");
		
		$db->setQuery('select * from #__sttnmlscatf ORDER BY ID');
		$cats=$db->loadAssocList();
		foreach($cats as $cat)
		{
			
			$query = $db->getQuery(true);
			$query->select('a.name, a.realname as text, a.ID as value');
			$query->from(' `#__sttnmlsvocfirms` as a ');
			$query->where(' a.active=1 ');
			if($cat) {
				$query->join('LEFT',' #__sttnmlsfirmcatf as c on a.ID=c.idfirm ');
				$query->where(' c.idcatf='.$db->quote($cat['id']));
			}
			if($mode==1) {
                $params = JComponentHelper::getParams('com_sttnmls');
                $sob = $params->get('compidsob', '0');
                if($sob)
                    $query->where(' a.ID<> '. $db->quote($sob));
			}
			$query->order('text');
			$db->setQuery($query);
			$res=$db->loadAssocList();
			if(is_array($res)) {
				if(count($res)>0)
				{
					$options[] = JHTML::_('select.option', "0__".$cat['id'], $cat['name']);
				}
				foreach($res as $row)
				{
					if(trim($row['text']))
						$options[] = JHTML::_('select.option', $row['value'], $row['text']);
					else
						$options[] = JHTML::_('select.option', $row['value'], $row['name']);
				}	
			}
			
		}
		return $options;
	}
	
	
	
        
        
	public static function checkObjModer($obj,$cardnum,$compid)
	{
		$db = JFactory::getDbo();
		$tabname='#__sttnmls'.$obj.'s';
		if($obj=='com') $tabname='#__sttnmlscommerc';
		$query = $db->getQuery(true);
		$query->select('a.DATEINP');
		$query->select('gr.mdays as mdays,  TIMESTAMPDIFF(MINUTE,DATEINP,now())/24/60 as alldays ');
		$query->from($db->quoteName($tabname) . ' AS a ');
		$query->join('LEFT','`#__sttnmlsvocagents` as ag on a.AGENTID=ag.id and a.COMPID=ag.COMPID');
		$query->join('LEFT','`#__sttnmlsgroup` as gr on gr.id=ag.grp');
		$query->where(' a.COMPID='.$db->quote($compid));
		$query->where(' a.CARDNUM='.$db->quote($cardnum));
		$db->setQuery($query);
		$item=$db->loadObject();
		if(!$item) return 0;
		if($item->mdays>$item->alldays) return 1;
	}
	public static function getCategories($idfirm)
	{
		$ret='';
		if($idfirm){
			$db = JFactory::getDbo();
			$query = 'select b.name from #__sttnmlsfirmcatf a left join #__sttnmlscatf b on a.idcatf=b.id where a.idfirm='.$db->quote($idfirm);
			$db->setQuery($query);
			$names = $db->loadColumn();
			if(count($names)) {
				$glue = '';
				foreach ($names as $cat) {
					$ret .= $glue . $cat;
					$glue = ', ';
				}
			}
		}
		return $ret;
	}

    /**
     * Функция проверки прав на добавление/редактирование фирмы
     *
     * @param int $comp_id
     * @return bool
     */
	public static function chkUserEditFirm($comp_id = 0)
	{
        $db = JFactory::getDbo();
		$user = JFactory::getUser();

        // Пользователь вошел?
        if(!$user->id) {
            return FALSE;
        }

        // Пользователь - администратор системы?
        if($user->authorise('core.admin')) {
            return TRUE;
        }

        // Проверка по таблице прав пользователя. Операции разрешены только боссам и администраторам
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('ID'));
        $query->from($db->quoteName('#__sttnmlsvocagents'));
        $query->where($db->quoteName('boss') . '="1" AND ' . $db->quoteName('userid') . '=' . $db->quote($user->id));
        if($comp_id) {
            $query->where($db->quoteName('compid') . '=' . $db->quote($comp_id));
        }
        $db->setQuery($query);
        $result = $db->loadResult();

        return (($result) ? TRUE : FALSE);
	}
        
	
        
	public static function getCatfOptions($txt) 
        {
            $options = array();
            if($txt) {
                $options[] = array("text" => $txt, "value" => "0");
            }

            $db = JFactory::getDbo();

            $q = "SELECT id as value, name as text FROM #__sttnmlscatf ";
            $q .= " ORDER BY name ASC ";
            $db->setQuery($q);
            $items = $db->loadObjectList();
            if(count($items)) {
                foreach ($items as $item) {
                    $itemarr = array();
                    $itemarr["value"] = $item->value;
                    $itemarr["text"] = $item->text;
                    $options[] = $itemarr;
                }
            }
            return JHTML::_('select.genericlist',  $options, 'jform[catf]', 'class="form-control"', 'value', 'text', 0);
	}
        
        
        
	public static function getBalance()
	{
		$user = JFactory::getUser();
		$userid = $user->get('id');
		if(!$userid) return 0;
		$db = JFactory::getDbo();
		$db->setQuery('select balans from #__sttnmlsbalans where userid='.$db->quote($userid));
		$ret = $db->loadResult();
		if(!$ret) return 0;
		return $ret;
	}

    public static function getTimeZone()
    {
        $userTz = JFactory::getUser()->getParam('timezone');
        $timeZone = JFactory::getConfig()->get('offset');
        if($userTz) {
            $timeZone = $userTz;
        }
        return new DateTimeZone($timeZone);
    }
}