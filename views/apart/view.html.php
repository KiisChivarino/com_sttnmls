<?php
defined('_JEXEC') or die ('Restricted access');

JLoader::import('joomla.application.component.model');

class SttNmlsViewApart extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->_allow_change_state = FALSE;

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $mApart = JModelLegacy::getInstance('apart', 'SttNmlsModel');
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $mOrder =  JModelLegacy::getInstance('order', 'SttNmlsModel');
        $params = JComponentHelper::getParams('com_sttnmls');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');

        // Request VARS
        $cid = $app->input->get('cid', $params->get('compidsob', 0), 'uint');
        if($cid && $cid == $params->get('compidsob', 0) && !$_is_admin) {
            $cid = 0;
        }


        // Доступ к изменению состояния проверки объекта
        if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
            $this->_allow_change_state = TRUE;
        }



        $this->item = $this->get('Item');
        $this->activeComplaints = $this->get('ActiveComplaint');
        $this->agent = $this->get('AgentInfo');
        $this->myagent = $mApart->findAgentByID($user->id);
        $this->order_info = $mOrder->getItem();
        $this->stype = (($this->item->VARIANT == 2) ? 1 : 0);
        $this->udobstva = $this->get('Udobstva');
        $this->imgpath = $this->get('Imgpath');
        $this->tp = $this->get('Tp');
        $this->agthumb = $this->get('ComUserThumb');
        $task = JRequest::getVar('task', ''); 
        if($task == 'edit')
        {
            $this->builder_list = $this->get('BuilderList');
            $this->building_list = $mApart->getBuildingList($app->input->get('builder', 0, 'uint'));
            $this->optwhat = $this->get('OptWhat');
            $this->optwmaterial = $this->get('OptWmaterial');
            $this->opthtype = $this->get('OptHtype');
            $this->optcity = $this->get('OptCity');
            $this->optrooms = $this->get('OptRooms');
            $this->firm_agents = $mAgent->getFirmAgentsList($cid);
            $chs = $this->get('CheckAr');
            $this->checks1 = $chs->checks1;
            $this->checks2 = $chs->checks2;
        } else {
            if(!$this->item) {
                header("HTTP/1.0 404 Not Found");
            }
            $this->userperm = $this->get('UserPerm');
            $doc->setTitle($this->get('Title'));
            $doc->setDescription($this->get('Desc'));
            $this->arLink = $this->get('SimilarObjects');
        }
        parent::display($tpl);
    }
}
