<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('jquery.framework');
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
	$doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
	$doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
	$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
	$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

?>
<div class="sttnmls">
	<h2><?php echo JText::_('COM_STTNMLS_ORDS_HEAD'); ?></h2>

	<?php if($user->id) : ?>
		<?php echo $this->loadTemplate('order_form') ?>
	<?php else : ?>
		<?php
			$signup_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'signup');
			$signin_link = SttNmlsHelper::getSEFUrl('com_users', 'login');
		?>
		<div class="alert alert-info">
			<?php echo JText::sprintf('COM_STTNMLS_ORD_ERROR_USER', $signup_link, $signin_link) ?>
		</div>
	<?php endif; ?>

	<ul class="list-group">
	<?php foreach($this->items as $item) : ?>
		<?php
			$agent_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&view=agent&agent_id=' . $item->agentid . '&comp_id=' . $item->compid);
			$remove_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'ords', '&task=delord&cid=' . $item->id);
		?>
		<li class="list-group-item">
			<div class="row">
				<div class="col-md-2 col-xs-12 text-center">
					<a href="<?php echo $agent_link ?>">
						<img class="img-rounded" src="<?php echo Avatar::getUserThumbAvatar($item->userid); ?>" alt="<?php echo $item->fio ?>" title="<?php echo $item->fio ?>" />
					</a>
				</div>
				<div class="col-md-4 col-xs-12">
					<a href="<?php echo $agent_link; ?>" style="padding-left:0;">
						<b><?php echo $item->fio; ?></b>
					</a>
					<br/>
					<?php echo (($item->realname) ? $item->realname : $item->firmname); ?>
					<br/>
					<b style="white-space:nowrap;">
						<?php echo $item->phone; ?>
					</b>
				</div>
				<div class="col-md-6 col-xs-12">
					<div>
						<b><?php echo SttNmlsHelper::GetRuDate($item->dateinp); ?></b>
						<?php echo JText::sprintf('COM_STTNMLS_ORD_NUM', $item->id); ?>
						<b><?php echo JText::_('COM_STTNMLS_ORDS_VARIANT' . $item->variant . $item->typeobj); ?></b>

						<?php if($user->id == $item->userid) : ?>
							<a href="<?php echo $remove_link ?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo JText::_('COM_STTNMLS_SHURE', true); ?>')">
								<?php echo JText::_('COM_STTNMLS_DELETE'); ?>
							</a>
						<?php endif; ?>
					</div>
					<div>
						<?php echo $item->descr; ?>
					</div>
				</div>
			</div>
		</li>
	<?php endforeach; ?>
	</ul>

	<div class="pagination">
		<?php echo $this->pagination->getListFooter(); ?>
	</div>
</div>
