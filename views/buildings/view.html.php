<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewBuildings extends JViewLegacy
{
	function display($tpl = null) 
	{
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();
        $params = JComponentHelper::getParams('com_sttnmls');
        $agent_model = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $building_model = JModelLegacy::getInstance('building', 'SttnmlsModel');
        $buildings_model = JModelLegacy::getInstance('buildings', 'SttnmlsModel');


        // Данные запроса
        $act = $app->input->get('act', '', 'string');
        $itemid = $app->input->get('Itemid', 0, 'uint');
		$this->raw = (($app->input->getString('format', 'html') == 'raw') ? TRUE : FALSE);

        $this->profile = $agent_model->getProfile(JFactory::getUser()->id);
		$this->access = $this->get('AccessRules');
        $this->items = $this->get('Items');


        $this->wmater = (($params->get('wmaterial') != '') ? $buildings_model->getOptionsByID($params->get('wmaterial'), 8) : FALSE);
        $this->builders_categories_list = $this->get('BuilderFirmsList');
        $this->buildings_classes_list = $this->get('BuildingsClassesList');
        $this->buildings_furnish_list = $this->get('BuildingsFurnishList');
        $this->building_types = $building_model->getBuildingsTypes();


        $this->countobj = $this->get('Total');
        $this->filtersList = $this->get('FiltersList');
		$this->filtersGETVarsList = $this->get('FiltersGETVarsList');
        $this->noitemtext = '';
        if(!$this->items) {
            $this->noitemtext =  $this->get('NText');
        }

        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->stype = JRequest::getInt('stype',0);
        $this->raions = $this->get('RaionsList');
        $this->mraions = $this->get('MRaionsList');
        $this->cityopt = $this->get('OptCity');
        $this->tp = $this->get('Tp');
        $this->what = JRequest::getInt('what', 0);


		$db = JFactory::getDbo();
		$db->setQuery('select * from #__menu WHERE id="' . $itemid . '"');
		$seo = $db->loadObject();
		$seotxt = json_decode($seo->params);
		$meta_dsc = '';
		$meta_key = '';
		$meta_title = '';
		foreach($seotxt as $k => $v) {
			if($k == 'menu-meta_description') {
				$meta_dsc = $v;
			}
			if($k == 'menu-meta_keywords') {
				$meta_key = $v;
			}
			if($k == 'page_title') {
				$meta_title = $v;
			}
		}

		if($meta_dsc != '') {
			$document->setDescription($meta_dsc);
		} else {
			if(!$this->stype) {
				$document->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_AP'));
			} else {
				$document->setDescription(SttNmlsLocal::TText('COM_STTNMLS_DESC_AP1'));
			}
		}
		if($meta_title != '') {
			$document->setTitle($meta_title);
		} else {
			if(!$this->stype) {
				$document->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_AP'));
			} else {
				$document->setTitle(SttNmlsLocal::TText('COM_STTNMLS_TITLE_AP1'));
			}
		}
		if($meta_key != '') {
			$document->setMetadata('keywords', $meta_key);
		}
		


		parent::display($tpl);
	}
}
