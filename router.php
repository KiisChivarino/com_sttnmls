<?php

defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

/**
 * @param	array	A named array
 * @return	array
 */
function SttnmlsBuildRoute(&$query)
{
    $segments = array();

    if(isset($query['view'])) {
        switch($query['view']) {

            case 'apart':
            case 'house':
            case 'com':
            case 'garage':
            case 'building':
                if(isset($query['type']) && $query['type'] == 'card') {
                    $segments[] = $query['cid'];
                    $segments[] = $query['cn'];
                    unset($query['view']);
                    unset($query['cn']);
                    unset($query['cid']);
                    unset($query['type']);
                }

                break;

            case 'firm':
                $segments[] = $query['agency'];
                unset($query['view']);
                unset($query['agency']);
                break;

            case 'agent':
                $segments[] = $query['comp_id'];
                $segments[] = $query['agent_id'];
                unset($query['view']);
                unset($query['comp_id']);
                unset($query['agent_id']);
                break;
        }
    }


    return $segments;
}



/**
 * @param	array	A named array
 * @param	array
 *
 */
function SttnmlsParseRoute($segments)
{
    $vars = array();

    // JOOMLA Instances
    $app = JFactory::getApplication();
    $menu = $app->getMenu()->getActive();
    $count = count($segments);

    if($menu && isset($menu->query['view'])) {

        $vars['view'] = $menu->query['view'];
        switch($menu->query['view']) {

            case 'apart':
            case 'com':
            case 'garage':
            case 'house':
            case 'building':
                    if(isset($segments[1]) && $segments[1]) {
                        $vars['cid'] = $segments[0];
                        $vars['cn'] = $segments[1];
                    } else {
                        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $segments[0]));
                    }
                break;


            case 'firm':
                    $vars['agency'] = $segments[0];

                break;

            case 'agent':
                    if(isset($segments[1]) && $segments[1]) {
                        $vars['comp_id'] = $segments[0];
                        $vars['agent_id'] = $segments[1];
                    } else {
                        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&view=firm&agency=' . $segments[0]));
                    }
                break;


        }


    }


    return $vars;
}
