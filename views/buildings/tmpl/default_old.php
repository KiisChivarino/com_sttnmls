<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
$doc->addScript("components/com_sttnmls/assets/js/jscript.js");
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
$params = JComponentHelper::getParams('com_sttnmls');
?>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">
<div class="sttnmls"> 
	
    <?php
	if($_REQUEST['format']!='raw')
		{
			require_once 'components/com_mynmls/file.php';
			foreach ($this->items as $k=>$item) {
				$db = JFactory::getDbo();
				$query	= 'SELECT * FROM j25_sttnmlscompl WHERE status=1 AND typeobj=1 AND compid="'.$item->COMPID.'" AND cardnum="'.$item->CARDNUM.'"';
				$db->setQuery($query);
				$compl	= $db->loadObjectList();
				if(count($compl)>0)
				{
					unset($this->items[$k]);
					$this->countobj--;
				}
			}
		}
	
	?>
    
    
    <h1>
	<?php
	if(JRequest::getVar('titleh1')!='')
	{
		echo JRequest::getVar('titleh1');
	}else{
	?>
	Новостройки
	<?php
	}
	?>
	(<?php echo $this->countobj;?>)</h1>
    
    
    

    
	<form action="/kvartiri/novostroika.html" method="post" name="adminForm" id="adminForm">
	
	
	
	
	<?php
		$street = JRequest::getInt('streets', 0);
		if($street) echo '<input type="hidden" name="streets" value="'.$street.'">';
		$agency = JRequest::getInt('agency', 0);
		if($agency) echo '<input type="hidden" name="agency" value="'.$agency.'">';
		$agent = JRequest::getInt('agent', 0);
		if($agent) echo '<input type="hidden" name="agent" value="'.$agent.'">';
	?>
	<?php if(!isset($this->raw)) { ?>
	<fieldset class="filters">
		<?php echo $this->loadTemplate('filtr'); ?>
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	</fieldset> 
	<?php } ?>
	<?php echo $this->filterlist; ?>
    <?php
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
	?>
	<table class="realty_list listobj" cellspacing="0" style="width:702px !important">
		<tr>
			<th align="center">
            <?php
			if($orderCol=='a.DATEINP')
			{
				?>
                <span class="active_sort">
                <?php	
			}
			?>
			<?php echo JHtml::_('grid.sort',  'Дата сдачи', 'a.DATEINP', $listDirn, $listOrder); ?>
            <?php
			if($orderCol=='a.DATEINP')
			{
				if($orderDirn=="desc")
				{
					echo "&#9660;";
				}else{
					echo "&#9650;";
				}
				echo "</span>";
			}
			?>
            </th>
			<th align="center">
			<?php
			if($orderCol=='PICCOUNT')
			{
				?>
                <span class="active_sort">
                <?php	
			}
			?>
			<?php echo JHtml::_('grid.sort',  'COM_STTNMLS_PHOTO', 'PICCOUNT', $listDirn, $listOrder); ?>
            <?php
			if($orderCol=='PICCOUNT')
			{
				if($orderDirn=="desc")
				{
					echo "&#9660;";
				}else{
					echo "&#9650;";
				}
				echo "</span>";
			}
			?>
            </th>
			<th>
			<?php
			if($orderCol=='a.AAREA')
			{
				?>
                <span class="active_sort">
                <?php	
			}
			?>
			<?php echo JHtml::_('grid.sort',  'COM_STTNMLS_OBJECTDESC', 'a.AAREA', $listDirn, $listOrder); ?>
            <?php
			if($orderCol=='a.AAREA')
			{
				if($orderDirn=="desc")
				{
					echo "&#9660;";
				}else{
					echo "&#9650;";
				}
				echo "</span>";
			}
			?>
            </th>
			<th>
			<?php
			if($orderCol=='a.PRICE')
			{
				?>
                <span class="active_sort">
                <?php	
			}
			?>
			<?php echo JHtml::_('grid.sort',  'COM_STTNMLS_PRICE', 'a.PRICE', $listDirn, $listOrder); ?>
            <?php
			if($orderCol=='a.PRICE')
			{
				if($orderDirn=="desc")
				{
					echo "&#9660;";
				}else{
					echo "&#9650;";
				}
				echo "</span>";
			}
			?>
            </th>
			<!--<th width='33' style='padding: 0;'></th>-->
		</tr>
		<?php
		$my_inc = 0;
		
		
		
		
		
		
		
		
		
		foreach ($this->items as $item) {
			if($_REQUEST['format']=='raw')
			{
				$db = JFactory::getDbo();
				$query	= 'SELECT * FROM j25_sttnmlscompl WHERE status!=2 AND typeobj=1 AND compid="'.$item->COMPID.'" AND cardnum="'.$item->CARDNUM.'"';
				$db->setQuery($query);
				$compl	= $db->loadObjectList();
			}
			
			
			
			
			
			
			
			
			$link = JRoute::_('index.php?option=com_sttnmls&view=building&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID, false);
			$linkstreet = JRoute::_('index.php?option=com_sttnmls&view=buildings&streets=' . $item->STREETID, false);
			$my_inc++;
			$style = ($my_inc % 2) ? 'odd0' : 'even0';
			if($item->upobj) $style .= ' upobj';
			if($item->ifdlit!=1) $style = 'neprod';
			?>
			<tr id="_<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>" class="middle" onmouseover="this.className='middle' " onmouseout="this.className='middle'">
				<td width="65">
					<?php
						if($item->datekvar==1)
						{
							$item->datekvar="I";
						}
						if($item->datekvar==2)
						{
							$item->datekvar="II";
						}
						if($item->datekvar==3)
						{
							$item->datekvar="III";
						}
						if($item->datekvar==4)
						{
							$item->datekvar="IV";
						}
						echo '<div class="rudate"><div class="month">'.$item->datekvar.' кв.<br>'.$item->dateyear.' г.</div></div>';
					?>
					<div class="wcnt"><?php echo $item->watchcount; ?></div>

                    <?php
					if($item->close==1){
					?>
                       	<div style="text-align:center; padding-top:10px;"><img src="/images/zam.png" title="Внутренняя база" style="cursor:pointer;" onclick="check_close();"/></div>
                        <script language="javascript">
						function check_close()
							{
								document.getElementById('close').checked=true;	
								document.getElementById('adminForm').submit();
							}
						</script>
                    <?php
					}
					?>
				</td>
				<td class="center middle" width="140" valign="top" style="vertical-align:top !important; padding-top:7px;">
					<?php
					$exclusive = '';
					if($item->EXCLUSIVE==3) $exclusive='exclusive';
					if($item->upobj) $exclusive .=' upobj';
					if($exclusive) $exclusive =' class="'.$exclusive.'"';
					?>
					<a href="<?php echo $link; ?>" <?php echo $exclusive; ?> target="_blank" style="position:relative; display:block; width:140px; height:95px;">
						<?php $pics = explode(';', $item->PICTURES);
							$wm='';
							if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
							$linkimg = SttNmlsHelper::getLinkPhoto($pics[0], 'smallcrop'.$wm);
						?>
						<img style="vertical-align:top;" src="<?php echo $linkimg; ?>" alt="" />
					</a>
				</td>
				<td align="left" width=290>
                
                
					<span class="objectdesc"><a href="<?php echo $link; ?>" target="_blank" title="<?php echo $item->title; ?>"><b><?php echo $item->title; ?></b></a></span>
					<br/>
					<span class="objectdesc2" style="color:#333; font-size:12px;">
					<span style="color: #333;">
					<?php
					echo $item->gorod;
					if($item->raion != '' && $item->RAIONID>1) {
						$r = str_replace('р-н', '', $item->raion);
						$r = str_replace('район', '', $r);
						$r = str_replace('  ', ' ', $r);
						echo ', ' . $r .' р-н';
					}
					if($item->mraion != '') {
						echo ', ' . $item->mraion;
					}
					?><br />
					<b><?php
						echo 'От '.$item->areafrom.' м<span class="qm">2</span>';
						echo ' до '.$item->areato.' м<span class="qm">2</span>';
					?></b>
<br/></span>
					<div class="MISC_SMALL">
						<?php echo strip_tags($item->MISC); ?>
                    </div>
				</td>
				<td align="center" width=75 nowrap>
					<b class="price">
						от <?php echo number_format($item->pricefrom, 0, ' ', ' '); ?> за м<span class="qm">2</span>
					</b>
                    <?php
					

					if($item->editable && ($item->flag==1 || $item->flag==2)) {
							$linkedit=JRoute::_('index.php?option=com_sttnmls&view=building&task=edit&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID, false);
						?>
						<div class="picsmang" <?php echo $style; ?> style="width:60px; position:relative; left:-12px; top:-2px;">
							<!--<a href="<?php echo $link.'#plat'; ?>" class="picplat" target="_blank" title="<?php echo JText::_('COM_STTNMLS_PLAT'); ?>"></a>-->
							<a href="<?php echo $linkedit; ?>" class="picedit" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>"></a>
							<!--<a class="picdate" title="<?php echo JText::_('COM_STTNMLS_EDITDATE'); ?>" onclick="changeDate(<?php echo $item->CARDNUM; ?>,<?php echo $item->COMPID; ?>,'building');"></a>
                            <a href="#" style="background:#FFF; float:left; width:28px; height:28px;" onclick="changeDate2(<?php echo $item->CARDNUM; ?>,<?php echo $item->COMPID; ?>,'building'); return false;" title="Сбросить дату добавления. Стоимость: <?php echo $params->get('dateprice');?> руб."><img src="http://www.iconsearch.ru/uploads/icons/nuove/24x24/kontact_date.png" /></a>-->
							<a class="picdelete" title="<?php echo JText::_('COM_STTNMLS_DELETE'); ?>" onclick="return deleteobj(<?php echo $item->CARDNUM; ?>,<?php echo $item->COMPID; ?>,'building');"></a>
						</div>
					<?php }?>
				</td>
				<!--<td>
					<div class="to_favorive" tp="<?php echo $this->tp; ?>" cid="<?php echo $item->COMPID; ?>" cn="<?php echo $item->CARDNUM; ?>"></div>
				</td>-->
			</tr>
		<?php } ?>
		<?php if($this->noitemtext) { ?>
			<tr>
				<td colspan="5"><?php echo $this->noitemtext; ?></td>
			</tr>
		<?php } ?>
        <?php
		if($_REQUEST['format']=='raw')
					{
						if(count($compl)>0)
						{
							?>
        <tr>
        	<td colspan="2"></td>
            <td colspan="3">
					
							<div id="compl" style="font-size:14px; font-weight:bold; padding-bottom:5px;">Жалобы</div>
							<?php
							foreach($compl as $item2)
							{
							
							$rs = '';
							if($item2->rs==1) $rs = "Продано";
							if($item2->rs==2) $rs = "Неверная цена";
							if($item2->rs==3) $rs = "Ошибка в информации";
							if($item2->rs==4) $rs = "Другое";
							?>
							<div style="padding-bottom:10px; margin-bottom:10px; font-size:12px; border-bottom:1px solid #dfdfdf;">
								<b><?php echo $rs; ?>:</b> <?php echo $item2->desc; ?><br />
								<b>Проверено:</b> <?php if($item2->status==0){ echo "Нет <img style='position:relative; top:4px;' src='http://www.iconsearch.ru/uploads/icons/crystalclear/16x16/edit_remove.png' alt='unapproved'/>"; }else{ echo "Да <img src='http://kurskmetr.ru/administrator/templates/isis/images/admin/icon-16-allow.png' alt='approved'/> [Объект не показывается в общем списке]"; }?><br />
								<span style="color:#999;"><b>Дата:</b> <?php echo date('d.m.Y H:i:s',strtotime($item2->dateinp)); ?></span>
							</div>
							<?php	
							}
						?>
            </td>
        </tr>
        <?php
		}
					}
					?>
                    
	</table>
	<?php if(!isset($this->raw)) { ?>
		<div class="pagination">
			<?php echo $this->pagination->getListFooter(); ?>
		</div>
	<?php } ?>
	</form>
	<?php
	if($this->pagination->get('pages.current',1)==1) {
		$db = JFactory::getDbo();
		$query	= 'SELECT * FROM #__content WHERE id="'.JRequest::getVar('article_id').'"';
		$db->setQuery($query);
		$aftertext	= $db->loadObjectList();
		echo $aftertext[0]->introtext."<br>".$aftertext[0]->fulltext;
	}
	?>

</div>
