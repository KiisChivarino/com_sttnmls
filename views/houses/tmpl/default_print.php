<?php defined('_JEXEC') or die ('Restricted access'); 
    $params = JComponentHelper::getParams('com_sttnmls');
    
    $gorod = '';
    $object = '';
    $raion = '';
    $rooms = '';
    $ulica = '';
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <title><?php echo JText::_('COM_STTNMLS_LABEL_PRINT') ?></title>
    </head>
    <body>
        <div style="font-family:Arial; width:900px; margin:auto; font-size:11px;">
        <?php if($this->agent) : ?>
            <h2 style="text-align:right;">
                <?php printf('%s %s %s, %s, %s', $this->agent->SIRNAME, $this->agent->NAME, $this->agent->SECNAME, $this->agent->PHONE, $this->agent->EMAIL) ?>
            </h2>
        <?php else : ?>
            <h2 style="text-align:right;">
                <?php printf('%s, %s, %s, %s', $params->get('site.title'), $params->get('site.phone'), $params->get('site.email'), $params->get('site.url')) ?>
            </h2>
        <?php endif; ?>
            <br />
            <br />
            <h3>Продажа домов</h3>
            <table width="100%" style="margin-top:5px; font-size:12px;" cellpadding="5" cellspacing="0" border="0">
                <tr style="background:#333; color:#fff; text-align:center; font-weight:bold;">
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <th>#</th>
                <?php endif; ?>
                    <th>Местонахождение</th>
                    <th>Тип</th>
                    <th>Эт-ть<br />Матер</th>
                    <th>Пл<br>дом/уч</th>
                    <th>Вода</th>
                    <th>Отопл</th>
                    <th>Канал</th>
                    <th>Крыша</th>
                    <th>Г</th>
                    <th>Эл</th>
                    <th>Цена</th>
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <th>Агент</th>
                <?php endif; ?>
                </tr>
            <?php if($this->items) : ?>
                <?php foreach($this->items as $item) : ?>
                
                <?php if($item->gorod != $gorod) : ?>
                <!-- Группировка ГОРОД -->
                <?php
                    $raion = '';
                    $gorod = $item->gorod;
                ?>
                <tr>
                    <td colspan="13" style="border-bottom:1px solid #666; background:#dfdfdf;">
                        <h3 style="padding:0px; margin:0px;">
                            <?php echo $item->gorod ?>
                        </h3>
                    </td>
                </tr>
                <!-- //Группировка ГОРОД -->
                <?php endif; ?>
                
                <?php if($item->raion != $raion) : ?>
                <!-- Группировка РАЙОН -->
                <?php
                    $ulica = '';
                    $raion = $item->raion;
                ?>    
                    <?php if($item->raion != 'Нет сведений') : ?>
                    <tr>
                        <td colspan="13" style="border-bottom:1px solid #666; background:#dfdfdf;">
                            <h3 style="padding:0px; margin:0px;">
                                <?php printf('%s - %s', $item->gorod, $item->raion) ?>
                            </h3>
                        </td>
                    </tr>
                    <?php endif; ?>
                <!-- Группировка РАЙОН -->
                <?php endif; ?>
                
                <?php
                    $dateinp = explode(" ", $item->DATEINP);
                    $dateinp = explode("-", $dateinp[0]);
                    $datecor = explode(" ", $item->DATECOR);
                    $datecor = explode("-", $datecor[0]);
                    
                    $san = '-';
                    
                    $item->HSTAGE = ($item->HSTAGE != 0) ? $item->HSTAGE . ' - ' : '';
                    
                    if($item->adph != '') {
                        $item->adph = ', ' . $item->adph;
                    }
                    
                    $item->HCHECKS1 = trim($item->HCHECKS1);
                    
                    $gaz = "-";
                    $el = "-";
                    if($item->HCHECKS1)
                    {
                        $ch = explode("/", $item->HCHECKS1);
                        for($y = 0; $y < count($ch); $y++)
                        {
                            if($ch[$y] == 'газ')
                            {
                                $gaz = "+";
                            }
                            if($ch[$y] == '220В' OR $ch[$y] == '380В')
                            {
                                $el = "+";
                            }
                        }
                    }
                    
                                       
                    $ch = '';
                    if($item->HCHECKS1 != '' && $item->HCHECKS1 != '/') {
                        $item->HCHECKS1 .= "/";
                        $item->HCHECKS1 = str_replace("//", "", $item->HCHECKS1);
                        $item->HCHECKS1 = str_replace("/", " / ", $item->HCHECKS1);
                        
                        if($_REQUEST['typepdf'] == 'cli')
                        {
                            $ch = '<div style="color:#444">' . $item->HCHECKS1 . '</div>';
                        }else{
                            $ch = '<div style="border-top:1px solid #bbb; padding-top:5px; margin-top:10px;">' . $item->HCHECKS1 . '</div>';
                        }
                    }
                    
                    $water = $this->houses_model->getOptionsByID($item->WATERID);
                    if(!isset($water->SHORTNAME)) {
                        $water = new stdClass();
                        $water->SHORTNAME = 'Не указано';
                    }
                    
                    $HEAT = $this->houses_model->getOptionsByID($item->HEATID);
                    if(!isset($HEAT->SHORTNAME)) {
                        $HEAT = new stdClass();
                        $HEAT->SHORTNAME = 'Не указано';
                    }
                    
                    $WC = $this->houses_model->getOptionsByID($item->WCID);
                    if(!isset($WC->SHORTNAME)) {
                        $WC = new stdClass();
                        $WC->SHORTNAME = 'Не указано';
                    }
                    
                    $roof = $this->houses_model->getOptionsByID($item->ROOFID);
                    if(!isset($roof->SHORTNAME)) {
                        $roof = new stdClass();
                        $roof->SHORTNAME = 'Не указано';
                    }
                ?>
                <tr>
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>                    
                    <td rowspan="2" style="width:150px; text-align:center; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;" valign="middle">
                        <b><?php echo $item->firm ?></b>
                        <br />(<?php echo $item->COMPID ?>)
                        <br />
                        <span style="padding:4px 0px; display:block">
                            <b>Объект #:</b> 
                            <a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=house&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID) ?>" target="_blank">
                                <?php echo $item->CARDNUM ?>
                            </a>
                        </span>
                        <span style="font-size:11px;">
                            <?php printf('%s.%s.%s&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;%s.%s.%s', $dateinp[2], $dateinp[1], $dateinp[0], $datecor[2], $datecor[1], $datecor[0]) ?>
                        </span>
                    </td>
                <?php endif; ?>
                    
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <td style="border-right:1px solid #666; width:150px; vertical-align:top !important;" valign="top">
                        <h4 style="padding:0px; margin:0px;"><?php echo $item->ulica ?></h4>
                    </td>
                    <td style="border-right:1px solid #666; width:60px; text-align:center;">
                        <?php echo $item->tip ?>
                    </td>
                    <td style="border-right:1px solid #666; width:60px; text-align:center;">
                        <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
                    </td>
                    <td style="border-right:1px solid #666; width:40px; text-align:center;">
                        <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $water->SHORTNAME ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $HEAT->SHORTNAME ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $WC->SHORTNAME ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $roof->SHORTNAME ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $gaz ?>
                    </td>
                    <td style="border-right:1px solid #666; width:30px; text-align:center;">
                        <?php echo $el ?>
                    </td>
                    <td style="width:70px; text-align:center; width:100px; white-space:nowrap;">
                        <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                    </td>
                <?php else : ?>
                    <?php if($ch != '') : ?>
                        <td style="width:150px; vertical-align:top !important;" valign="top">
                            <h4 style="padding:0px; margin:0px;"><?php echo str_replace(',', ', ', $item->ulica)?></h4>
                        </td>
                        <td style="width:60px; text-align:center;">
                            <?php echo $item->tip ?>
                        </td>
                        <td style="width:60px; text-align:center;">
                            <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
                        </td>
                        <td style="width:40px; text-align:center;">
                            <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $water->SHORTNAME ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $HEAT->SHORTNAME ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $WC->SHORTNAME ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $roof->SHORTNAME ?>
                        </td>
                        <td style="width:30px; text-align:center;">
                            <?php echo $gaz ?>
                        </td>
                        <td style="bwidth:30px; text-align:center;">
                            <?php echo $el ?>
                        </td>
                        <td style="width:70px; text-align:center; width:100px; white-space:nowrap;">
                            <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                        </td>
                    <?php else : ?>
                        <td style="border-bottom:1px solid #666; width:150px; vertical-align:top !important;" valign="top">
                            <h4 style="padding:0px; margin:0px;"><?php echo str_replace(',', ', ', $item->ulica)?></h4>
                        </td>
                        <td style="border-bottom:1px solid #666; width:60px; text-align:center;">
                            <?php echo $item->tip ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:60px; text-align:center;">
                            <?php printf('%s%s', $item->HSTAGE, mb_substr($item->mat, 0, 3)) ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:40px; text-align:center;">
                            <?php printf('%d / %d', $item->AAREA, $item->EAREA) ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $water->SHORTNAME ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $HEAT->SHORTNAME ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $WC->SHORTNAME ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $roof->SHORTNAME ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $gaz ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:30px; text-align:center;">
                            <?php echo $el ?>
                        </td>
                        <td style="border-bottom:1px solid #666; width:70px; text-align:center; width:100px; white-space:nowrap;">
                            <b><?php echo number_format(round($item->PRICE), 0, '', ' ') ?></b>
                        </td>
                    <?php endif; ?>
                <?php endif; ?>
                        
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                    <?php $access = explode(',', $this->agent->prava); ?>
                    
                        <td rowspan="2" style="width:200px; border-left:1px solid #666; border-bottom:1px solid #666; border-right:1px solid #666;">
                            <?php printf('%s %s', $item->sir, $item->nm) ?>
                            <br /><b><?php echo $item->ph . $item->adph ?></b>
                        <?php if((($item->AGENTID == $this->agent->ID && $item->COMPID == $this->agent->COMPID) OR in_array($this->agent->kod, $access) OR ($this->agent->boss == 1 && $item->COMPID == $this->agent->COMPID)) && JFactory::getUser()->id > 0) : ?>
                            <br />
                            <br /><b>Контакты собственника:</b>
                            <br /><?php echo $item->sob_fio ?>
                            <br /><?php echo $item->sob_tel ?>
                            <br /><?php echo $item->sob_tel2 ?>
                        <?php endif; ?>
                        </td>
                <?php endif; ?>
                    
                </tr>
                <?php if($_REQUEST['typepdf'] != 'cli') : ?>
                <tr>
                    <td colspan="11" style="border-top:1px solid #666; border-bottom:1px solid #666; height:50px; width:400px;">
                        <?php echo $item->MISC . $ch ?>
                    </td>
                </tr>
                <?php else : ?>
                    <?php if($ch != '') : ?>
                    <tr>
                        <td colspan="11" style="width:400px; border-bottom:1px solid #777;">
                            <?php echo $ch ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endif; ?>
                
                <?php endforeach; ?>
            <?php endif; ?>
            </table>
            
        </div>
    </body>
</html>