jQuery(document).ready(function($){
	$('.otzifOpen').click(function(){
		$('#otzif').slideToggle('fast');
	});
	$('#butshowcont').click(function(){
		$('#butshowcont').hide('');
		$('#moderform').show('');
		jQuery.ajax({
			type: "POST",
			url: 'index.php?option=com_sttnmls&task=showcont&format=raw',
			data: {typeobj: typeobj, cardnum:cardnum, compid:compid}
		}).done(function( response ) {
				var res=response.split('|');
				jQuery("#hidedop").html(res[0]);
				jQuery("#hidecont").html(res[1]);
				jQuery("#hidefirm").html(res[2]);
				jQuery("#listmoder").html(res[3]);
				jQuery("#hideud").show();
		});	 
	});
	$('#butmoderform').click(function(){
		var q1=$('#q1').val();
		var q2=$('#q2').val();
		if(q1!=0 || q2!=0) {
			$('#moderform').hide('');
		}
		jQuery.ajax({
			type: "POST",
			url: 'index.php?option=com_sttnmls&task=moderform&format=raw',
			data: {typeobj: typeobj, cardnum:cardnum, compid:compid, q1:q1, q2:q2}
		}).done(function( response ) {
				alert(response);
		});	 
	});

	$('#complsubmit').click(function(){
		var rs=$('#reason').val();
		var desc=$('#description').val();
		var name=$('#complname').val();
		var email=$('#complemail').val();
		if(document.formvalidator.isValid(this.form)) {
			$('#complsubmit').hide('');
			$('#otzif').html("Спасибо за обращение, нам очень важно Ваше мнение");
			jQuery.ajax({
				type: "POST",
				url: 'index.php?option=com_sttnmls&task=complaint&format=raw',
				data: {typeobj: typeobj, cardnum:cardnum, compid:compid, rs:rs, desc:desc, name:name, email:email }
			}).done(function( response ) {
					$('#otzif').html(response);
			});	
		} 
		return false;
	});

	jQuery('#add_favornmls').click(function(index) {
		var tp = jQuery(this).attr("tp");
		var type = jQuery(this).attr("type");
		var cid = jQuery(this).attr("cid");
		var cn = jQuery(this).attr("cn"); 
		var in_favor2 = find_cookie("tp" + tp + "cid" + cid + "cn" + cn); 
		if (in_favor2){
			var expire_date = new Date();
			expire_date.setMonth(expire_date.getMonth() +6);
			var fav_count = get_cookie("fav_count");
			var fav_nums = get_cookie("fav_nums");
			if (fav_nums == null||fav_nums == NaN||fav_nums < 0){
				fav_nums = 1;
			}
			else{
				fav_nums = parseInt(fav_nums) - 1;
			}
			cookies=explode(";",document.cookie);
			name='';
			for(a=0;a<cookies.length;a++)
			{
				find="tp" + tp + "cid" + cid + "cn" + cn;
				ar=explode("=",cookies[a]);
				if(find==ar[1])
				{
					name=ar[0];
				}
			}
			
			setCookie(name, "", { expires: expire_date.toGMTString(), path:"/" })
			document.cookie = "fav_nums=" + fav_nums + ";expires=" + expire_date.toGMTString() +"; path=/;";
			jQuery(this).removeClass("in_fav");
			jQuery('#favor_link a span').text(fav_nums);
		}else{
			var expire_date = new Date();
			expire_date.setMonth(expire_date.getMonth() +6);
			var fav_count = get_cookie("fav_count");
			var fav_nums = get_cookie("fav_nums");
			if (fav_nums == null||fav_nums == NaN||fav_nums < 0){
				fav_count = 1;
				fav_nums = 1;
			}
			else{
				fav_count = parseInt(fav_count) + 1;
				fav_nums = parseInt(fav_nums) + 1;
			}
			document.cookie = "cardids" + fav_count + "=tp" + tp + "cid" + cid + "cn" + cn +";expires=" + expire_date.toGMTString() +"; path=/;";
			document.cookie = "fav_count=" + fav_count + ";expires=" + expire_date.toGMTString() +"; path=/;";
			document.cookie = "fav_nums=" + fav_nums + ";expires=" + expire_date.toGMTString() +"; path=/;";
			jQuery(this).addClass("in_fav");
			jQuery('#favor_link a span').text(fav_nums);
		}
	});



	jQuery('#add_favornmls').each(function(index) {  
		var tp = jQuery(this).attr("tp");
		var cid = jQuery(this).attr("cid");
		var cn = jQuery(this).attr("cn"); 
		var in_favor = find_cookie("tp" + tp + "cid" + cid + "cn" + cn); 
		if (in_favor){
			jQuery(this).addClass("in_fav");  

		}
	}); 
});

window.addEvent('domready', function(){
		document.formvalidator.setHandler('sttnum', function(value) {
			var v=value.replace(',', '.');
			if(parseFloat(v)==0) return false;
			regex=/^-?\d+((\.|\,)\d+)?$/;
			return regex.test(v);
		}
		)
	}
);


function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires*1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) { 
  	options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for(var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];    
    if (propValue !== true) { 
      updatedCookie += "=" + propValue;
     }
  }

  document.cookie = updatedCookie;
}




function explode( delimiter, string ) {
	var emptyArray = { 0: '' };
	if ( arguments.length != 2
		|| typeof arguments[0] == 'undefined'
		|| typeof arguments[1] == 'undefined' )
	{
		return null;
	}
	if ( delimiter === ''
		|| delimiter === false
		|| delimiter === null )
	{
		return false;
	}
	if ( typeof delimiter == 'function'
		|| typeof delimiter == 'object'
		|| typeof string == 'function'
		|| typeof string == 'object' )
	{
		return emptyArray;
	}
	if ( delimiter === true ) {
		delimiter = '1';
	}
	return string.toString().split ( delimiter.toString() );
}



function find_cookie(value){
    if (document.cookie.search(value) != -1){
        return true;
    }
    else{
        return false;
    }
}

ymaps.ready(init);
function init () {
	ymaps.geocode(g_u, {results: 1}).then(function (res) {
		var firstGeoObject = res.geoObjects.get(0);
		var coords = firstGeoObject.geometry.getCoordinates()

		window.myMap = new ymaps.Map("YMapsID", {
			center: coords,
			zoom: _zoom
		});

		myPlacemark = new ymaps.Placemark(coords,{
			balloonContentBody: g_u
		},{preset: 'twirl#buildingsIcon'});

		myMap.controls
			.add('smallZoomControl', {left: 5, top: 185})
			.add('mapTools');

		myMap.geoObjects.add(myPlacemark);

	});
}
