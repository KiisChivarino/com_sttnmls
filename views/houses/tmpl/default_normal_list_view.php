<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;
$sort_icon = ' <span class="glyphicon glyphicon-sort-by-attributes' . (($orderDirn == 'desc') ? '-alt' : '') . '"></span>';

?>

<div class="row-fluid" style="margin-bottom: 20px;">
    <div class="col-md-12">
        <h5><?php echo JText::_('COM_STTNMLS_LABEL_SORT_BY_FIELD') ?>:</h5>
    </div>
    <ul class="nav nav-pills">
        <li role="presentation"<?php echo (($orderCol == 'a.DATEINP') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_DATE') . (($listOrder == 'o.DATEINP') ? $sort_icon : ''), 'o.DATEINP', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'PICCOUNT') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PHOTO') . (($listOrder == 'PICCOUNT') ? $sort_icon : ''), 'PICCOUNT', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'o.AAREA') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_SQUARE') . (($listOrder == 'o.AAREA') ? $sort_icon : ''), 'o.AAREA', $listDirn, $listOrder); ?>
        </li>
        <li role="presentation"<?php echo (($orderCol == 'o.PRICE') ? ' class="active"' : '')?>>
            <?php echo JHtml::_('grid.sort',  JText::_('COM_STTNMLS_LABEL_SORT_BY_PRICE') . (($listOrder == 'o.PRICE') ? $sort_icon : ''), 'o.PRICE', $listDirn, $listOrder); ?>
        </li>
    </ul>
</div>
<!-- Objects Container -->
<div class="clearfix objects-list">
<?php if($this->items) : ?>
    <?php foreach ($this->items as $item) : ?>
    <?php
        $link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'house', '&view=house&type=card&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);
        $linkstreet = SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&street_id=' . $item->STREETID);
        $linkedit = SttNmlsHelper::getSEFUrl('com_sttnmls', 'house' , '&task=edit&cn=' . $item->CARDNUM . '&cid=' . $item->COMPID);

        if($item->upobj) $style .= ' upobj';
        if($item->expired !=1) $style = 'neprod';
    ?>
    <div class="col-sm-6 col-md-4 object object-house" id="<?php echo $item->CARDNUM; ?>_<?php echo $item->COMPID; ?>">
        <div class="thumbnail">
        <?php
            $exclusive = '';
            if($item->EXCLUSIVE == 3) $exclusive = 'exclusive';
            if($item->upobj) $exclusive .= ' upobj';
            if($exclusive) $exclusive =' class="' . $exclusive . '"';
        ?>
            <div class="object-photo">
                <a href="<?php echo $link; ?>" <?php echo $exclusive; ?> target="_blank">
                <?php 
                    $pics = explode(';', $item->PICTURES);
                    $wm = '';
                    if($item->VTOUR && $item->VTOUR!='sWebLabel1') $wm='wm';
                    $img_url = SttNmlsHelper::getLinkPhoto($pics[0], 'editfirst' . $wm);
                ?>
                    <img class="img-rounded" src="<?php echo $img_url; ?>" alt="" />
                </a>
                <?php/* if($item->editable && ($item->flag == 1 OR $item->flag == 2) && !$item->upobj) : ?>
                    <a class="upobj picplat" href="<?php echo $link . '#plat'; ?>" target="_blank" title="<?php echo JText::_('COM_STTNMLS_PLAT'); ?>">
                        &nbsp;
                    </a>
                <?php endif; */?>
                <a href="<?php echo $link ?>" class="btn btn-success btn-sm object-view-button" target="_self">
                    <span class="glyphicon glyphicon-search"></span>
                    <?php echo JText::_('COM_STTNMLS_LABEL_VIEW') ?>
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $link ?>" class="objectdesc object-link" target="_self" title="">
                <?php if($item->WHAT == 0) : ?>    
                    <?php printf('%s %s <b>%s</b>м<sup>2</sup>', $item->tip, $item->mat, round($item->AAREA, 0)) ?><br />
                <?php endif; ?>
                    <?php printf('%s <b>%s</b> %s', JText::_('COM_STTNMLS_UCHASTOK'), round($item->EAREA, 0), JText::_('COM_STTNMLS_SOTOK')) ?>
                </a>
                <div class="object-public-info">
                    <p class="to-left">
                        <span class="glyphicon glyphicon-calendar"></span>
                        <?php echo SttNmlsHelper::GetRuDate($item->DATEINP); ?>
                    </p>
                    <p class="to-right">
                        <span class="glyphicon glyphicon-eye-open"></span>
                        <?php echo $item->watchcount; ?>
                    </p>
                </div>
                <div class="statusLabels">
                    <?php if($item->close == 1) : ?>
                        <div style="text-align:center;">
                            <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 1)) : ?>
                        <div style="text-align:center;">                    
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&f_realtors=1') ?>" 
                               class="label label-info" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>"
                            ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE');?></a>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(($currentAgent->COMPID != $compidsob AND !$this->user->guest) and ($item->realtors_base == 2)) : ?>
                        <div style="text-align:center;">
                            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'houses', '&f_realtors_and_open=1') ?>" 
                               class="label label-primary" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE') ?>"
                            ><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE');?></a>
                        </div>
                    <?php endif; ?>
                    
                    <?php if($item->expired != 1) : ?>
                        <div style="text-align:center;">
                            <div class="label label-default" data-toggle="tooltip" data-placement="top" title="<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_EXPIRED') ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if($item->checked == 1) : ?>
                        <div style="text-align:center;">
                            <span class="label label-success">
                                <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_CHECKED') ?>
                            </span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="object-description">
                <span>
                    <?php
                    $s = $item->gorod;
                    if($item->raion != '' && $item->RAIONID>1) {
                        $r = str_replace('р-н', '', $item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($item->mraion != '') {
                        $s .= ', ' . $item->mraion;
                    }
                    echo $s;
                    ?>
                </span>
                <a href="<?php echo $linkstreet; ?>" title="<?php echo JText::_('COM_STTNMLS_ULICA_DESC') ?>">
                    <b><?php echo $item->ulica; ?></b>
                </a>
                <p>
                    <?php if($item->EXCLUSIVE > 0) : ?>
                        <img src="<?php echo JURI::root() ?>components/com_sttnmls/assets/images/icon_rating.png" style="position:relative; top:2px;"/>
                    <?php endif; ?>
                    <?php echo $item->firm; ?>
                </p>
            </div>
            <div class="object-price">
                <div class="price text-right clearfix">
                    <span class="value">
                    <?php echo number_format($item->PRICE, 0, ' ', ' '); ?>
                    </span>
                    <?php if($item->percent > 0) : ?>
                        <div>
                            <span class="label label-danger">
                                <span class="glyphicon glyphicon-arrow-up"></span>
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_MORE_MIDDLE', round($item->percent, 2)) ?>
                            </span>
                        </div>
                    <?php endif; ?>
                    <?php if($item->percent < 0) : ?>
                        <div>
                            <span class="label label-success">
                                <span class="glyphicon glyphicon-arrow-down"></span>
                                <?php echo JText::sprintf('COM_STTNMLS_LABEL_PRICE_LESS_MIDDLE', round($item->percent, 2)*-1) ?>
                            </span>
                        </div>
                    <?php endif; ?>
                    <a href="javascript:void(0);" class="addToFavorite favorite-star to-left" data-toggle="button" data-tp="<?php echo $this->tp; ?>" data-cid="<?php echo $item->COMPID; ?>" data-type="aparts" data-cn="<?php echo $item->CARDNUM; ?>" aria-pressed="false" autocomplete="off">
                        <!--                        Необходимо добавить ссылке класс active если объект надохится в избранном -->
                        <span class="glyphicon glyphicon-star"></span>
                    </a>
                </div>
            </div>
            <?php if($item->editable && ($item->flag == 1 OR $item->flag == 2)) : ?>
                <div class="picsmang edit-button-block">
                    <a href="<?php echo $linkedit; ?>" class="picedit btn btn-warning btn-sm" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <a class="btn btn-info btn-sm picdate" title="<?php echo JText::_('COM_STTNMLS_EDITDATE'); ?>" onclick="changeDate(<?php echo $item->CARDNUM; ?>, <?php echo $item->COMPID; ?>,'house');">
                        <span class="glyphicon glyphicon-time"></span>
                    </a>
                    <a class="btn btn-danger btn-sm picdelete" title="<?php echo JText::_('COM_STTNMLS_DELETE'); ?>" onclick="return deleteobj(<?php echo $item->CARDNUM; ?>, <?php echo $item->COMPID; ?>,'house');">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
<?php else : ?>
    <?php if($this->noitemtext) : ?>
    <p class="text-center">
        <?php echo $this->noitemtext; ?>
    </p>
    <?php endif; ?>   
<?php endif; ?>
</div>
<!-- /Objects Container -->