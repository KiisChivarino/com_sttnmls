<?php
defined('_JEXEC') or die('Restricted access');

class SttNmlsModelSber extends JModelLegacy {
	public $_item;

	public function getItem()
	{
		$num = JRequest::getInt('id',0);
		if(!$num) return null;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('f.* ');
		$query->from(' #__sttnmlssber AS f ');
		$query->where(' f.id='.$db->quote($cid));
		$db->setQuery($query);
		$this->_item=$db->loadObject();
		return $this->_item;
	}
	function save(){
		$app = JFactory::getApplication();
		$plugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha', 0));
		if($plugin) {
			$captcha = JCaptcha::getInstance($plugin);
			if($captcha) {
				if (!$captcha->checkAnswer(0)){
					$redirect = JRoute::_('index.php?option=com_sttnmls&view=sbers', false);
					$app->redirect($redirect, JText::_('COM_STTNMLS_ANTISPAMBAD'));
					return;
				}
			}
		}
		$data  = JRequest::getVar('jform', array(), 'post', 'array');
		$new=1;
		$db = $this->getDbo();
		if($data['id'])	$new=0;
		$query = $db->getQuery(true);
		$data['descr'] = '';
		if(!$new){
			$query->update($db->quoteName('#__sttnmlssber'));
			$query->where(' id='.$db->quote($data['id']));
		} else {
			$query->insert($db->quoteName('#__sttnmlssber'));
			$query->set(' `dateinp`=now() ');
		}
		foreach($data as $key=>$val)
		{
			$query->set( $db->quoteName($key) . '=' . $db->quote($val));
		}
		$db->setQuery($query);
		$db->query();		
		if ($db->getErrorNum()){
			$redirect = JRoute::_('index.php?option=com_sttnmls&view=sbers', false);
			$app->redirect($redirect,  $db->getErrorMsg());
			return;
		}
		if($new) {
			$data['id'] = $db->insertid();
			$this->sendmail($data);
		}
		$redirect = JRoute::_('index.php?option=com_sttnmls&view=sbers', false);
		$app->redirect($redirect, JText::_('COM_STTNMLS_SBER_SENDED'));
		return;
	}
	public function sendmail($data)
	{
		$app = JFactory::getApplication();
		$mailfrom = $app->getCfg('mailfrom');
		$fromname = $app->getCfg('fromname');
		$subject = JText::sprintf('COM_STTNMLS_SBER_EMAIL_SUBJECT', $fromname);
		$body = JText::sprintf('COM_STTNMLS_SBER_EMAIL_BODY', $fromname, $data['fio'], $data['phone']);
		$params = JComponentHelper::getParams('com_sttnmls');
		$sberemail = $params->get('sberemail', '');
		if($sberemail) {
			$emails = explode(',', $sberemail);
			foreach( $emails as $email )
			{
				if(trim($email)){
					$mail = JFactory::getMailer();
					$mail->isHTML(true);
					$mail->addRecipient(trim($email));
					$mail->setSender(array($mailfrom, $fromname));
					$mail->setSubject($subject);
					$mail->setBody($body);
					$sent = $mail->Send();
				}
			}  
		}
	}
}
