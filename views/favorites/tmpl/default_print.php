<?php defined('_JEXEC') or die('Restricted access');

$params = JComponentHelper::getParams('com_sttnmls');

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <title><?php echo JText::_('COM_STTNMLS_LABEL_PRINT') ?></title>
        <link href="/templates/protostar/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    </head>
    <body>
        <div style="font-family:Arial; width:900px; margin:auto; font-size:11px;">
        <?php if($this->agent) : ?>
            <h2 style="text-align:right;">
                <?php printf('%s %s %s, %s, %s', $this->agent->SIRNAME, $this->agent->NAME, $this->agent->SECNAME, $this->agent->PHONE, $this->agent->EMAIL) ?>
            </h2>
        <?php else : ?>
            <h2 style="text-align:right;">
                <?php printf('%s, %s, %s, %s', $params->get('site.title'), $params->get('site.phone'), $params->get('site.email'), $params->get('site.url')) ?>
            </h2>
        <?php endif; ?>
            <br />
            <br />

        <?php
            // Тип кукиса для объекта типа "Квартира/Комната"
            if(in_array($this->tp, array(0, 1)) && isset($this->items[1])) {
                echo $this->loadTemplate('favorites_tp1');
            }

            // Тип кукиса для объекта типа "Дом/Участок"
            if(in_array($this->tp, array(0, 3)) && isset($this->items[3])) {
                echo $this->loadTemplate('favorites_tp3');
            }

            // Тип кукиса для объекта типа "Коммерческая недвижимость"
            if(in_array($this->tp, array(0, 4)) && isset($this->items[4])) {
                echo $this->loadTemplate('favorites_tp4');
            }

            // Тип кукиса для объекта типа "Гараж"
            if(in_array($this->tp, array(0, 5)) && isset($this->items[5])) {
                echo $this->loadTemplate('favorites_tp5');
            }
        ?>
        </div>
    </body>
</html>