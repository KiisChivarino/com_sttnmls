<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
//$user =& JFactory::getUser();
//$session = JFactory::getSession();
//$agency_sobstv = $params->get('compidsob', false);
//$compidsob = $params->get('compidsob', '0');
$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();

// Vars from request
$agency = $app->input->getUint('agency', 0);
$agent = $app->input->getUint('agent', 0);
$stype = $app->input->getUint('stype', 0);
$what = $app->input->getUint('what', 0);
$city_id = $app->input->getUint('city_id', 0);
$raion_id = $app->input->getUint('raion_id', 0);
$mraion_id = $app->input->getUint('mraion_id', 0);


$filter_rooms = $this->state->get('filter.rooms', array());
$filter_mw = $this->state->get('filter.mw', array());
$city = (($city_id) ? $city_id : $this->state->get('filter.city', 0));
$filter_raions = (($raion_id) ? array($raion_id) : $this->state->get('filter.raions', array()));
$filter_mraions = (($mraion_id) ? array($mraion_id) : $this->state->get('filter.mraions', array()));
$filter_area_ot = $this->state->get('filter.area_ot', 0);
$filter_area_do = $this->state->get('filter.area_do', 0);
$filter_kitch_ot = $this->state->get('filter.kitch_ot', 0);
$filter_kitch_do = $this->state->get('filter.kitch_do', 0);
$filter_price_ot = $this->state->get('filter.price_ot', 0);
$filter_price_do = $this->state->get('filter.price_do', 0);
$filter_view_type = $this->state->get('filter.view_type', 0);
$filter_newbuild = $this->state->get('filter.newbuild_selector', 2);


?>

    <input type="hidden" name="act" value="" id="actfil"/>
    <input type="hidden" name="typepdf" value="" id="typepdf"/>
    <input type="hidden" name="what" value="<?php echo $what ?>" id="whatfil"/>
    <input type="hidden" name="stype" value="<?php echo $stype; ?>" />
    <input type="hidden" name="UPD" value="" id="newsearch" />
    <input type="hidden" name="RST" value="" id="resetform" />

    <input type="hidden" name="filter_mw[]" value="null"/>
    <input type="hidden" name="filter_rooms[]" value="null"/>
    <input type="hidden" name="filter_raions[]" value="null"/>
    <input type="hidden" name="filter_mraions[]" value="null"/>
    <input type="hidden" name="filter_exclusive" value="0" />
    <input type="hidden" name="filter_internal_base" value="0" />
    <input type="hidden" name="filter_realtors_base" value="0" />
    <input type="hidden" name="filter_realtors_and_open" value="0" />
    <input type="hidden" name="filter_price_day" value="0" />
    <input type="hidden" name="filter_price_hour" value="0" />
    <input type="hidden" name="filter_archive_mode" value="0" />

    <input type="hidden" id="city_field" name="city" value="<?php echo $city; ?>" />

<?php if($this->access->isAdmin OR $this->access->isModerator) : ?>
    <input type="hidden" name="filter_expired" value="0" />
    <input type="hidden" name="filter_owner" value="0" />
<?php endif; ?>
    
    <ul class="nav nav-tabs">
        <li role="presentation"<?php echo ((!$stype) ? ' class="active"' : '') ?>>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : ''), 'default', array('filter_rooms' => 0, 'stype' => 0, 'what' => $what)) ?>">
                <span class="glyphicon glyphicon-rub"></span>
                <?php echo JText::_('COM_STTNMLS_SELL'); ?>
            </a>
        </li>
        <li role="presentation"<?php echo (($stype) ? ' class="active"' : '') ?>>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'aparts', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : ''), 'default', array('filter_rooms' => 0, 'stype' => 1, 'what' => $what)) ?>">
                <span class="glyphicon glyphicon-calendar"></span>
                <?php echo JText::_('COM_STTNMLS_RENT'); ?>
            </a>
        </li>
        <li role="presentation">
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites')?>" class="favorite-link">
                 <span class="glyphicon glyphicon-star"></span><span class="countFavor">0</span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-content-wrapper">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_SQUPR'); ?></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="text-left control-label col-xm-4 col-xs-12 col-md-4 col-sm-4"><?php echo JText::_('COM_STTNMLS_AREAOT'); ?></label>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_area_ot" class="form-control input-sm sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $sq = explode(",", $params->get('sq_aparts_sell', ''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="'.$i.'" ';
                                            if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }else{
                                        $sq = explode(",",$params->get('sq_aparts_arenda',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-xm-4 col-xs-6 col-sm-4">
                                <select name="filter_area_do" class="form-control input-sm  sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $sq = explode(",", $params->get('sq_aparts_sell',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }else{
                                        $sq = explode(",", $params->get('sq_aparts_arenda',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-left col-sm-4 col-md-4 col-xs-12 col-xm-4 control-label"><?php echo JText::_('COM_STTNMLS_KITCHOT'); ?></label>
                            <div class="col-md-4 col-sm-4 col-xm-4 col-xs-6">
                                <select name="filter_kitch_ot" class="form-control input-sm  sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $ar_k = explode(",", $params->get('sq2_aparts_sell',''));
                                    }else{
                                        $ar_k = explode(",", $params->get('sq2_aparts_arenda',''));
                                    }

                                    foreach($ar_k as $i)
                                    {
                                        echo'<option value="' . $i . '" ';
                                        if($filter_kitch_ot == $i && $filter_kitch_ot != 0) echo 'selected';
                                        echo'>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xm-4 col-xs-6">
                                <select name="filter_kitch_do" class="form-control input-sm  sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    foreach($ar_k as $i)
                                    {
                                        echo'<option value="' . $i . '" ';
                                        if($filter_kitch_do == $i && $filter_kitch_do != 0) echo 'selected';
                                        echo'>' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-left col-xm-4 col-md-4 col-sm-4 col-xs-12 control-label"><?php echo JText::_('COM_STTNMLS_PRICEOT'); ?></label>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_ot" class="form-control input-sm sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $array_price = explode(",",$params->get('price_aparts_sell',''));
                                    }else{
                                        $array_price = explode(",",$params->get('price_aparts_arenda',''));
                                    }
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_ot == $one_price && $filter_price_ot != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_do" class="form-control input-sm sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_do == $one_price && $filter_price_do != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-6">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_ROOMS'); ?></div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="1"<?php if(array_search(1, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 1
                        </label>
                    </div>
                    <div class="checkbox">    
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="2"<?php if(array_search(2, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 2
                        </label>
                    </div>    
                    <div class="checkbox">    
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="3"<?php if(array_search(3, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 3
                        </label>
                    </div>    
                    <div class="checkbox">    
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="4"<?php if(array_search(4, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 4+
                        </label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-5 col-xs-6">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_MW'); ?></div>
                    <div id="mwtypes">
                        <div name="filter_my_checkMW[]" multiple="multiple" size="3">
                            <div class="checkbox">    
                                <label>
                                    <input type="checkbox" name="filter_mw[]" value="80"<?php if(array_search(80, $filter_mw)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_MW80'); ?>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="filter_mw[]" value="82"<?php if(array_search(82, $filter_mw)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_MW82'); ?>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="filter_mw[]" value="83"<?php if(array_search(83, $filter_mw)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_MW83'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_EXT_FILTER'); ?></div>
                    <div class="form-horizontal">
                    <?php if($this->access->isLogin): ?>
                        <div class="form-group">
                            <!--Группировка фильтров: БАЗА-->
                            <?php
                                $filter_realtors_and_open = $this->state->get('filter.realtors_and_open', FALSE);
                                $filter_realtors_base = $this->state->get('filter.realtors_base', FALSE);
                                $filter_internal_base = $this->state->get('filter.internal_base', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">База</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                name="<?php if(($currentAgent->COMPID != $compidsob)) {
                                        echo (($filter_realtors_and_open or $_GET['f_realtors_and_open']=='1') ? 'filter_realtors_and_open' : '');
                                        echo (($filter_realtors_base or $_GET['f_realtors']=='1') ? 'filter_realtors_base' : '');
                                    }else{
                                        echo (($filter_internal_base) ? 'filter_internal_base' : '');
                                   }
                                ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>
                                <?php if(($currentAgent->COMPID != $compidsob)) { ?>

                                    <!--База агентов+открытая база-->
                                    <option name="filter_realtors_and_open" value="1"
                                        <?php echo (($filter_realtors_and_open or $_GET['f_realtors_and_open']=='1') ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE') ?>
                                    </option>

                                    <!--База агентов-->
                                    <option name="filter_realtors_base" value="1" 
                                        <?php echo (($filter_realtors_base or $_GET['f_realtors']=='1') ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE') ?>
                                    </option>
                                <?php }?>

                                    <!--Закрытая база-->
                                    <option name="filter_internal_base" value="1"
                                        <?php echo (($filter_internal_base) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_INTERNAL_DATABASE') ?>
                                    </option>

                                </select>
                            </div>
                        </div>
                    <?php endif;?>                   
                    <?php if ($this->access->isLogin){ ?>
                        <div class="form-group">
                            <!--Группировка фильтров: СТАТУС-->
                            <?php
                                $filter_expired = $this->state->get('filter.expired', FALSE);
                                $filter_archive_mode = $this->state->get('filter.archive_mode', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">Статус</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                    name="<?php
                                        if($this->access->seeExpiredObjects){
                                            echo (($filter_expired) ? 'filter_expired' : '');
                                        } else {
                                            echo (($filter_archive_mode) ? 'filter_archive_mode' : '');
                                        }
                                    ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>

                                <?php if($this->access->seeExpiredObjects){ ?>
                                    <!--Непродленные-->
                                    <option name="filter_expired" value="1" 
                                        <?php echo (($filter_expired) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_FILTR_NEPROD') ?>
                                    </option>
                                <?php }?>

                                    <!--Архив-->
                                    <option name="filter_archive_mode" value="1"
                                        <?php echo (($filter_archive_mode) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_ARCHIVE_MODE') ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    <?php }?>
                        <div class="form-group">
                            <!--Группировка фильтров: ОСОБЫЕ-->
                            <?php
                                $filter_owner = $this->state->get('filter.owner', FALSE);
                                $filter_exclusive = $this->state->get('filter.exclusive', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">Особые</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                    name="<?php
                                        if($this->access->seeContactsObjects){
                                            echo (($filter_owner) ? 'filter_owner' : '');
                                        }else{
                                            echo (($filter_exclusive) ? 'filter_exclusive' : '');
                                        }
                                    ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>

                                <?php if($this->access->seeContactsObjects){ ?>
                                    <!--Собственники-->
                                    <option name="filter_owner" value="1" 
                                        <?php echo (($filter_owner) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_FILTR_SOBST') ?>
                                    </option>
                                <?php }?>

                                    <!--Эксклюзив-->
                                    <option name="filter_exclusive" value="1"
                                        <?php echo (($filter_exclusive) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_EXCLUSIVE') ?>
                                    </option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />

            <a href="javascript:void(0);" class="showDopFilters<?php echo (($this->params->get('dopfilter', 0)) ? ' showed' : '') ?>" data-showed-text="<?php echo JText::_('COM_STTNMLS_DNDOPF'); ?>" data-closed-text="<?php echo JText::_('COM_STTNMLS_UPDOPF'); ?>"><?php echo JText::_('COM_STTNMLS_UPDOPF'); ?></a>
            
            <div class="dop_filters"<?php echo (($this->params->get('dopfilter', 0)) ? ' style="display: block;"' : '') ?>>
                <div class="row">
                    <!-- Select REGION or CITY -->
                    <div class="col-md-4 extend-filter-col">
                        <div class="cityselect">
                            <select name="filter_city" id="filter_city" class="form-control input-sm" onchange="changecity(this);jQuery('input#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');">
                                <?php echo JHtml::_('select.options', $this->cityopt, 'value', 'text', $city);?>
                            </select>
                        </div>
                    </div>
                    <!-- /Select REGION or CITY -->
                    
                    <!-- Select STREET -->
                    <div class="col-md-4 extend-filter-col">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="streetFilter" name="filter_street" value="<?php echo $this->state->get('filter.street') ?>" placeholder="<?php echo JText::_('COM_STTNMLS_STREET_SELECT') ?>" autocomplete="false" />
                            <span class="input-group-btn">
                                <a href="javascript:void(0);" class="btn btn-default btn-sm clearStreetFilter"><b>x</b></a>
                            </span>
                        </div>
                    </div>
                    <!-- /Select STREET -->

                    <!-- Search PHONE -->
                    <div class="col-md-4 extend-filter-col">
                        <input type="text" class="form-control input-sm" value="<?php echo $this->state->get('filter.phone') ?>" name="filter_phone" id="phone_fil" placeholder="Поиск по телефону" />
                    </div>
                    <!-- /Search PHONE -->
                </div>
                 
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-4 col-sm-6 checkbox-list-container">
                        <button id="regionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="raion">
                            <?php echo JText::_('COM_STTNMLS_OKRUG'); ?>
                            <span class="caret"></span>
                        </button>
                        <div id="raion" class="dropdown-menu dropdown-menu-checkbox">
                        <?php foreach($this->raions as $raion) : ?>
                            <div>
                                <label>
                                    <input type="checkbox" class="r_check" name="filter_raions[]" value="<?php echo $raion->value; ?>" <?php if(array_search($raion->value, $filter_raions)!==false) echo ' checked="checked" '; ?> onclick="clickraion();"/>
                                    <?php echo $raion->text; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 checkbox-list-container">
                        <button id="mraionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="mraion">
                            <?php echo JText::_('COM_STTNMLS_MRAIONS'); ?>
                            <span class="caret"></span>
                        </button>
                        <div id="mraion" class="dropdown-menu dropdown-menu-checkbox">
                        <?php foreach($this->mraions as $mraion) : ?>
                            <div>
                                <label>
                                    <input type="checkbox" class="mr_check" name="filter_mraions[]" value="<?php echo $mraion->value; ?>" <?php if(array_search($mraion->value, $filter_mraions)!==false) echo ' checked="checked" '; ?>/>
                                    <?php echo $mraion->text; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>

                    <?php if($stype) : ?>
                    <div class="col-md-4 col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="filter_price_day" value="1"<?php echo (($this->state->get('filter.price_day')) ? ' checked="checked"' : '') ?> />
                            <?php echo JText::_('COM_STTNMLS_LABEL_RENT_BY_DAY') ?>
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="filter_price_hour" value="1"<?php echo (($this->state->get('filter.price_hour')) ? ' checked="checked"' : '') ?> />
                            <?php echo JText::_('COM_STTNMLS_LABEL_RENT_BY_HOUR') ?>
                        </label>
                    </div>
                    <?php else : ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class="form-control" name="filter_newbuild_selector">
                            <option value="2"<?php echo (($filter_newbuild == 2) ? 'selected="selected"' : '') ?>><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_2') ?></option>
                            <option value="0"<?php echo (($filter_newbuild == 0) ? 'selected="selected"' : '') ?>><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_0') ?></option>
                            <option value="1"<?php echo (($filter_newbuild == 1) ? 'selected="selected"' : '') ?>><?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_NEWBUILD_1') ?></option>
                        </select>
                    </div>
                    <?php endif; ?>
                    <div class="visible-sm-inline-block visible-xs-inline-block">
                        <br />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-push-3 col-md-offset-3 col-sm-6 col-sm-push-6 col-xs-12">
                    <button type="submit" class="btn btn-warning btn-block to-right" onclick="document.getElementById('actfil').value='';document.getElementById('newsearch').value='search';">
                        <?php echo JText::_('COM_STTNMLS_SEARCH'); ?>
                    </button>
                </div>
                <div class="col-md-3 col-md-pull-3 col-sm-6 col-sm-pull-6 col-xs-12">
                    <button type="submit" class="btn btn-default btn-block to-left" onclick="document.getElementById('adminForm').target='_self'; document.getElementById('resetform').value='1'; document.getElementById('actfil').value=''; document.getElementById('filter_easy').value=0; document.getElementById('phone_fil').value=''; document.getElementById('close').checked=false;">
                        <?php echo JText::_('COM_STTNMLS_RESET'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="active-filters">
        <?php echo $this->loadTemplate('filters_list'); ?>
    </div>
    