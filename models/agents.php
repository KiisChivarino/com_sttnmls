<?php defined('_JEXEC') or die('Restricted access');

//
require_once( JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php' );

class SttNmlsModelAgents extends JModelList
{

	protected function getListQuery()
	{
		// JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

		// State VARS
		$_filter_boss = $this->getState('filter.boss', FALSE);
		$filter_fio = $this->getState('filter.fio', '');
		$filter_cat_id = $this->getState('filter.catid', 0);
		$filter_compid = $this->getState('filter.compid', 0);


		// ID группы собственников из настроек комопнента
		$grp_owners = $params->get('groupid1', 0);

		// ID организации собственников (псевдоорганизация) из настроек компонента
		$comp_id_owners = $params->get('compidsob', 0);



		$query = $db->getQuery(TRUE);
		$query->select($db->quoteName('ag') . '.*, ' . $db->quoteName('f.NAME', 'firm') . ', ' . $db->quoteName('f.realname', 'firm_realname'));
		$query->from($db->quoteName('#__sttnmlsvocagents', 'ag'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON ' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('f.id'));
		$query->where($db->quoteName('ag.flag') . '<3 AND ' . $db->quoteName('ag.flag') . '<>1');
		$query->where($db->quoteName('ag.grp') . '<>' . $db->quote($grp_owners));
		$query->where('TRIM(' . $db->quoteName('ag.PHONE') . ')<>""');
        $query->where('TRIM(' . $db->quoteName('ag.SIRNAME') . ')<>""');
		$query->where($db->quoteName('f.active') . '=' . $db->quote(1));
        $query->where($db->quoteName('ag.f_exclude_rating') . '=' . $db->quote(0));


		//> Передан ID организации
		$this->setAdditionalWhereFromState($query, 'compid', 'ag.COMPID');

		//> Передан флаг поиска по боссам
		if($_filter_boss) {
			$query->where($db->quoteName('ag.boss') . '=' . $db->quote(1));
		}

        //> Поиск по телефону
        $this->setAdditionalWhereFromState($query, 'phone', 'ag.phones', 'LIKE');

		//> Поиск по ФИО
		if($filter_fio != '') {
			$query->where('(' .
				$db->quoteName('ag.SIRNAME') . ' LIKE ' . $db->quote('%' . $filter_fio . '%') . ' OR ' .
				$db->quoteName('ag.NAME') . ' LIKE ' . $db->quote('%' . $filter_fio . '%') . ' OR ' .
				$db->quoteName('ag.SECNAME') . ' LIKE ' . $db->quote('%' . $filter_fio . '%') .
				')'
			);
		}


		if($filter_compid) {
			$query->where($db->quoteName('ag.COMPID') . '=' . $db->quote($filter_compid));
		} else {
			if($filter_cat_id) {
				$query->join('LEFT', $db->quoteName('#__sttnmlsfirmcatf', 'fc') . ' ON (' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('fc.idfirm') . ')');
				$query->where($db->quoteName('fc.idcatf') . '=' . $db->quote($filter_cat_id));
			}
		}
		$query->where($db->quoteName('ag.COMPID') . '<>' . $db->quote($comp_id_owners));


		if($app->input->get('sort', '', 'String') == 'by_name') {
			$query->order($db->quoteName('ag.SIRNAME'));
			$query->order($db->quoteName('ag.NAME'));
		} else {
			$query->order($db->quoteName('ag.f_have_avatar') . ' DESC');
			$query->order($db->quoteName('ag.activity') . 'DESC');
			$query->order($db->quoteName('ag.SIRNAME'));
		}

//        print_r((string)$query);exit;

//		print_r($this->getState());exit;
		
		return $query;
	}

	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		$this->setState('list.limit', $limit);
		$this->setState('list.start', JRequest::getUInt('limitstart', 0));

		$this->setFilterState('boss', FALSE, 'bool');
        $this->setFilterState('catid', 0, 'uint');
		$this->setFilterState('compid', 0, 'uint');
		$this->setFilterState('fio', '', 'string');
        $this->setFilterState('phone', '', 'string');
	}

	protected function setAdditionalWhereFromState($qInstance, $var, $tField, $condition = '=')
	{
		$db = JFactory::getDbo();
		$format = JFactory::getApplication()->input->getString('format', 'html');

		if($format == 'raw') {
			//> Исключается из формирования, если запрос AJAX или только самого содержимого
			return FALSE;
		}

		$sVar = $this->getState('filter.' . $var);
		if($sVar) {
			//> Переменная есть в сессии
			if($condition == 'LIKE') {
				//> Запрос поиска по LIKE
				$qInstance->where($db->quoteName($tField) . ' LIKE ' . $db->quote('%' . $sVar . '%'));
			} else {
				//> Обычный строгий запрос
				$qInstance->where($db->quoteName($tField) . $condition . $db->quote($sVar));
			}
		}
	}

	public function setAdditionalWhereFromStateCheckbox($qInstance, $var, $tField)
	{
		$db = JFactory::getDbo();
		$format = JFactory::getApplication()->input->getString('format', 'html');

		if($format == 'raw') {
			//> Исключается из формирования, если запрос AJAX или только самого содержимого
			return FALSE;
		}

		$sVar = $this->getState('filter.' . $var);
		if($sVar && is_array($sVar)) {
			//> Переменная есть в сессии и является массивом
			foreach($sVar as $k => $v) {
				//> Проверка значений массива на предмет целочисленных значений
				if(!(int)$v) {
					//> Значение не является целочисленным или равно нулю - убираем из массива
					unset($sVar[$k]);
				}
			}

			if($sVar) {
				//> В массиве после фильтрации остались значения
				$qInstance->where($db->quoteName($tField) . ' IN (' . implode(',', $sVar) . ')');
			}
		}
	}

	protected function setFilterState($var, $default = NULL, $type = 'none')
	{
		$app = JFactory::getApplication();

//		if($this->_updateFilterState) {
			$val = $this->getUserStateFromRequest($this->context . '.filter.' . $var, 'filter_' . $var, $default, $type);
//		} else {
//			$val = $app->getUserState($this->context . '.filter.' . $var, $default);
//		}

		if(is_array($default) && !is_array($val)) {
			$rval = array();
			$rval[] = $val;
		} else {
			$rval = $val;
		}

		$this->setState('filter.' . $var, $rval);
	}


    public function getAgentsList()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsvocagents'));
        $db->setQuery($query);

        return $db->loadObjectList();
    }

//    public function addWhere($var, $field, $query, $like = FALSE)
//    {
//        $search = $this->getState('filter.' . $var, FALSE);
//        if($search) {
//            if($like) { $search = "'%$search%'"; }
//            $query->where($field . $search);
//        }
//    }
//
//    public function addWhereCheckbox($var, $fld, $query)
//    {
//        $search = $this->getState('filter.' . $var);
//        if(count($search) > 1) {
//            $query->where($fld . $search[1]);
//        }
//    }

//	public function setFilterState($par,$default=null)
//	{
//		$a=$this->getUserStateFromRequest($this->context . '.filter.'.$par, 'filter_'.$par, $default);
//		$this->setState('filter.' .$par, $a);
//	}


	public function getCategoriesFirmsList($cat = 0, $order_direction = 'ASC')
	{
		// JOOMLA instances
		$db = JFactory::getDbo();

		$query = $db->getQuery(TRUE);
		$query->select('*');
		$query->from($db->quoteName('#__sttnmlscatf'));
		if($cat) {
			$query->where($db->quoteName('ID') . '=' . $db->quote($cat));
		}
		$query->order($db->quoteName('ID') . ' ' . $order_direction);
		$db->setQuery($query);

		return $db->loadObjectList('id');
	}

	public function getFirmsList($target_category = 0)
	{
		require_once JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php';

		// JOOMLA instances
        $app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_sttnmls');

		// Other VARS
		$result = array();
		$catid_sobstv = $params->get('compidsob', 0);
		$categories_list = $this->getCategoriesFirmsList($target_category);


		if($categories_list) {
			$temp = array();
			foreach($categories_list as $category) {
				$temp[] = $category->id;
			}


			$query = $db->getQuery(TRUE);
			$query->select($db->quoteName('firm') . '.*');
			$query->from($db->quoteName('#__sttnmlsvocfirms', 'firm'));
			$query->join('LEFT', $db->quoteName('#__sttnmlsfirmcatf', 'c') . ' ON(' . $db->quoteName('firm.ID') . '=' . $db->quoteName('c.idfirm') . ')');
			$query->where($db->quoteName('c.idcatf') . ' IN (' . implode(',', $temp) . ')');

			if($catid_sobstv) {
				$query->where($db->quoteName('firm.ID') . '<>' . $db->quote($catid_sobstv));
			}
			$query->where($db->quoteName('firm.active') . '=' . $db->quote(1));
			$query->order($db->quoteName('firm.realname') . ' ASC');
			$db->setQuery($query);

			$firms = $db->loadObjectList('ID');

			if($firms) {
				foreach($firms as $firm) {
					$result[$firm->ID] = $firm;
				}
			}
		}
		return $result;
	}
}
