<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewFirm extends JViewLegacy
{
    function display($tpl = null) 
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        
        $this->user = JFactory::getUser();
        $this->item = $this->get('Item');
        $this->agents = $mAgent->getFirmAgentsList($this->item->ID, FALSE);
        $this->current_agent = $mAgent->getAgentInfoByID();
        //$this->isAdmin = $this->user->authorise('core.admin');
        $this->canViewExpired = (($this->item->ID == $this->current_agent->COMPID) OR $this->isAdmin);
        
        $this->params = JComponentHelper::getParams('com_sttnmls');
        
        $this->isAdmin = $this->user->authorise('core.admin');
        //$this->canViewExpired = (($this->current_agent->COMPID == $this->item->COMPID) OR $this->isAdmin);
        $this->canViewClosed = (($this->current_agent->COMPID == $this->item->COMPID) OR $this->isAdmin);
		$this->canViewRealtorsBase = ($this->current_agent->COMPID!=$this->params->get('compidsob','0') and !$this->user->guest);
        
        if(!$this->item OR $this->item->ID == JComponentHelper::getParams('com_sttnmls')->get('compidsob', 0))
        {
            $app->enqueueMessage('Этой организации нет в списке', 'error');
            $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'firms'));
        }

        $layout = JRequest::getVar('layout', ''); 
        
        if($layout == 'edit')
        {
            if(!JFactory::getUser()->id) {
                $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE'), 'error');
                $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'firms'));
                return FALSE;
            }

            $this->catf = $this->get('Catf');
        } else {
            $session = JFactory::getSession();
            $uri = JFactory::getURI ();
            $url = $uri->toString(array('scheme', 'host', 'port', 'path', 'query', 'fragment')); 
            $session->set('firm.returnurl', $url);
            $this->cats = $this->get('Categories');
            
            $doc->setTitle($this->get('Title'));
            $doc->setDescription($this->get('Desc'));
        }


        parent::display($tpl);
    }
}
