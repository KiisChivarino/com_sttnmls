<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlslocal.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.js');
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jquery.realty.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/com_sttnmls.favorites.js?' . $component_version);
$doc->addScript(JURI::root() . 'components/com_sttnmls/assets/js/jscript.js?' . $component_version);


$script = <<<JS
    window.addEvent('domready', function(){
        document.formvalidator.setHandler('sttnum', function(value) {
                var v=value.replace(',', '.');
                if(parseFloat(v)==0) return false;
                regex=/^-?\d+((\.|\,)\d+)?$/;
                return regex.test(v);
            }
        )
    });

    jQuery(document).ready(function($){
        $('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});
    });
    
JS;
$doc->addScriptDeclaration($script);

$script = <<<JS
    jQuery(document).ready(function($) {
        $('#nofirm').attr('checked', 'checked');
        $('#jformcatf').on('change', function(){
            var cat = $(this).val();
            if(cat !== "0") {
                $.ajax({
                    type: "POST",
                    url: "index.php?option=com_sttnmls&task=ajax_firmlist&format=raw",
                    data: {cat: cat},
                    success: function( data) {
                        $('#firmlist').html(data);
                        $('#nofirm').removeAttr('checked');
                        $('#jformcompid').change(nofirm);
                    }
                });
            } else {
                $('#nofirm').attr('checked', 'checked');
            }
        });
        $('#jformcompid').change(nofirm);
        
        function nofirm() 
        {
            if($('#jformcompid').val() === 0) {
                $('#nofirm').attr('checked', 'checked');
            } else {
                $('#nofirm').removeAttr('checked');
            }
        }
    });
JS;
$doc->addScriptDeclaration($script);

?>
<div class="com_sttnmls">
    <h1>
        <?php echo JText::_('COM_STTNMLS_LABEL_SIGNUP_STEP2') ?>.
        <?php if($this->realtor == 1) : ?>
            <?php printf('<b>%s - %s</b>', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_REALTOR'))) ?>
        <?php endif; ?>
        
        <?php if($this->realtor == 0) : ?>
            <?php printf('<b>%s - %s</b>', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SOBSTVENNIK'))) ?>
        <?php endif; ?>
        
        <?php if($this->realtor == 2) : ?>
            <?php printf('<b>%s - %s</b>', JText::_('COM_STTNMLS_LABEL_I_AM'), mb_strtolower(JText::_('COM_STTNMLS_LABEL_SPECIALIST'))) ?>
        <?php endif; ?>
    </h1>
    <p class="text-justify">
        <?php if($this->realtor == 0) : ?>
            <?php echo SttNmlsLocal::parseString(JText::_('COM_STTNMLS_AG2DESC'))  ?>
        <?php endif; ?>
    
        <?php if($this->realtor == 1) : ?>
            <?php echo SttNmlsLocal::parseString(JText::_('COM_STTNMLS_AG1DESC'))  ?>
        <?php endif; ?>
    
        <?php if($this->realtor == 2) : ?>
            <?php echo SttNmlsLocal::parseString(JText::_('COM_STTNMLS_AG3DESC'))  ?>
        <?php endif; ?>
    </p>
    
    <?php
        switch($this->realtor) {
            case 1: $class = 'image-realtor'; break;
            case 2: $class = 'image-specialist'; break;
            default: $class = 'image-sobstvennik'; break;
        }
    ?>
    <form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_sttnmls&view=signup', TRUE, -1) ?>" method="post" class="form-validate form-horizontal">
        <?php echo JHtml::_('form.token');?>
        <input type="hidden" name="task" value="signup.activate" />
        <?php if($this->realtor == 0) : ?>
            <input type="hidden" name="jform[name2]" value=" " />
        <?php endif; ?>
        <input type="hidden" name="jform[realtor]" value="<?php echo $this->realtor ?>" />
        <input type="hidden" name="token" value="<?php echo $this->token ?>" />
        
        <div class="row">
            <div class="col-sm-4 col-md-2 text-center">
                <div class="<?php echo $class ?>"></div>
            </div>
            
            <div class="col-sm-8 col-md-8">
            <?php if($this->realtor > 0) : ?>
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jform_fam">
                        <?php echo JText::_('COM_STTNMLS_AGFAM'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" name="jform[fam]" class="required form-control" id="jform_fam" value="" />
                    </div>
                </div>
            <?php endif; ?>
                
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jform_name">
                        <?php echo JText::_('COM_STTNMLS_AGNAME'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" name="jform[name]" class="required form-control" id="jform_name" value="" />
                    </div>
                </div>
                
            <?php if($this->realtor > 0) : ?>
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jform_name2">
                        <?php echo JText::_('COM_STTNMLS_AGNAME2'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" name="jform[name2]" class="required form-control" id="jform_name2" value="" />
                    </div>
                </div>
            <?php endif; ?>
                
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jform_phone">
                        <?php echo JText::_('COM_STTNMLS_AGPHONE'); ?>
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <input type="text" name="jform[phone]" class="required form-control phone" id="jform_phone" value="" />
                    </div>
                </div>
                
            <?php if($this->realtor == 2) : ?>
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jformcatf">
                        <?php echo JText::_('COM_STTNMLS_CATF'); ?>
                    </label>
                    <div class="col-sm-6 col-md-6">
                        <?php echo $this->catf; ?>
                    </div>
                </div>
            <?php endif; ?>
              
            <?php if($this->realtor > 0) : ?>
                <div class="form-group">
                    <label class="control-label col-sm-6 col-md-3" for="jformcompid">                        
                        <?php echo JText::_('COM_STTNMLS_AGENC'); ?>
                        <?php if($this->realtor == 1) : ?>
                            <span class="required">*</span>
                        <?php endif; ?>
                    </label>
                    <div class="col-sm-6 col-md-6" id="firmlist">
                        <?php echo $this->firms; ?>
                    </div>
                </div>
            <?php endif; ?>    
            
            <?php if($this->realtor == 2) : ?>    
                <div class="form-group">
                    <div class="col-sm-offset-6 col-sm-6 col-md-offset-3 col-md-6">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="nofirm" name="jform[nofirm]" />
                                <?php echo JText::_('COM_STTNMLS_NOFIRM'); ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
                
                <div class="form-group">
                    <div class="col-sm-offset-6 col-sm-6 col-md-offset-3 col-md-6">
                        <button type="submit" class="btn btn-success validate">
                            <?php echo JText::_('COM_STTNMLS_BUTTON_DO_SIGNUP') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    
    </form>    
    
    
</div>