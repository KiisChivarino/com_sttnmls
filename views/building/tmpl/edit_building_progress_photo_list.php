<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

$building_progress_images_list = (($this->item->progress_photos != '') ? unserialize($this->item->progress_photos) : array());

$current_date = date('d.m.Y');

?>
<?php if($building_progress_images_list) : ?>
    <?php foreach($building_progress_images_list as $date => $photos) : ?>
        <h4><?php echo $date ?></h4>
        <hr />

        <div class="well">
            <?php foreach($photos as $i => $photo) : ?>
                <div class="thumbnail">
                    <div class="row" style="margin-left: 0; margin-right: 0;">
                        <div class="col-md-4 col-sm-6 col-xs-12 text-center" style="padding-left: 0;">
                            <a class="fbox big" rel="group_2" href="<?php echo SttNmlsHelper::getLinkPhoto($photo->src, 'big', 'images/objects/') . '&ts=' . time() ?>">
                                <img class="thumb img-thumbnail" src="<?php echo SttNmlsHelper::getLinkPhoto($photo->src, 'smallcrop', 'images/objects/') . '&ts=' . time() ?>" />
                            </a>
                            <div style="margin-top:5px;">
                                <a href="javascript:void(0);" class="btn btn-success btn-xs rotateBuildingProgressImage" data-direction="left" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="building" data-idx="<?php echo $i ?>" data-image-date="<?php echo $date ?>" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_ROTATE_TO_THE_LEFT', 90) ?>">
                                    <img src="<?php echo JURI::root() . 'components/com_sttnmls/assets/images/icon_rotate_left.png' ?>" alt="Rotate to the left" style="width:20px;height:20px;" />
                                </a>
                                <b><?php echo JText::_('COM_STTNMLS_LABEL_DO_ROTATE') ?></b>
                                <a href="javascript:void(0);" class="btn btn-success btn-xs rotateBuildingProgressImage" data-direction="right" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="building" data-idx="<?php echo $i ?>" data-image-date="<?php echo $date ?>" data-toggle="tooltip" data-placement="top" title="<?php echo JText::sprintf('COM_STTNMLS_LABEL_ROTATE_TO_THE_RIGHT', 90) ?>">
                                    <img src="<?php echo JURI::root() . 'components/com_sttnmls/assets/images/icon_rotate_right.png' ?>" alt="Rotate to the right" style="width:20px;height:20px;" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <input type="hidden" name="progress_photo[<?php echo $date ?>][image][]" value="<?php echo $photo->src ?>" />
                            <div class="form-group">
                                <label>
                                    <?php echo JText::_('COM_STTNMLS_LABEL_PHOTO_TITLE') ?>
                                </label>
                                <input type="text" class="form-control" name="progress_photo[<?php echo $date ?>][name][]" value="<?php echo $photo->name ?>" />
                            </div>
                            <div class="form-group text-right">
                                <a href="javascript:void(0);" class="btn btn-danger btn-xs removeBuildingProgressImage" data-view="building"  data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-idx="<?php echo $i ?>" data-image-date="<?php echo $date ?>">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    <?php echo JText::_('COM_STTNMLS_BUTTON_DELETE') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <p class="lead text-center">
        <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_NO_ITEMS') ?>
    </p>
<?php endif; ?>