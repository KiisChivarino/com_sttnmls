<?php

defined('_JEXEC') or die('Restricted access');

class SttNmlsModelImage extends JModelLegacy {

	protected $srcImage = false;  //  дескриптор созданного изображения
	protected $coordinate = 4; // 1-верхний, левый, 2-правый, верхний, 3-нижний, правый...
	protected $coord = array();
	protected $namefile = '000.png';
	protected $tmp = '000.png';
	protected $image_type = 3;
	protected $size = 'vsmall';

	function __construct() {
		parent::__construct();
	}

	public function getImg() {
		$src = JRequest::getVar('src', '');
		$this->size = JRequest::getVar('size', 'vsmall');
		if (!$src)
			return $this->nofoto();
		$params = JComponentHelper::getParams('com_sttnmls');
		$imgpath=$params->get('imgpath', 'http://www.nmls.ru/mlsfiles/images/');
		$namefile = $imgpath.$src;
		if (!$namefile)
			return $this->nofoto();
		if (!$this->loadimg($namefile))
			return $this->nofoto();
		$this->newsize();
		return $this->srcImage;
	}

	public function getOutput()
	{
		$src = JRequest::getVar('src', '');
		$size = JRequest::getVar('size', 'vsmall');
		$cache = JFactory::getCache('com_sttnmls', '');
		$cacheId = 'img_brouse_' . serialize(array($src, $size));
		if (!($s = $cache->get($cacheId)))
		{
			$this->getImg();
			ob_start();
			if($this->image_type==2){
				ImageJPEG($this->srcImage);
			} elseif($this->image_type==1){
				ImageGIF($this->srcImage);
			} elseif($this->image_type==3) {
				ImagePNG($this->srcImage);
			}
			$s=ob_get_contents();
			ob_end_clean();
			$this->destroy();
			$cache->store($s, $cacheId);
		}
		return $s;
	}

	// загрузка изображения из файла
	// $img откуда читаем
	public function loadimg($img) {
		if(preg_match('/^https?:\/\/[^\/]+/i', $img)){
			$this->namefile = $img;
		}
		else {
			$this->namefile = JPATH_ROOT . "/components/com_sttnmls/assets/images/" . $img;
		}
		if ($this->imagecreatefrom($this->namefile) != false) {
			$this->srcImage = $this->imagecreatefrom($this->namefile);
		} else {
			return false;
		}
		return true;
	}

	// создаёт новое изображение из файла
	// $filename адрес исх. изображения
	protected function imagecreatefrom($filename) {
		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		if ($this->image_type == 2) {
			return imagecreatefromjpeg($filename);
		} elseif ($this->image_type == 1) {
			return imagecreatefromgif($filename);
		} elseif ($this->image_type == 3) {
			return imagecreatefrompng($filename);
		} else {
			return false;
		}
	}

	public function newsize() {
		if ($this->size == 'big')
			$this->resize(-1, 600);
		if ($this->size == 'small')
			$this->resize(-1, 95);
		if ($this->size == 'vsmall')
			$this->resize(50, -1);
		if ($this->size == 'smallcrop')
			$this->resizecrop(140, 95);
	}

	public function nofoto() {
		$this->image_type = 3;
		$this->srcImage = $this->imagecreatefrom(JPATH_ROOT . "/components/com_sttnmls/assets/images/foto_net.png");
		$this->newsize();
		return $this->srcImage;
	}

	public function getType() {
		return $this->image_type;
	}

	// Изменение размера изображения
	// $width - ширина,  $height высота,
	// если не указать $height , то аргумент $width будет считать процентным соотношением во сколько процентов изменять масштаб
	public function resize($width, $height = false) {

		// Проверяем корректность введения ширины и высоты
		$width = filter_var($width, FILTER_VALIDATE_INT) ? $width : false;
		$height = filter_var($height, FILTER_VALIDATE_INT) ? $height : false;

		// Если ширина не указана или указана некорректно, то будет игнорироватся изменение масштаба изображения
		if (($width != '100' && $height == false) or ($width != false && $height != false)) {
			$w_src = ImageSX($this->srcImage);
			$h_src = ImageSY($this->srcImage);

			// Если высота -1, то пропорционально новой ширине
			if ($height == -1) {
				$height = ($h_src / $w_src) * $width;
			}

			// Если ширина -1, то пропорционально новой высоте
			if ($width == -1 && $height != false) {
				$width = ($w_src / $h_src) * $height;
			}

			// Если не указана высота, то по процентному соотношению вычисляем новый масштаб
			if ($height == false) {
				$height = ($h_src / 100) * $width;
				$width = ($w_src / 100) * $width;
			}

			$dest = imagecreatetruecolor($width, $height);
			imagealphablending ($dest, false);
			if ($this->image_type == 3){ 
				$color = imagecolorallocatealpha ($dest, 255, 255, 255, 127);		
			}else{
				$color = imagecolorallocatealpha ($dest, 255, 255, 255, 0);
			}
			imagefill ($dest, 0, 0, $color);
			if ($height == $width) {
				if ($w_src > $h_src) {
					imagecopyresized($dest, $this->srcImage, 0, 0, round((max($w_src, $h_src) - min($w_src, $h_src)) / 2), 0, $width, $height, min($w_src, $h_src), min($w_src, $h_src));
				} else {
					imagecopyresized($dest, $this->srcImage, 0, 0, 0, round((max($w_src, $h_src) - min($w_src, $h_src)) / 2), $width, $height, min($w_src, $h_src), min($w_src, $h_src));
				}
			} else {
				imagecopyresized($dest, $this->srcImage, 0, 0, 0, 0, $width, $height, $w_src, $h_src);
			}
			imagesavealpha ($dest, true);

			$this->srcImage = $dest;
			unset($dest);
		}
	}

	// Изменение размера изображения
	// $width - ширина,  $height высота, нового изображения
	// если пропорции не совпадают с исходным изображением, то изображение обрезается до нужного размера
	public function resizecrop($new_width, $new_height) {

		// Проверяем корректность введения ширины и высоты
		$new_width = filter_var($new_width, FILTER_VALIDATE_INT) ? $new_width : false;
		$new_height = filter_var($new_height, FILTER_VALIDATE_INT) ? $new_height : false;
		if(!$new_width) return;
		if(!$new_height) return;
		
		$width = ImageSX($this->srcImage);
		$height = ImageSY($this->srcImage);

		$dest = imagecreatetruecolor($new_width, $new_height);
		imagealphablending ($dest, false);
		if ($this->image_type == 3){ 
			$color = imagecolorallocatealpha ($dest, 255, 255, 255, 127);		
		}else{
			$color = imagecolorallocatealpha ($dest, 255, 255, 255, 0);
		}
		imagefill ($dest, 0, 0, $color);
		imagesavealpha ($dest, true);
		
		$src_x = $src_y = 0;
		$src_w = $width;
		$src_h = $height;

		$cmp_x = $width / $new_width;
		$cmp_y = $height / $new_height;

		if ($cmp_x > $cmp_y) {
			$src_w = round ($width / $cmp_x * $cmp_y);
			$src_x = round (($width - ($width / $cmp_x * $cmp_y)) / 2);
		} else if ($cmp_y > $cmp_x) {
			$src_h = round ($height / $cmp_y * $cmp_x);
			$src_y = round (($height - ($height / $cmp_y * $cmp_x)) / 2);
		}
		imagecopyresampled ($dest, $this->srcImage, 0, 0, $src_x, $src_y, $new_width, $new_height, $src_w, $src_h);		
		$this->srcImage = $dest;
		unset($dest);
	}
	
	// освобождает память, ассоциированную с изображением
	public function destroy() {
		ImageDestroy($this->srcImage);
	}

}

