jQuery(document).ready(function($) {
    $('[data-toggle="tooltip"]').tooltip({html: true});

    $('#images-list, #images-building-progress-list').on('hover', '[data-toggle="tooltip"]', function(){
        $(this).tooltip({html: true});
    });
    
    $('#images-list, #images-building-progress-list').sortable({
        items: '.thumbnail'
    });
    
    $('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});
    $('input.price').inputmask("decimal", { radixPoint: "", autoGroup: true, groupSeparator: " ", groupSize: 3 });
    
    $("#raion:input").click(clickraion);
    load_raion($('#cityid').val(), "raion", $("#f_raionid").val());
    
    
    $('#streetFilter').typeahead({
        items: 10,
        minLength: 2,
        source: function(q, process){
            var streets = new Array;
            $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonStreets&format=json",
                type: "POST",
                data: {filter_city: $('#cityid').val(), filter_street: $('#streetFilter').val()},
                dataType: "JSON",
                async: false,
                success: function(results){                    
                    results.each(function(data){                        
                        var group;
                        group = {
                            id: data.id,
                            value: data.value,
                            toString: function(){
                                return JSON.stringify(this);
                            },
                            toLowerCase: function() {
                                return this.value.toLowerCase();
                            },
                            indexOf: function() {
                                return String.prototype.indexOf.apply(this.value, arguments);
                            },
                            substr: function() {
                                return String.prototype.substr.apply(this.value, arguments);
                            },
                            replace: function (string) {
                                var value = '';
                                value +=  this.value;
                                if(typeof(this.id) != 'undefined') {
                                    value += ' <span style="font-size: 0.8em;" class="muted">[ID:';
                                    value += this.id;
                                    value += ']</span>';
                                }
                                return String.prototype.replace.apply('<div>' + value + '</div>', arguments);
                            }
                        };
                        streets.push(group);
                    });
                    process(streets);
                }
            });
        },
        updater: function(item) { 
            var item = JSON.parse(item);
            $('#streetid').val(item.id);
            return item.value;
        }
    });
    $('.clearStreetFilter').click(function(){
        $(this).closest('.input-group').find('input[name="street"]').val(''); 
        $('#streetid').val('0');
    });


    $('#street_selector')
        .selectpicker({
            liveSearch: true
        })
        .ajaxSelectPicker({
            ajax: {
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonStreets&format=json",
                type: 'POST',
                dataType: 'json',
                data: function() {
                    var params = {
                        filter_street: '{{{q}}}'
                    }
                    params.filter_city = $('#cityid').val();
                    return params;
                }
            },
            locale: {
                emptyTitle: 'Выберите улицу',
                searchPlaceholder: 'Улица',
                statusInitialized: 'Введите название улицы',
                statusNoResults: 'Не найдено'
            },
            cache: false,
            //log: 4,
            preprocessData: function(data) {
                var streets = [];

                for(var i = 0; i < data.length; i++) {
                    var curr = data[i];
                    var streetid = $('#streetid').val();
                    streets.push({
                        'value': curr.id,
                        'text': curr.value,
                        'selected': (streetid === curr.id)
                    });
                }
                return streets;
            },
            preserveSelected: false
        });

    $('#street_selector').change(function(){
        var value = $('#street_selector').val() || 0;
        $('#streetid').val(value);
        $('#street_selector').selectpicker('val', value);
        if (value > 0) {
            $('#street_selector').closest('div').removeClass('invalid');
        }
    });


    // Выгрузка фото объекта
    $('#objectPhoto').fileupload({
        sequentialUploads: true,
        dataType: 'json',
        add: function(e, data) {
            $('.sttnmls_photo .upload_image .fileinput-button').addClass('disabled');
            $('.sttnmls_photo .upload_image').append('<div class="progress" style="margin-top:10px;"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: 0%;"></div></div>');
            data.formData = {view: $(this).data('view') || 'apart', cardnum: $(this).data('cn'), compid: $(this).data('compid')};
            data.submit();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.sttnmls_photo .upload_image .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        done: function(e, data) {
            $.each(data.result.errors, function(index, e){
                $('#system-message-container').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + e + '</div>');
            });
            $.each(data.result.files, function(index, file) {
                // Reload image container after upload has finished
                $('#images-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                $.ajax({
                    url: ROOT_URI + "index.php?option=com_sttnmls&view=" + $('#objectPhoto').data('view') + "&task=ajaxGetEditObjectImages&format=raw",
                    type: "GET",
                    data: {cn: $('#objectPhoto').data('cn'), cid: $('#objectPhoto').data('compid')},
                    dataType: "HTML"
                }).done(function(html){
                    $('#images-list').html(html);
                });

                // Remove progress bar
                $('.sttnmls_photo .upload_image div.progress').remove();
                $('.sttnmls_photo .upload_image .fileinput-button').removeClass('disabled');
            });
        }
    });
    
    $('#images-list').on('click', '.removeObjectImage', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var idx = $(this).data('idx');
        var view = $(this).data('view');
        
        if(!confirm('Уверены?')) {
            return false;
        }
       
        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRemoveObjectImage&format=raw",
            type: "POST",
            data: {cn: cn, cid: cid, idx: idx},
            dataType: "JSON",
            success: function(response) {
                if(response.result) {
                    $('#images-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=" + $('#objectPhoto').data('view') + "&task=ajaxGetEditObjectImages&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function(html){
                        $('#images-list').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });
    
    // Поворот картинки
    $('#images-list').on('click', '.rotateObjectImage', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var idx = $(this).data('idx');
        var direction = $(this).data('direction');
        var view = $(this).data('view');
        
        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRotateObjectImage&foramt=raw",
            type: "POST",
            data: {cn: cn, cid: cid, idx: idx, direction: direction},
            dataType: "JSON"
        }).done(function(response){
            if(response.result) {
                $('#images-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                $.ajax({
                    url: ROOT_URI + "index.php?option=com_sttnmls&view=" + $('#objectPhoto').data('view') + "&task=ajaxGetEditObjectImages&format=raw",
                    type: "GET",
                    data: {cn: cn, cid: cid},
                    dataType: "HTML"
                }).done(function(html){
                    $('#images-list').html(html);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                });
            }
        });
    });

    // Выгрузка аватара
    $('#firmLogo').fileupload({
        dataType: 'json',
        add: function(e, data) {
            $('.sttnmls_photo .upload_image .fileinput-button').addClass('disabled');
            $('.sttnmls_photo .upload_image').append('<div class="progress" style="margin-top:10px;"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: 0%;"></div></div>');
            data.formData = {agency: $(this).data('agency') || 0};
            data.submit();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.sttnmls_photo .upload_image .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        done: function(e, data) {
            setTimeout(function(){
                $.each(data.result.errors, function(index, e){
                    $('#system-message-container').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + e + '</div>');
                });
                $.each(data.result.files, function(index, file) {
                    $('.sttnmls_photo .image').hide().html('<img class="img-thumbnail" src="' + file.url_path +  '?t' + Math.random() + '" alt="" />').fadeIn();
                    $('.sttnmls_photo .upload_image div.progress').remove();
                    $('.sttnmls_photo .upload_image .fileinput-button').removeClass('disabled');
                });
            }, 500);
        }
    });

    $('.addBuildingPlan').on('click', function(){
        var view = $(this).data('view');
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=ajaxAddBuildingPlan&format=raw",
            type: "POST",
            data: {'bplan_id': 0, cn: cn, cid: cid},
            success: function(data) {
                $('#building_form_container').empty().html(data);
                $('#building_form_container').modal();
            }
        });
    });

    $('#bulding_plans_list').on('click', '.editBuildingPlan', function(){
        var view = $(this).data('view');
        var bplan_id = $(this).data('bplan_id');
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=edit_form_building_plan&format=raw",
            type: "POST",
            data: {'bplan_id': bplan_id, cn: cn, cid: cid},
            success: function(data) {
                $('#building_form_container').empty().html(data);
                $('#building_form_container').modal();
            }
        });
    });

    $('#bulding_plans_list').on('click', '.removeBuildingPlan', function(){
        var view = $(this).data('view');
        var bplan_id = $(this).data('bplan_id');
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');

        if(!confirm('Вы уверены?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRemoveBuildingPlan",
            type: "POST",
            data: {'bplan_id': bplan_id, cn: cn, cid: cid},
            success: function(r) {
                response = $.parseJSON(r);
                if (response.result) {
                    $('#bulding_plans_list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=building&layout=edit_building_plans_list&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function (html) {
                        $('#bulding_plans_list').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function (message) {
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });

    $('#building_form_container').on('click', '.saveData', function(){
        var form = $('#building_form_container').find('form');

        // Form validation
        if (!document.formvalidator.isValid(form)) {
            return false;
        }

        form.submit();
    });

    $('#building_form_container').on('submit', 'form', function(e){
        var action_url = $(this).attr('action') || 'undefined';
        var cn = $(this).find('input[name="cn"]').val();
        var cid = $(this).find('input[name="cid"]').val();


        $.ajax({
            url: action_url,
            type: "POST",
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function(r) {
                response = $.parseJSON(r);
                if (response.result) {
                    $('#building_form_container').modal('hide').empty();

                    $('#bulding_plans_list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=building&layout=edit_building_plans_list&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function (html) {
                        $('#bulding_plans_list').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function (message) {
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
        e.preventDefault();
    });

    $('.fbox').fancybox({
        margin: 0,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'over',
        'titleFormat': function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }
    });


    $('#objectBuildingProgressPhoto').fileupload({
        sequentialUploads: true,
        dataType: 'json',
        add: function(e, data) {
            $('.sttnmls_building_progress_photo .upload_image .fileinput-button').addClass('disabled');
            $('.sttnmls_building_progress_photo .upload_image').append('<div class="progress" style="margin-top:10px;"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: 0%;"></div></div>');
            data.formData = {view: $(this).data('view') || 'apart', cardnum: $(this).data('cn'), compid: $(this).data('compid')};
            data.submit();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.sttnmls_photo .upload_image .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        done: function(e, data) {
            $.each(data.result.errors, function(index, e){
                $('#system-message-container').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + e + '</div>');
            });
            $.each(data.result.files, function(index, file) {
                // Reload image container after upload has finished
                $('#images-building-progress-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                $.ajax({
                    url: ROOT_URI + "index.php?option=com_sttnmls&view=" + $('#objectBuildingProgressPhoto').data('view') + "&layout=edit_building_progress_photo_list&format=raw",
                    type: "GET",
                    data: {cn: $('#objectBuildingProgressPhoto').data('cn'), cid: $('#objectBuildingProgressPhoto').data('compid')},
                    dataType: "HTML"
                }).done(function(html){
                    $('#images-building-progress-list').html(html);
                });

                // Remove progress bar
                $('.sttnmls_building_progress_photo .upload_image div.progress').remove();
                $('.sttnmls_building_progress_photo .upload_image .fileinput-button').removeClass('disabled');
            });
        }
    });

    $('#images-building-progress-list').on('click', '.removeBuildingProgressImage', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var idx = $(this).data('idx');
        var image_date = $(this).data('image-date');
        var view = $(this).data('view');

        if(!confirm('Уверены?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRemoveBuildingProgressImage",
            type: "POST",
            data: {cn: cn, cid: cid, idx: idx, image_date: image_date},
            dataType: "JSON",
            success: function(response) {
                if(response.result) {
                    // Reload image container after upload has finished
                    $('#images-building-progress-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=edit_building_progress_photo_list&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function(html){
                        $('#images-building-progress-list').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });

    // Поворот картинки
    $('#images-building-progress-list').on('click', '.rotateBuildingProgressImage', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var idx = $(this).data('idx');
        var image_date = $(this).data('image-date');
        var direction = $(this).data('direction');
        var view = $(this).data('view');

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRotateBuildingProgressImage",
            type: "POST",
            data: {cn: cn, cid: cid, idx: idx, image_date: image_date, direction: direction},
            dataType: "JSON",
            success: function(response) {
                if (response.result) {
                    // Reload image container after upload has finished
                    $('#images-building-progress-list').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=edit_building_progress_photo_list&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function (html) {
                        $('#images-building-progress-list').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function (message) {
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });

    $('#attached_project_file').on('click', '.removeProjectFile', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRemoveBuildingProjectFile",
            type: "POST",
            data: {cn: cn, cid: cid},
            dataType: "JSON",
            success: function(response) {
                if (response.result) {
                    // Reload image container after upload has finished
                    $('#attached_project_file').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=edit_project_file_input&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function (html) {
                        $('#attached_project_file').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function (message) {
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });

    $('#attached_dogovor_file').on('click', '.removeDogovorFile', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&task=jsonRemoveBuildingDogovorFile",
            type: "POST",
            data: {cn: cn, cid: cid},
            dataType: "JSON",
            success: function(response) {
                if (response.result) {
                    // Reload image container after upload has finished
                    $('#attached_dogovor_file').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                    $.ajax({
                        url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=edit_dogovor_file_input&format=raw",
                        type: "GET",
                        data: {cn: cn, cid: cid},
                        dataType: "HTML"
                    }).done(function (html) {
                        $('#attached_dogovor_file').html(html);
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function (message) {
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
            }
        });
    });

    $('#builder').on('change', function(){
        var builder = $(this).val() || 0;

        $('#building_list_container').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=apart&layout=edit_building_list_select&format=raw",
            type: "POST",
            data: {builder: builder},
            dataType: "HTML",
            success: function(response){
                $('#building_list_container').html(response);
            }
        });
    });
});

function clickraion() {
    var r_checked = jQuery('#raionid').val();
    var mr_checked = jQuery("#f_mraionid").val();
    var cityid = jQuery('#cityid').val();
    load_mraion(r_checked, "mraion", mr_checked, cityid);
}

function load_raion(cityid, name, r_checked) {
    jQuery("#" + name).html("<img class=\'ajax_activity\' src=\'/components/com_sttnmls/assets/images/ajax_activity.gif\'>");
    jQuery.ajax({
        type: "POST",
        url: ROOT_URI + "index.php?option=com_sttnmls&task=ajax_r&format=raw",
        data: {r_checked: r_checked, city: cityid, select: 1}
    }).done(function( data ) {
        jQuery("#"+name).html(data);
        clickraion();
    });
}

function load_mraion(rid, name, mr_checked, cityid){
    jQuery("#" + name).html("<img class=\'ajax_activity\' src=\'/components/com_sttnmls/assets/images/ajax_activity.gif\'>");
    jQuery.ajax({
        type: "POST",
        url: ROOT_URI + "index.php?option=com_sttnmls&task=ajax_mr&format=raw",
        data: {mr_checked: mr_checked, rid: rid, city: cityid, select:1}
    }).done(function( data ) {
        jQuery("#" + name).html(data);
    });
}

function deleteFoto(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=deletefile&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain").html(res[0]);
			jQuery("#imgfiles").html(res[1]);
	});	
}

function deleteFoto2(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=deletefile2&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain2").html(res[0]);
			jQuery("#imgfiles2").html(res[1]);
	});	
}






function mainFoto(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=mainfile&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain").html(res[0]);
			jQuery("#imgfiles").html(res[1]);
	});	
}




function chasFoto(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=chasfile&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain").html(res[0]);
			jQuery("#imgfiles").html(res[1]);
	});	
}

function chasnoFoto(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=chasnofile&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain").html(res[0]);
			jQuery("#imgfiles").html(res[1]);
	});	
}




function chasFoto2(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=chasfile2&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain2").html(res[0]);
			jQuery("#imgfiles2").html(res[1]);
	});	
}

function chasnoFoto2(flnum) {
	jQuery.ajax({
		type: "POST",
		url: 'index.php?option=com_sttnmls&task=chasnofile2&format=raw&view='+viewname+'&cardnum='+cardnum+'&compid='+compid,
		data: {flnum: flnum}
	}).done(function( response ) {
			var res=response.split('|');
			jQuery("#imgfilemain2").html(res[0]);
			jQuery("#imgfiles2").html(res[1]);
	});	
}




function changecity()
{
	var cityid = jQuery('#cityid').val();
	var r_checked = jQuery("#f_raionid").val();
	jQuery('input#T4').val('');
	jQuery("#f_street").val('');
	jQuery('#streetid').val('0');
	load_raion(cityid, "raion", r_checked);
}
	




function str_replace ( search, replace, subject ) {
					if(!(replace instanceof Array)){
						replace=new Array(replace);
						if(search instanceof Array){
							while(search.length>replace.length){
								replace[replace.length]=replace[0];
							}
						}
					}
					if(!(search instanceof Array))search=new Array(search);
					while(search.length>replace.length){
						replace[replace.length]='';
					}
					if(subject instanceof Array){
						for(k in subject){
							subject[k]=str_replace(search,replace,subject[k]);
						}
						return subject;
					}
					for(var k=0; k<search.length; k++){
						var i = subject.indexOf(search[k]);
						while(i>-1){
							subject = subject.replace(search[k], replace[k]);
							i = subject.indexOf(search[k],i);
						}
					}
					return subject;
				}




//window.addEvent('domready', function(){
//		document.formvalidator.setHandler('sttnum', function(value) {
//
//
//			value=str_replace(" ","",value);
//
//			var v=value.replace(',', '.');
//			if(parseFloat(v)==0) return false;
//			regex=/^-?\d+((\.|\,)\d+)?$/;
//			return regex.test(v);
//		}
//		)
//	}
//);
