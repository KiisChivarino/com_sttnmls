<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

$building_progress_images_list = (($this->item->progress_photos != '') ? unserialize($this->item->progress_photos) : array());

?>

<?php if($building_progress_images_list) : ?>

    <?php foreach($building_progress_images_list as $date => $photos) : ?>
        <div class="h5"><?php printf('Фото загружены: <b>%s</b>', $date) ?></div>
        <hr />
        <div class="row">
            <?php foreach($photos as $i => $photo) : ?>
                <div class="col-md-3 col-sm-4 col-xs-6">
                    <a class="fbox big" rel="group_2" href="<?php echo SttNmlsHelper::getLinkPhoto($photo->src, 'big', 'images/objects/') . '&ts=' . time() ?>">
                        <img class="thumb img-thumbnail" src="<?php echo SttNmlsHelper::getLinkPhoto($photo->src, 'smallcrop', 'images/objects/') . '&ts=' . time() ?>" alt="<?php echo $photo->name ?>" />
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <p class="lead text-center">
        <?php echo JText::_('COM_STTNMLS_MESSAGE_INFO_NO_ITEMS') ?>
    </p>
<?php endif; ?>