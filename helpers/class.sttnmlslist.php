<?php defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');


class SttnmlsModelList extends JModelList
{
    // Индекс объекта в избранном (1-5)
    protected $favoriteObjectType = 0;
    // Название таблицы объектов в зависимости от отображаемой модели
    protected $objectsTableName = '';
    // Подготовка запроса
    protected $query = NULL;
    // Флаг обновления фильтров в сессии
    protected $_updateFilterState = FALSE;

    // Текущее отображение
    protected $currentView = '';
    // Текущий формат запроса
    protected $format = 'html';
    // Тип операции над объектом: 0 - продажа, 1 - аренда
    protected $stype = 0;
    // Тип объекта
    protected $what = 0;
    // Текущий пользователь - агентская информация
    protected $currentAgent = NULL;

    // Права текущего пользователя
    protected $access = NULL;



    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->access = new stdClass();
        $this->access->isLogin = false;
        $this->access->isAdmin = false;
        $this->access->isModerator = false;
        $this->access->seeContactsObjects = false;
        $this->access->seeExpiredObjects = false;

        $this->raw = ((JFactory::getApplication()->input->getString('format', 'html') == 'html') ? FALSE : TRUE);
    }

    protected function chkExtendedAccess()
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();
        $user = JFactory::getUser();

        if(!$user->id) {
            //> Пользователь не вошел
            return false;
        }

        //> Сброс прав
        $this->access->isLogin = true;
        $this->access->isAdmin = false;
        $this->access->isModerator = false;
        $this->access->seeContactsObjects = false;
        $this->access->seeExpiredObjects = false;



        $this->access->isAdmin = $user->authorise('core.admin', 'com_sttnmls');
        if($this->access->isAdmin) {
            //> Администратор может смотреть непродленные объекты
            $this->access->isModerator = TRUE;
            $this->access->seeContactsObjects = TRUE;
            $this->access->seeExpiredObjects = TRUE;
        } else {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName(array('g.seecont', 'g.seexpired')));
            $query->select('IF(' . $db->quoteName('g.seecont') . '=0, 0, 1) AS ' . $db->quoteName('moderator'));
            $query->from($db->quoteName('#__sttnmlsvocagents', 'ag'));
            $query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON ' . $db->quoteName('g.id') . '=' . $db->quoteName('ag.grp'));
            $query->where($db->quoteName('ag.userid') . '=' . $db->quote($user->id));
            $query->where($db->quoteName('g.seecont') . '=' . $db->quote(1));
            $db->setQuery($query);
            $result = $db->loadObject();

            if($result) {
                $this->access->isModerator = $result->moderator;
                $this->access->seeContactsObjects = $result->seecont;
                $this->access->seeExpiredObjects = $result->seexpired;
            }
        }
    }

    public function getAccessRules()
    {
        $this->chkExtendedAccess();
        return $this->access;
    }

    protected function getDefaultFiltersValue()
    {
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');

        // Массив названий фильтров и их значений по-умолчанию -----------------
        $filters = array(
            // ОПИСАНИЕ ФИЛЬТРОВ:
            //    [имя_поля_фильтра]    =>  [массив значений]
            //
            //    (!) - обязательное значение фильтра
            //    [!] >>> <<< [!] - блок значений, которые должны быть объявлены вместе
            //
            // ЗНАЧЕНИЯ ФИЛЬТРОВ:
            //      "type"      -   (!) тип значения фильтра
            //      "value"     -   (!) значение фильтра по умолчанию
            //
            //      "getVar"    -   значение фильтра не хранится в сесии(передается в запросе)
            //
            //      [!] >>>
            //      "targetFilterKey"   -   поле фильтра, которому соответствует результат полученых из таблицы данных
            //                              (при этом исходный фильтр будет удален, если его имя не совпадает с текущим полем фильтра)
            //
            //      "valueFromTable"    -   название таблицы, из которой будут браться данные для targetFilterKey
            //
            //      "valueField"        -   колонка таблицы, из которой будут браться данные для targetFilterKey.
            //                              Фактически это SELECT секция запроса. Должна содержать ТОЛЬКО одно поле в выводе.
            //
            //      "where"             -   условие поиска в таблице. Фактически это WHERE секция запроса.
            //                              Поэтому должна содержать либо единичное значение условие, например: ID="%s",
            //                              либо завершенную строку WHERE запроса
            //      <<< [!]
            //
            'city'              =>      array('type' => 'uint', 'value' => 0, 'targetFilterKey' => 'city', 'valueFromTable' => '#__sttnmlsvoccity', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),
            'city_id'           =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'city', 'valueFromTable' => '#__sttnmlsvoccity', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),

            'raions'            =>      array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'raions', 'valueFromTable' => '#__sttnmlsvocraion', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s)'),
            'raion_id'          =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'raions', 'valueFromTable' => '#__sttnmlsvocraion', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),

            'mraions'           =>      array('type' => 'array', 'value' => array('null'), 'targetFilterKey' => 'mraions', 'valueFromTable' => '#__sttnmlsvocmraion', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . ' IN (%s)'),
            'mraion_id'         =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'mraions', 'valueFromTable' => '#__sttnmlsvocmraion', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),

            'street'            =>      array('type' => 'string', 'value' => ''),
            'street_id'         =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'street', 'valueFromTable' => '#__sttnmlsvocstreet', 'valueField' => $db->quoteName('NAME'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),
            'agency'            =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'agency', 'valueFromTable' => '#__sttnmlsvocfirms', 'valueField' => $db->quoteName('realname'), 'where' => $db->quoteName('ID') . '=' . $db->quote('%s')),
            'agent'             =>      array('type' => 'uint', 'value' => 0, 'getVar' => TRUE, 'targetFilterKey' => 'agent', 'valueFromTable' => '#__sttnmlsvocagents', 'valueField' => 'CONCAT(' . $db->quoteName('SIRNAME') . '," ",' . $db->quoteName('NAME') . '," ",' . $db->quoteName('SECNAME') . ')', 'where' => $db->quoteName('id') . '=' . $db->quote($app->input->getUint('agent', 0)) . ' AND ' . $db->quoteName('COMPID') . '=' . $db->quote($app->input->getUint('agency', 0))),

            'phone'             =>      array('type' => 'string', 'value' => ''),
            'price_ot'          =>      array('type' => 'uint', 'value' => 0),
            'price_do'          =>      array('type' => 'uint', 'value' => 0),
            'area_ot'           =>      array('type' => 'uint', 'value' => 0),
            'area_do'           =>      array('type' => 'uint', 'value' => 0),

            'exclusive'         =>      array('type' => 'bool', 'value' => false),
            'owner'             =>      array('type' => 'bool', 'value' => false),
            'expired'           =>      array('type' => 'bool', 'value' => false),
            'internal_base'     =>      array('type' => 'bool', 'value' => false),
			'realtors_base'		=>      array('type' => 'bool', 'value' => false),
            'realtors_and_open'	=>      array('type' => 'bool', 'value' => false),
            'archive_mode'      =>      array('type' => 'bool', 'value' => false),

            'f_expired'         =>      array('type' => 'bool', 'value' => false, 'getVar' => true, 'targetFilterKey' => 'expired'),
            'f_closed'          =>      array('type' => 'bool', 'value' => false, 'getVar' => true, 'targetFilterKey' => 'internal_base'),
			'f_realtors'		=>		array('type' => 'bool', 'value' => false, 'getVar' => true, 'targetFilterKey' => 'realtors_base'),
            'f_realtors_and_open'   =>		array('type' => 'bool', 'value' => false, 'getVar' => true, 'targetFilterKey' => 'realtors_and_open')
        );

        return $filters;
    }

    public function getFiltersGETVarsList()
    {
        $result = array();
        $app = JFactory::getApplication();

        $defaultFiltersValue = $this->getDefaultFiltersValue();
        if($defaultFiltersValue) {
            foreach($defaultFiltersValue as $filter => $value) {
                $getVar = $app->input->get($filter, $value['value'], $value['type']);
                if(isset($value['getVar']) && $value['value'] != $getVar) {
                    $result[] = $filter . '=' . $getVar;
                }
            }
        }

        return $result;
    }

    public function getFiltersList()
    {
        $filtersList = array();
        $result = array();
        $defaultFiltersList = $this->getDefaultFiltersValue();

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        //> Получаем данные об использованных в поисках фильтрах -------------------------------------------------------
        foreach($defaultFiltersList as $filter => $defaultFilterValue) {

            if(isset($defaultFilterValue['getVar'])) {
                //> Получаем данные из запроса
                $currentFilterValue = $app->input->get($filter, $defaultFilterValue['value'], $defaultFilterValue['type']);
            } else {
                //> Получаем значение фильтра из состояния модели
                $currentFilterValue = $this->getState('filter.' . $filter);
            }

            if($currentFilterValue !== $defaultFilterValue['value']) {
                //> Значение в состоянии присутствует и не равно значению по-умолчанию
                if(is_array($currentFilterValue)) {
                    //> Дополнительная обработка фильтра, если он массив данных (убираем "null")
                    foreach($currentFilterValue as $k => $v) {
                        if($v == 'null') {
                            unset($currentFilterValue[$k]);
                        }
                    }

                    //> Если фильтр после фильтрования еще имеет значение, то добавляем его в список на формирование строки
                    //  фильтров
                    if($currentFilterValue) {
                        $filtersList[$filter] = $currentFilterValue;
                    }
                } else {
                    $filtersList[$filter] = $currentFilterValue;
                }
            }
        }
        //> END Получаем данные об использованных в поисках фильтрах ---------------------------------------------------

        //> Подготовка фильтров. Часть №1 ------------------------------------------------------------------------------
        if($filtersList) {
            foreach($filtersList as $filter => $value) {
                $fTargetFilterKey = ((isset($defaultFiltersList[$filter]['targetFilterKey'])) ? $defaultFiltersList[$filter]['targetFilterKey'] : $filter);

                if(isset($defaultFiltersList[$filter]['valueFromTable'])) {
                    $fTable = $defaultFiltersList[$filter]['valueFromTable'];
                    $fValueField = $defaultFiltersList[$filter]['valueField'];

                    if (is_array($value)) {
                        foreach ($value as $k => $v) {
                            $value[$k] = $db->quote($v);
                        }
                        $value = implode(', ', $value);
                    }

                    $fCondition = sprintf($defaultFiltersList[$filter]['where'], $value);

                    $filtersList[$fTargetFilterKey] = $this->getValueFromTable($fTable, $fValueField, $fCondition);
                } elseif(isset($defaultFiltersList[$filter]['valueFromArray']) && isset($defaultFiltersList[$filter]['valueFromArray'][$value])) {
                    $filtersList[$fTargetFilterKey] = $defaultFiltersList[$filter]['valueFromArray'][$value];
                } else {
                    $filtersList[$fTargetFilterKey] = $value;
                }
                if(!$filtersList[$fTargetFilterKey] OR $filter != $fTargetFilterKey) {
                    unset($filtersList[$filter]);
                }
            }
        }
        //> END Подготовка фильтров. Часть №1 --------------------------------------------------------------------------

        //> Подготовка фильтров. Часть №2 ------------------------------------------------------------------------------
        if($filtersList) {
            $getVars = array();
            foreach($defaultFiltersList as $filter => $value) {
                if(isset($value['getVar'])) {
                    $valueFromRequest = $app->input->get($filter, $value['value'], $value['type']);
                    if($valueFromRequest !== $value['value']) {
                        $getVars[] = $filter . '=' . $valueFromRequest;
                    }
                }
            }

            $return_link = base64_encode(SttNmlsHelper::getSEFUrl('com_sttnmls', $app->input->getString('view', 'aparts'), (($getVars) ? '&' . implode('&', $getVars) : '')));

            foreach($filtersList as $filter => $value) {
                $f = new stdClass();
                $f->name = JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_' . strtoupper($filter));
                $f->value = $value;
                $f->returnURL = $return_link;
                $f->link = ((!isset($defaultFiltersList[$filter]['getVar'])) ? SttNmlsHelper::getSEFUrl('com_sttnmls', $app->input->getString('view', 'aparts'), '&task=rfilter&key=' . $filter . '&returnURL=' . $f->returnURL) : '') ;
                $f->isFlag = (($defaultFiltersList[$filter]['type'] == 'bool') ? TRUE : FALSE);
                $result[] = $f;
            }

            //> Формирования пункта - сброс всех фильтров
            $f = new stdClass();
            $f->name = JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_RESET_ALL');
            $f->value = NULL;
            $f->isFlag = TRUE;
            $f->returnURL = $return_link;
            $f->link = SttNmlsHelper::getSEFUrl('com_sttnmls', $app->input->getString('view', 'aparts'), '&task=rfilter&key=resetall&returnURL=' . $f->returnURL);
            $result[] = $f;
        }
        //> END Подготовка фильтров. Часть №2 --------------------------------------------------------------------------

//        print_r($this->getState());
//        print_r($filtersList);
//        print_r($result);
//        exit;
        return $result;
    }

    protected function getValueFromTable($tableName, $select, $where = array())
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        if($select) {


            $query = $db->getQuery(TRUE);

            $query->select($select);
            $query->from($db->quoteName($tableName));

            if($where) {
                if(is_array($where)) {
                    foreach($where as $tField => $val) {
                        $query->where($db->quoteName($tField) . $val);
                    }
                } else {
                    $query->where($where);
                }
            }
            $db->setQuery($query);

//            print_r((string)$query);

            return $db->loadColumn();
        }
    }

    public function getMRaionsList()
    {
        // JOOMLA Instances
        $db = $this->getDbo();

        // Получение списка микрорайонов
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('ID', 'value') . ', ' . $db->quoteName('NAME', 'text'));
        $query->from($db->quoteName('#__sttnmlsvocmraion'));
        $this->setAdditionalWhereFromStateCheckbox($query, 'raions', 'RAIONID');
        $this->setAdditionalWhereFromState($query, 'city', 'CITYID');
        $query->where($db->quoteName('CITYID') . '<>-1');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function getRaionsList()
    {
        // JOOMLA Instances
        $db = $this->getDbo();

        // Получение списка районов
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('ID', 'value') . ', ' . $db->quoteName('NAME', 'text'));
        $query->from($db->quoteName('#__sttnmlsvocraion'));
        $this->setAdditionalWhereFromState($query, 'city', 'CITYID');
        $query->where($db->quoteName('CITYID') . '<>-1');
        $query->order($db->quoteName('CITYID') . ' ASC, ' . $db->quoteName('ID') . ' ASC');
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function getStreetNameByID($street_id)
    {
        // JOOMLA Instances
        $db = JFactory::getDbo();

        if($street_id) {
            $query = $db->getQuery(TRUE);
            $query->select($db->quoteName('NAME'));
            $query->from($db->quoteName('#__sttnmlsvocstreet'));
            $query->where($db->quoteName('ID') . '=' . $db->quote($street_id));
            $db->setQuery($query);

            return $db->loadResult();
        }
        return '';
    }


    public function getTp()
    {
        return $this->favoriteObjectType;
    }

    public function resetFilter($key)
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();

        $defaultFiltersListValue = $this->getDefaultFiltersValue();

        if($key == 'resetall') {
            foreach($defaultFiltersListValue as $filter => $value) {
                if(!isset($value['getVar'])) {
                    $app->setUserState($this->context . '.filter.' . $filter, $value['value']);
                    $this->setState('filter.' . $filter, $value['value']);
                }
            }
        } elseif(isset($defaultFiltersListValue[$key])) {
            $defaultFilterValue = $defaultFiltersListValue[$key]['value'];
            $app->setUserState($this->context . '.filter.' . $key, $defaultFilterValue);
            $this->setState('filter.' . $key, $defaultFilterValue);
        }


    }

    protected function setAdditionalWhereFromState($qInstance, $var, $tField, $condition = '=')
    {
        $db = JFactory::getDbo();
        $format = JFactory::getApplication()->input->getString('format', 'html');

        if($format == 'raw') {
            //> Исключается из формирования, если запрос AJAX или только самого содержимого
            return FALSE;
        }

        $sVar = $this->getState('filter.' . $var);
        if($sVar) {
            //> Переменная есть в сессии
            if($condition == 'LIKE') {
                //> Запрос поиска по LIKE
                $qInstance->where($db->quoteName($tField) . ' LIKE ' . $db->quote('%' . $sVar . '%'));
            } else {
                //> Обычный строгий запрос
                $qInstance->where($db->quoteName($tField) . $condition . $db->quote($sVar));
            }
        }
    }

    public function setAdditionalWhereFromStateCheckbox($qInstance, $var, $tField)
    {
        $db = JFactory::getDbo();
        $format = JFactory::getApplication()->input->getString('format', 'html');

        if($format == 'raw') {
            //> Исключается из формирования, если запрос AJAX или только самого содержимого
            return FALSE;
        }

        $sVar = $this->getState('filter.' . $var);
        if($sVar && is_array($sVar)) {
            //> Переменная есть в сессии и является массивом
            foreach($sVar as $k => $v) {
                //> Проверка значений массива на предмет целочисленных значений
                if(!(int)$v) {
                    //> Значение не является целочисленным или равно нулю - убираем из массива
                    unset($sVar[$k]);
                }
            }

            if($sVar) {
                //> В массиве после фильтрации остались значения
                $qInstance->where($db->quoteName($tField) . ' IN (' . implode(',', $sVar) . ')');
            }
        }
    }

    protected function setFilterState($var, $default = NULL, $type = 'none')
    {
        $app = JFactory::getApplication();

        if($this->_updateFilterState) {
            $val = $this->getUserStateFromRequest($this->context . '.filter.' . $var, 'filter_' . $var, $default, $type);
        } else {
            $val = $app->getUserState($this->context . '.filter.' . $var, $default);
        }

        if(is_array($default) && !is_array($val)) {
            $rval = array();
            $rval[] = $val;
        } else {
            $rval = $val;
        }

        $this->setState('filter.' . $var, $rval);
    }

    protected function setPersonalConditions() {}

    protected function setUpdateFilterState()
    {
        if(JFactory::getApplication()->input->getString('UPD', '') !== '') {
            $this->_updateFilterState = TRUE;
        }
        if(JFactory::getApplication()->input->getString('RST', '') !== '') {
            $this->resetFilter('resetall');
        }
    }

    protected function getListQuery()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');

        // Подключение других моделей
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');

        // Настройки компонента
        $agency_sobstv = $params->get('compidsob', 99001);
        $compidsob = $params->get('compidsob', '0');
        $days = $params->get('days', 30);
        $_exclusive_only_nmls = $params->get('f_exclusive_only_nmls', 0);
        $_show_owner_expired = FALSE;


        // Request VARS ------------------------------------------------------------------------------------------------
        $act = $app->input->getString('act', '');
        $agency = $app->input->getUint('agency', 0);
        $agent = $app->input->getUint('agent', 0);
        $city_id = $app->input->getUint('city_id', 0);
        $raion_id = $app->input->getUint('raion_id', 0);
        $mraion_id = $app->input->getUint('mraion_id', 0);
        $street_id = $app->input->getUint('street_id', 0);
        $builder = $app->input->getUint('builder', 0);
        $building = $app->input->getUint('building', 0);
        $_src_profile = $app->input->getBool('profile', FALSE);
        $_show_owners_list = $app->input->getBool('owners', FALSE);
        $_f_expired = $app->input->getBool('f_expired', false);
        $_f_closed = $app->input->getBool('f_closed', false);
		$_f_realtors = $app->input->getBool('f_realtors', false);
        $_f_realtors_and_open = $app->input->getBool('f_realtors_and_open', false);
		
        $this->currentView = $app->input->getString('view', 'aparts');
        $this->format = $app->input->getString('format', 'html');
        $this->stype = $app->input->getUint('stype', 0);
        $this->what = $app->input->getUint('what', 0);



        // State VARS --------------------------------------------------------------------------------------------------
        $phone = $this->getState('filter.phone', '');
        $street = $this->getState('filter.street');

        $_internal_base = (($_f_closed) ? true : $this->getState('filter.internal_base', false));
		$_realtors_base = (($_f_realtors) ? true : $this->getState('filter.realtors_base', false));
		$_realtors_and_open_base = (($_f_realtors_and_open) ? true : $this->getState('filter.realtors_and_open', false));
        
        $_expired = (($_f_expired) ? true : $this->getState('filter.expired', false));
        $_exclusive = $this->getState('filter.exclusive', false);
        $_show_owners = $this->getState('filter.owner', false);
        $_archive_mode = $this->getState('filter.archive_mode', false);


        // Получение информации о расширенных правах
        $this->chkExtendedAccess();

        // Установка флага обновления запроса
        $this->setUpdateFilterState();

        // Если запрошена страница из профиля
        if($_src_profile) {
            $this->resetFilter('resetall');
        }


        //==============================================//
        // Подготовка запроса                           //
        //==============================================//
        $this->query = $db->getQuery(TRUE);

        // ОБРАБОТКА: Флаг возомжности редактирования объекта ----------------------------------------------------------
        $this->currentAgent = $mAgent->getAgentInfoByID();
        if(!$this->currentAgent) {
            $this->query->select('0 AS ' . $db->quoteName('editable'));
        } else {
            if($this->access->isAdmin) {
                $this->query->select('1 AS ' . $db->quoteName('editable'));
            } else {
                if($this->currentView == 'buildings') {
                    //> Запрашивается вид НОВОСТРОЙКИ
                    if($this->currentAgent->boss) {
                        $this->query->select('CASE WHEN (' . $db->quoteName('o.comp') . '=' . $db->quote($this->currentAgent->COMPID) . ' OR ' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ') THEN 1 ELSE 0 END AS ' . $db->quoteName('editable'));
                    } else {
                        $this->query->select('CASE WHEN (' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ') THEN 1 ELSE 0 END AS ' . $db->quoteName('editable'));
                    }
                } else {
                    //> Запрашивается вид отличный от НОВОСТРОЕК
                    if($this->currentAgent->boss) {
                        $this->query->select('CASE WHEN (' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ') THEN 1 ELSE 0 END AS ' . $db->quoteName('editable'));
                    } else {
                        $this->query->select('CASE WHEN (' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ') THEN 1 ELSE 0 END AS ' . $db->quoteName('editable'));
                    }
                }
            }
        }
        // END ОБРАБОТКА: Флаг возомжности редактирования объекта ------------------------------------------------------

        $this->query->select($db->quote($_archive_mode ? 1 : 0) . ' AS ' . $db->quoteName('isArchiveMode'));
        $this->query->select($this->getState('list.select', 'o.*, IF(' . $db->quoteName('o.DATEINP') . ' < DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.mdays') . ', 0) day), ' . $db->quoteName('f.NAME') . ', "") AS ' . $db->quoteName('firm') . ', ' . $db->quoteName('m.NAME', 'mraion')));
        $this->query->select('IF((IFNULL(' . $db->quoteName('g.days') . ', 0)>0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)), "0", "1") AS ' . $db->quoteName('expired'));

        if($this->currentView != 'buildings') {
            $this->query->select('IF(' . $db->quoteName('o.date_checked_to') . ' > NOW(), "1", "0") AS ' . $db->quoteName('checked'));
        }

        if($act == 'pdf') {
            $this->query->select($db->quoteName('ag.SIRNAME', 'sir') . ', ' . $db->quoteName('ag.NAME', 'nm') . ', ' . $db->quoteName('ag.PHONE', 'ph') . ', ' . $db->quoteName('ag.ADDPHONE', 'adph'));
        }

        // Подключение таблиц ------------------------------------------------------------------------------------------
        $this->query->from($db->quoteName($this->objectsTableName . (($_archive_mode) ? '_archive' : ''), 'o'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('o.CITYID') . '=' . $db->quoteName('c.id'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'r') . ' ON ' . $db->quoteName('o.RAIONID') . '=' . $db->quoteName('r.id'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('o.STREETID') . '=' . $db->quoteName('s.id'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocfirms', 'f') . ' ON ' . $db->quoteName('o.COMPID') . '=' . $db->quoteName('f.id'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocagents', 'ag') . ' ON ' . $db->quoteName('ag.COMPID') . '=' . $db->quoteName('f.id') . ' AND ' . $db->quoteName('ag.id') . '=' . $db->quoteName('o.agentid'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsgroup', 'g') . ' ON ' . $db->quoteName('g.id') . '=' . $db->quoteName('ag.grp'));
        $this->query->join('LEFT', $db->quoteName('#__sttnmlsvocmraion', 'm') . ' ON ' . $db->quoteName('o.MRAIONID') . '=' . $db->quoteName('m.id'));


        // Общая секция WHERE запроса ----------------------------------------------------------------------------------
        $this->query->where($db->quoteName('ag.flag') . '<3 AND ' . $db->quoteName('ag.flag') . '<>1' . ' AND ' . $db->quoteName('o.flag') . ' < 3');

        if($phone) {
            //> Поиск по номеру телефона есть 3 варианта:

            // 1) Поиск незарегистрированных пользователей
            if(!$user->id OR !$this->access->seeContactsObjects) {
                $this->query->where('(' . $db->quoteName('ag.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR ' . $db->quoteName('f.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ')');
            }

            // 2) Поиск для администратора системы
            if($this->access->isAdmin) {
                $this->query->where('(' . $db->quoteName('ag.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR ' . $db->quoteName('f.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR ' . $db->quoteName('o.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' )' );
            } else {
                // 3) Поиск для остальных агентов
                if($this->access->isModerator) {
                    if($this->currentAgent->boss) {
                        $this->query->where('(' . $db->quoteName('ag.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR ' . $db->quoteName('f.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR (' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ' AND ' . $db->quoteName('o.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . '))' );
                    } else {
                        $this->query->where('(' . $db->quoteName('ag.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR ' . $db->quoteName('f.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . ' OR (' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ' AND ' . $db->quoteName('o.AGENTID') . '=' . $db->quote($this->currentAgent->ID) . ' AND ' . $db->quoteName('o.phones') . ' LIKE ' . $db->quote('%' . $phone . '%') . '))' );
                    }
                }
            }
        }

        //> Учет действия над объектами Продажа/Аренда
        if($this->stype) {
            //> Аренда
            $this->query->where($db->quoteName('o.VARIANT') . '=' . $db->quote(2));
        } else {
            //> Продажа
            $this->query->where($db->quoteName('o.VARIANT') . '<>' . $db->quote(2));
        }

        if($street_id) {
            //> Передан ID улицы
            $this->query->where($db->quoteName('o.streetid') . '=' . $db->quote($street_id));
        } elseif($street) {
            //> Передано название улицы
            $this->query->where($db->quoteName('s.NAME') . ' LIKE ' . $db->quote('%' . $street . '%'));
        }

        //> Передан ID агенства
        if($agency) {
            //> Поиск по ID агенствам
            if($this->currentView == 'buildings') {
                //> Поиск по полю COMP если запрошен вид НОВОСТРОЕК
                $this->query->where($db->quoteName('o.comp') . '=' . $db->quote($agency));
            } else {
                //> Для остальные видов используется поле COMPID
                $this->query->where($db->quoteName('o.COMPID') . '=' . $db->quote($agency));
            }
        }

        //> Передан ID агента
        if($agent) {
            //> Поиск по ID объекта
            $this->query->where($db->quoteName('o.AGENTID') . '=' . $db->quote($agent));
            // если список по одному агенту, и не он смотрит список, то в списке не должно быть объявлений на премодерации
            $this->query->where('(' . $db->quoteName('o.DATEINP') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.mdays') . ', 0) day) OR ' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ')');
        }

        //> Применение поиска по флагу "Внутренняя база"
        if($this->currentAgent) {
            //> Пользователь залогинен
            if($_internal_base) {
                //> Передан флаг поиска "внутренняя база"
                if($this->access->isAdmin) {
                    $this->query->where($db->quoteName('o.close') . '=' . $db->quote(1));
                } else {
                    if($this->currentAgent->boss) {
                        $this->query->where('((' . $db->quoteName('o.agents') . ' LIKE "%|' . $this->currentAgent->kod . '|%" AND ' . $db->quoteName('o.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ' AND ' . $db->quoteName('o.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('o.AGENTID') . '=' . $db->quote($this->currentAgent->ID) . ' AND ' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ' AND ' . $db->quoteName('o.close') . '=' . $db->quote(1) . '))');
                    } else {
                        $this->query->where('((' . $db->quoteName('o.agents') . ' LIKE "%|' . $this->currentAgent->kod . '|%" AND ' . $db->quoteName('o.close') . '=' . $db->quote(1) . ') OR (' . $db->quoteName('o.AGENTID') . '=' . $db->quote($this->currentAgent->ID) . ' AND ' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) . ' AND ' . $db->quoteName('o.close') . '=' . $db->quote(1) . '))');
                    }
                }
            } else {
                //> Флаг поиска "внутренняя база" не установлен
                $this->query->where('(' . $db->quoteName('o.agents') . ' LIKE "%|' . $this->currentAgent->kod . '|%" OR ' . $db->quoteName('o.close') . '=' . $db->quote(0) . ' OR (' . $db->quoteName('o.AGENTID') . '=' . $db->quote($this->currentAgent->ID) . ' AND ' . $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID).'))');
            }
        } else {
            //> Пользователь не залогинен
            $this->query->where($db->quoteName('o.close') . '=' . $db->quote(0));
        }
		
		//> Применение поиска по флагу "База агентов"
        //if (!$_archive_mode){
            if($this->currentAgent AND ($this->currentAgent->COMPID != $compidsob) ) {
                //> Пользователь залогинен и он не собственник
                if($_realtors_base) {
                    //> Передан флаг поиска "Риэлторская база"
                    if($this->access->isAdmin) {
                        $this->query->where('('.$db->quoteName('o.realtors_base') . '=' . $db->quote(1) . ')');
                    } else {
                        if($this->currentAgent->boss) {
                            $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(1) . ')');
                        } else {
                            $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(1) . ')');
                        }
                    }     
                } else {
                    //> Флаг поиска "риэлторская база" не установлен
                    $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(0) . 'OR' . $db->quoteName('o.realtors_base') . '=' . $db->quote(1) . 'OR' . $db->quoteName('o.realtors_base') . '=' . $db->quote(2) .')');
                }
            } else {
                //> Пользователь не залогинен
                //$this->query->where($db->quoteName('o.realtors_base') . '=' . $db->quote(0) .' OR '. $db->quoteName('o.realtors_base') . '=' . $db->quote(2));
                $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(0) .' OR '.$db->quoteName('o.realtors_base') . '=' . $db->quote(2).')');
            }
            
            //> Применение поиска по флагу "Общая+база агентов"
            if($this->currentAgent AND ($this->currentAgent->COMPID != $compidsob) ) {
                //> Пользователь залогинен и он не собственник
                if($_realtors_and_open_base) {
                    //> Передан флаг поиска "Риэлторская база"
                    if($this->access->isAdmin) {
                        $this->query->where('('.$db->quoteName('o.realtors_base') . '=' . $db->quote(2) . 'OR' . $db->quoteName('o.realtors_base'). '=' . $db->quote(1) . ')');
                    } else {
                        if($this->currentAgent->boss) {
                            $this->query->where('('.$db->quoteName('o.realtors_base') . '=' . $db->quote(2) . 'OR' . $db->quoteName('o.realtors_base'). '=' . $db->quote(1) . ')');
                        } else {
                            $this->query->where('('.$db->quoteName('o.realtors_base') . '=' . $db->quote(2) . 'OR' . $db->quoteName('o.realtors_base'). '=' . $db->quote(1) . ')');
                        }
                    }     
                } else {
                    //> Флаг поиска "риэлторская база" не установлен
                    $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(0) . 'OR' . $db->quoteName('o.realtors_base') . '=' . $db->quote(1) . 'OR' . $db->quoteName('o.realtors_base') . '=' . $db->quote(2) .')');
                }
            } else {
                //> Пользователь не залогинен
                //$this->query->where($db->quoteName('o.realtors_base') . '=' . $db->quote(0) .' OR '. $db->quoteName('o.realtors_base') . '=' . $db->quote(2));
                $this->query->where('(' . $db->quoteName('o.realtors_base') . '=' . $db->quote(0) .' OR '.$db->quoteName('o.realtors_base') . '=' . $db->quote(2).')');
            }
        //}
		
        
		
		//> Передан флаг "Эксклюзив"
        if($_exclusive) {
            //> Флаг "эксклюзив" для поиска
            if($_exclusive_only_nmls) {
                //> Отображение только объектов с флагом эксклюзивности = 3
                $this->query->where($db->quoteName('o.EXCLUSIVE') . '=' . $db->quote(3));
            } else {
                $this->query->where($db->quoteName('o.EXCLUSIVE') . '>0');
            }
        }

        //> Передан флаг "Собственники"
        if($_show_owners && $this->access->seeContactsObjects && $compidsob) {
            //> Флаг отображения "собственник"
            $this->query->where($db->quoteName('f.ID') . '=' . $db->quote($compidsob));
        }

        if($this->raw) {
            if(($_show_owners OR $_show_owners_list) && $this->access->seeContactsObjects && $compidsob) {
                $this->query->where($db->quoteName('f.ID') . '=' . $db->quote($compidsob));
                if($this->access->seeExpiredObjects) {
                    $_show_owner_expired = TRUE;
                }
            } else {
                if(!$builder OR !$building) {
                    $_show_owner_expired = TRUE;
                    $this->query->where('(' . $db->quoteName('ag.userid') . '=' . $db->quote($user->id) . ' AND ' . $db->quoteName('ag.userid') . '>0)');
                }
            }
        }

        //> Учет запрошенного диапазона цен на объекты -----------------------------------------------------------------
        if($this->currentView == 'buildings') {
            $this->setAdditionalWhereFromState($this->query, 'price_ot', 'o.pricefrom', '>=');
            $this->setAdditionalWhereFromState($this->query, 'price_do', 'o.priceto', '<=');
        } else {
            $this->setAdditionalWhereFromState($this->query, 'price_ot', 'o.PRICE', '>=');
            $this->setAdditionalWhereFromState($this->query, 'price_do', 'o.PRICE', '<=');
        }
        //> END Учет запрошенного диапазона цен на объекты -------------------------------------------------------------


        // Учет запрошенных параметров площади -------------------------------------------------------------------------
        switch($this->currentView) {
            case 'buildings':
                $this->setAdditionalWhereFromState($this->query, 'area_ot', 'o.areafrom', '>=');
                $this->setAdditionalWhereFromState($this->query, 'area_do', 'o.areato', '<=');
                break;

            case 'coms':
                $this->setAdditionalWhereFromState($this->query, 'area_ot', 'o.SQUEAR', '>=');
                $this->setAdditionalWhereFromState($this->query, 'area_do', 'o.SQUEAR', '<=');
                break;

            default:
                $this->setAdditionalWhereFromState($this->query, 'area_ot', 'o.AAREA', '>=');
                $this->setAdditionalWhereFromState($this->query, 'area_do', 'o.AAREA', '<=');
                break;
        }
        // END Учет запрошенных параметров площади ---------------------------------------------------------------------


        // Учет Региона/Города -----------------------------------------------------------------------------------------
        if($city_id) {
            $this->query->where($db->quoteName('o.CITYID') . '=' . $db->quote($city_id));
        } else {
            $this->setAdditionalWhereFromState($this->query, 'city', 'o.CITYID');
        }
        // END Учет Региона/Города -------------------------------------------------------------------------------------

        // Учет Района/округа ------------------------------------------------------------------------------------------
        if($raion_id) {
            $this->query->where($db->quoteName('o.RAIONID') . '=' . $db->quote($raion_id));
        } else {
            $this->setAdditionalWhereFromStateCheckbox($this->query, 'raions', 'o.RAIONID');
        }
        // END Учет Района/округа --------------------------------------------------------------------------------------

        // Учет Микрорайона --------------------------------------------------------------------------------------------
        if($mraion_id) {
            $this->query->where($db->quoteName('o.MRAIONID') . '=' . $db->quote($mraion_id));
        } else {
            $this->setAdditionalWhereFromStateCheckbox($this->query, 'mraions', 'o.MRAIONID');
        }
        // END Учет Микрорайона ----------------------------------------------------------------------------------------



        // Временные параметры отображения объектов --------------------------------------------------------------------
        if(!$_show_owners_list && !$_archive_mode) {
            if($this->raw) {
                if($_expired && ($this->currentAgent OR $this->access->isAdmin)) {
                    if($this->access->isAdmin) {
                        //> Флаг отображения непродленных
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ', 0)<=0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                    } else {
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ', 0)<=0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                        $this->query->where(
                            '(' .
                                $db->quoteName('o.agents') . ' LIKE "%|' . $this->currentAgent->kod . '|%"' . ' OR ' .
                                $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) .
                            ')'
                        );
                    }
                } else {
                    if(!$_show_owner_expired) {
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                    }
                }
            } else {
                //> Передан флаг "Непродленные"
                if($_expired && ($this->currentAgent OR $this->access->isAdmin)) {
                    if($this->access->isAdmin) {
                        //> Флаг отображения непродленных
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ', 0)<=0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                    } else {
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ', 0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ', 0)<=0 AND ' . $db->quoteName('o.DATECOR') . '<DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                        $this->query->where(
                            '(' .
                                $db->quoteName('o.agents') . ' LIKE "%|' . $this->currentAgent->kod . '|%"' . ' OR ' .
                                $db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID) .
                            ')'
                        );
                    }
                } else {
                    //> Флаг непродленных не передавался
                    if($this->currentView != 'buildings') {
                        //> Для всех видов кроме НОВОСТРОЕК
//                        if($this->currentAgent) {
//                            //> Агент "вошел"
//                            $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
//                        } else {
//                            //> Агент не "вошел"
//                            $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
//                        }
                        $this->query->where('((IFNULL(' . $db->quoteName('g.days') . ', 0) > 0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL IFNULL(' . $db->quoteName('g.days') . ',0) day)) OR (IFNULL(' . $db->quoteName('g.days') . ',0)<=0 AND ' . $db->quoteName('o.DATECOR') . '>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
                    }

                }
            }
        } elseif(!$_show_owners_list && $_archive_mode) {
            if(!$this->access->isAdmin) {
                $this->query->where($db->quoteName('o.COMPID') . '=' . $db->quote($this->currentAgent->COMPID));

                // Если не является боссом, то добавляется еще и условие проверки архиватора
                if(!$this->currentAgent->boss) {
                    $this->query->where($db->quoteName('o.archiver_id') . '=' . $db->quote($this->currentAgent->userid));
                }
            }
        }

        // END Временные параметры отображения объектов ----------------------------------------------------------------



        // Применения дополнительных параметров запроса по отношению к конкретной модели
        $this->setPersonalConditions();


        // Обработка настроек сортировки ---------------------------------------
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');

        if($params->get('f_exclusive_only_nmls', 0)) {
            $this->query->order($db->quoteName('EXCLUSIVE') . '=' . $db->quote(3) . ' DESC');
        }

        if($this->currentView != 'buildings') {
            $this->query->order($db->quoteName('o.upobj') . ' DESC');
        }

        if($act != 'pdf') {
            if($orderCol == '' OR $orderCol == 'ordering') {
                if($this->currentView == 'buildings') {
                    $this->query->order($db->quoteName('o.dateyear') . ' ASC, ' . $db->quoteName('o.datekvar') . ' ASC');
                } else {
                    $this->query->order($db->quoteName('checked') . ' > 0 DESC');
                    $this->query->order($db->quoteName('o.PICCOUNT') . '>0 DESC, ' . $db->quoteName('o.DATEINP') . ' DESC');
                }
            } elseif($orderCol == 'PICCOUNT' && strtoupper($orderDirn) == 'DESC') {
                $this->query->order($db->quoteName('o.PICCOUNT') . '>0 DESC, ' . $db->quoteName('o.DATEINP') . ' DESC');
            } elseif($orderCol == 'o.DATEINP' && strtoupper($orderDirn) == 'ASC') {
                if($this->currentView == 'buildings') {
                    $this->query->order($db->quoteName('o.dateyear') . ' DESC, ' . $db->quoteName('o.datekvar') . ' DESC');
                } else {
                    $this->query->order($db->quoteName('o.DATEINP') . ' DESC');
                }
            } elseif($orderCol == 'o.DATEINP' && strtoupper($orderDirn) == 'DESC') {
                if($this->currentView == 'buildings') {
                    $this->query->order($db->quoteName('o.dateyear') . ' ASC, ' . $db->quoteName('o.datekvar') . ' ASC');
                } else {
                    $this->query->order($db->quoteName('o.DATEINP') . ' ASC');
                }
            } else {
                if($this->currentView == 'buildings') {
                    if($orderCol == 'o.AAREA') {
                        $orderCol = 'o.areafrom';
                    }

                    if($orderCol == 'o.PRICE') {
                        $orderCol = 'o.pricefrom';
                    }
                }
                $this->query->order($db->quoteName($orderCol) . ' ' . $orderDirn);
            }
        }



//        print_r($this->getState());
//        exit;

//        print_r((string)$this->query->dump());
// exit;

        return $this->query;
    }

    protected function populateState($ordering = NULL, $direction = NULL)
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $params = JComponentHelper::getParams('com_sttnmls');
        $this->setUpdateFilterState();

        // GetVars
        $act = $app->input->getString('act', '');

        // Обновление в сессии параметров сортировки ---------------------------
        $orderCol = $this->getUserStateFromRequest($this->context . '.order', 'filter_order', 'ordering');
        $this->setState('list.ordering', $orderCol);

        $listOrder = $this->getUserStateFromRequest($this->context . '.orderdir', 'filter_order_Dir');
        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
            $listOrder = 'ASC';
        }
        $this->setState('list.direction', $listOrder);

        // Обработка пагинации -------------------------------------------------
        if ($this->raw OR $act == 'pdf') {
            $this->_updateFilterState = FALSE;
            $this->setState('list.limit', 1000);
            $this->setState('list.start', 0);
        } else {
            $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
            if(!$limit) {
                //> Не должно быть нуля, иначе положит сервер при большом кол-ве записей
                $limit = 25;
            }
            $this->setState('list.limit', $limit);

            $limitstart = $app->input->getUint('start', 0);
            $this->setState('list.start', $limitstart);
        }

        // Обновление в сессии параметров поиска -------------------------------
        $filters = $this->getDefaultFiltersValue();
        foreach($filters as $filter => $value) {
            if(!isset($value['getVar'])) {
                $this->setFilterState($filter, $value['value'], $value['type']);
            }
        }
    }

    public function getOptCity()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        return SttNmlsHelper::getOptCity();
    }

    /**
     * Получение свойств объектов по их ID
     *
     * @param   integer|array of integer    $option_id - ID свойства/свойств
     * @return  object|array of object
     */
    public function getOptionsByID($option_id, $razdel = 0)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(TRUE);
        $query->select('*');
        $query->from($db->quoteName('#__sttnmlsvocalldata'));
        $query->order($db->quoteName('NAME') . ' ASC');
        if($razdel) {
            $query->where($db->quoteName('RAZDELID') . '=' . $db->quote($razdel));
        }

        if(is_array($option_id) && count($option_id) > 0) {
            $query->where($db->quoteName('ID') . ' IN (' . implode(',', $option_id) . ')');
            $db->setQuery($query);
            $result = $db->loadObjectList();
        } else {
            $query->where($db->quoteName('ID') . '=' . $db->quote($option_id));
            $db->setQuery($query);
            $result = $db->loadObject();
        }
        return $result;
    }

    function getExpiredObjectsOids($table = '', $additional_where = array(), $compid = 0)
    {
        $com_params = JComponentHelper::getParams('com_sttnmls');
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $agent = $mAgent->getAgentInfoByID();
        $days = $com_params->get('days', 30);
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        $result = array();
        if($table) {
            $query->select($db->quoteName('oid'));
            $query->from($db->quoteName($table));
            if($compid && ($this->access->isAdmin OR ($compid == $agent->COMPID && $agent->boss))) {
                $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
            } else {
                $query->where($db->quoteName('COMPID') . '=' . $db->quote($agent->COMPID));
                $query->where($db->quoteName('AGENTID') . '=' . $db->quote($agent->ID));
            }
            $query->where($db->quoteName('DATECOR') . '<= DATE_SUB(NOW(), INTERVAL ' . (int)$days . ' day)');
            $query->where($db->quoteName('close') . '=' . $db->quote(0));
			$query->where($db->quoteName('realtors_base') . '=' . $db->quote(0));
            $query->where($db->quoteName('flag') . '<' . $db->quote(3));
            if($additional_where) {
                foreach($additional_where as $where) {
                    $query->where($where);
                }
            }
            $db->setQuery($query);
            $result = $db->loadColumn();
        }

        return $result;
    }

    function changeExpiredObjectEditDate()
    {
        $app = JFactory::getApplication();
        $db = $this->getDbo();
        $user = JFactory::getUser();

        $tables = array(
            'aparts'    =>  '#__sttnmlsaparts',
            'coms'      =>  '#__sttnmlscommerc',
            'garages'   =>  '#__sttnmlsgarages',
            'houses'    =>  '#__sttnmlshouses'
        );

        //
        $table = $app->input->get('table', '', 'string');
        $stype = $app->input->get('stype', 0, 'uint');
        $compid = $app->input->get('compid', 0, 'uint');

        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE'));
            return false;
        }

        if(!isset($tables[$table])) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_PARAMS_ERROR'));
            return false;
        }

        //-> 2. Проверка возможности редактирования объекта пользователем(агентом)
        if($stype) {
            //> Аренда
            $where[] = $db->quoteName('VARIANT') . '=' . $db->quote(2);
        } else {
            //> Продажа
            $where[] = $db->quoteName('VARIANT') . '<>' . $db->quote(2);
        }
        $expired_objects = $this->getExpiredObjectsOids($tables[$table], $where, $compid);

        // Если просроченных объектов нет, выход
        if(!$expired_objects) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND'));
            return false;
        }

        try {
            $query = $db->getQuery(true);
            $query->set($db->quoteName('DATECOR') . '=NOW()');
            $query->where($db->quoteName('oid') . ' IN(' . implode(', ', $expired_objects) . ')');
            $query->update($db->quoteName($tables[$table]));
            $db->setQuery($query);
            $db->execute();
        } catch (Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return true;
    }


    function changeExpiredObjectEditDateALL()
    {
        //
        $db = $this->getDbo();
        $user = JFactory::getUser();


        //=================================================//
        //== Проверка прав доступа к операции сохранения ==//
        //=================================================//

        //-> 1. Проверка залогиненности пользователя
        if(!$user->id) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE'));
            return false;
        }

        //-> 2. Проверка возможности редактирования объекта пользователем(агентом)
        $expired_objects = array();

        // Получение ID объектов из таблицы квартир и комнат
        $aparts_expired = $this->getExpiredObjectsOids('#__sttnmlsaparts');
        if($aparts_expired) {
            $expired_objects['#__sttnmlsaparts'] = $aparts_expired;
        }

        // Получение ID объектов из таблицы коммерческой недвижимости
        $commerc_expired = $this->getExpiredObjectsOids('#__sttnmlscommerc');
        if($commerc_expired) {
            $expired_objects['#__sttnmlscommerc'] = $commerc_expired;
        }

        // Получение ID объектов из таблицы гаражей
        $garages_expired = $this->getExpiredObjectsOids('#__sttnmlsgarages');
        if($garages_expired) {
            $expired_objects['#__sttnmlsgarages'] = $garages_expired;
        }

        // Получение ID объектов из таблицы гаражей
        $houses_expired = $this->getExpiredObjectsOids('#__sttnmlshouses');
        if($houses_expired) {
            $expired_objects['#__sttnmlshouses'] = $houses_expired;
        }

        // Если просроченных объектов нет, выход
        if(!$expired_objects) {
            $this->setError(JText::_('COM_STTNMLS_MESSAGE_ERROR_OBJECT_NOT_FOUND'));
            return false;
        }

        try {
            foreach($expired_objects as $table => $oids) {
                $query = $db->getQuery(true);
                $query->set($db->quoteName('DATECOR') . '=NOW()');
                $query->where($db->quoteName('oid') . ' IN(' . implode(', ', $oids) . ')');
                $query->update($db->quoteName($table));
                $db->setQuery($query);
                $db->execute();
            }
        } catch (Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return true;
    }

}