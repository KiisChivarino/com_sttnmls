<?php defined('_JEXEC') or die ('Restricted access');

class SttnmlsViewSignup extends JViewLegacy
{    
    public function display($tpl = NULL)
    {
        $app = JFactory::getApplication();
        $layout = JRequest::getVar('layout', 'default');
        $user = JFactory::getUser();
        
        // Если пользователь вошел, то ошибка
        if($user->id) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_USER_LOGGED_IN'), 'error');
            $app->redirect('index.php');   
            return FALSE;
        }

        if($layout == 'activate') {
            $model = $this->getModel();
            $this->token = $this->get('Token');
            $this->user = $this->get('User');
            $this->realtor = $this->get('Profile');
            $this->catf = $this->get('Catf');
            
            if($this->realtor == 1) {
                $this->firms = $model->getFirmsList(JComponentHelper::getParams('com_sttnmls')->get('catfrealtor', 0));
            } else {
                $this->firms = $model->getFirmsList();
            }
            if($this->realtor==2) {
                $this->firms = str_replace('validate-sttnum required', '', $this->firms);
            }
        }
        
        parent::display($tpl);
    }


}