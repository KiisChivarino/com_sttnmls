<?php defined('_JEXEC') or die('Restricted access');

require_once (JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlslocal.php');
require_once (JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php');

class SttNmlsController extends JControllerLegacy {

    function display($cachable = false, $urlparams = Array()) {
        parent::display();
    }
    
    /**
     * Сохранение профиля
     * 
     * @return type
     */
    public function saveprofile()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $model = $this->getModel('agent');
        if (!$model->save()){
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=agent&layout=profile&opentab=tab-contacts'), $model->getError());
            return;
        }
        $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=agent&layout=profile&opentab=tab-contacts'), JText::_('COM_STTNMLS_SAVED') );
        return;
    }
    
    /**
     * Выгрузка аватара агента
     */
    function uploadavatar()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=uploadavatar&format=raw') . '"');


        $result = array(
            'files'     =>  array(),
            'errors'    =>  array()
        );
        
        $model = $this->getModel('agent');
        $result['files'] = $model->addPhoto();
        $result['errors'] = $model->getErrors();
        
        echo json_encode($result);
    }

    /**
     * Выгрузка лого фирмы
     */
    function uploadFirmLogo()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=uploadFirmLogo&format=raw') . '"');


        $result = array(
            'files'     =>  array(),
            'errors'    =>  array()
        );

        $model = $this->getModel('firm');
        $result['files'] = $model->addPhoto();
        $result['errors'] = $model->getErrors();

        echo json_encode($result);
        JFactory::getApplication()->close();
    }
    
    /**
     * Выгрузка аватара агента
     */
    function uploadObjectPhoto()
    {
        //
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();


        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=uploadObjectPhoto&format=raw') . '"');


        $result = array(
            'files'     =>  array(),
            'errors'    =>  array()
        );



        //
        $view = $app->input->get('view', 'apart', 'string');

        $model = $this->getModel($view);
        $result['files'] = $model->addPhoto();
        $result['errors'] = $model->getErrors();
        
        echo json_encode($result);
        $app->close();
    }


    /**
     * Получение сформированного html списка картинок объекта
     */
    function ajaxGetEditObjectImages()
    {
        $app = JFactory::getApplication();
        $app->input->set('layout', 'edit_photo_list');
        $this->display();
    }

    public function ajaxAddBuildingPlan()
    {
        $app = JFactory::getApplication();
        $app->input->set('layout', 'edit_form_building_plan');
        $this->display();
    }

    function jsonRemoveObjectImage()
    {
        // JOOMLA instances
        $app = JFactory::getApplication();


        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveObjectImage&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'errors'    =>  array()
        );

        $view = $app->input->get('view', 'apart', 'string');
        $model = $this->getModel($view);
        $result['result'] = $model->removeObjectImage();
        $result['errors'] = $model->getErrors();
        
        echo json_encode($result);
        $app->close();
    }

    public function jsonRemoveBuildingProgressImage()
    {
        // JOOMLA instances
        $app = JFactory::getApplication();


        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveObjectImage&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'errors'    =>  array()
        );

        $view = $app->input->get('view', 'apart', 'string');
        $model = $this->getModel($view);
        $result['result'] = $model->removeBuildingProgressImage();
        $result['errors'] = $model->getErrors();

        echo json_encode($result);
        $app->close();
    }


    public function jsonRemoveBuildingPlan()
    {
        // JOOMLA instances
        $app = JFactory::getApplication();


        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveBuildingPlan&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $view = $app->input->get('view', 'apart', 'string');
        $model = $this->getModel($view);
        $result['result'] = $model->removeBuildingPlan();
        $result['messages'] = $model->getErrors();

        echo json_encode($result);
        $app->close();
    }

    public function jsonRemoveBuildingProjectFile()
    {
        // JOOMLA instances
        $app = JFactory::getApplication();


        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveBuildingPlan&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $view = $app->input->get('view', 'building', 'string');
        $model = $this->getModel($view);
        $result['result'] = $model->removeBuildingProjectFile();
        $result['messages'] = $model->getErrors();

        echo json_encode($result);
        $app->close();
    }

    public function jsonRemoveBuildingDogovorFile()
    {
        // JOOMLA instances
        $app = JFactory::getApplication();


        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveBuildingPlan&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $view = $app->input->get('view', 'building', 'string');
        $model = $this->getModel($view);
        $result['result'] = $model->removeBuildingDogovorFile();
        $result['messages'] = $model->getErrors();

        echo json_encode($result);
        $app->close();
    }
        
    function edit()
    {
        JRequest::setVar('layout','edit');
        $this->display();
    }
    
    function save()
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $app = JFactory::getApplication();

        //
        $viewName = $app->input->get('view', $this->default_view, 'string');

        if($app->input->get('cancel', 0)) {
            $stype = $app->input->get('stype', 0, 'uint');
            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', $viewName . 's', '&stype=' . $stype);
            $app->redirect($redirect);
            return TRUE;
        }

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $model = $this->getModel($viewName);
        $model->save();
    }
    
    /**
     * Удаление запроса с последующим редиректом
     * 
     */
    function removeObject()
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        //
        $app = JFactory::getApplication();

        $table = $app->input->get('table', 'apart', 'string');
        $stype = $app->input->get('stype', 0, 'uint');

        $model = $this->getModel($table);
        $model->removeObject();

        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', $table . 's', '&stype=' . $stype));
    }
    
    /**
     * Удаление объекта через JSON запрос
     * 
     */
    function jsonRemoveObject()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveObject&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );
        
        $table = JRequest::getVar('table', 'apart');
        $model = $this->getModel($table);
        $result['result'] = $model->removeObject();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();
        
        echo json_encode($result);
        JFactory::getApplication()->close();
    }
    
    /**
     * Изменение даты создания объекта (поднять объект)
     * 
     */
    function jsonChangeObjectCreateDate()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonChangeObjectCreateDate&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );
                
        $table = JRequest::getVar('table', 'apart');

        $model = $this->getModel($table);
        $result['result'] = $model->changeObjectCreateDate();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();
                
        echo json_encode($result);
        JFactory::getApplication()->close();
    }
    
    /**
     * Изменение даты обновления объекта
     * 
     */
    function jsonChangeObjectEditDate()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonChangeObjectEditDate&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );
        
        $table = JRequest::getVar('table', 'apart');
        $model = $this->getModel($table);
        $result['result'] = $model->changeObjectEditDate();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();
        
        echo json_encode($result);
        JFactory::getApplication()->close();
    }
    
    /**
     * Поворачивает изображение у объекта
     * 
     */
    function jsonRotateObjectImage()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRotateObjectImage&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );
        
        $table = $app->input->get('view', 'apart', 'string');
        $model = $this->getModel($table);
        
        $result['result'] = $model->rotateObjectImage();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();
        
        echo json_encode($result);
        $app->close();
    }

    public function jsonRotateBuildingProgressImage()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRotateObjectImage&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $view = $app->input->get('view', 'building', 'string');
        $model = $this->getModel($view);

        $result['result'] = $model->rotateBuildingProgressImage();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        $app->close();
    }
    
    function jsonDoObjectModerate()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonDoObjectModerate&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );
        
        $table = JRequest::getVar('view', 'apart');
        $model = $this->getModel($table);
        
        $result['result'] = $model->doObjectModerate();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();
        
        echo json_encode($result);
        JFactory::getApplication()->close();
    }
    
    public function jsonDoServiceOffByID()
    {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';

        //
        $app = JFactory::getApplication();

        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonDoServiceOffByID&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $result['result'] = SttNmlsPlatusl::delete($app->input->get('uid', 0, 'uint'));
        $result['messages'] = ($result['result']) ? array(JText::_('COM_STTNMLS_MESSAGE_INFO_SERVICE_SUCCESSFULL_REMOVED')) : array('COM_STTNMLS_MESSAGE_ERROR_GENERAL');
        
        echo json_encode($result);
        JFactory::getApplication()->close();
    }

    public function jsonChangeAgentPost()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonChangeAgentPost&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $model = $this->getModel('firm');

        $result['result'] = $model->changeAgentPost();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        JFactory::getApplication()->close();
    }

    public function jsonChangeAgentPassword()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonChangeAgentPassword&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $model = $this->getModel('firm');

        $result['result'] = $model->changeAgentPassword();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        JFactory::getApplication()->close();
    }

    public function jsonConfirmAgent()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonConfirmAgent&format=raw') . '"');

        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $model = $this->getModel('firm');

        $result['result'] = $model->confirmAgent();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        JFactory::getApplication()->close();
    }

    public function jsonRemoveAgent()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonRemoveAgent&format=raw') . '"');


        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        $model = $this->getModel('firm');

        $result['result'] = $model->removeAgent();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();

        echo json_encode($result);
        JFactory::getApplication()->close();
    }

    public function platRecalc() {
        // пересчет платных услуг
        // запускается кроном по адресу http://site/index.php?option=com_sttnmls&task=platRecalc&format=raw
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';

        SttNmlsPlatusl::recalc();

        JFactory::getApplication()->close();
    }


    
    public function objectPromotionOn() 
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';


        //
        $app = JFactory::getApplication();

        $par = new stdClass();
        $par->view = str_replace('ss', 's', $app->input->get('view', 'aparts', 'string') . 's');
        $par->cardnum = $app->input->get('cn', 0, 'uint');
        $par->compid = $app->input->get('cid', 0, 'uint');

        SttNmlsPlatusl::addUsl('upobj', $par);

        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', $par->view, '&type=card&cn=' . $par->cardnum . '&cid=' . $par->compid));
        return TRUE;
    }
    
    public function objectPromotionOff()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';

        //
        $app = JFactory::getApplication();

        $view = str_replace('ss', 's', $app->input->get('view', 'aparts', 'string') . 's');
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        if($cardnum && $compid) {
            SttNmlsPlatusl::deleteUpobj($cardnum, $compid, $view);
        }

        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', $view, '&type=card&cn=' . $cardnum . '&cid=' . $compid));
        return TRUE;
    }
    
    	
    function add_plan(){
        $viewName = JRequest::getCmd('view', $this->default_view); 
        if(JRequest::getVar('cancel', '')){
            $stype = JRequest::getInt('stype', 0);
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view='.$viewName.'s&stype='.$stype, false));
        }
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $model = $this->getModel($viewName);
        $model->add_plan();
    }
	
	
    function edit_plan(){
        $viewName = JRequest::getCmd('view', $this->default_view); 
        if(JRequest::getVar('cancel', '')){
            $stype = JRequest::getInt('stype', 0);
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view='.$viewName.'s&stype='.$stype, false));
        }
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $model = $this->getModel($viewName);
        $model->edit_plan();
    }
	
    function del_plan(){
        $viewName = JRequest::getCmd('view', $this->default_view); 
        if(JRequest::getVar('cancel', '')){
            $stype = JRequest::getInt('stype', 0);
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view='.$viewName.'s&stype='.$stype, false));
        }
        $model = $this->getModel($viewName);
        $model->del_plan();
    }
	
	
    public function rfilter()
    {
        // JOOMLA Instances
        $app = JFactory::getApplication();

        // Query VARS
        $key = $app->input->getString('key', '');
        $view = $app->input->getString('view', 'aparts');
        $returnURL = base64_decode($app->input->getString('returnURL', base64_encode(SttNmlsHelper::getSEFUrl('com_sttnmls', $view))));

        if(!JURI::isInternal($returnURL)) {
            $returnURL = SttNmlsHelper::getSEFUrl('com_sttnmls', $view);
        }

        if($key) {
            //в принципе, не важно модель какого типа брать.
            $model = $this->getModel($view);
            $model->resetFilter($key);
        }

        $this->setRedirect($returnURL);
        return;
    }
    
    function ajax_mr()
    {
        // JOOMLA instance
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Полученные из запроса данные
        $city = $app->input->post->get('city', 0, 'uint');
        $select = $app->input->post->get('select', 0, 'uint');
        $rid = $app->input->post->get('rid', 0, 'uint');
        if($select) {
            $mr_checked = $app->input->post->get('mr_checked', 0, 'uint');
        } else {
            $mr_checked = $app->input->post->get('mr_checked', array(), 'array');
        }

        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('a.ID', 'a.NAME'), array('value', 'text')));
        $query->from($db->quoteName('#__sttnmlsvocmraion', 'a'));
        if($rid && $rid != 1) {
            $query->where($db->quoteName('a.RAIONID') . ' IN (' . $db->quote($rid) . ')');
        }

        if($city) {
            $query->where($db->quoteName('a.CITYID') . '=' . $db->quote($city));
        } else {
            $query->where($db->quoteName('CITYID') . ' <> -1');
        }
        $db->setQuery($query);
        $res = $db->loadObjectList();



        $option = array();

        if($res) {
            foreach($res as $o) {
                $option[] = JHTML::_('select.option', $o->value, $o->text, 'value', 'text');
            }
        } else {
            $option[] = JHTML::_('select.option', 0, JTEXT::_('JNO'), 'value', 'text');
        }

        echo JHTML::_('select.genericlist', $option, 'jform[mraionid]', ' class="form-control"', 'value', 'text', $mr_checked);


//
//        $selector_node = '<select id="mraionid" name="jform[mraionid]" class="form-control">';
//        if($res) {
//
//        }
//
//
//        $selector_node .= '</select>';
//
//        if($select) {
//            if(!$res) {
//                echo '';
//                return;
//            }
//            echo '<select id="mraionid" name="jform[mraionid]" class="form-control">';
//        }
//        foreach($res as $i){
//            if($select) {
//                echo '<option value="'.$i->value.'" ';
//                if($i->value==$mr_checked) echo ' selected="selected"';
//                echo '>'.$i->text.'</option>';
//            } else {
//                echo "<input type='checkbox' class='mr_check' name='filter_my_MRcheck[]' ";
//                if(in_array($i->value, $mr_checked)) echo "checked";
//                echo " value='".$i->value."'> ".$i->text."<br/>";
//            }
//        }
//        if($select) {
//                echo '</select>';
//        }
    }
    
    function ajax_r()
    {
        //
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        //

        // список районов по городу
        $r_checked = JRequest::getVar('r_checked', array(), 'post', 'array');
        $select = JRequest::getInt('select', '0');

        $query = $db->getQuery(true);
        $query->select(' a.ID as value, a.NAME as text ');
        $query->from(' #__sttnmlsvocraion as a ');
        $city = JRequest::getInt('city', 0);
        if($city) {
            $query->where('a.CITYID=' . $db->Quote($city));
        }
        else {
            $query->where('CITYID <> -1');
        }
        $db->setQuery($query);
        $res=$db->loadObjectList();
        if($select) {
            if(!$res) {
                echo '';
                return;
            }
            echo '<select id="raionid" name="jform[raionid]" onchange="clickraion();" class="form-control">';
        }
        foreach($res as $i) {
            if($select) {
                echo '<option value="'.$i->value.'" ';
                if($i->value==$r_checked[0]) echo ' selected="selected"';
                echo '>'.$i->text.'</option>';
            } else {
                echo "<input type='checkbox' class='r_check' name='filter_my_check[]' ";
                if(in_array($i->value, $r_checked)) echo "checked";
                echo " value='".$i->value."' onclick=\"clickraion();\"> ".$i->text."<br/>";
            }
        }
        if($select) {
            echo '</select>';
        }
    }
    
    function delfav()
    {
        $delfav = JRequest::getVar('cname','');
        if ($delfav){
            setcookie($delfav,'', 1, '/');
            $fav_nums = $_COOKIE['fav_nums'] - 1;
            setcookie("fav_nums", $fav_nums, time()+60*60*24*180,'/');
        }
        $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=favorites',false));
    }

    public function jsonSaveBuildingPlan()
    {
        $result = array(
            'result'    =>  FALSE,
            'messages'    =>  array()
        );

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Set mimetype
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonChangeAgentPost&format=raw') . '"');

        $model = $this->getModel('building');

        $result['result'] = $model->save_building_plan();
        $result['messages'] = ($result['result']) ? $model->getEvents() : $model->getErrors();


        echo json_encode($result);
        $app->close();
    }

    function jsonStreets()
    {
        $doc = JFactory::getDocument();
        $doc->setMimeEncoding('application/json');
        JResponse::setHeader('Content-Disposition','attachment;filename="' . JRoute::_('index.php?option=com_sttnmls&task=jsonStreets&format=raw') . '"');

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        //
        $city = $app->input->get('filter_city', 0, 'uint');
        if($city == -1) { $city = 0; }
        $street = str_replace('"', '', $app->input->get('filter_street', '', 'string'));


        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('NAME', 'value') . ', ' . $db->quoteName('ID', 'id') . ', ' . $db->quoteName('CITYID', 'city'));
        $query->from($db->quoteName('#__sttnmlsvocstreet'));

        if($city) {
            $query->where($db->quoteName('CITYID') . '=' . $db->quote($city));
        }

        if(trim($street) != '') {
            $query->where($db->quoteName('NAME') . ' LIKE ' . $db->quote('%' . $street . '%'));
        }

        $query->order($db->quoteName('value') . ' ASC');
        $query->group('value');
        $query->setLimit(10);
        $db->setQuery($query);
        $result = $db->loadObjectList();

//        print_r((string)$query);

        echo json_encode($result);
        $app->close();
    }
    
    function jsonFirms()
    {
        $cat = JRequest::getInt('cat', 0);
        $inp = JRequest::getVar('filter_search', '', 'post');
        $inp = str_replace('"', '', $inp);
        $n = mb_strlen($inp);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.realname as value, a.ID as id');
        $query->from('#__sttnmlsvocfirms as a');
        if($cat) {
            $query->where(' a.ID in (select idfirm from #__sttnmlsfirmcatf where idcatf=' . $db->quote($cat) . ')');
        }
        if($n) {
            $query->where('a.realname LIKE "%' . $inp . '%"');
        }
        $query->order('value');
        $db->setQuery($query, 0, 10);
        $res = $db->loadObjectList();
        echo json_encode($res);
    }
    
    function ajax_firmlist()
    {
        $cat = JRequest::getInt('cat', 0);
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        echo SttNmlsHelper::getOptFirms(0, '', 0, $cat);
        return;
    }
    
    function upload()
    {
        $viewName = JRequest::getVar('view', 'apart');
        $cardnum = JRequest::getInt('cardnum', 0);
        $compid = JRequest::getInt('compid', 0);
        $typeimg = $_REQUEST['typeimg'];
        $model = $this->getModel($viewName);
        echo $model->addPhoto();
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        echo SttNmlsHelper::getEditPhoto($viewName, $cardnum, $compid,true).'|'.SttNmlsHelper::getEditPhoto($viewName, $cardnum, $compid,false);
    }
	
	
    function upload2()
    {
        $viewName = JRequest::getVar('view', 'apart');
        $cardnum = JRequest::getInt('cardnum', 0);
        $compid = JRequest::getInt('compid', 0);
        $typeimg=$_REQUEST['typeimg'];
        $model = $this->getModel($viewName);
        echo $model->addPhoto();
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        echo SttNmlsHelper::getEditPhoto($viewName, $cardnum, $compid,true,'hod').'|'.SttNmlsHelper::getEditPhoto($viewName, $cardnum, $compid,false,'hod');
    }
	

	
    function regcomplete()
    {
        $lang = JFactory::getLanguage();
        $lang->load('com_users', JPATH_BASE);
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $data  = JRequest::getVar('jform', array(), 'post', 'array');
        if(!isset($data['fam'])) $data['fam']='';
        $params = JComponentHelper::getParams('com_sttnmls');
        if(!isset($data['compid'])){
            $data['compid']=$params->get('compidsob', '0');
        }
        require_once JPATH_SITE. '/components/com_users/models/registration.php';
        $model = new UsersModelRegistration();
        $token = JRequest::getVar('tokenreg', null, 'request', 'alnum');
        if ($token === null || strlen($token) !== 32) {
            $app = JFactory::getApplication();
            $app->redirect('index.php', JText::_('JINVALID_TOKEN'));
            return true;
        }
        $return = $model->activate($token);
        if ($return === false) {
            $this->setRedirect('index.php', JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError()));
            return true;
        }
        $userid=$return->get('id');
        // добавляем запись в таблицу агентов
        $db = JFactory::getDbo();
        $db->setQuery('SELECT id,name,realname FROM #__sttnmlsvocfirms where id='.$db->Quote($data['compid']));
        $firm = $db->loadObject();
        if($firm){
            if(trim($firm->realname)) $data['firmname']=trim($firm->realname);
            else $data['firmname']=trim($firm->name);
        }
        else $data['firmname']='';
        $db->setQuery('SELECT id,compid FROM #__sttnmlsvocagents WHERE userid = '.$db->Quote($userid));
        $agent = $db->loadObject();
        $query = $db->getQuery(true);
        if($agent){
            $query->update('#__sttnmlsvocagents');
            $query->where('id='.$db->quote($agent->id));
            $query->where('compid='.$db->quote($agent->compid));
        } else {
            $query->insert('#__sttnmlsvocagents');
            $query->set('compid='.$db->quote($data['compid']));
            $query->set('id='.$db->quote($userid*100));
        }
        $query->set('sirname='.$db->quote($data['fam']));
        $query->set('name='.$db->quote($data['name']));
        $query->set('secname='.$db->quote($data['name2']));
        $query->set('phone='.$db->quote($data['phone']));
        $query->set('email='.$db->quote($data['email']));
        $query->set('userid='.$db->quote($userid));
        $query->set('fullinf='.$db->quote($data['fam'].' '.$data['name'].' '.$data['name2'].','.$data['phone']));
		
        $compidsliv=$params->get('compidsliv', '0');
        if($data['realtor']==1){
            $grp = $params->get('groupid2', '0');
            if($compidsliv && $compidsliv==$data['compid'])
                $query->set('flag=2');
            else
                $query->set('flag=1');
        } else {
            $grp = $params->get('groupid1', '0');
            $query->set('flag=2');
        }
        $query->set('grp='.$db->quote($grp));
        $db->setQuery($query);
        $db->query();
        $db->setQuery('UPDATE `#__users` SET `name`='.$db->quote($data['fam'].' '.$data['name'].' '.$data['name2']).' WHERE `id` ='.$db->quote($userid));
        $db->query();
        $this->sendmail($data);

        $this->setMessage(JText::_('COM_USERS_REGISTRATION_ACTIVATE_SUCCESS'));
        $link1 = JRoute::_('index.php?option=com_users&view=login');
        $returnURL = JRoute::_('index.php?option=com_sttnmls&view=agent&layout=profile');
        $link = new JURI($link1);
        $link->setVar('return', base64_encode($returnURL)); 
        $this->setRedirect($link);
    }
    
    
	
    
	
    function delete()
    {
        $table = JRequest::getVar('table', 'apart');
        $model = $this->getModel($table);
        echo $model->delete();
    }
    
    public function sendmail($data)
    {
        $subject = JText::_('COM_STTNMLS_EMAIL_SUBJECT');
        if($data['realtor']==1){
            $body = JText::sprintf('COM_STTNMLS_REGMAILREALTOR',$data['email'],$data['fam'],$data['name'],$data['name2'],$data['phone'],$data['firmname']);
        } else {
            $body = JText::sprintf('COM_STTNMLS_REGMAILSOBST',$data['email'],$data['name'],$data['name2'],$data['phone']);
        }
        $app = JFactory::getApplication();
        $mailfrom = $app->getCfg('mailfrom');
        $fromname = $app->getCfg('fromname');
        $mail = JFactory::getMailer();
        $db = JFactory::getDbo();
        // get all admin users
        $query = 'SELECT name, email, sendEmail, id' .
                                ' FROM #__users' .
                                ' WHERE sendEmail=1';
        $db->setQuery( $query );
        $rows = $db->loadObjectList();
        // Send mail to all users with users creating permissions and receiving system emails
        foreach( $rows as $row )
        {
            $usercreator = JFactory::getUser($id = $row->id);
            if ($usercreator->authorise('core.create', 'com_users'))
            {
                $mail = JFactory::getMailer();
                $mail->addRecipient($row->email);
                $mail->setSender(array($mailfrom, $fromname));
                $mail->setSubject($subject);
                $mail->setBody($body);
                $sent = $mail->Send();
            }
        }  
    }
    
    
    
    public function saveord()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $model = $this->getModel('ord');

        $model->save();
        $this->setRedirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'ords'));
    }
    
    public function delord()
    {
        $model = $this->getModel('ord');
        if (!$model->delete()){
            $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=ords',false), $model->getError());
            return;
        }
        $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=ords',false));
        return;
    }
    
    public function savesber()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $model = $this->getModel('sber');
        $model->save();
        return;
    }
    
    function showcont()
    {
        $user = JFactory::getUser();
        $userid = $user->get('id');
        if(!$userid) return;
        $typeobj = JRequest::getInt('typeobj', 1);
        $cardnum = JRequest::getInt('cardnum', 0);
        $compid = JRequest::getInt('compid', 0);
//		$date = JFactory::getDate();
        $db = JFactory::getDbo();
        // модератор?
        $db->setQuery('select a.kod, a.activity, g.seecont from #__sttnmlsvocagents as a left join #__sttnmlsgroup as g on g.id=a.grp where a.userid=' . $db->quote($userid));
        $ret=$db->loadObject();
        if($ret->seecont==1 && $ret->activity>0)
        {
            // есть такая запись в журнале модерации?
            $db->setQuery('select a.* from #__sttnmlsmoder as a where a.userid=' . $db->quote($userid) . ' and a.compid=' . $db->quote($compid) . ' and a.cardnum=' . $db->quote($cardnum) . ' and typeobj=' . $db->quote($typeobj));
            $moder=$db->loadObject();
            if(!$moder) {
                // добавляем запись в журнале модерации
                $db->setQuery('insert into #__sttnmlsmoder (userid,kodagent,typeobj,compid,cardnum,dateinp) values (' . $db->quote($userid) . ',' . $db->quote($ret->kod) . ',' . $db->quote($typeobj) . ',' . $db->quote($compid) . ',' . $db->quote($cardnum) . ',now())');
                $db->query();
                // отнимаем балл активности
                $db->setQuery('update #__sttnmlsvocagents set activity=' . $db->quote($ret->activity-1) . ' where kod=' . $db->quote($ret->kod));
                $db->query();
            }
            // получаем доп.информацию по объекту
            $tabname='#__sttnmlsaparts';
            if($typeobj==2) $tabname='#__sttnmlscommerc';
            if($typeobj==3) $tabname='#__sttnmlsgarages';
            if($typeobj==4) $tabname='#__sttnmlshouses';
            $query = $db->getQuery(true);
            $query->select(' a.MISC, a.AGENTID ');
            $query->select('ag.userid as agentuserid, concat(ag.SIRNAME, " ", ag.NAME , " ", ag.SECNAME , "<br/>", ag.PHONE , "<br/>", ag.ADDPHONE) as agentinf');
            $query->select('f.article_url as firm_url ');
            $query->select('f.NAME as firm, f.PHONE as firmphone, f.ADDPHONE as firmphone2 ');
            $query->from($db->quoteName($tabname) . ' AS a ');
            $query->join('LEFT','`#__sttnmlsvocagents` as ag on a.AGENTID=ag.id and a.COMPID=ag.COMPID');
            $query->join('LEFT','`#__sttnmlsvocfirms` as f on a.COMPID=f.id');
            $query->where(' a.COMPID='.$db->quote($compid));
            $query->where(' a.CARDNUM='.$db->quote($cardnum));
            $db->setQuery($query);
            $item=$db->loadObject();
            if($item) {
                $dop='<h3>' . JText::_('COM_STTNMLS_DOP') . '</h3>';
                $dop .= '<span>' . $item->MISC . '</span>';
                $dop = str_replace('|', '', $dop);
                //контакты агента
                require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
                $userthumb = Avatar::getUserThumbAvatar($item->agentuserid);
                $cont = '<td class="nmls_agent agentthumb">';
                $cont .= '<a href="'.JRoute::_('index.php?option=com_sttnmls&view=agent',false).'&agent_id='. $item->AGENTID . '&comp_id=' . $compid. '"><img src="'.$userthumb.'" alt="" /></a>';
                $cont .= '</td><td class="vtop">';
                $agent = trim($item->agentinf);
                $cont .= (strpos($agent,',')) ? implode(',<br />', explode(',', $agent)) : $agent;
                $cont .= '</td>';
                $cont = str_replace('|', '', $cont);
                // контакты фирмы
                $imgfirm = Avatar::getLinkFirmLogo($compid,0);
                $contfirm = '<td class="nmls_agent agentthumb"><a href="';
                if($item->firm_url != '') {
                        $contfirm .=  $item->firm_url;
                }else{
                        $contfirm .= JRoute::_('index.php?option=com_sttnmls&view=firm',false).'&agency='. $compid;
                }
                if($item->firm_url != '') $contfirm .= ' target="_blank"'; 
                $contfirm .= '><img src="' . $imgfirm .'" alt="" /></a></td><td class="vtop">';
                $contfirm .= $item->firm. '<br/>'.$item->firmphone.'<br/>'.$item->firmphone2.'<br/></td>';
                $contfirm = str_replace('|', '', $contfirm);
                //
                require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
                $listmoder = SttNmlsHelper::getListModer($typeobj, $compid, $cardnum);
                $listmoder = str_replace('|', '', $listmoder);
                echo $dop . '|' .$cont.'|'.$contfirm.'|'.$listmoder;
                return;
            }
        }
        return;
    }
    
    
    
    function complaint()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/complaint.php';



        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $plg_recaptcha = JPluginHelper::getPlugin('captcha', 'recaptcha');
        $plg_recaptcha_enabled = JPluginHelper::isEnabled('captcha', 'recaptcha');
        $plg_recaptcha_params = new JRegistry($plg_recaptcha->params);


        // Request VARS
        $view = $app->input->get('view', 'apart', 'string');
        $typeobj = $app->input->get('typeobj', 0, 'uint');
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');

        $name = $app->input->get('complname', '', 'string');
        $email = $app->input->get('complemail', '', 'string');
        $reason = $app->input->get('reason', 0, 'uint');
        $description = $app->input->get('description', '', 'string');

        if(!$typeobj OR !$cardnum OR !$compid OR !$reason) {
            $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'), 'error');
            $app->redirect();
            return FALSE;
        }

        // Проверка капчи, если включена
        if ($params->get('use_captcha_security') && $plg_recaptcha_enabled && $plg_recaptcha_params->get('version', '1.0') == '2.0') {
            // Подключение плагина капчи
            JPluginHelper::importPlugin('captcha');

            // Подключение диспетчера
            $dispatcher = JEventDispatcher::getInstance();

            $post = JFactory::getApplication()->input->post;
            $chk_result = $dispatcher->trigger('onCheckAnswer', $post->get('recaptcha_response_field'));
            if(!$chk_result[0]){
                $app->enqueueMessage(JText::_('COM_STTNMLS_MESSAGE_ERROR_CAPTCHA_CODE'), 'error');
                $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', $view, '&type=card&cn=' . $cardnum . '&cid=' . $compid));
                return FALSE;
            }
        }

        $data = new stdClass();
        $data->userid = $user->id;
        $data->typeobj = $typeobj;
        $data->cardnum = $cardnum;
        $data->compid = $compid;
        $data->rs = $reason;
        $data->desc = $description;

        if($data->userid) {
            $data->email = $user->get('email');
            $data->name = $user->get('name');
            $db->setQuery('select a.kod from #__sttnmlsvocagents as a where a.userid=' . $db->quote($data->userid));
            $data->kodagent = $db->loadResult();
            if(!$data->kodagent) {
                $data->kodagent = 0;
            }
        } else {
            $data->kodagent = 0;
            $data->name = $name;
            $data->email = $email;
        }
        $data->id = Complaint::store($data);
        Complaint::sendmail($data);

        $app->enqueueMessage(JText::_('COM_STTNMLS_COMPLAINT_OK'));
        $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', $view, '&type=card&cn=' . $cardnum . '&cid=' . $compid));
        return TRUE;
    }
    
    function activcomplaint()
    {
        require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/complaint.php';
        echo '<div>'.Complaint::activlink().'</div>';
        return;
    }

    public function sendmailadmin($subject,$body)
    {
        $app = JFactory::getApplication();
        $mailfrom = $app->getCfg('mailfrom');
        $fromname = $app->getCfg('fromname');
        $params = JComponentHelper::getParams('com_sttnmls');
        $sendadmin = $params->get('adminemail', '');
        if(!$sendadmin) {
            $db = JFactory::getDbo();
            // get all admin users
            $query = 'SELECT name, email, sendEmail, id' .
                                    ' FROM #__users' .
                                    ' WHERE sendEmail=1';
            $db->setQuery( $query );
            $rows = $db->loadObjectList();
            // Send mail to all users with users creating permissions and receiving system emails
            foreach( $rows as $row )
            {
                $usercreator = JFactory::getUser($id = $row->id);
                if ($usercreator->authorise('core.create', 'com_users'))
                {
                    $mail = JFactory::getMailer();
                    $mail->isHTML(true);
                    $mail->addRecipient($row->email);
                    $mail->setSender(array($mailfrom, $fromname));
                    $mail->setSubject($subject);
                    $mail->setBody($body);
                    $sent = $mail->Send();
                }
            }  
        } else {
            $mail = JFactory::getMailer();
            $mail->isHTML(true);
            $mail->addRecipient($sendadmin);
            $mail->setSender(array($mailfrom, $fromname));
            $mail->setSubject($subject);
            $mail->setBody($body);
            $sent = $mail->Send();
        }	
    }
    
    public function robo_pay() {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/robokassa.php';
        jexit(SttNmlsRobokassa::putPay());
        return;		
    }
    
    public function robo_cancel() {
        require_once JPATH_ROOT . '/components/com_sttnmls/helpers/robokassa.php';
        SttNmlsRobokassa::cancelPay();
        $this->setRedirect(JRoute::_('index.php?option=com_sttnmls&view=agent&layout=profile&opentab=2',false));
        return;		
    }
    
    
    

}

