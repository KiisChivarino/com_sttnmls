<?php defined('_JEXEC') or die('Restricted access'); ?>

<h3><?php echo JText::_('COM_STTNMLS_LINK_OBJ');?></h3>
<div class="row objects-list-sm">            
<?php foreach ($this->arLink as $o) : ?>    
    <div class="col-md-3 object" id="<?php echo $o->item->COMPID . '_' . $o->item->CARDNUM ?>">
        <div class="thumbnail similar-object">
            <div class="object-photo text-center">
                <a href="<?php echo $o->url ?>" target="_blank">
                    <img class="img-rounded" src="<?php echo $o->linkimg ?>" alt="" />
                </a>
            </div>
            <div class="object-header">
                <a href="<?php echo $o->url; ?>" class="objectdesc object-link" target="_self" title="">
                <?php printf('<b>%s</b>-комнатная %s', $o->item->ROOMS, $o->item->object) ?>
                </a>
            </div>
            <div class="object-description">
                <?php printf('<b>%s</b>, %s', $o->item->planirovka, $o->item->tip) ?>,
                <?php
                    $s = $o->item->gorod;
                    if($o->item->raion != '' && $o->item->RAIONID>1) {
                        $r = str_replace('р-н', '', $o->item->raion);
                        $r = str_replace('район', '', $r);
                        $r = str_replace('  ', ' ', $r);
                        $s .= ', ' . $r .' р-н';
                    }
                    if($o->item->mraion != '') {
                        $s .= ', ' . $o->item->mraion;
                    }
                    echo $s;
                ?>
                <br />
                <?php printf('<b>Площадь:</b> %s м<sup>2</sup>', $o->item->o_zh_k) ?><br />
                <?php printf('<b>Цена:</b> %s руб.', number_format($o->item->PRICE, 0, ' ', ' ')) ?>
                
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
