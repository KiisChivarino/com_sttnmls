<?php defined('_JEXEC') or die ('Restricted access');

JLoader::import('joomla.application.component.model');

class SttNmlsViewBuilding extends JViewLegacy
{
    function display($tpl = null) 
    {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $building_model = $this->getModel('building');
        
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $layout = $app->input->get('layout', 'edit_photo_list');

        $this->item = $this->get('Item');

        // AJAX data switcher
        switch ($layout) {
            case 'edit_photo_list':
                    if($cardnum == 0 OR $compid == 0) {
                        JError::raiseError('400', JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'));
                        return FALSE;
                    }  
                    
                    $this->item = $building_model->getObjectImages('building', $compid, $cardnum, TRUE);
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                break;

            case 'edit_building_plans_list':
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                    $this->item_building_plans = $building_model->getBuildingPlans($this->item->CARDNUM, $this->item->COMPID);
                break;

            case 'edit_form_building_plan':
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                    $bplan_id = $app->input->get('bplan_id', 0, 'uint');
                    $this->building_plan = $building_model->getBuildingPlan($bplan_id);

                break;

            case 'edit_building_progress_photo_list':
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                break;

        }
        
        parent::display($tpl);
    }
}