<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');
require_once JPATH_COMPONENT.'/helpers/sttnmlsitem.php';

class SttNmlsModelGarage extends SttNmlsModelItem {
	public function __construct($config = array()) {
		$this->setTabname('#__sttnmlsgarages');
		$this->setTp('5');
		parent::__construct($config);
	}
	public function getTitle()
	{
        $pTtl = $this->getHead();
        $pTtl .= ', '.number_format($this->_item->PRICE / 1000, 3, ' ', ' ').' рублей.';
		return $pTtl;
	}
	public function getHead()
	{
		return $this->getHeadItem($this->_item);
	}
	private function getHeadItem($item)
	{
        $pTtl = '';
		if ($item->VARIANT == 2)
		{
			$pTtl = JText::_('COM_STTNMLS_SDAMGR').': '.$item->ulica;
		} else {
			$pTtl = JText::_('COM_STTNMLS_PRODAMGR').': '.$item->ulica;
		}
		return $pTtl;
	}
	public function getDesc()
	{
        return JText::_('COM_STTNMLS_DESC_GR');
	}
	public function getDesc2()
	{
        return JText::_('COM_STTNMLS_DESC_GR');
	}
	public function setDopJoin($query)
	{
		$query->join('LEFT','`#__sttnmlsvocalldata` as d on a.GTYPEID=d.id');
		return;
	}

	public function getDopSelect()
	{
		return ',c.NAME as gorod,
			r.NAME as raion,
			m.NAME as mraion,
			concat(s.NAME,if(a.HAAP <> " ",	CONCAT(",",a.HAAP), ""),if(a.ORIENT<>" ",CONCAT(" /",a.ORIENT), "")) as ulica,
			d.NAME as tip ';
	}
	function getOptGtype()
	{
		return $this->getOpt(15, 'GTYPEID');
	}

	function save()
    {
        //
		$app = JFactory::getApplication();

        //
		$data  = $app->input->post->get('jform', array(), 'array');

        if(!isset($data['cityid']) OR !$data['cityid']) {
            $data['cityid'] = -1;
        }

        $_export = $app->input->post->get('export', 0);
        $_exclusive = $app->input->post->get('EXCLUSIVE', 0);
        //$_close = $app->input->post->get('close', 0);
		$_encumbrance = $app->input->post->get('encumbrance', 0);
        $target_agent = $app->input->post->get('fromag', 0, 'uint');

        $data['export'] = (($_export) ? 1 : 0);
        $data['EXCLUSIVE'] = (($_exclusive) ? 1 : 0);
        //$data['close'] = (($_close) ? 1 : 0);
		$data['encumbrance'] = (($_encumbrance) ? 1 : 0);
		
		//доступ к базе
		$_open_and_realtors = $app->input->post->get('open_and_realtors', 0);
		$_access_base = $app->input->post->get('access_base', 0);
		
		switch ((int)$_access_base + (int)$_open_and_realtors){
			case 1:
				$data['close'] = 1;
				$data['realtors_base'] = 0;
				break;
			case 2:
				$data['close'] = 0;
				$data['realtors_base'] = 1;
				break;
			case 3:
				$data['close'] = 0;
				$data['realtors_base'] = 0;
				break;
			case 4:
				$data['close'] = 0;
				$data['realtors_base'] = 2;
				break;
			default:
				//$data['close'] = 0;
				//$data['realtors_base'] = 0;
				//break;
				echo 'Ошибка при определении доступа записи к базе';
				exit;
		}
		
		
        if($target_agent) {
            $data['fromag'] = $target_agent;
        }

        // Сохранение данных
		$this->store($data);
	}

	function getMailBody()
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

		// JOOMLA Instances
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$query = $db->getQuery(TRUE);
		$query->select($db->quoteName('o') . '.*, ' . $db->quoteName('c.NAME', 'gorod'));
		$query->select('CONCAT(' . $db->quoteName('s.NAME') . ', IF(' . $db->quoteName('o.HAAP') . ' <> " ", CONCAT(",", ' . $db->quoteName('o.HAAP') . '), "")) AS ' . $db->quoteName('ulica'));
		$query->select($db->quoteName('r.NAME', 'raion') . ', ' . $db->quoteName('d.NAME', 'tip'));
		$query->from($db->quoteName('#__sttnmlsgarages', 'o'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('o.CITYID') . '=' . $db->quoteName('c.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocraion', 'r') . ' ON ' . $db->quoteName('o.RAIONID') . '=' . $db->quoteName('r.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocstreet', 's') . ' ON ' . $db->quoteName('o.STREETID') . '=' . $db->quoteName('s.id'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvocalldata', 'd') . ' ON ' . $db->quoteName('o.GTYPEID') . '=' . $db->quoteName('d.id'));
		$query->where($db->quoteName('o.COMPID') . '=' . $db->quote($this->_item->compid));
		$query->where($db->quoteName('o.CARDNUM') . '=' . $db->quote($this->_item->cardnum));
		$db->setQuery($query);
		$row = $db->loadObject();

		if(!$row) {
			return '';
		}

		// Подготовка данных для подстановки в письмо
		foreach($row as $key => $value) {
			$data[$key] = $value;
		}

		$data['object_title'] = $this->getHeadItem($row);
		$data['street'] = $row->ulica;
		$data['object_link'] = SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', '&type=card&cn=' . $this->_item->cardnum . '&cid=' . $this->_item->compid);

		return SttNmlsLocal::parseString(JText::_('COM_STTNMLS_EMAIL_ITEM_ADD_ADMIN_NOTIFICATION_BODY_GARAGE'), $data);
	}
}
