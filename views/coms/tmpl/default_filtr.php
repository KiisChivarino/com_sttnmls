<?php

// no direct access
defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.core.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.widget.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.position.js');
$doc->addScript('components/com_sttnmls/assets/js/jquery.ui.autocomplete.js');

$filter_rooms = $this->state->get('filter.rooms', array());
$filter_htype = $this->state->get('filter.htype', array());
$filter_my_check = $this->state->get('filter.my_check', array());
$filter_my_MRcheck = $this->state->get('filter.my_MRcheck', array());
$filter_area_ot = $this->state->get('filter.area_ot', 0);
$filter_area_do = $this->state->get('filter.area_do', 0);
$filter_earea_ot = $this->state->get('filter.earea_ot', 0);
$filter_earea_do = $this->state->get('filter.earea_do', 0);
$filter_price_ot = $this->state->get('filter.price_ot', 0);
$filter_price_do = $this->state->get('filter.price_do', 0); 
$city = 0;
$link = JRoute::_('index.php?option=com_sttnmls&view=coms', false);
if ($this->stype) $class='stype1';
else $class='stype0';
?>
<input type="hidden" name="act" value="" id="actfil"/>
<input type="hidden" name="typepdf" value="" id="typepdf"/>
<input type="hidden" name="fltr_view" value="<?php if(isset($_REQUEST['fltr_view']) && $_REQUEST['fltr_view']==1){ ?>1<?php }else{ ?>0<?php }?>" id="filter_easy"/>
<input type="hidden" name="filter_htype[]" value="null"/>
<input type="hidden" name="filter_rooms[]" value="null"/>
<input type="hidden" name="filter_my_check[]" value="null"/>
<input type="hidden" name="filter_my_MRcheck[]" value="null"/>
<div class="<?php echo $class; ?>"><img src="/components/com_sttnmls/assets/images/home-share.png" style="margin:0px" alt="<?php echo JText::_('COM_STTNMLS_SELL'); ?>" />
	<?php
	if ($this->stype) { 
	
		$l = JRoute::_('index.php?option=com_sttnmls&view=coms&stype=0', false);
		if(isset($_REQUEST['agency']))
		{
			$l.="&agency=".$_REQUEST['agency'];
		}
		if(isset($_REQUEST['agent']))
		{
			$l.="&agent=".$_REQUEST['agent'];
		}
		echo '<a href="'.$l.'" style="text-decoration:underline;font-weight:bold;">'.JText::_('COM_STTNMLS_SELLO').'</a>';
	} else {
		echo JText::_('COM_STTNMLS_SELLO');
	} ?>
</div>
<div class="<?php echo $class; ?>a"><img src="/components/com_sttnmls/assets/images/calendar-day.png" style="margin:0px" alt="<?php echo JText::_('COM_STTNMLS_RENT'); ?>" />
	<?php
	if (!$this->stype)
	{

		$l = JRoute::_('index.php?option=com_sttnmls&view=coms&stype=1', false);
		if(isset($_REQUEST['agency']))
		{
			$l.="&agency=".$_REQUEST['agency'];
		}
		if(isset($_REQUEST['agent']))
		{
			$l.="&agent=".$_REQUEST['agent'];
		}
		echo '<a href="'.$l.'" style="text-decoration:underline;font-weight:bold;">'.JText::_('COM_STTNMLS_RENT').'</a>';
	} else {
		echo JText::_('COM_STTNMLS_RENT');
	} ?>

</div>
<div style="float:right;">
<?php
	/*if($_REQUEST['stype']!=1)
	{*/
	?>
	<?php
		$l = JRoute::_('index.php?option=com_sttnmls&view=coms', false);
	?>
	<!-- noindex -->
	<div style="position:relative; display:inline-block;">
		<a href="#" onclick="if(document.getElementById('print_all').style.display=='none'){document.getElementById('print_all').style.display='block';}else{document.getElementById('print_all').style.display='none'} return false;" style="position:relative; top:10px;" target="_blank"><img src="/printer.png" />
			<span style="position:relative; top:1px; left:2px;">Печатать все (<?php echo $this->countobj;?>)</span>
		</a>
		<div style="position:absolute; background:#fff; border:1px solid #efefef; padding:10px; top:35px; display:none; white-space:nowrap;" id="print_all">
			<div onclick="document.getElementById('adminForm').target='_blank'; document.getElementById('typepdf').value='cli'; document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px; cursor:pointer;">Клиентский</div>
			<div onclick="document.getElementById('adminForm').target='_blank'; document.getElementById('typepdf').value=''; document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;" style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px; cursor:pointer;">Агентский</div>
			<div style="cursor:pointer;" onclick=" document.getElementById('adminForm').target='_blank';document.getElementById('typepdf').value='agcli';  document.getElementById('actfil').value='pdf'; Joomla.submitform(); return false;">Агентский с контактами клиента</div>
		</div>
	</div>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<?php
	$countfav=0;
	foreach($_COOKIE as $name => $value){
			if (ereg('cardids[0-9]+',$name) && $value){
				ereg("tp([0-9]+)cid([0-9]+)cn([0-9]+)", $_COOKIE[$name], $card);
				if($card[1]==4)
				{
					$countfav++;
				}
			}
		}
	?>
	<div style="position:relative; display:inline-block;">
		<a href="#" onclick="if(document.getElementById('print_fav').style.display=='none'){document.getElementById('print_fav').style.display='block';}else{document.getElementById('print_fav').style.display='none'} return false;" style="position:relative; top:10px;" target="_blank"><img src="/printer.png" /> <span style="position:relative; top:1px; left:2px;">Печатать избранные <span id="countfav4">(<?php echo $countfav;?>)</span></span>
		</a>
		<div style="position:absolute; background:#fff; border:1px solid #efefef; padding:10px; top:35px; display:none; white-space:nowrap; z-index:999999 !important" id="print_fav">
			<div style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px; cursor:pointer;"><a  target="_blank" href="/favorites.raw?print=1&tp=4&typepdf=cli">Клиентский</a></div>
			<div style="border-bottom:1px solid #efefef; padding-bottom:5px; margin-bottom:5px; cursor:pointer;"><a  target="_blank" href="/favorites.raw?print=1&tp=4">Агентский</a></div>
			<div style="cursor:pointer;" ><a target="_blank" href="/favorites.raw?print=1&tp=4&typepdf=agcli">Агентский с контактами клиента</a></div>
		</div>
	</div>
	<!-- /noindex -->
	<?php
	//}
	?>
</div>
<div class="realty_filter" style="clear:both">
	<table><input type="hidden" id="city_field" name="city" value="<?php echo $city; ?>" />
	<input type="hidden" name="stype" value="<?php echo $this->stype; ?>" />
	<tr valign="top">
		<td width="200">
			<strong><?php echo JText::_('COM_STTNMLS_HTYPE_LBL'); ?></strong><br/>
			<div class="overfl">
			<div name="filter_htype[]" multiple="multiple" size="3">
			<input type="checkbox" name="filter_htype[]" value="168"<?php if(array_search(168, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE168'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="174"<?php if(array_search(174, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE174'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="175"<?php if(array_search(175, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE175'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="176"<?php if(array_search(176, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE176'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="177"<?php if(array_search(177, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE177'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="178"<?php if(array_search(178, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE178'); ?><br>
			<input type="checkbox" name="filter_htype[]" value="179"<?php if(array_search(179, $filter_htype)!==false) echo ' checked="checked" '; ?> > <?php echo JText::_('COM_STTNMLS_HTYPE179'); ?><br>
			</div></div>
		</td>
		<td width="380" align="center"><b><?php echo JText::_('COM_STTNMLS_SQUPRH'); ?></b> <br />
        
<?php
$params = JComponentHelper::getParams('com_sttnmls');
?>
        
			<div style="text-align: right;"><?php echo JText::_('COM_STTNMLS_AREAOT'); ?>&nbsp;
				<select name="filter_area_ot" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
					<?php 
					if($_REQUEST['stype']==0)
					{
						
						$sq=explode(",",$params->get('sq_comms_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}else{
						$sq=explode(",",$params->get('sq_comms_arenda',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}
					?>
				</select>
				&nbsp;<?php echo JText::_('COM_STTNMLS_TO'); ?>
				<select name="filter_area_do" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
					<?php
					if($_REQUEST['stype']==0)
					{
						
						$sq=explode(",",$params->get('sq_comms_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}else{
						$sq=explode(",",$params->get('sq_comms_arenda',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
							echo '>'.$i.'';
						}
					} ?>
			</select></div>
            
            
            <div style="text-align: right;"><?php echo JText::_('COM_STTNMLS_EAREAOT'); ?>&nbsp;
				<select name="filter_earea_ot" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
				<?php 
					if($_REQUEST['stype']==0)
					{
						
						$sq=explode(",",$params->get('sq2_comms_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_earea_ot == $i && $filter_earea_ot != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}else{
						$sq=explode(",",$params->get('sq2_comms_arenda',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_earea_ot == $i && $filter_earea_ot != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}
					?>
				</select>
				&nbsp;<?php echo JText::_('COM_STTNMLS_TO'); ?>
				<select name="filter_earea_do" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
				<?php
					if($_REQUEST['stype']==0)
					{
						
						$sq=explode(",",$params->get('sq2_comms_sell',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_earea_do == $i && $filter_earea_do != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}else{
						$sq=explode(",",$params->get('sq2_comms_arenda',''));
						foreach ($sq as $i)
						{
							echo '<option value="'.$i.'" ';
							if($filter_earea_do == $i && $filter_earea_do != 0) echo 'selected';
							echo '>'.$i.'';
						}
					}
					?>
				</select>
			</div>
            
            
			<br><div style="text-align: right"><?php echo JText::_('COM_STTNMLS_PRICEOT'); ?>&nbsp&nbsp;<select name="filter_price_ot" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
			<?php
			
			if($_REQUEST['stype']==0)
			{
				$array_price=explode(",",$params->get('price_comms_sell',''));
			}else{
				$array_price=explode(",",$params->get('price_comms_arenda',''));
			}
			
			
			
			foreach($array_price as $one_price)
			{
				echo'<option value="'.$one_price.'" ';
				if($filter_price_ot == $one_price && $filter_price_ot != 0) echo 'selected';
				echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
			}?>
			</select> <?php echo JText::_('COM_STTNMLS_TO'); ?>&nbsp;<select name="filter_price_do" style="width: 100px;"><option value=""><?php echo JText::_('COM_STTNMLS_NOTV'); ?></option>
			<?php
			foreach($array_price as $one_price)
			{
				echo'<option value="'.$one_price.'" ';
				if($filter_price_do == $one_price && $filter_price_do != 0) echo 'selected';
				echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
			} ?>
			</select>
			</div>
		</td>
				<td width="120" style=" text-align: center;"><br>
			<input name="UPD" onclick="document.getElementById('adminForm').target='_self'; document.getElementById('actfil').value='';" type="submit" class="button red" value="<?php echo JText::_('COM_STTNMLS_SEARCH'); ?>"/><br><br>
			<input name="RST" class="button silver" onclick="document.getElementById('adminForm').target='_self'; document.getElementById('actfil').value=''; document.getElementById('close').checked=false;" type="submit" value="<?php echo JText::_('COM_STTNMLS_RESET'); ?>"/>
		</td>
		</tr>
		<tr>
			<td colspan="4">
			Поиск по телефону
			<input type="text" value="<?php echo $_REQUEST['phone'];?>" name="phone" id="phone_fil"/><br>
			Впишите несколько цифр телефона без пробелов и разделителей
			</td>
		</tr>
		</table>
		<?php
			$d1='';
			$d2=' style="display: none;"';
			if($this->params->get('dopfilter','0'))
			{
				$d2='';
				$d1=' style="display: none;"';
			}
		?>
		<div class="updopfltr" <?php echo $d2; ?> ><?php echo JText::_('COM_STTNMLS_UPDOPF'); ?></div>
		<div class="dopfilter" <?php echo $d2; ?> >
			<div class="cityselect linediv">
				<select name="filter_city" id="filter_city" class="inputbox" onchange="changecity(this);jQuery('input#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');">
					<?php echo JHtml::_('select.options', $this->cityopt, 'value', 'text', $this->state->get('filter.city'));?>
				</select>
			</div>
			<?php
				$street = $this->state->get('filter.street');
				if(!$street) $street = JText::_('COM_STTNMLS_STREET_SELECT');
			?>
			<div class="ui-widget linedivstreet">
				<input class="ui-widget-content" type="text" name="filter_street" id="T4" value="<?php echo $street; ?>" onblur="if(this.value.replace(/^\s+|\s+$/g, '')=='') this.value='<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>';" onfocus="if(this.value=='<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>') this.value='';" size="30"/>
			</div>
			<button class="butclr" onclick="jQuery('#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');return false;"></button>
			<div id="filter_street"></div>
			<div style="clear: both;"></div>
<!--			end Выбор города и улицы-->
			<div class="linediv" >
				<strong><?php echo JText::_('COM_STTNMLS_OKRUG'); ?></strong><br/>
				<div id="raion">
					<?php
					foreach($this->raions as $raion) { ?>
						<input type="checkbox" class="r_check" name="filter_my_check[]" value="<?php echo $raion->value; ?>" <?php if(array_search($raion->value, $filter_my_check)!==false) echo ' checked="checked" '; ?> onclick="clickraion();"/>
						<?php echo $raion->text; ?>
						<br/>
					<?php } ?>
				</div>
			</div>
			<div class="linediv" >
				<strong><?php echo JText::_('COM_STTNMLS_MRAIONS'); ?></strong><br/>
				<div id="mraion">
					<?php
					foreach($this->mraions as $mraion) { ?>
						<input type="checkbox" class="mr_check" name="filter_my_MRcheck[]" value="<?php echo $mraion->value; ?>" <?php if(array_search($mraion->value, $filter_my_MRcheck)!==false) echo ' checked="checked" '; ?>/>
						<?php echo $mraion->text; ?>
						<br/>
					<?php } ?>
				</div>
			</div>
			<?php if($this->moder) {?>
			<div class="linediv" style="width:190px;" >
				<input type="hidden" name="filter_sobst[]" value="null"/>
				<input type="hidden" name="filter_neprod[]" value="null"/>
				<?php
					$fsobst=$this->state->get('filter.sobst', array()); 
					$fneprod=$this->state->get('filter.neprod', array()); 
				?>
				<input type="checkbox" class="moderfiltr" name="filter_sobst[]" value="1" <?php if(count($fsobst)>1) echo ' checked="checked"';  ?> /><b><?php echo JText::_('COM_STTNMLS_FILTR_SOBST'); ?></b><br/>
				<input type="checkbox" class="moderfiltr" name="filter_neprod[]" value="1" <?php if(count($fneprod)>1) echo ' checked="checked"';  ?> /><b><?php echo JText::_('COM_STTNMLS_FILTR_NEPROD'); ?></b><br/>
                <?php $excluzive = JRequest::getInt('excluzive', 0);?>
				<input type="checkbox" class="moderfiltr" name="excluzive" value="1" <?php if($excluzive) echo ' checked="checked"';  ?> /><b>Эксклюзив</b><br/>
                <?php $close = JRequest::getInt('close', 0);?>
				<input type="checkbox" class="moderfiltr" name="close" id="close" value="1" <?php if($close) echo ' checked="checked"';  ?> /><b>Внутренная база</b><br/>
			</div>
			<?php }else{ ?>
            <?php
            $excluzive = JRequest::getInt('excluzive', 0);?>
			<div class="linediv" style="width:190px;">
            
            	<input type="checkbox" class="moderfiltr" name="excluzive" value="1" <?php if($excluzive) echo ' checked="checked"';  ?> /><b>Эксклюзив</b><br/>
                <?php $close = JRequest::getInt('close', 0);?>
				<input type="checkbox" class="moderfiltr" name="close" id="close" value="1" <?php if($close) echo ' checked="checked"';  ?> /><b>Внутренная база</b><br/>
            
            </div>
            <?php
			}
			?>
		</div>
		<div class="dndopfltr" <?php echo $d1; ?> ><?php echo JText::_('COM_STTNMLS_DNDOPF'); ?></div>
	</div>
