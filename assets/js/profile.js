jQuery(document).ready(function($) {
    $('[data-toggle="tooltip"], .hasTooltip').tooltip({html: true});

    $('#object-list-container-home').on('hover', '.hasTooltip', function(){
        $(this).tooltip({html: true});
    });
    
    // Выгрузка аватара
    //$('#agentphoto').fileupload({
    //    dataType: 'json',
    //    add: function(e, data) {
    //        $('.sttnmls_photo .upload_image .fileinput-button').addClass('disabled');
    //        $('.sttnmls_photo .upload_image').append('<div class="progress" style="margin-top:10px;"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: 0%;"></div></div>');
    //        data.submit();
    //    },
    //    progressall: function (e, data) {
    //        var progress = parseInt(data.loaded / data.total * 100, 10);
    //        $('.sttnmls_photo .upload_image .progress-bar').css(
    //            'width',
    //            progress + '%'
    //        );
    //    },
    //    done: function(e, data) {
    //        setTimeout(function(){
    //            $.each(data.result.errors, function(index, e){
    //            $('#system-message-container').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + e + '</div>');
    //            });
    //            $.each(data.result.files, function(index, file) {
    //                $('.sttnmls_photo .image').hide().html('<img class="img-thumbnail" src="' + file.url_path +  '?t' + Math.random() + '" alt="" />').fadeIn();
    //                $('.sttnmls_photo .upload_image div.progress').remove();
    //                $('.sttnmls_photo .upload_image .fileinput-button').removeClass('disabled');
    //            });
    //        }, 500);
    //    }
    //});
    
    $('#agentphoto').fileupload({
        dataType: 'json',
        add: function(e, data) {
            $('.sttnmls_photo .upload_image .fileinput-button').addClass('disabled');
            $('.sttnmls_photo .upload_image').append('<div class="progress" style="margin-top:10px;"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: 0%;"></div></div>');
            data.submit();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.sttnmls_photo .upload_image .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        done: function(e, data) {
            if(data.result.error) {
                alert(data.result.error);
            } else {
                var wrapper = $('#object-list-container-home');
                $.ajax({
                    url: 'index.php?option=com_sttnmls&view=agent&layout=profile_home_avatar_edit&format=raw',
                    type: 'post',
                    dataType: 'html',
                    data: {id: data.result.id},
                    success: function(response) {
                        wrapper.html(response);
                        $('#com_sttnmls_modal').modal();
                    }
                });
            }
            $('.sttnmls_photo .upload_image div.progress').remove();
            $('.sttnmls_photo .upload_image .fileinput-button').removeClass('disabled');
        }
    });

    $('.getObjectsList').bind('click', function(){
        var table = $(this).data('table');
        var stype = $(this).data('stype');
        var what = $(this).data('what');
        var f_expired = $(this).data('expired') || 0;
        var f_closed = $(this).data('closed') || 0;
        var f_realtors_and_open = $(this).data('realtors') || 0;
        var src_profile = $(this).data('src-profile') || 0;
        var sobstvennik = $(this).data('sobstvennik') || 0;
        var container = $(this).data('container') || 'ajax-container';
        //var screenWidth = screen.width;
        var listViewParametr = '0';
        console.log(screen.width);
        if(screen.width<700) listViewParametr = '1';
        $('#' + container).html("<img class=\"ajax_activity\" src=\"/components/com_sttnmls/assets/images/ajax_activity.gif\">");
        //console.log(table);
        $.ajax({
            type: "POST",
            url: "index.php?option=com_sttnmls&view=" + table + "&format=raw"+"&list=" + listViewParametr,
            data: {
                table: table,
                stype: stype,
                what: what,
                f_expired: f_expired,
                f_closed: f_closed,
                f_realtors_and_open: f_realtors_and_open,
                profile: src_profile,
                owners: sobstvennik
            }
        }).done(function(data){
            // Mark favorites
            var result = $(data);
            $(result).find('.addToFavorite').each(function(i){
                var tp = $(this).data("tp");
                var cid = $(this).data("cid");
                var cn = $(this).data("cn");
                var chk_favor = find_cookie("tp" + tp + "cid" + cid + "cn" + cn);
                if (chk_favor){
                    if($(this).data('toggle') === 'button') {
                        $(this, result).attr('aria-pressed', 'true');
                        $(this, result).addClass("active");
                    } else {
                        $(this, result).addClass("in_fav");
                    }
                }
            });
            $('#' + container).html(result);
        });
    });
    
    
    $('input.phone').inputmask({'mask': '\8 (999) 999-99-99'});
    
    $('.account_type_selector').on('click', function(){
        $('#jform_profile_realtor').val($(this).val());
        $('#account_type_image').removeClass().addClass($(this).data('img-class'));
    });
    
    
    // Удаление объекта
    $('#object-list-container-home, #object-list-container-sobstvenniki').on('click', '.removeObjectFromList', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');
        
        if(!confirm('Удалить объявление?')) {
            return false;
        }
        
        $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonRemoveObject&format=raw",
                type: "POST",
                data: {cn: cn, cid: cid, table: view},
                dataType: "JSON"
        }).done(function(response) {
                if(response.result) {
                    // TODO: Доделать
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                    $('#' + cn + '_' + cid).fadeOut().remove();
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
        });
    });
    
    // Изменение даты создания объекта
    $('#object-list-container-home, #object-list-container-sobstvenniki').on('click', '.changeObjectCreateDate', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');
        
        if(!confirm('Подтверждаете действие?')) {
            return false;
        }
        
        $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonChangeObjectCreateDate&format=raw",
                type: "POST",
                data: {cn: cn, cid: cid, table: view},
                dataType: "JSON"
        }).done(function( response ) {
                if(response.result) {
                    $('#' + cn + '_' + cid).removeClass('expired');
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
        });
    });

    // Изменение даты обновления объекта
    $('#object-list-container-home, #object-list-container-sobstvenniki').on('click', '.changeObjectChangeDate', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');
        
        if(!confirm('Подтверждаете действие?')) {
            return false;
        }
        
        $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonChangeObjectEditDate&format=raw",
                type: "POST",
                data: {cn: cn, compid: cid, table: view},
                dataType: "JSON"
        }).done(function( response ) {
            if(response.result) {
                $('#' + cn + '_' + cid).removeClass('expired');

                // TODO: Доделать
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                });
            }
        });
    });

    $('.changeExpiredObjectChangeDate').on('click', function(){
        var table = $(this).data('table');
        var stype = $(this).data('stype');
        var what = $(this).data('what');
        var compid = $(this).data('compid') || 0;
        var message= $(this).data('message') || 'Подтверждаете действие';

        if(!confirm(message + '?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=agent&task=json.changeExpiredObjectEditDate',
            type: 'post',
            dataType: 'json',
            data: {table:table, stype:stype, what:what, compid:compid}
        }).done(function(response){
            if(response.result) {
                window.location.reload();
            } else {
                $('#system-message-container').html();
                $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
                alert(response.message);
            }
        });

    });

    $('.changeExpiredObjectChangeDateAll').on('click', function(){
        var message= $(this).data('message') || 'Подтверждаете действие';

        if(!confirm(message + '?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=agent&task=json.changeExpiredObjectEditDateAll',
            type: 'post',
            dataType: 'json'
        }).done(function(response){
            if(response.result) {
                window.location.reload();
            } else {
                $('#system-message-container').html();
                $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
                alert(response.message);
            }
        });

    });
    
    $('.doServiceOff').on('click', function(){
        var uid = $(this).data('uid');
        
        if(!confirm('Подтверждаете действие?')) {
            return false;
        }
        
        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonDoServiceOffByID&format=raw",
            type: "POST",
            data: {uid: uid},
            dataType: "JSON"
        }).done(function( response ) {
            if(response.result) {
                    $('#u_' + uid).fadeOut().remove();
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        alert(message);
                        $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                } else {
                    $('#system-message-container').html();
                    response.messages.each(function(message){
                        alert(message);
                        $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    });
                }
        });
    });

});

//подготовка формы добавления нового объекта
function checkProfileHomeForm(formEl){
    
    var selectedOption = formEl.view.options[formEl.view.options.selectedIndex];
    var selectedOptionValue = selectedOption.getAttribute('value');
    var selectedOptionWhat = selectedOption.getAttribute('what');
    
    //определяем: квартира или комната, дом или участок по выбранной категории объекта
    if (selectedOptionValue == 'apart' || selectedOptionValue == 'house'){
        if (selectedOptionWhat !== undefined){
            if (formEl.what){
                formEl.what.value = selectedOptionWhat;
            } else return false;
        } else return false;
    }
    
    //определяем адрес отправки формы по выбранной категории объекта
    switch (selectedOptionValue){
        case ('apart'): formEl.action = '/kvartira.html'; break;
        case ('house'): formEl.action = '/dom.html';break;
        case ('garage'): formEl.action = '/garage.html'; break;
        case ('com'): formEl.action = '/com.html'; break;
        default: return false;
    }
    
}