jQuery(document).ready(function($) {

    // Counters update function
    var updateFavoriteCounters = function () {
        var arrSplit = document.cookie.split(';');
        var tp1 = 0;
        var tp3 = 0;
        var tp4 = 0;
        var tp5 = 0;

        for (var i = 0; i < arrSplit.length; i++) {
            var cookie = arrSplit[i].trim();
            var cookieName = cookie.split('=')[1] || '';

            if (~cookieName.indexOf('tp1')) {
                tp1++;
            }
            if (~cookieName.indexOf('tp3')) {
                tp3++;
            }
            if (~cookieName.indexOf('tp4')) {
                tp4++;
            }
            if (~cookieName.indexOf('tp5')) {
                tp5++;
            }
        }

        var total = tp1 + tp3 + tp4 + tp5;

        $('.countFavorTp1').html(tp1);
        $('.countFavorTp3').html(tp3);
        $('.countFavorTp4').html(tp4);
        $('.countFavorTp5').html(tp5);
        $('.countFavor').html(total);
    }

    var addToFavorites = function(object) {
        var tp = object.data("tp");
        var type = object.data("type");
        var cid = object.data("cid");
        var cn = object.data("cn");
        var chk_favor = find_cookie("tp" + tp + "cid" + cid + "cn" + cn);
        var expire_date = new Date();
        expire_date.setMonth(expire_date.getMonth() +6);
        var fav_nums = get_cookie("fav_nums");

        if (chk_favor) {
            if (fav_nums == null||fav_nums == NaN||fav_nums < 0) {
                fav_nums = 1;
            } else {
                fav_nums = parseInt(fav_nums) - 1;
            }
            remove_cookie("cardids" + cid + '-' + cn);
            object.removeClass("in_fav");
        } else {
            if (fav_nums == null||fav_nums == NaN||fav_nums < 0) {
                fav_nums = 1;
            } else {
                fav_nums = parseInt(fav_nums) + 1;
            }
            document.cookie = "cardids" + cid + '-' + cn + "=tp" + tp + "cid" + cid + "cn" + cn +";expires=" + expire_date.toGMTString() +"; path=/;";
            object.addClass("in_fav");
        }
        document.cookie = "fav_nums=" + fav_nums + ";expires=" + expire_date.toGMTString() +"; path=/;";
    }

    var markFavorites = function(object) {
        var tp = object.data("tp");
        var cid = object.data("cid");
        var cn = object.data("cn");
        var chk_favor = find_cookie("tp" + tp + "cid" + cid + "cn" + cn);
        if (chk_favor){
            if(object.data('toggle') === 'button') {
                object.attr('aria-pressed', 'true');
                object.addClass("in_fav");
            } else {
                object.removeClass("in_fav");
            }
        }
    }

    updateFavoriteCounters();

    // Adding favorites items
    $('.favorites, .object-card-wrapper, #object-list-container-home, #object-list-container-sobstvenniki').on('click', '.addToFavorite', function() {
        console.log(this);
        addToFavorites($(this));
        updateFavoriteCounters();
    });

    $('.addToFavorite').each(function() {
        markFavorites($(this));
    });

    $('#favorites').on('click', 'a.removeAllFromFavorites', function() {
        var arrSplit = document.cookie.split(';');

        for(var i = 0; i < arrSplit.length; i++) {
            var cookie = arrSplit[i].trim();
            var cookieName = cookie.split('=')[0];

            if(cookieName.indexOf('cardids') === 0) {
                document.cookie = cookieName + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
            }
        }
        remove_cookie('fav_nums');
        remove_cookie('fav_count');

        location.reload();
    });

    $('#favorites').on('click', 'a.removeObjectFromFavorites', function() {
        var expire_date = new Date();
        var cookieName = $(this).data('cookie-name');
        var fav_nums = parseInt(get_cookie("fav_nums"));

        if(cookieName) {
            remove_cookie(cookieName);
            $(this).closest('tbody').remove();


            if (fav_nums == null||fav_nums == NaN||fav_nums < 0) {
                fav_nums = 0;
            } else {
                fav_nums = parseInt(fav_nums) - 1;
            }

            document.cookie = "fav_nums=" + fav_nums + ";expires=" + expire_date.toGMTString() +"; path=/;";
        }
        updateFavoriteCounters();
    });
});

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function find_cookie(value){
    if (document.cookie.search(value) != -1){
        return true;
    } else{
        return false;
    }
}

function get_cookie(name){
    cookie_name = name + "=";
    cookie_length = document.cookie.length;
    cookie_begin = 0;
    while (cookie_begin < cookie_length){
        value_begin = cookie_begin + cookie_name.length;
        if (document.cookie.substring(cookie_begin, value_begin) == cookie_name){
            var value_end = document.cookie.indexOf (";", value_begin);
            if (value_end == -1){
                value_end = cookie_length;
            }
            return unescape(document.cookie.substring(value_begin, value_end));
        }
        cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
        if (cookie_begin == 0){
            break;
        }
    }
    return null;
}

function remove_cookie(name) {
    if(find_cookie(name)){
        document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/;';
    }
}