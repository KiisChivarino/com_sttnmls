jQuery(document).ready(function($) {
    $('[data-toggle="tooltip"], .hasTooltip').tooltip({html: true});


    $('.showDopFilters').click(function(){
        $('.dop_filters').slideToggle('fast');
        $(this).toggleClass('showed');
        if($(this).hasClass('showed')) {
            $(this).html($(this).data('closed-text'));
        } else {
            $(this).html($(this).data('showed-text'));
        }
    });
    $('.clearStreetFilter').click(function(){
        $(this).closest('.input-group').find('input[name="filter_street"]').val('');
    });

    $('.view-type-select label').on('click', function() {
        var view_type = $(this).find('input').val() || 0;

        document.cookie = "viewType=" + view_type + ";expires=0" + "; path=/;";
        location.reload();
    });
    
    //для группировки фильтров в выпадающие списки
    $('.filterGroupSelect').on('change', function() {
        var selectedOption = this.options[this.selectedIndex];
        this.setAttribute('name', selectedOption.getAttribute('name'));
    });


    $('#streetFilter').typeahead({
        items: 10,
        minLength: 2,
        source: function(q, process){
            var streets = new Array;
            $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonStreets&format=json",
                type: "POST",
                data: $("form#adminForm").serialize(),
                dataType: "JSON",
                async: false,
                success: function(results){
                    results.each(function(data){
                        var group;
                        group = {
                            id: data.id,
                            value: data.value,
                            toString: function(){
                                return JSON.stringify(this);
                            },
                            toLowerCase: function() {
                                return this.value.toLowerCase();
                            },
                            indexOf: function() {
                                return String.prototype.indexOf.apply(this.value, arguments);
                            },
                            substr: function() {
                                return String.prototype.substr.apply(this.value, arguments);
                            },
                            replace: function (string) {
                                var value = '';
                                value +=  this.value;
                                if(typeof(this.id) != 'undefined') {
                                    value += ' <span style="font-size: 0.8em;" class="muted">[ID:';
                                    value += this.id;
                                    value += ']</span>';
                                }
                                return String.prototype.replace.apply('<div>' + value + '</div>', arguments);
                            }
                        };
                        streets.push(group);
                    });
                    process(streets);
                }
            });
        },
        updater: function(item) {
            var item = JSON.parse(item);
            return item.value;
        }
    });
    $("#raion:input").click(clickraion);

    // Resizing images
    if( $('.object-card-wrapper > article > .col-md-4').length > 0 ) {
        $('.object-card-wrapper > article > .col-md-4').realtyPhotoAutoWidth();
    }

    if( $('.object-card-wrapper .col-sttnmls-photo').length > 0 ) {
        $('.object-card-wrapper .col-sttnmls-photo').realtyPhotoAutoWidth();
    }

    if( $('.fbox').length > 0 ) {
        $('.fbox').fancybox({
            margin: 0,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'titlePosition': 'over',
            'titleFormat': function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    }


    $('.showComplaintForm').click(function(){
        $('#complaint_form').slideToggle();
    });


    // Удаление объекта
    $('.removeObjectFromList').on('click', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        if(!confirm('Удалить объявление?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonRemoveObject&format=raw",
            type: "POST",
            data: {cn: cn, cid: cid, table: view},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                // TODO: Доделать
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
                $('#' + cn + '_' + cid).fadeOut().remove();
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.repairObjectFromArchive').on('click', function(){
        var oid = $(this).data('oid');
        var view = $(this).data('view');
        var confirm_message = $(this).data('confirm-message');

        if(!confirm(confirm_message)) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=json.repairObjectFromArchive",
            type: "POST",
            data: {oid: oid, view: view},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                // TODO: Доделать
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
                window.location.reload();
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.removeObjectFromArchive').on('click', function(){
        var oid = $(this).data('oid');
        var view = $(this).data('view');
        var confirm_message = $(this).data('confirm-message');

        if(!confirm(confirm_message)) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=json.removeObjectFromArchive",
            type: "POST",
            data: {oid: oid, view: view},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                // TODO: Доделать
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
                window.location.reload();
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    // Изменение даты создания объекта
    $('.changeObjectCreateDate').on('click', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        if(!confirm('Подтверждаете действие?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonChangeObjectCreateDate&format=raw",
            type: "POST",
            data: {cn: cn, cid: cid, table: view},
            dataType: "JSON"
        }).done(function( response ) {
            if(response.result) {
                $('#' + cn + '_' + cid).removeClass('expired');
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
                window.location.reload();
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }

        });
    });

    // Изменение даты обновления объекта
    $('.changeObjectChangeDate').on('click', function(){
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        if(!confirm('Подтверждаете действие?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonChangeObjectEditDate&format=raw",
            type: "POST",
            data: {cn: cn, compid: cid, table: view},
            dataType: "JSON"
        }).done(function( response ) {
            if(response.result) {
                $('#' + cn + '_' + cid).removeClass('expired');
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.changeExpiredObjectChangeDate').on('click', function(){
        var table = $(this).data('table');
        var stype = $(this).data('stype');
        var what = $(this).data('what') || 0;
        var compid = $(this).data('compid') || 0;
        var message= $(this).data('message') || 'Подтверждаете действие';

        if(!confirm(message + '?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=agent&task=json.changeExpiredObjectEditDate',
            type: 'post',
            dataType: 'json',
            data: {table:table, stype:stype, what:what, compid:compid}
        }).done(function(response){
            if(response.result) {
                window.location.reload();
            } else {
                $('#system-message-container').html();
                $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
                alert(response.message);
            }
        });

    });


    $('#objectSendModeratorInfo').click(function(){
        var q1 = $('#q1').val();
        var q2 = $('#q2').val();
        var typeobj = $(this).data('otype');
        var cn = $(this).data('cn');
        var cid = $(this).data('cid');
        var view = $(this).data('view');

        if(q1 != 0 || q2 != 0) {
            $('#moderform').fadeOut('slow').remove();
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&task=jsonDoObjectModerate&format=raw',
            type: "POST",
            data: {view: view, typeobj: typeobj, cardnum: cn, compid: cid, q1: q1, q2: q2},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.account_type_selector').on('click', function(){

        if($(this).hasClass('showExtendFields')) {
            $('#extendFields').removeClass('hide').find('input[type="password"]').addClass('required');
        } else {
            $('#extendFields').addClass('hide').find('input[type="password"]').removeClass('required');
        }

        $('#jform_profile_type').val($(this).val());
        $('#account_type_image').removeClass().addClass($(this).data('img-class'));
    });


    $('#searchFirmInput').typeahead({
        items: 10,
        minLength: 2,
        source: function(q, process){
            var firms = new Array;
            $.ajax({
                url: ROOT_URI + "index.php?option=com_sttnmls&task=jsonFirms&format=json",
                type: "POST",
                data: $("form#filterForm").serialize(),
                dataType: "JSON",
                async: false,
                success: function(results){
                    results.each(function(data){
                        var group;
                        group = {
                            id: data.id,
                            value: data.value,
                            toString: function(){
                                return JSON.stringify(this);
                            },
                            toLowerCase: function() {
                                return this.value.toLowerCase();
                            },
                            indexOf: function() {
                                return String.prototype.indexOf.apply(this.value, arguments);
                            },
                            substr: function() {
                                return String.prototype.substr.apply(this.value, arguments);
                            },
                            replace: function (string) {
                                var value = '';
                                value +=  this.value;
                                if(typeof(this.id) != 'undefined') {
                                    value += ' <span style="font-size: 0.8em;" class="muted">[ID:';
                                    value += this.id;
                                    value += ']</span>';
                                }
                                return String.prototype.replace.apply('<div>' + value + '</div>', arguments);
                            }
                        };
                        firms.push(group);
                    });
                    process(firms);
                }
            });
        },
        updater: function(item) {
            var item = JSON.parse(item);
            return item.value;
        }
    });

    // Изменение должности босса
    $('.com_sttnmls').on('click', '.changeAgentPost', function() {
        var compid = $(this).data('compid');
        var agent_id = $(this).data('agent-id');
        var new_post_title = $('.changeAgentPostInput' + compid + '-' + agent_id).val();

        $('.changeAgentPostContainer' + compid + '-' + agent_id).modal('hide');

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=firm&agency=' + compid + '&task=jsonChangeAgentPost',
            type: "POST",
            data: {compid: compid, agent_id: agent_id, post_title: new_post_title},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                $('.agent_post' + compid + '-' + agent_id).html(new_post_title);

                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.com_sttnmls').on('click', '.changeAgentPassword', function() {
        var compid = $(this).data('compid');
        var agent_id = $(this).data('agent-id');
        var user_id = $(this).data('userid');
        var new_password = $('.changeAgentPasswordInput' + compid + '-' + agent_id).val();

        $('.changeAgentPasswordContainer' + compid + '-' + agent_id).modal('hide');

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=firm&agency=' + compid + '&task=jsonChangeAgentPassword',
            type: "POST",
            data: {agent_id: agent_id, user_id: user_id, password: new_password},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.com_sttnmls').on('click', '.confirmAgent', function() {
        var el = $(this);
        var compid = $(this).data('compid');
        var agent_id = $(this).data('agent-id');

        if(!confirm('Вы действительно хотите подтвердить агента?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&view=firm&agency=' + compid + '&task=jsonConfirmAgent',
            type: "POST",
            data: {compid: compid, agent_id: agent_id},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                el.closest('li').remove();
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });


    $('.com_sttnmls').on('click', '.removeAgent', function(){
        var el = $(this);
        var compid = $(this).data('compid');
        var agent_id = $(this).data('agent-id');

        if(!confirm('Вы действительно хотите удалить агента?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&task=jsonRemoveAgent',
            type: "POST",
            data: {compid: compid, agent_id: agent_id},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                el.closest('li').remove();
                $('#row' + compid + '-' + agent_id).fadeOut('slow').remove();

                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });
    });

    $('.com_sttnmls').on('click', '.setObjectValidateState', function(){
        var el = $(this);
        var view = el.data('view') || 'apart';
        var cid = el.data('cid');
        var cn = el.data('cn');
        var current_state = el.data('state');

        if(!confirm('Вы действительно хотите изменить состояние объекта?')) {
            return false;
        }

        $.ajax({
            url: ROOT_URI + 'index.php?option=com_sttnmls&task=json.setObjectCheckedState',
            type: "POST",
            data: {view: view, cn: cn, cid: cid, state: current_state},
            dataType: "JSON"
        }).done(function(response) {
            if(response.result) {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                });

                $('#object_state_container').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
                $.ajax({
                    url: ROOT_URI + "index.php?option=com_sttnmls&view=" + view + "&layout=default_object_check_state_button&cn=" + cn + "&cid=" + cid + "&format=raw",
                    type: "GET",
                    data: {},
                    dataType: "HTML"
                }).done(function(html){
                    $('#object_state_container').html(html);
                });
            } else {
                $('#system-message-container').html();
                response.messages.each(function(message){
                    $('#system-message-container').append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>');
                    alert(message);
                });
            }
        });

    });

    var isMobile = function(e){
        return e.width() < 768 ? true : false;
    };

    isMobile = isMobile.bind(this, $(window));

    $(window).scroll(function() {
        if(!isMobile()){
            if(isIntoView('.sttnmls_contact_info')) {
                $('.fixed-contacts').stop(true, false).fadeOut('fast');
            } else {
                if($('.fixed-contacts').is(':hidden')) {
                    $('.fixed-contacts').css({
                        opacity : 1
                    }).fadeIn('fast');
                }
            }
        }
    });

    $('.object-address-link').on('click', function(e){
        e.preventDefault();
        console.log(this);
        var target = $(this).attr('href');
        var offset = $(target).offset().top;
        console.log(offset);
        $('html, body').animate({scrollTop: offset - 200}, 500);
    })


    $('#filter_catid').on('change', function(){
        var cat_id = $(this).val();

        $('#firms_container').html('<img src="' + ROOT_URI + '/components/com_sttnmls/assets/images/ajax_activity.gif" alt="ajax" />');
        $.ajax({
            url: ROOT_URI + "index.php?option=com_sttnmls&view=agents&layout=default_select_compid&format=raw",
            type: "GET",
            data: {"filter_catid": cat_id},
            dataType: "HTML"
        }).done(function(html){
            $('#firms_container').html(html);
        });
    });



    $('.showModerateContacts').on('click', function(){
        $('#dopinfo, #contacts').toggleClass('hide');
        if($('#dopinfo').is(':visible')) {
            $(this).html('Скрыть контакты');
        } else {
            $(this).html('Показать контакты');
        }
    });

    $('.checkbox-list').each(function(i, e){
        e = $(e);
        var target = e.data('target');
        var callback = target ? applyContext(e, target) : false;
        callback && e.on('click', callback);
    })
});

function switchCheckboxList(target){
    target.hasClass('show') && target.removeClass('show') || target.addClass('show');
    console.log(this, target);
}

function applyContext(context, target){
    return switchCheckboxList.bind(context, $(target))
}

function isIntoView(elem) {
    if(!jQuery(elem).length) return false; // element not found

    var docViewTop = jQuery(window).scrollTop();
    var docViewBottom = docViewTop + jQuery(window).height();

    var elemTop = jQuery(elem).offset().top;
    var elemBottom = elemTop + jQuery(elem).height();

    return ((elemBottom = docViewTop));
}

function str_replace ( search, replace, subject ) {

    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }

    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){
        replace[replace.length]='';
    }

    if(subject instanceof Array){
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }

    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }

    return subject;

}



function explode( delimiter, string ) {
    var emptyArray = { 0: '' };
    if ( arguments.length != 2
        || typeof arguments[0] == 'undefined'
        || typeof arguments[1] == 'undefined' )
    {
        return null;
    }
    if ( delimiter === ''
        || delimiter === false
        || delimiter === null )
    {
        return false;
    }
    if ( typeof delimiter == 'function'
        || typeof delimiter == 'object'
        || typeof string == 'function'
        || typeof string == 'object' )
    {
        return emptyArray;
    }
    if ( delimiter === true ) {
        delimiter = '1';
    }
    return string.toString().split ( delimiter.toString() );
}


function clickraion() {
    var r_checked = [];
    jQuery(".r_check:checked").each(function(index){
        r_checked[index] = jQuery(this).val();
    });
    r_checked = r_checked.join(',');

    var mr_checked = [];
    jQuery(".mr_check:checked").each(function(index){
        mr_checked[index] = jQuery(this).val();
    });
    mr_checked = mr_checked.join(',');
    var cityid = jQuery("#filter_city").val();
    load_mraion(r_checked, "mraion", mr_checked, cityid);
}
function changecity(s)
{
    var r_checked = [];
    jQuery(".r_check:checked").each(function(index){
        r_checked[index] = jQuery(this).val();
    });
    r_checked = r_checked.join(',');
    load_raion(s.value, "raion", r_checked);
}

function load_mraion(rid, name, mr_checked, cityid){
    jQuery("#mraion").html("<img class=\'ajax_activity\' src=\'/components/com_sttnmls/assets/images/ajax_activity.gif\'>");
    jQuery.ajax({
        type: "POST",
        url: ROOT_URI + "index.php?option=com_sttnmls&task=ajax_mr&format=raw",
        data: {mr_checked: mr_checked, rid: rid, city: cityid}
    }).done(function( data ) {
        jQuery("#"+name).html(data);
    });
}
function load_raion(cityid, name, r_checked){
    jQuery("#raion").html("<img class=\'ajax_activity\' src=\'/components/com_sttnmls/assets/images/ajax_activity.gif\'>");
    jQuery.ajax({
        type: "POST",
        url: ROOT_URI + "index.php?option=com_sttnmls&task=ajax_r&format=raw",
        data: {r_checked: r_checked, city: cityid}
    }).done(function( data ) {
        jQuery("#"+name).html(data);
        clickraion();
    });
}