<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

$compidsob = $params->get('compidsob', '0');
$mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
$currentAgent = $mAgent->getAgentInfoByID();

// Vars from request
$agency = $app->input->getUint('agency', 0);
$agent = $app->input->getUint('agent', 0);
$stype = $app->input->getUint('stype', 0);
$city_id = $app->input->getUint('city_id', 0);
$raion_id = $app->input->getUint('raion_id', 0);
$mraion_id = $app->input->getUint('mraion_id', 0);

$filter_gtypes = $this->state->get('filter.gtypes', array());
$city = (($city_id) ? $city_id : $this->state->get('filter.city', 0));
$filter_raions = (($raion_id) ? array($raion_id) : $this->state->get('filter.raions', array()));
$filter_mraions = (($mraion_id) ? array($mraion_id) : $this->state->get('filter.mraions', array()));
$filter_price_ot = $this->state->get('filter.price_ot', 0);
$filter_price_do = $this->state->get('filter.price_do', 0);
$filter_view_type = $this->state->get('filter.view_type', 0);

?>
    <input type="hidden" name="act" value="" id="actfil"/>
    <input type="hidden" name="typepdf" value="" id="typepdf"/>
    <input type="hidden" name="stype" value="<?php echo $stype; ?>" />
    <input type="hidden" name="UPD" value="" id="newsearch" />
    <input type="hidden" name="RST" value="" id="resetform" />

    <input type="hidden" name="filter_gtypes[]" value="null"/>
    <input type="hidden" name="filter_raions[]" value="null"/>
    <input type="hidden" name="filter_mraions[]" value="null"/>
    <input type="hidden" name="filter_exclusive" value="0" />
    <input type="hidden" name="filter_internal_base" value="0" />
    <input type="hidden" name="filter_realtors_base" value="0" />
    <input type="hidden" name="filter_realtors_and_open" value="0" />
    <input type="hidden" name="filter_price_day" value="0" />
    <input type="hidden" name="filter_price_hour" value="0" />
    <input type="hidden" name="filter_archive_mode" value="0" />

    <input type="hidden" id="city_field" name="city" value="<?php echo $city; ?>" />

<?php if($this->access->isAdmin OR $this->access->isModerator) : ?>
    <input type="hidden" name="filter_expired" value="0" />
    <input type="hidden" name="filter_owner" value="0" />
<?php endif; ?>

    <ul class="nav nav-tabs">
        <li role="presentation"<?php echo ((!$this->stype) ? ' class="active"' : '') ?>>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : ''), 'default', array('stype' => 0)) ?>">
                <span class="glyphicon glyphicon-rub"></span>
                <?php echo JText::_('COM_STTNMLS_SELL'); ?>
            </a>
        </li>
        <li role="presentation"<?php echo (($this->stype) ? ' class="active"' : '') ?>>
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'garages', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : ''), 'default', array('stype' => 1)) ?>">
                <span class="glyphicon glyphicon-calendar"></span>
                <?php echo JText::_('COM_STTNMLS_RENT'); ?>
            </a>
        </li>
        <li role="presentation">
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'favorites')?>" class="favorite-link">
                <span class="glyphicon glyphicon-star"></span><span class="countFavor">0</span>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-content-wrapper">
            <!-- ROW1 -->
            <div class="row">
                <!-- SECTION: Garage type -->
                <div class="col-md-3">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_GTYPE'); ?></div>
                    <?php foreach($this->types as $type) : ?>
			            <div class="checkbox">
                            <label>
                                <input type="checkbox" name="filter_gtypes[]" value="<?php echo $type->value; ?>"<?php if(array_search($type->value, $filter_gtypes) !== FALSE) echo ' checked="checked" '; ?> >
                                <?php echo $type->text; ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- //SECTION: Garage type -->
                
                <!-- SECTION: Price -->
                <div class="col-md-6">
                    <div class="col-header text-center"><?php echo JText::_('COM_STTNMLS_LABEL_GARAGE_COST'); ?></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="text-left control-label col-xm-4 col-xs-12 col-md-4 col-sm-4"><?php echo JText::_('COM_STTNMLS_PRICEOT'); ?></label>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_ot" class="form-control input-sm sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $array_price = explode(",",$params->get('price_garages_sell',''));
                                    }else{
                                        $array_price = explode(",",$params->get('price_garages_arenda',''));
                                    }
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_ot == $one_price && $filter_price_ot != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_do" class="form-control input-sm sm-options">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_do == $one_price && $filter_price_do != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //SECTION: Price -->
                
                <div class="col-md-3">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_EXT_FILTER'); ?></div>
                    <div class="form-horizontal">
                    <?php if($this->access->isLogin): ?>
                        <div class="form-group">
                            <!--Группировка фильтров: БАЗА-->
                            <?php
                                $filter_realtors_and_open = $this->state->get('filter.realtors_and_open', FALSE);
                                $filter_realtors_base = $this->state->get('filter.realtors_base', FALSE);
                                $filter_internal_base = $this->state->get('filter.internal_base', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">База</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                name="<?php if(($currentAgent->COMPID != $compidsob)) {
                                        echo (($filter_realtors_and_open or $_GET['f_realtors_and_open']=='1') ? 'filter_realtors_and_open' : '');
                                        echo (($filter_realtors_base or $_GET['f_realtors']=='1') ? 'filter_realtors_base' : '');
                                    }else{
                                        echo (($filter_internal_base) ? 'filter_internal_base' : '');
                                   }
                                ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>
                                <?php if(($currentAgent->COMPID != $compidsob)) { ?>

                                    <!--База агентов+открытая база-->
                                    <option name="filter_realtors_and_open" value="1"
                                        <?php echo (($filter_realtors_and_open or $_GET['f_realtors_and_open']=='1') ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE_AND_OPEN_BASE') ?>
                                    </option>

                                    <!--База агентов-->
                                    <option name="filter_realtors_base" value="1" 
                                        <?php echo (($filter_realtors_base or $_GET['f_realtors']=='1') ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_FILTER_FIELD_REALTORS_BASE') ?>
                                    </option>
                                <?php }?>

                                    <!--Закрытая база-->
                                    <option name="filter_internal_base" value="1"
                                        <?php echo (($filter_internal_base) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_INTERNAL_DATABASE') ?>
                                    </option>

                                </select>
                            </div>
                        </div>
                    <?php endif;?>                   
                    <?php if ($this->access->isLogin){ ?>
                        <div class="form-group">
                            <!--Группировка фильтров: СТАТУС-->
                            <?php
                                $filter_expired = $this->state->get('filter.expired', FALSE);
                                $filter_archive_mode = $this->state->get('filter.archive_mode', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">Статус</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                    name="<?php
                                        if($this->access->seeExpiredObjects){
                                            echo (($filter_expired) ? 'filter_expired' : '');
                                        } else {
                                            echo (($filter_archive_mode) ? 'filter_archive_mode' : '');
                                        }
                                    ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>

                                <?php if($this->access->seeExpiredObjects){ ?>
                                    <!--Непродленные-->
                                    <option name="filter_expired" value="1" 
                                        <?php echo (($filter_expired) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_FILTR_NEPROD') ?>
                                    </option>
                                <?php }?>

                                    <!--Архив-->
                                    <option name="filter_archive_mode" value="1"
                                        <?php echo (($filter_archive_mode) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_ARCHIVE_MODE') ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    <?php }?>
                        <div class="form-group">
                            <!--Группировка фильтров: ОСОБЫЕ-->
                            <?php
                                $filter_owner = $this->state->get('filter.owner', FALSE);
                                $filter_exclusive = $this->state->get('filter.exclusive', FALSE);
                            ?>
                            <label class="text-left control-label col-xm-3 col-xs-12 col-md-3 col-sm-3">Особые</label>
                            <div class="col-xs-12 col-xm-9 col-md-9 col-sm-9">
                                <select class="moderfiltr form-control input-sm filterGroupSelect" 
                                    name="<?php
                                        if($this->access->seeContactsObjects){
                                            echo (($filter_owner) ? 'filter_owner' : '');
                                        }else{
                                            echo (($filter_exclusive) ? 'filter_exclusive' : '');
                                        }
                                    ?>" >

                                    <option name=""> <?php echo JText::_('COM_STTNMLS_HTYPE0') ?> </option>

                                <?php if($this->access->seeContactsObjects){ ?>
                                    <!--Собственники-->
                                    <option name="filter_owner" value="1" 
                                        <?php echo (($filter_owner) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_FILTR_SOBST') ?>
                                    </option>
                                <?php }?>

                                    <!--Эксклюзив-->
                                    <option name="filter_exclusive" value="1"
                                        <?php echo (($filter_exclusive) ? ' selected ' : '') ?> >
                                        <?php echo JText::_('COM_STTNMLS_LABEL_EXCLUSIVE') ?>
                                    </option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /ROW1 -->

            <hr />
            <a href="javascript:void(0);" class="showDopFilters<?php echo (($this->params->get('dopfilter', 0)) ? ' showed' : '') ?>" data-showed-text="<?php echo JText::_('COM_STTNMLS_DNDOPF'); ?>" data-closed-text="<?php echo JText::_('COM_STTNMLS_UPDOPF'); ?>"><?php echo JText::_('COM_STTNMLS_UPDOPF'); ?></a>

            <!-- SECTION: Additional filters -->
            <div class="dop_filters"<?php echo (($this->params->get('dopfilter', 0)) ? ' style="display: block;"' : '') ?>>
                <div class="row">
                    <!-- Select REGION or CITY -->
                    <div class="col-md-4">
                        <div class="cityselect">
                            <select name="filter_city" id="filter_city" class="form-control input-sm" onchange="changecity(this);jQuery('input#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');">
                                <?php echo JHtml::_('select.options', $this->cityopt, 'value', 'text', $city);?>
                            </select>
                        </div>
                    </div>
                    <!-- /Select REGION or CITY -->

                    <!-- Select STREET -->
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="streetFilter" name="filter_street" value="<?php echo $this->state->get('filter.street') ?>" placeholder="<?php echo JText::_('COM_STTNMLS_STREET_SELECT') ?>" autocomplete="false" />
                            <span class="input-group-btn">
                                <a href="javascript:void(0);" class="btn btn-default btn-sm clearStreetFilter"><b>x</b></a>
                            </span>
                        </div>
                    </div>
                    <!-- /Select STREET -->

                    <!-- Search PHONE -->
                    <div class="col-md-4">
                        <input type="text" class="form-control input-sm" value="<?php echo $this->state->get('filter.phone') ?>" name="filter_phone" id="phone_fil" placeholder="Поиск по телефону" />
                    </div>
                    <!-- /Search PHONE -->
                </div>

                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-4 col-sm-6 checkbox-list-container">
                        <button id="regionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="raion">
                            <?php echo JText::_('COM_STTNMLS_OKRUG'); ?>
                            <span class="caret"></span>
                        </button>
                        <div id="raion" class="dropdown-menu dropdown-menu-checkbox">
                            <?php foreach($this->raions as $raion) : ?>
                                <div>
                                    <label>
                                        <input type="checkbox" class="r_check" name="filter_raions[]" value="<?php echo $raion->value; ?>" <?php if(array_search($raion->value, $filter_raions)!==false) echo ' checked="checked" '; ?> onclick="clickraion();"/>
                                        <?php echo $raion->text; ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 checkbox-list-container">
                        <button id="mraionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="mraion">
                            <?php echo JText::_('COM_STTNMLS_MRAIONS'); ?>
                            <span class="caret"></span>
                        </button>
                        <div id="mraion" class="dropdown-menu dropdown-menu-checkbox">
                            <?php foreach($this->mraions as $mraion) : ?>
                                <div>
                                    <label>
                                        <input type="checkbox" class="mr_check" name="filter_mraions[]" value="<?php echo $mraion->value; ?>" <?php if(array_search($mraion->value, $filter_mraions)!==false) echo ' checked="checked" '; ?>/>
                                        <?php echo $mraion->text; ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-4 groupbox">
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-push-3 col-md-offset-3 col-sm-6 col-sm-push-6 col-xs-12">
                    <button type="submit" class="btn btn-warning btn-block to-right" onclick="document.getElementById('actfil').value='';document.getElementById('newsearch').value='search';">
                        <?php echo JText::_('COM_STTNMLS_SEARCH'); ?>
                    </button>
                </div>
                <div class="col-md-3 col-md-pull-3 col-sm-6 col-sm-pull-6 col-xs-12">
                    <button type="submit" class="btn btn-link btn-block to-left" onclick="document.getElementById('adminForm').target='_self'; document.getElementById('resetform').value='1'; document.getElementById('actfil').value=''; document.getElementById('filter_easy').value=0; document.getElementById('phone_fil').value=''; document.getElementById('close').checked=false;">
                        <?php echo JText::_('COM_STTNMLS_RESET'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="active-filters">
        <?php echo $this->loadTemplate('filters_list'); ?>
    </div>
