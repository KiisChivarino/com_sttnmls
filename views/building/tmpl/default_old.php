<?php
defined('_JEXEC') or die('Restricted access');
if(!$this->item) {
	echo $this->loadTemplate('404');
} else {
JHtml::_('behavior.framework');
JHTML::_('behavior.modal');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js");
$doc->addScript("http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU");
$script = ' 
	var typeobj = 1;
	var cardnum="'.$this->item->CARDNUM.'";
	var compid="'.$this->item->COMPID.'";	
	var g_u = "'.$this->item->gorod.', '.$this->item->ulica.'";';
if($this->item->gorod == SttNmlsLocal::TText('COM_STTNMLS_OBL'))
	$script .= ' var _zoom = "13";';
else
	$script .= ' var _zoom = "15";';
$doc->addScriptDeclaration($script);
$doc->addScript("components/com_sttnmls/assets/js/ymaps.js");
$doc->addScript("components/com_sttnmls/assets/js/viewob.js");
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
$pics = (trim($this->item->PICTURES) != '') ? explode(';',$this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';',$this->item->PICNAMES) : '';


$pics2 = (trim($this->item->PICTURES2) != '') ? explode(';',$this->item->PICTURES2) : '';
$picnames2 = (trim($this->item->PICNAMES2) != '') ? explode(';',$this->item->PICNAMES2) : '';


$params = JComponentHelper::getParams('com_sttnmls');
$socbut=$params->get('socbut', '');
$region=$params->get('region', 'Курская область');
$kursk=$params->get('kursk', 'Курск');
$kurske=$params->get('kurske', 'Курске');
$regione=$params->get('regione', 'Курской области');
?>
<style>
	.sttnmls td, td{
		padding:0px !important;
		padding-bottom:5px !important;
		padding-top:5px !important;
	}
</style>
<link rel="stylesheet" type="text/css" href="components/com_sttnmls/assets/css/sttnmls.css">

<style>
			.tabs div{
				float:left;
				margin-right:15px;
				border-bottom:1px dotted #099;
				color:#099;
				cursor:pointer;
			}
			
			.tabs .active{
				color:#333;
				border:none;
			}
			#uploadButton2{
				width:187px !important;
				height:42px !important;
			}
			
			.bot .nmls_table tr:last-child td:last-child{
				display:table-cell;	
			}
		</style>
        <script language="javascript">
			jQuery(function(){
				jQuery(".tabs div").click(function(){

					jQuery("#pagetab1").hide();
					jQuery("#pagetab2").hide();
					jQuery("#pagetab3").hide();
					jQuery("#tab1").removeClass('active');
					jQuery("#tab2").removeClass('active');
					jQuery("#tab3").removeClass('active');
					
					jQuery("#page"+jQuery(this).attr('id')).show();
					jQuery(this).addClass('active');
				});
			});
		</script>
        <div style="background:#efefef; padding:10px; margin-bottom:10px;" class="tabs">
        	<div id="tab1" class="active">Общая информация</div>
            <div id="tab2">Ход строительства <?php if ($pics2 != ''){ echo '('.count($pics2).')';} ?></div>
            <!--<div id="tab3">Инфраструктура</div>--><br />
			<div class="clr"></div>
        </div>

<div id="pagetab2" style="display:none;">
        	<div class="editfoto" style="margin-bottom:10px;">
				<b style="padding-bottom:6px; margin-left:3px; display:inline-block;">Ход строительства</b>
                <?php

				if ($pics2 != ''){
				?>
                <div class="nmls-photo">
                <?php
				$date='';
				?>
                    <?php foreach($pics2 as $i => $pic): ?>
                        
                        	<?php
								$this->imgpath='http://kurskmetr.ru/images/sttnmls/';
								$imgm = $this->imgpath.$pic;
								$src = imagecreatefromjpeg($imgm);
								$w_ntmx = imagesx($src);
								$w_ntmy = imagesy($src);
								$res = ($w_ntmx * $w_ntmy);
								if($res > 10) {
								} else {
									$this->imgpath='http://www.nmls.ru/mlsfiles/images/';
								}
								if($this->imgpath!='http://kurskmetr.ru/images/sttnmls/')
								{
									continue;	
								}
								if($date!=date("d.m.Y",filemtime($_SERVER['DOCUMENT_ROOT']."/images/sttnmls/".$pic)))
								{
									$date=date("d.m.Y",filemtime($_SERVER['DOCUMENT_ROOT']."/images/sttnmls/".$pic));
									echo '<div style="text-align:center; clear:both; font-size:12px; padding:10px 0px; font-weight:bold;">Фото загружены: '.$date.'</div>';
								}
							?>
                            <a rel="group_2" class="jcepopup small" title="<?php echo $picnames[$i] ?>" href="<?php echo $this->imgpath.$pic ?>">
                                <img class="thumb" src="<?php echo $this->imgpath.$pic ?>" height="50" /></a>
                        
                        </a>
                    <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>
                <?php
				}else{
					echo "<br>Пока нет загруженных фото";	
				}?>
            </div>
        </div>
        
        
        <div id="pagetab3" style="display:none;">
        	<div class="editfoto" style="margin-bottom:10px;">
				<b style="padding-bottom:6px; margin-left:3px; display:inline-block;">Инфраструктура</b>
                
            </div>
        </div>








<div class="sttnmls" id="pagetab1">
<b style="padding-bottom:6px; margin-left:3px; display:inline-block; font-size:16px;">Общая информация</b>
<div class="clr"></div>
	            <div class="nmls-sidebar">
                <?php if ($pics != ''):?>
                <div class="nmls-photo">
                    <?php foreach($pics as $i => $pic): ?>
                        <?php if ($i == 0): ?>
                        	<?php
								$this->imgpath='http://kurskmetr.ru/images/sttnmls/';
								$imgm = $this->imgpath.$pic;
								$src = imagecreatefromjpeg($imgm);
								$w_ntmx = imagesx($src);
								$w_ntmy = imagesy($src);
								$res = ($w_ntmx * $w_ntmy);
								if($res > 10) {
								} else {
									$this->imgpath='http://www.nmls.ru/mlsfiles/images/';
								}
							?>
                            <a rel="group_1" class="jcepopup big" title="<?php echo $picnames[$i] ?>" href="<?php echo $this->imgpath.$pic ?>">
                                <img class="thumb" src="<?php echo $this->imgpath.$pic ?>" width="320" />
                        <?php else: ?>
                        	<?php
								$this->imgpath='http://kurskmetr.ru/images/sttnmls/';
								$imgm = $this->imgpath.$pic;
								$src = imagecreatefromjpeg($imgm);
								$w_ntmx = imagesx($src);
								$w_ntmy = imagesy($src);
								$res = ($w_ntmx * $w_ntmy);
								if($res > 10) {
								} else {
									$this->imgpath='http://www.nmls.ru/mlsfiles/images/';
								}
							?>
                            <a rel="group_1" class="jcepopup small" title="<?php echo $picnames[$i] ?>" href="<?php echo $this->imgpath.$pic ?>">
                                <img class="thumb" src="<?php echo $this->imgpath.$pic ?>" height="36" width="40" /></a>
                        <?php endif; ?>
                        </a>
                    <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>
				<?php if($this->item->VTOUR && $this->item->VTOUR!='sWebLabel1') { ?>
					<div>
						<noindex>
						<a rel="rel nofollow" href="<?php echo $this->item->VTOUR; ?>" target="_blank"><img src="components/com_sttnmls/assets/images/3dm.png"/></a>
						</noindex>
					</div>
                <?php } ?>
                <?php endif ?>
                <div class="nmls-map">
                    <div id="YMapsID"></div>
                </div>
				<div style="padding-top: 8px;"><?php echo $socbut;?></div>
            </div>
            <div class="nmls-content">
                <a name="TopShowObject"></a>
				<?php
				$user = JFactory::getUser();
				if( $user->authorise('core.admin') || $this->userperm==1) {

					$linkedit=JRoute::_('index.php?option=com_sttnmls&view=building&task=edit&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID, false);
					?>
					<div class="picsmang" style="top:0px;">
						<a href="<?php echo $linkedit; ?>" class="picedit" target="_blank" title="<?php echo JText::_('COM_STTNMLS_EDIT'); ?>"></a>
						<!--<a class="picdate" title="<?php echo JText::_('COM_STTNMLS_EDITDATE'); ?>" onclick="changeDate(<?php echo $this->item->CARDNUM; ?>,<?php echo $this->item->COMPID; ?>,'building');"></a>
                         <a href="#" style="background:#FFF; float:left; width:28px; height:28px;" onclick="changeDate2(<?php echo $this->item->CARDNUM; ?>,<?php echo $this->item->COMPID; ?>,'building'); return false;" title="Сбросить дату добавления. Стоимость: <?php echo $params->get('dateprice');?> руб."><img src="http://www.iconsearch.ru/uploads/icons/nuove/24x24/kontact_date.png" /></a>-->
						<a class="picdelete" title="<?php echo JText::_('COM_STTNMLS_DELETE'); ?>" onclick="return deleteobj(<?php echo $this->item->CARDNUM; ?>,<?php echo $this->item->COMPID; ?>,'building');"></a>
					</div>
				<?php } ?>
				<?php if($this->item->mdays>$this->item->alldays && $this->userperm==1) { ?>
					<div class="premodertext">
						<?php echo JText::_('COM_STTNMLS_PREMODER_'); ?>
					</div>
				<?php } ?>
                <h1><?php echo $this->item->title;?></h1>
                    <?php
						$db = JFactory::getDbo();
						$query	= 'SELECT * FROM j25_sttnmlscompl WHERE status=1 AND typeobj=1 AND compid="'.$this->item->COMPID.'" AND cardnum="'.$this->item->CARDNUM.'"';
						$db->setQuery($query);
						$compl	= $db->loadObjectList();
						if(count($compl)>0)
						{
						?>
                    		<a href="#compl" style="color:#B80000; border-bottom:1px dotted #B80000; position:relative; top:-5px; display:inline-block; margin-bottom:10px;">[Есть проверенные жалобы]</a>
                    	<?php
						}
					?>
                    <b><?php echo $this->item->fulltitle;?></b>
                    <table width="100%" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Город: </td>
                            <td><?php echo $this->item->gorod ?></td>
                        </tr>
                        <?php
						if($this->item->raion != '' && $this->item->RAIONID>1) {
							$r = str_replace('р-н', '', $this->item->raion);
							$r = str_replace('район', '', $this->item->raion);
							$r = str_replace('  ', ' ', $this->item->raion);
							?>
                            <tr>
                                <td style="width:10px; white-space:nowrap; padding-right:10px !important;">Район: </td>
                                <td><?php echo $r ?></td>
                            </tr>
                            <?php
						}
						?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Адрес: </td>
                            <td><?php echo $this->item->ulica ?><?php if($this->item->haap){echo ", ".$this->item->haap;}?></td>
                        </tr>
                        <?php
                        	$kv=Array('','I',"II","III","IV");
                        ?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Дата сдачи: </td>
                            <td><?php echo $kv[$this->item->datekvar]; ?> кв. <?php echo $this->item->dateyear ?> г.</td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Проектная<br />документация: </td>
                            <td>
                            <?php
                            $this->item->project="http://".str_replace("http://","",$this->item->project);
                            include("idna_convert.class.php");
                            if(preg_match('/[^0-9a-z-_A-Z:\/\.]/is', $this->item->project)!=0){
								
								$IDN1 = new idna_convert(array('idn_version' => '2008'));
								$this->item->project=$IDN1->encode($this->item->project);
							}

                            $status=get_headers($this->item->project);
							
							

							if(in_array("HTTP/1.1 200 OK", $status) or in_array("HTTP/1.0 200 OK", $status) or in_array("HTTP/1.1 200 Ok", $status) or in_array("HTTP/1.0 200 Ok", $status) or in_array("HTTP/1.1 200 ok", $status) or in_array("HTTP/1.0 200 ok", $status)){
								?>
                                	<a target="_blank" href="<?php echo $this->item->project;?>" >
                                		<img src="/images/link.png" />
                                	</a>
                                <?php
							}else{
								if($this->item->project!='')
								{
									echo str_replace("http://","",$this->item->project);
								}
							}
							if($this->item->proj_file!='')
							{
								?>
                                	<a href="/buildings_files/<?php echo $this->item->proj_file;?>" target="_blank">
                                		<img src="/images/file.png" />
                                	</a>
                                <?php	
							}
							?>
                            </td>
                        </tr>
                        
                        
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Класс жилья: </td>
                            <td>
								<?php
                                $db = JFactory::getDbo();
                                $query	= 'SELECT * FROM j25_sttnmlsclass WHERE id="'.$this->item->CLASS.'"';
                                $db->setQuery($query);
                                $classname	= $db->loadObject();
                                echo $classname->title
                                ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Тип здания: </td>
                            <td>
                                <?php echo $this->item->mat ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Этажность: </td>
                            <td>
                                <?php echo $this->item->HSTAGE ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Квартир: </td>
                            <td>
                                <?php echo $this->item->flatcount ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">В продаже: </td>
                            <td>
                            	<?php
								$types=explode(",,",$this->item->type);
								$types=array_diff($types, array(''));
                                $db = JFactory::getDbo();
                                $query	= 'SELECT * FROM j25_sttnmlstype WHERE id IN ('.implode(",",$types).')';
                                $db->setQuery($query);
                                $types	= $db->loadObjectList();
								$cn=0;
								foreach($types as $t)
								{
									$cn++;
									echo $t->title;	
									if($cn<count($types))
									{
										echo ', ';	
									}
								}
                                ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Площадь: </td>
                            <td>
                                От <?php echo $this->item->areafrom ?> до <?php echo $this->item->areato ?> м<sup>2</sup>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Отделка: </td>
                            <td>
								<?php
                                $db = JFactory::getDbo();
                                $query	= 'SELECT * FROM j25_sttnmlsfurnish WHERE id="'.$this->item->furnish.'"';
                                $db->setQuery($query);
                                $furnish	= $db->loadObject();
                                echo $furnish->title
                                ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Цена: </td>
                            <td>
                                От <?php echo $this->item->pricefrom ?> до <?php echo $this->item->priceto ?> руб.
                            </td>
                        </tr>
                        <?php
						if($this->item->comcount>0)
						{
						?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;"><b>Коммерческие объекты:</b></td>
                            <td>
                                <?php echo $this->item->comcount ?>
                            </td>
                        </tr>
                        <?php
                        	if($this->item->comareafrom>0 || $this->item->comareato>0)
                        	{
                        ?>
	                        <tr>
	                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Площадь: </td>
	                            <td>
	                            	<?php
	                            	if($this->item->comareafrom>0)
	                            	{
	                            	?>
	                                	От <?php echo $this->item->comareafrom ?>
	                                <?php
	                            	}
	                            	if($this->item->comareato>0)
	                            	{
	                            	?>
	                                	до <?php echo $this->item->comareato ?>
	                                <?php
	                                }
	                                ?>м<sup>2</sup>
	                            </td>
	                        </tr>
                        <?php
                        }
                        ?>
                        <?php
                        	if($this->item->compricefrom>0 || $this->item->compriceto>0)
                        	{
                        ?>
	                        <tr>
	                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Цена: </td>
	                            <td>
	                            	<?php
	                            	if($this->item->compricefrom>0)
	                            	{
	                            	?>
	                                	От <?php echo $this->item->compricefrom ?>
	                                <?php
	                            	}
	                            	if($this->item->compriceto>0)
	                            	{
	                            	?>
	                                	до <?php echo $this->item->compriceto ?>
	                                <?php
	                                }
	                                ?>руб.
	                            </td>
	                        </tr>
                        <?php
                        }
                        ?>

                        <?php
						}
						?>
                         <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Оплата: </td>
                            <td>
                            	<?php
								$types=explode(",,",$this->item->pay);
								$types=array_diff($types, array(''));
                                $db = JFactory::getDbo();
                                $query	= 'SELECT * FROM j25_sttnmlspay WHERE id IN ('.implode(",",$types).')';
                                $db->setQuery($query);
                                $types	= $db->loadObjectList();
								$cn=0;
								foreach($types as $t)
								{
									$cn++;
									echo $t->title;	
									if($cn<count($types))
									{
										echo ', ';	
									}
								}
                                ?>
                            </td>
                        </tr>
                        
                        
                        
                        
                        <?php
						if($this->item->dog_file!='' || $this->item->dogovor)
						{
						?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Договор: </td>
                            <td>


                            <?php



                            $this->item->dogovor="http://".str_replace("http://","",$this->item->dogovor);
                            


                            if(preg_match('/[^0-9a-z-_A-Z:\/\.]/is', $this->item->dogovor)!=0){
								$IDN = new idna_convert(array('idn_version' => '2008'));
								$this->item->dogovor=$IDN->encode($this->item->dogovor);
							}


                            $url_p=parse_url($this->item->dogovor);
                            

                            $status=get_headers($this->item->dogovor);
							
							



							if(in_array("HTTP/1.1 200 OK", $status) or in_array("HTTP/1.0 200 OK", $status) or in_array("HTTP/1.1 200 Ok", $status) or in_array("HTTP/1.0 200 Ok", $status) or in_array("HTTP/1.1 200 ok", $status) or in_array("HTTP/1.0 200 ok", $status)){
								?>
                                	<a target="_blank" href="<?php echo $this->item->dogovor;?>" >
                                		<img src="/images/link.png" />
                                	</a>
                                <?php
							}else{
								if($this->item->dogovor!='')
								{
									echo str_replace("http://","",$this->item->dogovor);
								}
							}
							if($this->item->dog_file!='')
							{
								?>
                                	<a href="/buildings_files/<?php echo $this->item->dog_file;?>" target="_blank">
                                		<img src="/images/file.png" />
                                	</a>
                                <?php	
							}
							?>
                            </td>
                        </tr>
                        <? } ?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Паркинг: </td>
                            <td>
                                <?php echo $this->item->park ?>
                            </td>
                        </tr>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Застройщик: </td>
                            <td>
                                <a target="_blank" href="/component/sttnmls/?view=firm&agency=<?php echo $this->item->comp ?>">
                                    <?php
                                    $db = JFactory::getDbo();
                                    $query	= 'SELECT * FROM j25_sttnmlsvocfirms WHERE ID="'.$this->item->comp.'"';
                                    $db->setQuery($query);
                                    $furnish	= $db->loadObject();
                                    echo $furnish->NAME;
                                    ?>
                                </a>
                                
                            </td>
                        </tr>
                        <?php
						$banks=explode(",,",$this->item->banks);
						$banks=array_diff($banks, array(''));
						if(count($banks)>0)
						{
						?>
                        <tr>
                        	<td style="width:10px; white-space:nowrap; padding-right:10px !important;">Аккредитация: </td>
                            <td>
                            <?php
							$u=0;
							foreach($banks as $b){
								$u++;
							?>
                                <a target="_blank" href="/component/sttnmls/?view=firm&agency=<?php echo $b ?>">
                                    <?php
                                    $db = JFactory::getDbo();
                                    $query	= 'SELECT * FROM j25_sttnmlsvocfirms WHERE ID="'.$b.'"';
                                    $db->setQuery($query);
                                    $furnish	= $db->loadObject();
                                    echo $furnish->NAME;
                                    ?></a><?php if($u<count($banks)){ echo ", ";}
							}
							?>
                            </td>
                        </tr>
                        <?	
						}
						?>
                    </table>
                    
                    
                    
                    
					
                
                
                <div class="nmls-block">
					<?php if($this->item->mdays<=$this->item->alldays || $this->userperm==1 || $this->userperm>2) { ?>
						<h3><?php echo JText::_('COM_STTNMLS_DOP'); ?>:</h3>
						<span style="font-size:13px; color:#333;"><?php echo nl2br($this->item->MISC); ?></span>
					<?php } else {?>
						<div><div id="hidedop"></div>
							<span id="hideud" style="display:none;"><?php echo $this->udobstva; ?></span>
						</div>
					<?php }?>
                </div>

                
                
                
                
                
                <div style="width:710px; position:relative; left:-270px;">
                <?php
					$db = JFactory::getDbo();
					$query	= 'SELECT * FROM j25_sttnmlsbplan WHERE bid="'.$_REQUEST['cn'].'" AND cid="'.$_REQUEST['cid'].'" AND type=0';
					$db->setQuery($query);
					$itms	= $db->loadObjectList();
					if(count($itms)>0)
					{
					?>
                    	<h4 style="padding:0px; margin:0px; padding-top:10px;">Студии</h4>
                        <table width="100%">
                        	<tr style="background:#666; border:none; color:#FFF; text-align:center; font-weight:bold;">
                            	<td style="padding-top:5px;">
                                	S общ 
                                </td>
   								<td>
                                	S жил
                                </td>
                                <td>
                                	S кухн 
                                </td>
                                <td>
                                	Санузел  
                                </td>
                                <td>
                                	Балкон 
                                </td>
                                <td>
                                	Цена за м<sup>2</sup>
                                </td>
                                <td>
                                	Стоимость
                                </td>
                                <td>
                                	Фото
                                </td>
                           	</tr>
                            <?php
								foreach($itms as $i)
								{
								?>
                                	<tr style="border-bottom:1px solid #efefef; color:#333; text-align:center; font-weight:bold;">
                                    	<td>
                                        	<?php echo $i->sall;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->szhil;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->skitch;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php
												if($i->san==0)
												echo "Совм.";
												else
												echo "Разд.";
											?>
                                        </td>
                                        <td>
                                        <?php
												if($i->bal==0)
												echo "Нет";
												else
												echo "Есть";
											?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price,0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price*$i->sall, 0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php
											if($i->photos!='')
											{
												?>
                                                	<a href="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=600" rel="group_<?php echo $i->id;?>" class="jcepopup small"><img src="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=50" style="width:50px; padding:2px; border:1px solid #dfdfdf;"/></a>
                                                <?php
											}
											?>
                                        </td>
                                    </tr>
                                <?php	
								}
							?>
                            </table>
                        <?php
					}
				?>
                
                
                
                
                
                
                
                
                
                <?php
					$db = JFactory::getDbo();
					$query	= 'SELECT * FROM j25_sttnmlsbplan WHERE bid="'.$_REQUEST['cn'].'" AND cid="'.$_REQUEST['cid'].'" AND type=1';
					$db->setQuery($query);
					$itms	= $db->loadObjectList();
					if(count($itms)>0)
					{
					?>
                    	<h4 style="padding:0px; margin:0px; padding-top:10px;">1-комнатные</h4>
                        <table width="100%">
                        	<tr style="background:#666; border:none; color:#FFF; text-align:center; font-weight:bold;">
                            	<td>
                                	S общ 
                                </td>
   								<td>
                                	S жил
                                </td>
                                <td>
                                	S кухн 
                                </td>
                                <td>
                                	Санузел  
                                </td>
                                <td>
                                	Балкон 
                                </td>
                                <td>
                                	Цена за м<sup>2</sup>
                                </td>
                                <td>
                                	Стоимость
                                </td>
                                <td>
                                	Фото
                                </td>

                           	</tr>
                            <?php
								foreach($itms as $i)
								{
								?>
                                	<tr style="border-bottom:1px solid #efefef; color:#333; text-align:center; font-weight:bold;">
                                    	<td>
                                        	<?php echo $i->sall;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->szhil;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->skitch;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php
												if($i->san==0)
												echo "Совм.";
												else
												echo "Разд.";
											?>
                                        </td>
                                        <td>
                                        <?php
												if($i->bal==0)
												echo "Нет";
												else
												echo "Есть";
											?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price,0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price*$i->sall, 0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php
											if($i->photos!='')
											{
												?>
                                                	<a href="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=600" rel="group_<?php echo $i->id;?>" class="jcepopup small"><img src="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=50" style="width:50px; padding:2px; border:1px solid #dfdfdf;"/></a>
                                                <?php
											}
											?>
                                        </td>
                                    </tr>
                                <?php	
								}
							?>
                            </table>
                        <?php
					}
				?>
                
                
                
                
                
                
                <?php
					$db = JFactory::getDbo();
					$query	= 'SELECT * FROM j25_sttnmlsbplan WHERE bid="'.$_REQUEST['cn'].'" AND cid="'.$_REQUEST['cid'].'" AND type=2';
					$db->setQuery($query);
					$itms	= $db->loadObjectList();
					if(count($itms)>0)
					{
					?>
                    	<h4 style="padding:0px; margin:0px; padding-top:10px;">2-комнатные</h4>
                        <table width="100%">
                        	<tr style="background:#666; border:none; color:#FFF; text-align:center; font-weight:bold;">
                            	<td>
                                	S общ 
                                </td>
   								<td>
                                	S жил
                                </td>
                                <td>
                                	S кухн 
                                </td>
                                <td>
                                	Санузел  
                                </td>
                                <td>
                                	Балкон 
                                </td>
                                <td>
                                	Цена за м<sup>2</sup>
                                </td>
                                <td>
                                	Стоимость
                                </td>
                                <td>
                                	Фото
                                </td>
                           	</tr>
                            <?php
								foreach($itms as $i)
								{
								?>
                                	<tr style="border-bottom:1px solid #efefef; color:#333; text-align:center; font-weight:bold;">
                                    	<td>
                                        	<?php echo $i->sall;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->szhil;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->skitch;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php
												if($i->san==0)
												echo "Совм.";
												else
												echo "Разд.";
											?>
                                        </td>
                                        <td>
                                        <?php
												if($i->bal==0)
												echo "Нет";
												else
												echo "Есть";
											?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price,0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price*$i->sall, 0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php
											if($i->photos!='')
											{
												?>
                                                	<a href="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=600" rel="group_<?php echo $i->id;?>" class="jcepopup small"><img src="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=50" style="width:50px; padding:2px; border:1px solid #dfdfdf;"/></a>
                                                <?php
											}
											?>
                                        </td>
                                    </tr>
                                <?php	
								}
							?>
                            </table>
                        <?php
					}
				?>
                
                
                
                
                
                
                <?php
					$db = JFactory::getDbo();
					$query	= 'SELECT * FROM j25_sttnmlsbplan WHERE bid="'.$_REQUEST['cn'].'" AND cid="'.$_REQUEST['cid'].'" AND type=3';
					$db->setQuery($query);
					$itms	= $db->loadObjectList();
					if(count($itms)>0)
					{
					?>
                    	<h4 style="padding:0px; margin:0px; padding-top:10px;">3-комнатные</h4>
                        <table width="100%">
                        	<tr style="background:#666; border:none; color:#FFF; text-align:center; font-weight:bold;">
                            	<td>
                                	S общ 
                                </td>
   								<td>
                                	S жил
                                </td>
                                <td>
                                	S кухн 
                                </td>
                                <td>
                                	Санузел  
                                </td>
                                <td>
                                	Балкон 
                                </td>
                                <td>
                                	Цена за м<sup>2</sup>
                                </td>
                                <td>
                                	Стоимость
                                </td>
                                <td>
                                	Фото
                                </td>
                           	</tr>
                            <?php
								foreach($itms as $i)
								{
								?>
                                	<tr style="border-bottom:1px solid #efefef; color:#333; text-align:center; font-weight:bold;">
                                    	<td>
                                        	<?php echo $i->sall;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->szhil;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->skitch;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php
												if($i->san==0)
												echo "Совм.";
												else
												echo "Разд.";
											?>
                                        </td>
                                        <td>
                                        <?php
												if($i->bal==0)
												echo "Нет";
												else
												echo "Есть";
											?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price,0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price*$i->sall, 0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php
											if($i->photos!='')
											{
												?>
                                                	<a href="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=600" rel="group_<?php echo $i->id;?>" class="jcepopup small"><img src="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=50" style="width:50px; padding:2px; border:1px solid #dfdfdf;"/></a>
                                                <?php
											}
											?>
                                        </td>
                                    </tr>
                                <?php	
								}
							?>
                            </table>
                        <?php
					}
				?>
                
                
                
                
                
                
                <?php
					$db = JFactory::getDbo();
					$query	= 'SELECT * FROM j25_sttnmlsbplan WHERE bid="'.$_REQUEST['cn'].'" AND cid="'.$_REQUEST['cid'].'" AND type=4';
					$db->setQuery($query);
					$itms	= $db->loadObjectList();
					if(count($itms)>0)
					{
					?>
                    	<h4 style="padding:0px; margin:0px; padding-top:10px;">4+-комнатные</h4>
                        <table width="100%">
                        	<tr style="background:#666; border:none; color:#FFF; text-align:center; font-weight:bold;">
                            	<td>
                                	S общ 
                                </td>
   								<td>
                                	S жил
                                </td>
                                <td>
                                	S кухн 
                                </td>
                                <td>
                                	Санузел  
                                </td>
                                <td>
                                	Балкон 
                                </td>
                                <td>
                                	Цена за м<sup>2</sup>
                                </td>
                                <td>
                                	Стоимость
                                </td>
                                <td>
                                	Фото
                                </td>
                           	</tr>
                            <?php
								foreach($itms as $i)
								{
								?>
                                	<tr style="border-bottom:1px solid #efefef; color:#333; text-align:center; font-weight:bold;">
                                    	<td>
                                        	<?php echo $i->sall;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->szhil;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php echo $i->skitch;?> м<sup>2</sup>
                                        </td>
                                        <td>
                                        	<?php
												if($i->san==0)
												echo "Совм.";
												else
												echo "Разд.";
											?>
                                        </td>
                                        <td>
                                        <?php
												if($i->bal==0)
												echo "Нет";
												else
												echo "Есть";
											?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price,0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php echo number_format($i->price*$i->sall, 0, ',', ' ');?>
                                        </td>
                                        <td>
                                        	<?php
											if($i->photos!='')
											{
												?>
                                                	<a href="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=600" rel="group_<?php echo $i->id;?>" class="jcepopup small"><img src="/thumb.php?name=/buildings_files/<?php echo $i->photos;?>&w=50" style="width:50px; padding:2px; border:1px solid #dfdfdf;"/></a>
                                                <?php
											}
											?>
                                        </td>
                                    </tr>
                                <?php	
								}
							?>
                            </table>
                        <?php
					}
				?>
                </div>
                
                
                
                
                
                
                
                
                <?php
				$db = JFactory::getDbo();
				$user = JFactory::getUser();
				$userid = $user->get('id');
				$query2	= 'SELECT * FROM #__sttnmlsvocagents WHERE userid="'.$userid.'"';
				$db->setQuery($query2);
				$myagent	= $db->loadObject();
				$vis=0;
				if($this->item->sob_visible==1)
				{
					if($this->item->COMPID==$myagent->COMPID)
					{
						$vis=1;	
					}
				}else{
					if($this->item->COMPID==$myagent->COMPID && $this->item->AGENTID==$myagent->ID)
					{
						$vis=1;	
					}
				}
				if($vis==1)
				{
					if($this->item->sob_tel!='')
					{
					?>
                    	<strong>Основной телефон собственника:</strong> <?php echo $this->item->sob_tel;?><br />
                    <?php
					}
					if($this->item->sob_tel2!='')
					{
					?>
                    	<strong>Дополнительный телефон собственника:</strong> <?php echo $this->item->sob_tel2;?><br />
                    <?php
					}
					if($this->item->sob_fio!='')
					{
					?>
                        <strong>ФИО собственника:</strong> <?php echo $this->item->sob_fio;?><br />
                    <?php
					}
					
				}
				?>
                
                
				
                <div class="nmls-block" style="margin-top:0px">
                    <div class="nmls-attributs calendar">
                        <b><?php echo JText::_('COM_STTNMLS_ADDED'); ?>:</b> <span class="datecard"><?php echo $this->item->datecr ?></span><br />
                        <b><?php echo JText::_('COM_STTNMLS_UPDATED'); ?>:</b> <span class="datecard"><?php echo $this->item->datemd ?></span>
                    </div>
					<div class="textright">
						<div>
							<div class="wcntcard">
								<span><?php echo JText::_('COM_STTNMLS_WATCHCNT'); ?>:</span> <b><?php echo $this->item->watchcount ?></b>
							</div>
						</div>
					</div>
                </div>
                <div class="clearr"></div>
                <div class="nmls-block">
					<!--<div style="min-height:35px;">
						<div id="add_favornmls" tp="<?php echo $this->tp; ?>" cid="<?php echo $this->item->COMPID; ?>" cn="<?php echo $this->item->CARDNUM; ?>"></div>
						<a class="modal prosmotr" rel="{handler: 'iframe', size: {x: 400, y: 300}}" href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=order&tmpl=component&obj=apart&cid='.$this->item->COMPID.'&cn='.$this->item->CARDNUM); ?>"></a>
					</div>-->
					<div class="clearr"></div>
                </div>
                <div class="nmls-block" id="main_cont_pos">
                    <hr />
                </div>
                
                
                
                <div class="nmls-block" id="main_cont">
                    <h3><?php echo JText::_('COM_STTNMLS_CONTS'); ?>:</h3>
                    <table class="nmls_table">
						<?php if($this->item->mdays<=$this->item->alldays || $this->userperm==1 || $this->userperm==3) { ?>
							<tr>
								<td class="nmls_agent agentthumb">
									<a href="<?php echo '/component/sttnmls/?view=firm&agency='. $this->item->comp; ?>"<?php if($this->item->firm_url != ''){ echo ' target="_blank"'; } ?>>
									
									
									
									
									<img src="<?php echo $this->item->imgfirm ?>" alt="" /></a>
								</td>
								<td class="vtop" style="padding-left:7px !important;">
									<?php echo $this->item->FIRMNAME ?>
								</td>
							</tr>
						<?php } else { ?>
							<tr  id="hidefirm"></tr>
						<?php } ?>
                    </table>
                </div>
                
                
                
                <div class="nmls-block">
                    <div class="nmls-otzif-btn otzifOpen">
                        <a href="javascript:void(0);"><?php echo JText::_('COM_STTNMLS_SENDOTZ'); ?></a>
                    </div>
                    <div class="nmls-otzif" id="otzif">
                        <?php echo $this->loadTemplate('complaint'); ?>
                    </div>
                    <?php
					if(count($compl)>0)
					{
						?>
                        <div id="compl"></div>
                        <?php
						foreach($compl as $item)
						{
						$rs = '';
						if($item->rs==1) $rs = "Продано";
						if($item->rs==2) $rs = "Неверная цена";
						if($item->rs==3) $rs = "Ошибка в информации";
						if($item->rs==4) $rs = "Другое";
						?>
                        <div style="padding-bottom:10px; margin-top:10px; font-size:12px; border-bottom:1px solid #dfdfdf;">
                            <b><?php echo $rs; ?>:</b> <?php echo $item->desc; ?><br />
                            <b>Проверено:</b> <?php if($item->status==0){ echo "Нет <img style='position:relative; top:4px;' src='http://www.iconsearch.ru/uploads/icons/crystalclear/16x16/edit_remove.png' alt='unapproved'/>"; }else{ echo "Да <img src='http://kurskmetr.ru/administrator/templates/isis/images/admin/icon-16-allow.png' alt='approved'/> [Объект не показывается в общем списке]"; }?><br />
                            <span style="color:#999;"><b>Дата:</b> <?php echo date('d.m.Y H:i:s',strtotime($item->dateinp)); ?></span>
                            <?php
							if($_SERVER['REMOTE_ADDR']=='77.121.91.122' && $item->correct!=1)
							{
								if( $user->authorise('core.admin') || $this->userperm==1) {
									if($_REQUEST['act']=='check_comp')
									{
										$app = JFactory::getApplication();
										$mailfrom = $app->getCfg('mailfrom');
										$fromname = $app->getCfg('fromname');
										$mail = JFactory::getMailer();
										$params = JComponentHelper::getParams('com_sttnmls');
										$sendadmin = $params->get('adminemail', '');
										
										$body="Была исправлена жалоба в объекте.\r\n\r\n";
										$body .= 'Объект: '.$_SERVER['HTTP_REFERER']."\n";
										$body .= 'Текст жалобы: '.$_REQUEST['text']."\n";
										$body .= 'В админ панеле жалоба помечена как исправленная';
										
										
										$db->setQuery('UPDATE #__sttnmlscompl set correct="1" WHERE id='.$db->quote($_REQUEST['comp_id']));
										$db->query();
										
										if($sendadmin) {
											$mail = JFactory::getMailer();
											$mail->addRecipient($sendadmin);
											$mail->setSender(array($mailfrom, $fromname));
											$mail->setSubject("Исправлена жалоба");
											$mail->setBody($body);
											$sent = $mail->Send();
										}				
									}else{
										?>
											<form method="post" id="compla_<?php echo $item->id;?>">
												<input type="hidden" name="act" value="check_comp" />
												<input type="hidden" name="comp_id" value="<?php echo $item->id;?>" />
                                                <input type="hidden" name="text" value="<?php echo $item->desc; ?>" />
												<input type="hidden" name="cardnum" value="<?php echo $this->item->CARDNUM;?>" />
												<input type="hidden" name="compid" value="<?php echo $this->item->COMPID;?>" />
												<input type="submit" value="Исправлено" />
											
											</form>
										<?php
									}
								}
							}
							?>
                        </div>
                        <?php	
						}
					}
					?>
				<div class="nmls-block margintop">
                </div>
                </div>
				<?php if($this->creditdoptext) { ?>
				<div class="creditd"><?php echo '<sup>*</sup>'.$this->creditdoptext; ?></div>
				<?php } ?>
            </div>
            <div class="clearfix"></div>
			<?php 
				if( $user->authorise('core.admin') || $this->userperm==1) { 
					$upobj=$params->get('upobj', '30');
					$balance = SttNmlsHelper::getBalance();
					$linkaddusl = JRoute::_('index.php?option=com_sttnmls&view=aparts&type=card&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID . '&task=upobj', false);
					$oUpobj = new stdClass();
					$oUpobj->typeobj = 1;
					$oUpobj->cardnum = $this->item->CARDNUM;
					$oUpobj->compid = $this->item->COMPID;
					$oUpobj->usluga = 'upobj';
					?>
					<div id="plat" style="display:none;">
						<div class="platdesc"><?php echo JText::sprintf('COM_STTNMLS_PLAT_OBJ_UP_DESC', $upobj);?></div>
						<?php
						require_once JPATH_ROOT . '/components/com_sttnmls/helpers/platusl.php';
						if(!SttNmlsPlatusl::check($oUpobj)) {
							if($balance>=$upobj) { ?>
								<a class="platgreen" href="<?php echo $linkaddusl;?>" onclick="return confirm('<?php echo JText::_('COM_STTNMLS_PLAT_SHURE');?>');"><?php echo JText::_('COM_STTNMLS_PLAT_OBJ_UP');?></a>
							<?php } else { ?>
								<div class="nomoney">
									<b><?php echo JText::_('COM_STTNMLS_NOMONEY');?></b>
									<br/>
									<a href="<?php echo JRoute::_('index.php?option=com_sttnmls&view=robokassa&layout=default', false);?>"><?php echo JText::_('COM_STTNMLS_PAY_NOW');?></a>
								</div>
							<?php }
						} else { 
							$linkoff = JRoute::_('index.php?option=com_sttnmls&view=aparts&type=card&cn=' . $this->item->CARDNUM . '&cid=' . $this->item->COMPID . '&task=upobjoff', false);
							?>
							<a href="<?php echo $linkoff; ?>" class="platoff" onclick="return confirm('<?php echo JText::_('COM_STTNMLS_PLAT_OFF_SHURE');?>');">
								<?php echo JText::_('COM_STTNMLS_PLATUSL_OFF'); ?>
							</a>	
						<?php } ?>
					</div>
				<?php }
			?>			
            <div class="clearfix"></div>
            <div class="nmls-footer" style="display:none;">
                <?php
                    $this->stype = (intval($this->item->VARIANT) == 2)?1:0;

                    $footer = array();
					$link = 'index.php?option=com_sttnmls&view=aparts&stype='.$this->stype;
                    $footer[] = '<a href="'.JRoute::_($link,false).'">'.SttNmlsLocal::TText('COM_STTNMLS_OBL').'</a>';
                    if($this->item->gorod != SttNmlsLocal::TText('COM_STTNMLS_OBL')) { $footer[] = '<a href="'.JRoute::_($link,false).'&filter_city='.$this->item->CITYID.'">'.$this->item->gorod.'</a>'; }
                    if($this->item->raion != '' && $this->item->RAIONID>1){ $footer[] = '<a href="'.JRoute::_($link,false).'&filter_my_check[]='.$this->item->RAIONID.'">'.$this->item->raion.JText::_('COM_STTNMLS_RAION_M').'</a>'; }
                    if($this->item->mraion != ''){ $footer[] = '<a href="'.JRoute::_($link,false).'&filter_my_MRcheck[]='.$this->item->MRAIONID.'">'.$this->item->mraion.'</a>'; }
                    if($this->item->ulica != ''){ $footer[] = '<a href="'.JRoute::_($link,false).'&streets='.$this->item->STREETID.'">'.$this->item->ulica.'</a>'; }

                    echo implode(' &rarr; ', $footer);
                ?>
            </div>
</div>
<?php } ?>
