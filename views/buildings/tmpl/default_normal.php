<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');

// Data ordering
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$orderCol = $listOrder;
$orderDirn = $listDirn;

// Getting vars
$street = $app->input->getUint('street_id', 0);
$agency = $app->input->getUint('agency', 0);
$agent = $app->input->getUint('agent', 0);
$stype = $app->input->getUint('stype', 0);
$what = $app->input->getUint('what', 0);

$filter_view_type = SttNmlsHelper::getViewType();

?>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <h1>
            <?php
                $title = '';
                if(JRequest::getString('titleh1') != '') {
                    $title = JRequest::getString('titleh1');
                } else {
                    $title = JText::_('COM_STTNMLS_BUILDINGS_HEAD');
                }
                printf('%s (%d)', $title, $this->countobj);
            ?>
            </h1>
        </div>
        <form action="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'buildings', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : '')) ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal filters" target="_self">
            <?php if($street) : ?>
                <input type="hidden" name="streets" value="<?php echo $street; ?>">
            <?php endif; ?>
            <?php if($agency) : ?>
                <input type="hidden" name="agency" value="<?php echo $agency; ?>">
            <?php endif; ?>
            <?php if($agent) : ?>
                <input type="hidden" name="agent" value="<?php echo $agent; ?>">
            <?php endif; ?>
            <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
            <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />

            <?php echo (($this->raw) ? '' : $this->loadTemplate('filter')) ?>

            <div class="row-fluid clearfix view-type-select" style="margin: 10px 0;">
                <div class="col-md-12">
                    <div class="btn-group to-left" data-toggle="buttons">
                        <label class="btn btn-default btn-sm<?php echo (($filter_view_type == 0) ? ' active' : '') ?>" data-toggle="tooltip" data-placement="top" title="Стандартный вид">
                            <input type="radio" name="filter_easy" value="0" autocomplete="off"<?php echo (($filter_view_type == 0) ? ' checked="checked"' : '') ?> />
                            <span class="glyphicon glyphicon-th-list"></span>
                        </label>
                        <label class="btn btn-default btn-sm<?php echo (($filter_view_type == 1) ? ' active' : '') ?>" data-toggle="tooltip" data-placement="top" title="Адаптивный вид">
                            <input type="radio" name="filter_easy" value="1" autocomplete="off"<?php echo (($filter_view_type == 1) ? ' checked="checked"' : '') ?> />
                            <span class="glyphicon glyphicon-th"></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <?php echo $this->loadTemplate('normal_' . (($filter_view_type == 1) ? 'list' : 'standart') . '_view'); ?>
            </div>

            <div class="row-fluid text-center">
                <?php echo $this->pagination->getListFooter(); ?>
            </div>

        </form>
    </div>
</div>
<div class="col-md-12">
    <?php
	if($this->pagination->get('pages.current',1) == 1) {
            $db = JFactory::getDbo();
            $query	= 'SELECT * FROM #__content WHERE id="' . JRequest::getVar('article_id') . '"';
            $db->setQuery($query);
            $aftertext	= $db->loadObjectList();
            if($aftertext) {
                echo $aftertext[0]->introtext."<br>".$aftertext[0]->fulltext;
            }
	}
    ?>
</div>