<?php defined('_JEXEC') or die;

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');

// Get JOOMLA core classes
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$params = JComponentHelper::getParams('com_sttnmls');


$filter_rooms = $this->state->get('filter.rooms', array());
$filter_mw = $this->state->get('filter.mw', array());
$filter_raions = $this->state->get('filter.raions', array());
$filter_mraions = $this->state->get('filter.mraions', array());
$filter_area_ot = $this->state->get('filter.area_ot', 0);
$filter_area_do = $this->state->get('filter.area_do', 0);
$filter_price_ot = $this->state->get('filter.price_ot', 0);
$filter_price_do = $this->state->get('filter.price_do', 0);
$filter_comp = $this->state->get('filter.comp', 0);
$filter_class = $this->state->get('filter.class', 0);
$filter_wmaterid = $this->state->get('filter.wmaterid', 0);
$filter_datekvar = $this->state->get('filter.datekvar', array());
$filter_dateyear = $this->state->get('filter.dateyear', array());
$filter_furnish = $this->state->get('filter.furnish', 0);
$filter_view_type = $this->state->get('filter.view_type', 0);
$filter_hide_complete = $this->state->get('filter.hide_complete', 1);
$city = $this->state->get('filter.city', 0);

// VARS from request
$agency = $app->input->getUint('agency', 0);
$agent = $app->input->getUint('agent', 0);
$stype = $app->input->getUint('stype', 0);
$what = $app->input->getUint('what', 0);

?>

    <input type="hidden" name="act" value="" id="actfil"/>
    <input type="hidden" name="typepdf" value="" id="typepdf"/>
    <input type="hidden" name="what" value="<?php echo $what ?>" id="whatfil"/>
    <input type="hidden" name="stype" value="<?php echo $stype; ?>" />
    <input type="hidden" name="UPD" value="" id="newsearch" />
    <input type="hidden" name="RST" value="" id="resetform" />

    <input type="hidden" name="filter_mw" value="null"/>
    <input type="hidden" name="filter_rooms[]" value="null"/>
    <input type="hidden" name="filter_raions[]" value="null"/>
    <input type="hidden" name="filter_mraions[]" value="null"/>
    <input type="hidden" name="filter_exclusive" value="0" />
    <input type="hidden" name="filter_price_day" value="0" />
    <input type="hidden" name="filter_price_hour" value="0" />
    <input type="hidden" name="filter_hide_complete" value="0" />

    <input type="hidden" id="city_field" name="city" value="<?php echo $city; ?>" />

<?php if($this->access->isAdmin OR $this->access->isModerator) : ?>
    <input type="hidden" name="filter_expired" value="0" />
    <input type="hidden" name="filter_owner" value="0" />
<?php endif; ?>

    <ul class="nav nav-tabs">
        <li role="presentation" class="active">
            <a href="<?php echo SttNmlsHelper::getSEFUrl('com_sttnmls', 'buildings', (($this->filtersGETVarsList) ? '&' . implode('&', $this->filtersGETVarsList) : '')) ?>">
                <span class="glyphicon glyphicon-rub"></span>
                <?php echo JText::_('COM_STTNMLS_SELL'); ?>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-content-wrapper">
            <!-- ROW #1 -->
            <div class="row">
                <!-- COL #1 -->
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_LABEL_POSITION') ?></div>
                    <div class="row">
                        <!-- Select REGION or CITY -->
                        <div class="col-md-12 extend-filter-col" style="margin-bottom: 15px;">
                            <div class="cityselect">
                                <select name="filter_city" id="filter_city" class="form-control input-sm" onchange="changecity(this);jQuery('input#T4').val('<?php echo JText::_('COM_STTNMLS_STREET_SELECT'); ?>');">
                                    <?php echo JHtml::_('select.options', $this->cityopt, 'value', 'text', $this->state->get('filter.city'));?>
                                </select>
                            </div>
                        </div>
                        <!-- /Select REGION or CITY -->
                    </div>

                    <div class="row">
                        <!-- Select STREET -->
                        <div class="col-md-12 extend-filter-col">
                            <div class="input-group">
                                <input type="text" class="form-control input-sm" id="streetFilter" name="filter_street" value="<?php echo $this->state->get('filter.street') ?>" placeholder="<?php echo JText::_('COM_STTNMLS_STREET_SELECT') ?>" autocomplete="false" />
                                <span class="input-group-btn">
                                    <a href="javascript:void(0);" class="btn btn-default btn-sm clearStreetFilter"><b>x</b></a>
                                </span>
                            </div>
                        </div>
                        <!-- /Select STREET -->
                    </div>
                </div>
                <!-- /COL #1 -->

                <!-- COL #2 -->
                <div class="col-md-1 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_ROOMS'); ?></div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="1"<?php if(array_search(1, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 1
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="2"<?php if(array_search(2, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 2
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="3"<?php if(array_search(3, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 3
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_rooms[]" value="4"<?php if(array_search(4, $filter_rooms) !== false) echo ' checked="checked"'; ?> > 4+
                        </label>
                    </div>
                </div>
                <!-- /COL #2 -->

                <!-- COL #3 -->
                <div class="col-md-6 col-sm-12 col-xs-12">

                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_SQUPR'); ?></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="text-left control-label col-xm-4 col-xs-12 col-md-4 col-sm-4"><?php echo JText::_('COM_STTNMLS_AREAOT'); ?></label>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_area_ot" class="form-control input-sm sm-options" style="width:90px;">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $sq = explode(",", $params->get('sq_aparts_sell', ''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="'.$i.'" ';
                                            if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }else{
                                        $sq = explode(",",$params->get('sq_aparts_arenda',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_ot == $i && $filter_area_ot != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-xm-4 col-xs-6 col-sm-4">
                                <select name="filter_area_do" class="form-control input-sm  sm-options" style="width:90px;">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $sq = explode(",", $params->get('sq_aparts_sell',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }else{
                                        $sq = explode(",", $params->get('sq_aparts_arenda',''));
                                        foreach ($sq as $i)
                                        {
                                            echo '<option value="' . $i . '" ';
                                            if($filter_area_do == $i && $filter_area_do != 0) echo 'selected';
                                            echo '>' . $i . '';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top:10px;">
                            <label class="text-left col-xm-4 col-md-4 col-sm-4 col-xs-12 control-label"><?php echo JText::_('COM_STTNMLS_PRICEOT'); ?></label>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_ot" class="form-control input-sm sm-options" style="width:90px;">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVOT'); ?></option>
                                    <?php
                                    if(!$stype)
                                    {
                                        $array_price = explode(",",$params->get('price_aparts_sell',''));
                                    }else{
                                        $array_price = explode(",",$params->get('price_aparts_arenda',''));
                                    }
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_ot == $one_price && $filter_price_ot != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6 col-xm-4 col-md-4 col-sm-4">
                                <select name="filter_price_do" class="form-control input-sm sm-options" style="width:90px;">
                                    <option value=""><?php echo JText::_('COM_STTNMLS_NOTVDO'); ?></option>
                                    <?php
                                    foreach($array_price as $one_price)
                                    {
                                        if($one_price != '') {
                                            echo'<option value="'.$one_price.'" ';
                                            if($filter_price_do == $one_price && $filter_price_do != 0) echo 'selected';
                                            echo'>'.number_format($one_price, 0, ',', ' ').'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //ROW #1 -->

            <hr />

            <a href="javascript:void(0);" class="showDopFilters<?php echo (($params->get('dopfilter', 0)) ? ' showed' : '') ?>" data-showed-text="<?php echo JText::_('COM_STTNMLS_DNDOPF'); ?>" data-closed-text="<?php echo JText::_('COM_STTNMLS_UPDOPF'); ?>"><?php echo JText::_('COM_STTNMLS_UPDOPF'); ?></a>


            <!-- ROW #2 -->
            <div class="row" style="margin-top:10px;">
                <!-- Raions select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <button id="regionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="raion">
                        <?php echo JText::_('COM_STTNMLS_OKRUG'); ?>
                        <span class="caret"></span>
                    </button>
                    <div id="raion" class="dropdown-menu dropdown-menu-checkbox">
                        <?php foreach($this->raions as $raion) : ?>
                            <div>
                                <label>
                                    <input type="checkbox" class="r_check" name="filter_raions[]" value="<?php echo $raion->value; ?>" <?php if(array_search($raion->value, $filter_raions)!==false) echo ' checked="checked" '; ?> onclick="clickraion();"/>
                                    <?php echo $raion->text; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- /Raions select -->

                <!-- MRaions select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <button id="mraionList" type="button" class="btn btn-default btn-block checkbox-list" data-target="mraion">
                        <?php echo JText::_('COM_STTNMLS_MRAIONS'); ?>
                        <span class="caret"></span>
                    </button>
                    <div id="mraion" class="dropdown-menu dropdown-menu-checkbox">
                        <?php foreach($this->mraions as $mraion) : ?>
                            <div>
                                <label>
                                    <input type="checkbox" class="mr_check" name="filter_mraions[]" value="<?php echo $mraion->value; ?>" <?php if(array_search($mraion->value, $filter_mraions)!==false) echo ' checked="checked" '; ?>/>
                                    <?php echo $mraion->text; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- /MRaions select -->

                <!-- Material select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <?php
                        $options = array();
                        $options[] = array('text' => JText::_('COM_STTNMLS_MW'), 'value' => 0);
                        if($this->wmater) {
                            $options[] = JHtml::_('select.optgroup', '-------');
                            foreach($this->wmater as $item) {
                                $options[] = JHtml::_('select.option', $item->ID, $item->NAME);
                            }
                            $options[] = JHtml::_('select.optgroup', '');
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_mw', ' class="form-control"', 'value', 'text', $filter_mw);
                    ?>
                </div>
                <!-- /Material select -->
                
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="filter_hide_complete" value="1"<?php echo (($filter_hide_complete) ? ' checked="checked"' : '')?> />
                            <?php echo JText::_('COM_STTNMLS_LABEL_HIDE_COMPLETE'); ?>
                        </label>
                    </div>
                </div>

            </div>
            <!-- //ROW #2 -->

            <!-- ROW #3 -->
            <div class="row" style="margin-top:10px;">

                <!-- Builders select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDER') ?></div>
                    <div>
                    <?php
                        $options = array();
                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                        if($this->builders_categories_list) {
                            $options[] = JHtml::_('select.optgroup', '-------');
                            foreach($this->builders_categories_list as $item) {
                                $options[] = JHtml::_('select.option', $item->ID, $item->realname);
                            }
                            $options[] = JHtml::_('select.optgroup', '');
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_comp', ' class="form-control"', 'value', 'text', $filter_comp);
                    ?>
                    </div>
                </div>
                <!-- /Builders select -->

                <!-- Building classes select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_CLASS') ?></div>
                    <div>
                    <?php
                        $options = array();
                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));
                        if($this->buildings_classes_list) {
                            $options[] = JHtml::_('select.optgroup', '-------');
                            foreach($this->buildings_classes_list as $item) {
                                $options[] = JHtml::_('select.option', $item->id, $item->title);
                            }
                            $options[] = JHtml::_('select.optgroup', '');
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_class', ' class="form-control"', 'value', 'text', $filter_class);
                    ?>
                    </div>
                </div>
                <!-- /Building classes select -->

                <!-- Building furnish select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_FURNISH') ?></div>
                    <div>
                    <?php
                        $options = array();
                        $options[] = JHtml::_('select.option', 0, JText::_('COM_STTNMLS_SELECT0'));

                        if($this->buildings_furnish_list) {
                            $options[] = JHtml::_('select.optgroup', '-------');
                            foreach($this->buildings_furnish_list as $item) {
                                $options[] = JHtml::_('select.option', $item->id, $item->title);
                            }
                            $options[] = JHtml::_('select.optgroup', '');
                        }
                        echo JHTML::_('select.genericlist',  $options, 'filter_furnish', ' class="form-control"', 'value', 'text', $filter_furnish);
                    ?>
                    </div>
                </div>
                <!-- /Building furnish select -->

                <!-- Building date select -->
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="col-header"><?php echo JText::_('COM_STTNMLS_LABEL_BUILDING_DATE') ?></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-right:2px;">
                            <?php
                                $options = array();
                                $options[] = JHtml::_('select.option', 0, 'Квартал');
                                $options[] = JHtml::_('select.option', 1, 'I');
                                $options[] = JHtml::_('select.option', 2, 'II');
                                $options[] = JHtml::_('select.option', 3, 'III');
                                $options[] = JHtml::_('select.option', 4, 'IV');

                                echo JHtml::_('select.genericlist', $options, 'filter_datekvar', ' id="datekvar" class="form-control"', 'value', 'text', $filter_datekvar);
                            ?>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left:2px;">
                            <?php
                                $options = array();
                                $options[] = JHtml::_('select.option', 0, 'Год');

                                $Y = date('Y');
                                for($i = 0; $i < 11; $i++) {
                                    $options[] = JHtml::_('select.option', $Y, $Y);
                                    $Y++;
                                }
                                echo JHtml::_('select.genericlist', $options, 'filter_dateyear', ' id="dateyear" class="form-control validate-sttnum"', 'value', 'text', $filter_dateyear);
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /Building date select -->
            </div>
            <!-- //ROW #3 -->


            <div class="row" style="margin-top:10px;">
                <div class="col-md-3 col-md-push-3 col-md-offset-3 col-sm-6 col-sm-push-6 col-xs-12">
                    <button type="submit" class="btn btn-warning btn-block to-right" onclick="document.getElementById('actfil').value='';document.getElementById('newsearch').value='search';">
                        <?php echo JText::_('COM_STTNMLS_SEARCH'); ?>
                    </button>
                </div>
                <div class="col-md-3 col-md-pull-3 col-sm-6 col-sm-pull-6 col-xs-12">
                    <button type="submit" class="btn btn-default btn-block to-left" onclick="document.getElementById('adminForm').target='_self'; document.getElementById('resetform').value='1'; document.getElementById('actfil').value=''; document.getElementById('filter_easy').value=0; document.getElementById('phone_fil').value=''; document.getElementById('close').checked=false;">
                        <?php echo JText::_('COM_STTNMLS_RESET'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="active-filters">
        <?php echo $this->loadTemplate('filters_list'); ?>
    </div>
    