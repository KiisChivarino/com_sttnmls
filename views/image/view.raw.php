<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewImage extends JViewLegacy
{
	function display($tpl = null) 
	{
		$src = JRequest::getVar('src', '');
		$this->image_type = substr($src, -3);
		$this->out=$this->get('Output');
		parent::display($tpl);
	}

}
