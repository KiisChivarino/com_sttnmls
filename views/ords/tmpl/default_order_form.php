<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Get JOOMLA core classes
$app = JFactory::getApplication();
$db = JFactory::getDbo();
$params = JComponentHelper::getParams('com_sttnmls');

//
$form_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'ords');
$pay_link = SttNmlsHelper::getSEFUrl('com_sttnmls', 'robokassa');

// Флаг недостаточности средств на счету
$_enough_money = ($this->agentBalanceInfo->balans < $params->get('deliveryprice', 0));

?>

<div class="well well-sm">
    <form action="<?php echo $form_link ?>" class="form-horizontal form-validate" method="post">
        <?php echo JHtml::_('form.token');?>
        <input type="hidden" name="jform[id]" id="jform_id" value="0">
        <input type="hidden" name="task" value="saveord">

        <div class="h3" style="color: #000;">
            Отправить заявку
        </div>
        <hr />

        <div class="form-group">
            <label class="col-md-2 col-sm-6 col-xs-12 control-label"><?php echo JText::_('COM_STTNMLS_VARIANT'); ?></label>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <select id="jform_variant" name="jform[variant]" class="form-control">
                    <option value="0"><?php echo JText::_('COM_STTNMLS_VARIANT0'); ?></option>
                    <option value="1"><?php echo JText::_('COM_STTNMLS_VARIANT1'); ?></option>
                    <option value="2"><?php echo JText::_('COM_STTNMLS_VARIANT2'); ?></option>
                    <option value="3"><?php echo JText::_('COM_STTNMLS_VARIANT3'); ?></option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 col-sm-6 col-xs-12 control-label"><?php echo JText::_('COM_STTNMLS_ORD_TYPEOBJ'); ?></label>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <select id="jform_typeobj" name="jform[typeobj]" class="form-control">
                    <option value="0"><?php echo JText::_('COM_STTNMLS_ORD_TYPEOBJ0'); ?></option>
                    <option value="1"><?php echo JText::_('COM_STTNMLS_ORD_TYPEOBJ1'); ?></option>
                    <option value="2"><?php echo JText::_('COM_STTNMLS_ORD_TYPEOBJ2'); ?></option>
                    <option value="3"><?php echo JText::_('COM_STTNMLS_ORD_TYPEOBJ3'); ?></option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 col-sm-6 col-xs-12 control-label"><?php echo JText::_('COM_STTNMLS_ORD_DESCR'); ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea name="jform[descr]" id="jform_descr" class="form-control required"  rows="5"></textarea>
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-offset-2 col-md-6 col-sm-offset-6 col-sm-6 col-xs-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delivery" value="1"  <?php echo (($_enough_money) ? ' disabled="disabled"' : '') ?> />
                        <?php echo JText::sprintf('COM_STTNMLS_LABEL_SEND_ORDER_TO_AGENTS', $params->get('deliveryprice', 0)) ?>
                    </label>
                </div>
                <?php if($_enough_money) : ?>
                    <div class="alert alert-warning" style="margin-top: 5px;">
                        <?php echo JText::sprintf('COM_STTNMLS_MESSAGE_INFO_NOT_ENOUGH_MONEY', $pay_link) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-6 col-sm-offset-6 col-sm-6 col-xs-12">
                <button type="submit" class="btn btn-success btn-lg validate"><?php echo JText::_('COM_STTNMLS_SEND'); ?></button>
            </div>
        </div>
    </form>
</div>

