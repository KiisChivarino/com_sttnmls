<?php
// На сайте робокассы указывать:
// Result URL: http://www.вашсайт.ru/index.php?option=com_sttnmls&task=robo_pay&tmpl=component
// Success URL: http://www.вашсайт.ru/index.php?option=com_sttnmls&view=robokassa&layout=done
// Fail URL: http://www.вашсайт.ru/index.php?option=com_sttnmls&task=robo_cancel
// методы - POST

defined('_JEXEC') or die;

abstract class SttNmlsRobokassa {
	
	public static function getParams() {
		$params = JComponentHelper::getParams('com_sttnmls');
		$ret = new stdClass();
		$ret->login=$params->get('robologin', '');
		$ret->pass1=$params->get('robopass1', '');
		$ret->server=$params->get('roboserver', 'http://test.robokassa.ru/Index.aspx');
		$sums=$params->get('sums', '100;200;500;1000');
		$ret->sums = explode(';', $sums);
		return $ret;
	}
	
	public static function putPay() {
		// успешное завершение оплаты
		$payment_data = JRequest::get('post');
		if(!isset($payment_data['InvId'])) return 'error no InvId';
		$invid = $payment_data['InvId'];
		// проверка правильности данных
		$notify_signature = $payment_data['SignatureValue'];
		$sum = $payment_data['OutSum'];
		$params = JComponentHelper::getParams('com_sttnmls');
		$mrh_pass2 = $params->get('robopass2', '');
		$sig = strtoupper(md5("$sum:$invid:$mrh_pass2"));
		if ($sig == $notify_signature) {
			$db = JFactory::getDbo();
			$db->setQuery('select a.userid, ifnull(b.userid,0) as buserid from #__sttnmlsbalanslog as a left join #__sttnmlsbalans as b on a.userid=b.userid where a.id='.$db->quote($invid));
			$ret=$db->loadObject();
			if(!$ret) 'error no order';
			$db->setQuery('update #__sttnmlsbalanslog set status=1, summa='.$db->quote($sum).', prim="Пополнение лицевого счета" where status=0 and id='.$db->quote($invid));
			$db->execute();
			if($ret->buserid) {
				$db->setQuery('update `#__sttnmlsbalans` as b, (select sum(summa) as itog from `#__sttnmlsbalanslog` where `status`>0 and userid='.$db->quote($ret->userid).') as a set b.dateinp=NOW(), b.balans=a.itog where b.userid='.$db->quote($ret->userid));
			} else {
				$db->setQuery('insert into `#__sttnmlsbalans` (userid,balans,dateinp) select '.$db->quote($ret->userid).', sum(summa), now() from `#__sttnmlsbalanslog` where `status`>0 and userid='.$db->quote($ret->userid) );
			}
			$db->execute();
			return 'OK'.$invid;
		} else {
			return 'error signature';
		}
	}
	public static function cancelPay() {
		// отказ от оплаты
		$payment_data = JRequest::get('post');
		if(!isset($payment_data['InvId'])) return false;
		$db = JFactory::getDbo();
		$invid = $payment_data['InvId'];
		$db->setQuery('delete from #__sttnmlsbalanslog where status=0 and id='.$db->quote($invid));
		$db->execute();
		return true;
	}
}