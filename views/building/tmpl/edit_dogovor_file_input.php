<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

?>

<?php if($this->item->dog_file) : ?>
    <div class="thumbnail">
        <div class="btn-group">
            <a href="<?php echo JURI::root() . 'files/buildings/' . $this->item->dog_file ?>" class="btn btn-xs btn-default" target="_blank">
                <?php echo $this->item->dog_file ?>
            </a>
            <a href="javascript:void(0);" class="btn btn-xs btn-danger removeDogovorFile" data-cn="<?php echo $this->item->CARDNUM ?>" data-cid="<?php echo $this->item->COMPID ?>" data-view="building">
                <span class="glyphicon glyphicon-remove"></span>
                <span class="sr-only">remove</span>
            </a>
        </div>
    </div>
<?php else: ?>
    <input type="text" id="dogovor" class="form-control" name="jform[dogovor]" value="<?php echo $this->item->dogovor ?>" />
    <div class="help-block">
        <?php echo JText::_('COM_STTNMLS_LABEL_BUILDINGS_PROJECT_DOCS_UPLOAD') ?>:<br />
        <input type="file" name="dogovor_file" />
    </div>
<?php endif; ?>