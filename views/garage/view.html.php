<?php
defined('_JEXEC') or die ('Restricted access');


class SttNmlsViewGarage extends JViewLegacy
{
    function display($tpl = null) 
    {
        $this->_allow_change_state = FALSE;

        // JOOMLA Instances
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $mGarage = JModelLegacy::getInstance('garage', 'SttNmlsModel');
        $mOrder =  JModelLegacy::getInstance('order', 'SttNmlsModel');
        $params = JComponentHelper::getParams('com_sttnmls');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');

        // Доступ к изменению состояния проверки объекта
        if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
            $this->_allow_change_state = TRUE;
        }

        
        $this->item = $this->get('Item');
        $this->activeComplaints = $this->get('ActiveComplaint');
        $this->agent = $this->get('AgentInfo');
        $this->myagent = $mGarage->findAgentByID($user->id);
        $this->order_info = $mOrder->getItem();
        $this->stype = (($this->item->VARIANT == 2) ? 1 : 0);
        $this->imgpath = $this->get('Imgpath');
        $this->tp = $this->get('Tp');
        $this->agthumb = $this->get('ComUserThumb');
        $this->what = $this->get('What');
        $task = JRequest::getVar('task', '');    
        
           
        if($task=='edit') {
            $this->optgtype = $this->get('OptGtype');
            $this->optcity = $this->get('OptCity');
            $this->firm_agents = $mAgent->getFirmAgentsList($_REQUEST['cid']);
        } else {
            if(!$this->item) {
                header("HTTP/1.0 404 Not Found");
            }
            $this->userperm = $this->get('UserPerm');
            $doc->setTitle($this->get('Title'));
            $doc->setDescription($this->get('Desc'));
            $this->arLink = $this->get('SimilarObjects');
        }
        
        parent::display($tpl);
    }
}
