<?php
defined('_JEXEC') or die('Restricted access');

class SttNmlsModelFirm extends JModelLegacy
{
    protected $_errors = array();
    protected $_events = array();

	public $_item;

    /**
     * Консолидирует ошибки
     *
     */
    private function raiseEx()
    {
        $args = func_get_args();
        $error_msg_alias = array_shift($args);
        $app = JFactory::getApplication();

        if(count($args) > 0) {
            $this->_errors[] = vsprintf(JText::_($error_msg_alias), $args);
            $app->enqueueMessage(vsprintf(JText::_($error_msg_alias), $args), 'error');
        } else {
            $this->_errors[] = JText::_($error_msg_alias);
            $app->enqueueMessage(JText::_($error_msg_alias), 'error');
        }
    }

    public function getErrors() {
        return $this->_errors;
    }

    /**
     * Консолидирует события
     *
     */
    private function raiseEvent()
    {
        $args = func_get_args();
        $event_msg_alias = array_shift($args);
        $app = JFactory::getApplication();

        if(count($args) > 0) {
            $this->_events[] = vsprintf(JText::_($event_msg_alias), $args);
            $app->enqueueMessage(vsprintf(JText::_($event_msg_alias), $args), 'notice');
        } else {
            $this->_events[] = JText::_($event_msg_alias);
            $app->enqueueMessage(JText::_($event_msg_alias), 'notice');
        }
    }

    public function getEvents()
    {
        return $this->_events;
    }

	public function getItem()
	{
        // Системные паттерны
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Переменные запроса
		$agency = $app->input->get('agency', 0, 'int');

		$query = $db->getQuery(TRUE);
        $query->select($db->quoteName('f') . '.*, ' . $db->quoteName('c.NAME', 'cityname'));
        $query->from($db->quoteName('#__sttnmlsvocfirms', 'f'));
		$query->join('LEFT', $db->quoteName('#__sttnmlsvoccity', 'c') . ' ON ' . $db->quoteName('f.CITYID') . '=' . $db->quoteName('c.id'));
		$query->where($db->quoteName('f.ID') . '=' . $db->quote($agency));
		$db->setQuery($query);
		$this->_item = $db->loadObject();

		if($this->_item)
		{
			require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';

			$this->_item->imgfirm = Avatar::getLinkFirmLogo($this->_item->ID, 1);
			$this->_item->thumbimg = Avatar::getLinkFirmLogo($this->_item->ID, 0);
			

			$this->_item->cntkv = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT=0 AND a.close=0 AND a.realtors_base<>1');
			$this->_item->cntkm = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT>0 AND a.close=0 AND a.realtors_base<>1');
			$this->_item->cntkvar = $this->getCnt('aparts',' a.VARIANT=2 AND a.WHAT=0 AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntkmar = $this->getCnt('aparts',' a.VARIANT=2  AND a.WHAT>0 AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntkv_expired = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT=0 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntkm_expired = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT>0 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntkvar_expired = $this->getCnt('aparts',' a.VARIANT=2  AND a.WHAT=0 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntkmar_expired = $this->getCnt('aparts',' a.VARIANT=2  AND a.WHAT>0 AND a.close=0 AND a.realtors_base<>1', true);
            //$this->_item->cntar_expired = $this->getCnt('aparts',' a.VARIANT=2 AND a.close=0 AND a.realtors_base<>1', true);
			$this->_item->cntcom = $this->getCnt('commerc',' a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1');
			$this->_item->cntcom1 = $this->getCnt('commerc',' a.VARIANT=2  AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntcom_expired = $this->getCnt('commerc',' a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntcom1_expired = $this->getCnt('commerc',' a.VARIANT=2  AND a.close=0 AND a.realtors_base<>1', true);
			$this->_item->cnths = $this->getCnt('houses', ' a.close=0 AND a.realtors_base<>1');
			$this->_item->cnths_expired = $this->getCnt('houses', ' a.close=0 AND a.realtors_base<>1', true);
			
            $this->_item->cntuch = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntuchar = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT=2 AND a.close=0 AND a.realtors_base<>1');
			$this->_item->cntuch_expired = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntuchar_expired = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT=2 AND a.close=0 AND a.realtors_base<>1', true);
			//$this->_item->cntuch_n_expired = $this->getCnt('buildings',' a.close=0 AND a.realtors_base<>1', true);
            
            $this->_item->cntgr = $this->getCnt('garages',' a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntgrar = $this->getCnt('garages',' a.VARIANT=2 AND a.close=0 AND a.realtors_base<>1');
            $this->_item->cntgrar_expired = $this->getCnt('garages',' a.VARIANT=2 AND a.close=0 AND a.realtors_base<>1', true);
            $this->_item->cntgr_expired = $this->getCnt('garages',' a.VARIANT<>2 AND a.close=0 AND a.realtors_base<>1', true);
            


			$user = JFactory::getUser();
			$userid = $user->get('id');
			$query2	= 'SELECT * FROM #__sttnmlsvocagents WHERE userid="'.$userid.'"';
			$db->setQuery($query2);
			$myagents	= $db->loadObject();
			if($agency==$myagents->COMPID && $userid>0)
			{
				$this->_item->showclose=1;
			}else{
				$this->_item->showclose=0;
			}


			$this->_item->cntkv_c = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT=0 AND a.close!=0 ');
			$this->_item->cntkm_c = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT>0 AND a.close!=0 ');
            $this->_item->cntkvar_c = $this->getCnt('aparts',' a.VARIANT=2  AND a.WHAT=0 AND a.close!=0 ');
			$this->_item->cntkmar_c = $this->getCnt('aparts',' a.VARIANT=2  AND a.WHAT>0 AND a.close!=0 ');
			$this->_item->cntar_c = $this->getCnt('aparts',' a.VARIANT=2 AND a.close!=0 ');
			$this->_item->cntcom_c = $this->getCnt('commerc',' a.VARIANT<>2 AND a.close!=0 ');
            $this->_item->cntcomar_c = $this->getCnt('commerc',' a.VARIANT=2 AND a.close!=0 ');
			$this->_item->cntcom1_c = $this->getCnt('commerc',' a.VARIANT=2  AND a.close!=0');
			$this->_item->cnths_c = $this->getCnt('houses', ' a.close!=0 ');
			$this->_item->cntuch_c = $this->getCnt('houses','a.VARIANT<>2  AND a.WHAT>0 AND a.close!=0 ');
            $this->_item->cntuchar_c = $this->getCnt('houses',' a.VARIANT=2  AND a.WHAT>0 AND a.close!=0 ');
            $this->_item->cntgr_c = $this->getCnt('garages',' a.VARIANT<>2 AND a.close!=0 ');
            $this->_item->cntgrar_c = $this->getCnt('garages',' a.VARIANT=2 AND a.close!=0 ');
            
            
            $this->_item->cntkv_r = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT=0 AND a.realtors_base>0 ');
			$this->_item->cntkm_r = $this->getCnt('aparts',' a.VARIANT<>2  AND a.WHAT>0 AND a.realtors_base>0 ');
			$this->_item->cntkvar_r = $this->getCnt('aparts',' a.VARIANT=2 AND a.WHAT=0 AND a.realtors_base>0 ');
            $this->_item->cntkmar_r = $this->getCnt('aparts',' a.VARIANT=2 AND a.WHAT>0 AND a.realtors_base>0 ');
            $this->_item->cntcom_r = $this->getCnt('commerc',' a.VARIANT<>2 AND a.realtors_base>0 ');
			$this->_item->cntcomar_r = $this->getCnt('commerc',' a.VARIANT=2 AND a.realtors_base>0 ');
            $this->_item->cntcom1_r = $this->getCnt('commerc',' a.VARIANT=2  AND a.realtors_base>0');
			$this->_item->cnths_r = $this->getCnt('houses', ' a.realtors_base>0 ');
			$this->_item->cntuch_r = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT<>2 AND a.realtors_base>0 ');
            $this->_item->cntuchar_r = $this->getCnt('houses',' a.WHAT>0 AND a.VARIANT=2 AND a.realtors_base>0 ');
            $this->_item->cntgr_r = $this->getCnt('garages',' a.VARIANT<>2 AND a.realtors_base>0 ');
            $this->_item->cntgrar_r = $this->getCnt('garages',' a.VARIANT=2 AND a.realtors_base>0 ');
            
            $this->_item->st_cntsale = $this->_item->cntkv + $this->_item->cntkm + $this->_item->cntcom + $this->_item->cnths + $this->_item->cntgr;
            $this->_item->st_cntrent = $this->_item->cntkvar + $this->_item->cntkmar + $this->_item->cntcomar + $this->_item->cnthsar + $this->_item->cntgrar;
            
		}
		return $this->_item;
	}

    /**
     * Функция получает информацию об организации по ее ID
     *
     * @param       int     $comp_id - ID организации
     * @return      bool|object
     */
    public function getFirmInfoByID($comp_id = 0)
    {
        $result = FALSE;

        if($comp_id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(TRUE);

            $query->select('*');
            $query->from($db->quoteName('#__sttnmlsvocfirms'));
            $query->where($db->quoteName('ID') . '=' . $db->quote($comp_id));
            $db->setQuery($query);

            $result = $db->loadObject();
        }

        return $result;
    }

    public function changeAgentPost()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlsvalidate.php' );

        $app = JFactory::getApplication();
        $db = JFactory::getDbo();

        $compid = $app->input->post->get('compid', 0, 'Int');
        $agent_id = $app->input->post->get('agent_id', 0, 'Int');
        $post_title = $app->input->post->get('post_title', '', 'String');

        // Валидация переданных данных
        if(
            !SttnmlsValidateHelper::is_natural_no_zero($compid) OR
            !SttnmlsValidateHelper::is_natural_no_zero($agent_id) OR
            $post_title == '' OR
            !SttnmlsValidateHelper::alpha_dash_ru_advanced($post_title)
        ) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_VERIFICATION_DATA');
            return FALSE;
        }

        // Проверка прав на редактирование организации
        if(!SttNmlsHelper::chkUserEditFirm($compid)) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Сохранение названия должности директора
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsvocagents'));
        $query->set($db->quoteName('boss_title') . '=' . $db->quote($post_title));
        $query->where($db->quoteName('kod') . '=' . $db->quote($agent_id));
        $db->setQuery($query);
        $db->execute();

        if($db->getErrorNum()) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SAVE_DATA');
            return FALSE;
        } else {
            $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_FIRM_SAVED');
            return TRUE;
        }
    }

    public function removeAgent()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $params = JComponentHelper::getParams('com_sttnmls');
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $_can_edit = JFactory::getUser()->authorise('core.admin', 'com_sttnmls');

        // Request VARS
        $compid = $app->input->post->getUint('compid', 0);
        $agent_id = $app->input->post->getUint('agent_id', 0);

        // Ничего не выполнять если нехватает данных
        if(!$compid OR !$agent_id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        // Проверка редактируемого агента
        $target_agent = $mAgent->getAgentByFirmID($compid, $agent_id);
        if(!$target_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }

        // Проверка редакторующего агента
        $editor_agent = $mAgent->getAgentInfoByID();
        if(!$editor_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }

        // Если пользователь БОСС в той же организации, то может редактировать
        if($target_agent->COMPID == $editor_agent->COMPID && $editor_agent->boss) {
            $_can_edit = TRUE;
        }

        // В остальных случаях редактирование запрещено
        if(!$_can_edit) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }

        $group = $params->get('halyavagroup', 0);
        $target_free_agent_compid = $params->get('compidsliv', 0);

        // Проверка указания категории свободных агентов
        if(!$target_free_agent_compid) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_FREE_AGENT_CATEGORY_SET');
            return FALSE;
        }


        // Подготовка к переводу агента в разряд свободных
        $query = $db->getQuery(TRUE);
        $query->select('MAX(ID)');
        $query->from($db->quoteName('#__sttnmlsvocagents'));
        $query->where($db->quoteName('COMPID') . '=' . $target_free_agent_compid);
        $db->setQuery($query);

        $max_id_free_agent_comp = (int)$db->loadResult() + 1;

        // Перенос удаляемого агента в группу свободных агентов
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsvocagents'));
        $query->set($db->quoteName('grp') . '=' . $db->quote($group));
        $query->set($db->quoteName('COMPID') . '=' . $db->quote($target_free_agent_compid));
        $query->set($db->quoteName('ID') . '=' . $db->quote($max_id_free_agent_comp));
        $query->where($db->quoteName('kod') . '=' . $db->quote($agent_id));
        $db->setQuery($query);
        $db->execute();

        // Пометка объектов агента флагом 4
        //@TODO: Не знаю, что за флаг 4 - узнать!
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsaparts'));
        $query->set($db->quoteName('flag') . '="4"');
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($target_agent->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($target_agent->ID));
        $db->setQuery($query);
        $db->execute();

        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsgarages'));
        $query->set($db->quoteName('flag') . '="4"');
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($target_agent->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($target_agent->ID));
        $db->setQuery($query);
        $db->execute();

        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlshouses'));
        $query->set($db->quoteName('flag') . '="4"');
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($target_agent->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($target_agent->ID));
        $db->setQuery($query);
        $db->execute();

        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlscommerc'));
        $query->set($db->quoteName('flag') . '="4"');
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($target_agent->COMPID));
        $query->where($db->quoteName('AGENTID') . '=' . $db->quote($target_agent->ID));
        $db->setQuery($query);
        $db->execute();

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_AGENT_REMOVED');
        return TRUE;
    }

    public function changeAgentPassword()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $_can_edit = JFactory::getUser()->authorise('core.admin', 'com_sttnmls');

        // Request VARS
        $user_id = $app->input->post->getUint('user_id', 0);
        $password = $app->input->post->getString('password', '');

        // Ничего не выполнять если нехватает данных
        if(!$user_id OR $password == '') {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        // Проверка редактируемого агента
        $target_agent = $mAgent->getAgentInfoByID($user_id, FALSE);
        if(!$target_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }

        // Проверка редакторующего агента
        $editor_agent = $mAgent->getAgentInfoByID();
        if(!$editor_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }

        // Если пользователь БОСС в той же организации, то может редактировать
        if($target_agent->COMPID == $editor_agent->COMPID && $editor_agent->boss) {
            $_can_edit = TRUE;
        }

        // В остальных случаях редактирование запрещено
        if(!$_can_edit) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }

        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__users'));
        $query->set($db->quoteName('password') . '= MD5(' . $db->quote($password) . ')');
        $query->where($db->quoteName('id') . '=' . $db->quote($user_id));
        $db->setQuery($query);
        $db->execute();

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_FIRM_SAVED');
        return TRUE;
    }

    public function confirmAgent()
    {
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

        // JOOMLA Instances
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $mAgent = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $_can_edit = JFactory::getUser()->authorise('core.admin', 'com_sttnmls');

        // Request VARS
        $compid = $app->input->post->getUint('compid', 0);
        $agent_id = $app->input->post->getUint('agent_id', 0);

        // Ничего не выполнять если нехватает данных
        if(!$compid OR !$agent_id) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS');
            return FALSE;
        }

        // Проверка редактируемого агента
        $target_agent = $mAgent->getAgentByFirmID($compid, $agent_id);
        if(!$target_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }

        // Проверка редакторующего агента
        $editor_agent = $mAgent->getAgentInfoByID();
        if(!$editor_agent) {
            $this->_errors = $mAgent->getErrors();
            return FALSE;
        }


        // Если пользователь БОСС в той же организации, то может редактировать
        if($target_agent->COMPID == $editor_agent->COMPID && $editor_agent->boss) {
            $_can_edit = TRUE;
        }

        // В остальных случаях редактирование запрещено
        if(!$_can_edit) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED_EDIT');
            return FALSE;
        }

        // Получаем группу директора
        $query = $db->getQuery(TRUE);
        $query->select($db->quoteName('grp'));
        $query->from($db->quoteName('#__sttnmlsvocagents'));
        $query->where($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $query->where($db->quoteName('boss') . '=' . '1');
        $db->setQuery($query);
        $boss_grp = $db->loadResult();

        // Обновления профиля подтверждаемого агента
        $query = $db->getQuery(TRUE);
        $query->update($db->quoteName('#__sttnmlsvocagents'));
        $query->set($db->quoteName('flag') . '="2"');
        $query->set($db->quoteName('COMPID') . '=' . $db->quote($compid));
        $query->set($db->quoteName('grp') . '=' . $db->quote(($boss_grp) ? $boss_grp : 2));
        $query->where($db->quoteName('kod') . '=' . $db->quote($agent_id));
        $db->setQuery($query);

        $db->execute();

        $this->raiseEvent('COM_STTNMLS_MESSAGE_INFO_AGENT_CONFIRMED');
        return TRUE;
    }


	public function getCnt($table, $where = '', $expired = false)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(a.CARDNUM ) ');
		$query->from(' #__sttnmls'.$table.' AS a ');
		$query->join('LEFT','`#__sttnmlsvocagents` as ag on ag.COMPID=a.COMPID and ag.id=a.agentid');
		$query->join('LEFT','`#__sttnmlsgroup` as g on g.id=ag.grp');

		if($table=='buildings')
		{
			$query->where(' a.comp='.$db->quote($this->_item->ID));
		}else{
			$query->where(' a.COMPID='.$db->quote($this->_item->ID));
		}

		$query->where(' ag.flag<3');
		$query->where(' ag.flag<>1');
		$query->where(' a.INET=1');
		$query->where(' a.flag<3');
		$params = JComponentHelper::getParams('com_sttnmls');
		$days = $params->get('days', '30');
        if(!$expired) {
            $query->where(' ((IFNULL(g.days,0)>0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR>DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        } else {
            $query->where(' ((IFNULL(g.days,0)>0 and a.DATECOR<=DATE_SUB(NOW(), INTERVAL IFNULL(g.days,0) day)) or (IFNULL(g.days,0)<=0 and a.DATECOR<=DATE_SUB(NOW(), INTERVAL ' . (int) $days . ' day)))');
        }
		if($where) $query->where($where);

		/*if($_SERVER['REMOTE_ADDR']=='134.249.132.135')
		{
			echo $query."<br><br>";
			$db->setQuery($query);
			echo $db->loadResult();


		}*/

		$db->setQuery($query);
		return $db->loadResult();
	}
	public function getComUserAvatar($userid)
	{
		require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
		return Avatar::getUserThumbAvatar($userid);
	}
	public function getAgents()
	{
		if(!$this->_item->ID) return null;
		$db = $this->getDbo();
		$query = $db->getQuery(true);
        $query->select('concat(a.SIRNAME, " ", a.NAME, " ", a.SECNAME) as agname, a.*');
		$query->from(' `#__sttnmlsvocagents` as a ');
		$query->where(' COMPID='.$db->quote($this->_item->ID));
		$query->where(' concat(a.PHONE,a.ADDPHONE)<>"" ');
		$query->where(' flag<3 ');
		







		//$query->where(' flag<>1 ');
		$query->order(' SIRNAME ASC ');
		$db->setQuery($query);
		$res=$db->loadObjectList();
		foreach($res as $item)
		{
			$item->foto=$this->getComUserAvatar($item->userid);
		}
		return $res;
	}
	public function getTitle()
	{
		return $this->_item->NAME;
	}
	public function getDesc()
	{
		return SttNmlsLocal::GetText('portal: дома и квартиры в kurske. '.$this->_item->NAME.' и другие агентства недвижимости. Купи квартиру получше!');
	}
	public function getCategories()
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		return SttNmlsHelper::getCategories($this->_item->ID);
	}

	public function getCatf() {
		// получаем список с мультивыбором категорий. с отмеченными категориями для данной организации
		$html='<select id="jform_catf" name="jform[catf][]" class="form-control required" size="5" multiple="multiple">';
		$db = $this->getDbo();
		$db->setQuery('select c.id, c.name, ifnull(f.ID,0) as firm from #__sttnmlscatf as c
			left join #__sttnmlsfirmcatf as cf on c.id=cf.idcatf and cf.idfirm='.$db->quote($this->_item->ID).'
			left join #__sttnmlsvocfirms as f on cf.idfirm=f.ID
			order by c.id');
		$ret = $db->loadObjectList();
		foreach($ret as $cat) {
			$s = ' selected="selected"';
			if($cat->firm==0) $s='';
			$html .= '<option value="'.$cat->id.'" '.$s.'>'.$cat->name.'</option>';
		}
		$html .= '</select>';
		return $html;
	}

	function checkuser($compid)
	{
		require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
		return SttNmlsHelper::chkUserEditFirm($compid);
	}

	function save()
    {
        // Include additional classes
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );
        require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlsvalidate.php' );


        // Системные классы
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $mail = JFactory::getMailer();
        $params = JComponentHelper::getParams('com_sttnmls');
        $session = JFactory::getSession();

        $new_firm = FALSE;


        // Полученные данные формы
		$data  = $app->input->post->get('jform', array(), 'array');


        // Проверка прав на редактирование/добавление организации
        if(!SttnmlsValidateHelper::is_natural($data['ID']) OR !SttNmlsHelper::chkUserEditFirm($data['ID'])) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
            return FALSE;
        }

        // Проверка существования организации, если передан ID
        if($data['ID'] && !$this->getFirmInfoByID($data['ID'])) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_FIRM_NOT_FOUND');
            return FALSE;
        }

        // Подготовка запроса в базу данных для сохранения данных
        $query = $db->getQuery(TRUE);
        if($data['ID']) {
            $query->update($db->quoteName('#__sttnmlsvocfirms'));
            $query->where($db->quoteName('ID') . '=' . $db->quote($data['ID']));
        } else {
            $new_firm = TRUE;

            $query->insert($db->quoteName('#__sttnmlsvocfirms'));
            $query->set($db->quoteName('EXCHDATE') . '=NOW()');
            $query->set($db->quoteName('flag') . '=1');

            $db->setQuery('SELECT MAX(ID) FROM ' . $db->quoteName('#__sttnmlsvocfirms') . ' WHERE ID < 100000');
            $key = $db->loadResult();
            if($key >= 99001) {
                $key += 1;
            }
            $data['ID'] = $key;
            $data['realname'] = $data['NAME'];
            $data['active'] = 0;
        }

        foreach($data as $key => $val)
        {
            if($key != 'catf') {
                $query->set($db->quoteName($key) . '=' . $db->quote($val));
            }
        }

        $db->setQuery($query);
        $db->execute();

        if($db->getErrorNum()) {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_SAVE_DATA');
            $app->redirect(SttNmlsHelper::getSEFUrl('com_sttnmls', 'firm', '&layout=edit&agency=' . $data['compid']));
            return FALSE;
        } else {
            $catnames = '';
            $ids = array();
            $pk = ($data['ID']) ? $data['ID'] : (int) $db->insertid();

            // Удаление всех привязок категорий к организации
            $db->setQuery('DELETE FROM #__sttnmlsfirmcatf WHERE idfirm=' . $db->quote($pk));
            $db->execute();

            // Если переданы новые категории
            if(count($data['catf']) > 0) {
                // Обновление связей
                $query = $db->getQuery(TRUE);
                $query->insert($db->quoteName('#__sttnmlsfirmcatf'));
                $query->columns($db->quoteName(array('idcatf', 'idfirm')));

                foreach($data['catf'] as $value) {
                    $query->values($value . ', ' . $pk);
                    $ids[] = $value;
                }

                $db->setQuery($query);
                $db->execute();


                $query = $db->getQuery(TRUE);
                $query->select($db->quoteName('NAME'));
                $query->from($db->quoteName('#__sttnmlscatf'));
                $query->where($db->quoteName('id') . ' IN (' . implode(',', $ids) . ')');
                $db->setQuery($query);

                $res = $db->loadColumn();

                $catnames = implode(', ', $res);
            }

            if($new_firm) {

                // установим текущего пользователя руководителем новой организации
                $user = JFactory::getUser();
                if($user->id) {
                    // еще нужно бы переписать на него все объекты
                    $db->setQuery('SELECT * FROM ' . $db->quoteName('#__sttnmlsvocagents') . ' WHERE ' . $db->quoteName('userid') . '=' . $db->quote($user->id));
                    $old = $db->loadObject();

                    $db->setQuery('UPDATE ' . $db->quoteName('#__sttnmlsvocagents') . ' SET ' . $db->quoteName('boss') . '=1, ' . $db->quoteName('ID') . '=' . $user->id . '00, ' . $db->quoteName('COMPID') . '=' . $db->quote($pk) . ' WHERE ' . $db->quoteName('userid') . '=' . $db->quote($user->id));
                    $db->query();
                    $db->setQuery('UPDATE ' . $db->quoteName('#__sttnmlsaparts') . ' SET ' . $db->quoteName('FIRMNAME') . '=' . $db->quote($data['NAME']) . ', ' . $db->quoteName('AGENTID') . '=' . $user->id . '00, ' . $db->quoteName('COMPID') . '=' . $db->quote($pk) . ' WHERE ' . $db->quoteName('AGENTID') . '=' . $db->quote($old->ID) . ' AND ' . $db->quoteName('COMPID') . '=' . $db->quote($old->COMPID));
                    $db->query();
                    $db->setQuery('UPDATE ' . $db->quoteName('#__sttnmlscommerc') . ' SET ' . $db->quoteName('FIRMNAME') . '=' . $db->quote($data['NAME']) . ', ' . $db->quoteName('AGENTID') . '=' . $user->id . '00, ' . $db->quoteName('COMPID') . '=' . $db->quote($pk) . ' WHERE ' . $db->quoteName('AGENTID') . '=' . $db->quote($old->ID) . ' AND ' . $db->quoteName('COMPID') . '=' . $db->quote($old->COMPID));
                    $db->query();
                    $db->setQuery('UPDATE ' . $db->quoteName('#__sttnmlsgarages') . ' SET ' . $db->quoteName('FIRMNAME') . '='.$db->quote($data['NAME']) . ', ' . $db->quoteName('AGENTID') . '=' . $user->id . '00, ' . $db->quoteName('COMPID') . '=' . $db->quote($pk) . ' WHERE ' . $db->quoteName('AGENTID') . '=' . $db->quote($old->ID) . ' AND ' . $db->quoteName('COMPID') . '=' . $db->quote($old->COMPID));
                    $db->query();
                    $db->setQuery('UPDATE ' . $db->quoteName('#__sttnmlshouses') . ' SET ' . $db->quoteName('FIRMNAME') . '=' . $db->quote($data['NAME']) . ', ' . $db->quoteName('AGENTID') . '=' . $user->id . '00, ' . $db->quoteName('COMPID') . '=' . $db->quote($pk) . ' WHERE ' . $db->quoteName('AGENTID') . '=' . $db->quote($old->ID) . ' AND ' . $db->quoteName('COMPID') . '=' . $db->quote($old->COMPID));
                    $db->query();
                }

                // отправим администратору письмо, что создана новая фирма
                $sendadmin = $params->get('adminemail', '');

                if($sendadmin != '') {
                    $mailfrom = $app->getCfg('mailfrom');
                    $fromname = $app->getCfg('fromname');
                    $subject = JText::_('COM_STTNMLS_FIRMADD_SUBJECT') . $fromname;
                    $fio = $old->SIRNAME.' '.$old->NAME.' '.$old->SECNAME;
                    $fio = '<a href="' . SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=default&agent_id=' . $user->id . '00&comp_id=' . $pk) . '">' . $fio . '</a>';
                    $body = JText::sprintf('COM_STTNMLS_FIRMADD_BODY1', $fio) . $data['NAME'];
                    $body .= JText::_('COM_STTNMLS_FIRMADD_BODY2') . $data['addr'];
                    $body .= JText::_('COM_STTNMLS_FIRMADD_BODY3') . $data['PHONE'];
                    $body .= JText::_('COM_STTNMLS_FIRMADD_BODY4') . $catnames;

                    $mail->isHTML(true);
                    $mail->addRecipient($sendadmin);
                    $mail->setSender(array($mailfrom, $fromname));
                    $mail->setSubject($subject);
                    $mail->setBody($body);
                    $mail->Send();
                }
            }

            $redirect = SttNmlsHelper::getSEFUrl('com_sttnmls', 'agent', '&layout=profile');
            $redirect = $session->get('firm.returnurl', $redirect);
            $app->redirect($redirect, JText::_('COM_STTNMLS_MESSAGE_INFO_FIRM_SAVED'));
            return TRUE;
        }
	}

    public function addPhoto()
    {
        // Хелпер аплоадера картинок
        require_once JPATH_COMPONENT.'/helpers/sttnmls_image.php';

        $agent_model = JModelLegacy::getInstance('agent', 'SttnmlsModel');
        $comp_id = JRequest::getInt('agency', 0);
        $user	= JFactory::getUser();

        // Пользователь вошел
        if($user->id) {
            $isAdmin = $user->authorise('core.admin');
            $agent = $agent_model->getAgentInfoByID($user->id);

            if($agent && $comp_id == $agent->COMPID) {
                $isAdmin = TRUE;
            }

            if(!$isAdmin) {
                $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_ACCESS_DENIED');
                return FALSE;
            }

            $config = array(
                'prefix_name'               =>  'firm' . $comp_id,
                'thumb_prefix_name'         =>  'thumb_firm' . $comp_id,
                'use_prefix_as_imagename'   =>  TRUE,
                'store_path'                =>  'images/agency/',
                'thumb_store_path'          =>  'images/agency/',
                'width'                     =>  160,
                'height'                    =>  160,
                'thumb_width'               =>  64,
                'thumb_height'              =>  64,
                'resize'                    =>  TRUE,
                'create_thumb'              =>  TRUE
            );

            $logo = SttnmlsHelperImage::getInstance();
            $logo->setConfig($config);
            $result = $logo->upload();

            $this->_errors = $logo->getErrors();
            if(!$result OR $this->_errors) { return FALSE; }

            $result[0]->alt = (($agent) ? $agent->firmname : '');

            return $result;
        } else {
            $this->raiseEx('COM_STTNMLS_MESSAGE_ERROR_AUTHORIZE');
            return FALSE;
        }
    }



//	public function addPhoto($idfirm,$main)
//	{
//		require_once JPATH_ADMINISTRATOR . '/components/com_sttnmls/helpers/avatar.php';
//		if($this->checkuser($idfirm)){
//			return Avatar::addFirmLogo('myfile', $idfirm, $main);
//		}
//		return Avatar::getFirmLogo($idfirm);
//	}
}
