<?php defined('_JEXEC') or die('Restricted access');

// Include additional classes
require_once( JPATH_ROOT . '/components/com_sttnmls/helpers/sttnmlshelper.php' );

// Load JOOMLA core frameworks
JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');


// Get JOOMLA core classes
$app = JFactory::getApplication();
$menu = $app->getMenu()->getActive();
$doc = JFactory::getDocument();
$user = JFactory::getUser();
$params = JComponentHelper::getParams('com_sttnmls');
$component_version = ((SttNmlsHelper::getComoponentVersion()) ? SttNmlsHelper::getComoponentVersion() : time());
$min_price = $params->get('garage_min_price_' . (($this->stype) ? 'rent' : 'sale'));

$limitobj = SttNmlsHelper::checkLimit(); //для базы риэлторов

// Если нужно использовать CSS Bootstrap из компонента
if($params->get('loadbootstrap', 0)) {
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap.min.css');
    $doc->addStyleSheet( JURI::root() . 'components/com_sttnmls/assets/css/bootstrap-theme.min.css');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap.min.js');
    $doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-typeahead.min.js');
}

// Additional component CSS
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/jquery.fileupload-ui.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/bootstrap-select.min.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/ajax-bootstrap-select.css');
$doc->addStyleSheet('components/com_sttnmls/assets/css/com_sttnmls.css?' . $component_version);

// Additional component JS
$script = 'var ROOT_URI="' . JURI::root() . '";' . "\n";
$script .= <<<JS
    var viewname = "house";
    var cardnum="{$this->item->CARDNUM}";
    var compid="{$this->item->COMPID}";

    jQuery(document).ready(function($){
		
		/*для базы риэлторов*/
		$('.hideRealtorBaseBlock').on('click', function(){
            $('#RealtorBaseBlock').slideToggle('fast');
        });
		
		$('.hideClientContactsBlock').on('click', function(){
            $('#ClientContactsBlock').slideToggle('fast');
        });
	
        $('.showHideBlocks').on('click', function(){
            $('#hideBlock').slideToggle('fast');
        });
		
        document.formvalidator.setHandler('streetid', function(value) {
            var streetid = $('#streetid').val() || 0;
            var result = (streetid > 0);

            if(result === false) {
                $('#street_selector').closest('div').addClass('invalid');
            } else {
                $('#street_selector').closest('div').removeClass('invalid');
            }

            return result;
        });

        document.formvalidator.setHandler('sttnum', function(value) {
			value = str_replace(" ","",value);
			var v = value.replace(',', '.');
			if(parseFloat(v) == 0) return false;
			regex = /^-?\d+((\.|\,)\d+)?$/;
			return regex.test(v);
        });

        document.formvalidator.setHandler('price', function(value) {
            var min_price = {$min_price};
            var regex = /^\d+$/;

            value = parseInt(value.replace(/\s+/g, ''));
			if(value === 0) {
			    return false;
			}

			if(value && min_price && value < min_price) {
                alert('Цена не может быть меньше ' + min_price + ' руб.');
                return false;
            }

			return regex.test(value);
        });

    });
    
JS;
$doc->addScriptDeclaration($script);
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery-ui.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.iframe-transport.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.fileupload.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/jquery.inputmask.numeric.extensions.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/ajax-bootstrap-select.min.js');
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/edit.js?' . $component_version);
$doc->addScript( JURI::root() . 'components/com_sttnmls/assets/js/editForRealtors.js');

$pics = (trim($this->item->PICTURES) != '') ? explode(';',$this->item->PICTURES) : '';
$picnames = (trim($this->item->PICNAMES) != '') ? explode(';',$this->item->PICNAMES) : '';
$stype = ($this->stype == 0) ? 0 : 1;

$this->item->ulica = explode(',', $this->item->ulica);

$active_tab = $app->input->get('tab', '', 'string');


// Пользователь - собственник?
$_is_owner = (!$this->firm_agents OR $params->get('compidsob', 0) == $this->firm_agents[0]->COMPID);

?>
<div class="com_sttnmls sttnmls_profile" style="margin-bottom: 20px;">
    <div class="object-card-edit-wrapper">
    <?php if($this->item->CARDNUM == 0) : ?>
        <h2><?php echo JText::_('COM_STTNMLS_EDITNEWGARAGE' . $stype); ?></h2>
    <?php else : ?>
        <?php if($this->item->flag == 2) : ?>
        <h2><?php echo JText::_('COM_STTNMLS_EDITGARAGE' . $stype); ?></h2>
        <?php else: ?>
        <h3><?php echo JText::_('COM_STTNMLS_NOEDIT'); ?></h3>
        <?php endif; ?>
    <?php endif; ?>
        
        <form class="form-horizontal form-validate" action="<?php echo JRoute::_('index.php?option=com_sttnmls&view=garage&task=save') ?>" method="post" autocomplete="off">
            <?php echo JHTML::_( 'form.token' ); ?>
            <input type="hidden" name="stype" value="<?php echo $this->stype; ?>"/>
            <input type="hidden" name="jform[cardnum]" value="<?php echo $this->item->CARDNUM; ?>"/>
            <input type="hidden" name="jform[compid]" value="<?php echo $this->item->COMPID; ?>"/>
            <input type="hidden" name="jform[agentid]" value="<?php echo $this->item->AGENTID; ?>"/>
            <input type="hidden" id="f_raionid" value="<?php echo $this->item->RAIONID; ?>"/>
            <input type="hidden" id="f_mraionid" value="<?php echo $this->item->MRAIONID; ?>"/>
            <input type="hidden" id="f_street" value="<?php echo $this->item->ulica[0] ?>"/>
            <input type="hidden" id="streetid" name="jform[streetid]" class="validate-streetid" value="<?php echo (($this->item->STREETID) ? $this->item->STREETID : 0); ?>"/>
            
            <div class="well well-sm text-right">
            <?php if($this->item->CARDNUM) : ?>    
                <?php if($this->item->flag == 2) : ?>
                <input name="save" type="submit" class="btn btn-success validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                <input name="apply" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
                <input name="addnew" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
                <?php endif; ?>
            <?php else : ?>
                <input name="save" type="submit" class="btn btn-success validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                <input name="newobj" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
            <?php endif; ?>
                <input name="cancel" type="submit" class="btn btn-danger sbred edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
            </div>
        
            <ul class="nav nav-tabs">
                <li role="presentation"<?php echo (($active_tab == '' OR !$this->item->CARDNUM) ? ' class="active"' : '') ?>>
                    <a href="#sttnmls-object-info" id="tab-sttnmls-object-info" role="tab" data-toggle="tab">
                        <?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INFO') ?>
                    </a>
                </li>

                <?php if($this->item->CARDNUM) : ?>
                <li role="presentation"<?php echo (($active_tab == 'photo') ? ' class="active"' : '') ?>>
                    <a href="#sttnmls-photo" id="tab-sttnmls-photo" role="tab" data-toggle="tab">
                        <b><?php echo JText::_('COM_STTNMLS_LABEL_PHOTO') ?></b>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
            
            <div class="tab-content" id="tab-sttnmls">
                <!-- TAB: Garage params -->
                <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == '' OR !$this->item->CARDNUM) ? ' active in' : '') ?>" id="sttnmls-object-info" aria-labelledby="tab-sttnmls-object-info">
                    <div class="tab-content-wrapper">
                        <!-- ROW: 1 -->
                        <div class="row">
                            <!-- COLUMN: LEFT -->
                            <div class="col-md-6">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_PARAMS_GARAGE') ?></h4>
                                <hr />
                                
                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_GARAGE_TYPE') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-sm-6 col-md-7">
                                        <?php echo $this->optgtype; ?>
                                    </div>
                                </div>
                                
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_ADDRESS') ?></h4>
                                <hr />
                                
                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_CITY') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-sm-6 col-md-7">
                                        <select id="cityid" name="jform[cityid]" class="form-control" onchange="changecity();">
                                            <?php echo JHtml::_('select.options', $this->optcity, 'value', 'text', $this->item->CITYID);?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_RAION') ?>
                                    </label>
                                    <div class="col-sm-6 col-md-7" id="raion">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_MRAION') ?>
                                    </label>
                                    <div class="col-sm-6 col-md-7" id="mraion">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_STREET') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-sm-6 col-md-7">
                                        <select id="street_selector" class="selectpicker form-control">
                                            <?php if($this->item->STREETID && $this->item->STREETID != -1) : ?>
                                                <option value="<?php echo $this->item->STREETID ?>" selected="selected"><?php echo $this->item->ulica[0] ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <!-- Цена -->
                                <div class="form-group">
                                    <label class="col-sm-6 col-md-5 control-label">
                                        <?php echo JText::_('COM_STTNMLS_LABEL_PRICE') ?>
                                        <span class="important">*</span>
                                    </label>
                                    <div class="col-sm-6 col-md-7">
                                        <div class="input-group">
                                            <input type="text" id="price"  name="jform[price]" class="form-control validate-price required price" value="<?php echo (int)$this->item->PRICE ?>" align="right"/>
                                            <span class="input-group-addon"><?php echo JText::_('COM_STTNMLS_LABEL_RUB') ?></span>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" name="checkprice" />
                                                <?php echo JText::_('COM_STTNMLS_SQMPRICE'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- //Цена -->
                                
                                <hr />
                                
                                <!-- CHECKBOXES -->
                               <div class="form-group">
										<div class="col-sm-offset-6 col-sm-6 col-md-offset-5 col-md-7">
											<div class="radio checkBase">
												<?php if(
														$limitobj<1 and 
														(($this->item->close==1 or $this->item->close==NULL) or 
														($this->item->realtors_base==1 or $this->item->realtors_base==NULL))
														) :?>
												<br/><a href="/lichnyj-kabinet.html?view=robokassa&layout=default" target="_blank" style="color: green;">
													<span>Вы достигли лимита объектов! Новые объекты будут добавляться автоматически в  
													<?php
														echo (($_is_owner) ? ' скрытую базу' : ' базу агентов');;
													?>
													</span>
												</a>
												<?php endif;?>
												<label>
													<input type="radio" value="3" id="openBase" name="access_base"
														<?php if(
														$limitobj<1 and 
														(($this->item->close==1 or $this->item->close==NULL) or 
														($this->item->realtors_base==1 or $this->item->realtors_base==NULL))
														) 
															echo 'disabled="disabled;"'?> 
														<?php if(!$_is_owner) echo 'onclick = "accessBase(this)"';?> 
														<?php echo ((
														(($this->item->realtors_base == 1 or $this->item->realtors_base == 2) or $this->item->close == 0) 
																	 or
																	 ($this->item->close===NULL and $limitobj>0)
																	 ) ? ' checked ' : ''); ?> />
																	 
													<span <?php if($limitobj<1 and 
														(($this->item->close==1 or $this->item->close==NULL) or 
														($this->item->realtors_base==1 or $this->item->realtors_base==NULL))) :?>
															style="color: #c5c5c5;"<?endif;?>>
														
														<?php echo JText::_('COM_STTNMLS_LABEL_OPEN_BASE') ?>
													</span>
												</label>

												<?php if(!$_is_owner) : ?>		
												<label>
													<input type="radio" class="accessBaseInput" value="2" name="access_base"
														   <?php if(!$_is_owner) echo 'onclick = "accessBase(this)"';?> 
														   <?php echo ((
																		($this->item->realtors_base == 1)
																		or
																		($this->item->close===NULL and !$_is_owner and $limitobj<1)
																		) ? ' checked ' : '') ?> />
													<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_REALTORS_BASE') ?>
												</label>
												<?php endif; ?>
												<label>
													<input type="radio" value="1" name="access_base"
													<?php if(!$_is_owner) echo 'onclick = "accessBase(this)"';?>  
													<?php echo ((
																 ($this->item->close == 1)
																 or
																 ($this->item->close === NULL and $_is_owner and $limitobj<1)													
																) ? ' checked ' : '') ?>/>
														<?php //var_dump($this->item->close);?>
													<?php echo JText::_('COM_STTNMLS_LABEL_OBJECT_INTERNAL_BASE') ?>
													<?php //var_dump($this->item->close == 1);?>
												</label>
											</div>
										</div>	
									</div>	
								<?php if(!$_is_owner) : ?>
									<div class="form-group">
										<div class="col-sm-offset-6 col-sm-6 col-md-offset-5 col-md-7">
											<div class="checkbox">
												<label>
													<input type="checkbox"  value="1" name="open_and_realtors" 
														<?php if(
														$limitobj<1 and 
														(($this->item->close==1 or $this->item->close==NULL) or 
														($this->item->realtors_base==1 or $this->item->realtors_base==NULL))
														) 
															echo 'disabled="disabled;"'?>
														<?php if(!$_is_owner) echo 'onclick = "accessBase(this)"';?>
														<?php echo ((
																	 ($this->item->realtors_base == 2)
																	 or
																	 ($this->item->close == NULL and $limitobj>0 and !$_is_owner)
																	) ? ' checked ' : '') ?>/>
													<span
													<?php if($limitobj<1 and 
														(($this->item->close==1 or $this->item->close==NULL) or 
														($this->item->realtors_base==1 or $this->item->realtors_base==NULL))) :?>style="color: #c5c5c5;"<?endif;?>
													><?php echo JText::_('COM_STTNMLS_DUBL_INTO_REALTORS_BASE')?></span>
												</label>
											</div>
										</div>
									</div>
								<?php endif; ?>
                                <!-- //CHECKBOXES -->
                            </div>
                            <!-- /COLUMN: LEFT -->
                            
                            <!-- COLUMN: RIGHT -->
                            <div class="col-md-6">
                                <h4 style="margin-top: 20px;"><?php echo JText::_('COM_STTNMLS_LABEL_ADDITIONAL_INFO') ?></h4>
                                <hr />
                                                                
                                <div  style="margin-top:20px;">
                                    <div class="form-group">
                                        <label class="col-sm-6 col-md-3 control-label">
                                            <?php echo JText::_('COM_STTNMLS_LABEL_DESCRIPTION') ?>
                                        </label>
                                        <div class="col-sm-6 col-md-9">
                                            <textarea name="jform[misc]" class="form-control" rows="10"><?php echo $this->item->MISC; ?></textarea>
                                        </div>
                                    </div>

                                    <?php if($params->get('showfirm', '') == 1 OR $this->firm_agents[0]->boss == 1 OR $user->authorise('core.edit', 'com_sttnmls')) : ?>
                                    <div class="form-group">
                                        <label class="col-sm-6 col-md-3 control-label">
                                            <?php echo JText::_('COM_STTNMLS_LABEL_CREATE_FROM') ?>
                                        </label>
                                        <div class="col-sm-6 col-md-9">
                                            <select class="form-control" name="fromag">
                                                <option value="<?php echo $this->item->AGENTID ?>">От себя</option>
                                                <?php foreach($this->firm_agents as $agent) :?>
                                                <option value="<?php echo $agent->userid ?>"<?php echo (($this->item->AGENTID == $agent->ID) ? ' selected="selected"' : '') ?>><?php echo $agent->SIRNAME . " " . $agent->NAME . " " . $agent->SECNAME ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php else :?>
                                        <input type="hidden" name="fromag" value="<?php echo $this->item->AGENTID ?>" />
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- //COLUMN: RIGHT -->
                            
                        </div>
                        <!-- /ROW: 1 -->
                        
                        <!-- ROW: 2 -->
                        <?php if(!$_is_owner) : ?>
						
						<!-- Закрытая база для риэлторов -->
						<div class="row">
							<div class="col-md-6" >
								<h4 class="hideRealtorBaseBlock" style="cursor: pointer;"><?php echo JText::_('COM_STTNMLS_LABEL_FOR_REALTORS') ?>
                                <span class="edit-arrow"></span>
                                </h4>
								<hr />
								<div id="RealtorBaseBlock" style="display: none; ">
									<!--Число собственников-->
									<div class="form-group" >
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_OWNERS_NUMBER') ?>
										</label>
										<div class="col-sm-3 col-md-3">
											<input type="number" class="form-control" id="owners_number" name="jform[owners_number]" value="<?php echo $this->item->owners_number ?>" />
										</div>
									</div>
									
									<!--Размер задатка-->
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_DEPOSIT_AMOUNT') ?>
										</label>
										<div class="col-sm-6 col-md-7">
											<div class="input-group">
												<input type="number" id="deposit_amount"  name="jform[deposit_amount]" class="form-control" value="<?php echo $this->item->deposit_amount ?>" align="right"/>
												<span class="input-group-addon"><?php echo JText::_('COM_STTNMLS_LABEL_RUB') ?></span>
											</div>
										</div>
									</div>
									
									<!--Примечание для риэлторов-->
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_NOTE_FOR_REALTORS') ?>
										</label>
										<div class="col-sm-6 col-md-7">
											<textarea name="jform[note_for_realtors]" class="form-control" rows="10"><?php echo $this->item->note_for_realtors; ?></textarea>
										</div>
									</div>
									
									<!--Размер отката-->
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_ROLLBACK_SIZE') ?>
										</label>
										 <div class="col-sm-6 col-md-7">
											<div class="input-group">
												<input type="number" id="rollback_size"  name="jform[rollback_size]" class="form-control" value="<?php echo $this->item->rollback_size ?>" align="right"/>
												<span class="input-group-addon"><?php echo JText::_('COM_STTNMLS_LABEL_RUB') ?></span>
											</div>
										</div>
									</div>
									
									<!--Наличие обременения-->
									<div class="form-group">
										<div class="col-sm-offset-6 col-sm-6 col-md-offset-5 col-md-7">
											<div class="checkbox">
												<label>
													<input type="checkbox" id="encumbrance" name="encumbrance" value="1" <?php echo (($this->item->encumbrance) ? ' checked="checked"' : '') ?> />
													<?php echo JText::_('COM_STTNMLS_LABEL_ENCUMBRANCE') ?>
												</label>
											</div>
										</div>
									</div>
						<?php endif; ?>	
									
									
									<div class="form-group <?if ($_is_owner) echo 'apart-edit-exclusive'?>">
										<div class="col-sm-offset-6 col-sm-6 col-md-offset-5 col-md-7">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="1" name="EXCLUSIVE" <?php echo (($this->item->EXCLUSIVE != 0 or $_is_owner) ? ' checked="checked"' : '') ?>/>
													<span class="glyphicon glyphicon-asterisk"></span>
													<?php echo sprintf('%s(%s)', JText::_('COM_STTNMLS_LABEL_EXCLUSIVE'), JText::_('COM_STTNMLS_LABEL_EXCLUSIVE_DESC')) ?>
												</label>
											</div>
										</div>
									</div>
						<?php if(!$_is_owner) : ?>
									<!--Отправить на свой сайт-->
									<div class="form-group">
										<div class="col-sm-offset-6 col-sm-6 col-md-offset-5 col-md-7">
											<div class="checkbox">
												<label>
													<input type="checkbox" value="1" name="export" <?php echo (($this->item->export != 0 OR $_REQUEST['cn'] == 0) ? ' checked="checked"' : '') ?> />
													<?php echo JText::_('COM_STTNMLS_LABEL_REDIRECT_ON_YOUR_SITE') ?>
												</label>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<!--Конец: закрытая база риэлторов-->
						
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="hideClientContactsBlock" style="margin-top: 20px; cursor: pointer;"><?php echo JText::_('COM_STTNMLS_LABEL_CLIENT_CONTACTS') ?>
                                <span class="edit-arrow"></span>
                                </h4>
                                <hr />
								<div id="ClientContactsBlock" style="display: none; ">    
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_PHONE') ?>
										</label>
										<div class="col-sm-6 col-md-7">
											<input type="text" value="<?php echo $this->item->sob_tel;?>" name="sob_tel" id="jform_phone" class="form-control phone" placeholder="_(___)___-__-__" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_PHONE_ADDITIONAL') ?>
										</label>
										<div class="col-sm-6 col-md-7">
											<input type="text" value="<?php echo $this->item->sob_tel2;?>" name="sob_tel2" id="jform_phone2" class="form-control phone" placeholder="_(___)___-__-__" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-6 col-md-5 control-label">
											<?php echo JText::_('COM_STTNMLS_LABEL_CLIENT_INFO') ?>
										</label>
										<div class="col-sm-6 col-md-7">
											<input type="text" class="form-control" value="<?php echo $this->item->sob_fio;?>" placeholder="<?php echo JText::_('COM_STTNMLS_LABEL_CLIENT_INFO') ?>" name="sob_fio"/>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <!-- ROW: 2 -->
                    </div>
                </div>
                <!-- //TAB: Garage params -->
                
                <?php if($this->item->CARDNUM) : ?>
                <!-- TAB: Photo -->
                <div role="tabpanel" class="tab-pane fade<?php echo (($active_tab == 'photo') ? ' active in' : '') ?>" id="sttnmls-photo" aria-labelledby="tab-sttnmls-photo">
                    <div class="tab-content-wrapper">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <h4><?php echo JText::_('COM_STTNMLS_LABEL_ADD_PHOTO') ?></h4>
                                <hr />
                                
                                <div class="sttnmls_photo">
                                    <div class="upload_image">
                                        <span class="btn btn-block btn-success btn-lg fileinput-button">
                                            <span class="glyphicon glyphicon-open"></span>
                                            <span><?php echo JText::_('COM_STTNMLS_LABEL_ADD_PHOTO') ?></span>
                                            <input type="file" id="objectPhoto" name="files[]" data-url="<?php echo JRoute::_('index.php?option=com_sttnmls&Itemid=' . $menu->id . '&task=uploadObjectPhoto&format=raw') ?>" data-cn="<?php echo $this->item->CARDNUM ?>" data-compid="<?php echo $this->item->COMPID ?>" data-view="garage" multiple />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-8">
                                <h4><?php echo JText::_('COM_STTNMLS_LABEL_SORT_ORDER') ?></h4>
                                <hr />
                                <div class="well">
                                    <div id="images-list">
                                        <?php echo $this->loadTemplate('photo_list') ?>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
                <!-- /TAB: Photo -->
                <?php endif; ?>
            </div>
        
            <div class="well well-sm text-right" style="margin-top:20px;">
            <?php if($this->item->CARDNUM) : ?>    
                <?php if($this->item->flag == 2) : ?>
                <input name="save" type="submit" class="btn btn-success validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                <input name="apply" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE'); ?>"/>
                <input name="addnew" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_NEW'); ?>"/>
                <?php endif; ?>
            <?php else : ?>
                <input name="save" type="submit" class="btn btn-success validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CLOSE'); ?>"/>
                <input name="newobj" type="submit" class="btn btn-default validate edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_ADDPHOTO'); ?>"/>
            <?php endif; ?>
                <input name="cancel" type="submit" class="btn btn-danger sbred edit-submit-mobile" value="<?php echo JText::_('COM_STTNMLS_SAVE_CANCEL'); ?>"/>
            </div>
        </form>
    </div>
</div>