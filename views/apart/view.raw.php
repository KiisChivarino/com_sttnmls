<?php defined('_JEXEC') or die ('Restricted access');

JLoader::import('joomla.application.component.model');

class SttNmlsViewApart extends JViewLegacy
{
    function display($tpl = null) 
    {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $user = JFactory::getUser();
        $params = JComponentHelper::getParams('com_sttnmls');
        $mApart = $this->getModel('apart');
        $_is_admin = $user->authorise('core.admin', 'com_sttnmls');
        
        $cardnum = $app->input->get('cn', 0, 'uint');
        $compid = $app->input->get('cid', 0, 'uint');
        $layout = $app->input->get('layout', 'edit_photo_list', 'string');
        
        // AJAX data switcher
        switch ($layout) {

            case 'default_object_check_state_button':
                    $this->item = $mApart->getItem();

                    // Доступ к изменению состояния проверки объекта
                    if($_is_admin OR ($params->get('object_moderation.moderator', 0) > 0 && $user->id == $params->get('object_moderation.moderator', 0))) {
                        $this->_allow_change_state = TRUE;
                    } else {
                        $this->_allow_change_state = FALSE;
                    }
                break;

            case 'edit_photo_list':
                    if($cardnum == 0 OR $compid == 0) {
                        JError::raiseError('400', JText::_('COM_STTNMLS_MESSAGE_ERROR_NO_PARAMS'));
                        return FALSE;
                    }  
                    
                    $this->item = $mApart->getObjectImages('apart', $compid, $cardnum, TRUE);
                    $this->item->CARDNUM = $cardnum;
                    $this->item->COMPID = $compid;
                break;

            case 'edit_building_list_select':
                    $this->building_list = $mApart->getBuildingList($app->input->get('builder', 0, 'uint'));
                break;

        }
        
        parent::display($tpl);
    }
}